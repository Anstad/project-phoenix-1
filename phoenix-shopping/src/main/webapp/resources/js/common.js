/**** Dropdown menu event handler ****/
$(document).ready( function() {addFunctionToCallAfterAjax(function() {
    $('.dropdown').on({
        mouseenter: function() {
            if($(this).hasClass('is-hovered') == false) {
                $(this).toggleClass('is-hovered');    
            }
            $(this).children('ul').show();
        },
        mouseleave: function() {
            if($(this).hasClass('is-hovered')) {
                $(this).toggleClass('is-hovered');
            }
            var me = this;
            setTimeout(function() {

                var isHovering = $(me).hasClass("is-hovered");
                if (isHovering == false) {
                    $(me).children('ul').hide();
                }
            }, 50);
        }
    });
    
    $('#myCarousel').carousel({
        interval: 5000
    });
    $('#carousel-text').html($('#slide-content-0').html());
    //Handles the carousel thumbnails
    $('[id^=carousel-selector-]').click(function() {
        var id_selector = $(this).attr("id");
        var id = id_selector.substr(id_selector.length - 1);
        $('#myCarousel').carousel(parseInt(id));
    });
    // When the carousel slides, auto update the text
    $('#myCarousel').on('slid', function(e) {
        var id = $('.item.active').data('slide-number');
        $('#carousel-text').html($('#slide-content-' + id).html());
    });

    $('.accordion').on('show', function(e) {
        $(e.target).prev('.accordion-heading').find('.accordion-toggle div').addClass("arrowDown");
    });

    $('.accordion').on('hide', function(e) {
        $(this).find('.accordion-toggle div').not($(e.target)).removeClass('arrowDown').addClass("arrowUp");
    });
    
    
    updateFilterWidget();
    //init tooltips
    $("[rel=tooltip]").tooltip({ placement: 'right'});
    $(".tooltip-top").tooltip({ placement: 'top'});
    
    updateNumericInputs();
    addListenerToCheckoutLinks();
    
    $(".selectpicker").selectpicker();
    
    $('div.animateDD').css({"width": "90px", "margin-top": "0px"});
});});

function handleDefaultInputValue(id, value, applyStyling) {
    var el = document.getElementById(id);
    el.value = value;
    if (applyStyling) {
        el.style.color = "#808080";
        el.onclick = function() {
            el.value = "";
            el.style.color = "";
            el.onclick = function(){};
        };
    }
}

function addDatePickers() {
    $(".day-month-year-date").datepicker();
    $(".month-year-date").datepicker( {
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });    
}

function processData(data){
    var jsonData = jQuery.parseJSON( data );
    $('.favoritesListsInput .typeahead').typeahead({
        name: 'favorites',
        local: jsonData
    });
}

function setupSearchTypeahead(data){
    $('.searchSelectContainer .typeahead').typeahead([
    {
        name: 'search-input-typeahead',
        remote: {
            url: data,

            replace: function(url, query) {
                return url + encodeURI(query) + '/' + encodeURI($('select.animateDD').val());
            }
        },
        minLength: 3
    }
    ]).on('keyup', this, function (event) {
        if (event.which === 13) {
            $handleSearchRedirect();
            $handleSearchIconClick();
        }
    });
}

$cartNotify = function() {
	if (jsonData.display == "added") {
		$addNotify(jsonData.components);
	}
	if (jsonData.display == "notify") {
		$cartNotificationHandler(jsonData.components);
	}
        if (jsonData.display == "canceled") {
                $canceledNotify(jsonData.components);
        }
};

$clearFields = function() {
	document.getElementById("expressOrderForm:skuInput").value="";
	document.getElementById("expressOrderForm:qtyBox").value=0;
};

$canceledNotify = function(cartNotifications) {
    $("#notification-top-right").offset({left: $('#shopping-cart-dropdown').offset().left - 150});

    $('.shopping-cart').notify({
        type: 'cart-notification',
        cartNotification: cartNotifications
    }).show();
};

$addNotify = function(cartNotifications) {
    $("#notification-top-right").offset({left: $('#shopping-cart-dropdown').offset().left - 150});

    $('.shopping-cart').notify({
        type: 'cart-notification',
        cartNotification: cartNotifications
    }).show();
};

renderButtons = function(index) {
    var notification = $cartNotifications[index];
    if (notification == null) {
        return;
    }
    var buttons = getButtons(notification, index);
    var container = document.getElementById("wizard-footer-buttons");
    if (container != null && index > 0) {
        container.innerHTML = buttons;
    }
    else {
        var div = '<div class="wizard-modal-footer modal-footer" >'
                +       '<div class="wizard-buttons-container" id="wizard-footer-buttons">'
                + buttons
                +       '</div>'
                + '</div>';
        var wizardCardContainer = wizard.el.find(".wizard-card-container");
        wizardCardContainer.append(div);
    }
    var actionBtn = wizard.el.find("a.add-to-cart-index-" + index);
    actionBtn.click(wizard, wizard._handleNextClick);
};

$cartNotificationHandler = function(cartNotifications) {
    this.$cartNotifications = cartNotifications;
    this.$element = $("#top-container");
    this.$wizardBlock = $('<div class="wizard" id="wizard-demo"></div>');
    $wizardBlock.append('<h1>' + cartNotifications[0].header + '</h1>');
    $.each(cartNotifications, function(index, notification) {
    	var cardText = '<div class="wizard-card" data-cardname="' + notification.itemList[0].itemName + '">'
                + '    <h3 style="display:none;">' + notification.itemList[0].itemName + '</h3>' //this is here so that the menu on the left side works
                + '<div>'
                + '<h2 style="margin-top:0px">' + notification.line1 + '</h2>'
                + '<p>' + notification.line2 + '</p>'
                + itemComparisonTableGenerator(notification, index)
                + '</div>'
                + '</div>';
        
    	this.$card = $(cardText);
        $wizardBlock.append(this.$card);
    });

    this.$element.append(this.$wizardBlock);
    var options = {
        width: "900px",
        progressBarCurrent: false,
        buttons: {
            submitText: "Continue"
        }
    };
    wizard = $wizardBlock.wizard(options);
    
    renderButtons(0);
    
    var closeButton = wizard.el.find("button.wizard-close");

    closeButton.click(function() {
        $wizardBlock.remove();
        cancelAddToCart();
        wizard.reset();
        wizard.close();
    });
    
    if (wizard._firstShow) {
        wizard.setCard(0);
        wizard._firstShow = false;
    }
    wizard.el.modal({
        backdrop: "static",
        keyboard: false
    });
    wizard.on("submit", function() {
        $wizardBlock.remove();
        wizard.close();
        addToCartResolved();
    });
};

addToCartAction = function(action, index) {
	setActionForItem(action, index);
};

getButtons = function(notification, index) {
    if (notification == null) {
        return "";
    }
    var toReturn = '';
    for (var i = 0; i < notification.actionList.length; i++) {
        var id = 'add-to-cart-index-' + index;
    	toReturn += '<a id="' + id + '" class="btn ' + id + ' add-to-cart-action-' 
    		+ notification.actionList[i] + '" onclick="addToCartAction(\'' + notification.actionList[i] 
    		+ '\', ' + index + '); renderButtons(' + (index+1) + ');" style="margin-left:10px">' + notification.actionButtonMap[notification.actionList[i]] + '</a>';
    }
    return toReturn;
};

activateSubmitButton = function() {
    $(".btn-success").removeAttr("disabled");
};

itemComparisonTableGenerator = function(notification, index) {
    //Start Building Table
    var compareTable = '<table class="table table-bordered table-striped">';
    //Add Table Header
    compareTable +=
            '<thead>'                
            + '<tr>'
            + '<th/>';
    		for (var i = 0; i < notification.itemList.length; i++) {
    			var displayedItem = notification.itemList[i];
	            compareTable += '<th class="product_comparison">';
                    if (notification.itemList.length>1) {
                        if (i==0) {
                            compareTable += '<h3>Selected Item</h3>';
                        }
                        else {
                            compareTable += '<h3>Substitute Item</h3>';
                        }
                    }
	            compareTable += '<div class="thumbnail">'
	            + '<a href="#"><img alt="" src="' + displayedItem.imgURL + '" style="width:64px; height:64px;" /></a>'
	            + '<div id="product_image" class="caption">'
	            + ' <h5>' + displayedItem.itemName + '</h5>'
	            + '<p/>'
	            + '</div>'
	            + '</div>'
	            + '</th>';
            }
            compareTable += '</tr>'
            + '</thead>';
    //Add Table Rows 
    compareTable += '<tbody>'
            + '<tr>'
            + '<td>SKU</td>';
			for (var i = 0; i < notification.itemList.length; i++) {
				displayedItem = notification.itemList[i];
    			compareTable += ' <td>' + displayedItem.itemSku + '</td>';
    		}
    compareTable += '</tr>'
            + '<tr>'
            + '<td>Price</td>';
    		for (var i = 0; i < notification.itemList.length; i++) {
    			displayedItem = notification.itemList[i];
                        var price = "";
                        if (displayedItem.itemPrice !== null && displayedItem.itemPrice !== "") {
                            price = parseFloat(displayedItem.itemPrice).toFixed(2);
                        }
    			compareTable += ' <td>$' + price + '</td>';
    		}
            compareTable += '</tr>'
            + '<tr>'
            + '<td>Description</td>';
    		for (var i = 0; i < notification.itemList.length; i++) {
    			displayedItem = notification.itemList[i];
            	compareTable += ' <td>' + displayedItem.itemDescription + '</td>';
            }
            compareTable += '</tr>';

    		for (var i = 0; i < notification.specList.length; i++) {
    			spec = notification.specList[i];
            	compareTable += '<tr><td>' + spec + '</td>';
        		for (var j = 0; j < notification.itemList.length; j++) {
        			displayedItem = notification.itemList[j];
            		compareTable += '<td>';
            		if (spec in displayedItem.specMap) {
            			compareTable += displayedItem.specMap[spec];
            		}
            		compareTable += '</td>';
            	}
                compareTable += '</tr>';
            }
            compareTable += '</tbody>';

    //Close Table
    compareTable +=
            '</table>'
            ;

    return compareTable;
};

function addPopTriggers() { 
    $(".popover-trigger").each(function() {
        var $pElem = $(this);
        $pElem.popover(
            {
                html: true,
                placement: 'bottom',
                trigger: 'manual',
                title: getPopTitle(this),
                content: getPopContent(this)
            }
        );
        $pElem.mouseenter(function() {
            $pElem.popover('show');
            var $pop = $pElem.parent().children(".popover");
            $pElem.mouseleave(function() {
                $pop.mouseenter(function() {
                   $pop.addClass("is-hovered"); 
                });
                hidePopIfNotHovering($pop, $pElem);
            });
        });
    });
}

function hidePopIfNotHovering($pop, $pElem) {
    setTimeout(function() {
        if (!$pop.hasClass("is-hovered")) {
            $pElem.popover('hide');
        } else {
            $pop.mouseleave(function() {
                $pop.removeClass("is-hovered");
                hidePopIfNotHovering($pop, $pElem);
            });
        }
    }, 1200);
}

function getPopTitle(target) {
    return $(target).children(".popSourceBlock").children('.popTitle').html();
}


function getPopContent(target) {
    return $(target).children(".popSourceBlock").children('.popContent').html();
}




$("#search-form").keypress(function(event) {
    if (event.which === 13) {
        $handleSearchRedirect();
    }
});

$("#search-icon").click(function() {
    $handleSearchIconClick();
});

$("#refine-search-icon").click(function() {
   $handleRefineSearchRedirect(); 
});

$handleSearchRedirect = function() {
    $("#search-form").submit(function() {
        initSearch($('input[id=search-input]').val(), $('select.animateDD').val());
        return false;
    });
};

$handleSearchIconClick = function() {
    initSearch($('input[id=search-input]').val(), $('select.animateDD').val());
    return false;   
};

$handleRefineSearchRedirect = function() {
    initRefineSearch($('input[id=browse-form\\:refine-search-input]').val());
    return false;
};

function getComboboxIndex(id) {
    var x = document.getElementById(id).selectedIndex;
    var y = document.getElementById(id).options;
    return y[x].index;
}

function updateFilterWidget() {
    //This will support the more links for the filter widget
    $('ul.filterAttributeList').find('li:gt(2)').hide();

    $("ul.filterAttributeList").each(function(index) {
        if (this.childElementCount > 3) {
            $(this).append(
                    $("<li class='btn_more'><a>More...</a></li>").click(function() {
                $(this).siblings(':hidden').show().end().remove();

            })
                    );
        }
    });


}
$('.filter-show-all').click(function() {
    $('ul.filterCategoryList').find('li.activeCategoryFilter').show();
    $(this).remove();
});

$('ul.filterCategoryList').find('li.categoryFilter:gt(3)').hide();

$(".hideButton").click(function() {
    $(this).parent().parent().parent().parent().parent().hide();
});

function filterOn(filterCategoryName, filterCategoryChild) {
    var n = filterCategoryChild.lastIndexOf(" (");
    filterCategoryChild = filterCategoryChild.substring(0, n);
    applyFilter(filterCategoryName + "| " + filterCategoryChild);
}

function validateFilters() {
    var prmstr = window.location.search.substr(1);
    var prmarr = prmstr.split ("&");
    var params = {};

    for (var i=0; i<prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    var filters = decodeURIComponent(params.filters).replace(/\+/g," ").split("|||");
    $("input:checkbox:checked").each(function() {
        var keepChecked = "false";
        var filter = this.id.substring(0, this.id.lastIndexOf(" ("));
        for (var i=0; i<filters.length; i++) {
            if (filters[i]==filter) {
                keepChecked = "true";
            }
        }
        if (keepChecked=="false") {
            this.checked="";
            deactivateFilter(filter);
        }
    });
}

$('select.animateDD').change(function() {
    var myvalue = $('select.animateDD option:selected').text();
    mywidth = ((myvalue.length + 1) * 8) + 21;
    var maxWidth = 270;
    var minWidth = 90;
    var searchInputNewWidth = maxWidth;
    if (mywidth <= minWidth) {
        mywidth = minWidth;
        searchInputNewWidth = maxWidth;
    }
    else if (mywidth > maxWidth) {
    	mywidth = maxWidth;
    	searchInputNewWidth = minWidth;
    }
    else
    {
    	searchInputNewWidth = (maxWidth + minWidth - mywidth);
    }
    $('div.animateDD').animate({width: mywidth}, 400);
    $('#searchOuterBox').animate({width: searchInputNewWidth}, 400);
});

$('#searchOuterBox').click(function() {
	$('div.animateDD').animate({width: "90px"}, 400);
    $(this).animate({width: "270px"}, 400);
});

function spinner() {
    $('.qtyPlus').click(function() {
        var temp = $(this).siblings('input');
        temp.val(parseInt(temp.val()) + 1);
    });
    $('.qtyMinus').click(function() {
        var temp = $(this).siblings('input');
        if (parseInt(temp.val()) > 0) {
            temp.val(parseInt(temp.val()) - 1);
        }
    });
}

function showAjaxActive() {
    var ajaxloader = document.getElementById("ajaxloader");
    ajaxloader.style.display = 'block';
}
function hideAjaxActive() {
    var ajaxloader = document.getElementById("ajaxloader");
    ajaxloader.style.display = 'none';
}


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateNumericInputs(){
    $(".numeric").numeric();
    $(".integer").numeric(false, function() {
        alert("Integers only");
        this.value = "";
        this.focus();
    });
    $(".positive").numeric({negative: false}, function() {
        alert("No negative values");
        this.value = "";
        this.focus();
    });
    $(".positive-integer").numeric({decimal: false, negative: false}, function() {
        alert("Positive integers only");
        this.value = "";
        this.focus();
    });
}

function showPopup(popupModalId) {
    controlPopup(popupModalId, "show");
}
function hidePopup(popupModalId) {
    controlPopup(popupModalId, "hide");
}
function controlPopup(popupModalId, operation) {

    if (RichFaces) {
        if (operation == "show") {
            RichFaces.$(popupModalId).show();
        } else if (operation == "hide") {
            RichFaces.$(popupModalId).hide();
        }
    } else {
        throw "Supported JavaScript implementation of popup modal control could not be found.";
    }
}

function listInputNotEmpty() {
    var listInput = document.getElementById("productDetailsForm:selectedListNameInput");
    if (listInput.value == '') {
            document.getElementById("productDetailsForm:listInputRequiredMessage").style.display = '';
            return false;
    }
    return true;
}

function inkTonerBrandNotEmpty(value, defaultValue, errorId) {
    if (value == defaultValue) {
        document.getElementById(errorId).style.display='';
        return false;
    }
    return true;
}

function addListenerToCheckoutLinks() {
    $("a.cashout").click(function(e) {
        checkoutListener();
    });
    $(".cashout").click(function(e) {
        checkoutListener();
    });
}

function checkoutListener(e) {
    var cartIsEmpty = document.getElementById("cartIsEmpty").value;
    if (cartIsEmpty=="true") {
        var event = e || window.event;
        event.returnValue = false;
        if ( event.preventDefault ){ 
            event.preventDefault(); 
            showPopup("emptyCartModal");
        }
        return false;
    }
}

var shouldShowWaitModal = false;

function ajaxStartWait() {
	if (shouldShowWaitModal) {
		shouldShowWaitModal = false;
		showPopup('waitModal');
	}
}

function ajaxStopWait() {
	hidePopup('waitModal');
	for (var i = 0; i < functionsAfterAjax.length; i++) {
		functionsAfterAjax[i]();
	}
}

function waitOnNextAjax() {
	shouldShowWaitModal = true;
}

function addToCartResolved() {
	waitOnNextAjax();
	addToCartResolvedAjax();
}

function toggleShowFilters(filterCat) {
    var expanded = document.getElementById(filterCat + 'expanded');
    var collapsed = document.getElementById(filterCat + 'collapsed');
    if (expanded.style.display=="none") {
        expanded.style.display = "";
        collapsed.style.display = "none";
    }
    else if (collapsed.style.display=="none") {
        expanded.style.display = "none";
        collapsed.style.display = "";        
    }
}

function expandCheckedFilters() {
    $("input:checkbox:checked").each(function() {
        expandFilter(this.id.split("|")[0]);
    });
}

function expandAllFilters() {
    $(".filterContainer").each(function() {
        $(this).children(".row-fluid").each(function() {
            $(this).children("div[id~='expanded']").each(function () {
                expandFilter(this.id.substring(0, this.id.lastIndexOf(" "))+" ");
            });
        });
    });
}

function collapseAllFilters() {
    $(".filterContainer").each(function() {
        $(this).children(".row-fluid").each(function() {
            $(this).children("div[id~='expanded']").each(function () {
                collapseFilter(this.id.substring(0, this.id.lastIndexOf(" "))+" ");
            });
        });
    });    
}

function expandFilter(filterCat) {
    var expanded = document.getElementById(filterCat + 'expanded');
    var collapsed = document.getElementById(filterCat + 'collapsed');
    expanded.style.display = "";
    collapsed.style.display = "none";
}

function collapseFilter(filterCat) {
    var expanded = document.getElementById(filterCat + 'expanded');
    var collapsed = document.getElementById(filterCat + 'collapsed');
    expanded.style.display = "none";
    collapsed.style.display = "";       
}

var functionsAfterAjax = [];

function addFunctionToCallAfterAjax(f) {
	f();
	functionsAfterAjax[functionsAfterAjax.length] = f;
}

function setSearchDropdownValue(hierarchyPath) {
	var select = $('select.animateDD');
	$('select.animateDD > option').each(function() {
		//checks if the hierarchy path starts with the option, if so, selects it
		if (hierarchyPath.indexOf($(this).val()) == 0 && select.val().length < $(this).val().length) {
			select.val($(this).val());
		}
	});
	$('.selectpicker').selectpicker('refresh');
}
