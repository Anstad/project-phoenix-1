function setGetParameter(paramName, paramValue, url)
            {
                if(paramValue === null){
                	window.location.href = url;
                }
                else{
                    var splitAtAnchor = url.split('#');
                    url = splitAtAnchor[0];
                    var anchor = typeof splitAtAnchor[1] === 'undefined' ? '' : '#' + splitAtAnchor[1];
                if (url.indexOf(paramName + '=') >= 0)
                {
                    var prefix = url.substring(0, url.indexOf(paramName));
                    var suffix = url.substring(url.indexOf(paramName));
                    suffix = suffix.substring(suffix.indexOf('=') + 1);
                    suffix = (suffix.indexOf('&') >= 0) ? suffix.substring(suffix.indexOf('&')) : '';
                    url = prefix + paramName + '=' + paramValue + suffix;
                }
                else
                {
                if (url.indexOf('?') < 0)
                    url += '?' + paramName + '=' + paramValue;
                else
                    url += '&' + paramName + '=' + paramValue;
                }
                window.location.href = url + anchor;
                }
            }