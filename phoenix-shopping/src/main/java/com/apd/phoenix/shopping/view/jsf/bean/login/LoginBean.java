package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.jacc.PolicyContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.business.PasswordResetEntryBp;
import com.apd.phoenix.service.business.SingletonPropertiesLoader;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.utility.JSFUtils;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.ShoppingCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ValidationPropertiesLoader;
import com.apd.phoenix.web.utils.SessionUtils;

@Named
@Stateful
@SessionScoped
@Lock(LockType.READ)
public class LoginBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -1127601116139535684L;
    private static final Logger logger = LoggerFactory.getLogger(LoginBean.class);
    private static final String PUNCHOUT_SESSION_ATTRIBUTE = "punchoutSessionToken";
    private static final String resetPasswordMessage = "login.info.passwordReset";
    private static final String CHS_SUBDOMAIN_NAME = "css";
    private static final String WALMART_SUBDOMAIN_NAME = "walmart";
    private static final String NOVANT_SUBDOMAIN_NAME = "novant";
    private static final String CINTAS_SUBDOMAIN_NAME = "cnts";
    private static final String EBIZ_SUBDOMAIN_NAME = "ebiz";
    private static final String FEMA_SUBDOMAIN_NAME = "fema";
    private static final String TEMA_SUBDOMAIN_NAME = "tema";
    private static final String MARFIELD_SUBDOMAIN_NAME = "marfield";
    private static final String CINCINNATI_SUBDOMAIN_NAME = "cincinnati";
    private static final String CUSTOM_CHANGE_USER_PAGE = "/ecommerce/user/changeUser.xhtml";
    private static final String CUSTOM_CHANGE_LOCATION_PAGE = "/ecommerce/user/changeLocation.xhtml";
    private static final String CUSTOM_ADD_LOCATION_PAGE = "/ecommerce/user/addLocation.xhtml";
    private static final String CUSTOM_CREATE_USER_PAGE = "/ecommerce/user/cCreateUserRequest.xhtml";
    private static final String DEFAULT_CREATE_USER_PAGE = "/ecommerce/user/createUserRequest.xhtml";
    private static final String CINTAS_CREATE_USER_PAGE = "/ecommerce/user/cnts/createUserRequest.xhtml";
    private static final String HOMEPAGE = "/shopping/ecommerce/home.xhtml";
    private static final String PW_RESET_PATH = "/ecommerce/login/newPassword.xhtml";
    private static final String DIRECT_ACCESS_USERNAME_KEY = "directAccessUsernameKey";
    private static final String DIRECT_ACCESS_PASSWORD_KEY = "directAccessPasswordKey";
    private static final String NOVANT_REGISTRATION_INFO_PAGE = "/ecommerce/user/registrationInfo.xhtml";

    private String username;
    private String password;
    private String passResetUsername;
    private SystemUser systemUser;
    private Boolean hideUserRegistrationLink;
    private Boolean hideUserResetPasswordLink;
    //stores the ip address being used
    private Set<String> validIpAddresses = new HashSet<String>();
    //stores the subdomain being used
    private Set<String> validSubdomains = new HashSet<String>();
    private String punchoutSessionToken;

    @Inject
    private SystemUserBp systemUserBp;

    @Inject
    private PasswordResetEntryBp passwordResetEntryBp;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private AccountBp accountBp;

    @Inject
    private ShoppingCartBean shoppingCartBean;

    @PostConstruct
    @Lock(LockType.WRITE)
    public void init() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        String[] ipAndSubdomain = IpSubdomainRestriction.ipAndSubdomain(request);
        validIpAddresses.add(ipAndSubdomain[0]);
        validSubdomains.add(ipAndSubdomain[1]);

    }

    /**
     * This method returns the currently logged in SystemUser. If no user is logged in, the session is invalidated.
     * 
     * @return
     */
    public SystemUser getSystemUser() {

        //Returns the current user instance if it is not null and exists on the database
        if (this.systemUser != null && this.systemUser.getId() != null) {
            return this.systemUser;
        }

        ExternalContext context = null;
        try {
            //if the username has not been set on the bean, gets it from the current context
            if (this.getUsername() == null) {
                //looks up the login based on the context's principal.
                context = FacesContext.getCurrentInstance().getExternalContext();
                this.username = context.getUserPrincipal().getName();
            }

            //Set the default username and password for pages that require direct access
            if (this.getRequiresDirectAccess()) {

                Properties properties = PropertiesLoader.getAsProperties("ecommerce");
                if (properties != null) {
                    username = properties.getProperty(DIRECT_ACCESS_USERNAME_KEY, "");
                    password = properties.getProperty(DIRECT_ACCESS_PASSWORD_KEY, "");
                }

                //We want to generate a failure if the properties file cannot be reached, therefore, we will not use an else condition.
            }

            if (StringUtils.isNotEmpty(punchoutSessionToken)) {

                if (credentialSelectionBean.getPunchoutSession() == null) {
                    credentialSelectionBean.loadPunchoutSession(punchoutSessionToken);
                }

                try {
                    this.systemUser = systemUserBp.getSystemUserByLogin(
                            credentialSelectionBean.getPunchoutSession().getSystemUserLoginName()).get(0);
                }
                catch (IndexOutOfBoundsException ex) {
                    throw new IllegalStateException("No SystemUser with the login name: "
                            + credentialSelectionBean.getPunchoutSession().getSystemUserLoginName());
                }
            }
            else {
                //searches for the user with that login
                this.systemUser = systemUserBp.getSystemUserByLogin(username).get(0);
            }
        }
        catch (Exception ex) {
            logger.error(null, ex);
            //if any error occurs (such as the principal being null or no user existing for that name)
            //the session is invalidated.
            this.systemUser = new SystemUser();
            if (context != null) {
                context.invalidateSession();
            }
        }

        return this.systemUser;
    }

    public void setSystemUser(SystemUser systemUser) {
        this.systemUser = systemUser;
    }

    //    /**
    //     * Called by the logout button in the topNavBar.xhtml file. Ends the session and redirects to login page.
    //     */
    //    public void logout() {
    //        //invalidates the current session
    //        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
    //        context.invalidateSession();
    //        try {
    //            //redirects to the login page.
    //            context.redirect(context.getRequestContextPath() + "/ecommerce/home.xhtml");
    //        }
    //        catch (IOException e) {
    //            logger.error("IOException", e);
    //        }
    //    }

    @Lock(LockType.WRITE)
    public void logout() {
        String logoutRedirect = "/ecommerce/home.xhtml";

        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            if (facesContext != null) {
                ExternalContext context = facesContext.getExternalContext();
                SessionUtils.invalidateSessionIfValid(((HttpServletRequest) context.getRequest()),
                        ((HttpSession) context.getSession(false)));
                context.redirect(context.getRequestContextPath() + logoutRedirect);
            }
            else {
                HttpServletRequest httpRequest = (HttpServletRequest) PolicyContext
                        .getContext("javax.servlet.http.HttpServletRequest");
                HttpServletResponse httpResponse = (HttpServletResponse) PolicyContext
                        .getContext("javax.servlet.http.HttpServletResponse");
                SessionUtils.invalidateSessionIfValid(httpRequest, httpRequest.getSession());
                httpResponse.sendRedirect(httpRequest.getContextPath() + logoutRedirect);
            }
        }
        catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassResetUsername() {
        return passResetUsername;
    }

    public void setPassResetUsername(String passResetUsername) {
        this.passResetUsername = passResetUsername;
    }

    /**
     * This method is called by the button in the forgot password modal, and schedules a password reset.
     */
    public String resetPassword() {
        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage("resetPasswordInfo", new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                ValidationPropertiesLoader.getInstance().getValidationProperties().getString(resetPasswordMessage)));
        try {
            //Searches for and obtains the user whose login matches userLogin
            List<SystemUser> searchList = systemUserBp.getSystemUserByLogin(passResetUsername);
            if (!searchList.isEmpty()) {
                SystemUser user = searchList.get(0);
                //TODO: refactor out external context
                ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

                StringBuffer resetUrl = new StringBuffer();
                resetUrl.append(externalContext.getRequestScheme());
                resetUrl.append("://");
                resetUrl.append(externalContext.getRequestServerName());
                if (externalContext.getRequestServerPort() != 80) {
                    resetUrl.append(":");
                    resetUrl.append(externalContext.getRequestServerPort());
                }
                resetUrl.append(externalContext.getRequestContextPath());
                resetUrl.append(PW_RESET_PATH);

                String bodyMsg = "Click Link to reset Password: \n" + resetUrl.toString();
                passwordResetEntryBp.scheduleReset(user, bodyMsg);
            }

        }
        catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("Error resetting password", e);
            }
            //If some other error occurs
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected error",
                    "An unexpected error occurred."));
        }
        clearForm();

        return LinksBean.PASSWORD_RESET_INFO_PAGE;
    }

    @Lock(LockType.WRITE)
    public String logoutAndGoToLoginPage() {
        logout();
        return HOMEPAGE;

    }

    @Lock(LockType.WRITE)
    //TODO: pull JSFUtils in Service Project and fix ClearForm issue similar to Admin login
    public void clearForm() {
        setPassResetUsername("");
        JSFUtils.clearForm("passwordReset_form");
    }

    /**
     * Returns the URL to the "register new user" page
     * 
     * @return
     */

    public String getRegisterUrl() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getRequestContextPath() + DEFAULT_CREATE_USER_PAGE;
    }

    public String getNovantRegistrationInfoPage() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getRequestContextPath() + NOVANT_REGISTRATION_INFO_PAGE;
    }

    /**
     * Returns the appropriate URL to the "register new user" page, depending on the subdomain.
     * 
     * @return
     */
    public String getCreateUserUrl() {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        logger.debug("Create User URL Subdomain: " + this.validSubdomains);

        //Novant, Walmart, and CHS use the "Custom" registration page, which has several conditionally rendered components
        if (this.getIsNovantSubdomain() || this.getIsWalmartSubdomain() || this.getIsCHSSubdomain()) {
            return context.getRequestContextPath() + CUSTOM_CREATE_USER_PAGE;
        }
        //otherwise, checks the subdomain, and uses the appropriate registration page
        else if (this.getIsCintasSubdomain()) {
            return context.getRequestContextPath() + CINTAS_CREATE_USER_PAGE;
        }
        //if no registration pages are specified for the subdomains, uses the default
        return context.getRequestContextPath() + DEFAULT_CREATE_USER_PAGE;

    }

    public String getChangeUserUrl() {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        logger.debug("Change User URL Subdomain: " + this.validSubdomains);

        return context.getRequestContextPath() + CUSTOM_CHANGE_USER_PAGE;

        /*if (this.validSubdomains.contains(CHS_SUBDOMAIN_NAME) || this.validSubdomains.contains(WALMART_SUBDOMAIN_NAME)) {
            return context.getRequestContextPath() + CUSTOM_CHANGE_USER_PAGE;
        }
        else {
            return context.getRequestContextPath() + DEFAULT_CREATE_USER_PAGE;
        }*/

    }

    public String getChangeLocationUrl() {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        logger.debug("Change Location URL subdomain: " + this.validSubdomains);

        return context.getRequestContextPath() + CUSTOM_CHANGE_LOCATION_PAGE;
    }

    public String getAddLocationUrl() {

        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        logger.debug("Add Location URL subdomain: " + this.validSubdomains);

        return context.getRequestContextPath() + CUSTOM_ADD_LOCATION_PAGE;
    }

    public boolean isRegistrationLinkHidden(String subdomain) {
        if (hideUserRegistrationLink == null) {
            hideUserRegistrationLink = accountBp.isRegistrationLinkHidden(subdomain);
        }

        return hideUserRegistrationLink.booleanValue();

    }

    public Set<String> getValidSubdomains() {
        return this.validSubdomains;
    }

    public Boolean getIsCHSSubdomain() {
        return this.subdomainMatches(CHS_SUBDOMAIN_NAME);
    }

    public Boolean getIsWalmartSubdomain() {
        return this.subdomainMatches(WALMART_SUBDOMAIN_NAME);
    }

    public Boolean getIsEbizSubdomain() {
        return this.subdomainMatches(EBIZ_SUBDOMAIN_NAME);
    }

    public Boolean getIsFemaSubdomain() {
        return this.subdomainMatches(FEMA_SUBDOMAIN_NAME);
    }

    public Boolean getIsTemaSubdomain() {
        return this.subdomainMatches(TEMA_SUBDOMAIN_NAME);
    }

    public Boolean getIsNovantSubdomain() {
        return this.subdomainMatches(NOVANT_SUBDOMAIN_NAME);
    }

    public Boolean getIsCintasSubdomain() {
        return this.subdomainMatches(CINTAS_SUBDOMAIN_NAME);
    }

    public Boolean getIsMarfieldSubdomain() {
        return this.subdomainMatches(MARFIELD_SUBDOMAIN_NAME);
    }

    public Boolean isDefaultSubdomain() {
        return !getIsCHSSubdomain() && !getIsCintasSubdomain() && !getIsNovantSubdomain() && !getIsWalmartSubdomain()
                && !getIsFemaSubdomain() && !getIsEbizSubdomain() && !getIsTemaSubdomain();
    }

    public Boolean getRequiresUserMaintenance() {
        return this.getIsCHSSubdomain() || this.getIsWalmartSubdomain();
    }

    private boolean subdomainMatches(String subdomainToMatch) {

        for (String subdomain : validSubdomains) {
            if (subdomain.equalsIgnoreCase(subdomainToMatch)) {
                return true;
            }
        }
        return false;
    }

    public Set<String> getValidIpAddresses() {
        return validIpAddresses;
    }

    public String getPunchoutSessionToken() {
        return punchoutSessionToken;
    }

    public void setPunchoutSessionToken(String punchoutSessionToken) {
        if (StringUtils.isBlank(this.punchoutSessionToken)
                || (this.credentialSelectionBean.getPunchoutSession() == null && this.credentialSelectionBean
                        .getPunchoutBuyerCookie() == null)) {
            this.punchoutSessionToken = punchoutSessionToken;
        }
        else if (!StringUtils.isBlank(punchoutSessionToken) && !this.punchoutSessionToken.equals(punchoutSessionToken)) {
            logger.warn("Not setting existing session token " + this.punchoutSessionToken + " to "
                    + punchoutSessionToken);
        }
    }

    // Use CredentialSelectionBean.getPunchoutSession() where punchout session(s) are now exclusively maintained.
    @Deprecated
    public PunchoutSession getPunchoutSession() {
        return credentialSelectionBean.getPunchoutSession();
    }

    // Use CredentialSelectionBean.setPunchoutSession() where punchout session(s) are now exclusively maintained.
    @Deprecated
    public void setPunchoutSession(PunchoutSession punchoutSession) {
        this.credentialSelectionBean.setPunchoutSession(punchoutSession);
    }

    @Lock(LockType.WRITE)
    public void validatePunchoutSessionToken() {
        if (punchoutSessionToken == null) {
            return;
        }
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            HttpServletRequest request = (HttpServletRequest) context.getRequest();
            Principal userPrincipal = request.getUserPrincipal();
            if (userPrincipal != null && !punchoutSessionToken.equals(userPrincipal.getName())) {
                logger.error("Token: "
                        + punchoutSessionToken
                        + " does not equal "
                        + (StringUtils.isEmpty(userPrincipal.getName()) ? " null principal name " : userPrincipal
                                .getName()) + ". Invalidating session.");
                SessionUtils.invalidateSessionIfValid(request, request.getSession());
                request.logout();
                try {
                    Properties ecommerceProperties = PropertiesLoader.getAsProperties("ecommerce");
                    String redirectUrl = ecommerceProperties.getProperty("punchoutBaseLoginUrl") + punchoutSessionToken;
                    context.redirect(redirectUrl);
                }
                catch (IOException e) {
                    logger.error(e.getMessage());

                }
                return;
            }
            if (userPrincipal == null) {
                request.login(punchoutSessionToken, punchoutSessionToken);
                HttpSession httpSession = (HttpSession) context.getSession(true);
                httpSession.setAttribute(PUNCHOUT_SESSION_ATTRIBUTE, punchoutSessionToken);
            }
            credentialSelectionBean.loadPunchoutSession(punchoutSessionToken);

            if (credentialSelectionBean.getPunchoutSession().getOperation().equals(PunchoutOrderOperation.edit)) {
                shoppingCartBean.clearCart();
                shoppingCartBean.addOrderItemsToCart(credentialSelectionBean.getPunchoutSession().getCustomerOrder());
            }
            credentialSelectionBean.redirectToCredentialSelection(request, (HttpServletResponse) context.getResponse());
        }
        catch (ServletException ex) {
            logger.error("Error validating punchout session.", ex);
            FacesContext.getCurrentInstance()
                    .addMessage(
                            "loginContainerMain",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error validating punchout session.", ex
                                    .getMessage()));
        }
    }

    //    private void initPunchoutSession() {
    //        PunchoutSession session = null;
    //        try {
    //            session = new PunchoutSession();
    //            session.setSessionToken(punchoutSessionToken);
    //            List<PunchoutSession> searchResultList = punchoutSessionBp.searchByExactExample(session, 0, 0);
    //            session = searchResultList.get(0);
    //        }
    //        catch (Exception ex) {
    //            //do nothing
    //        }
    //        if (session != null) {
    //            punchoutSession = session;
    //        }
    //    }

    public boolean isPunchout() {
        return credentialSelectionBean.isPunchoutSession();
    }

    public boolean isDoublePunchout() {
        return (isPunchout() && StringUtils.isNotEmpty(credentialSelectionBean.getCurrentCredential()
                .getVendorPunchoutUrl()));
    }

    public Boolean getRequiresDirectAccess() {
        return this.subdomainMatches(EBIZ_SUBDOMAIN_NAME) || this.subdomainMatches(FEMA_SUBDOMAIN_NAME);
    }

    public String getHomePage() {
        return HOMEPAGE;
    }

    public boolean validEmailAddress(String emailAddress, String subdomain) {
        return accountBp.validEmailAddress(emailAddress, subdomain);
    }

    public Boolean isHideUserRegistrationLink() {
        return hideUserRegistrationLink;
    }

    public void setHideUserRegistrationLink(Boolean hideUserRegistrationLink) {
        this.hideUserRegistrationLink = hideUserRegistrationLink;
    }

    public boolean isResetPasswordLinkHidden(String subdomain) {
        if (hideUserResetPasswordLink == null) {
            hideUserResetPasswordLink = accountBp.isResetPasswordLinkHidden(subdomain);
        }

        return hideUserResetPasswordLink.booleanValue();

    }

    public Boolean getIsCincinnatiSubdomain() {
        return this.subdomainMatches(CINCINNATI_SUBDOMAIN_NAME);
    }

    public Boolean getIsApdTenant() {
        return SingletonPropertiesLoader.DEFAULT_TENANT.equals(SingletonPropertiesLoader.getTenant());
    }

}
