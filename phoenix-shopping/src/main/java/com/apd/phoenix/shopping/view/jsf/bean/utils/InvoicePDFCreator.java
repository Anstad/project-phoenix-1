package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.persistence.jpa.PaymentInformationDao;

@Named
@RequestScoped
public class InvoicePDFCreator implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = LoggerFactory.getLogger(InvoicePDFCreator.class);
    private static final float TOP_MARGIN = -25;
    private static final float LEFT_MARGIN = 25;
    private static final float RIGHT_MARGIN = -25;
    private static final float BOTTOM_MARGIN = 25;
    private static final float FONT_SIZE = 8;
    private static final float LINE_SPACING = 3;

    private static final int QTY_DIVISION = 0;
    private static final int SKU_DIVISION = 2;
    private static final int DESCRIPTION_DIVISION = 6;
    private static final int STATUS_DIVISION = 20;
    private static final int PRICE_PER_DIVISION = 27;
    private static final int TOTAL_DIVISION = 30;

    private float trX;
    private float trY;

    //Used for dividing page into grid for formatting convenience
    private float horizontalDivision;
    private float verticalDivision;

    private PDDocument document;
    private PDPage page;
    private PDPageContentStream contentStream;
    private CustomerOrder co;

    @Inject
    PaymentInformationDao paymentInformationDao;

    public InvoicePDFCreator() {
        super();

        try {
            // Create a document and add a page to it
            document = new PDDocument();
            page = new PDPage();
            document.addPage(page);
            trX = page.getMediaBox().getUpperRightX();
            trY = page.getMediaBox().getUpperRightY();
            horizontalDivision = (trX - LEFT_MARGIN + RIGHT_MARGIN) / 32;
            verticalDivision = (trY - BOTTOM_MARGIN + TOP_MARGIN) / 32;
            // Start a new content stream which will "hold" the to be created content
            contentStream = new PDPageContentStream(document, page);
            co = new CustomerOrder();
        }
        catch (IOException e) {
            LOGGER.error("An error occured:", e);
            try {
                contentStream.close();
            }
            catch (IOException e1) {
                LOGGER.error("Could not close the content stream", e);
            }
            try {
                document.close();
            }
            catch (IOException e1) {
                LOGGER.error("Could not close the document", e);
            }

        }

    }

    public void downloadInvoice(CustomerOrder co) throws NoInvoiceException {

        try {
            this.co = co;
            createHeader();
            createBillingInformation();
            createShipToAddress();
            createOrderNumberInfo();
            createItemList();

            // Make sure that the content stream is closed:
            contentStream.close();

            ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();

            // Save the results and ensure that the document is properly closed:
            document.save(pdfOutputStream);

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            // Header
            response.setHeader("Expires", "0");
            response.setContentType("application/pdf");
            response.setContentLength(pdfOutputStream.size());

            // Write the PDF
            ServletOutputStream responseOutputStream = response.getOutputStream();
            responseOutputStream.write(pdfOutputStream.toByteArray());
            responseOutputStream.flush();
            responseOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();
            try {
                document.close();
            }
            catch (IOException e) {
                LOGGER.error("An error occured:", e);
            }

        }
        catch (Exception e) {
            try {
                contentStream.close();
            }
            catch (IOException e1) {
                LOGGER.error("An error occured:", e1);
            }
            LOGGER.error("An error occured:", e);
            try {
                document.close();
            }
            catch (IOException e2) {
                LOGGER.error("An error occured:", e2);
            }
            throw new NoInvoiceException();
        }

    }

    private void createHeader() throws IOException {
        // Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        String currentLine = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.header.address.name");
        contentStream.moveTextPositionByAmount(
                (page.getMediaBox().getWidth() - (font.getStringWidth(currentLine) / 1000 * FONT_SIZE)) / 2, this.trY
                        + TOP_MARGIN);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.header.address.name"));
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.header.address.line1"));
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.header.address.line2"));

        contentStream.endText();
    }

    private void createOrderNumberInfo() throws IOException {
        float x = LEFT_MARGIN;
        float y = this.trY + TOP_MARGIN - (10 * verticalDivision);

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(x, y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.ordernumber.label"));
        contentStream.moveTextPositionByAmount(calculateWidth(EcommercePropertiesLoader.getInstance()
                .getEcommerceProperties().getString("invoice.ordernumber.label")
                + " ", font), 0);
        if (co.getApdPo() != null) {
            contentStream.drawString(co.getApdPo().getValue());
        }
        contentStream.moveTextPositionByAmount(-calculateWidth(EcommercePropertiesLoader.getInstance()
                .getEcommerceProperties().getString("invoice.ordernumber.label")
                + " ", font), -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.quotedate.label"));
        contentStream.moveTextPositionByAmount(calculateWidth(EcommercePropertiesLoader.getInstance()
                .getEcommerceProperties().getString("invoice.quotedate.label")
                + " ", font), 0);
        if (co.getOrderDate() != null) {
            contentStream.drawString(co.getOrderDate().toString());
        }

        contentStream.endText();
    }

    private void createItemList() throws IOException {
        float y = yDiv(12);

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(xDiv(QTY_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.qtyquoted"));//quantity
        contentStream.endText();
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(SKU_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.sku"));//customer sku
        contentStream.endText();
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(DESCRIPTION_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.description"));//description
        contentStream.endText();
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(STATUS_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.status"));//status
        contentStream.endText();
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(PRICE_PER_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.priceper"));//price per
        contentStream.endText();
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(TOTAL_DIVISION), y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.header.total"));//total
        contentStream.endText();

        int row = 0;
        for (LineItem li : co.getItems()) {
            int wordWrapAdjust = createItemListRow(li, row);
            row = row + wordWrapAdjust + 1;
        }

        font = PDType1Font.HELVETICA_BOLD;
        contentStream.setFont(font, FONT_SIZE);

        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(20), yDiv(13) - ((row + 2) * (FONT_SIZE + LINE_SPACING)));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.totals.totalmerchandise"));

        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.totals.estshippinghandling"));

        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.totals.tax"));

        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.itemlist.totals.total"));
        contentStream.endText();

        contentStream.beginText();
        contentStream.moveTextPositionByAmount(xDiv(30), yDiv(13) - ((row + 2) * (FONT_SIZE + LINE_SPACING)));
        if (co.getSubtotal() != null) {
            contentStream.drawString("$" + co.getSubtotal().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        if (co.getEstimatedShippingAmount() != null) {
            contentStream.drawString("$"
                    + co.getEstimatedShippingAmount().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        if (co.getMaximumTaxToCharge() != null) {
            contentStream.drawString("$"
                    + co.getMaximumTaxToCharge().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        if (co.getOrderTotal() != null) {
            contentStream.drawString("$" + co.getOrderTotal().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        }
        contentStream.endText();
    }

    private int createItemListRow(LineItem li, int row) throws IOException {
        float yRowAdjusted = yDiv(13) - (row * (FONT_SIZE + LINE_SPACING));

        PDFont font = PDType1Font.HELVETICA;

        contentStream.setFont(font, FONT_SIZE);

        if (li.getQuantity() != null) {
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(xDiv(QTY_DIVISION), yRowAdjusted);
            contentStream.drawString(li.getQuantity().toString()); //quantity
            contentStream.endText();
        }
        if (li.getCustomerSku() != null) {
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(xDiv(SKU_DIVISION), yRowAdjusted);
            contentStream.drawString(li.getCustomerSku().getValue()); //customer sku
            contentStream.endText();
        }

        StringBuilder description = new StringBuilder();
        if (li.getShortName() != null) {
            description.append(li.getShortName());
        }
        if (li.getManufacturerName() != null) {
            description.append(" " + li.getManufacturerName());
        }
        if (li.getApdSku() != null) {
            description.append(" " + li.getApdSku()); //description
        }

        int wordWrapAdjust = wordWrap(description.toString(), (xDiv(STATUS_DIVISION) - xDiv(DESCRIPTION_DIVISION)),
                font, xDiv(DESCRIPTION_DIVISION), yRowAdjusted);

        if (li.getStatus() != null) {
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(xDiv(STATUS_DIVISION), yRowAdjusted);
            contentStream.drawString(li.getStatus().getValue()); //status
            contentStream.endText();
        }
        if (li.getUnitPrice() != null) {
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(xDiv(PRICE_PER_DIVISION), yRowAdjusted);
            contentStream.drawString("$" + li.getUnitPrice().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString()); //price per
            contentStream.endText();
        }
        if (li.getTotalPrice() != null) {
            contentStream.beginText();
            contentStream.moveTextPositionByAmount(xDiv(TOTAL_DIVISION), yRowAdjusted);
            contentStream.drawString("$" + li.getTotalPrice().setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString()); //total
            contentStream.endText();
        }

        return wordWrapAdjust;
    }

    private void createShipToAddress() throws IOException {
        float x = (LEFT_MARGIN + (16 * horizontalDivision));
        float y = this.trY + TOP_MARGIN - (3 * verticalDivision);

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(x, y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.shipto.label"));
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        if (co.getUser() != null && co.getUser().getPerson() != null) {
            contentStream.drawString(co.getUser().getPerson().getFormalName());
        }
        if (co.getAddress() != null) {
            if (co.getAddress().getLine1() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getLine1());
            }
            if (co.getAddress().getLine2() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getLine2());
            }
            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString(co.getAddress().getCity() + ", " + co.getAddress().getState() + " "
                    + co.getAddress().getZip());
            if (co.getAddress().getCountry() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getCountry());
            }
            for (AddressXAddressPropertyType field : co.getAddress().getFields()) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(field.getType().getName() + ": ");
                contentStream.moveTextPositionByAmount(calculateWidth(field.getType().getName() + ": ", font), 0);
                contentStream.drawString(field.getValue());
                contentStream.moveTextPositionByAmount(-calculateWidth(field.getType().getName() + ": ", font), 0);
            }

        }
        contentStream.endText();
    }

    private void createBillingInformation() throws IOException {
        float x = LEFT_MARGIN;
        float y = this.trY + TOP_MARGIN - (3 * verticalDivision);

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(x, y);
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.billinginformation.label"));

        if (co.getPaymentInformation() != null) {
            PaymentInformation paymentInfo = paymentInformationDao.eagerLoad(co.getPaymentInformation());
            Address billingAddress = paymentInfo.getBillingAddress();

            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString(paymentInfo.getContact().getFormalName());

            if (billingAddress.getLine1() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(billingAddress.getLine1());
            }
            if (billingAddress.getLine2() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(billingAddress.getLine2());
            }
            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString(billingAddress.getCity() + ", " + billingAddress.getState() + " "
                    + billingAddress.getZip());
            if (billingAddress.getCountry() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(billingAddress.getCountry());
            }
            for (AddressXAddressPropertyType field : billingAddress.getFields()) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(field.getType().getName() + ": ");
                contentStream.moveTextPositionByAmount(calculateWidth(field.getType().getName() + ": ", font), 0);
                contentStream.drawString(field.getValue());
                contentStream.moveTextPositionByAmount(-calculateWidth(field.getType().getName() + ": ", font), 0);
            }
        }

        contentStream.moveTextPositionByAmount(0, -2 * (FONT_SIZE + LINE_SPACING));
        contentStream.drawString(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "invoice.billinginformation.costcenter.label"));

        contentStream.moveTextPositionByAmount(calculateWidth(EcommercePropertiesLoader.getInstance()
                .getEcommerceProperties().getString("invoice.billinginformation.costcenter.label")
                + " ", font), 0);
        if (co.getAssignedCostCenter() != null && co.getAssignedCostCenter().getCostCenter() != null
                && co.getAssignedCostCenter().getCostCenter().getNumber() != null) {
            contentStream.drawString(co.getAssignedCostCenter().getCostCenter().getNumber());
        }
        contentStream.endText();
    }

    private float calculateWidth(String string, PDFont font) throws IOException {
        return font.getStringWidth(string) / 1000 * FONT_SIZE;
    }

    private float xDiv(int x) {
        return (LEFT_MARGIN + (x * horizontalDivision));
    }

    private float yDiv(int y) {
        return this.trY + TOP_MARGIN - (y * verticalDivision);
    }

    private int wordWrap(String textToWrap, float paragraphWidth, PDFont font, float startX, float startY)
            throws IOException {
        contentStream.beginText();
        contentStream.moveTextPositionByAmount(startX, startY);

        int start = 0;
        int end = 0;

        int wraps = 0;
        for (int i : possibleWrapPoints(textToWrap)) {
            float width = calculateWidth(textToWrap.substring(start, i), font);
            if (start < end && width > paragraphWidth) {
                contentStream.drawString(textToWrap.substring(start, end));
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                start = end;
                wraps++;
            }

            end = i;
        }
        contentStream.drawString(textToWrap.substring(start));
        contentStream.endText();
        return wraps;
    }

    private int[] possibleWrapPoints(String text) {
        String[] split = text.split("(?<=\\W)");
        int[] ret = new int[split.length];
        ret[0] = split[0].length();
        for (int i = 1; i < split.length; i++) {
            ret[i] = ret[i - 1] + split[i].length();
        }
        return ret;
    }

    public class NoInvoiceException extends Exception {

    }

}