package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.apd.phoenix.shopping.view.jsf.bean.search.Product;

@Named
@Stateful
@RequestScoped
public class ProductCompareSelectionBean {
	private Map<Product, Boolean> compareMap = new HashMap<>();

	public Map<Product, Boolean> getCompareMap() {
		return compareMap;
	}

	public void setCompareMap(Map<Product, Boolean> compareMap) {
		this.compareMap = compareMap;
	}
	
	public List<Product> getProductsToCompare() {
		List<Product> toReturn = new ArrayList<>();
		for (Product product : compareMap.keySet()) {
			if (compareMap.get(product)) {
				toReturn.add(product);
			}
		}
		return toReturn;
	}
}
