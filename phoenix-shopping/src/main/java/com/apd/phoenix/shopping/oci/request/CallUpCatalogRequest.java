package com.apd.phoenix.shopping.oci.request;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.shopping.oci.OCIRequestWrapper;

public class CallUpCatalogRequest extends OCIRequestWrapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallUpCatalogRequest.class);

    public CallUpCatalogRequest(HttpServletRequest request, HttpServletResponse response) {
        super(request, response);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && StringUtils.isNotBlank(getParameter(REQUEST_PARAM_USERNAME))
                && StringUtils.isNotBlank(getParameter(REQUEST_PARAM_PASSWORD))
                && StringUtils.isNotBlank(getParameter(REQUEST_PARAM_RETURN_URL))
                && StringUtils.isBlank(getParameter(REQUEST_PARAM_FUNCTION));
    }

    @Override
    public void authenticate() throws ServletException {
        try {
            super.authenticate(getResponse());
        }
        catch (IOException e) {
            LOGGER.error("An error occured:", e);
        }
    }

}
