/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.user;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.SystemUser;

@Named
@Stateful
@ConversationScoped
public class UserRegistrationBean {

    private SystemUser user;

    @Inject
    private SystemUserBp systemUserBp;

    public void beginUserRegistration() {
        user.setLogin("New User");
        systemUserBp.beginUserRegistration(user);
    }

    public boolean isUsernameAvailable(String username) {
        return systemUserBp.isLoginAvailable(username);
    }

}
