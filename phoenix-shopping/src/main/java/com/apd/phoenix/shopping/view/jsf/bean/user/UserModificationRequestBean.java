package com.apd.phoenix.shopping.view.jsf.bean.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.model.dto.DepartmentRequestDto;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@Named
@Stateful
@SessionScoped
public class UserModificationRequestBean {

    private static final Logger LOG = LoggerFactory.getLogger(UserModificationRequestBean.class);

    private static final String csrEmail = "";
    private static final String REDIRECT_THANKYOU_PAGE = "/ecommerce/user/thankYouUserRegistration?faces-redirect=true";

    @Inject
    WorkflowService workflowService;

    @Inject
    UserRequestBp userRequestBp;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    private LoginBean loginBean;

    private RequestType requestType;
    private String accountId;
    private String reason;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String ext;

    private String locationAction;

    private String address1Old;
    private String address2Old;
    private String cityOld;
    private String stateOld;
    private String zipOld;
    private String mailstopOld;
    private String poleOld;

    private String address1New;
    private String address2New;
    private String cityNew;
    private String stateNew;
    private String zipNew;
    private String mailstopNew;
    private String poleNew;

    private List<DepartmentRequestDto> departments;

    private String divDeptNew;
    private String deptManagerNameNew;
    private String deptManagerEmailNew;
    private String deptActionNew;

    private String requesterName;
    private String requesterEmail;
    private String requesterPhone;
    private String requesterManagerEmail;

    private List<String> emailList;

    @PostConstruct
    public void init() {
        requestType = RequestType.MODIFY;

        departments = new ArrayList<>();
        emailList = new ArrayList<>();

    }

    public void addDepartment() {
        DepartmentRequestDto department = new DepartmentRequestDto();
        department.setDivDept(divDeptNew);
        department.setManagerName(deptManagerNameNew);
        department.setManagerEmail(deptManagerEmailNew);
        department.setDeptAction(deptActionNew);
        if (divDeptNew != null && !divDeptNew.isEmpty() && deptManagerNameNew != null && !deptManagerNameNew.isEmpty()
                && deptManagerEmailNew != null && !deptManagerEmailNew.isEmpty() && deptActionNew != null
                && !deptActionNew.isEmpty()) {
            departments.add(department);
        }
        divDeptNew = "";
        deptManagerNameNew = "";
        deptManagerEmailNew = "";
        deptActionNew = "";
    }

    public void removeDepartment(int index) {
        departments.remove(index);
    }

    public String submit() {
        LOG.info("Submitting user modification request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department
            addDepartment();
            UserModificationRequestDto umr = new UserModificationRequestDto();
            
            umr.setValidAccountId(true);
            if(systemUserBp.isLoginAvailable(accountId)) {
            	LOG.info("Invalid Account Name");
            	umr.setValidAccountId(false);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Invalid account name"));
                return null;
            }
            umr.setRequestType(requestType.getBusinessKey());
            umr.setAccountId(accountId);
            umr.setReason(reason);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);

            umr.setLocationAction(locationAction);
           
            umr.setAddress1Old(address1Old);
            umr.setAddress2Old(address2Old);
            umr.setCityOld(cityOld);
            umr.setStateOld(stateOld);
            umr.setZipOld(zipOld);
            umr.setPoleOld(poleOld);
            umr.setMailstopOld(mailstopOld);

            umr.setAddress1New(address1New);
            umr.setAddress2New(address2New);
            umr.setCityNew(cityNew);
            umr.setStateNew(stateNew);
            umr.setZipNew(zipNew);
            umr.setPoleNew(poleNew);
            umr.setMailstopNew(mailstopNew);
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }

            umr.setDepartments(departments);
            umr.setRequesterName(requesterName);
            umr.setRequesterEmail(requesterEmail);
            umr.setRequesterPhone(requesterPhone);
            umr.setRequesterManagerEmail(requesterManagerEmail);
            
            umr.setInternalEmail(csrEmail);
            
            for(String email : requesterManagerEmail.split(",")) {
            	emailList.add(email);
            }
            
            for(DepartmentRequestDto dr : departments) {
            	emailList.add(dr.getManagerEmail());
            }
            
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);
            
            workflowService.startUserRequest(id,params);
            clearAll();
        }
        
        return REDIRECT_THANKYOU_PAGE;
    }

    public void clearAll() {
        requestType = RequestType.MODIFY;
        accountId = "";
        reason = "";
        firstName = "";
        lastName = "";
        email = "";
        phone = "";
        ext = "";
        locationAction = "";
        address1Old = "";
        address2Old = "";
        cityOld = "";
        stateOld = "";
        zipOld = "";
        mailstopOld = "";
        poleOld = "";
        address1New = "";
        address2New = "";
        cityNew = "";
        stateNew = "";
        zipNew = "";
        mailstopNew = "";
        poleNew = "";
        departments.clear();
        divDeptNew = "";
        deptManagerNameNew = "";
        deptManagerEmailNew = "";
        deptActionNew = "";
        requesterName = "";
        requesterEmail = "";
        requesterPhone = "";
        requesterManagerEmail = "";
        emailList.clear();
    }

    public String getLocationAction() {
        return locationAction;
    }

    public void setLocationAction(String locationAction) {
        this.locationAction = locationAction;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDivDeptNew() {
        return divDeptNew;
    }

    public void setDivDeptNew(String divDeptNew) {
        this.divDeptNew = divDeptNew;
    }

    public String getDeptManagerNameNew() {
        return deptManagerNameNew;
    }

    public void setDeptManagerNameNew(String deptManagerNameNew) {
        this.deptManagerNameNew = deptManagerNameNew;
    }

    public String getDeptManagerEmailNew() {
        return deptManagerEmailNew;
    }

    public void setDeptManagerEmailNew(String deptManagerEmailNew) {
        this.deptManagerEmailNew = deptManagerEmailNew;
    }

    public String getDeptActionNew() {
        return deptActionNew;
    }

    public void setDeptActionNew(String deptActionNew) {
        this.deptActionNew = deptActionNew;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getAddress1Old() {
        return address1Old;
    }

    public void setAddress1Old(String address1Old) {
        this.address1Old = address1Old;
    }

    public String getAddress2Old() {
        return address2Old;
    }

    public void setAddress2Old(String address2Old) {
        this.address2Old = address2Old;
    }

    public String getCityOld() {
        return cityOld;
    }

    public void setCityOld(String cityOld) {
        this.cityOld = cityOld;
    }

    public String getStateOld() {
        return stateOld;
    }

    public void setStateOld(String stateOld) {
        this.stateOld = stateOld;
    }

    public String getZipOld() {
        return zipOld;
    }

    public void setZipOld(String zipOld) {
        this.zipOld = zipOld;
    }

    public String getMailstopOld() {
        return mailstopOld;
    }

    public void setMailstopOld(String mailstopOld) {
        this.mailstopOld = mailstopOld;
    }

    public String getPoleOld() {
        return poleOld;
    }

    public void setPoleOld(String poleOld) {
        this.poleOld = poleOld;
    }

    public String getAddress1New() {
        return address1New;
    }

    public void setAddress1New(String address1New) {
        this.address1New = address1New;
    }

    public String getAddress2New() {
        return address2New;
    }

    public void setAddress2New(String address2New) {
        this.address2New = address2New;
    }

    public String getCityNew() {
        return cityNew;
    }

    public void setCityNew(String cityNew) {
        this.cityNew = cityNew;
    }

    public String getStateNew() {
        return stateNew;
    }

    public void setStateNew(String stateNew) {
        this.stateNew = stateNew;
    }

    public String getZipNew() {
        return zipNew;
    }

    public void setZipNew(String zipNew) {
        this.zipNew = zipNew;
    }

    public String getMailstopNew() {
        return mailstopNew;
    }

    public void setMailstopNew(String mailstopNew) {
        this.mailstopNew = mailstopNew;
    }

    public String getPoleNew() {
        return poleNew;
    }

    public void setPoleNew(String poleNew) {
        this.poleNew = poleNew;
    }

    public List<DepartmentRequestDto> getDepartments() {
        return departments;
    }

    public void setDepartments(List<DepartmentRequestDto> departments) {
        this.departments = departments;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

    public String getRequesterPhone() {
        return requesterPhone;
    }

    public void setRequesterPhone(String requesterPhone) {
        this.requesterPhone = requesterPhone;
    }

    public String getRequesterManagerEmail() {
        return requesterManagerEmail;
    }

    public void setRequesterManagerEmail(String requesterManagerEmail) {
        this.requesterManagerEmail = requesterManagerEmail;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public enum RequestType {

        CREATE("create", EmailFactoryBp.USER_REQUEST_CREATE), MODIFY("modify", EmailFactoryBp.USER_REQUEST_MODIFY);

        private String label;
        private String businessKey;

        private RequestType(String label, String businessKey) {
            this.label = label;
            this.businessKey = businessKey;
        }

        public String getLabel() {
            return this.label;
        }

        public String getBusinessKey() {
            return this.businessKey;
        }
    }

    public SelectItem[] getAllRequestTypes() {
        SelectItem[] items = new SelectItem[RequestType.values().length];
        int i = 0;
        for (RequestType g : RequestType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

}
