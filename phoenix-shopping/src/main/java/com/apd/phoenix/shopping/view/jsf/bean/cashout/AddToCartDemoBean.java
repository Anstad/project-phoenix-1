/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.shopping.view.jsf.bean.data.MockDataFactory;
import com.google.gson.Gson;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class AddToCartDemoBean {

    private ItemJsonContainer tempNotification;

    private static final Logger LOGGER = LoggerFactory.getLogger(AddToCartDemoBean.class);

    public String addSelectedItemsToCart() {
        StringBuilder jsonResponse = new StringBuilder();
        jsonResponse.append("[");
        List<Item> items = MockDataFactory.ItemFactory.getItemList(2);

        jsonResponse.append(getJSON(items.get(0), "hardSubstitute"));
        jsonResponse.append(",");
        jsonResponse.append(getJSON(items.get(1), "softSubstitute"));
        jsonResponse.append("]");
        LOGGER.info("Response: ");
        LOGGER.info(jsonResponse.toString());
        return jsonResponse.toString();
    }

    private String getJSON(Item item, String type) {
        ItemJsonContainer obj = new ItemJsonContainer();
        obj.setImgURL("/shopping/resources/img/image_placement/75x75");
        obj.setItemName(item.getName());
        obj.setItemPrice("3.99");
        obj.setItemQty("2");
        obj.setMessageHeader("Item Added Successfully!!!");

        Gson gson = new Gson();

        // convert java object to JSON format,
        // and returned as JSON formatted string
        String json = gson.toJson(obj);

        return json;
    }

    public void addToTempList() {
        //        notificationList.add(tempNotification);
        LOGGER.info("addToTempList Method Called");
    }

    public ItemJsonContainer getTempNotification() {
        return tempNotification;
    }

    public void setTempNotification(ItemJsonContainer tempNotification) {
        this.tempNotification = tempNotification;
    }

}
