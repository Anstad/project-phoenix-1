package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.io.Serializable;
import java.net.InetAddress;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 * This is a utility bean, used to generate URLs for the shopping site.
 * 
 * @author RHC
 *
 */
@Named
@Stateless
public class LinksBean implements Serializable {

    private static final String ECOMMERCE_PRODUCT_LOOKAHEAD_ENDPOINT = "/rest/typeahead/mainSearch/";
    private static final String PUBLIC_RESET_PASSWORD_PAGE = "/ecommerce/user/resetPassword.xhtml";
    private static final String RESET_PASSWORD_PAGE = "/ecommerce/account/resetPassword.xhtml";
    public static final String PASSWORD_EXPIRED_PAGE = "/ecommerce/account/passwordExpired.xhtml";
    private static final String MY_ACCOUNT_PAGE = "/ecommerce/account/manage.xhtml";
    private static final String ECOMMERCE_HOME_PAGE = "/ecommerce/home.xhtml";
    private static final String ECOMMERCE_CASHOUT = "/ecommerce/cashout/checkout.xhtml";
    private static final String ECOMMERCE_PUNCHOUT = "/ecommerce/marketplace/returnCart.xhtml";
    private static final String ECOMMERCE_BROWSE = "/ecommerce/product/browse.xhtml";
    private static final String ECOMMERCE_COMPARE = "/ecommerce/product/compare.xhtml";
    private static final String ECOMMERCE_CART = "/ecommerce/shoppingCart/cart.xhtml";
    private static final String ECOMMERCE_ORDER_HISTORY = "/ecommerce/order/orderHistory.xhtml";
    private static final String ECOMMERCE_USER_FAVORITES = "/ecommerce/product/favorites.xhtml";
    private static final String ECOMMERCE_NEW_PUNCHOUT = "/ecommerce/marketplace/welcome.xhtml";
    //TODO: url
    private static final String ECOMMERCE_COMPANY_FAVORITES = "/ecommerce/product/favorites.xhtml";
    private static final String ECOMMERCE_RETURN_CART = "/ecommerce/marketplace/returnCart.xhtml";
    private static final long serialVersionUID = 1L;
    private static final String REQUEST_ITEM = "/ecommerce/product/requestItem.xhtml";
    private static final String ITEM_NOT_FOUND = "/ecommerce/errors/itemNotFound.xhtml";
    private static final String ECOMMERCE_PUNCHOUT_CONTINUE_SHOPPING = "/ecommerce/marketplace/punchoutWindow.xhtml";
    private static final String ECOMMERCE_PUNCHOUT_EDIT = "/ecommerce/marketplace/punchoutWindow.xhtml";
    public static final String PASSWORD_RESET_INFO_PAGE = "/ecommerce/user/passwordResetInfo.xhtml";
    public static final String PASSWORD_CHANGED_PAGE = "/ecommerce/user/passwordChangedConfirmation.xhtml";

    public String getPasswordChangedPage() {
        return url(PASSWORD_CHANGED_PAGE);
    }

    public String getPasswordResetInfoPage() {
        return url(PASSWORD_RESET_INFO_PAGE);
    }

    public String getResetPassword() {
        return url(RESET_PASSWORD_PAGE);
    }

    public String getPublicResetPassword() {
        return url(PUBLIC_RESET_PASSWORD_PAGE);
    }

    public String getMyAccount() {
        return url(MY_ACCOUNT_PAGE);
    }

    public String getHome() {
        return url(ECOMMERCE_HOME_PAGE);
    }

    public String getCashout() {
        return url(ECOMMERCE_CASHOUT);
    }

    public String getBrowse() {
        return url(ECOMMERCE_BROWSE);
    }

    public String getCompare() {
        return url(ECOMMERCE_COMPARE);
    }

    public String getCart() {
        return url(ECOMMERCE_CART);
    }

    public String getOrderHistory() {
        return url(ECOMMERCE_ORDER_HISTORY);
    }

    public String getUserFavorites() {
        return url(ECOMMERCE_USER_FAVORITES);
    }

    public String getCompanyFavorites() {
        return url(ECOMMERCE_COMPANY_FAVORITES);
    }

    public String getRequestItem() {
        return url(REQUEST_ITEM);
    }

    public String getItemNotFound() {
        return url(ITEM_NOT_FOUND);
    }

    public String getReturnCart() {
        return url(ECOMMERCE_RETURN_CART);
    }

    private String url(String pathEnding) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        StringBuilder toReturn = new StringBuilder();
        toReturn.append(context.getRequestContextPath());
        toReturn.append(pathEnding);
        return toReturn.toString();
    }

    public String getPunchout() {
        return url(ECOMMERCE_PUNCHOUT);
    }

    public String getNewPunchout() {
        return url(ECOMMERCE_NEW_PUNCHOUT);
    }

    public String getPunchoutContinueShopping() {
        return url(ECOMMERCE_PUNCHOUT_CONTINUE_SHOPPING);
    }

    public String getPunchoutEditOrder() {
        return url(ECOMMERCE_PUNCHOUT_EDIT);
    }

    public String getLookahead() {
        return url(ECOMMERCE_PRODUCT_LOOKAHEAD_ENDPOINT);

    }

    public String getHostName() {
        String hostName = "";

        try {
            hostName = InetAddress.getLocalHost().getHostName();
        }
        catch (Exception exp) {

        }
        return hostName;
    }
}
