/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.workflow;

import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.workflow.UserTaskService;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class HumanTaskFormBean {

    protected static final String TO_TASK_SCREEN = "/ecommerce/task/taskList?faces-redirect=true";

    private static final Logger logger = LoggerFactory.getLogger(HumanTaskFormBean.class);

    @Inject
    UserTaskService userTaskService;

    @Inject
    WorkflowTaskBean workflowTaskBean;

    protected long taskId;

    protected Map<String, Object> taskContent;

    @PostConstruct
    public void postConstruct() {
        taskId = workflowTaskBean.getTaskToWork();
        taskContent = userTaskService.getTaskContent(taskId);
        this.initDomainInfo();
    }

    public abstract void initDomainInfo();

    protected String completeTask(Map<String, Object> params, long domainId, CompleteTaskCommand.Type domainIdType) {
        logger.info("Complete task with params:" + params.toString());
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        String user = context.getUserPrincipal().getName();
        userTaskService.suspendTask(taskId, user);
        userTaskService.completeTask(taskId, domainId, domainIdType, user, params);
        return TO_TASK_SCREEN;
    }
}
