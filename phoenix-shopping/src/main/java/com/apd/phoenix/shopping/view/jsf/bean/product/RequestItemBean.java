/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.itemrequest.ItemRequest;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author nreidelb
 */
@Named
@Stateful
@RequestScoped
public class RequestItemBean {

    private static final String FILL_IN_FIELDS_BASE = "Please fill in all required fields. The following required fields are empty: ";
    private static final String FORM_SUBMITTED_PAGE = "/ecommerce/product/requestItem.xhtml?faces-redirect=true&formSubmitted=true";

    private Boolean formSubmitted;

    private String requestor;
    private String email;
    private String partNumber;
    private String description;
    private String howOften;
    private String quantity;
    private String divDept;

    @Inject
    WorkflowService workflowService;

    @Inject
    LinksBean linksBean;

    @Inject
    UserRequestBp userRequestBp;

    /**
     * @return the requestor
     */
    public String getRequestor() {
        return requestor;
    }

    /**
     * @param requestor the requestor to set
     */
    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the partNumber
     */
    public String getPartNumber() {
        return partNumber;
    }

    /**
     * @param partNumber the partNumber to set
     */
    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the howOften
     */
    public String getHowOften() {
        return howOften;
    }

    /**
     * @param howOften the howOften to set
     */
    public void setHowOften(String howOften) {
        this.howOften = howOften;
    }

    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the divDept
     */
    public String getDivDept() {
        return divDept;
    }

    /**
     * @param divDept the divDept to set
     */
    public void setDivDept(String divDept) {
        this.divDept = divDept;
    }

    public Boolean getFormSubmitted() {
        return formSubmitted;
    }

    public void setFormSubmitted(Boolean formSubmitted) {
        this.formSubmitted = formSubmitted;
    }

    public String submit() {
        ItemRequest itemRequest = new ItemRequest(requestor, email, partNumber, description, howOften, quantity,
                divDept);
        UserRequest ur = userRequestBp.createUserRequest();
        String token = ur.getToken();
        Long id = userRequestBp.getIdbyToken(token);
        workflowService.requestItem(id, itemRequest);
        return FORM_SUBMITTED_PAGE;
    }
}
