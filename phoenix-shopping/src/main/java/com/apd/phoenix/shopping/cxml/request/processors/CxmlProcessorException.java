/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.processors;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dnorris
 */
public class CxmlProcessorException extends Exception {

    private static final long serialVersionUID = -3711705910840303497L;
    private final int statusCode;

    private CxmlProcessorException(int statusCode) {
        super();
        this.statusCode = statusCode;
    }

    private CxmlProcessorException(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public static class Builder {

        private int statusCode;
        private String message;

        public Builder statusCode(int statusCode) {
            this.statusCode = statusCode;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public CxmlProcessorException build() {
            if (StringUtils.isEmpty(this.message)) {
                return new CxmlProcessorException(this.statusCode);
            }
            else {
                return new CxmlProcessorException(this.statusCode, this.message);
            }
        }
    }
}
