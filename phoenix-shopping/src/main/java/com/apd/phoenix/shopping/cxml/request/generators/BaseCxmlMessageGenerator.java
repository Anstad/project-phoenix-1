/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.generators;

import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.shopping.cxml.BaseCxmlProcessor;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 *
 * @author dnorris
 */
public class BaseCxmlMessageGenerator extends BaseCxmlProcessor {

    protected static final String CXML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
            + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd\">";
    protected static final String ENGLISH = "en-US";
    protected static final PropertiesConfiguration properties = EcommercePropertiesLoader.getInstance()
            .getEcommerceProperties();

}
