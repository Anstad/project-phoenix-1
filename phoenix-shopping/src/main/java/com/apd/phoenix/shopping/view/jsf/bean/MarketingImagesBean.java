package com.apd.phoenix.shopping.view.jsf.bean;

import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.integration.cxml.model.catalog.Credential;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author anicholson
 */
@Named
public class MarketingImagesBean {

    private static final String STATIC_CONTENT_URL = PropertiesLoader.getAsProperties("aws.integration").getProperty(
            "content.service.secure.domain");
    private static final String IMAGE_DIRECTORY = "/images/marketing/";
    private static final String IMAGE_DIRECTORY_URL = STATIC_CONTENT_URL + IMAGE_DIRECTORY;

    private static final String PROPERTIES_PREFIX = "image.name.";

    @Inject
    CredentialSelectionBean credentialSelectionBean;

    public String getBannerUrl() {
        if (credentialSelectionBean.getCurrentCredential() != null
                && credentialSelectionBean.getCurrentCredential().getProperties() != null) {
            String bannerName = findBannerName(credentialSelectionBean.getCurrentCredential());
            if (StringUtils.isNotEmpty(bannerName)) {
                return IMAGE_DIRECTORY_URL + StringEscape.escapeForUrl(bannerName);
            }
        }
        return getImageUrl("banner");
    }

    private String findBannerName(com.apd.phoenix.service.model.Credential currentCredential) {
        for (CredentialXCredentialPropertyType credentialPropety : currentCredential.getProperties()) {
            if (credentialPropety.getType() != null
                    && CredentialPropertyTypeEnum.BANNER_NAME.getValue().equals(credentialPropety.getType().getName())) {
                return credentialPropety.getValue();
            }
        }
        return null;
    }

    public static String getImageUrl(String location) {
        String imageName = PropertiesLoader.getAsProperties("themeMap.properties").getProperty(
                PROPERTIES_PREFIX + location);
        String url = IMAGE_DIRECTORY_URL + StringEscape.escapeForUrl(imageName);
        return url;
    }

}
