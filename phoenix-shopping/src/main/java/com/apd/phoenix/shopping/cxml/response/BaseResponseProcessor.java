package com.apd.phoenix.shopping.cxml.response;

import com.apd.phoenix.shopping.cxml.BaseCxmlProcessor;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author dnorris
 * @param <T>
 * @param <U>
 */
public abstract class BaseResponseProcessor<T, U> extends BaseCxmlProcessor {

    public abstract T createBaseResponse();

    public abstract void initializeResponse(HttpServletResponse response, U cxmlResponse);

    public abstract U createInvalidResponse();

    public abstract U createOkResponse();

    public abstract U createInternalErrorResponse();

    public abstract U createNotImplementedResponse();

}
