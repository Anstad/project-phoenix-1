package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.apd.phoenix.core.StringEscape;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Presentation layer wrapper for storing product data pulled from solr results
 * or db directly
 *
 * @author dcnorris
 */
public class Product {

    private static final Logger logger = LoggerFactory.getLogger(Product.class);

    private Long id;
    private String name;
    private String description;
    private String customerSku;
    private String apdSku;
    private String manufacturerSku;
    private String vendorSku;
    private String vendorName;
    private Map<String, String> skuMap = new HashMap<>();
    private String manufacturerName;
    private String mainImageUrl;
    private Set<String> imageUrls = new HashSet<>();
    private Map<String, String> iconMap = new HashMap<>();
    private Map<String, String> properties = new HashMap<>();
    private BigDecimal price;
    private String units;
    private Date coreItemStartDate;
    private Date coreItemExpirationDate;
    static final String DETAILS_PAGE_BASE_URL = "/shopping/ecommerce/product/productDetails.xhtml";
    private String status;
    private Integer multiple;
    private Integer minimum;
    private String customerReplacementSku;
    private String customerReplacementVendor;
    private String specialOrder;
    private String customOrder;
    private static final String URL_ENCODING = "UTF-8";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Map<String, String> getSkuMap() {
        return skuMap;
    }

    public void setSkuMap(HashMap<String, String> skuMap) {
        this.skuMap = skuMap;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getMainImageUrl() {
        return mainImageUrl;
    }

    public void setMainImageUrl(String mainImageUrl) {
        this.mainImageUrl = mainImageUrl;
    }

    public Set<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(Set<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public String getCustomerSku() {
        return customerSku;
    }

    public void setCustomerSku(String customerSku) {
        this.customerSku = customerSku;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getManufacturerSku() {
        return manufacturerSku;
    }

    public void setManufacturerSku(String manufacturerSku) {
        this.manufacturerSku = manufacturerSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public Map<String, String> getIconMap() {
        return iconMap;
    }

    public void setIconMap(Map<String, String> iconMap) {
        this.iconMap = iconMap;
    }

    public Date getCoreItemStartDate() {
        return coreItemStartDate;
    }

    public void setCoreItemStartDate(Date coreItemStartDate) {
        this.coreItemStartDate = coreItemStartDate;
    }

    public Date getCoreItemExpirationDate() {
        return coreItemExpirationDate;
    }

    public void setCoreItemExpirationDate(Date coreItemExpirationDate) {
        this.coreItemExpirationDate = coreItemExpirationDate;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDetailsLink() {
        StringBuilder url = new StringBuilder(DETAILS_PAGE_BASE_URL);
        if (StringUtils.isBlank(apdSku) || StringUtils.isBlank(vendorName)) {
        	return url.toString();
        }
        url.append("?");
        url.append("apdSku=");
        url.append(StringEscape.escapeForUrl(apdSku));
        url.append("&");
        url.append("vendorName=");
        url.append(StringEscape.escapeForUrl(vendorName));
        return url.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (that instanceof Product) {
            that = (Product) that;
            if (this.getId() != null) {
                return this.getId().equals(((Product) that).getId());
            }
            return super.equals(that);
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public boolean isCoreItem() {
        if (coreItemStartDate == null || coreItemExpirationDate == null) {
            return false;
        }
        Date date = new Date();
        if (coreItemStartDate.before(date) && coreItemExpirationDate.after(date)) {
            return true;
        }
        return false;
    }

    public static String getDETAILS_PAGE_BASE_URL() {
        return DETAILS_PAGE_BASE_URL;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public String getCustomerReplacementSku() {
        return customerReplacementSku;
    }

    public void setCustomerReplacementSku(String customerReplacementSku) {
        this.customerReplacementSku = customerReplacementSku;
    }

    public String getCustomerReplacementVendor() {
        return customerReplacementVendor;
    }

    public void setCustomerReplacementVendor(String customerReplacementVendor) {
        this.customerReplacementVendor = customerReplacementVendor;
    }

    public String getSpecialOrder() {
        return specialOrder;
    }

    public void setSpecialOrder(String specialOrder) {
        this.specialOrder = specialOrder;
    }

    public String getCustomOrder() {
        return customOrder;
    }

    public void setCustomOrder(String customOrder) {
        this.customOrder = customOrder;
    }

    /**
     * Returns the customerReplacementSku specified on the CatalogXItem if one exists. If not,
     * returns the APD sku of the item specified by item.replacement.
     * @return 
     */
    public String getReplacementSku() {
        String toReturn = null;
        if (properties != null) {
            toReturn = properties.get("customerReplacementSku");
        }
        if (StringUtils.isBlank(toReturn)) {
            toReturn = customerReplacementSku;
        }
        return toReturn;
    }

    /**
     * Returns the customerReplacementVendor specified on the CatalogXItem if one exists. If not,
     * returns the vendor of the item specified by item.replacement.
     * @return 
     */
    public String getReplacementVendor() {
        String toReturn = null;
        if (properties != null) {
            toReturn = properties.get("customerReplacementVendor");
        }
        if (StringUtils.isBlank(toReturn)) {
            toReturn = customerReplacementVendor;
        }
        return toReturn;
    }

}
