/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.MatchbookBp;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.product.Breadcrumb;
import com.apd.phoenix.shopping.view.jsf.bean.search.ProductSort.SortOption;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@LocalBean
@SessionScoped
@Lock(LockType.READ)
public class SearchBean implements SolrSearcher {

    private static final String CATALOG_EMPTY_MESSAGE = "The Catalog is Empty.";
    private static final String HEIRARCHY_PATH_SOLR_COLUMN = "hierarchy_path";
    private static final int NO_PAGE_DATA = 0;
    private static final Logger logger = LoggerFactory.getLogger(SearchBean.class);
    private static final String SEARCH_REQUEST_HANDLER = "MainSearchHandler";
    private static final String INK_TONER_SEARCH_REQUEST_HANDLER = "InkTonerSearchHandler";
    private List<Product> products;
    private QueryResponse response;
    private int numFound;
    private List<String> categories;
    private Map<String, Integer> manufacturerWidgetData;
    private Multimap<String, String> facetMap;
    private String requestParameterString;
    private SolrQuery lastSolrQuery;
    private SolrServer solr;
    private CategoryMenu categoryMenu;
    private Breadcrumb breadcrumb;
    private SubCategoryMenu subCategoryMenu;
    private QueryData queryData;
    private QueryData previousQueryData;
    private PageNumberDisplay pageDisplay;
    private int displayCount = 0;
    private PageSize pageSize = PageSize.TEN;
    private int pageSizeIndex;
    private int previousPage;
    private boolean resetSort;
    private String inkTonerModel;
    private String inkTonerManufacturer;
    private ProductSort productSort;
    private List<Manufacturer> manufacturers;
    private Integer pageViewParam = 0;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private SolrQueryUtilities solrQueryUtilities;
    @Inject
    SolrServiceBean solrServiceBean;
    @Inject
    private MatchbookBp matchbookBp;
    @Inject
    private LinksBean linksBean;

    private boolean catalogNotEmpty = true;

    protected static String REFINE_YOUR_SEARCH_STRING = "Search within results";

    @PostConstruct
    public void init() {
        solr = solrServiceBean.getSolrServer();
        breadcrumb = new Breadcrumb();
        categories = new ArrayList<>();
        queryData = new QueryData();
        productSort = new ProductSort();
        categories.add("All");
        initCategoryMenuWidget();
        resetSort = true;
        populateInkAndToner();
    }

    private void populateInkAndToner() {
        manufacturers = new ArrayList<Manufacturer>();
        manufacturers.addAll(matchbookBp.matchbookManufacturersFromCatalog(credentialSelectionBean
                .getCurrentCredential().getCatalog()));
        Collections.sort(manufacturers, new EntityComparator());
    }

    /**
     * Initializes the category menu contents once per session with a custom
     * solr query
     */
    public void initCategoryMenuWidget() {
        //Init breadcrumb
        //Note: most of the time hierarchyPath will be empty at this point, but we still need to init to catch cases where 
        //a hierarchyPath is passed in the url on the first page load
        breadcrumb = new Breadcrumb(queryData.getHierarchyPath());
        if (categoryMenu == null) {
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.addField(HIERARCHY_PATH_FACET_VALUE);
            solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
            solrQuery.add("q", "*:*");
            solrQuery.addFacetField(HIERARCHY_PATH_FACET_VALUE);
            ModifiableSolrParams param = new ModifiableSolrParams();
            param.set("facet.mincount", 1);
            param.set("facet.limit", -1);
            solrQuery.add(param);
            try {
                QueryResponse categoryMenuResponse = solr.query(solrQuery);
                categoryMenu = new CategoryMenu(categoryMenuResponse);
                initSearchCombobox();
            }
            catch (SolrServerException ex) {
                logger.info("Error querying solr to build category widget");
            }
        }
    }

    private void initSearchCombobox() {
        for (String categoryName : categoryMenu.getTopLevelCategoriesList()) {
            categories.add(categoryName);
        }
    }

    public void solrProductSearchInit() {
        try {
            if (StringUtils.isEmpty(queryData.getActiveFiltersString())) {
                //sync activeFilters set with activeFilersString
                queryData.setActiveFilters(new HashSet<String>());
            }
            requestParameterString = SearchUtilities.generateRequestParameterString(queryData);
            FacesContext.getCurrentInstance().getExternalContext().redirect(requestParameterString);
        }
        catch (IOException ex) {
            logger.error("IOException in solrProductSearchInit()", ex);
        }
    }

    public void solrProductRefineSearchInit() {
        resetSort = false;
        String refineSearchString = queryData.getRefineSearchString();
        queryData = QueryData.clone(previousQueryData);
        queryData.setRefineSearchString(refineSearchString);
        solrProductSearchInit();
    }

    //TODO add user specific filtering 
    public void solrProductSearch() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return; // Skip postback requests.
        }
        if (allRequestParametersEmpty()) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(getFirstLevelHierarchyBrowseUrl());
            }
            catch (IOException ex) {
                logger.error("Error Redirecting to first level hierarchy on Product Browse Page.");
            }
        }
        SolrQuery solrQuery = new SolrQuery();
        if (StringUtils.isNotBlank(queryData.getSearchString())) {
            solrQuery.setRequestHandler(SEARCH_REQUEST_HANDLER);
        }
        else if (this.isInkTonerSearch()) {
            solrQuery.setRequestHandler(INK_TONER_SEARCH_REQUEST_HANDLER);
        }
        breadcrumb = new Breadcrumb();
        subCategoryMenu = new SubCategoryMenu();
        solrProductSearchCore(solrQuery);
    }

    private void solrProductSearchCore(SolrQuery solrQuery) {
        solrQuery = addAllParams(solrQuery);
        querySolr(solrQuery);
        handlResults();
    }

    private SolrQuery addAllParams(SolrQuery solrQuery) {
        ModifiableSolrParams params = getQueryParams();
        solrQuery.add(params);
        solrQuery.setRows(pageSize.toInt());
        solrQuery = addSortOptions(solrQuery);
        solrQuery = addFacetFieldsToQuery(solrQuery);
        solrQuery = addFilterFieldsToQuery(solrQuery);
        solrQuery = addManufacturerFilter(solrQuery);
        solrQuery = addPriceRangeFilter(solrQuery);
        solrQuery = addRefineSearchFilter(solrQuery);
        solrQuery = addCoreItemBoost(solrQuery);
        solrQuery = addPhraseSlopBoost(solrQuery);
        solrQuery = addUnavailableItemsFilter(solrQuery);
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        resetSort = true;
        return solrQuery;
    }

    private boolean allRequestParametersEmpty() {
        Iterator<String> requestParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterNames();
        if (requestParameters.hasNext()) {
            return false;
        }
        return true;
    }

    private void querySolr(SolrQuery query) {
        try {
            logger.info(query.toString());
            response = solr.query(query);
            lastSolrQuery = query;
        }
        catch (SolrServerException | SolrException ex) {
            logger.error("Error trying to query solr: ", ex);
        }
    }

    private void handlResults() {
        try {
            SolrDocumentList searchResultDocuments = response.getResults();

            if (queryData != null && queryData.getSearchString() != null) {
                String heirarchyMatch = checkForHierarchyMatches(response);
                if (!(queryData.getHierarchyPath() != null && queryData.getHierarchyPath().equals(
                        queryData.getSearchString()))
                        && null != heirarchyMatch) {
                    searchByHierarchyOnly(heirarchyMatch);
                    return;
                }
            }
            products = SearchUtilities.convertToProductList(searchResultDocuments);
            numFound = (int) response.getResults().getNumFound();
            displayCount = products.size();
            Integer page = null;
            if (pageViewParam != null && pageViewParam != NO_PAGE_DATA) {
                page = pageViewParam;
                pageViewParam = NO_PAGE_DATA;
            }
            pageDisplay = new PageNumberDisplay(numFound);
            pageDisplay.setPageSize(pageSize);
            previousPage = 1;
            if (page != null) {
                pageDisplay.setPage(page);
            }
            logger.info("Results Found: " + numFound);
            parseFacets(response);
        }
        catch (Exception ex) {
            logger.error("Error handling solr results", ex);
        }
        clearParameters();
    }

    private void searchByHierarchyOnly(String heirarchyMatch) {
        queryData.setHierarchyPath(heirarchyMatch);
        queryData.setSearchString("");
        solrProductSearch();

    }

    private ModifiableSolrParams getQueryParams() {
        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set("facet", true);
        params.set("facet.mincount", 1);

        if (queryData.getStart() > 0) {
            params.set("start", queryData.getStart());
        }

        //using search bar
        if (StringUtils.isNotBlank(queryData.getSearchString())) {
            params.set("q", queryData.getSolrSafeSearchString());

            //Set to best match by default when using search bar
            if (resetSort) {
                productSort.setSelectedSortBy(SortOption.BestMatch);
            }

        }
        //viewing company favorites
        else if (StringUtils.isNotBlank(queryData.getCompanyFavoritesId())) {
            params.set("q", COMPANY_FAVORITES_FIELD + ":" + queryData.getCompanyFavoritesId());
        }
        else if (StringUtils.isNotBlank(queryData.getMatchbookId())) {
            params.set("q", MB_ID_FIELD + ":" + queryData.getMatchbookId());
        }
        if (StringUtils.isNotBlank(queryData.getHierarchyPath())
                && !queryData.getHierarchyPath().equals(SolrSearcher.ALL_HIERARCHY_PATH)) {
            if (StringUtils.isBlank(queryData.getSearchString())) {
                params.set("q", HIERARCHY_PATH_FACET_VALUE + ":" + "/"
                        + SearchUtilities.removeRegexCharacters(queryData.getHierarchyPath()) + HIERARCHY_REGEX_ARG
                        + "/");
            }
            else {
                params.set("fq", HIERARCHY_PATH_FACET_VALUE + ":" + "/"
                        + SearchUtilities.removeRegexCharacters(queryData.getHierarchyPath()) + HIERARCHY_REGEX_ARG
                        + "/");
            }
            if (StringUtils.isBlank(queryData.getSearchString())) {
                //intialize hierarchy view only if the user browsed from the left menu, not when searching
                initSubMenuWidgets(queryData.getHierarchyPath());
            }
        }
        return params;
    }

    private SolrQuery addRefineSearchFilter(SolrQuery query) {
        if (StringUtils.isNotBlank(queryData.getRefineSearchString())) {
            query.addFilterQuery(queryData.getSolrSafeRefineSearchString().trim().replace(" ", " AND "));
            resetSort = false;
        }
        return query;
    }

    private SolrQuery addCoreItemBoost(SolrQuery query) {
        query.add("bf", this.getCoreItemBoostSolrFunction());
        return query;
    }

    private String getCoreItemBoostSolrFunction() {
        String boostFactor = PropertiesLoader.getAsProperties("solr.properties").getProperty("core.item.boost");
        //note: by this query, only core item start date is checked when determining whether to boost. This follows 
        //business requirements, where items that have been expired from a core list should still be boosted.
        String toReturn = "map(if(" + CORE_ITEM_START_DATE_FIELD + ",div(abs(ms(NOW," + CORE_ITEM_START_DATE_FIELD
                + ")),ms(NOW,coreitemstartdate)),-1),0,1," + boostFactor + ",1)";
        return toReturn;
    }

    private SolrQuery addPhraseSlopBoost(SolrQuery query) {
        String phraseFields = PropertiesLoader.getAsProperties("solr.properties").getProperty("phrase.fields");
        query.add("pf", phraseFields);
        String phraseSlop = PropertiesLoader.getAsProperties("solr.properties").getProperty("phrase.slop");
        query.add("ps", phraseSlop);
        return query;
    }

    @SuppressWarnings("static-method")
    private SolrQuery addFacetFieldsToQuery(SolrQuery query) {
        query.addFacetField("filters");
        query.addFacetField(SOLR_PRODUCT_MANUFACTURER_NAME_FIELD);
        query.addFacetField(HEIRARCHY_PATH_SOLR_COLUMN);
        return query;
    }

    private SolrQuery addFilterFieldsToQuery(SolrQuery query) {
        if (StringUtils.isNotBlank(queryData.getActiveFiltersString())) {
            separateFilterFacets(queryData.getActiveFiltersString());
            Map<String, Set<String>> filterMap = parseFilters(queryData.getActiveFilters());
            for (String filterCategory : filterMap.keySet()) {
                //tag the query constructed for this category for reference in exclusion below
                String filterQueryString = "{!tag=" + SearchUtilities.getTagName(filterCategory) + "}(";
                for (String filterCategoryChild : filterMap.get(filterCategory)) {
                    filterQueryString += "filters:\""
                            + SearchUtilities.makeSolrSafe(getFilterString(filterCategory, filterCategoryChild)) + "\""
                            + " OR ";
                }
                filterQueryString = filterQueryString.substring(0, filterQueryString.length() - 4) + ")"; //remove the last " OR " and close parentheses
                query.addFilterQuery(filterQueryString);
                //add facet field excluding the filters from this category for later reference to get filter counts for filters in the same category
                query.addFacetField("{!key=" + SearchUtilities.getKeyName(filterCategory) + " ex="
                        + SearchUtilities.getTagName(filterCategory) + "}filters");
            }
        }
        return query;
    }

    private SolrQuery addManufacturerFilter(SolrQuery query) {
        if (StringUtils.isNotBlank(queryData.getSelectedManufacturer())) {
            query.addFilterQuery("{!raw f=mname}" + queryData.getSelectedManufacturer());
        }
        return query;
    }

    private SolrQuery addPriceRangeFilter(SolrQuery query) {
        if (queryData.getLowPrice() != -1 && queryData.getHighPrice() != -1) {
            query.addFilterQuery("price:[" + queryData.getLowPrice() + " TO " + queryData.getHighPrice() + "]");
        }
        return query;
    }

    private SolrQuery addSortOptions(SolrQuery query) {
        if (productSort.getSelectedSortBy() != SortOption.BestMatch) {

            SortOption selectedSortOption = productSort.getSelectedSortBy();
            if (selectedSortOption == SortOption.SortByProductNameAsc) {
                query.addSort(NAME_SORT_FIELD, ORDER.asc);
            }
            else if (selectedSortOption == SortOption.SortByProductNameDesc) {
                query.addSort(NAME_SORT_FIELD, ORDER.desc);
            }
            else if (selectedSortOption == SortOption.SortBySkuAsc) {
                //detect sku type preference for sorting from credential
                switch (getCredentialSkuType()) {
                    case "customer":
                        query.addSort(CUSTOMER_SKU_FIELD, ORDER.asc);
                        break;
                    case "manufacturer":
                        query.addSort(MANUFACTURER_SKU_FIELD, ORDER.asc);
                        break;
                    case "vendor":
                        query.addSort(MANUFACTURER_SKU_FIELD, ORDER.asc);
                        break;
                    default:
                        query.addSort(APD_SKU_FIELD, ORDER.asc);
                        break;
                }
            }
            else if (selectedSortOption == SortOption.SortBySkuDesc) {
                //detect sku type preference for sorting from credential
                switch (getCredentialSkuType()) {
                    case "customer":
                        query.addSort(CUSTOMER_SKU_FIELD, ORDER.desc);
                        break;
                    case "manufacturer":
                        query.addSort(MANUFACTURER_SKU_FIELD, ORDER.desc);
                        break;
                    case "vendor":
                        query.addSort(MANUFACTURER_SKU_FIELD, ORDER.desc);
                        break;
                    default:
                        query.addSort(APD_SKU_FIELD, ORDER.desc);
                        break;
                }
            }
            else if (selectedSortOption == SortOption.SortByPriceAsc) {
                query.addSort(SOLR_PRODUCT_PRICE_FIELD, ORDER.asc);
            }
            else if (selectedSortOption == SortOption.SortByPriceDesc) {
                query.addSort(SOLR_PRODUCT_PRICE_FIELD, ORDER.desc);
            }
            else if (selectedSortOption == SortOption.SortByManufacturerAsc) {
                query.addSort(SOLR_PRODUCT_MANUFACTURER_NAME_FIELD, ORDER.asc);
            }
            else if (selectedSortOption == SortOption.SortByManufacturerDesc) {
                query.addSort(SOLR_PRODUCT_MANUFACTURER_NAME_FIELD, ORDER.desc);
            }
        }

        return query;
    }

    public void clearAllFilters() {
        queryData.setActiveFilters(new HashSet<String>());
        queryData.setActiveFiltersString("");
        queryData.setFiltersString("");
        previousQueryData.setActiveFilters(new HashSet<String>());
        previousQueryData.setActiveFiltersString("");
        previousQueryData.setFiltersString("");
        solrProductSearchRefresh();
    }

    public void solrProductSearchRefresh() {
        queryData = previousQueryData;
        if (allRequestParametersEmpty()) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(getFirstLevelHierarchyBrowseUrl());
            }
            catch (IOException ex) {
                logger.error("Error Redirecting to first level hierarchy on Product Browse Page.", ex);
            }
        }
        SolrQuery solrQuery = new SolrQuery();
        solrProductSearchCore(solrQuery);
    }

    public Set<Long> findMatchbooksInCatalog() {
        SolrQuery query = new SolrQuery();
        query.addField(MB_ID_FIELD);
        query = solrQueryUtilities.addFilterByCatalogID(query);
        query.add("q", MB_ID_FIELD + ":" + FIELD_IS_NOT_NULL);
        query.add("rows", 999999 + "");
        try {
            QueryResponse matchbookQueryResponse = solr.query(query);
            return parseMatchbookQueryResponse(matchbookQueryResponse);
        }
        catch (SolrServerException ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
	private Set<Long> parseMatchbookQueryResponse(QueryResponse queryResponse) {
        Set<Long> toReturn = new HashSet<>();
        SolrDocumentList results = queryResponse.getResults();
        for (SolrDocument d : results) {
            ArrayList<String> matchbooks = (ArrayList<String>)d.getFieldValue(MB_ID_FIELD);
            for (String s : matchbooks) {
                try {
                    toReturn.add(Long.valueOf(s));
                } catch (NumberFormatException e) {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        return toReturn;
    }

    private void clearParameters() {
        previousQueryData = queryData;
        queryData = new QueryData();
    }

    private void separateFilterFacets(String paramString) {
        String[] filters = paramString.split(DELIMITER_SPLITTER);
        queryData.getActiveFilters().addAll(Arrays.asList(filters));
    }

    /**
     * Parses filters from a set of strings containing filter category information into a map of filter categories mapped onto filters in that category
     * @param filters
     * @return 
     */
    private Map<String, Set<String>> parseFilters(Set<String> filters) {
        Map<String, Set<String>> toReturn = new HashMap<>();
        for (String f : filters) {
            String filterCategory = getFilterCategory(f);
            String filterCategoryChild = getFilterCategoryChild(f);
            if (toReturn.get(filterCategory) == null) {
                Set<String> newSet = new HashSet<>();
                newSet.add(filterCategoryChild);
                toReturn.put(filterCategory, newSet);
            }
            else {
                toReturn.get(filterCategory).add(filterCategoryChild);
            }
        }
        return toReturn;
    }

    public List<String> getCategories() {
        if (categories.size() == 1) {
            initSearchCombobox();
        }
        return categories;
    }

    private void initSubMenuWidgets(String hierarchyPath) {
        breadcrumb = new Breadcrumb(hierarchyPath);
        subCategoryMenu = new SubCategoryMenu(categoryMenu.getCategoryData(), breadcrumb);
    }

    private void parseFacets(QueryResponse response) {
        resetFacetData();
        facetMap = SearchUtilities.parseFilters(response);
        manufacturerWidgetData = SearchUtilities.parseManufacturerFacets(response);
    }

    private String checkForHierarchyMatches(QueryResponse response) {
        FacetField hierarchyField = response.getFacetField(HEIRARCHY_PATH_SOLR_COLUMN);
        if (hierarchyField != null) {
            for (FacetField.Count c : hierarchyField.getValues()) {
                String name = c.getName();
                String lastHierarchy = name.substring(name.lastIndexOf('!') + 1);
                if (queryData.getSearchString().equals(lastHierarchy)) {
                    return name;
                }
            }
        }
        return null;

    }

    private void resetFacetData() {
        manufacturerWidgetData = new HashMap<>();
        facetMap = HashMultimap.create();
    }

    public List<String> getFilterCategoryNames() {
        if (facetMap == null) {
            return null;
        }
        List<String> categoryNameList = new ArrayList<>();
        categoryNameList.addAll(facetMap.keySet());
        Collections.sort(categoryNameList);
        return categoryNameList;

    }

    public List<String> getFilterAttributeList(String categoryName) {
        List<String> filterCategoryChildren = new ArrayList<>();
        filterCategoryChildren.addAll(facetMap.get(categoryName));
        Collections.sort(filterCategoryChildren);
        return filterCategoryChildren;
    }

    public boolean isFilterActive(String categoryName, String filterCategoryChild) {
        int n = filterCategoryChild.lastIndexOf(" (");
        filterCategoryChild = filterCategoryChild.substring(0, n);
        String filterString = getFilterString(categoryName, filterCategoryChild);
        queryData.setActiveFilters(getLastActiveFilter());
        if (queryData.getActiveFilters() != null && queryData.getActiveFilters().contains(filterString)) {
            return true;
        }
        return false;
    }

    private Set<String> getLastActiveFilter() {
        Set<String> filters = new HashSet<>();
        if (previousQueryData.getActiveFilters() != null) {
            filters = previousQueryData.getActiveFilters();
        }
        return filters;
    }

    public Map<String, Integer> getManufacturerWidgetData() {
        if (manufacturerWidgetData == null) {
            return null;
        }
        return manufacturerWidgetData;
    }

    public List<String> getManufacturerWidgetKeySet() {
        if (manufacturerWidgetData == null) {
            return null;
        }
        Map<String, Integer> sortedMap = new TreeMap<String, Integer>(new ValueComparator(manufacturerWidgetData));
		sortedMap.putAll(manufacturerWidgetData);
        List<String> keyList = new ArrayList<>(sortedMap.keySet());        
        return keyList;
    }

    public int getManufacturerCountByName(String mname) {
        if (manufacturerWidgetData == null) {
            return 0;
        }
        return manufacturerWidgetData.get(mname);
    }

    public boolean renderManufacturerWidget() {
        if (manufacturerWidgetData != null && getManufacturerWidgetKeySet() != null
                && getManufacturerWidgetKeySet().size() > 0) {
            return true;
        }
        return false;
    }

    public void applyFilter() {
        resetSort = false;
        queryData.setActiveFilters(getLastActiveFilter());
        if (queryData.getActiveFilters().contains(queryData.getSelectedFilterName())) {
            //deactivate
            deactivateFilter();
        }
        else {
            //activate
            queryData.getActiveFilters().add(queryData.getSelectedFilterName());
            setActiveFiltersString();
        }
        String activeFilterString = queryData.getActiveFiltersString();
        Set<String> activeFilters = queryData.getActiveFilters();
        queryData = previousQueryData;
        queryData.setActiveFilters(activeFilters);
        queryData.setActiveFiltersString(activeFilterString);
        solrProductSearchInit();
    }

    public void deactivateFilter() {
        queryData.getActiveFilters().remove(queryData.getSelectedFilterName());
        setActiveFiltersString();
    }

    private void setActiveFiltersString() {
        queryData.setActiveFiltersString(StringUtils.join(queryData.getActiveFilters(), DELIMITER));
    }

    @SuppressWarnings("static-method")
    public boolean isCategoryActive() {
        return true;
    }

    public boolean renderCategoryFilters() {
        if (getFilterCategoryNames() != null && getFilterCategoryNames().size() > 0) {
            return true;
        }
        return false;
    }

    public String filterOnManufacturer(String mname) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setSelectedManufacturer(mname);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public SolrQuery getLastSolrQuery() {
        return lastSolrQuery;
    }

    public void setLastSolrQuery(SolrQuery lastSolrQuery) {
        this.lastSolrQuery = lastSolrQuery;
    }

    public QueryResponse getResponse() {
        return response;
    }

    public int getNumFound() {
        return numFound;
    }

    public void setRequestParameterString(String requestParameterString) {
        this.requestParameterString = requestParameterString;
    }

    public String getRequestParameterString() {
        return requestParameterString;
    }

    public List<Product> getProducts() {
        if (pageDisplay.getPage() != previousPage) {
            try {
                previousPage = pageDisplay.getPage();
                lastSolrQuery.setStart((pageDisplay.getPage() - 1) * pageDisplay.getSize());
                QueryResponse updatedResponse = solr.query(lastSolrQuery);
                SolrDocumentList updatedDocList = updatedResponse.getResults();
                products = SearchUtilities.convertToProductList(updatedDocList);
                numFound = (int) updatedResponse.getResults().getNumFound();
                displayCount = products.size();
            }
            catch (SolrServerException ex) {
                logger.error("Error sending query to solr", ex);
            }
        }
        return products;
    }

    public CategoryMenu getCategoryMenu() {
        return categoryMenu;
    }

    public Breadcrumb getBreadcrumb() {
        return breadcrumb;
    }

    public SubCategoryMenu getSubCategoryMenu() {
        return subCategoryMenu;
    }

    public String getPriceUrl(float lowPrice, float highPrice) {
        QueryData data = QueryData.clone(previousQueryData);
        data.setLowPrice(lowPrice);
        if (highPrice == 0) {
            highPrice = Float.MAX_VALUE;
        }
        data.setHighPrice(highPrice);
        return SearchUtilities.generateRequestParameterString(data);
    }

    public boolean priceFilterIsApplied(float min, float max) {
        if (Float.compare(previousQueryData.getLowPrice(), min) == 0
                && Float.compare(previousQueryData.getHighPrice(), max) == 0) {
            return true;
        }
        else
            return false;
    }

    public QueryData getQueryData() {
        return queryData;
    }

    public void setQueryData(QueryData queryData) {
        this.queryData = queryData;
    }

    /**
     * Returns the object used to store the page display information for the
     * page navigation.
     *
     * @return The page display information.
     */
    public PageNumberDisplay getPageDisplay() {
        //TODO: complete implementation of PageNumberDisplay object
        //TODO: first instantiation of pageDisplay object
        if (this.pageDisplay == null) {
            this.pageDisplay = new PageNumberDisplay(100);
        }
        return pageDisplay;
    }

    public int getDisplayCount() {
        return displayCount;
    }

    public boolean isIsSearchByString() {
        return StringUtils.isNotEmpty(previousQueryData.getSearchString());
    }

    public QueryData getPreviousQueryData() {
        return previousQueryData;
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public void refreshPage() {
        resetSort = false;
        queryData = QueryData.clone(previousQueryData);
        solrProductSearchInit();
    }

    public int getPageSizeIndex() {
        return pageSizeIndex;
    }

    public void setPageSizeIndex(int pageSizeIndex) {
        this.pageSizeIndex = pageSizeIndex;
        int i = 0;
        for (PageSize p : PageSize.values()) {
            if (i == pageSizeIndex) {
                setPageSize(p);
                break;
            }
            i++;
        }
    }

    public String getResultInformationString() {
        int start = 1;
        if (pageDisplay.getPage() != 1) {
            start += (pageDisplay.getPage() - 1) * pageDisplay.getSize();
        }

        int end = start + pageSize.toInt() - 1;
        if (end > numFound) {
            end = numFound;
        }

        StringBuilder resultInfoString = new StringBuilder("Displaying ");
        resultInfoString.append(start);
        resultInfoString.append(" - ");
        resultInfoString.append(end);
        resultInfoString.append(" of ");
        resultInfoString.append(numFound);
        resultInfoString.append(" Results ");
        return resultInfoString.toString();
    }

    public ProductSort getProductSort() {
        return productSort;
    }

    public List<Manufacturer> getManufacturers() {
        return manufacturers;
    }

    public void setManufacturers(List<Manufacturer> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public String getCredentialSkuType() {
        SkuType credentialSkuType = credentialSelectionBean.getCurrentCredential().getSkuType();
        if (credentialSkuType == null) {
            return APD_SKU_FIELD;
        }
        return credentialSkuType.getName();
    }

    private String getFirstLevelHierarchyBrowseUrl() {
        StringBuilder urlPrefix = new StringBuilder(SEARCH_PAGE_BASE_URL);
        String hierarchyPath;
        urlPrefix.append("?").append(HIERARCHY_PATH_PARAMETER);
        urlPrefix.append("=");
        urlPrefix.append(HIERARCHY_DELIMITER);
        if (categoryMenu != null && categoryMenu.getTopLevelCategories() != null
                && !categoryMenu.getTopLevelCategories().isEmpty()) {
            hierarchyPath = categoryMenu.getTopLevelCategories().iterator().next();
        }
        else {
            //This is a bit hacky, but it only happens in the already bad state of having an empty customer catalog.
            hierarchyPath = CATALOG_EMPTY_MESSAGE;
        }
        try {
            hierarchyPath = URLEncoder.encode(hierarchyPath, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        return urlPrefix.toString() + hierarchyPath;
    }

    public boolean isSelectedManufacturer(String manufacturer) {
        if (previousQueryData.getSelectedManufacturer() != null && manufacturer != null) {
            return previousQueryData.getSelectedManufacturer().equals(manufacturer);
        }
        else {
            return false;
        }
    }

    public boolean shouldRenderPriceFilters() {
        if ((products != null && !products.isEmpty()) || previousQueryData != null) {
            return true;
        }
        else
            return false;
    }

    public String getDefaultRefineSearchString() {
        return REFINE_YOUR_SEARCH_STRING;
    }

    public String getFilterCategory(String filterString) {
        return filterString.split(INNER_FILTER_DELIMITER_SPLITTER)[0];
    }

    public String getFilterCategoryChild(String filterString) {
        return filterString.split(INNER_FILTER_DELIMITER_SPLITTER)[1];
    }

    public String getFilterString(String filterCategory, String filterCategoryChild) {
        return filterCategory + INNER_FILTER_DELIMITER + filterCategoryChild;
    }

    public boolean isInkTonerSearch() {
        return (queryData != null && StringUtils.isNotBlank(queryData.getMatchbookId()))
                || (previousQueryData != null && StringUtils.isNotBlank(previousQueryData.getMatchbookId()));
    }

    public boolean isInkTonerRendered() {
        return !(manufacturers == null || manufacturers.isEmpty());
    }

    private SolrQuery addUnavailableItemsFilter(SolrQuery solrQuery) {
        solrQuery.addFilterQuery(UNAVAILABLE_ITEMS_FILTER);
        return solrQuery;
    }

    public boolean isResetSort() {
        return resetSort;
    }

    public void setResetSort(boolean resetSort) {
        this.resetSort = resetSort;
    }

    public String getPageNumber() {
        return Integer.toString(this.pageDisplay.getPage());
    }

    public Converter getInkTonerManufacturerConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (manufacturers == null) {
                    return null;
                }
                for (Manufacturer m : manufacturers) {
                    if (m.getName().equals(value)) {
                        return m;
                    }
                }
                return null;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return null;
                }
                return ((Manufacturer) value).getName();
            }
        };
    }

    public String getInkTonerModel() {
        return inkTonerModel;
    }

    public void setInkTonerModel(String inkTonerModel) {
        this.inkTonerModel = inkTonerModel;
    }

    public String getInkTonerManufacturer() {
        return inkTonerManufacturer;
    }

    public void setInkTonerManufacturer(String inkTonerManufacturer) {
        this.inkTonerManufacturer = inkTonerManufacturer;
    }

    public Integer getPageViewParam() {
        return pageViewParam;
    }

    public void setPageViewParam(Integer pageViewParam) {
        this.getPageDisplay().setPage(pageViewParam);
        this.pageViewParam = pageViewParam;
    }

    public String buildResultsTitle() {
        String toReturn = "Results for '";
        if (!StringUtils.isEmpty(previousQueryData.getRefineSearchString())) {
            toReturn = toReturn + previousQueryData.getRefineSearchString();
        }
        else {
            toReturn = toReturn + previousQueryData.getSearchString();
        }
        if (!StringUtils.isEmpty(previousQueryData.getHierarchyPath())) {
            Breadcrumb breadCrumb = new Breadcrumb(previousQueryData.getHierarchyPath());
            toReturn = toReturn + "' within '" + breadCrumb.getTail() + "'";
        }
        return toReturn;
    }

    public String getSearchTermsJson() {
        return "'" + linksBean.getLookahead()
                + this.credentialSelectionBean.getCurrentCredential().getCatalog().getId() + "/'";
    }

    class ValueComparator implements Comparator {

        Map map;

        public ValueComparator(Map map) {
            this.map = map;
        }

        public int compare(Object keyA, Object keyB) {
            Comparable valueA = (Comparable) map.get(keyA);
            Comparable valueB = (Comparable) map.get(keyB);
            return valueB.compareTo(valueA);
        }
    }
}
