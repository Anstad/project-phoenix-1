package com.apd.phoenix.shopping.view.jsf.bean.product;

/**
 *
 * @author dcnorris
 */
public enum SortOption {

    ALPHABETICAL("Product name A - Z"), REVERSE_ALPHABETICAL("Product name Z - A"), PRICE_LOWEST("Price Lowest first"), PRICE_HIGHEST(
            "Price Highest first");

    private String label;

    private SortOption(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
