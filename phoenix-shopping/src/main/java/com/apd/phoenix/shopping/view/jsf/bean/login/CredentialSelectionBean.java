package com.apd.phoenix.shopping.view.jsf.bean.login;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.MIN_ORDER_FEE;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.jacc.PolicyContext;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.IpSubdomainRestriction;
import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.CustomerSpecificIconUrl;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.ShoppingCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 120)
public class CredentialSelectionBean {

    private static final Logger logger = LoggerFactory.getLogger(CredentialSelectionBean.class);
    public static final String DEFAULT_USER_GUIDE_NAME = "Cheat_Sheet-estore_CSS.pdf";
    public static final String DEFAULT_SPECIAL_ORDER_FILE_NAME = "special_orders_278.docx";
    public static final String DEFAULT_CUSTOM_ORDER_FILE_NAME = "custom_orders.docx";

    private AccountXCredentialXUser currentAXCXU;

    private String arrivalRequestUrl;

    private Map<String, String> propertyMap;

    private Map<String, String> iconUrlMap;

    private PunchoutSession punchoutSession;

    private String punchoutBuyerCookie;

    private PunchoutOrderOperation punchoutOperation;

    @Inject
    private PunchoutSessionBp punchoutSessionBp;

    @Inject
    private LoginBean loginBean;

    @Inject
    private ShoppingCartBean cartBean;

    @Inject
    private AccountXCredentialXUserBp axcxuBp;

    @Inject
    private IpSubdomainRestriction restriction;

    @Inject
    private SecurityContext securityContext;

    /**
     * Gets the current credential
     * 
     * @return
     */
    public Credential getCurrentCredential() {
        if (currentAXCXU == null) {
            return null;
        }
        return currentAXCXU.getCredential();
    }

    /**
     * Gets the current AccountXCredentialXUser
     * 
     * @return
     */
    public AccountXCredentialXUser getCurrentAXCXU() {
        return currentAXCXU;
    }

    public String getUserGuideName() {
        if (getCurrentCredential() != null && StringUtils.isNotEmpty(getCurrentCredential().getUserGuideFileName())) {
            return getCurrentCredential().getUserGuideFileName();
        }
        else {
            return DEFAULT_USER_GUIDE_NAME;
        }
    }

    public String getSpecialOrderGuideName() {
        if (getCurrentCredential() != null && StringUtils.isNotEmpty(getCurrentCredential().getSpecialOrderFileName())) {
            return getCurrentCredential().getSpecialOrderFileName();
        }
        else {
            return DEFAULT_SPECIAL_ORDER_FILE_NAME;
        }
    }

    public String getCustomOrderGuideName() {
        if (getCurrentCredential() != null && StringUtils.isNotEmpty(getCurrentCredential().getCustomOrderFileName())) {
            return getCurrentCredential().getCustomOrderFileName();
        }
        else {
            return DEFAULT_CUSTOM_ORDER_FILE_NAME;
        }
    }

    /**
     * Sets the current AccountXCredentialXUser. This should only be used by the credentialSelection.xhtml page.
     * 
     * @param axcxu
     */
    public void setCurrentAXCXU(AccountXCredentialXUser axcxu) {
        currentAXCXU = axcxuBp.getAccountXCredentialXUserForBrowse(axcxu.getId());
        propertyMap = new HashMap<>();
        for (CredentialXCredentialPropertyType property : currentAXCXU.getCredential().getProperties()) {
        	propertyMap.put(property.getType().getName(), property.getValue());
        }
        for (AccountXAccountPropertyType property : currentAXCXU.getAccount().getProperties()) {
        	if (MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue().equals(property.getType().getName())
        			|| MIN_ORDER_FEE.getValue().equals(property.getType().getName())) {
        		propertyMap.put(property.getType().getName(), property.getValue());
        	}
        }
        
        iconUrlMap = new HashMap<>();
        for (CustomerSpecificIconUrl iconUrl : currentAXCXU.getAccount().getIconUrls()) {
        	if (iconUrl.getClassificationType() != null) {
        		iconUrlMap.put(iconUrl.getClassificationType().getIconUrl(), iconUrl.getOverrideIconUrl());
        	}
        	else if (iconUrl.getPropertyType() != null) {
        		iconUrlMap.put(iconUrl.getPropertyType().getIconUrl(), iconUrl.getOverrideIconUrl());
        	}
        }
        securityContext.setCredential(currentAXCXU.getCredential());
        
        //clears ip addresses and subdomains, to prevent the user having access to a credential when they 
        //shouldn't (but would otherwise be able to log in)
        loginBean.getValidIpAddresses().clear();
        loginBean.getValidSubdomains().clear();
        
        cartBean.init();
    }

    /**
     * Returns only AccountXCredentialXUsers for the current user that are valid/authorized
     * based on the requested url and the nature of the user web session 
     * (e.g. punchin from procurement platform or direct login)
     *
     * @return
     */
    public List<AccountXCredentialXUser> getUserCredentials() {
        SystemUser user = loginBean.getSystemUser();
        List<AccountXCredentialXUser> userCredentialsList = new ArrayList<>();
        if (isPunchoutSession() && getPunchoutSession().getAccntCredUser() != null) {
        	userCredentialsList.add(axcxuBp.findById(getPunchoutSession().getAccntCredUser().getId(), AccountXCredentialXUser.class));
        }
        else  {
        	userCredentialsList = axcxuBp.getAllAXCXU(user);
        }
        List<AccountXCredentialXUser> authorizedCredentials = new ArrayList<>();
        try {
            HttpServletRequest request = (HttpServletRequest) PolicyContext
                    .getContext("javax.servlet.http.HttpServletRequest");
            for (AccountXCredentialXUser credential : userCredentialsList) {
                if (restriction.isAccXCredXUserValid(credential, request, isPunchoutSession())) {
                	authorizedCredentials.add(credential);
                }
            }
        } catch (PolicyContextException ex) {
            logger.error(null, ex);
        }
        return authorizedCredentials;
    }

    /**
     * Gets a map of the properties associated with this Credential
     * 
     * @return
     */
    public Map<String, String> getPropertyMap() {
        return propertyMap;
    }

    /**
     * Gets a map of the properties associated with this Credential
     * 
     * @return
     */
    public String getIconUrl(String originalUrl) {
        if (this.iconUrlMap != null && this.iconUrlMap.containsKey(originalUrl)) {
            return this.iconUrlMap.get(originalUrl);
        }
        return originalUrl;
    }

    /**
     * Returns whether a credential has a certain permission.
     * 
     * @param permission - the name of the permission
     * @return Returns whether the Credential has the given permission.
     */
    public boolean hasPermission(String permission) {
        //TODO: call securityContext.hasPermission from the facelets instead of this method.
        return securityContext.hasPermission(permission);
    }

    /**
     * Attempts to direct the user away from the credential selection page 
     */
    @Lock(LockType.WRITE)
    public void trySkipSelection() {
        //if a credential is currently selected, no selection is required
        if (this.getCurrentCredential() != null) {
            redirectToRequestedPageOrStartPage(null, null);
            return;
        }

        List<AccountXCredentialXUser> validShoppingCredentials = getUserCredentials();

        if (validShoppingCredentials == null || validShoppingCredentials.isEmpty()) {
            //if there are no credentials, user is kicked out
            logger.error("Could not select credential, invalidating session.");
            loginBean.logout();
        }
        else if (validShoppingCredentials.size() == 1) {
            //if there is exactly one available credential, it is used, and no selection is required
            this.setCurrentAXCXU(validShoppingCredentials.get(0));
            redirectToRequestedPageOrStartPage(null, null);
            return;
        }
    }

    /**
     * Checks to see if the login is from a punchout event // and handles the redirection
     *
     * @throws CredentialSelectionException
     */
    public boolean isPunchoutSession() {
        return punchoutSession != null;
    }

    public boolean isExternalCatalog() {
        if (getCurrentCredential() != null) {
            return StringUtils.isNotBlank(getCurrentCredential().getVendorPunchoutUrl());
        }
        return false;
    }

    public boolean isShoppingCartDisplayed() {
        return !(this.isExternalCatalog() || this.currentAXCXU == null);
    }

    @Lock(LockType.WRITE)
    public void redirectToCredentialSelection(HttpServletRequest request, HttpServletResponse response) {
        try {
            redirect(request, response, "/ecommerce/login/credentialSelection.xhtml");
        }
        catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exception redirecting to credential selection, logging out", e);
            }
            //if there is an error redirecting the user
            loginBean.logout();
        }
    }

    @Lock(LockType.WRITE)
    public void redirectToChangePassword(HttpServletRequest request, HttpServletResponse response) {
        try {
            redirect(request, response, LinksBean.PASSWORD_EXPIRED_PAGE);
        }
        catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exception redirecting to change password, logging out", e);
            }
            //if there is an error redirecting the user
            loginBean.logout();
        }
    }

    @Lock(LockType.WRITE)
    public void redirectToRequestedPageOrStartPage() {
        redirectToRequestedPageOrStartPage(null, null);
    }

    /**
     * This method redirects the user to a page, either automatically if they have exactly one credential or 
     * when the "Continue" button is pressed.
     * <br /><br />
     * If the arrivalRequest is set, then that request is used to determine the redirect path; otherwise, they are 
     * sent to the home page.
     * 
     * @return
     */
    @Lock(LockType.WRITE)
    public void redirectToRequestedPageOrStartPage(HttpServletRequest request, HttpServletResponse response) {

        try {
            String view = null;
            if ((StringUtils.isNotBlank(this.arrivalRequestUrl) && !this.arrivalRequestUrl.contains("RES_NOT_FOUND"))) {
                if ((isExternalCatalog() && this.arrivalRequestUrl.startsWith("/ecommerce/marketplace/"))
                        || (!isExternalCatalog() && !this.arrivalRequestUrl.startsWith("/ecommerce/marketplace/"))) {

                    view = this.arrivalRequestUrl;
                    this.arrivalRequestUrl = null;
                }
                else {
                    view = getStartPage();
                }
            }
            else {
                view = getStartPage();
            }
            redirect(request, response, view);
        }
        catch (Exception e) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exception redirecting to requested page, logging out", e);
            }
            //if there is an error redirecting the user
            loginBean.logout();
        }
    }

    private void redirect(HttpServletRequest request, HttpServletResponse response, String view) throws IOException,
            PolicyContextException {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            ExternalContext context = facesContext.getExternalContext();
            context.redirect(context.getRequestContextPath() + view);
        }
        else {
            response.sendRedirect(request.getContextPath() + view);
        }
    }

    public String getStartPage() {
        if (isExternalCatalog()) {
            return "/ecommerce/marketplace/welcome.xhtml";
        }
        else {
            return "/ecommerce/home.xhtml";
        }
    }

    /**
     * Sets the URL that was requested before selecting a credential.
     * 
     * @param arrivalRequest - the request that the URL will be pulled from
     */
    public void setArrivalRequestUrl(HttpServletRequest arrivalRequest) {
        String url = arrivalRequest.getRequestURL().toString();
        url = url.substring(url.indexOf("/shopping") + 9);
        String queryString = arrivalRequest.getQueryString();
        this.arrivalRequestUrl = url + ((queryString == null) ? "" : ("?" + queryString));
    }

    /**
     * This converter is used to generate the dropdown for the credentialSelection.xhtml page.
     * 
     * @return
     */
    public Converter getCredentialConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return axcxuBp.getAXCXUForConverter(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AccountXCredentialXUser) value).getId());
            }
        };
    }

    public BigDecimal getMinimumOrderAmount() {
        final Credential currentCredential = getCurrentCredential();
        if (currentCredential != null) {
            for (CredentialXCredentialPropertyType property : currentCredential.getProperties()) {
                if (property.getType().getName().equals(MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue())) {
                    return new BigDecimal(property.getValue());
                }
            }
        }
        return null;
    }

    public BigDecimal getMinimumOrderFee() {
        final Credential currentCredential = getCurrentCredential();
        if (currentCredential != null) {
            for (CredentialXCredentialPropertyType property : currentCredential.getProperties()) {
                if (property.getType().getName().equals(MIN_ORDER_FEE.getValue())) {
                    return new BigDecimal(property.getValue());
                }
            }
        }
        return null;
    }

    public void loadPunchoutSession(String punchoutSessionToken) {
        PunchoutSession session = null;
        try {
            session = new PunchoutSession();
            session.setSessionToken(punchoutSessionToken);
            List<PunchoutSession> searchResultList = punchoutSessionBp.searchByExactExample(session, 0, 0);
            session = searchResultList.get(0);
        }
        catch (Exception ex) {
            if (logger.isDebugEnabled()) {
                logger.debug("Exception loading punchout session", ex);
            }
        }
        if (session != null) {
            punchoutSession = session;
            this.setCurrentAXCXU(punchoutSession.getAccntCredUser());
        }
    }

    public static String getDefaultBuyerCookie() {
        return EncryptionUtils.randomAlphanumeric(13);
    }

    public static PunchoutOrderOperation getDefaultPunchoutOperation() {
        return PunchoutOrderOperation.create;
    }

    public void setupExternalPunchoutSession() {
        if (this.currentAXCXU != null && this.isExternalCatalog()) {
            if (punchoutSession != null) {
                this.punchoutBuyerCookie = punchoutSession.getBuyerCookie();
                this.punchoutOperation = punchoutSession.getOperation();
            }
            else {
                String newBuyerCookie = getDefaultBuyerCookie();
                PunchoutOrderOperation newOperation = getDefaultPunchoutOperation();
                punchoutSessionBp.createPunchoutSession(newBuyerCookie, newOperation, this.currentAXCXU);
                this.punchoutBuyerCookie = newBuyerCookie;
                this.punchoutOperation = newOperation;
            }
        }
    }

    public PunchoutSession getPunchoutSession() {
        return punchoutSession;
    }

    public void setPunchoutSession(PunchoutSession punchoutSession) {
        this.punchoutSession = punchoutSession;
    }

    public void endCurrentPunchoutSession() {
        String buyerCookie = null;
        if (this.punchoutBuyerCookie != null) {
            buyerCookie = this.punchoutBuyerCookie;
        }
        if (StringUtils.isBlank(buyerCookie) && punchoutSession != null) {
            buyerCookie = punchoutSession.getBuyerCookie();
        }
        punchoutSessionBp.endBuyerCookieSessions(buyerCookie);
        punchoutBuyerCookie = null;
        punchoutOperation = null;
        punchoutSession = null;
    }

    public void endExternalPunchoutSession() {
        //Only ends the external punchout session if there is not also an inbound punchout session (shallow copy) 
        if (punchoutSession == null) {
            if (this.punchoutBuyerCookie != null) {
                punchoutSessionBp.endBuyerCookieSessions(punchoutBuyerCookie);
            }
            punchoutBuyerCookie = null;
            punchoutOperation = null;
        }
    }

    public String getPunchoutBuyerCookie() {
        return punchoutBuyerCookie;
    }

    public void setPunchoutBuyerCookie(String punchoutBuyerCookie) {
        this.punchoutBuyerCookie = punchoutBuyerCookie;
    }

    public PunchoutOrderOperation getPunchoutOperation() {
        return punchoutOperation;
    }

    public void setPunchoutOperation(PunchoutOrderOperation punchoutOperation) {
        this.punchoutOperation = punchoutOperation;
    }
}
