package com.apd.phoenix.shopping.camel.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeRequest;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.shopping.camel.routes.CxmlRequestTypeRouteBuilder;
import com.apd.phoenix.shopping.camel.routes.FulfillRequestTypeRouteBuilder;
import com.apd.phoenix.shopping.camel.service.api.CxmlService;

@Singleton
@Lock(LockType.READ)
public class CxmlServiceCamelImpl implements CxmlService {

    private static Logger LOGGER = LoggerFactory.getLogger(CxmlServiceCamelImpl.class);

    @Inject
    private CdiCamelContext camelContext;

    private ProducerTemplate producerTemplate;

    Properties commonProperties;

    @PostConstruct
    private void init() throws Exception {
        commonProperties = PropertiesLoader.getAsProperties("camel.cxml.integration");

        producerTemplate = camelContext.createProducerTemplate();

        FulfillRequestTypeRouteBuilder fulfillRequestTypeRouteBuilder = new FulfillRequestTypeRouteBuilder();
        fulfillRequestTypeRouteBuilder.setFulfillShipNoticeRequestSource(commonProperties
                .getProperty("fulfillShipNoticeRequestSource"));
        camelContext.addRoutes(fulfillRequestTypeRouteBuilder);

        CxmlRequestTypeRouteBuilder cxmlRequestTypeRouteBuilder = new CxmlRequestTypeRouteBuilder();
        cxmlRequestTypeRouteBuilder.setCxmlOrderRequestSource(commonProperties.getProperty("cxmlOrderRequestSource"));
        cxmlRequestTypeRouteBuilder.setCxmlOrderRequestOutput(commonProperties.getProperty("cxmlRequestOutput"));
        cxmlRequestTypeRouteBuilder.setCxmlPunchOutSetupRequestSource(commonProperties
                .getProperty("cxmlPunchOutSetupRequestSource"));
        cxmlRequestTypeRouteBuilder
                .setCxmlPunchOutSetupRequestOutput(commonProperties.getProperty("cxmlRequestOutput"));
        cxmlRequestTypeRouteBuilder.setCxmlPunchOutOrderMessageSource(commonProperties
                .getProperty("cxmlPunchOutOrderMessageSource"));
        cxmlRequestTypeRouteBuilder.setCxmlPunchOutOrderMessageOutput(commonProperties
                .getProperty("cxmlPunchOutOrderMessageOutput"));
        camelContext.addRoutes(cxmlRequestTypeRouteBuilder);
        camelContext.start();
    }

    @PreDestroy
    private void destroy() throws Exception {
        producerTemplate.stop();
        camelContext.stop();
    }

    @Override
    public com.apd.phoenix.service.integration.cxml.model.fulfill.Response processShipNoticeRequest(
            ShipNoticeRequest shipNoticeRequest, String rawMessage, String payloadId) {
    	Map<String, Object> headers = new HashMap<>();
    	headers.put("rawMessage", rawMessage);
    	headers.put("payloadId", payloadId);
    	//TODO:find sender id from credential
    	headers.put("senderId", "staples");
        return (com.apd.phoenix.service.integration.cxml.model.fulfill.Response) producerTemplate.requestBodyAndHeaders(
                commonProperties.getProperty("fulfillShipNoticeRequestSource"), shipNoticeRequest, headers);
    }

    @Override
    public com.apd.phoenix.service.integration.cxml.model.cxml.Response processOrderRequest(OrderRequest orderRequest,
            String rawMessage, String payloadId, Account account) {
    	Map<String, Object> headers = new HashMap<>();
    	headers.put("rawMessage", rawMessage);
    	headers.put("payloadId", payloadId);
        headers.put("account", account);
        return (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producerTemplate.requestBodyAndHeaders(
                commonProperties.getProperty("cxmlOrderRequestSource"), orderRequest, headers);
    }

    @Override
    public com.apd.phoenix.service.integration.cxml.model.cxml.Response processPunchoutSetupRequest(
            PunchOutSetupRequest punchoutSetupRequest, String rawMessage, String payloadId) {
    	Map<String, Object> headers = new HashMap<>();
    	headers.put("rawMessage", rawMessage);
    	headers.put("payloadId", payloadId);
        return (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producerTemplate.requestBodyAndHeaders(
                commonProperties.getProperty("cxmlPunchOutSetupRequestSource"), punchoutSetupRequest, headers);
    }

    @Override
    public String processPunchoutOrderMessage(
    		com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessage punchoutOrderMessage,
    		String rawMessage, String payloadId) {
    	Map<String, Object> headers = new HashMap<>();
    	headers.put("rawMessage", rawMessage);
    	headers.put("payloadId", payloadId);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("rawMessage: " + rawMessage);
        }
    	String quoteId = (String) producerTemplate.requestBodyAndHeaders(
                commonProperties.getProperty("cxmlPunchOutOrderMessageSource"), punchoutOrderMessage, headers);
    	
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("quoteId:" + quoteId);
        }
    	return quoteId;
    }
}
