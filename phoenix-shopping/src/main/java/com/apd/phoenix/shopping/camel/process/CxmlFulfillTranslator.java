package com.apd.phoenix.shopping.camel.process;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.cxml.model.fulfill.CarrierIdentifier;
import com.apd.phoenix.service.integration.cxml.model.fulfill.DocumentReference;
import com.apd.phoenix.service.integration.cxml.model.fulfill.OrderReference;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipControl;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeHeader;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeItem;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticePortion;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeRequest;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.persistence.jpa.LineItemDao;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;

public class CxmlFulfillTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(CxmlFulfillTranslator.class);

    @Inject
    MessageMetadataDao messageMetadataDao;

    @Inject
    LineItemDao lineItemDao;

    @Inject
    PoNumberDao poNumberDao;

    public List<AdvanceShipmentNoticeDto> fromShipNoticeToAdvShipNotice(@Header("senderId") String senderId, @Body ShipNoticeRequest request) throws Exception {
    	List<AdvanceShipmentNoticeDto> advanceShipmentNoticeDtos = new ArrayList<AdvanceShipmentNoticeDto>();
    	List<ShipNoticePortion> shipNoticePortions = request.getShipNoticePortion();
    	
    	for(ShipNoticePortion shipNoticePortion : shipNoticePortions) {
	        AdvanceShipmentNoticeDto advanceShipmentNoticeDto = new AdvanceShipmentNoticeDto();
	        List<ShipmentDto> shipmentDtos = new ArrayList<>();
	        ShipNoticeHeader shipNoticeHeader = request.getShipNoticeHeader();
	        
	        ShipmentDto shipmentDto = new ShipmentDto();
	       
	        if( request.getShipControl() == null || request.getShipControl().isEmpty()) {
	        	throw new Exception("Validation Error - No ship control");
	        }
	        //Only support one tracking number
	        ShipControl shipControl = request.getShipControl().get(0);
	        if(shipControl.getShipmentIdentifier() != null) {
	        	
	        	shipmentDto.setTrackingNumber(shipControl.getShipmentIdentifier().getvalue());
	        }
	        if(shipControl.getCarrierIdentifier() != null && !shipControl.getCarrierIdentifier().isEmpty()) {
	        	ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();
	        	for(CarrierIdentifier ci : shipControl.getCarrierIdentifier()) {
	        		if(ci.getDomain().equalsIgnoreCase("scac")) {
	        			shippingPartnerDto.setScacCode(ci.getDomain());
	        		}
	        		if(ci.getDomain().equalsIgnoreCase("companyName")) {
	        			shippingPartnerDto.setName(ci.getDomain());
	        		}
	        	}
	        	shipmentDto.setShippingPartnerDto(shippingPartnerDto);
	        }
	        
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
	        shipmentDto.setDeliveredTime(format.parse(shipNoticeHeader.getDeliveryDate()));
	
	        if (shipNoticeHeader.getShipmentID().isEmpty()) {
	            throw new Exception("Validation Error");
	        }
	        //Order mapping
	        OrderReference orderReference = shipNoticePortion.getOrderReference();
	        //Use exchange and transation id to lookup order 
	        if(orderReference.getDocumentReference() == null || orderReference.getDocumentReference().getPayloadID().isEmpty()) {
	        	throw new Exception("Validation Error - Missing document reference");
	        }
	        DocumentReference documentReference = orderReference.getDocumentReference();
	        
	        //Find apdPo
	        CustomerOrder customerOrder = messageMetadataDao.getCOByTransactionAndInterchangeId(orderReference.getOrderID(),senderId,documentReference.getPayloadID());
        
	        if(customerOrder == null) {
	        	throw new Exception("Validation Error - Customer order not found");
	        }
	        
	        advanceShipmentNoticeDto.setApdPo(customerOrder.getApdPo().getValue());
	        
	        //Items
	        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
	        
	        if(shipNoticePortion.getShipNoticeItem() == null || shipNoticePortion.getShipNoticeItem().isEmpty()) {
	        	//Hydrate the customer order lineItems
	        	LOG.info("Ship notice has no items therefore apply to all items");
	        	customerOrder.getItems().size();
	        	Set<LineItem> lineItems = customerOrder.getItems();
	        	for(LineItem lineItem : lineItems) {
	        		LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
		        	lineItemXShipmentDto.setQuantity(new BigInteger(lineItem.getQuantity().toString()));
		        	LineItemDto lineItemDto = new LineItemDto();
		        	lineItemDto.setManufacturerPartId(lineItem.getManufacturerPartId());
		        	lineItemDto.setApdSku(lineItem.getApdSku());
		        	lineItemDto.setBuyerPartNumber(lineItem.getCustomerSku().getValue());
		        	lineItemXShipmentDto.setLineItemDto(lineItemDto);
		        	shipmentDto.getLineItemXShipments().add(lineItemXShipmentDto);
	        	}
	        } 
	        else {
		        for(ShipNoticeItem shipNoticeItem : shipNoticePortion.getShipNoticeItem()) {
		        	if( shipNoticeItem.getQuantity() == null ) {
		        		throw new Exception("Validation Error - No quantity on item");
		        	}
		        	LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
		        	lineItemXShipmentDto.setQuantity(new BigInteger(shipNoticeItem.getQuantity()));
		        	LineItemDto lineItemDto = new LineItemDto();
		        	String lineNumber = shipNoticeItem.getLineNumber();
		        	lineItemDto.setLineNumber(Integer.parseInt(lineNumber));
		        	LineItem lineItem = lineItemDao.getLineItemByLineNumberAndApdPo(lineNumber, customerOrder.getApdPo().getValue());
		        	lineItemDto.setManufacturerPartId(lineItem.getManufacturerPartId());
		        	lineItemDto.setApdSku(lineItem.getApdSku());
		        	lineItemDto.setBuyerPartNumber(lineItem.getCustomerSku().getValue());
		        	lineItemXShipmentDto.setLineItemDto(lineItemDto);
		        	shipmentDto.getLineItemXShipments().add(lineItemXShipmentDto);
		        }
	        }
	        shipmentDtos.add(shipmentDto);
	        advanceShipmentNoticeDto.setShipmentDtos(shipmentDtos);
	        advanceShipmentNoticeDtos.add(advanceShipmentNoticeDto);
    	}

        return advanceShipmentNoticeDtos;
    }
}
