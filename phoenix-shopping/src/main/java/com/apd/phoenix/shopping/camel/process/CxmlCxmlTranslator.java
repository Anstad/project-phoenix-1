package com.apd.phoenix.shopping.camel.process;

import com.apd.phoenix.service.business.AccountBp;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.cxf.common.util.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CxmlConfigurationBp;
import com.apd.phoenix.service.business.OrderTypeBp;
import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.UnitOfMeasureBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.Address;
import com.apd.phoenix.service.integration.cxml.model.cxml.Classification;
import com.apd.phoenix.service.integration.cxml.model.cxml.Contact;
import com.apd.phoenix.service.integration.cxml.model.cxml.Description;
import com.apd.phoenix.service.integration.cxml.model.cxml.Extrinsic;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemDetail;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemIn;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemOut;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequestHeader;
import com.apd.phoenix.service.integration.cxml.model.cxml.PCard;
import com.apd.phoenix.service.integration.cxml.model.cxml.Payment;
import com.apd.phoenix.service.integration.cxml.model.cxml.Phone;
import com.apd.phoenix.service.integration.cxml.model.cxml.PostalAddress;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessage;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.Street;
import com.apd.phoenix.service.integration.cxml.model.cxml.SupplierPartAuxiliaryID;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.MessageMetadata.PunchoutType;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.OrderStatusDto;
import com.apd.phoenix.service.model.dto.OrderTypeDto;
import com.apd.phoenix.service.model.dto.PaymentInformationDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.TransactionDto.TransactionOperation;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.factory.DtoFactory;

@LocalBean
@Stateless
public class CxmlCxmlTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(CxmlCxmlTranslator.class);

    public static final String CXML_DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ss-hh:mm";
    public static final String EXPIRATION_FORMAT = "yyyy-MM-dd";
    public static final String OTHER_CXML_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    //skip the characters ", " in address parsing
    public static final int TWO_CHARACTERS = 2;
    //skip the characters ", NC" where NC is any state code in address parsing
    public static final int FOUR_CHARACTERS = 4;
    private static String[] format = new String[] { CXML_DATE_FORMAT, EXPIRATION_FORMAT, OTHER_CXML_FORMAT };

    @Inject
    PunchoutSessionBp punchoutSessionBp;

    @Inject
    UnitOfMeasureBp unitOfMeasureBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    AccountXCredentialXUserBp accountXCredentialXUserBp;

    @Inject
    OrderTypeBp orderTypeBp;

    @Inject
    CxmlConfigurationBp cxmlConfigurationBp;

    @Inject
    AccountBp accountBp;

    @Inject
    SystemUserBp systemUserBp;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void verifyAndSetupOrderRequest(Exchange exchange) {
        OrderRequest orderRequest = (OrderRequest) exchange.getIn().getBody();
        String quotedOrderNumber = getQuotedOrderNumber(orderRequest.getItemOut());
        CustomerOrder customerOrder = null;
        if (StringUtils.isNotEmpty(quotedOrderNumber)) {
            customerOrder = customerOrderBp.searchByApdPo(quotedOrderNumber);
        }

        //Detect adhoc orders
        ItemOut anyItem = orderRequest.getItemOut().get(0);
        if (StringUtils.isNotEmpty(anyItem.getIsAdHoc())
                && (Boolean.parseBoolean(anyItem.getIsAdHoc()) || "yes".equalsIgnoreCase(anyItem.getIsAdHoc()))) {
            LOG.info("Adhoc order detected");
            AccountXCredentialXUser explicitAccountXCredentialXUser = null;
            //Check for addressid
            if (orderRequest.getOrderRequestHeader().getShipTo() != null
                    && orderRequest.getOrderRequestHeader().getShipTo() != null
                    && orderRequest.getOrderRequestHeader().getShipTo().getAddress() != null
                    && StringUtils.isNotEmpty(orderRequest.getOrderRequestHeader().getShipTo().getAddress()
                            .getAddressID())) {
                String login = orderRequest.getOrderRequestHeader().getShipTo().getAddress().getAddressID();
                Account account = (Account) exchange.getIn().getHeader("account");
                CxmlConfiguration cxmlConfiguration = cxmlConfigurationBp.getCxmlConfigurationFromAccount(account);
                if (cxmlConfiguration != null && StringUtils.isNotBlank(cxmlConfiguration.getSystemUserPrefix())) {
                    String systemUserPrefix = cxmlConfiguration.getSystemUserPrefix();
                    login = systemUserPrefix + login;
                }
                List<SystemUser> systemUsers = systemUserBp.getSystemUserByLogin(login);
                if (systemUsers == null || systemUsers.size() == 0) {
                    List<Extrinsic> extrinsicList = orderRequest.getOrderRequestHeader().getExtrinsic();
                    for (Extrinsic extrinsic : extrinsicList) {
                        if ("Requesting Location Code".equals(extrinsic.getName())) {
                            if (extrinsic.getContent().size() > 0) {
                                login = (String) extrinsic.getContent().get(0);
                                login = "NOVANT-" + login;
                            }
                        }
                    }
                }
                List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(login);
                if (response != null && response.size() > 0) {
                    for (AccountXCredentialXUser cred : response) {
                        if (cred.getCredential().getPunchoutType() != null
                                && cred.getCredential().getPunchoutType().equals(PunchoutType.CXML)) {
                            explicitAccountXCredentialXUser = cred;
                        }
                    }
                }
            }
            if (explicitAccountXCredentialXUser == null) {
                LOG.error("No account/user found for order.");
            }

            OrderType adhocOrderType = orderTypeBp.getTypeFromValue("ADHOC");
            CustomerOrder newCustomerOrder = customerOrderBp.createEmptyOrder(explicitAccountXCredentialXUser,
                    adhocOrderType);
            exchange.getIn().setHeader("apdPo", newCustomerOrder.getApdPo().getValue());
        }
        //Invalid request
        else if (customerOrder == null) {
            LOG.error("Customer order not found, invalid order request.");
        }
        //Detect split orders, If a customer po number is already set then the first order has already come through.
        else if (customerOrder.getCustomerPo() != null
                && StringUtils.isNotEmpty(customerOrder.getCustomerPo().getValue())) {
            LOG.info("Split order detected");

            CustomerOrder newCustomerOrder = null;
            AccountXCredentialXUser explicitAccountXCredentialXUser = null;
            //Check for addressid
            if (orderRequest.getOrderRequestHeader().getShipTo() != null
                    && orderRequest.getOrderRequestHeader().getShipTo() != null
                    && orderRequest.getOrderRequestHeader().getShipTo().getAddress() != null
                    && StringUtils.isNotEmpty(orderRequest.getOrderRequestHeader().getShipTo().getAddress()
                            .getAddressID())) {
                String login = orderRequest.getOrderRequestHeader().getShipTo().getAddress().getAddressID();
                CxmlConfiguration cxmlConfiguration = cxmlConfigurationBp.getCxmlConfigurationFromAccount(customerOrder
                        .getAccount());
                if (cxmlConfiguration != null && StringUtils.isNotBlank(cxmlConfiguration.getSystemUserPrefix())) {
                    String systemUserPrefix = cxmlConfiguration.getSystemUserPrefix();
                    login = systemUserPrefix + login;
                }
                List<SystemUser> systemUsers = systemUserBp.getSystemUserByLogin(login);
                if (systemUsers == null || systemUsers.size() == 0) {
                    List<Extrinsic> extrinsicList = orderRequest.getOrderRequestHeader().getExtrinsic();
                    for (Extrinsic extrinsic : extrinsicList) {
                        if ("Requesting Location Code".equals(extrinsic.getName())) {
                            if (extrinsic.getContent().size() > 0) {
                                login = (String) extrinsic.getContent().get(0);
                                login = "NOVANT-" + login;
                            }
                        }
                    }
                }
                List<AccountXCredentialXUser> response = accountXCredentialXUserBp.getAllAXCXUByLogin(login);
                if (response != null && response.size() > 0) {
                    for (AccountXCredentialXUser cred : response) {
                        if (cred.getCredential().getPunchoutType() != null
                                && cred.getCredential().getPunchoutType().equals(PunchoutType.CXML)) {
                            explicitAccountXCredentialXUser = cred;
                        }
                    }
                }
                if (explicitAccountXCredentialXUser == null) {
                    LOG.info("No account/user found for ShipTo/AddressId, defaulting to account from previous order.");
                    newCustomerOrder = customerOrderBp.createEmptyOrder(customerOrder.getAccount(), customerOrder
                            .getCredential(), customerOrder.getUser(), orderTypeBp.getTypeFromValue("PUNCHOUT"));
                }
                else {
                    newCustomerOrder = customerOrderBp.createEmptyOrder(explicitAccountXCredentialXUser, orderTypeBp
                            .getTypeFromValue("PUNCHOUT"));
                }
            }
            exchange.getIn().setHeader("apdPo", newCustomerOrder.getApdPo().getValue());
        }
        //Regular order
        else {
            exchange.getIn().setHeader("apdPo", quotedOrderNumber);
        }

    }

    public PurchaseOrderDto fromOrderToPurchaseOrder(@Header("apdPo") String apdPo,
            @Header("payloadId") String payloadId, @Header("account") Account account, OrderRequest orderRequest)
            throws Exception {
        PurchaseOrderDto purchaseOrderDto = createPurchaseOrderDto(orderRequest, account, payloadId, apdPo);
        OrderRequestHeader orderRequestHeader = orderRequest.getOrderRequestHeader();

        if (orderRequestHeader.getOrderID().isEmpty()) {
            throw new Exception("Invalid request");
        }

        return purchaseOrderDto;
    }

    public void getOrderRequestApdPo(Exchange exchange) {
        OrderRequest orderRequest = (OrderRequest) exchange.getIn().getBody();
        String quotedOrderNumber = getQuotedOrderNumber(orderRequest.getItemOut());
        if (StringUtils.isNotBlank(quotedOrderNumber)) {
            exchange.getIn().setHeader("apdPo", quotedOrderNumber);
        }
        else {
            LOG.error("No quoted order number could be found in the request");
        }
    }

    public void getPunchOutOrderMessageApdPo(Exchange exchange) {
        PunchOutOrderMessage punchOutOrderMessage = (PunchOutOrderMessage) exchange.getIn().getBody();

        PunchoutSession punchoutSessionSearch = new PunchoutSession();
        punchoutSessionSearch.setBuyerCookie(punchOutOrderMessage.getBuyerCookie().getContent().get(0).toString());
        List<PunchoutSession> matchingPunchoutSessions = punchoutSessionBp.searchByExactExample(punchoutSessionSearch,
                0, 0);
        PunchoutSession punchoutSession;
        String quotedOrderNumber = "";
        if (!CollectionUtils.isEmpty(matchingPunchoutSessions)) {
            punchoutSession = matchingPunchoutSessions.get(0);
            if (punchoutSession.getCustomerOrder() != null) {
                quotedOrderNumber = punchoutSession.getCustomerOrder().getApdPo().getValue();
            }
        }
        if (StringUtils.isNotBlank(quotedOrderNumber)) {
            exchange.getIn().setHeader("apdPo", quotedOrderNumber);
        }
        else {
            LOG
                    .error("No quoted order number could be found for the punchout session referenced by the buyer cookie in the punchout order message");
        }
    }

    public static String getQuotedOrderNumber(List<ItemOut> items) {
        String quotedOrderNumber = null;
        if (items != null) {
            for (ItemOut item : items) {
                String supplierPartId = item.getItemID().getSupplierPartID();
                SupplierPartAuxiliaryID supplierPartAuxiliaryID = item.getItemID().getSupplierPartAuxiliaryID();

                if (supplierPartAuxiliaryID != null && !CollectionUtils.isEmpty(supplierPartAuxiliaryID.getContent())
                        && StringUtils.isNotBlank(supplierPartAuxiliaryID.getContent().get(0).toString())) {
                    String[] supplierPartAuxiliaryIdSegments = supplierPartAuxiliaryID.getContent().get(0).toString()
                            .split("\\|");
                    if (supplierPartAuxiliaryIdSegments != null && supplierPartAuxiliaryIdSegments.length > 0) {
                        quotedOrderNumber = supplierPartAuxiliaryIdSegments[(supplierPartAuxiliaryIdSegments.length - 1)];
                    }
                }
                if (StringUtils.isBlank(quotedOrderNumber)) {
                    String[] supplierPartIdSegments = supplierPartId.split("\\|");
                    if (supplierPartIdSegments != null && supplierPartIdSegments.length > 1) {
                        quotedOrderNumber = supplierPartIdSegments[1];
                    }
                }
                if (!StringUtils.isBlank(quotedOrderNumber)) {
                    return quotedOrderNumber;
                }
            }
        }
        return quotedOrderNumber;
    }

    public PunchOutSetupRequest fromPunchoutSetupToSomething(PunchOutSetupRequest punchOutSetupRequest) {
        //TODO:Change this function to map to the appropriate object
        return punchOutSetupRequest;
    }

    public PurchaseOrderDto fromPunchOutOrderMessageToPurchaseOrder(PunchOutOrderMessage punchOutOrderMessage) {

        PunchoutSession punchoutSessionSearch = new PunchoutSession();
        punchoutSessionSearch.setBuyerCookie(punchOutOrderMessage.getBuyerCookie().getContent().get(0).toString());
        List<PunchoutSession> matchingOpenPunchoutSessions = punchoutSessionBp
                .getOpenPunchoutSessions(punchoutSessionSearch);
        PunchoutSession punchoutSession;
        if (CollectionUtils.isEmpty(matchingOpenPunchoutSessions)) {
            return null;
        }
        else {
            punchoutSession = matchingOpenPunchoutSessions.get(0);
        }

        PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
        if (punchoutSession.getCustomerOrder() != null) {
            purchaseOrderDto.setApdPoNumber(punchoutSession.getCustomerOrder().getApdPo().getValue());
        }
        purchaseOrderDto
                .setOperationAllowed(punchOutOrderMessage.getPunchOutOrderMessageHeader().getOperationAllowed());

        // not needed for Staples incoming
        //punchOutOrderMessage.getPunchOutOrderMessageHeader().getQuoteStatus();

        //------ Shipping mapping
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getDescription().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getOriginalPrice().getMoney().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getOriginalPrice().getMoney().getCurrency();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getOriginalPrice().getType();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getModificationDetail().getDescription().getXmlLang();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getModificationDetail().getDescription().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getModificationDetail().getName();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getModificationDetail().getStartDate();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getModifications().getModification().get(0).getModificationDetail().getEndDate();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getMoney().getCurrency();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getMoney().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getTracking();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getTrackingDomain();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getTrackingId();
        if ((punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping() != null)
                && (punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipping().getMoney() != null)) {
            purchaseOrderDto.setEstimatedShippingAmount(new BigDecimal(punchOutOrderMessage
                    .getPunchOutOrderMessageHeader().getShipping().getMoney().getvalue()));
        }
        //------- ShipTo field mapping
        // Set<CashoutPageXField> orderMappingFields = punchoutSession.getAccntCredUser().getCredential().getCashoutPage().getPageXFields();
        // Not Included in Staples incoming
        //    	AddressDto shipToAddressDto = createAddress(punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipTo().getAddress(), orderMappingFields);
        //    	purchaseOrderDto.setShipToAddressDto(shipToAddressDto);
        //    	String street2Mapping = getMapping(orderMappingFields, CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping());
        //        final List<Street> streets = punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipTo().getAddress().getPostalAddress().getStreet();
        //        if(street2Mapping!=null){
        //            if(streets.size()>2){//is there a third street address line
        //                insertOrderDataByField(purchaseOrderDto, street2Mapping, streets.get(3).getvalue());
        //            }
        //        }
        //        String street3Mapping = getMapping(orderMappingFields, CashoutPageXFieldDto.CxmlMapping.STREET_3.getMapping());
        //        if(street3Mapping!=null){
        //            if(streets.size()>3){//is there a fourth street address line
        //                insertOrderDataByField(purchaseOrderDto, street3Mapping, streets.get(4).getvalue());
        //            }
        //        }
        //        String addressIdMapping = getMapping(orderMappingFields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID.getMapping());
        //        if(addressIdMapping!=null){
        //            insertOrderDataByField(purchaseOrderDto, addressIdMapping, punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipTo().getAddress().getAddressID());
        //        }
        //        String addressIdBase36Mapping = getMapping(orderMappingFields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID_BASE_36.getMapping());
        //        if(addressIdBase36Mapping!=null){
        //            insertOrderDataByField(purchaseOrderDto, street3Mapping, punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipTo().getAddress().getAddressID());
        //        }
        //        String carrierRoutingMapping = getMapping(orderMappingFields, "EXT:CarrierRouting");
        //        if(carrierRoutingMapping!=null){
        //            insertOrderDataByField(purchaseOrderDto, carrierRoutingMapping, punchOutOrderMessage.getPunchOutOrderMessageHeader().getShipTo().getCarrierIdentifier().getvalue());
        //        }

        // Not needed for Staples incoming
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getDescription().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getMoney().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getMoney().getCurrency();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getTaxDetail().get(0).getTaxAmount().getMoney().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getTaxDetail().get(0).getTaxAmount().getMoney().getCurrency();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getTaxDetail().get(0).getDescription().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getTaxDetail().get(0).getExemptDetail();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getExtrinsic().get(0).getName();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTax().getExtrinsic().get(0).getContent().get(0).toString();
        purchaseOrderDto.setCurrencyCode(punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getMoney()
                .getCurrency());
        purchaseOrderDto.setOrderTotal(new BigDecimal(punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal()
                .getMoney().getvalue()));

        // Not included in Staples incoming
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getOriginalPrice().getMoney().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getOriginalPrice().getMoney().getCurrency();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getOriginalPrice().getType();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getModificationDetail().getDescription().getXmlLang();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getModificationDetail().getDescription().getvalue();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getModificationDetail().getName();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getModificationDetail().getStartDate();
        //    	punchOutOrderMessage.getPunchOutOrderMessageHeader().getTotal().getModifications().getModification().get(0).getModificationDetail().getEndDate();
        //        if (punchOutOrderMessage.getPunchOutOrderMessageHeader().getSupplierOrderInfo() != null) {
        //        	PoNumberDto supplierPo = new PoNumberDto();
        //        	PoNumberTypeDto poNumberType = new PoNumberTypeDto();
        //        	poNumberType.setName(""/*no vendor/supplier poNumber Type so this may be an unnecessary mapping*/);
        //        }
        AccountDto account = DtoFactory.createAccountDto(punchoutSession.getAccntCredUser().getAccount());
        purchaseOrderDto.setAccount(account);
        purchaseOrderDto.setCredential(DtoFactory.createCredentialDto(punchoutSession.getAccntCredUser()
                .getCredential()));
        purchaseOrderDto.setSystemUserDto(DtoFactory.createSystemUserDto(punchoutSession.getAccntCredUser().getUser()));
        purchaseOrderDto.setItems(createLineItems(punchOutOrderMessage.getItemIn(), account));
        //TODO set account
        //
        // Not included in Staples incoming
        purchaseOrderDto.setOrderDate(new Date());
        OrderStatusDto status = new OrderStatusDto();
        status.setValue(OrderStatusEnum.QUOTED.getValue());
        purchaseOrderDto.setOrderStatus(status);
        return purchaseOrderDto;
    }

    private List<LineItemDto> createLineItems(List<ItemIn> punchInItems, AccountDto account) {

        List<LineItemDto> lineItems = new ArrayList<LineItemDto>();
        for (ItemIn punchInItem : punchInItems) {
            LineItemDto lineItem = new LineItemDto();
            //lineItem.setApdSku(punchInItem.getItemID().getSupplierPartID());
            lineItem.setSupplierPartId(punchInItem.getItemID().getSupplierPartID());
            lineItem.setSupplierPartAuxiliaryId(punchInItem.getItemID().getSupplierPartAuxiliaryID().getContent()
                    .get(0).toString());
            // Trim supplier part aux id setting empty value to null
            if (StringUtils.isNotBlank(lineItem.getSupplierPartAuxiliaryId())) {
                lineItem.setSupplierPartAuxiliaryId(StringUtils.trimToNull(lineItem.getSupplierPartAuxiliaryId()));
            }
            ItemDetail detail = punchInItem.getItemDetail();
            if (punchInItem.getLineNumber() != null) {
                lineItem.setCustomerLineNumber(punchInItem.getLineNumber());
                try {
                    lineItem.setLineNumber(Integer.valueOf(punchInItem.getLineNumber()));
                }
                catch (NumberFormatException e) {
                    LOG.error("lineItemDto.customerLineNumber was set, but lineItemDto.lineNumber was not because "
                            + "punch in item line number has non-numeric characters." + e.toString());
                }
            }
            lineItem.setUnitPrice(new BigDecimal(detail.getUnitPrice().getMoney().getvalue()));
            lineItem.setQuantity(new BigInteger(punchInItem.getQuantity()));
            String description = "";
            for (Description descriptionLine : detail.getDescription()) {
                description = description + descriptionLine.getvalue();
            }
            lineItem.setShortName(description); // line item short description
            lineItem.setDescription(description); // line item long description
            UnitOfMeasureDto vendorUom = new UnitOfMeasureDto();
            vendorUom.setName(detail.getUnitOfMeasure());
            vendorUom = unitOfMeasureBp.convertToCustomerUom(account, vendorUom);
            lineItem.setUnitOfMeasure(vendorUom);
            for (Classification classification : detail.getClassification()) {
                if ("unspsc".equalsIgnoreCase(classification.getDomain())) {
                    lineItem.setUnspscClassification(classification.getvalue());
                }
            }
            if (detail.getManufacturerName() != null) {
                lineItem.setManufacturerName(detail.getManufacturerName().getvalue());
            }
            lineItem.setManufacturerPartId(detail.getManufacturerPartID());
            if (punchInItem.getComments() != null) {
                lineItem.setPurchaseComment(punchInItem.getComments().getvalue());
            }
            if (punchInItem.getShipTo() != null) {
                LOG.error("Multiple shipto addresses per order functionality not supported.");
            }
            if (punchInItem.getBillTo() != null) {
                LOG.error("Multiple billto addresses per order functionality not supported.");
            }
            lineItems.add(lineItem);
        }
        return lineItems;
    }

    private AddressDto createAddress(final Address shipToAddress, Set<CashoutPageXField> fields) {
        final AddressDto shipToAddressDto = new AddressDto();
        shipToAddressDto.setCompanyName(shipToAddress.getName().getvalue());
        shipToAddressDto.setMiscShipToDto(new MiscShipToDto());
        if (shipToAddress.getEmail() != null) {
            shipToAddressDto.getMiscShipToDto().setOrderEmail(shipToAddress.getEmail().getvalue());
        }
        if (shipToAddress.getName() != null) {
            shipToAddressDto.getMiscShipToDto().setRequesterName(shipToAddress.getName().getvalue());
        }
        if (shipToAddress.getPhone() != null) {
            String areaOrCityCode = "";
            if (shipToAddress.getPhone().getTelephoneNumber().getAreaOrCityCode() != null) {
                areaOrCityCode = shipToAddress.getPhone().getTelephoneNumber().getAreaOrCityCode();
            }
            String number = "";
            if (shipToAddress.getPhone().getTelephoneNumber().getNumber() != null) {
                number = shipToAddress.getPhone().getTelephoneNumber().getNumber();
            }
            String ext = "";
            if (shipToAddress.getPhone().getTelephoneNumber().getExtension() != null) {
                ext = shipToAddress.getPhone().getTelephoneNumber().getExtension();
            }
            shipToAddressDto.getMiscShipToDto().setRequesterPhone(areaOrCityCode + number + ext);
        }
        final PostalAddress shipToPostalAddress = shipToAddress.getPostalAddress();
        switch (shipToPostalAddress.getDeliverTo().size()) {
            //Intentionally skipping break statement
            case 2:
                shipToAddressDto.getMiscShipToDto().setMailStop(shipToPostalAddress.getDeliverTo().get(1).getvalue());
            case 1:
                shipToAddressDto.getMiscShipToDto().setDeliverToName(
                        shipToPostalAddress.getDeliverTo().get(0).getvalue());
                shipToAddressDto.setName(shipToPostalAddress.getDeliverTo().get(0).getvalue());
            default:
                break;

        }
        switch (shipToPostalAddress.getStreet().size()) {
            //intentionally skipping of break statements
            case 4:
                shipToAddressDto.getMiscShipToDto().setStreet3(shipToPostalAddress.getStreet().get(3).getvalue());
            case 3:
                shipToAddressDto.getMiscShipToDto().setStreet2(shipToPostalAddress.getStreet().get(2).getvalue());
            case 2:
                shipToAddressDto.setLine2(shipToPostalAddress.getStreet().get(1).getvalue());
            case 1:
                shipToAddressDto.setLine1(shipToPostalAddress.getStreet().get(0).getvalue());
                break;
            default:
                break;
        }
        if (StringUtils.isNotBlank(shipToAddress.getAddressID())) {
            shipToAddressDto.getMiscShipToDto().setAddressId(shipToAddress.getAddressID());
        }
        shipToAddressDto.setCity(shipToPostalAddress.getCity());
        shipToAddressDto.setState(shipToPostalAddress.getState());
        shipToAddressDto.setZip(shipToPostalAddress.getPostalCode());
        shipToAddressDto.setCountry(shipToPostalAddress.getCountry().getIsoCountryCode());
        return shipToAddressDto;
    }

    private PurchaseOrderDto createPurchaseOrderDto(OrderRequest orderRequest, Account account, String payloadId, String apdPo) throws ParseException {

    	CustomerOrder orderEntity = customerOrderBp.searchByApdPo(apdPo);
        
        Credential credential = null;
        if(orderEntity!=null && orderEntity.getCredential()!=null){
            credential= orderEntity.getCredential();
        }else{
            LOG.error("Could not find credential");
            if(account.getCredentials().size()==1){                
                credential = account.getCredentials().iterator().next();                
            }
            else{
                LOG.error("Ambiguous or missing credential");
            }
        }
        Set<CashoutPageXField> fields = credential.getCashoutPage().getPageXFields();
        PurchaseOrderDto customerOrder = new PurchaseOrderDto();
        
        customerOrder.setOrderPayloadId(payloadId);
        customerOrder.setApdPoNumber(apdPo);
        //customerOrder.setCredential(credential);
        OrderStatusDto orderStatusDto = new OrderStatusDto();
        orderStatusDto.setValue(OrderStatusEnum.CREATED.getValue());
        customerOrder.setOrderStatus(orderStatusDto);
        if (customerOrder.getTransactionOperation() == null) {
    		customerOrder.setTransactionOperation(TransactionOperation.NEW);
    	}
        customerOrder.setUpdateEmptyOrder(true);
        customerOrder.setComments(new ArrayList<String>());

        final OrderRequestHeader orderRequestHeader = orderRequest.getOrderRequestHeader();
        customerOrder.setOrderDate(DateUtils.parseDate(orderRequestHeader.getOrderDate(), format));
        
        //FIXME The rest of the code in this method can be drastically simplified by breaking the mapping logic into 2 steps.
        //
        // (1).  Iterate through the collection of fields configured on the credential's cashout page, and for each field, get the value
        // from the CXML payload based on the input mapping for that field (if there is no input mapping, use a default input mapping
        // from the CXML payload to that field) and put the value in a map using the name of the field as the key.  For example:
        // Map<String,String> cxmlDataMap = new HashMap<String,String>();
        // for (CashoutPageXField field : credential.getCashoutPage().getPageXFields()) {
        //    String cxmlElementNameOrXpath = field.getInputMapping != null ? field.getInputMapping : getDefaultCxmlMapping(field.getName());
        //    String inputValue = null;
        //    if (inputMapping != null) {
        //        inputValue = getValueFromCxml(cxmlOrderRequest, cxmlElementNameOrXpath);
        //    }
        //    if (inputValue == null && field.isRequired()) {
        //        // Use default translation logic (if possible) to pull the value from the accountXCredentialXUser 
        //        // associated with the order
        //        inputValue = getValueFromAXCXU(axcxu, field.getName());
        //    }
        //    cxmlDataMap.put(field.getName(),inputValue);
        // }
        //
        //
        // (2).  Construct the canonical AddressDto and associated MishShipToDto POJO by setting each bean property
        // equal to the value of the entry in the field map whose key correlates with the bean property.  For example:
        // AddressDto shipToAddressDto = new AddressDto();
        // shipToAddressDto.setName(cxmlDataMap.get("ship name");
        // shipToAddressDto.setCompany(cxmlDataMap.get("ship company name");
        // shipToAddressDto.setMiscShipToDto(new MiscShipToDto());
        // shipToAddressDto.getMiscShipToDto().setDesktop(cxmlDataMap.get("Desktop"));
        // etc...
        AddressDto shipToAddressDto = null;
        if(orderRequestHeader.getShipTo()!=null && orderRequestHeader.getShipTo().getAddress()!=null){
            shipToAddressDto = createAddress(orderRequestHeader.getShipTo().getAddress(),fields);
        }else if(orderRequest.getItemOut().get(0).getShipTo()!=null && orderRequest.getItemOut().get(0).getShipTo().getAddress()!=null){
            shipToAddressDto = createAddress(orderRequest.getItemOut().get(0).getShipTo().getAddress(), fields);
        }else{
            LOG.error("No Address Specified on Inbound Order Request!");
        }
        CashoutPageXField street2Field = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.STREET_2.getMapping());
        final List<Street> streets = orderRequestHeader.getShipTo().getAddress().getPostalAddress().getStreet();
        if(street2Field!=null){
            if(streets.size()>2){//is there a third street address line
                insertOrderDataByField(customerOrder, street2Field, streets.get(3).getvalue());
            }
        }
        CashoutPageXField street3Field = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.STREET_3.getMapping());
        if(street3Field!=null){
            if(streets.size()>3){//is there a fourth street address line
                insertOrderDataByField(customerOrder, street3Field, streets.get(4).getvalue());
            }
        }
        
        CashoutPageXField addressIdField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID.getMapping());
        if(addressIdField!=null){
            insertOrderDataByField(customerOrder, addressIdField, orderRequestHeader.getShipTo().getAddress().getAddressID());
        }
        CashoutPageXField addressIdBase36Field = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.ADDRESS_ID_BASE_36.getMapping());
        if(addressIdBase36Field!=null){
            insertOrderDataByField(customerOrder, addressIdBase36Field, orderRequestHeader.getShipTo().getAddress().getAddressID());
        }
        customerOrder.setShipToAddressDto(shipToAddressDto);
        CashoutPageXField carrierRoutingField = getMappedField(fields, "EXT:CarrierRouting");
        if(carrierRoutingField!=null){
            insertOrderDataByField(customerOrder, carrierRoutingField, orderRequestHeader.getShipTo().getCarrierIdentifier().getvalue());
        }
        AddressDto billToAddress = createAddress(orderRequestHeader.getBillTo().getAddress(),fields);
        if(orderRequestHeader.getPayment()!=null){
            Payment payment = orderRequestHeader.getPayment();
            final PaymentInformationDto paymentInformationDto = new PaymentInformationDto();
            final PCard card = payment.getPCard();
            paymentInformationDto.setPaymentType(PaymentInformationDto.Type.CC.getValue());
            paymentInformationDto.setCardNumber(card.getNumber());  
            paymentInformationDto.setExpiration(DateUtils.parseDate(card.getExpiration(),format));
            if(StringUtils.isNotEmpty(card.getNumber()) && card.getNumber().length() > 3){
                //Identity is just last four digits of card number
                paymentInformationDto.setIdentity(card.getNumber().substring(card.getNumber().length()-4, card.getNumber().length()));
            }
            paymentInformationDto.setNameOnCard(card.getName());
            paymentInformationDto.setBillToAddress(billToAddress);
            customerOrder.setPaymentInformationDto(paymentInformationDto);
        } else {
        	PaymentInformationDto paymentInformationDto = new PaymentInformationDto();
        	paymentInformationDto.setPaymentType(PaymentInformationDto.Type.INVOICE.getValue());
        	paymentInformationDto.setBillToAddress(billToAddress);
        	customerOrder.setPaymentInformationDto(paymentInformationDto);
        }
        for(Extrinsic extrinsic:orderRequestHeader.getExtrinsic()){
            switch(extrinsic.getName()){
                case "Global Location Number":
                    shipToAddressDto.getMiscShipToDto().setGlNumber((String) extrinsic.getContent().get(0));
                    break;
                case "Requesting Location Code":
                    customerOrder.setAccountLocationCode((String) extrinsic.getContent().get(0));
                    break;
                /**
                 * Novant Specifies that they will include their ship to address in this location.
                 * Each ship to address has been guaranteed to have this format. 
                 */
                case "Requesting Location Address":
                    String rawAddress = (String) extrinsic.getContent().get(0);
                    String[] addressLines = rawAddress.trim().split("\n");
                    for(int i = 0; i<addressLines.length; i++){
                        String line = addressLines[i].trim();
                        //checks if the line is the last line
                        if((i+1) == addressLines.length){
                            //splits based on the format "CITY TWO_CHARACTER_STATE_CODE, ZIP"
                            final int indexOfComma = line.indexOf(",");
                            String city = line.substring(0, indexOfComma);
                            String state = line.substring(indexOfComma + TWO_CHARACTERS, indexOfComma + FOUR_CHARACTERS);
                            String zip = line.substring(indexOfComma+FOUR_CHARACTERS, line.length()).trim();
                            shipToAddressDto.setCity(city);
                            shipToAddressDto.setState(state);
                            shipToAddressDto.setZip(zip);
                        }
                        else{
                            //The address comes in as a series of lines followed by the city/zip informations
                            switch(i){
                                case 0:
                                    shipToAddressDto.setLine1(line);
                                    break;
                                case 1:
                                    shipToAddressDto.setLine2(line);
                                    shipToAddressDto.getMiscShipToDto().setStreet2(line);
                                    break;
                                case 2:
                                    shipToAddressDto.getMiscShipToDto().setStreet3(line);
                                    break;
                                default:
                                    LOG.error("Unexpected Requesting Location address line " + 
                                            Integer.toBinaryString(i+1) + ": " + line);
                                    break;                                        
                            }
                        }
                    }
                    break;
                default:
                    boolean recognizedExtrinsic = false;
                    for(CashoutPageXField field:fields){
                        //checks to see if portion after EXT: matches the extrinsic name
                        if(field.getInboundMapping() != null){
                            if(hasMappingForField(field, extrinsic)){
                                recognizedExtrinsic = true;
                                insertOrderDataByField(customerOrder, field, (String) extrinsic.getContent().get(0));
                                break;
                            }
                        }
                    }
                    if(recognizedExtrinsic){
                        break;
                    }
                    CashoutPageXField requesterPhoneField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.REQUESTER_PHONE.getMapping());
                    if(requesterPhoneField!=null){
                        recognizedExtrinsic = true;
                        insertOrderDataByField(customerOrder, requesterPhoneField,  (String) extrinsic.getContent().get(0));
                    }
                    if(!recognizedExtrinsic){
                        String comment = extrinsic.getName()+ ": ";
                        if(extrinsic.getContent().size()==1){
                            comment = comment + extrinsic.getContent().get(0).toString();
                        }
                        customerOrder.getComments().add(comment);
                        break;
                    }
            }
        }
        
        CashoutPageXField headerCommentField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.HEADER_COMMENTS.getMapping());
        if(headerCommentField != null){
            insertOrderDataByField(customerOrder, headerCommentField, orderRequestHeader.getComments().getvalue());
        }
        
        customerOrder.setCustomerPoNumber(orderRequestHeader.getOrderID());
        
        for(Contact contact:orderRequestHeader.getContact()){            
            String phoneNumber = "";
            if(contact.getPhone()!=null){
                for(Phone phone:contact.getPhone()){
                    phoneNumber = phoneNumber + " " + phone.getTelephoneNumber().getAreaOrCityCode() + phone.getTelephoneNumber().getNumber();
                    if(!StringUtils.isEmpty(phone.getTelephoneNumber().getExtension())){
                        phoneNumber=phoneNumber+"extension:"+phone.getTelephoneNumber().getExtension();
                    }
                    
                }
            }
            switch(contact.getRole()){
                case "endUser":
                    if(StringUtils.isEmpty(customerOrder.getShipToAddressDto().getMiscShipToDto().getDeliverToName())){
                        customerOrder.getShipToAddressDto().getMiscShipToDto().setDeliverToName(contact.getName().getvalue());
                    }
                    if(StringUtils.isEmpty(customerOrder.getShipToAddressDto().getMiscShipToDto().getDeliverToPhoneNumber())){
                        customerOrder.getShipToAddressDto().getMiscShipToDto().setDeliverToPhoneNumber(phoneNumber);
                    }
                    break;
                default:
                    if(StringUtils.isEmpty(customerOrder.getShipToAddressDto().getMiscShipToDto().getRequesterName())){
                        customerOrder.getShipToAddressDto().getMiscShipToDto().setRequesterName(contact.getName().getvalue());
                    }
                    if(StringUtils.isEmpty(customerOrder.getShipToAddressDto().getMiscShipToDto().getRequesterPhone())){
                        customerOrder.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(phoneNumber);
                    }
                    break;
            }
            
            CashoutPageXField contactNameField = getMappedField(fields,CashoutPageXFieldDto.CxmlMapping.CONTACT_NAME.getMapping());
            if(contactNameField!=null){
                insertOrderDataByField(customerOrder, contactNameField, contact.getName().getvalue());
            }
            CashoutPageXField emailField = getMappedField(fields, CashoutPageXFieldDto.CxmlMapping.ORDER_EMAIL.getMapping());
            if(emailField!=null){
                insertOrderDataByField(customerOrder, emailField, contact.getEmail().get(0).getvalue());
            }
            //Users specifying Contact Name and/or Order Email mappoings must only specify one contact
            if((contactNameField!=null)||(emailField!=null)){
                if(orderRequestHeader.getContact().size()>1){
                    LOG.error("Ambiguous contact information mapping.");
                }
            }
        }
        if(orderRequestHeader.getComments()!=null){
            customerOrder.getComments().add(orderRequestHeader.getComments().getvalue());
        }
        customerOrder.setItems(new ArrayList<LineItemDto>());
        if((orderRequestHeader.getShipping() != null) && (orderRequestHeader.getShipping().getMoney() != null)){
            customerOrder.setEstimatedShippingAmount(new BigDecimal(orderRequestHeader.getShipping().getMoney().getvalue()));
        }
        for(ItemOut xmlItem : orderRequest.getItemOut()){
            LineItemDto lineItem = new LineItemDto();
            lineItem.setCustomerComments(new ArrayList<String>());
            //will throw error if attempt to set noninteger value, but allows ".00" in string
            lineItem.setQuantity(new BigDecimal(xmlItem.getQuantity()).toBigIntegerExact());

            String supplierPartID = xmlItem.getItemID().getSupplierPartID();
            if(supplierPartID != null){
            	lineItem.setSupplierPartId( supplierPartID );
            }
            SupplierPartAuxiliaryID itemSuppAuxId = xmlItem.getItemID().getSupplierPartAuxiliaryID();
            if(itemSuppAuxId != null && itemSuppAuxId.getContent() != null && itemSuppAuxId.getContent().size() > 0) {
        		String itemSuppAuxIdValue = (String)itemSuppAuxId.getContent().get(0);
        		//Strip last element
        		itemSuppAuxIdValue = itemSuppAuxIdValue.replaceFirst("\\|[^|]*$", "");
        		lineItem.setSupplierPartAuxiliaryId(itemSuppAuxIdValue);
            }
            lineItem.setCustomerLineNumber(xmlItem.getLineNumber());
            ItemDetail detail = (ItemDetail) xmlItem.getItemDetailOrBlanketItemDetail().get(0);
            lineItem.setUnitPrice(new BigDecimal(detail.getUnitPrice().getMoney().getvalue()));
            String description = "";
            for(Description descriptionLine : detail.getDescription()){
                description = description + descriptionLine.getvalue();
            }
            lineItem.setShortName(description); // line item short description
            lineItem.setDescription(description);
            //NOTE: do not convert UOM here (currently conversion is only occurring between APD and vendor for certain customer accounts)
            final UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
            unitOfMeasureDto.setName(detail.getUnitOfMeasure());
            lineItem.setUnitOfMeasure(unitOfMeasureDto);
            for(Classification classification:detail.getClassification()){
                if("UNSPSC".equalsIgnoreCase(classification.getDomain())){
                    lineItem.setUnspscClassification(classification.getvalue());
                }
            }
            if(detail.getManufacturerName()!=null){
                lineItem.setManufacturerName(detail.getManufacturerName().getvalue());
            }
            lineItem.setManufacturerPartId(detail.getManufacturerPartID());
            if(xmlItem.getComments()!=null){
                lineItem.setPurchaseComment(xmlItem.getComments().getvalue());
            }
            if(xmlItem.getShipTo()!=null){
                LOG.warn("Multiple addresses per order functionality not supported.");
            }
            LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
            lineItemStatusDto.setValue(LineItemStatus.LineItemStatusEnum.CREATED.getValue());
            lineItem.setStatus(lineItemStatusDto);
            ItemDto itemDto = new ItemDto();
            ArrayList<SkuDto> skus = new ArrayList<>();
            if(lineItem.getSupplierPartId()!=null){
            	
                SkuDto apdSku = new SkuDto();
                SkuTypeDto skuTypeDto = new SkuTypeDto();
                skuTypeDto.setName(SkuTypeBp.SKUTYPE_APD);
                apdSku.setType(skuTypeDto);
                apdSku.setValue(lineItem.getSupplierPartId());
                skus.add(apdSku);
            }
            itemDto.setSkus(skus);
            lineItem.setItem(itemDto);
            lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
            if(detail.getExtrinsic()!=null){
                for(Extrinsic extrinsic:detail.getExtrinsic()){
                    if(extrinsic.getContent()!=null && extrinsic.getContent().size()>0){
                    	CashoutPageXField extrinsicMatchingField = getMappedField(fields, extrinsic.getName());
                        if(extrinsicMatchingField != null){
                            insertItemDataByField(lineItem, extrinsicMatchingField, 
                                    (String) extrinsic.getContent().get(0));
                        }else{
                            lineItem.getCustomerComments().add(
                                    extrinsic.getName()+": " + ((String) extrinsic.getContent().get(0)));
                        }
                    }
                }
            }
            customerOrder.getItems().add(lineItem);         
        }
        return customerOrder;
    }

    public void insertItemDataByField(LineItemDto item, CashoutPageXField field, String value) {
        if (field != null) {
            switch (field.getField().getName()) {
                case "Manufacturer Part Number":
                    if (!StringUtils.isEmpty(item.getManufacturerPartId())
                            && !item.getManufacturerPartId().equals(value)) {
                        LOG.error("Ambiguous manufacturer part id, " + item.getManufacturerPartId() + ", " + value);
                    }
                    item.setManufacturerPartId(value);
                    break;
                case "Customer Sku":
                    SkuDto customerSku = new SkuDto();
                    SkuTypeDto skuTypeDto = new SkuTypeDto();
                    skuTypeDto.setName(SkuTypeBp.SKUTYPE_CUSTOMER);
                    customerSku.setType(skuTypeDto);
                    customerSku.setValue(value);
                    item.setCustomerSku(customerSku);
                    break;
                case "Manufacturer":
                    item.setManufacturerName(value);
                    break;
            }
        }
    }

    public void insertOrderDataByField(PurchaseOrderDto order, CashoutPageXField field, String value)
            throws ParseException {
        if (field != null) {
            if (field.getIncludeLabelInMapping() != null && field.getIncludeLabelInMapping()) {
                value = field.getLabel() + "|" + value;
            }
            switch (field.getField().getName()) {
                case "ship date":
                    order.setShipDate(DateUtils.parseDate(value, format));
                    break;
                case "delivery date":
                    order.setDeliveryDate(DateUtils.parseDate(value, format));
                    break;
                case "cancel date":
                    order.setCancelDate(DateUtils.parseDate(value, format));
                    break;
                case "existing shipto address selection":
                case "existing billto address selection":
                case "line item comments":
                    LOG.error("Invalid Field:" + field + ". Expected singular result");
                    break;
                case "custom po number":
                    final PoNumberDto poNumberDto = new PoNumberDto();
                    poNumberDto.setType("customer");
                    poNumberDto.setValue(value);
                    order.getPoNumbers().add(poNumberDto);
                    break;
                case "blanket po number":
                    final PoNumberDto blanketPoNumberDto = new PoNumberDto();
                    blanketPoNumberDto.setType("blanket");
                    blanketPoNumberDto.setValue(value);
                    order.getPoNumbers().add(blanketPoNumberDto);
                    break;
                case "credit card name on card":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    order.getPaymentInformationDto().setNameOnCard(value);
                    break;
                case "credit card number":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    order.getPaymentInformationDto().setCardNumber(value);
                    break;
                case "credit card expiration":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    order.getPaymentInformationDto().setExpiration(
                            CardInformation.addExpirationMargin(DateUtils.parseDate(value, format)));
                    break;
                case "payment type selection":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    order.getPaymentInformationDto().setPaymentType(value);
                    break;
                case "bill name":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setName(value);
                    break;
                case "bill company":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setCompanyName(value);
                    break;
                case "bill address 1":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setLine1(value);
                    break;
                case "bill address 2":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setLine2(value);
                    break;
                case "bill city":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setCity(value);
                    break;
                case "bill zip":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setZip(value);
                    break;
                case "bill states":
                case "bill contiguos states":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setState(value);
                    break;
                case "bill north america":
                case "bill us only":
                    if (order.getPaymentInformationDto() == null) {
                        order.setPaymentInformationDto(new PaymentInformationDto());
                    }
                    if (order.getPaymentInformationDto().getBillToAddress() == null) {
                        order.getPaymentInformationDto().setBillToAddress(new AddressDto());
                    }
                    order.getPaymentInformationDto().getBillToAddress().setCountry(value);
                    break;
                case "comments":
                    if (order.getComments() == null) {
                        order.setComments(new ArrayList<String>());
                    }
                    order.getComments().add(value);
                    break;
                case "order type":
                    if (order.getOrderType() == null) {
                        order.setOrderType(new OrderTypeDto());
                    }
                    order.getOrderType().setValue(value);
                    break;
                case "ship name":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setName(value);
                    break;
                case "ship company":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setCompanyName(value);
                    break;
                case "ship address 1":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setLine1(value);
                    break;
                case "ship address 2":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setLine2(value);
                    break;
                case "ship city":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setCity(value);
                    break;
                case "ship zip":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setZip(value);
                    break;
                case "ship contiguous states":
                case "ship states":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setState(value);
                    break;
                case "ship us and canada":
                case "ship us only":
                case "ship north america":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    order.getShipToAddressDto().setCountry(value);
                    break;
                case "desktop":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setDesktop(value);
                    break;
                case "Desktop":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setDesktop(value);
                    break;
                case "building or department":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setDepartment(value);
                    break;
                case "cost center":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setCostCenter(value);
                    break;
                case "shipping method":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setShippingMethod(value);
                    break;
                case "email":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setOrderEmail(value);
                    break;
                case "AddressID":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setAddressId(value);
                    break;
                case "AddressID(base 36)":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setAddressId36(value);
                    break;
                case "Contact Name":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setContactName(value);
                    break;
                case "EXT:APD_InterchangeID":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setApdInterchangeId(value);
                    break;
                case "EXT:CarrierRouting":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setCarrierRouting(value);
                    break;
                case "EXT:DeliveryDate":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setDeliveryDate(value);
                    break;
                case "EXT:Dept":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setDept(value);
                    break;
                case "EXT:Facility":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setFacility(value);
                    break;
                case "EXT:Fedex No":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setFedexNumber(value);
                    break;
                case "EXT:LocationID":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setLocationId(value);
                    break;
                case "EXT:Mailstop":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setMailStop(value);
                    break;
                case "EXT:NOWL":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setNowl(value);
                    break;
                case "EXT:POL":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setPol(value);
                    break;
                case "EXT:SolomonID":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setSolomonId(value);
                    break;
                case "EXT:USAccount":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setUsAccount(value);
                    break;
                case "Header Comments":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setHeaderComments(value);
                    break;
                case "OldRelease":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setOldRelease(value);
                    break;
                case "Order Email":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setOrderEmail(value);
                    break;
                case "Street 2":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setStreet2(value);
                    break;
                case "Street 3":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setStreet3(value);
                    break;
                case "Requester Name":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setRequesterName(value);
                    break;
                case "Requester Phone":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(value);
                    break;
                case "accountLocationCode":
                    if (order.getShipToAddressDto() == null) {
                        order.setShipToAddressDto(new AddressDto());
                    }
                    if (order.getShipToAddressDto().getMiscShipToDto() == null) {
                        order.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    }
                    order.setAccountLocationCode(value);
                    break;
                default:
                    LOG.error("Could not recognize field:" + field);
            }
        }
    }

    private CashoutPageXField getMappedField(Set<CashoutPageXField> cashoutFields, String mapping) {
        CashoutPageXField mappedField = null;
        for (CashoutPageXField field : cashoutFields) {
            if (field.getInboundMapping() != null && field.getInboundMapping().equals(mapping)) {
                mappedField = field;
                break;
            }
        }
        return mappedField;
    }

    private boolean hasMappingForField(CashoutPageXField field, Extrinsic extrinsic) {
        final boolean matchesFieldThatStartsWithExt = field.getInboundMapping().length() > 5
                && extrinsic.getName().equalsIgnoreCase(
                        field.getInboundMapping().substring(5, field.getInboundMapping().length()));
        final boolean matchesInboundMapping = extrinsic.getName().equalsIgnoreCase(field.getInboundMapping());
        return matchesFieldThatStartsWithExt || matchesInboundMapping;
    }
}
