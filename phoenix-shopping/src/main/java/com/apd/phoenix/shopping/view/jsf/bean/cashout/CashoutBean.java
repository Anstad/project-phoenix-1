package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.UnitOfMeasureBp;
import com.apd.phoenix.service.business.UnitOfMeasureBp.UnitOfMeasureConversion;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.utility.AbstractPropertyUtils;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.apd.phoenix.web.cashout.AbstractCashoutBean;

/**
 * This class is divided into three parts.
 * 
 * The first part is the @PostConstruct method, and related methods. This initializes the Cashout bean, setting values that will 
 * not change unless the credential is changed (ie the session ends).
 * 
 * The second part starts with the retrieve() method, and related methods. This method is called when the user arrives at the 
 * cashout page. It makes sure the order is persisted, and populates the order with the contents of the cart.
 * 
 * Finally, there's the part starting with checkout(). This updates the order status, calculates the tax, and places the order.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@ConversationScoped
public class CashoutBean extends AbstractCashoutBean {

    private static final long serialVersionUID = -777621104522227062L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CashoutBean.class);

    private static final String MISSING_PUNCHOUT_CONTINUE_SHOPPING_LABEL = "Shop Again";

    private static final String MISSING_PUNCHOUT_EDIT_LABEL = "Edit Order";

    private static final String MISSING_PUNCHOUT_RETURN_CART_LABEL = "Return Cart";

    public static final String MINIMUM_ORDER_FEE_SKU = "min_order_fee";

    private static final String QUOTEID_REQUEST_PARAMETER = "quoteId";

    @Inject
    private CredentialSelectionBean credBean;

    @Inject
    private CatalogXItemBp catalogXitemBp;

    @Inject
    private SearchUtilities searchUtils;

    @Inject
    private ShoppingCartBean cartBean;

    @Inject
    private CurrentOrderBean currentOrderBean;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private ConfirmationBean confirmationBean;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private LinksBean linksBean;

    @Inject
    private UnitOfMeasureBp unitOfmeasureBp;

    @Override
    public void retrieve() {
        super.retrieve();
        if (cartBean.getCartList().isEmpty() && this.currentOrder.getItems().isEmpty()) {
            try {
                LOGGER.info("User attempted to navigate to cashout with an empty cart.");
                FacesContext.getCurrentInstance().getExternalContext().redirect(linksBean.getHome());
            }
            catch (IOException e) {
                LOGGER.warn("Unable to redirect user from cashout after force-navigate with empty cart", e);
            }
        }
    }

    @Override
    protected AccountXCredentialXUser getCurrentAXCXU() {
        return credBean.getCurrentAXCXU();
    }

    protected void generateLineItems() {
        //gets the rate to charge for shipping, based on the "shipping rate" on the Credential property
        BigDecimal shipRate = BigDecimal.ZERO;
        String displayedShipping = credBean.getPropertyMap().get(SHIPPING_RATE.getValue());
        if (displayedShipping != null) {
            try {
                shipRate = new BigDecimal(displayedShipping);
            }
            catch (NumberFormatException e) {
                LOGGER.warn("Error in parsing the ship rate", e);
            }
        }

        //generates the LineItems to be placed on the Order
        for (Product product : cartBean.getCartList()) {

            CatalogXItem catalogXItem = searchUtils.getFromProduct(product);
            LineItem lineItem = catalogXitemBp.getFromCatalogXItem(catalogXItem);
            //Set the promised prices of item
            if (currentOrderBean.getPromisedPrices().get(catalogXItem) != null) {
                lineItem.setUnitPrice(currentOrderBean.getPromisedPrices().get(catalogXItem));
            }
            //temporarily sets the tax to zero. it will be calculated when the order is placed
            lineItem.setMaximumTaxToCharge(BigDecimal.ZERO);
            if (cartBean.getSubsToOriginals().keySet().contains(product)) {
                Product originalSelected = cartBean.getSubsToOriginals().get(product);
                lineItem.setOriginallyAddedSku(originalSelected.getCustomerSku());
                lineItem.setOriginallyAddedName(originalSelected.getName());
            }
            this.addLineItem(lineItem, cartBean.getCartQuantity().get(product), shipRate);

        }
    }

    @Override
    protected PropertiesConfiguration getProperties() {
        return EcommercePropertiesLoader.getInstance().getEcommerceProperties();
    }

    /**
     * This method is called when the checkout page is loaded; it creates and persists the order, in the "Quoted" 
     * status.
     */
    protected void createOrder() {
        // This request param will be set when returning cart from external punchout site
        String quoteID = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                .getParameter(QUOTEID_REQUEST_PARAMETER);
        if (StringUtils.isNotBlank(quoteID) && customerOrderBp.searchByApdPo(quoteID) != null) {
            credentialSelectionBean.endExternalPunchoutSession();
            currentOrder = customerOrderBp.hydrateForOrderDetails(customerOrderBp.searchByApdPo(quoteID));
            addAdditionalCosts();
        }
        else {
            if (StringUtils.isNotBlank(quoteID)) {
                LOGGER.error("Was unable to find a quoted order with APD PO " + quoteID + " so creating a new one.");
            }
            // For punchout orders where the shopping happened in our site, set the current order to the punchout order and add the cart items.
            if (credentialSelectionBean.isPunchoutSession()) {
                currentOrder = customerOrderBp.hydrateForOrderDetails(credentialSelectionBean.getPunchoutSession()
                        .getCustomerOrder());
                removeLineItems();
                populateFromCart();

            }
            else {
                //checks to see if cashout page has been reached previously with this order
                //if set from the order details page, then the items that the order is populated with 
                //were the items previously on the order, which have been added to the cart.
                CustomerOrder existingOrder = null;
                if (currentOrderBean.getCurrentOrder() != null && currentOrderBean.getCurrentOrder().getId() != null) {
                    existingOrder = customerOrderBp.findById(currentOrderBean.getCurrentOrder().getId(),
                            CustomerOrder.class);
                }
                if (existingOrder != null) {
                    currentOrder = customerOrderBp.hydrateForOrderDetails(existingOrder);
                    if (currentOrderBean.getShouldOverrideWithCart()) {
                        removeLineItems();
                        populateFromCart();
                    }
                }
                //otherwise, this is the first time hitting the cashout page, with a new order. Generates it.
                else {
                    createEmptyOrder();
                    populateFromCart();
                }
            }
            currentOrder = customerOrderBp.hydrateForOrderDetails(customerOrderBp.update(currentOrder));
        }
        if (UnitOfMeasureBp.doesAccountRequireUomConversion(currentOrder.getAccount().getApdAssignedAccountId())) {
            for (LineItem item : currentOrder.getItems()) {
                UnitOfMeasureDto newUom = unitOfmeasureBp.convertUom(DtoFactory.createUnitOfMeasureDto(item
                        .getUnitOfMeasure()), UnitOfMeasureConversion.STAPLES_TO_NGCHII);
                item.setUnitOfMeasure(unitOfmeasureBp.getByName(newUom.getName()));
            }
            customerOrderBp.update(currentOrder);
        }
        if (credentialSelectionBean.isPunchoutSession()) {
            credentialSelectionBean.getPunchoutSession().setCustomerOrder(currentOrder);
        }
        currentOrderBean.setCurrentOrder(currentOrder);
    }

    private void removeLineItems() {
        currentOrder.getItems().clear();
        currentOrder = customerOrderBp.update(currentOrder);
    }

    private void populateFromCart() {
        generateLineItems();
        addAdditionalCosts();

        //Sets the individual line item comment for the order to the default value provided
        if (this.getPageValues().getValue().get(this.getPageValues().getField().get("line item comments")) != null) {
            for (LineItem item : currentOrder.getItems()) {
                this.getPageValues().getComment().put(
                        item,
                        this.getPageValues().getValue().get(this.getPageValues().getField().get("line item comments"))
                                .toString());
            }
        }

        //Calculates subtotals
        currentOrder.refreshOrderTotal();
    }

    /**
     * This method adds the list of additional costs (minimum order, etc) to the order as LineItems
     */
    private void addAdditionalCosts() {
        List<LineItem> additionalCosts = customerOrderBp.getOrderFees(this.currentOrder);
        for (LineItem item : additionalCosts) {
            if (item.getUnitPrice().compareTo(BigDecimal.ZERO) > 0) {
                addLineItem(item, 1, BigDecimal.ZERO);
            }
        }
    }

    @Override
    protected void prePlaceOrder() throws OrderPlaceException {
        if (!this.isPlaceOrderAllowed()) {
            throw new OrderPlaceException();
        }
        cartBean.clearCart();
    }

    @Override
    protected String postPlaceOrder() {
        confirmationBean.setCurrentOrder(currentOrder);
        confirmationBean.setCashoutPageContainer(this.getPageValues());
        currentOrderBean.setCurrentOrder(null);
        return "confirmation?faces-redirect=true";
    }

    @Override
    protected Map<String, String> credentialPropertiesMap() {
        return this.credBean.getPropertyMap();
    }

    @Override
    protected String orderTypeValue() {
        return "URL";
    }

    public String generateContinueLink() {
        if (credentialSelectionBean.isPunchoutSession()) {
            return linksBean.getNewPunchout();
        }
        return linksBean.getHome();
    }

    public String generatePunchoutContinueLabel() {
        String label = credentialSelectionBean.getPropertyMap().get("punchoutContinueShoppinglabel");
        return StringUtils.isNotEmpty(label) ? label : MISSING_PUNCHOUT_CONTINUE_SHOPPING_LABEL;
    }

    public String generatePunchoutEditLabel() {
        String label = credentialSelectionBean.getPropertyMap().get("punchoutEditlabel");
        return StringUtils.isNotEmpty(label) ? label : MISSING_PUNCHOUT_EDIT_LABEL;
    }

    public boolean isPlaceOrderAllowed() {

        // If punchout order, first check the allowed operation on the punchout session/cart
        if (this.getCurrentOrder().getOperationAllowed() != null) {
            PunchoutOrderOperation allowedOp = this.getCurrentOrder().getOperationAllowed();
            if (!allowedOp.equals(PunchoutOrderOperation.create) && !allowedOp.equals(PunchoutOrderOperation.edit)) {
                return false;
            }
        }
        String requiredString = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_AMT_ALLOWED.getValue());
        String requiredAmnt = this.credentialSelectionBean.getPropertyMap().get(
                MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue());
        String fee = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_FEE.getValue());
        try {
            if (StringUtils.isNotBlank(requiredString)) {
                BigDecimal requiredAmount = new BigDecimal(requiredString);
                return this.getSubtotalNoFees().compareTo(requiredAmount) >= 0;
            }
            else {
                if (fee == null && requiredAmnt != null) {
                    BigDecimal requiredAmount = new BigDecimal(requiredAmnt);
                    return this.getSubtotalNoFees().compareTo(requiredAmount) >= 0;
                }
                else {
                    return true;
                }
            }
        }
        catch (Exception e) {
            LOGGER.warn("Couldn't parse minimum amount required, defaulting to allow: ", e);
            return true;
        }
    }

    public String getPlaceOrderDisallowedMessage() {
        String requiredString = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_AMT_ALLOWED.getValue());
        String requiredAmnt = this.credentialSelectionBean.getPropertyMap().get(
                MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue());
        String fee = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_FEE.getValue());
        if (StringUtils.isNotBlank(requiredString)) {
            return "Please add "
                    + ViewUtils.toCurrency(new BigDecimal(requiredString).subtract(this.getSubtotalNoFees()) + "")
                    + " to your cart to meet the " + ViewUtils.toCurrency(requiredString)
                    + " minimum order requirement";
        }
        else {
            if (StringUtils.isBlank(fee) && StringUtils.isNotBlank(requiredAmnt)) {
                return "Please add "
                        + ViewUtils.toCurrency(new BigDecimal(requiredAmnt).subtract(this.getSubtotalNoFees()) + "")
                        + " to your cart to meet the " + ViewUtils.toCurrency(requiredAmnt)
                        + " minimum order requirement";
            }
        }
        return "Placing this order is not allowed";
    }

    public boolean isMinimumOrderFeeApplies() {
        String requiredAmnt = this.credentialSelectionBean.getPropertyMap().get(
                MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue());
        String fee = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_FEE.getValue());
        if (StringUtils.isNotBlank(requiredAmnt) && StringUtils.isNotBlank(fee)) {
            return new BigDecimal(requiredAmnt).compareTo(this.getSubtotalNoFees()) > 0;
        }
        else {
            return false;
        }
    }

    public boolean isUpsShippingInfo() {
        String upsShippingInfo = this.credentialSelectionBean.getPropertyMap().get(
                DISPLAY_UPS_SHIPPING_NOTICE.getValue());
        if (upsShippingInfo == null && currentOrder != null && currentOrder.getCredential() != null) {
            upsShippingInfo = currentOrder.getCredential().getPropertyValueByType(
                    DISPLAY_UPS_SHIPPING_NOTICE.getValue());
        }
        return AbstractPropertyUtils.isTrue(upsShippingInfo);
    }

    public String getMinimumOrderFeeMessage() {
        String requiredAmnt = this.credentialSelectionBean.getPropertyMap().get(
                MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue());
        String fee = this.credentialSelectionBean.getPropertyMap().get(MIN_ORDER_FEE.getValue());
        if (StringUtils.isNotBlank(fee) && StringUtils.isNotBlank(requiredAmnt)) {
            return "Please add "
                    + ViewUtils.toCurrency(new BigDecimal(requiredAmnt).subtract(this.getSubtotalNoFees()) + "")
                    + " to avoid paying a minimum order fee of " + ViewUtils.toCurrency(fee);
        }
        else {
            return "";
        }
    }

    private BigDecimal getSubtotalNoFees() {
        return cartBean.getSubtotal().max(this.currentOrder.getSubtotalNoFees());
    }

    public String generatePunchoutReturnCartLabel() {
        String label = credentialSelectionBean.getPropertyMap().get(PUNCHOUT_RETURN_BUTTON_LABEL.getValue());
        return StringUtils.isNotEmpty(label) ? label : MISSING_PUNCHOUT_RETURN_CART_LABEL;
    }

    public String generateCheckoutLink() {
        if (credentialSelectionBean.isPunchoutSession()) {
            return linksBean.getReturnCart();
        }
        return placeOrder();
    }

    public boolean priceChange(LineItem item) {
        if (getCurrentAXCXU() == null || getCurrentAXCXU().getUser() == null) {
            LOGGER.warn("No user to lookup order history found");
            return false;
        }
        if (getCurrentAXCXU().getCredential() != null
                && "Y".equals(getCurrentAXCXU().getCredential().getPropertyValueByType(
                        CredentialPropertyTypeEnum.DISPLAY_PRICE_INCREASE_MESSAGE.getValue()))) {
            LineItem lastItem = customerOrderBp.findLastTimeItemOrdered(item, getCurrentAXCXU().getUser());
            if (lastItem != null) {
                return (lastItem.getUnitPrice().compareTo(item.getUnitPrice()) == -1);
            }
        }
        return false;
    }

    public String getPriceChangeNotice(LineItem item) {
        LineItem lastItem = customerOrderBp.findLastTimeItemOrdered(item, getCurrentAXCXU().getUser());
        StringBuilder notice = new StringBuilder();
        notice.append("The current price of $");
        notice.append(item.getUnitPrice().setScale(2).toPlainString());
        notice.append(" is ");
        notice.append(getPercenthigher(item.getUnitPrice(), lastItem.getUnitPrice()));
        notice.append("% higher than the previous purchase price of $");
        notice.append(lastItem.getUnitPrice().setScale(2).toPlainString());
        notice.append(" on PO # ");
        notice.append(lastItem.getOrder().getApdPo().getValue());
        notice.append(", placed on ");
        notice.append(DateUtil.formatDate(lastItem.getOrder().getOrderPlacedDate(), "MM/dd/YYYY"));
        notice.append(".");
        return notice.toString();
    }

    private String getPercenthigher(BigDecimal newPrice, BigDecimal oldPrice) {
        BigDecimal fraction = newPrice.divide(oldPrice, 4, BigDecimal.ROUND_HALF_UP).subtract(BigDecimal.ONE);
        BigDecimal percent = fraction.multiply(new BigDecimal("100").setScale(2)).setScale(2);
        return percent.toPlainString();
    }

}
