/**
 * 
 */
package com.apd.phoenix.shopping.view.jsf.bean.workflow;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.jbpm.task.Status;
import org.jbpm.task.query.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.workflow.TaskComparator;
import com.apd.phoenix.service.workflow.UserTaskService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.faces.context.ExternalContext;

@Named
@Stateful
@SessionScoped
public class WorkflowTaskBean implements Serializable {

    private static final String HUMAN_TASK_FORM = "/ecommerce/task/humanTaskForm.xhtml?faces-redirect=true";

    private static final Logger logger = LoggerFactory.getLogger(WorkflowTaskBean.class);

    private static final long serialVersionUID = -7878893937965889019L;

    private Long id;

    private String user;

    private Long taskToWork;

    private String taskToWorkName;

    private List<TaskSummary> assignedTasks;

    private boolean showingCompletedTasks;

    @Inject
    private UserTaskService userTaskService;

    @PostConstruct
    public void init() {
        logger.info("Initializing wtb");
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        user = context.getUserPrincipal().getName();
        logger.info("...for user " + user);
    }

    public void refreshList() {
        List<PhoenixTaskSummary> allAssigned = userTaskService.getAssignedTasks(user,false);
        Collections.sort(allAssigned, new TaskComparator());
        this.assignedTasks = new ArrayList<>();
        Set<Status> statusesToExclude = new HashSet<>();
        statusesToExclude.add(Status.Exited);
        if (!this.showingCompletedTasks){
            statusesToExclude.add(Status.Completed);
        }
        for (TaskSummary summary : allAssigned) {
            if (!statusesToExclude.contains(summary.getStatus())) {
                this.assignedTasks.add(summary);
            }
        }
        logger.info("Fetched " + this.assignedTasks.size() + " assigned tasks");
    }

    public void claimTask(TaskSummary task) {
        logger.info("Claiming task=" + task.getId() + " as user=" + user);
        userTaskService.claimTask(task.getId(), user);
    }

    public boolean canClaim(TaskSummary task) {
        return task.getStatus().equals(Status.Ready);
    }

    public String startTask(TaskSummary task) {
        logger.info("Starting taskid=" + task.getId() + " taskname=" + task.getName());
        userTaskService.startTask(task.getId(), user);
        this.taskToWork = task.getId();
        this.taskToWorkName = task.getName();
        return HUMAN_TASK_FORM;
    }

    public String continueTask(TaskSummary task) {
        logger.info("Continueing taskid=" + task.getId() + " taskname=" + task.getName());
        this.taskToWork = task.getId();
        this.taskToWorkName = task.getName();
        return HUMAN_TASK_FORM;
    }

    public boolean canContinue(TaskSummary task) {
        if (task == null) {
            logger.error("Can't continue a null task!!!!");
            return false;
        }
        return task.getStatus().equals(Status.InProgress);
    }

    public boolean canStart(TaskSummary task) {
        if (task == null) {
            logger.error("Can't start a null task!!!!!");
            return false;
        }
        return task.getStatus().equals(Status.Reserved);
    }

    public void retrieve() {
        this.refreshList();
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the taskToWork
     */
    public Long getTaskToWork() {
        return taskToWork;
    }

    /**
     * @param taskToWork
     *            the taskToWork to set
     */
    public void setTaskToWork(Long taskToWork) {
        this.taskToWork = taskToWork;
    }

    /**
     * @return the assignedTasks
     */
    public List<TaskSummary> getAssignedTasks() {
        return this.assignedTasks;
    }

    public boolean isShowingCompletedTasks() {
        return showingCompletedTasks;
    }

    public void setShowingCompletedTasks(boolean showingCompletedTasks) {
        this.showingCompletedTasks = showingCompletedTasks;
    }

    public long getNumberOfAssignedTasks() {
        return userTaskService.numberOfAssignedTasks(this.user, false);
    }

    public String getTaskToWorkName() {
        return taskToWorkName;
    }

    public void setTaskToWorkName(String taskToWorkName) {
        this.taskToWorkName = taskToWorkName;
    }

    public String getApdPo(TaskSummary task) {
        String description = task.getDescription();
        if (StringUtils.isBlank(description)) {
            return "N/A";
        }
        else {
            return description;
        }
    }

    public String getStatus(TaskSummary task) {
        if (task != null && task.getStatus() == Status.Suspended) {
            return "Completing...";
        }
        else if (task != null) {
            return task.getStatus().toString();
        }
        else {
            return "";
        }
    }

}
