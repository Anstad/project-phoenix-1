package com.apd.phoenix.shopping.oci.request.processors;

import org.apache.commons.configuration.PropertiesConfiguration;
import com.apd.phoenix.shopping.oci.OCIRequestWrapper;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;

public abstract class OCIRequestProcessor<T extends OCIRequestWrapper> {

    public abstract void processRequest(T request) throws OciProcessorException;

    protected static final PropertiesConfiguration propLoader = EcommercePropertiesLoader.getInstance()
            .getEcommerceProperties();
    protected static final String BASE_PUNCHOUT_LOGIN_PAGE = propLoader.getString("punchoutBaseLoginUrl");

}
