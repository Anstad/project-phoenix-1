package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemSpecification;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SpecificationProperty;
import com.apd.phoenix.service.ussco.interlink.UsscoStockCheck;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ValidationPropertiesLoader;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.google.gson.Gson;

import java.util.ResourceBundle;

/**
 * This class is used to contain the functionality for adding items to the cart. 
 * <br /><br />
 * If you want to add additional conditions that trigger a notification, modify the processItem method (to add the 
 * trigger for an item), the PromptIssue enum (adding the issue that will be triggered), the PromptAction enum (adding 
 * the actions that the user can take to resolve the issue), and the performActions method (determining, based on the 
 * issue and the action taken, what will be done).
 * <br /><br />
 * Note: when there is a discrepancy between the database and Solr, this class errs on the side of preventing items 
 * from being added. So, if an item goes from restricted to available in the database, it cannot be added until Solr 
 * is updated; if an item goes from available to restricted in the database, it cannot be added, even if Solr isn't 
 * updated.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@SessionScoped
public class AddToCartBean {

    private static final String STOCK_CHECK_ZIP_PROPERTY_TYPE = "zip code for stock check";
    private static final String US_ACCOUNT_PROPERTY_TYPE = "US Account #";
    private static final String STOCK_CHECK_ALL_FACILITIES_PROPERTY_TYPE = "check all facilities (ADOT)";
    private static final String VENDOR_SKU_TYPE = "vendor";
    private static final String STOCK_CHECK_VENDOR_NAME = "USSCO";
    //TODO: change to "false" and "true"
    public static final String NO_VALUE = "N";
    public static final String YES_VALUE = "Y";
    public static final String SPECIAL_ORDER_TYPE = "specialOrder";
    private static final String RESTRICTED_VALUE = "restricted";
    private static final String AVAILABILITY_TYPE = "availability";
    public static final String CUSTOM_ORDER_TYPE = "customOrder";
    private static final int DEFAULT_ADDED_QUANTITY = 1;
    private static final String ADD_SUCCESS_PROPERTY = "addToCart.success";
    public static final String FORCE_SUB_PROPERTYTYPE = "force substitute";
    private static final String NONE_IN_CART_QUANTITY = "0";
	
    @Inject
    private ShoppingCartBean cartBean;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    @Inject
    private SearchUtilities searchUtils;
    @Inject 
    private ViewUtils viewUtils;
    @Inject
    private LinksBean linksBean;
    @Inject
    private CredentialSelectionBean credBean;
    @Inject
    private UsscoStockCheck stockCheck;
    @Inject
    private CatalogBp catalogBp;
	
    //this map stores the quantity entered by the user
    private ShoppingMap<Product, String> displayQuantity = new ShoppingMap<>();
    //this is the list of items that have been added to the cart
    private Map<Product, Integer> itemsAdded = new HashMap<>();
    //this is a map from the items added to the cart, to the original item selected
    private Map<Product, Product> itemsAddedOriginals = new HashMap<>();
    //the list of items that need to be validated by user
    private List<Product> toPromptForInput = new ArrayList<>();
    //a map from the items to be validated to the quantity to be set
    private Map<Product, Integer> toPromptQuantity = new HashMap<>();
    //a map from the items to be validated to the action taken by the user
    private Map<Product, PromptAction> toPromptActions = new HashMap<>();
    //a map from the items to be validated to the substitute (if applicable)
    private Map<Product, Product> toPromptSubstitute = new HashMap<>();
    //a map from the items to be validated to the issue causing the prompt
    private Map<Product, PromptIssue> toPromptIssue = new HashMap<>();
    //a map from the items that may need validation to the quantity
    private Map<Product, Integer> toProcess = new HashMap<>();
    //a map from the items that may need validation to the issues that have been resolved by the user
    private Map<Product, List<PromptIssue>> handledIssues = new HashMap<>();
    private PromptAction action;
    private int itemIndex;
    //the element of the page to render after the item is added
    private String elementToRender;
    private String jsonData = "{ \"display\": \"none\", \"components\": [] }";

    public String getYES_VALUE() {
    	return YES_VALUE;
    }
    
    public void preRender() {
    	if (!FacesContext.getCurrentInstance().isPostback()) {
    		clearItemState();
    	}
    }
    
    /**
     * Takes a Product, and returns true if the user shouldn't be able to add the item to their cart. Used to change
     * the styling of the "Add to cart" buttons.
     * 
     * @param p
     * @return
     */
    public boolean isAddingPrevented(Product p) {
    	if (p == null) {
    		return true;
    	}
    	if (isDiscontinued(p) && hasReplacement(p)) {
    		return false;
    	}
    	return isRestricted(p) || isDiscontinued(p) || isContactRequired(p) || !credBean.hasPermission("shopping add to cart") 
    			|| (!credBean.hasPermission("shopping add zero price to cart") && p.getPrice().compareTo(BigDecimal.ZERO) == 0);
    }

    public boolean showPrice(Product p) {
        return !isDiscontinued(p) && !isContactRequired(p) && !isSpecialOrder(p) && !isCustomOrder(p);
    }
    /**
     * Takes a Product, and returns the text for the "Add To Cart" button.
     * 
     * @param product
     * @param isProductDetailsPage
     * @return
     */
    public String getAddToCartButtonText(Product product, boolean isProductDetailsPage) {
    	if (isDiscontinued(product)) {
    		return "DISCONTINUED";
    	}
    	if (isRestricted(product)) {
    		return "RESTRICTED";
    	}
    	if (isContactRequired(product)) {
    		return "CONTACT APD";
    	}
        if (isSpecialOrder(product)) {
                return "SPECIAL ORDER";
        }

        return cartBean.getCartQuantity().containsKey(product) && cartBean.getCartQuantity().get(product) > 0 ? "UPDATE" : "ADD TO CART";
    }
    
    public boolean isDiscontinued(Product product) {
    	if (product == null) {
    		return false;
    	}
    	return "DISCONTINUED".equals(product.getStatus());
    }
    
    public boolean isRestricted(Product product) {
    	if (product == null) {
    		return true;
    	}
    	//If a condition is added for a product being restricted, it has to be added to 
    	//the database check as well, in the RESTRICTED case of processItem
    		return (product.getProperties().containsKey(AVAILABILITY_TYPE) && (RESTRICTED_VALUE.equalsIgnoreCase(product.getProperties().get(AVAILABILITY_TYPE)) || "no".equalsIgnoreCase(product.getProperties().get(AVAILABILITY_TYPE))));
    	
	}
    
    private boolean isContactRequired(Product product) {
    	if (product == null) {
    		return false;
    	}
    	return "CONTACT_REQUIRED".equals(product.getStatus());
    }
    
    public boolean isSpecialOrder(Product product) {
    	if (product == null) {
    		return false;
    	}
        return product.getSpecialOrder() != null && 
                !NO_VALUE.equals(product.getSpecialOrder());
    }
    
    public boolean isCustomOrder(Product product) {
    	if (product == null) {
    		return false;
    	}
        return product.getCustomOrder() != null && !NO_VALUE.equals(product.getCustomOrder());
    }
    
    public boolean hasReplacement(Product product) {
    	return (StringUtils.isNotBlank(product.getCustomerReplacementSku())
    			&& StringUtils.isNotBlank(product.getCustomerReplacementVendor()));
    }
    
    /**
     * Takes a List of Products, and returns true if the user shouldn't be able to add any of them to their cart. 
     * Used to change the styling of the "Add all to cart" button.
     * 
     * @param toCheck
     * @return
     */
    public boolean isAllAddingPrevented(List<Product> toCheck) {
    	for (Product product : toCheck) {
    		if (!isAddingPrevented(product)) {
    			return false;
    		}
    	}
    	return true;
    }

	/**
	 * Returns a JSON containing the information to display
	 * 
	 * @return
	 */
	public String getJsonData() {
		return "<script type=\"text/javascript\">var jsonData = " + this.jsonData + "</script>";
	}
	
	/**
	 * Called by the Add To Cart buttons, sets the item that the system will attempt to add.
	 * 
	 * @param product
	 */
	public void addSingleItem(Product product) {
		clearItemState();
		this.preProcessItem(product, this.parseQuantity(product));
		this.nextAction();
	}
	
        public void addSingleItem(Product product, int qty) {
		clearItemState();
		this.preProcessItem(product, qty);
		this.nextAction();            
        }
        
        /**
	 * Called by the Add All To Cart button, sets the set of items that the system will attempt to add.
	 * 
	 * @param productList
	 */
        
    public void addAllToCart(List<Product> toAdd) {
        List<Product> products = new ArrayList<>();
        if (toAdd != null) {
            for (Product product : toAdd) {
                if (this.shouldAttemptAdding(product)) {
                	products.add(product);
                }
            }
        }
        this.addItemList(products);
    }
    
    private boolean shouldAttemptAdding(Product product) {
    	if (isAddingPrevented(product)) {
    		//shouldn't try to add if the "add to cart" button is disabled
    		return false;
    	}
    	if (StringUtils.isBlank(displayQuantity.getValueEntered(product))) {
    		//shouldn't try to add if the entered quantity is empty
    		return false;
    	}
    	
    	String quantityToAdd = displayQuantity.getValueEntered(product);
    	Integer cartQuantity = cartBean.getCartQuantity().get(product);
    	
    	if (StringUtils.isBlank(quantityToAdd) || quantityToAdd.trim().equals(NONE_IN_CART_QUANTITY.trim())) {
    		return false;
    	}
    	
    	if (cartQuantity != null && quantityToAdd.trim().equals(cartQuantity + "")) {
    		//shouldn't try to add if the entered quantity is the same as the cart quantity
    		return false;
    	}
    	
    	return true;
    }

	private void addItemList(List<Product> productList) {
		clearItemState();
		for (Product product : productList) {
			this.preProcessItem(product, this.parseQuantity(product));
		}
		this.nextAction();
	}
    
    private int parseQuantity(Product toAdd) {
		//parses the entered value into the quantity 
		int quantity = 0;
		try {
			quantity = Integer.parseInt(displayQuantity.getValueEntered(toAdd));
		} catch (NumberFormatException e) {
			//do nothing
		}
		if (quantity <= 0) {
			//uses default if value is zero or unparseable
			quantity = DEFAULT_ADDED_QUANTITY;
		}
		return quantity;
    }
	
	/**
	 * Attempts to add the item to the cart, and apply any issue resolutions. Then, updates the jsonData.
	 */
	public void nextAction() {
		//performs any actions specified by the user
		if (!this.toPromptActions.isEmpty()) {
			performActions();
		}
		//processes any items specified in issue resolution or by addSingleItem or addItemList
    	for (Product itemToProcess : toProcess.keySet()) {
    		this.findIssue(itemToProcess, toProcess.get(itemToProcess));
    	}
    	//based on the result of findIssue, generates the Json to be used to display
    	if (toPromptForInput.isEmpty()) {
    		if (itemsAdded.isEmpty()) {
    			//no items were added, and there are no issues needing resolving
    			this.jsonData = "{ \"display\": \"none\", \"components\": [] }";
    			clearItemState();
    	    	this.displayQuantity = new ShoppingMap<>();
    			return;
    		}
    		//items were added, no issues need resolving
    		this.jsonData = "{ \"display\": \"added\", \"components\": " + getItemsAddedJson() + " }";
    		clearItemState();
        	this.displayQuantity = new ShoppingMap<>();
    		return;
    	}
    	//issues need resolving
		this.jsonData = "{ \"display\": \"notify\", \"components\": " + getItemsToNotifyJson() + " }";
		return;
	}
    
    /**
     * Adds the item to toProcess, setting it up to begin the processing. This clears any resolved issues associated 
     * with the item, so this is for an item that is starting the resolution process.
     * 
     * @param item
     * @param quantity
     */
    private void preProcessItem(Product item, int quantity) {
        toProcess.put(item, quantity);
        handledIssues.put(item, new ArrayList<PromptIssue>());
    }
    
    public void setAction(String action) {
    	this.action = PromptAction.DISCARD;
    	for (PromptAction possibleAction : PromptAction.values()) {
    		if (possibleAction.getLabel().equals(action)) {
    			this.action = possibleAction;
    			break;
    		}
    	}
    }
    
    public void setItemIndex(int index) {
    	this.itemIndex = index;
    }
    
    public void setActionForItem() {
    	this.toPromptActions.put(this.toPromptForInput.get(this.itemIndex), this.action);
    }
    
    public String getElementToRender() {
    	return this.elementToRender;
    }
    
    /**
     * Clears all of the state of the bean, except for the displayQuantity map. Called just before the items are first 
     * processed.
     */
    private void clearItemState() {
    	this.elementToRender = "";
    	this.toPromptActions = new HashMap<>();
    	this.itemsAdded = new HashMap<>();
    	this.toPromptForInput = new ArrayList<>();
    	this.toProcess = new HashMap<>();
    	this.toPromptIssue = new HashMap<>();
    	this.toPromptQuantity = new HashMap<>();
    	this.toPromptSubstitute = new HashMap<>();
    	this.handledIssues = new HashMap<>();
    	this.itemsAddedOriginals = new HashMap<>();
    }
    
    /**
     * Called when the add to cart process is canceled to clear the internal state of this bean
     */
    public void cancelAddToCartProcess() {
        if (!this.itemsAdded.isEmpty()) {
            this.jsonData = "{ \"display\": \"added\", \"components\": " + getItemsAddedJson() + " }";     
        } else {
            this.jsonData = "{ \"display\": \"canceled\", \"components\": [] }";
        }
        this.clearItemState();
    }
    
    /**
     * Produces a JSON, containing the items in itemsAdded.
     * 
     * @return
     */
    private String getItemsAddedJson() {
        StringBuilder jsonResponse = new StringBuilder();
        jsonResponse.append("[");
        for (Product item : this.itemsAdded.keySet()) {
        	jsonResponse.append(getDisplayJson(item, PromptIssue.NO_ISSUE));
        	jsonResponse.append(",");
        }
        if (!this.itemsAdded.isEmpty()) {
        	jsonResponse.deleteCharAt(jsonResponse.length() - 1);
        }
        jsonResponse.append("]");
        return jsonResponse.toString();
    }
	
    /**
     * Produces a JSON, with the items that the user has to validate.
     * 
     * @return
     */
    private String getItemsToNotifyJson() {
        StringBuilder jsonResponse = new StringBuilder();
        jsonResponse.append("[");

        for (Product item : this.toPromptForInput) {
        	jsonResponse.append(getDisplayJson(item, this.toPromptIssue.get(item)));
        	jsonResponse.append(",");
        }
        if (!this.toPromptForInput.isEmpty()) {
        	jsonResponse.deleteCharAt(jsonResponse.length() - 1);
        }

        jsonResponse.append("]");
        return jsonResponse.toString();
    }
    
    /**
     * Based on the issue and the item, returns a JSON object that will be displayed.
     * 
     * @param item
     * @param issue
     * @return
     */
    private String getDisplayJson(Product item, PromptIssue issue) {
    	List<Product> items = new ArrayList<>();
    	items.add(item);
    	if (issue == PromptIssue.FORCE_SUB || issue == PromptIssue.SOFT_SUB || issue == PromptIssue.DISCONTINUED_WITH_REPLACE) {
    		items.add(toPromptSubstitute.get(item));
    	}
    	return getComparisonDisplayJson(items, issue);
    }

    /**
     * Based on the issue and a list of items, returns a JSON object that will display the items to be compared.
     * 
     * @param item
     * @param type
     * @return
     */
    private String getComparisonDisplayJson(List<Product> items, PromptIssue issue) {
    	if (items.isEmpty()) {
    		return "";
    	}
    	List<ItemJsonContainer> itemContainerList = new ArrayList<>();
    	//Gets the quantity to be displayed
    	String quantity = "";
        if (issue != PromptIssue.NO_ISSUE) {
            quantity = toPromptQuantity.get(items.get(0)).toString();
        } else if (cartBean.getCartQuantity().get(items.get(0)) == null){
        	quantity = "";
        }else {
        	quantity = cartBean.getCartQuantity().get(items.get(0)).toString();
        }
        //gets the specifications to be displayed
        List<String> specifications = new ArrayList<>();
        List<CatalogXItem> dbItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
        	dbItems.add(catalogXItemBp.hydrateForLineItem(searchUtils.getFromProduct(items.get(i))));
        	for (ItemSpecification spec : dbItems.get(i).getItem().getItemSpecifications()) {
        		for (SpecificationProperty prop : spec.getSpecificationProperties()) {
        			if (!specifications.contains(prop.getName())) {
        				specifications.add(prop.getName());
        			}
        		}
        	}
        }
    	for (int i = 0; i < items.size(); i++) {
    		CatalogXItem item = dbItems.get(i);
    		Product product = items.get(i);
	        ItemJsonContainer obj = new ItemJsonContainer();
	        itemContainerList.add(obj);
	        obj.setImgURL(product.getMainImageUrl());
	        obj.setItemName(item.getItem().getName());
	        obj.setItemQty(quantity);
                if (showPrice(product)) {
                    obj.setItemPrice((item.getPrice()).toString());
                } else {
                    obj.setItemPrice("");
                }
	        obj.setMessageHeader(ValidationPropertiesLoader.getInstance().getValidationProperties()
                    .getString(ADD_SUCCESS_PROPERTY));
	        Map<String, String> specMap = new HashMap<>();
	        obj.setSpecMap(specMap);
	        obj.setItemDescription(item.getItem().getDescription());
	        obj.setItemSku(product.getCustomerSku());
        	for (ItemSpecification spec : item.getItem().getItemSpecifications()) {
        		for (SpecificationProperty prop : spec.getSpecificationProperties()) {
        			specMap.put(prop.getName(), prop.getValue());
        		}
        	}
    	}
        String messageOverride = null;
        if ((PromptIssue.FORCE_SUB.equals(issue) || PromptIssue.SOFT_SUB.equals(issue)) && dbItems.get(0).getProperty("substituteReason") != null) {
            messageOverride = dbItems.get(0).getProperty("substituteReason").getValue();
        }
        
        String fullMessageOverride = null;
        if (issue.equals(PromptIssue.INVALID_QUANTITY)) {
        	Integer minimum = dbItems.get(0).getItem().getMinimum();
        	Integer multiple = dbItems.get(0).getItem().getMultiple();
        	if ((minimum != null && minimum > 1) && (multiple != null && multiple > 1)) {
        		fullMessageOverride = "You must select a minimum quantity of " + minimum + ", in multiples of " + multiple + ".";
        	}
        	else if ((minimum != null && minimum > 1) && (multiple == null || multiple <= 1)) {
        		fullMessageOverride = "You must select a minimum quantity of " + minimum + ".";
        	}
        	else if ((minimum == null || minimum <= 1) && (multiple != null && multiple > 1)) {
        		fullMessageOverride = "You must select a quantity in multiples of " + multiple + ".";
        	}
        }

        Gson gson = new Gson();
    	CartNotificationContainer container = new CartNotificationContainer(itemContainerList, 
    			specifications, issue, messageOverride, fullMessageOverride, linksBean.getCart());

        // convert java object to JSON format,
        // and returned as JSON formatted string
        String json = gson.toJson(container);

        return json;
    }

    public ShoppingMap<Product, String> getDisplayQuantity() {
            return displayQuantity;
    }
    
    public void setDisplayQuantity(Product prod, int qty) {
        displayQuantity.put(prod, qty+"");
    }

    private void addToCart(Product item, int quantity) {
        itemsAdded.put(item, quantity);
        cartBean.addToCart(item, quantity);
        if (itemsAddedOriginals.containsKey(item)) {
            cartBean.getSubsToOriginals().put(item, itemsAddedOriginals.get(item));
        }
    }

	/**
	 * This class extends HashMap, and has two main differences from a regular Map implementation. First, the get() 
	 * method returns 0 of the key doesn't exist; if it does exist, it returns the value on the quantity map in 
	 * ShoppingCartBean. Second, the value entered using "put" can be retrieved using getValueEntered.
	 * <br /><br />
	 * This allows for having the values in the Map being value-bound to fields on the pages, but not being saved 
	 * unless the "Add to cart" or "Add all to cart" buttons are pressed.
	 * 
	 * @author RHC
	 *
	 * @param <K>
	 * @param <V>
	 */
    public class ShoppingMap<K,V> extends HashMap<K,V> {

		private static final long serialVersionUID = 1L;

		@SuppressWarnings("unchecked")
		@Override
		public V get(Object key) {
			if (!cartBean.getCartQuantity().containsKey(key)) {
				return (V) NONE_IN_CART_QUANTITY;
			}
			return (V) cartBean.getCartQuantity().get(key);
		}
		
		public V getValueEntered(Object key) {
			return super.get(key);
		}
    }
    
    /**
     * Determines whether an item can be added to the cart. If it can, it is added; if it can't, adds it to the 
     * toPrompt collections.
     * 
     * @param itemToProcess
     * @param quantity
     */
    private void findIssue(Product itemToProcess, int quantity) {
    	CatalogXItem catXItem = catalogXItemBp.hydrateForLineItem(searchUtils.getFromProduct(itemToProcess));
    	for (PromptIssue issue : PromptIssue.values()) {
    		if (!this.handledIssues.get(itemToProcess).contains(issue) 
    				&& processItem(catXItem, itemToProcess, quantity, issue)) {
    			toPromptForInput.add(itemToProcess);
    			toPromptQuantity.put(itemToProcess, quantity);
    			if (catXItem.getSubstituteItem() != null) {
    				Product subProduct = viewUtils.getProductFromCatalogXItem(catXItem.getSubstituteItem());
    				toPromptSubstitute.put(itemToProcess, subProduct);
    			}
    			if (catXItem.getItem().getReplacement() != null) {
    				CatalogXItem replacementItem = this.getCatalogXItem(catXItem.getItem().getReplacement(), credBean.getCurrentCredential().getCatalog());
    				if (replacementItem != null) {
        				Product subProduct = viewUtils.getProductFromCatalogXItem(replacementItem);
    					toPromptSubstitute.put(itemToProcess, subProduct);
    				}
    			}
        		toPromptIssue.put(itemToProcess, issue);
        		toPromptActions.put(itemToProcess, PromptAction.DISCARD);
        		return;
    		}
    	}
        //no PromptIssues for this item
    	int newQuantity = toProcess.get(itemToProcess);
    	if (itemsAdded.containsKey(itemToProcess)) {
    		newQuantity += itemsAdded.get(itemToProcess);
    	}
        this.addToCart(itemToProcess, newQuantity);
    }
    
    /**
     * This enum lists the different reasons a user can be prompted for an item. Each entry has two lines of output, 
     * and an array of possible options.
     * <br /><br />
     * When adding an additional issue that triggers a prompt, make sure to edit processItem, promptAction, and 
     * performActions.
     * <br /><br />
     * The order of the entries in the list determines the order that the errors will be displayed. For example, as 
     * of this writing, "SOFT_SUB" is listed before "OUT_OF_STOCK". This means that if an item with a soft substitute 
     * that is out of stock is added to the cart, the "Soft Substitute" warning will be shown, then the "Out Of Stock" 
     * warning if the user insists on adding it.
     * 
     * @author RHC
     * 
     */
    public enum PromptIssue {
        ERROR("error", new PromptAction[] {PromptAction.DISCARD}), 
        RESTRICTED("restricted", new PromptAction[] {PromptAction.DISCARD}),
        DISCONTINUED_WITH_REPLACE("discont_with_replace", new PromptAction[] {PromptAction.DISCARD, PromptAction.USE_SUBSTITUTE}),
        DISCONTINUED("discontinued", new PromptAction[] {PromptAction.DISCARD}),
        CUSTOM("custom", new PromptAction[] {PromptAction.DISCARD}),
        SPECIAL("special", new PromptAction[] {PromptAction.DISCARD}), 
        FORCE_SUB("force_sub", new PromptAction[] {PromptAction.DISCARD, PromptAction.USE_SUBSTITUTE}), 
        SOFT_SUB("soft_sub", new PromptAction[] {PromptAction.DISCARD, PromptAction.USE_ORIGINAL, PromptAction.USE_SUBSTITUTE}), 
        OUT_OF_STOCK("exceeds_availability", new PromptAction[] {PromptAction.USE_ORIGINAL, PromptAction.DISCARD}), 
        OUT_OF_STOCK_NO_BACK("exceeds_availability_no_bo", new PromptAction[] {PromptAction.DISCARD}), 
        INVALID_QUANTITY("quantity_invalid", new PromptAction[] {PromptAction.DISCARD}),
        NO_ISSUE("no_issue", new PromptAction[] {});

        private final String messageProperty;
        private final PromptAction[] actionArray;

        private PromptIssue(String messageProperty, PromptAction[] actionArray) {
        	this.messageProperty = messageProperty;
            this.actionArray = actionArray;
        }
        
        public String getMessageProperty() {
        	return this.messageProperty;
        }
        
        public PromptAction[] getActionArray() {
        	return this.actionArray;
        }
    }
    
    /**
     * This enum lists the different actions a user can take. Each entry has a string used to identify the issue to 
     * the app (no spaces), and a label for the option that will be on the button.
     * <br /><br />
     * When adding an additional issue that triggers a prompt, make sure to edit processItem, promptIssue, and 
     * performActions.
     * 
     * @author RHC
     *
     */
    public enum PromptAction {
        DISCARD("discard", ResourceBundle.getBundle("com.apd.phoenix.strings.promptIssues").getString("button.discard")),
        USE_ORIGINAL("use-original", ResourceBundle.getBundle("com.apd.phoenix.strings.promptIssues").getString("button.use_original")),
        USE_SUBSTITUTE("use-substitute", ResourceBundle.getBundle("com.apd.phoenix.strings.promptIssues").getString("button.use_substitute"));

        private final String label;
        private final String buttonText;

        private PromptAction(String label, String buttonText) {
            this.label = label;
            this.buttonText = buttonText;
        }

        public String getLabel() {
            return this.label;
        }
        
        public String getButtonText() {
        	return this.buttonText;
        }
    }
	
	/**
	 * Iterates through the items the user was prompted about, executing the selected actions
     * <br /><br />
     * When adding an additional issue that triggers a prompt, make sure to edit processItem, promptIssue, and 
     * promptAction.
	 */
	private void performActions() {
		toProcess = new HashMap<>();
		for (Product item : toPromptForInput) {
			switch (toPromptIssue.get(item)) {
			case FORCE_SUB: 
				if (PromptAction.USE_SUBSTITUTE.equals(this.toPromptActions.get(item))) {
					preProcessItem(this.toPromptSubstitute.get(item), this.toPromptQuantity.get(item));
					itemsAddedOriginals.put(this.toPromptSubstitute.get(item), item);
				}
				break;
			case SOFT_SUB:
				if (PromptAction.USE_SUBSTITUTE.equals(this.toPromptActions.get(item))) {
					preProcessItem(this.toPromptSubstitute.get(item), this.toPromptQuantity.get(item));
					itemsAddedOriginals.put(this.toPromptSubstitute.get(item), item);
				} else if (PromptAction.USE_ORIGINAL.equals(this.toPromptActions.get(item))) {
					this.handledIssues.get(item).add(PromptIssue.SOFT_SUB);
					toProcess.put(item, this.toPromptQuantity.get(item));
				}
				break;
			case DISCONTINUED_WITH_REPLACE: 
				if (PromptAction.USE_SUBSTITUTE.equals(this.toPromptActions.get(item))) {
					preProcessItem(this.toPromptSubstitute.get(item), this.toPromptQuantity.get(item));
					itemsAddedOriginals.put(this.toPromptSubstitute.get(item), item);
				}
				break;
			case OUT_OF_STOCK:
				if (PromptAction.USE_ORIGINAL.equals(this.toPromptActions.get(item))) {
					toProcess.put(item, this.toPromptQuantity.get(item));
					this.handledIssues.get(item).add(PromptIssue.OUT_OF_STOCK);
				}
				break;
			default:
				break;
			}
			this.toPromptActions.remove(item);
		}
		toPromptForInput = new ArrayList<>();
	}
    
    /**
     * Takes an item, a quantity, and a PromptIssue, and determines whether adding the item to the cart would trigger 
     * that issue.
     * <br /><br />
     * When adding an additional issue that triggers a prompt, make sure to edit promptIssue, promptAction, and 
     * performActions.
     * 
     * @param item
     * @param quantity
     * @param issueToTest
     * @return
     */
    private boolean processItem(CatalogXItem item, Product productOfItem, int quantity, PromptIssue issueToTest) {
    	switch (issueToTest) {
    	case FORCE_SUB:
    		if (item.getSubstituteItem() != null) {
	    		for (CatalogXItemXItemPropertyType property : item.getProperties()) {
	    			if (property.getType().getName().equals(FORCE_SUB_PROPERTYTYPE) && !property.getValue().equals(NO_VALUE)) {
	    		    	return true;
	    			}
	    		}
    		}
    		break;
    	case SOFT_SUB:
    		if (item.getSubstituteItem() != null && !processItem(item, productOfItem, quantity, PromptIssue.FORCE_SUB)) {
    			return true;
    		}
    		break;
    	case OUT_OF_STOCK:
    		if (credBean.hasPermission("shopping order backordered items")) {
    			return isItemOutOfStock(item, quantity, issueToTest);
    		}
    		break;
    	case OUT_OF_STOCK_NO_BACK:
    		if (!credBean.hasPermission("shopping order backordered items")) {
    			return isItemOutOfStock(item, quantity, issueToTest);
    		}
    		break;
    	case CUSTOM:
    		for (ItemXItemPropertyType property : item.getItem().getPropertiesReadOnly()) {
    			if (property.getType().getName().equals(CUSTOM_ORDER_TYPE) && !property.getValue().equals(NO_VALUE)) {
                                return true;
                        }
    		}
    		break;
    	case RESTRICTED:
    		//checks the latest availability on the database, in case Solr is out of sync
    		if (item.getItem().getStatus() != null && !Item.ItemStatus.AVAILABLE.equals(item.getItem().getStatus()) && !Item.ItemStatus.DISCONTINUED.equals(item.getItem().getStatus())) {
    			return true;
    		}
    		//if the user somehow manages to kick off the add to cart action with an item with a dead button 
    		//(possibly through a custom HTTP request), they are prevented from actually adding it.
    		if (isAddingPrevented(productOfItem)) {
    			return true;
    		}
    		//re-checking credential permissions, on the item itself this time
    		//first, checking the "add to cart" permission
    		if (!credBean.hasPermission("shopping add to cart")) {
    			return true;
    		}
    		//then, checking the "add zero price" permission, if the product has a price of zero
    		if (!credBean.hasPermission("shopping add zero price to cart") && item.getPrice().compareTo(BigDecimal.ZERO) == 0) {
    			return true;
    		}
    		for (CatalogXItemXItemPropertyType property : item.getProperties()) {
    			if ((property.getType().getName().equals(AVAILABILITY_TYPE)) && (RESTRICTED_VALUE.equalsIgnoreCase(property.getValue()) || "no".equalsIgnoreCase(property.getValue()))) {
    				return true;
    			}
    		}
    		break;
        case DISCONTINUED:
                if (Item.ItemStatus.DISCONTINUED.equals(item.getItem().getStatus())) {
                	//if the item is discontinued, checks if there exists a replacement in the catalog. If not, this
                	//item is discontinued, and returns true. If there exists a replacement, it will be caught in
                	//the DISCONTINUE_WITH_REPLACE case
                	if (item.getItem().getReplacement() != null) {
                		return this.getCatalogXItem(item.getItem().getReplacement(), credBean.getCurrentCredential().getCatalog()) == null;
                	}
                    return true;
                }
                break;
    	case SPECIAL:
    		for (ItemXItemPropertyType property : item.getItem().getPropertiesReadOnly()) {
    			if (property.getType().getName().equals(SPECIAL_ORDER_TYPE) && !property.getValue().equals(NO_VALUE)) {
                                return true;
                        }
    		}
    		break;
    	case INVALID_QUANTITY:
    		//somewhat more verbose than strictly necessary, but makes more clear what conditions are being checked:
    		//first, checks that the quantity added is greater than the minimum. If not, this is an error.
    		if (item.getItem().getMinimum() != null && item.getItem().getMinimum() > 1 && quantity < item.getItem().getMinimum()) {
    			return true;
    		}
    		//then, checks that the quantity added is divisble by the multiples. If not, this is an error.
    		if (item.getItem().getMultiple() != null && item.getItem().getMultiple() > 1 && quantity % item.getItem().getMultiple() != 0) {
    			return true;
    		}
    		return false;
    	case DISCONTINUED_WITH_REPLACE:
    		if (Item.ItemStatus.DISCONTINUED.equals(item.getItem().getStatus()) && item.getItem().getReplacement() != null) {
    			//if there is a replacement assigned to the item, returns true if that replacement exists in this catalog
    			return this.getCatalogXItem(item.getItem().getReplacement(), credBean.getCurrentCredential().getCatalog()) != null;
    		}
    		break;
    	default:
    		return false;
    	}
    	return false;
    }
    
    private boolean isItemOutOfStock(CatalogXItem item, int quantity, PromptIssue issueToTest) {
    	if (item.getItem().getVendorCatalog().getVendor().getName().equals(STOCK_CHECK_VENDOR_NAME)) {
			String vendorSku = "";
			for (Sku s : item.getItem().getSkus()) {
				if (s.getType().getName().equals(VENDOR_SKU_TYPE)) {
					vendorSku = s.getValue();
					break;
				}
			}
			String zip = credBean.getPropertyMap().get(STOCK_CHECK_ZIP_PROPERTY_TYPE);
        	//TODO: refactor, use the "getUsAccountNumber" method on CustomerOrderBp
			String usAccountNumber = credBean.getPropertyMap().get(US_ACCOUNT_PROPERTY_TYPE);
			if (!StringUtils.isEmpty(credBean.getCurrentAXCXU().getUsAccountNumber())) {
				usAccountNumber = credBean.getCurrentAXCXU().getUsAccountNumber();
			}
			boolean checkAllFacilities = YES_VALUE.equals(credBean.getPropertyMap().get(STOCK_CHECK_ALL_FACILITIES_PROPERTY_TYPE));
			
			int available = stockCheck.available(vendorSku, zip, usAccountNumber, checkAllFacilities);
			return (available < quantity);
		}
    	return false;
    }
    
    private CatalogXItem getCatalogXItem(Item item, Catalog catalog) {
        List<Long> catalogIdList = new ArrayList<Long>();
        catalogIdList.add(catalog.getId());
        catalogIdList.addAll(catalogBp.getParentCatalogIdList(catalog.getId()));
        List<CatalogXItem> catxItems = new ArrayList<>();
        for (int i = 0; i < catalogIdList.size(); i++) {
            Long id = catalogIdList.get(i);
            CatalogXItem catalogXItem = new CatalogXItem();
            catalogXItem.setItem(new Item());
            catalogXItem.getItem().setId(item.getId());
            catalogXItem.setCatalog(new Catalog());
            catalogXItem.getCatalog().setId(id);
            catxItems = catalogXItemBp.searchByExactExample(catalogXItem, 0, 0);
            if (!catxItems.isEmpty()) {
                return catxItems.get(0);
            }
        }
        return null;
    }

}
