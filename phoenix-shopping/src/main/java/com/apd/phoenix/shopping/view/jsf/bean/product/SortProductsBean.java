/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@RequestScoped
public class SortProductsBean {

    private static final Logger logger = LoggerFactory.getLogger(SortProductsBean.class);
    private SortOption selectedSortOption = SortOption.ALPHABETICAL;
    private SortOption[] sortOptions;

    public SortOption getSelectedSortOption() {
        return selectedSortOption;
    }

    public void setSelectedSortOption(SortOption selectedSortOption) {
        this.selectedSortOption = selectedSortOption;
    }

    public SortOption[] getSortOptions() {
        return SortOption.values();
    }

    public void updateSortOption(ValueChangeEvent e) {
        //TODO: do something
    }
}
