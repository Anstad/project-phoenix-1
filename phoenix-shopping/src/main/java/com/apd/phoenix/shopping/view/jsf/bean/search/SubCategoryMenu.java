package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.apd.phoenix.shopping.view.jsf.bean.product.Breadcrumb;
import com.google.common.collect.HashMultimap;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
public class SubCategoryMenu implements SolrSearcher {

    private static final Logger logger = LoggerFactory.getLogger(SubCategoryMenu.class);
    private Breadcrumb breadcrumb;
    private List<String> nextLevelMenuEntries;
    private List<String> subList;
    private List<String> extendedList;

    public SubCategoryMenu() {
    };

    public SubCategoryMenu(Map<CategoryMenuNode, HashMultimap<String, String>> categoryData, Breadcrumb breadcrumb) {
        this.breadcrumb = breadcrumb;
        nextLevelMenuEntries = new ArrayList<>();
        int level = breadcrumb.getCrumbs().size() + 1;
        String key = breadcrumb.getCrumbs().isEmpty() ? breadcrumb.getTail() : breadcrumb.getCrumbs().get(breadcrumb.getCrumbs().size() - 1);
        CategoryMenuNode node = new CategoryMenuNode(level, key);
        if (categoryData.get(node) != null) {
        	nextLevelMenuEntries.addAll(categoryData.get(node).get(breadcrumb.getTail()));
        }
        buildSubMenuLists();
    }

    /**
     * Builds two list for storing the next level category links the sublist
     * will hold the first 3 entries, and the extendedList will hold any
     * overflow for display in the dropdown widget
     */
    private void buildSubMenuLists() {
        subList = new ArrayList<>();
        extendedList = new ArrayList<>();
        List<String> sortedNextLevelEntries = new ArrayList<>();
        sortedNextLevelEntries.addAll(nextLevelMenuEntries);
        Collections.sort(sortedNextLevelEntries);
        for (int i = 0; i < sortedNextLevelEntries.size() && i < 3; i++) {
            subList.add(sortedNextLevelEntries.get(i));
        }
        if (sortedNextLevelEntries.size() > 3) {
            for (int i = 3; i < sortedNextLevelEntries.size(); i++) {
                extendedList.add(sortedNextLevelEntries.get(i));
            }
        }
    }

    /**
     * @return url for the subCategoryMenu entries
     */
    public String getUrl(String categoryName) {
        StringBuilder urlPrefix = new StringBuilder(SEARCH_PAGE_BASE_URL);
        urlPrefix.append("?").append(HIERARCHY_PATH_PARAMETER);
        urlPrefix.append("=");
        StringBuilder hierarchyString = new StringBuilder();
        for (String name : breadcrumb.getCrumbs()) {
            hierarchyString.append(HIERARCHY_DELIMITER);
            hierarchyString.append(name);
        }
        hierarchyString.append(HIERARCHY_DELIMITER);
        hierarchyString.append(breadcrumb.getTail());
        hierarchyString.append(HIERARCHY_DELIMITER);
        hierarchyString.append(categoryName);
        String url = hierarchyString.toString();
        try {
            url = URLEncoder.encode(url, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        return urlPrefix.toString() + url;
    }

    /**
     * Return first three next level links
     */
    public List<String> getSubList() {
        return subList;
    }

    /**
     * Return remaining next level links for the dropdown widget
     */
    public List<String> getExtendedList() {
        return extendedList;
    }
}
