package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Cart;
import com.apd.phoenix.service.model.CartItem;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.persistence.jpa.CartDao;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;

@Named
@Stateful
@SessionScoped
@Lock(LockType.READ)
public class ShoppingCartBean implements Serializable {

    private static final long serialVersionUID = 4853497534947535545L;
    //This constant determines the number of items to display when showing the "previous items" window
    private static final int PREVIOUS_ITEMS_TO_DISPLAY = 3;
    private static final String MISSING_PUNCHOUT_LABEL = "Review Order";
    private static final Logger logger = LoggerFactory.getLogger(ShoppingCartBean.class);
    @Inject
    private CartDao cartDao;
    @Inject
    private CredentialSelectionBean credentialBean;
    @Inject
    private ViewUtils viewUtils;
    @Inject
    SearchUtilities searchUtils;
    private List<Product> lastItemsAdded = new ArrayList<>();
    private List<Product> similarItemList;
    //This is a map from the items to the quantity of the item. This map is the "real" cart.
    private Map<Product, Integer> cartQuantity = new HashMap<>();
    //An ordered list of the items. This is only used to store the order that the items were added
    private List<Product> cartList = new ArrayList<>();
    private Map<Product, Product> subsToOriginals = new HashMap<>();
    @Inject 
    private CatalogXItemBp catalogXItemBp;
    @Inject
    CustomerOrderBp customerOrderBp;
    
    @Inject
    private CurrentOrderBean currentOrderBean;
    
    @Inject
    private LoginBean loginBean;

    /**
     * This method is called by the Credential Selection Bean, once a credential is selected.
     */
    public void init() {
    	if (this.preventCartPersistence()) {
    		return;
    	}
        Cart savedCart = findPersistedCart();
        if (savedCart != null) {
            this.loadPersistedCart(savedCart);
        }
    }

    /**
     * This method is called whenever the cart.xhtml page is rendered
     */
    public void retrieve() {
    	retrieveSimilarItems();
    }

    /**
     * This method populates the similarItemList list
     */
    public void retrieveSimilarItems() {
        this.similarItemList = new ArrayList<>();
        if (!cartList.isEmpty()) {
            int index = (int)(Math.random() * ((double)cartList.size()-1) + .5);
            Product random = cartList.get(index);
            CatalogXItem cXItem = searchUtils.getFromProduct(random);
            List<CatalogXItem> similarItems = catalogXItemBp.getCatalogXItemsSimilarItems(cXItem, 5);
            this.similarItemList = viewUtils.convertCatalogXItemsToProducts(similarItems);
        }
    }

    /**
     * Calculates the dollar sum of all the items in the shopping cart
     *
     * @return
     */
    public BigDecimal getSubtotal() {
    	BigDecimal sum = BigDecimal.ZERO;
        for (Product product : this.cartQuantity.keySet()) {
        	if (this.cartQuantity.get(product) != null) {
        		BigDecimal unitPrice = this.getPromisedPriceFromProduct(product);
        		sum = sum.add(unitPrice.multiply(new BigDecimal(this.cartQuantity.get(product).intValue())));
        	}
        }
        return sum;
    }

    /**
     * Persists the shopping cart for later
     */
    public void saveCart() {
        if (this.preventCartPersistence()) {
        	return;
        }
        Cart cart = new Cart();
        cart.resetExpirationDate();
        CartItem cartItem;
        for (Product item : cartQuantity.keySet()) {
            cartItem = new CartItem();
            cartItem.setCatalogXItem(searchUtils.getFromProduct(item));
            cartItem.setQuantity(cartQuantity.get(item));
            cart.getCartItems().add(cartItem);
        }
        cart.setUserCredential(credentialBean.getCurrentAXCXU());
        deletePersistedCart();
        cartDao.create(cart);
    }

    /**
     * Returns an ordered List of the items on the cart.
     *
     * @return
     */
    public List<Product> getCartList() {
        return new ArrayList<Product>(this.cartList);
    }

    /**
     * Returns a Map from an item to the quantity of that item.
     *
     * @return
     */
    public Map<Product, Integer> getCartQuantity() {
        return this.cartQuantity;
    }

    /**
     * Empties the cart, and ensures that the cart won't be automatically 
     * populated the next time the user logs in.
     */
    public void clearCart() {
        this.clearCartBean();
        this.deletePersistedCart();
    }
    
    private void clearCartBean() {
        this.cartList = new ArrayList<>();
        this.cartQuantity = new HashMap<>();
        this.subsToOriginals = new HashMap<>();
    }

    /**
     * Removes an item from the cart.
     *
     * @param item - the item to remove
     */
    public void remove(Product item) {
        if (cartQuantity.containsKey(item)) {
            this.cartList.remove(item);
            this.cartQuantity.remove(item);
        }
    }

    /**
     * Returns the List of similar items, populated by retrieveSimilarItems().
     *
     * @return
     */
    public List<Product> getSimilarItemList() {
        return this.similarItemList;
    }

    /**
     * Returns the last few items that have been added to the cart. The number
     * of items to return is determined by the constant
     * PREVIOUS_ITEMS_TO_DISPLAY.
     *
     * @return
     */
    public List<Product> getLastItemsAdded() {
        lastItemsAdded = new ArrayList<>();
        for (int i = 0; i < PREVIOUS_ITEMS_TO_DISPLAY && i < cartList.size(); i++) {
            lastItemsAdded.add(cartList.get(cartList.size() - 1 - i));
        }
        return lastItemsAdded;
    }
    
    public void addOrderItemsToCart(CustomerOrder addOrder){
        addOrder = customerOrderBp.hydrateForOrderDetails(addOrder);
        for (LineItem lineItem : addOrder.getItems()){
        	if (lineItem.getVendor() != null) {
        		addToCart(viewUtils.getCatalogProductFromVendorNameAndApdSku(lineItem.getVendor().getName(), lineItem.getApdSku()), lineItem.getQuantity());
        	}
        }
    }

    /**
     * Saves the value associated with the item parameter to the shopping cart
     * map.
     *
     * @param item
     * @param quantity
     * @return
     */
    public void addToCart(Product item, int quantity) {
        if (quantity == 0) {
            this.remove(item);
            return;
        }
        if (item != null && item.getId() != null) {
	        if (!this.cartQuantity.containsKey(item)) {
	            cartList.add(item);
	        }
	        this.cartQuantity.put(item, new Integer(quantity));
        }
        else {
        	logger.error("Attempted to add a null product, or product with null ID, to cart.");
        	if (item != null) {
        		logger.error("APD SKU: " + item.getApdSku());
        	}
        }
    }

    /**
     * This method takes a Cart entity, and loads it into the current shopping
     * cart.
     *
     * @param cart - the Cart entity to load
     */
    private void loadPersistedCart(Cart cart) {
        if (this.preventCartPersistence()) {
        	return;
        }
        this.clearCartBean();
        cart = cartDao.eagerLoad(cart);
        List<CatalogXItem> itemList = new ArrayList<>();
        List<Integer> quantityList = new ArrayList<>();
        for (CartItem item : cart.getCartItems()) {
        	itemList.add(item.getCatalogXItem());
        	quantityList.add(item.getQuantity());
        }
        List<Product> productList = viewUtils.convertCatalogXItemsToProducts(itemList);
        for (int i = 0; i < productList.size(); i++) {
        	addToCart(productList.get(i), quantityList.get(i));
        }
    }

    /**
     * This method deletes the persisted cart associated with this
     * accountXCredentialXUser
     */
    public void deletePersistedCart() {
        if (this.preventCartPersistence()) {
        	return;
        }
        Cart toDelete = findPersistedCart();
        if (toDelete != null) {
            cartDao.delete(toDelete.getId(), Cart.class);
        }
    }

    /**
     * This method finds the persisted cart associated with this
     * accountXCredentialXUser
     *
     * @return
     */
    private Cart findPersistedCart() {
        if (this.preventCartPersistence() || credentialBean.getCurrentAXCXU() == null) {
            return null;
        }
        Cart searchCart = new Cart();
        searchCart.setUserCredential(new AccountXCredentialXUser());
        searchCart.getUserCredential().setId(credentialBean.getCurrentAXCXU().getId());
        List<Cart> cartList = cartDao.searchByExactExample(searchCart, 0, 0);

        if (!cartList.isEmpty()) {
            return cartList.get(0);
        }
        return null;
    }
    
    public Map<Product, Product> getSubsToOriginals() {
    	return this.subsToOriginals;
    }
    
    
    public String generatePunchoutCheckoutLabel() {
		String label = credentialBean.getPropertyMap().get("punchoutlabel");
		return StringUtils.isNotEmpty(label) ? label : MISSING_PUNCHOUT_LABEL;
    }
    
    public BigDecimal getPromisedPriceFromCatalogXItem(CatalogXItem catalogXItem) {
    	if (currentOrderBean.getPromisedPrices().containsKey(catalogXItem)) {
    		return currentOrderBean.getPromisedPrices().get(catalogXItem);
    	} else {
    		return catalogXItem.getPrice();
    	}
    }
    
    public BigDecimal getPromisedPriceFromProduct(Product product) {
    	return getPromisedPriceFromCatalogXItem(searchUtils.getFromProduct(product));
    }
    
    private boolean preventCartPersistence() {
    	return loginBean.getSystemUser() != null && loginBean.getSystemUser().getConcurrentAccess();
    }
    
}
