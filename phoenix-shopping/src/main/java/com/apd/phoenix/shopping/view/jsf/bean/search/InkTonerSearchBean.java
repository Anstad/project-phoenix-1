/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.MatchbookBp;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import java.util.Collections;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@ConversationScoped
public class InkTonerSearchBean {

    private static final String INK_TONER_MANUFACTURER = "inkTonerManufacturer";
    private static final String INK_TONER_MODEL = "inkTonerModel";
    private static final Logger logger = LoggerFactory.getLogger(InkTonerSearchBean.class);
    private Manufacturer selectedManufacturer;
    private List<String> models;
    private String selectedModel;
    private static final String SELECT_BRAND_DEFAULT = "Select a brand";
    @Inject
    private MatchbookBp matchbookBp;
    @Inject
    private Conversation conversation;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @PostConstruct
    public void init() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public void search() {
        try {
            Long matchbookId = matchbookBp.searchByModelAndManufacturer(selectedModel, selectedManufacturer).get(0)
                    .getId();
            String browseLink = SearchUtilities.SEARCH_PAGE_BASE_URL + "?" + SearchUtilities.MB_ID_PARAM + "="
                    + matchbookId + "&" + INK_TONER_MODEL + "=" + selectedModel + "&" + INK_TONER_MANUFACTURER + "="
                    + selectedManufacturer;
            FacesContext.getCurrentInstance().getExternalContext().redirect(browseLink);
            conversation.end();
        }
        catch (Exception ex) {
            logger.error("Exception in InkTonerSearchBean#search()", ex);
        }
    }

    public String getSelectedModel() {
        return selectedModel;
    }

    public void setSelectedModel(String selectedModal) {
        this.selectedModel = selectedModal;
    }

    public String getSELECT_BRAND_DEFAULT() {
        return SELECT_BRAND_DEFAULT;
    }

    public void manufacturerValueChanged(ValueChangeEvent e) {
        selectedManufacturer = (Manufacturer)e.getNewValue();
        models = new ArrayList<>();
        for (Matchbook m : matchbookBp.searchByCatalogAndManufacturer(credentialSelectionBean.getCurrentCredential().getCatalog(), selectedManufacturer)) {
            if (StringUtils.isNotBlank(m.getModel())) {
                models.add(m.getModel());
            }
        }
        Collections.sort(models);
    }

    public Manufacturer getSelectedManufacturer() {
        return selectedManufacturer;
    }

    public void setSelectedManufacturer(Manufacturer selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public List<String> getModels() {
        return models;
    }

    public void setModels(List<String> models) {
        this.models = models;
    }

}
