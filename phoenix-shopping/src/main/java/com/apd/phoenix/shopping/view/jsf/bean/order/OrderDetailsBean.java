package com.apd.phoenix.shopping.view.jsf.bean.order;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.MessageMetadataBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.NotificationLimit.LimitType;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.CashoutBean;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.CurrentOrderBean;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.ShoppingCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.InvoicePDFCreator;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.apd.phoenix.web.cashout.CashoutPageContainer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateful
@ConversationScoped
public class OrderDetailsBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(OrderDetailsBean.class);

    private static final long serialVersionUID = 1L;

    @Inject
    private Conversation conversation;

    @Inject
    private CashoutPageContainer pageValues;

    @Inject
    private CashoutPageBp cashoutBp;

    @Inject
    private CredentialSelectionBean credBean;

    @Inject
    private CustomerOrderBp orderBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private InvoicePDFCreator invoicePDFCreator;

    @Inject
    private CashoutBean cashoutBean;

    @Inject
    private CurrentOrderBean currentOrderBean;

    @Inject
    private ShoppingCartBean cartBean;

    @Inject
    private MessageUtils messageUtils;

    @Inject
    private MessageService messageService;

    private CustomerOrder currentOrder;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private ViewUtils viewUtils;

    @Inject
    private ShoppingCartBean shoppingCartBean;

    @Inject
    private MessageMetadataBp messageMetadataBp;

    @Inject
    private CredentialBp credentialBp;

    @Deprecated
    private OrderLog lastNotification;

    private String resendEmailRecipient;

    private Boolean resendEmailIncludeOriginalCC;

    private Boolean resendEmailIncludeOriginalBCC;

    private OrderLog currentNotification;

    private static final List<LimitType> limitTypesToView = Arrays.asList(LimitType.ALL);

    @PostConstruct
    public void create() {
        beginConversation();
    }

    private void endConversation() {
        if (!conversation.isTransient()) {
            conversation.end();
        }
    }

    private void beginConversation() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public CustomerOrder getCurrentOrder() {
        return currentOrder;
    }

    public String setCurrentOrder(CustomerOrder currentOrder) {
        this.currentOrder = orderBp.hydrateForOrderDetails(currentOrder);
        this.lastNotification = this.getLastNotification();
        this.pageValues.init(cashoutBp.getFromCredentialForCashout(this.currentOrder.getCredential()));
        this.pageValues.orderToPage(this.currentOrder);
        return "/ecommerce/order/orderDetails.xhtml?faces-redirect=true";
    }

    public String returnToResults() {
        return "orderHistory?faces-redirect=true";
    }

    public void downloadInvoice() {
        try {
            invoicePDFCreator.downloadInvoice(this.currentOrder);
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage("print-invoice-form:print-invoice-btn",
                    new FacesMessage("This order has not been invoiced"));
        }
    }

    public void viewNotification() {

    }

    @Deprecated
    public void resendLastNotification() {
        if (lastNotification != null) {
            try {
                String recipient = lastNotification.getMessageMetadata().getDestination();
                MessageMetadata clonedData = messageMetadataBp.cloneEntity(lastNotification.getMessageMetadata());
                clonedData.setDestination(recipient);
                Message message = messageUtils.retrieveMessage(clonedData);
                messageService.sendOrderMessage(currentOrder, message, lastNotification.getEventType());
                FacesContext.getCurrentInstance().addMessage(
                        "resend-notification-form:resend-notification-btn",
                        new FacesMessage(FacesMessage.SEVERITY_INFO, "Notification sent successfully",
                                "Notification sent successfully"));
            }
            catch (RuntimeException e) {
                FacesContext.getCurrentInstance().addMessage(
                        "resend-notification-form:resend-notification-btn",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error sending notification",
                                "Error sending notification"));
                logger.error(e.getMessage(), e);
            }
        }
        else {
            FacesContext.getCurrentInstance().addMessage(
                    "resend-notification-form:resend-notification-btn",
                    new FacesMessage(FacesMessage.SEVERITY_WARN, "No notifications associated with this order",
                            "No notifications associated with this order"));
        }
    }

    @Deprecated
    private OrderLog getLastNotification() {
        List<OrderLog> logs = new ArrayList<>();
        for (OrderLog log : currentOrder.getOrderLogs()) {
            if (log.getMessageMetadata() != null) {
                logs.add(log);
            }
        }
        Collections.sort(logs, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                OrderLog l1 = (OrderLog) o1;
                OrderLog l2 = (OrderLog) o2;
                if (l1.getUpdateTimestamp() != null && l2.getUpdateTimestamp() != null) {
                    return new OrderLog.OrderLogComparator().compare(l1, l2);
                } else {
                    Date d1, d2;
                    if ((d1 = l1.getMessageMetadata().getMessageDate()) != null && (d2 = l2.getMessageMetadata().getMessageDate()) != null) {
                        return d1.compareTo(d2);
                    }
                }
                return 0;
            }
            
        });
        return logs.isEmpty() ? null : logs.get(0);
    }

    public CashoutPageContainer getPageValues() {
        return pageValues;
    }

    public void setPageValues(CashoutPageContainer container) {
        this.pageValues = container;
    }

    public void resendNotification(MessageMetadata message) {

    }

    public void viewNotification(MessageMetadata message) {

    }

    public String placeOrder() {
        currentOrderBean.setCurrentOrder(this.currentOrder, false);
        return "/ecommerce/cashout/checkout.xhtml?faces-redirect=true";
    }

    public boolean canReOrder() {
        return customerOrderBp.canReorder(currentOrder);
    }

    public void reOrder() {
        this.currentOrder = orderBp.hydrateForOrderDetails(currentOrder);
        for (LineItem lineItem : this.currentOrder.getItems()) {
            if (lineItem.getItem() != null) {
                CatalogXItem catalogXItem = catalogXItemBp.getCatalogXItem(this.currentOrder.getCredential()
                        .getCatalog(), lineItem.getItem());
                if (catalogXItem != null) {
                    currentOrderBean.getPromisedPrices().put(catalogXItem, lineItem.getUnitPrice());
                    shoppingCartBean.addToCart(viewUtils.getProductFromCatalogXItem(catalogXItem), lineItem
                            .getQuantity());
                }

            }
        }
        currentOrderBean.setCurrentOrder(this.currentOrder, false);
    }

    public List<OrderLog> getOrderNotifications() {
    	List<OrderLog> toReturn = new ArrayList<>();
        Set<EventType> typesToView = this.getMessageEventTypesToView();
    	for (OrderLog log : this.currentOrder.getOrderLogs()) {
    		if (log.getMessageMetadata() != null && typesToView.contains(log.getEventType()) &&
                        com.apd.phoenix.service.model.MessageMetadata.MessageType.EMAIL.equals(log.getMessageMetadata().getMessageType())) {
    			toReturn.add(log);
    		}
    	}
    	Collections.sort(toReturn, new EntityComparator());
    	return toReturn;
    }

    private Set<EventType> getMessageEventTypesToView() {
        Set<EventType> toReturn = new HashSet<>();
        toReturn.addAll(this.credentialBp.getEventTypesWithNotificationLimit(credBean.getCurrentCredential().getId(), limitTypesToView));
        toReturn.remove(EventType.VENDOR_NOTIFICATION);
        return toReturn;
    }

    public List<String> getTrackingNumbers() {
    	List<String> toReturn = new ArrayList<>();
    	for (Shipment shipment : this.currentOrder.getShipments()) {
    		if (StringUtils.isNotBlank(shipment.getTrackingNumber())) {
    			toReturn.add(shipment.getTrackingNumber());
    		}
    	}
    	return toReturn;
    }

    public String getResendEmailRecipient() {
        logger.info("Getting email recipient");
        return resendEmailRecipient;
    }

    public void setResendEmailRecipient(String resendEmailRecipient) {
        logger.info("Setting email recipient: " + resendEmailRecipient);
        this.resendEmailRecipient = resendEmailRecipient;
    }

    public Boolean getResendEmailIncludeOriginalCC() {
        return resendEmailIncludeOriginalCC;
    }

    public void setResendEmailIncludeOriginalCC(Boolean resendEmailIncludeOriginalCC) {
        this.resendEmailIncludeOriginalCC = resendEmailIncludeOriginalCC;
    }

    public Boolean getResendEmailIncludeOriginalBCC() {
        return resendEmailIncludeOriginalBCC;
    }

    public void setResendEmailIncludeOriginalBCC(Boolean resendEmailIncludeOriginalBCC) {
        this.resendEmailIncludeOriginalBCC = resendEmailIncludeOriginalBCC;
    }

    @Inject
    EmailFactoryBp emailFactoryBp;

    public void sendNotification() {
        logger.info("Resending notification to " + this.resendEmailRecipient);
        Message message = messageUtils.retrieveMessage(currentNotification.getMessageMetadata());
        if (currentNotification.getMessageMetadata().getMessageType().equals(MessageType.EMAIL)) {
            message = emailFactoryBp.createOrderEmailToResend(message, this.resendEmailRecipient, false, false);
        }
        messageService.sendOrderMessage(this.currentOrder, message, currentNotification.getEventType());
    }

    public void specifyToResend(OrderLog notification) {
        logger.info("Specifying notification to resend: " + notification.getId());
        this.currentNotification = notification;
        this.resendEmailRecipient = notification.getMessageMetadata().getDestination();
        logger.info("Default destination: " + this.resendEmailRecipient);
    }
}
