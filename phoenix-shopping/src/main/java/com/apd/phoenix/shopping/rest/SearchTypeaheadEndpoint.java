package com.apd.phoenix.shopping.rest;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.HierarchyNodeBp;
import com.apd.phoenix.service.business.ManufacturerBp;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrQueryUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import com.google.gson.Gson;

@Stateless
@Path("/typeahead")
public class SearchTypeaheadEndpoint {

    private static final int MAX_NUMBER_OF_SUGGESTIONS_NEEDED = 10;

    private static final String SOLR_MANUFACTURER_NAME = "mname";

    private static final Logger LOG = LoggerFactory.getLogger(SearchTypeaheadEndpoint.class);

    @Inject
    ManufacturerBp manufacturerBp;

    @Inject
    HierarchyNodeBp hierarchyNodeBp;

    @Inject
    SolrServiceBean solrServiceBean;

    @Inject
    SolrQueryUtilities solrQueryUtilities;

    private SolrServer solr;

    @GET
    @Path("/mainSearch/{catalogId}/{query}/{selectedPath}")
    @Produces("application/json")
    public Response mainSearch(@PathParam("query") String query, @PathParam("selectedPath") String selectedPath, @PathParam("catalogId") Long catalogId) {
    	if(solr == null){
    		solr = solrServiceBean.getSolrServer();
    	}
    	if (selectedPath == null || selectedPath.equals(SolrSearcher.ALL_HIERARCHY_PATH)) {
    		selectedPath = "";
    	}
		List<String> jsonList = new ArrayList<>();
		try {
			query = URLDecoder.decode(query,"UTF-8");
			selectedPath = URLDecoder.decode(selectedPath,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.error(e.getLocalizedMessage());
		}
        for (String listName : getSearchSuggestions(query, selectedPath, catalogId)) {
        	jsonList.add(listName);
        }
        String jsonResponse =  (new Gson()).toJson(jsonList);
        return Response.ok(jsonResponse, MediaType.APPLICATION_JSON).build();
    }

    private List<String> getSearchSuggestions(String query, String selectedPath, Long catalogId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Typeahead request heard.");
        }
        List<String> returnList = new ArrayList<String>();
        returnList = getFacetNames(returnList, SolrSearcher.HIERARCHY_PATH_FACET_VALUE, catalogId, query, selectedPath);
        //        returnList = getFacetNames(returnList, SOLR_MANUFACTURER_NAME, catalogId, query);
        return returnList;
    }

    public List<String> getFacetNames(List<String> returnList, String fieldName, Long catalogId, String searchString,
            String selectedPath) {
        SolrQuery solrQuery = new SolrQuery();
        //solrQuery = solrQuery.setRequestHandler("TermsSearchHandler");
        solrQuery.addFacetField(fieldName);
        //Don't actually need any item data, we're just looking to see if any items exist in the catalog
        solrQuery.setRows(1);
        solrQuery = solrQuery.addFilterQuery(SolrSearcher.UNAVAILABLE_ITEMS_FILTER);
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery, catalogId);
        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set("facet", true);
        params.set("facet.mincount", 1);
        //Use _typeahead field, because need to be case insensitive 
        searchString = searchString.toLowerCase();//.replace(" ", "\\ ");
        params.set("q", fieldName + "_typeahead:" + ClientUtils.escapeQueryChars(SolrSearcher.HIERARCHY_DELIMITER)
                + "*" + ClientUtils.escapeQueryChars(searchString) + "*");
        //filters results by the path selected in the dropdown
        params.set("fq", fieldName + "_typeahead:" + ClientUtils.escapeQueryChars(selectedPath)
                + ClientUtils.escapeQueryChars(SolrSearcher.HIERARCHY_DELIMITER) + "*");
        solrQuery.add(params);
        if (LOG.isDebugEnabled()) {
            LOG.debug(solrQuery.toString());
        }
        QueryResponse response;
        try {
            response = solr.query(solrQuery);
            FacetField facet = response.getFacetField(fieldName);
            if (facet != null) {
                for (Count value : facet.getValues()) {
                    if (returnList.size() > MAX_NUMBER_OF_SUGGESTIONS_NEEDED) {
                        break;
                    }
                    String name = value.getName();
                    if (fieldName.equals(SolrSearcher.HIERARCHY_PATH_FACET_VALUE)) {
                        /* The String will be a hierarchy eg: !eggs!chicken!gradeALarge the typeahead 
                         * only needs the last element. */
                        name = name.substring(name.lastIndexOf(SolrSearcher.HIERARCHY_DELIMITER) + 1);
                        //Some hierarchy paths are actually parent ones and will not contain the relevant search string
                        if (name.toLowerCase().contains(searchString)) {
                            returnList.add(name.trim());
                        }
                    }
                    else {
                        returnList.add(name.trim());
                    }
                }
            }
            else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("No facet found  for " + fieldName + ".");
                }
            }
        }
        catch (SolrServerException e) {
            LOG.error("Could not connect to solr server for typeahead.");
        }
        return returnList;
    }
}
