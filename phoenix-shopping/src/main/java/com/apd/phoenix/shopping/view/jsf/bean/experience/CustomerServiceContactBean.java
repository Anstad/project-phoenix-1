package com.apd.phoenix.shopping.view.jsf.bean.experience;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@SessionScoped
public class CustomerServiceContactBean {

    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceContactBean.class);

    private static final String DEFAULT_CSR_PHONE = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("footer.default.csrphone");

    private static final String DEFAULT_CSR_EMAIL = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("footer.default.csremail");

    private Account account;

    private Credential credential;

    private String csrPhone;

    private String csrEmail;

    @Inject
    private AccountBp accountBp;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    public String getCsrPhone() {
        updateStrings();
        return csrPhone;
    }

    public String getCsrEmail() {
        updateStrings();
        return csrEmail;
    }

    private void updateStrings() {
        Account currentAccount;
        Credential currentCredential;
        if (credentialSelectionBean.getCurrentAXCXU() == null) {
            currentAccount = null;
            currentCredential = null;
        }
        else {
            currentAccount = credentialSelectionBean.getCurrentAXCXU().getAccount();
            currentCredential = credentialSelectionBean.getCurrentAXCXU().getCredential();
        }
        if (StringUtils.isBlank(this.csrEmail) || StringUtils.isBlank(this.csrPhone)) {
            this.csrPhone = DEFAULT_CSR_PHONE;
            this.csrEmail = DEFAULT_CSR_EMAIL;
        }
        if (currentAccount != null && !currentAccount.equals(this.account)) {
            logger.info("Fetching new Customer Service contact information from root account");
            Account rootAccount = this.accountBp.findById(currentAccount.getId(), Account.class).getRootAccount();
            if (rootAccount != null) {
                this.setValues(rootAccount.getCsrPhone(), rootAccount.getCsrEmail());
            }
        }
        if (currentAccount != null && !currentAccount.equals(this.account)) {
            logger.info("Fetching new Customer Service contact information from account");
            this.setValues(currentAccount.getCsrPhone(), currentAccount.getCsrEmail());
        }
        if (currentCredential != null && !currentCredential.equals(this.credential)) {
            logger.info("Fetching new Customer Service contact information from credetial");
            this.setValues(currentCredential.getCsrPhone(), currentCredential.getCsrEmail());
        }
        this.account = currentAccount;
        this.credential = currentCredential;
    }

    private void setValues(String phone, String email) {
        if (StringUtils.isNotBlank(phone)) {
            this.csrPhone = phone;
        }
        if (StringUtils.isNotBlank(email)) {
            this.csrEmail = email;
        }
    }
}
