/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.util.Map;

/**
 *
 * @author dcnorris
 */
public class ItemJsonContainer {

    private String messageHeader;
    private String imgURL;
    private String itemName;
    private String itemQty;
    private String itemPrice;
    private String itemSku;
    private String itemDescription;
    private String itemVendor;
    private Map<String, String> specMap;

    public String getMessageHeader() {
        return messageHeader;
    }

    public void setMessageHeader(String messageHeader) {
        this.messageHeader = messageHeader;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemQty() {
        return itemQty;
    }

    public void setItemQty(String itemQty) {
        this.itemQty = itemQty;
    }

    public String getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(String itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Map<String, String> getSpecMap() {
        return specMap;
    }

    public void setSpecMap(Map<String, String> specMap) {
        this.specMap = specMap;
    }

    public String getItemSku() {
        return itemSku;
    }

    public void setItemSku(String itemSku) {
        this.itemSku = itemSku;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemVendor() {
        return itemVendor;
    }

    public void setItemVendor(String itemVendor) {
        this.itemVendor = itemVendor;
    }

    @Override
    public String toString() {
        return "CartNotification [messageHeader=" + messageHeader + ", imgURL=" + imgURL + ", itemName=" + itemName
                + ", itemQty=" + itemQty + ", itemPrice=" + itemPrice + ", specMap=" + specMap + ", itemDescription="
                + itemDescription + ", itemSku=" + itemSku + "]";
    }
}
