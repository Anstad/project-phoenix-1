package com.apd.phoenix.shopping.camel.process;

import java.util.Date;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class CxmlTransactionLogger {

    @Inject
    MessageService messageService;

    private static final Logger LOG = LoggerFactory.getLogger(CxmlTransactionLogger.class);

    public void logCxmlTransaction(@Header("apdPo") String apdPo,
            @Header("messageEventType") EventTypeDto messageEventType, @Header("payloadId") String payloadId,
            @Header("groupId") String groupdId, @Body String body) {
        LOG.debug("apdPo: {}", apdPo);
        LOG.debug("body: {}", body);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.INBOUND);
        metadata.setContentLength(body.length());
        metadata.setDestination("APD");
        metadata.setFilePath(apdPo + "/" + apdPo + "-cxml-" + System.currentTimeMillis() + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.CXML);
        metadata.setInterchangeId(payloadId);
        metadata.setGroupId(groupdId);
        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(body);

        messageService.receiveOrderMessage(message, apdPo, messageEventType.name());
    }
}
