package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
public class Breadcrumb {

    private static final Logger logger = LoggerFactory.getLogger(Breadcrumb.class);
    private static final String SEARCH_PAGE_BASE_URL = "/shopping/ecommerce/product/browse.xhtml";
    private static final String HIERARCHY_DELIMITER = "!";
    private static final String HIERARCHY_PATH_PARAMETER = "hierarchyPath";
    private List<String> crumbs;
    private String tail;

    public Breadcrumb() {
    }

    public Breadcrumb(String hierarchyPath) {
        if (StringUtils.isNotBlank(hierarchyPath)) {
            parseHierarchyPath(hierarchyPath);
        }
    }

    private void parseHierarchyPath(String hierarchyPath) {
        logger.debug("Info from parseHierarchyPath(), before Parsing of hierarchyPath: " + hierarchyPath);
        String[] pathTokens = hierarchyPath.substring(1, hierarchyPath.length()).split(HIERARCHY_DELIMITER);
        crumbs = new ArrayList<>();
        for (String token : pathTokens) {
            logger.debug("Added to breadcrumb list:" + token);
            crumbs.add(token);
        }
        //Store Tail
        tail = crumbs.get(crumbs.size() - 1);
        //Remove tail
        crumbs.remove(crumbs.size() - 1);
    }

    public List<String> getCrumbs() {
        if (crumbs == null) {
            return new ArrayList<>();
        }
        return crumbs;
    }

    public String getCrumbURL(String crumb) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        try {
            int index = crumbs.indexOf(crumb) + 1;
            url.append("?").append(HIERARCHY_PATH_PARAMETER);
            url.append("=");
            for (int i = 0; i < index; i++) {
                url.append(HIERARCHY_DELIMITER).append(URLEncoder.encode(crumbs.get(i), "UTF-8"));
            }
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        return url.toString();
    }

    public void setCrumbs(List<String> crumbs) {
        this.crumbs = crumbs;
    }

    public String getTail() {
        return tail;
    }
}
