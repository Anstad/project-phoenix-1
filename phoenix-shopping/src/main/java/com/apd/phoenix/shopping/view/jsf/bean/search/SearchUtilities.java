/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

/**
 * Utilities for supporting the Search UI
 */
@Stateless
@LocalBean
public class SearchUtilities implements SolrSearcher {

    private static final Logger logger = LoggerFactory.getLogger(SearchUtilities.class);
    private static final String coreItemIconUrl = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("coreItemIconUrl");
    @Inject
    private CatalogXItemBp catXItemBp;
    @Inject
    private ItemBp itemBp;

    public static String generateRequestParameterString(QueryData queryData) {
        StringBuilder requestParameterStringBuilder = new StringBuilder(SEARCH_PAGE_BASE_URL + "?");
        if (StringUtils.isNotBlank(queryData.getSearchString())) {
            requestParameterStringBuilder.append("searchString=");
            requestParameterStringBuilder.append(queryData.getSearchStringEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getRefineSearchString())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("refineSearchString=");
            requestParameterStringBuilder.append(queryData.getRefineSearchStringEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getSelectedManufacturer())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("selectedManufacturer=");
            requestParameterStringBuilder.append(queryData.getSelectedManufacturerEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getActiveFiltersString())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("filters=");
            requestParameterStringBuilder.append(queryData.getActiveFiltersStringEncoded().toString());
        }
        if (StringUtils.isNotBlank(queryData.getHierarchyPath())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(HIERARCHY_PATH_PARAMETER + "=");
            requestParameterStringBuilder.append(queryData.getHierarchyPathEncoded().toString());
        }
        if (queryData.getLowPrice() != -1) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("lowPrice" + "=");
            requestParameterStringBuilder.append(queryData.getLowPrice());
        }
        if (queryData.getHighPrice() != -1) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("highPrice" + "=");
            requestParameterStringBuilder.append(queryData.getHighPrice());
        }
        if (StringUtils.isNotBlank(queryData.getCompanyFavoritesId())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append("favoritesId" + "=");
            requestParameterStringBuilder.append(queryData.getCompanyFavoritesId());
        }
        if (StringUtils.isNotBlank(queryData.getMatchbookId())) {
            requestParameterStringBuilder.append("&");
            requestParameterStringBuilder.append(MB_ID_PARAM + "=");
            requestParameterStringBuilder.append(queryData.getMatchbookId());
        }
        return requestParameterStringBuilder.toString();
    }

    /**
     * Parses Solr Query Response Object to build a map the available filters
     * The key in this map is the filter category and the value is the filter
     * name
     */
    public static Multimap<String, String> parseFilters(QueryResponse response) {
        Multimap<String, String> facetMap = HashMultimap.create();
        FacetField filterFields = response.getFacetField("filters");
        for (FacetField.Count c : filterFields.getValues()) {
            String[] data = c.getName().split("\\|");
            String filterCategoryName = data[0];
            FacetField catFilterFields = response.getFacetField(getKeyName(filterCategoryName)); //get field corresponding to current category
            String filterName = null;
            if (catFilterFields != null) {
                for (FacetField.Count c2 : catFilterFields.getValues()) { //iterate through each filter field looking for filters in same category
                    String[] data2 = c2.getName().split("\\|");
                    if (filterCategoryName.equals(data2[0])) { //prefix checking for filters in same category
                        filterName = data2[1].trim();
                        if (!"".equals(filterCategoryName) && !"null".equals(filterCategoryName) && filterName != null
                                && !"".equals(filterName) && !"null".equals(filterName)) {
                            String combinedNameCount = filterName + " (" + c2.getCount() + ")";
                            facetMap.put(filterCategoryName, combinedNameCount);
                        }
                    }
                }
            }
            else {
                filterName = data[1].trim();
            }
            if (filterCategoryName != null && !"".equals(filterCategoryName) && !"null".equals(filterCategoryName)
                    && filterName != null && !"".equals(filterName) && !"null".equals(filterName)) {
                String combinedNameCount = filterName + " (" + c.getCount() + ")";
                boolean exists = false;
                if (facetMap.get(filterCategoryName) != null) {
                    for (String s : facetMap.get(filterCategoryName)) {
                        if (s.contains(filterName)) {
                            exists = true;
                        }
                    }
                }
                if (!exists) { //above loop takes precedence in adding filters to facetMap
                    facetMap.put(filterCategoryName, combinedNameCount);
                }
            }
        }
        return facetMap;
    }

    /**
     * Method for parsing the manufacturer facet data from the QueryResponse
     * response The key for the returned HashMap is the manufacturer name, and
     * the value is the count of results tied to that manufacturer
     */
    public static Map<String, Integer> parseManufacturerFacets(QueryResponse response) {
        HashMap<String, Integer> manufacturerWidgetData = new HashMap<>();
        FacetField manufacturerField = response.getFacetField("mname");
        for (FacetField.Count c : manufacturerField.getValues()) {
            manufacturerWidgetData.put(c.getName(), (int) (long) c.getCount());
        }
        return manufacturerWidgetData;
    }

    /**
     * Converts a list of SolrDocuments into a collection of Product objects for
     * use in the UI
     */
    public static List<Product> convertToProductList(SolrDocumentList solrQueryResults) {
        List<Product> products = new ArrayList<>();
        for (SolrDocument doc : solrQueryResults) {
            Product product = new Product();
            product.setId(Long.parseLong((String) doc.getFieldValue(ID_FIELD)));
            product.setName((String) doc.getFieldValue(SOLR_PRODUCT_NAME_FIELD));
            product.setStatus((String) doc.getFieldValue(ITEM_STATUS));
            product.setCustomerSku((String) doc.getFieldValue(CUSTOMER_SKU_FIELD));
            if (StringUtils.isEmpty(product.getCustomerSku())) {
                product.setCustomerSku((String) doc.getFieldValue(APD_SKU_FIELD));
            }
            product.setManufacturerSku((String) doc.getFieldValue(MANUFACTURER_SKU_FIELD));
            product.setVendorSku((String) doc.getFieldValue(VENDOR_SKU_FIELD));
            product.setApdSku((String) doc.getFieldValue(APD_SKU_FIELD));
            product.setDescription((String) doc.getFieldValue(SOLR_PRODUCT_DESCTIPTION_FIELD));
            product.setManufacturerName((String) doc.getFieldValue(SOLR_PRODUCT_MANUFACTURER_NAME_FIELD));
            float priceField = (Float) doc.getFieldValue(SOLR_PRODUCT_PRICE_FIELD);
            product.setPrice(new BigDecimal("" + priceField));
            product.setVendorName((String) doc.getFieldValue(VENDOR_NAME_FIELD));
            Collection<Object> iconNameValues = doc.getFieldValues(ICON_MAP_FIELD);
            Map<String, String> iconMap = new HashMap<>();
            if (iconNameValues != null) {
                for (Object mapValue : iconNameValues) {
                    if (mapValue instanceof String) {
                        String urlMapValue = (String) mapValue;
                        String[] data = urlMapValue.split("\\|");
                        String url = data[0].trim();
                        String toolTipValue = data[1].trim();
                        if (!"null".equals(url)) {
                            iconMap.put(url, toolTipValue);
                        }
                        else {
                            logger.debug("item classification " + toolTipValue + " missing icon URL");
                        }
                    }
                }
            }
            product.setCoreItemStartDate((Date) doc.getFieldValue(CORE_ITEM_START_DATE_FIELD));
            product.setCoreItemExpirationDate((Date) doc.getFieldValue(CORE_ITEM_EXPIRATION_DATE_FIELD));
            if (product.isCoreItem()) {
                iconMap.put(coreItemIconUrl, "Core Item");
            }
            
            product.setIconMap(iconMap);
            Collection<Object> imageUrlValues = doc.getFieldValues(IMAGE_FIELD);
            
            Set<String> imageUrls = new HashSet<>();
            if (imageUrlValues != null) {
                for (Object imageUrl : imageUrlValues) {
                    if (imageUrl instanceof String) {
                        imageUrls.add((String) imageUrl);
                    }
                }
            }
            //If these are no images add a placeholder image
            if (imageUrls.isEmpty()) {
                imageUrls.add(MarketingImagesBean.getImageUrl("searchUtilities1"));
            }
            //For now set first image as main image
            //TODO choose this by the size of the image once that ETL process is determined
            for (String url : imageUrls) {
                product.setMainImageUrl(url);
                break;
            }
            product.setImageUrls(imageUrls);
            product.setUnits((String) doc.getFieldValue(UNIT_OF_MEASURE_NAME));

            try {
	            product.setMultiple(doc.getFieldValue(ITEM_MULTIPLE) != null ? 
	            		((Long) doc.getFieldValue(ITEM_MULTIPLE)).intValue() : null);
	            product.setMinimum(doc.getFieldValue(ITEM_MINIMUM) != null ?
	            		((Long) doc.getFieldValue(ITEM_MINIMUM)).intValue() : null);
            } catch (ClassCastException e) {
            	logger.debug("Error casting item multiples nad minimums, setting to zero");
            	product.setMultiple(1);
            	product.setMinimum(1);
            }
            
            Collection<Object> propertyNameValues = doc.getFieldValues(ITEM_PROPERTY_VALUES_FIELD);
            Map<String, String> properties = new HashMap<>();
            if (propertyNameValues != null) {
                for (Object mapValue : propertyNameValues) {
                    if (mapValue instanceof String) {
                        String urlMapValue = (String) mapValue;
                        String[] data = urlMapValue.split("\\|");
                        String name = data[0].trim();
                        String value = data[1].trim();
                        properties.put(name, value);
                    }
                }
            }
            product.setProperties(properties);
            
            product.setCustomerReplacementSku(doc.getFieldValue(CUSTOMER_REPLACEMENT_SKU) != null ? (String) doc.getFieldValue(CUSTOMER_REPLACEMENT_SKU) : null);
            product.setCustomerReplacementVendor(doc.getFieldValue(CUSTOMER_REPLACEMENT_VENDOR) != null ? (String) doc.getFieldValue(CUSTOMER_REPLACEMENT_VENDOR) : null);
            product.setSpecialOrder(doc.getFieldValue(SPECIAL_ORDER) != null ? (String)doc.getFieldValue(SPECIAL_ORDER) : null);
            product.setCustomOrder(doc.getFieldValue(CUSTOM_ORDER) != null ? (String) doc.getFieldValue(CUSTOM_ORDER) : null);
            
            products.add(product);
        }
        return products;
    }

    /**
     * Replaces characters which will impact regex search with .
     *
     * @return regex safe search string
     */
    public static String removeRegexCharacters(String input) {
        try {
            input = URLDecoder.decode(input, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        String output = input.replace("&", ".");
        output = output.replace('/', '.');
        return output;
    }

    /**
     * Makes a string safe for inputting to solr for a search. Used on the literal strings passed as search input.
     * @param input
     * @return solr safe search string
     */
    public static String makeSolrSafe(String input) {
        return escapeSolrSearchInput(replaceSolrSyntaxWords(input));
    }

    /**
     * Escapes solr syntax characters that throw errors when present in a search string
     * @param input
     * @return escaped search string
     */
    private static String escapeSolrSearchInput(String input) {
        String replacedInput = input.replace("||", "").replace("&&", "").replace("\"", "").replace("+", "").replace(
                "-", "");
        if (replacedInput.isEmpty()) {
            return input.replace("||", "\\||").replace("&&", "\\&&").replace("\"", "\\\"").replace("+", "\\+").replace(
                    "-", "\\-"); //all solr syntax words that throw errors when present in search at time of writing
        }
        else {
            return input;
        }
    }

    /**
     * Makes boolean connective solr syntax terms lowercase so that they are ignored by solr
     * @param input
     * @return string with AND, OR, and NOT replaced with and, or, and not
     */
    public static String replaceSolrSyntaxWords(String input) {
        return input.replace("AND", "and").replace("OR", "or").replace("NOT", "not"); //AND, OR, and NOT are solr syntax, lowercase are not
    }

    public CatalogXItem getFromProduct(Product searchProduct) {
        CatalogXItem toReturn = catXItemBp.findById(searchProduct.getId(), CatalogXItem.class);
        if (toReturn == null
                && EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
            //if the id on the Product doesn't map to a CatalogXItem, gets ANY catalogXItem with this product
            //this block of code should never be reached in production
            logger.warn("ID on Product not found! See getFromProduct method in SearchUtilities.");
            CatalogXItem searchItem = new CatalogXItem();
            searchItem.setItem(itemBp.searchItem(searchProduct.getApdSku(), searchProduct.getVendorName()));
            toReturn = catXItemBp.searchByExactExample(searchItem, 0, 0).get(0);
        }
        return toReturn;
    }

    public static String getTagName(String filterCategory) {
        if (filterCategory != null) {
            return SearchUtilities.makeSolrSafe(filterCategory.replace(" ", "")) + "Filter";
        }
        else {
            return "";
        }
    }

    public static String getKeyName(String filterCategory) {
        if (filterCategory != null) {
            return SearchUtilities.makeSolrSafe(filterCategory.replace(" ", "")) + "Field";
        }
        else {
            return "";
        }
    }
}
