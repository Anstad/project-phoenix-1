/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemSpecification;
import com.apd.phoenix.service.model.SpecificationProperty;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ValidationPropertiesLoader;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@SessionScoped
public class ProductCompareBean {
	
	@Inject
	private ProductCompareSelectionBean selectionBean;
	
	@Inject
	private LinksBean linksBean;
	
	@Inject
	private CatalogXItemBp catXItemBp;
	@Inject
	private SearchUtilities searchUtils;

    private static final int MAX_NUM_ITEMS_TO_COMPARE = 4;
    private static final String MANY_PREFIX = ValidationPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooManyPrefix");
    private static final String MANY_SUFFIX = ValidationPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooManySuffix");
    private static final String FEW = ValidationPropertiesLoader.getInstance().getValidationProperties()
			.getString("browse.compare.tooFew");
    private List<ComparisonItem> compareList = new ArrayList<>();

    private static final Logger logger = LoggerFactory.getLogger(ProductCompareBean.class);

    public void init() {
    	if (selectionBean.getProductsToCompare().size() > MAX_NUM_ITEMS_TO_COMPARE) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    		MANY_PREFIX + " " + MAX_NUM_ITEMS_TO_COMPARE + " " + MANY_SUFFIX, 
                    		MANY_PREFIX + " " + MAX_NUM_ITEMS_TO_COMPARE + " " + MANY_SUFFIX));
    		return;
    	}
    	if (selectionBean.getProductsToCompare().size() < 2) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, 
                    		FEW, 
                    		FEW));
    		return;
    	}
    	
        compareList = new ArrayList<>();

        for (Product product : selectionBean.getProductsToCompare()) {
        	ComparisonItem comp = new ComparisonItem(product);
        	compareList.add(comp);
        }
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        
        try {
			context.redirect(linksBean.getCompare());
		} catch (IOException e) {
			//do nothing
		}
    }

    public List<ComparisonItem> getCompareList() {
        return this.compareList;
    }

    public void remove(int index) {
        compareList.remove(index);
    }

    public List<String> compileUniqueSpecLabels() {
        List<String> list = new ArrayList<>();
        for (ComparisonItem item : compareList) {
            for (String key : item.getProductDetails().keySet()) {
                if (!list.contains(key) && StringUtils.isNotBlank(key)) {
                    list.add(key);
                }
            }
        }
        Collections.sort(list);
        return list;
    }

    public class ComparisonItem {

        private CatalogXItem catalogItem;
        private Product product;
        private Map<String, String> productDetails = new HashMap<>();

        public ComparisonItem(Product product) {
        	this.setProduct(product);
            this.catalogItem = catXItemBp.hydrateForProductDetails(searchUtils.getFromProduct(product));

            buildDetails(catalogItem.getItem());
        }

        public void buildDetails(Item item) {
            if (item != null) {
                for (ItemSpecification itemSpec : item.getItemSpecifications()) {
                    for (SpecificationProperty specProp : itemSpec.getSpecificationProperties()) {
                        if (specProp.getSequence() != null) {
                            productDetails.put(specProp.getName(), specProp.getValue());
                        }
                    }
                }
            }
        }

        public String retrieveValue(String key) {
            if (productDetails.containsKey(key)) {
                return productDetails.get(key);
            }
            else {
                return "not provided";
            }
        }

        public CatalogXItem getCatalogItem() {
            return catalogItem;
        }

        public void setCatalogItem(CatalogXItem catalogItem) {
            this.catalogItem = catalogItem;
        }

		public Product getProduct() {
			return product;
		}

		public void setProduct(Product product) {
			this.product = product;
		}

        public Map<String, String> getProductDetails() {
            return productDetails;
        }

        public void setProductDetails(Map<String, String> productDetails) {
            this.productDetails = productDetails;
        }
    }
}
