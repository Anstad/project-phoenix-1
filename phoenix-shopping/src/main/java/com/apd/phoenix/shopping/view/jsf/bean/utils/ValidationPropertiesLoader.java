package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationPropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(ValidationPropertiesLoader.class);
    private PropertiesConfiguration validationProperties;
    private static final ValidationPropertiesLoader singleton = new ValidationPropertiesLoader();

    public static ValidationPropertiesLoader getInstance() {
        return singleton;
    }

    private ValidationPropertiesLoader() {
        
        try(InputStream is = ValidationPropertiesLoader.class.getClassLoader().getResourceAsStream(
                "com/apd/phoenix/messages/validation.properties")) {
            validationProperties = new PropertiesConfiguration();
            validationProperties.load(is);
        }
        catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"ecommerceProperties.properties\": ", ex);
        }
    }

    public PropertiesConfiguration getValidationProperties() {
        return validationProperties;
    }
}
