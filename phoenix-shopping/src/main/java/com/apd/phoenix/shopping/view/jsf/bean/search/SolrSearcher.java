/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.apd.phoenix.core.utility.EcommercePropertiesLoader;

/**
 *
 * @author dcnorris
 */
public interface SolrSearcher {

    static final String solrUrl = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString("solrUrl");
    static final String HIERARCHY_PATH_FACET_VALUE = "hierarchy_path";
    static final String MATCH_ALL_QUERY = "*:*";
    static final String HIERARCHY_REGEX_ARG = ".*";
    static final String SOLR_PRODUCT_MANUFACTURER_NAME_FIELD = "mname";
    static final String INNER_FILTER_DELIMITER = "| ";
    static final String INNER_FILTER_DELIMITER_SPLITTER = "\\| ";
    static final String DELIMITER = "|||";
    static final String DELIMITER_SPLITTER = "\\|\\|\\|";
    static final String SEARCH_PAGE_BASE_URL = "/shopping/ecommerce/product/browse.xhtml";
    static final String SOLR_PRODUCT_NAME_FIELD = "name";
    static final String NAME_SORT_FIELD = "nameSort";
    static final String SOLR_PRODUCT_DESCTIPTION_FIELD = "description";
    static final String SOLR_PRODUCT_PRICE_FIELD = "price";
    static final String HIERARCHY_PATH_PARAMETER = "hierarchyPath";
    static final String HIERARCHY_DELIMITER = "!";
    static final String MANUFACTURER_SKU_FIELD = "MANUFACTURER_SKU";
    static final String APD_SKU_FIELD = "APD_SKU";
    static final String APD_SKU_FIELD_S = APD_SKU_FIELD + "_s";
    static final String VENDOR_SKU_FIELD = "VENDOR_SKU";
    static final String CUSTOMER_SKU_FIELD = "CUSTOMER_SKU";
    static final String COMPANY_FAVORITES_FIELD = "companyFavoritesListIds";
    static final String CUSTOMER_SKU_FIELD_S = CUSTOMER_SKU_FIELD + "_s";
    static final String VENDOR_NAME_FIELD = "vendorName";
    static final String IMAGE_FIELD = "imageUrl";
    static final String ID_FIELD = "catalogxitemID";
    static final String ICON_MAP_FIELD = "iconMap";
    static final String ICON_DELIMETER = "|";
    static final String CORE_ITEM_START_DATE_FIELD = "coreitemstartdate";
    static final String CORE_ITEM_EXPIRATION_DATE_FIELD = "coreitemexpirationdate";
    static final String UNIT_OF_MEASURE_NAME = "UNIT_OF_MEASURE_NAME";
    static final String ITEM_PROPERTY_VALUES_FIELD = "ItemPropertyNameValuePair";
    static final String SOLR_OR_DELIMETER = " OR ";
    static final String ITEM_STATUS = "item_status";
    static final String MB_ID_FIELD = "mb_id";
    static final String MB_ID_PARAM = "mb";
    static final String FIELD_IS_NOT_NULL = "['' TO *]";
    static final String UNAVAILABLE_ITEM_PROPERTY = "\"availability | no\"";
    static final String UNAVAILABLE_ITEMS_FILTER = "NOT(" + ITEM_PROPERTY_VALUES_FIELD + ":"
            + UNAVAILABLE_ITEM_PROPERTY + ")";
    static final String ITEM_MULTIPLE = "item_multiple";
    static final String ITEM_MINIMUM = "item_minimum";
    static final String CUSTOMER_REPLACEMENT_SKU = "customerReplacementSku";
    static final String CUSTOMER_REPLACEMENT_VENDOR = "customerReplacementVendor";
    static final String SPECIAL_ORDER = "specialOrder";
    static final String CUSTOM_ORDER = "customOrder";
    static final String ALL_HIERARCHY_PATH = "!All";
}
