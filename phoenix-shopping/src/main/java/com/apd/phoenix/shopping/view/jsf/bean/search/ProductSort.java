/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import javax.faces.model.SelectItem;

/**
 *
 * @author dcnorris
 */
public class ProductSort {

    private SortOption selectedSortBy = SortOption.BestMatch;
    private int selectedSortByIndex;

    public enum SortOption {

        BestMatch("Best Match"), SortByProductNameAsc("Sort By Product Name A-Z"), SortByProductNameDesc(
                "Sort By Product Name Z-A"), SortBySkuAsc("Sort By Sku A-Z"), SortBySkuDesc("Sort By Sku Z-A"), SortByManufacturerAsc(
                "Sort By Manufacturer A-Z"), SortByManufacturerDesc("Sort By Manufacturer Z-A"), SortByPriceAsc(
                "Sort By Price Low-High"), SortByPriceDesc("Sort By Price High-Low");

        private final String label;

        private SortOption(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

    public SelectItem[] getSortByOptions() {
        SelectItem[] items = new SelectItem[SortOption.values().length];
        int i = 0;
        for (SortOption option : SortOption.values()) {
            items[i++] = new SelectItem(option, option.getLabel());
        }
        return items;
    }

    public SortOption getSelectedSortBy() {
        return selectedSortBy;
    }

    public void setSelectedSortBy(SortOption selectedSortBy) {
        this.selectedSortBy = selectedSortBy;
    }

    public int getSelectedSortByIndex() {
        return selectedSortByIndex;
    }

    public void setSelectedSortByIndex(int selectedSortByIndex) {
        this.selectedSortByIndex = selectedSortByIndex;
        int i = 0;
        for (SortOption option : SortOption.values()) {
            if (i == selectedSortByIndex) {
                setSelectedSortBy(option);
                break;
            }
            i++;
        }
    }
}
