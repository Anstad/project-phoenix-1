package com.apd.phoenix.shopping.oci.request.processors;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.MessageMetadata.PunchoutType;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.shopping.oci.OCIRequestWrapper;
import com.apd.phoenix.shopping.oci.request.CallUpCatalogRequest;

@Stateless
public class CallUpCatalogRequestProcessor extends OCIRequestProcessor<CallUpCatalogRequest> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallUpCatalogRequestProcessor.class);

    private static final String QUOTE_DATE_FORMAT = "yyyy-MM-dd-HH-mm-ss:zzz";
    private static final String QUOTE_PREFIX = "APD_QUOTE_";

    @Inject
    AccountXCredentialXUserBp axcxuBp;

    @Inject
    PunchoutSessionBp punchoutSessionBp;

    @Inject
    SystemUserBp systemUserBp;

    @Override
    public void processRequest(CallUpCatalogRequest request) throws OciProcessorException {
        PunchoutSession punchOutSession = createPunchoutSessionEntity(request);
        if (punchOutSession != null) {
            String redirectUrl = request.getResponse().encodeRedirectURL(
                    BASE_PUNCHOUT_LOGIN_PAGE + punchOutSession.getSessionToken());
            try {
                request.getResponse().sendRedirect(redirectUrl);
            }
            catch (IOException e) {
                LOGGER.error("An error occured:", e);
            }
        }
    }

    private PunchoutSession createPunchoutSessionEntity(CallUpCatalogRequest request) throws OciProcessorException {
        SimpleDateFormat df = new SimpleDateFormat(QUOTE_DATE_FORMAT);
        List<SystemUser> searchResults = systemUserBp.getSystemUserByLogin(request
                .getParameter(OCIRequestWrapper.REQUEST_PARAM_USERNAME));
        //TODO: DAO lookup should return at most 1 result!
        if (searchResults.size() != 1) {
            throw new OciProcessorException("User is not unique in the system!");
        }
        SystemUser user = searchResults.get(0);
        List<AccountXCredentialXUser> axcxus = axcxuBp.getAllAXCXU(user);
        AccountXCredentialXUser ociCredential = null;
        for (AccountXCredentialXUser axcxu : axcxus) {
            if (axcxu.getCredential().getPunchoutType() != null
                    && axcxu.getCredential().getPunchoutType().equals(PunchoutType.OCI)) {
                ociCredential = axcxu;
            }
        }
        if (ociCredential == null) {
            throw new OciProcessorException("No credential for OCI punchout could be found for user.");
        }

        PunchoutSession session = new PunchoutSession();
        session.setBuyerCookie(QUOTE_PREFIX + df.format(new Date()));
        session.setSystemUserLoginName(request.getParameter(OCIRequestWrapper.REQUEST_PARAM_USERNAME));
        String browserFormPostUrl = request.getParameter(OCIRequestWrapper.REQUEST_PARAM_RETURN_URL);
        String target = request.getParameter(OCIRequestWrapper.REQUEST_PARAM_TARGET);

        //TODO discuss with Jamie
        if (StringUtils.isBlank(browserFormPostUrl)) {
            //            browserFormPostUrl = ociCredential.getCredential().getPunchoutExitUrl();
        }
        session.setBrowserFormPostTargetFrame(StringUtils.isNotBlank(target) ? target
                : OCIRequestWrapper.REQUEST_PARAM_TARGET_VALUE);
        session.setBrowserFormPostUrl(browserFormPostUrl);
        PunchoutSession existingSession;
        try {
            //TODO: DAO lookup should return at most 1 result!

            List<PunchoutSession> matchingPunchoutSessions = punchoutSessionBp.searchByExactExample(session, 0, 0);
            existingSession = matchingPunchoutSessions.get(0);
            session = existingSession;
        }
        catch (Exception ex) {
            session.setAccntCredUser(ociCredential);
            try {
                punchoutSessionBp.createPunchoutSession(session);
            }
            catch (Exception e) {
                String errorMessage = "Error persisting PunchoutSession entity";
                LOGGER.error(errorMessage, e);
                throw new OciProcessorException(e);
            }
        }
        return session;
    }

}
