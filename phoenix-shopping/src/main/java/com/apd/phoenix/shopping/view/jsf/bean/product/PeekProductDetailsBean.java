package com.apd.phoenix.shopping.view.jsf.bean.product;

import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author anicholson
 */
@RequestScoped
@Named
public class PeekProductDetailsBean {

    private static final Logger logger = LoggerFactory.getLogger(PeekProductDetailsBean.class);
    private String apdSku;
    private String vendorName;
    private Product product;
    private static final String SPACER = "  ";
    private Item item;
    private ItemImage selectedItemImage;
    private List<ItemImage> itemImages;
    private Map<String, String> specifications;
    private Map<String, String> classifications;
    @Inject
    private ViewUtils viewUtils;
    @Inject
    private ItemBp itemBp;
    @Inject
    private LinksBean linksBean;

    public void retrieve() {
        product = retrieveProductInfo();
        try {
            item = retrieveItem();
            classifications = retrieveItemClassifications();
            specifications = retrieveItemSpecifications();
            itemImages = retrieveItemImages();
        }
        catch (Exception e) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(linksBean.getItemNotFound());
            }
            catch (IOException ex) {
                logger.error("IOException during redirect to product not found page.", ex);
            }
        }
    }

    private Item retrieveItem() {
        item = itemBp.searchByExactExample(itemBp.searchItem(apdSku, vendorName), 0, 0).get(0);
        item = itemBp.eagerLoad(item);
        return item;
    }

    private Product retrieveProductInfo() {
        return viewUtils.getProductFromVendorNameAndApdSku(vendorName, apdSku);
    }

    private Map<String, String> retrieveItemClassifications() {
        return item.getClassificationStringMap();
    }

    private Map<String, String> retrieveItemSpecifications() {
        return item.getSpecifications();
    }

    private List<ItemImage> retrieveItemImages() {
        return new ArrayList<>(item.getItemImages());
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public ItemImage getSelectedItemImage() {
        return selectedItemImage;
    }

    public void setSelectedItemImage(ItemImage selectedItemImage) {
        this.selectedItemImage = selectedItemImage;
    }

    public List<ItemImage> getItemImages() {
        return itemImages;
    }

    public void setItemImages(List<ItemImage> itemImages) {
        this.itemImages = itemImages;
    }

    public Map<String, String> getSpecifications() {
        return specifications;
    }

    public void setSpecifications(Map<String, String> specifications) {
        this.specifications = specifications;
    }

    public Map<String, String> getClassifications() {
        return classifications;
    }

    public void setClassifications(Map<String, String> classifications) {
        this.classifications = classifications;
    }

    public String getProductName() {
        return product.getName();
    }

    public Product getProduct() {
        if (product == null) {
            reinit();
        }
        return product;
    }

    public String getProductDescription() {
        return product.getDescription();
    }

    public String getProductManufacturer() {
        return product.getManufacturerSku();
    }

    public String getProductSku() {
        //Note customer sku will be the same as the apdSku if a true customer sku did not exist
        return product.getCustomerSku();
    }

    public List<String> getItemSpecificationKeys() {
        if (specifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(specifications.keySet());
    }

    public String getItemSpecificationValue(String key) {
        return specifications.get(key);
    }

    public List<String> getItemClassificationKeys() {
        if (classifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(classifications.keySet());
    }

    public String getItemClassificationValue(String key) {
        return classifications.get(key);
    }

    private void reinit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
