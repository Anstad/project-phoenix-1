/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean;

import com.apd.phoenix.core.StringEscape;
import java.io.Serializable;
import java.util.Properties;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.IpSubdomainRestriction;

/**
 * This class is used by the ecommerceMainTemplate.xthml template page to get
 * the appropriate css file for a specific customer.
 * <br /><br />
 * To set a CSS file for a particular account, save the file in
 * phoenix-shopping/src/main/webapp/resources/css/main, then add a property to
 * cssMap.properties and cssMapResponsive.properties, from the root account name
 * to the path of the CSS file.
 *
 * @author RHC
 *
 */
@Named
@Stateful
@RequestScoped
public class PageCssBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private static String STATIC_CONTENT_URL = PropertiesLoader.getAsProperties("aws.integration").getProperty(
            "content.service.secure.domain");
    private static String CUSTOM_CSS_URL = STATIC_CONTENT_URL + "/phoenix-css/";
    public static String SUBDOMAIN_IMAGES = STATIC_CONTENT_URL + "/images/";
    public static String SUBDOMAIN_WELCOME_IMAGES = SUBDOMAIN_IMAGES + "welcome-images/";
    public static String SUBDOMAIN_LOGOS = SUBDOMAIN_IMAGES + "logos/";

    private static final String SUBDOMAIN_CSS_LOCATION = "css/subDomain/";
    private static final String CSS_FILE_EXTENSION = ".css";

    private static final String DEFAULT_CUSTOMER_SERVICE = "Customer Service: 800-849-5842 | sales@americanproduct.com";
    private static final String DEFAULT_PHONE = "800-849-5842";
    private static final String DEFAULT_DAYS = "Monday-Friday";
    private static final String DEFAULT_HRS = "8:00 am-8:00 pm est";
    private static final String DEFAULT_EMAIL = "sales@americanproduct.com";
    private static final String DEFAULT_WELCOME_IMAGE = "default.png";
    private static final String DEFAULT_VISIT_US_URL = "http://www.americanproduct.com";
    private static final String DEFAULT_VISIT_US_URL_TO_DISPLAY = "www.AmericanProduct.com";
    private static final String DEFAULT_FACEBOOK = "http://www.facebook.com/americanproduct";
    private static final String DEFAULT_LINKEDIN = "http://www.linkedin.com/company/american-product-distributors-inc-";
    private static final String DEFAULT_YOUTUBE = "http://www.youtube.com/user/TheAPDStory";
    private static final String DEFAULT_WORDPRESS = "http://americanproduct.wordpress.com/";
    private static final String DEFAULT_COPYRIGHT = "Copyright 2015 American Product Distributors Inc.";

    @Inject
    private AccountBp accountBp;

    public String getSubDomainBasedCssFile() {
        String cssKey = getSubdomainValue();
        if (cssKey.isEmpty() || cssKey.equals(IpSubdomainRestriction.GENERIC_SUBDOMAIN)) {
            return "";
        }
        return CUSTOM_CSS_URL + SUBDOMAIN_CSS_LOCATION + StringEscape.escapeForUrl(cssKey) + CSS_FILE_EXTENSION;
    }

    public String getSubdomainValue() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
                .getRequest();
        return IpSubdomainRestriction.ipAndSubdomain(request)[1];
    }

    public String getSubDomainWelcomeMessage() {
        return this.getSubdomainSpecificProperty("_WelcomMessage", "");
    }

    public String getSubDomainWelcomeMessageHeader() {
        return this.getSubdomainSpecificProperty("_WelcomMessageHeader", "");
    }

    public String getSubDomainWelcomeImage() {
        return SUBDOMAIN_WELCOME_IMAGES + this.getSubdomainSpecificProperty("_WelcomeImage", DEFAULT_WELCOME_IMAGE);
    }

    public String getSubDomainLogo() {
        String imageName = this.getSubdomainSpecificProperty("_Logo", "");
        if (StringUtils.isNotEmpty(imageName)) {
            return SUBDOMAIN_LOGOS + imageName;
        }
        else {
            return "";
        }
    }

    public String getCustomerServiceInfo() {
        return this.getSubdomainSpecificProperty("_customerServiceInfo", DEFAULT_CUSTOMER_SERVICE);
    }

    public String getCallUsPhone() {
        return this.getSubdomainSpecificProperty("_callUsPhone", DEFAULT_PHONE);
    }

    public String getCallUsDays() {
        return this.getSubdomainSpecificProperty("_callUsDays", DEFAULT_DAYS);
    }

    public String getCallUsHrs() {
        return this.getSubdomainSpecificProperty("_callUsHrs", DEFAULT_HRS);
    }

    public String getEmailUsAddress() {
        return this.getSubdomainSpecificProperty("_emailUsAddress", DEFAULT_EMAIL);
    }

    public String getVisitUsSite() {
        return this.getSubdomainSpecificProperty("_visitUsSite", DEFAULT_VISIT_US_URL);
    }

    public String getVisitUsSiteToDisplay() {
        return this.getSubdomainSpecificProperty("_visitUsSiteToDsiplay", DEFAULT_VISIT_US_URL_TO_DISPLAY);
    }

    public String getFaceBook() {
        return this.getSubdomainSpecificProperty("_facebook", DEFAULT_FACEBOOK);
    }

    public String getLinkedIn() {
        return this.getSubdomainSpecificProperty("_linkedin", DEFAULT_LINKEDIN);
    }

    public String getYouTube() {
        return this.getSubdomainSpecificProperty("_youtube", DEFAULT_YOUTUBE);
    }

    public String getWordPress() {
        return this.getSubdomainSpecificProperty("_wordpress", DEFAULT_WORDPRESS);
    }

    public String getCopyRight() {
        return this.getSubdomainSpecificProperty("_copyright", DEFAULT_COPYRIGHT);
    }

    private String getSubdomainSpecificProperty(String propertyName, String defaultValue) {
        String prefix = getSubdomainValue();
        String key = prefix + propertyName;
        Properties properties = PropertiesLoader.getAsProperties("themeMap");
        if (properties != null && properties.containsKey(key)) {
            return properties.getProperty(key, "");
        }
        else {
            return defaultValue;
        }
    }

    public void validateEmailDomain(FacesContext context, UIComponent component, Object value) {
        if (value != null && !accountBp.validEmailAddress(value.toString(), getSubdomainValue())) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                    "registration.email.invalid.summary"));
            message.setDetail(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                    "registration.email.invalid.detail"));
            context.addMessage(null, message);
        }
    }
}
