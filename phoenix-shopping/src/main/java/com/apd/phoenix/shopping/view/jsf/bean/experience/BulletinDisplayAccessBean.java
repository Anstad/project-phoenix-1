package com.apd.phoenix.shopping.view.jsf.bean.experience;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.BulletinBp;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;

/**
 * This request-scoped bean is a helper bean for BulletinDisplayBean. It loads the bulletin list from persistence, 
 * saving the list in memory, and returns the list.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@RequestScoped
public class BulletinDisplayAccessBean implements Serializable {

    private static final long serialVersionUID = 41743704237803173L;

    private List<Bulletin> bulletinList;

    @Inject
    private BulletinBp bulletinBp;

    @Inject
    private CredentialSelectionBean credBean;

    @Inject
    private LoginBean loginBean;

    public boolean hasLoadedBulletins() {
        return bulletinList != null;
    }

    /**
     * Returns the list of bulletins that should be displayed. If they have not been loaded, loads them from 
     * persistence; otherwise, returns the bulletins loaded into memory.
     * 
     * @return
     */
    public List<Bulletin> getBulletinList() {
        if (bulletinList == null) {
            //if the list is null, loads form memory
            if (credBean.getCurrentAXCXU() != null) {
                //if there is an AXCXU, uses it to load bulletins
                bulletinList = bulletinBp.getBulletinsToDisplay(credBean.getCurrentAXCXU());
            }
            else {
                //if there is no AXCXU, just gets the bulletins for the user
                bulletinList = bulletinBp.getBulletinsToDisplay(loginBean.getSystemUser());
            }
            if (bulletinList == null) {
                bulletinList = new ArrayList<Bulletin>();
            }
        }
        return bulletinList;
    }
}
