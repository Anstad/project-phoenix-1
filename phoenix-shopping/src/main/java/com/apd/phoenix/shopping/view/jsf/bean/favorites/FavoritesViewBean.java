/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.favorites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import java.io.IOException;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@LocalBean
@SessionScoped
public class FavoritesViewBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(FavoritesViewBean.class);
    private static final String COMPANY_FAVORITES_TYPE = "COMPANY FAVORITES";
    private static final String PERSONAL_FAVORITES_TYPE = "MY FAVORITES";
    private AccountXCredentialXUser user;
    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private AccountXCredentialXUserBp accountXCredUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private CatalogBp catalogBp;
    private String listName;
    private String listType;
    private List<Product> products;
    private int previousPage;
    private PageNumberDisplay pageDisplay;
    private int numFound;
    private PageNumberDisplay.PageSize pageSize = PageNumberDisplay.PageSize.FIVE;
    @Inject
    private ViewUtils viewUtils;
    private long credentialId;
    private String pageDisplayListName;
    private String pageDisplayListType;
    private Integer pageViewParam;
    private int pageSizeIndex;
    @Inject
    private FavoritesBean favoritesBean;
    private int currentPage = 1;

    public void init() {
        if (!FacesContext.getCurrentInstance().isPostback()) {
            pageDisplay = new PageNumberDisplay(numFound);
            pageDisplay.setPageSize(pageSize);
            pageDisplay.setPage(1);
            if (pageViewParam != null) {
                pageDisplay.setPage(pageViewParam);
                pageViewParam = null;
            }
            retrieveProducts();
        }
        credentialId = credentialSelectionBean.getCurrentCredential().getId().longValue();
    }

    public boolean isPersonalFavoritesType(String favoritesType) {
        return PERSONAL_FAVORITES_TYPE.equals(favoritesType);
    }

    public boolean isCompanyFavoritesType(String favoritesType) {
        return COMPANY_FAVORITES_TYPE.equals(favoritesType);
    }

    public String getListName() {
        return listName;
    }

    public String getUpperCaseListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListType() {
        return listType;
    }

    public void setListType(String listType) {
        this.listType = listType;
    }

    public List<Product> getProducts() {
        if (pageDisplay.getPage() != previousPage) {
            previousPage = pageDisplay.getPage();
            retrieveProducts();
        }
        return products;
    }

    private void retrieveProducts() {
        if (StringUtils.isEmpty(listName) || StringUtils.isEmpty(listType)) {
            return;
        }
        if (credentialSelectionBean.getCurrentAXCXU() != null && user == null) {
            if (listType.equals(PERSONAL_FAVORITES_TYPE)) {
                long userID = credentialSelectionBean.getCurrentAXCXU().getId();
                List<FavoritesList> userFavorites = accountXCredUserBp.getUnInitializedFavoritesLists(userID);
                for (FavoritesList list : userFavorites) {
                    if (listName.equals(list.getName())) {
                        int start = 0;
                        if (pageDisplay != null) {
                            start = (pageDisplay.getPage() - 1) * pageDisplay.getSize();
                            currentPage = pageDisplay.getPage();
                        }
                        Set<CatalogXItem> temp = favoritesListBp.getInitializedSet(
                                list.getId(), start, pageSize.toInt());
                        numFound = favoritesListBp.getListItemCount(list.getId());
                        products = viewUtils.convertCatalogXItemsToProducts(new ArrayList<>(temp));
                        break;
                    }
                }
            }
            else if (listType.equals(COMPANY_FAVORITES_TYPE)) {
                List<FavoritesList> companyFavorites = catalogBp.getFavoritesLists(credentialId);
                for (FavoritesList list : companyFavorites) {
                    if (listName.equals(list.getName())) {
                        int start = 0;
                        if (pageDisplay != null) {
                            start = (pageDisplay.getPage() - 1) * pageDisplay.getSize();
                            currentPage = pageDisplay.getPage();
                        }
                        Set<CatalogXItem> temp = favoritesListBp.getInitializedSet(
                                list.getId(), start, pageSize.toInt());
                        numFound = favoritesListBp.getListItemCount(list.getId());
                        products = viewUtils.convertCatalogXItemsToProducts(new ArrayList<>(temp));
                        break;
                    }
                }
            }
        }
        pageDisplay = new PageNumberDisplay(numFound);
        pageDisplay.setPageSize(pageSize);
        pageDisplay.setPage(currentPage);
//        if (pageDisplay == null 
//                || pageDisplay.getResultQuantity() != numFound //instantiate new pageDisplay if list size changes
//                || !listName.equals(pageDisplayListName) //instantiate new pageDisplay if list name changes
//                || !listType.equals(pageDisplayListType)) { //instantiate new pageDisplay if list type changes
//            pageDisplay = new PageNumberDisplay(numFound);
//            pageDisplay.setPageSize(pageSize);
        pageDisplayListName = listName;
        pageDisplayListType = listType;
//        }
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getCOMPANY_FAVORITES_TYPE() {
        return COMPANY_FAVORITES_TYPE;
    }

    public String getPERSONAL_FAVORITES_TYPE() {
        return PERSONAL_FAVORITES_TYPE;
    }

    public AccountXCredentialXUser getUser() {
        return user;
    }

    public void setUser(AccountXCredentialXUser user) {
        this.user = user;
    }

    public int getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(int previousPage) {
        this.previousPage = previousPage;
    }

    public PageNumberDisplay getPageDisplay() {
        return pageDisplay;
    }

    public void setPageDisplay(PageNumberDisplay pageDisplay) {
        this.pageDisplay = pageDisplay;
    }

    public int getNumFound() {
        return numFound;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageSizeIndex() {
        return pageSizeIndex;
    }

    public void setPageSizeIndex(int pageSizeIndex) {
        this.pageSizeIndex = pageSizeIndex;
        int i = 0;
        for (PageNumberDisplay.PageSize p : PageNumberDisplay.PageSize.values()) {
            if (i == pageSizeIndex) {
                setPageSize(p);
                break;
            }
            i++;
        }
    }

    public void refreshPage() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(
                    favoritesBean.getViewLink(pageDisplayListName, 0, PERSONAL_FAVORITES_TYPE));
        }
        catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
    }

    public String getPageNumber() {
        return Integer.toString(this.pageDisplay.getPage());
    }

    public Integer getPageViewParam() {
        return pageViewParam;
    }

    public void setPageViewParam(Integer pageViewParam) {
        this.pageViewParam = pageViewParam;
    }

}
