package com.apd.phoenix.shopping.exceptions;

import java.io.IOException;
import java.util.Iterator;
import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.context.FacesContext;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class CustomExceptionHandler extends ExceptionHandlerWrapper {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);
    private ExceptionHandler wrapped;

    CustomExceptionHandler(ExceptionHandler exception) {
        this.wrapped = exception;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {

        final Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
        while (i.hasNext()) {
            ExceptionQueuedEvent event = i.next();
            ExceptionQueuedEventContext context = (ExceptionQueuedEventContext) event.getSource();

            // get the exception from context
            Throwable t = context.getException();
            final FacesContext fc = FacesContext.getCurrentInstance();

            //here you do what ever you want with exception
            try {
                //log error
                logger.error("Critical Exception!", t);

                try {
                    fc.getExternalContext().redirect("/shopping/ecommerce/errors/error.xhtml");
                    fc.getExternalContext().invalidateSession();
                }
                catch (IOException ex) {
                    logger.error("IOException", ex);
                }

            }
            finally {
                //remove it from queue
                i.remove();
            }
        }
        //parent hanle
        getWrapped().handle();
    }
}