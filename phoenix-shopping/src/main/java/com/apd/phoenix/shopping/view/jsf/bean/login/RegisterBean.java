package com.apd.phoenix.shopping.view.jsf.bean.login;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateful
@ConversationScoped
public class RegisterBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegisterBean.class);
    private String login;
    private String email;
    private String password;
    private String confirm;
    private String first;
    private String last;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public void register() {
        //TODO: link to BRMS
        LOGGER.info("Clicked register!");
    }
}
