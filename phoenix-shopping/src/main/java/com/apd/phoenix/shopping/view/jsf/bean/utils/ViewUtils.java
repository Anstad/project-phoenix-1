package com.apd.phoenix.shopping.view.jsf.bean.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Item.ItemStatus;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrQueryUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Utilities for presentation layer.
 */
@Named
@LocalBean
@Stateless
public class ViewUtils implements SolrSearcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewUtils.class);
    private static final String APD_SKU_NAME = "APD";
    @Inject
    private SolrQueryUtilities solrQueryUtilities;
    private SolrServer solr;
    @Inject
    private SolrServiceBean solrServiceBean;
    @Inject
    private CatalogXItemBp catalogXItemBp;

    public static <T> List<T> asList(Collection<T> collection) {

        if (collection == null) {
            return null;
        }

        List<T> toReturn = new ArrayList<>(collection);

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }

    public <T, S> List<Map.Entry<T, S>> mapToList(Map<T, S> map) {
        if (map == null) {
            return null;
        }
        List<Map.Entry<T, S>> list = new ArrayList<>();
        list.addAll(map.entrySet());
        return list;
    }

    public List<Product> convertCatalogXItemsToProducts(List<CatalogXItem> items) {
        if (items == null) {
            return null;
        }
        List<Product> products = new ArrayList<>();
        for (CatalogXItem catXItem : items) {
            Product p = getProductFromCatalogXItem(catXItem);
            if (p != null && !"no".equals(p.getProperties().get("availability"))) {
                products.add(p);
            }
        }
        return products;
    }

    public Product getProductFromCatalogXItem(CatalogXItem catXItem) {
        if (catXItem == null) {
            return null;
        }
        Product product = null;
        solr = solrServiceBean.getSolrServer();
        SolrQuery solrQuery = new SolrQuery();
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        try {
            solrQuery.set("q", ID_FIELD + ":" + catXItem.getId());
            QueryResponse response = solr.query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                product = products.get(0);
            }
            if (EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
                //Will be reached when solr index is not in sync with local development db instance
                LOGGER
                        .error("This code should not execute in a production environment.  It is for development testing only.");
                catXItem = this.catalogXItemBp.findById(catXItem.getId(), CatalogXItem.class);
                String apdSku = "";
                for (Sku sku : catXItem.getItem().getSkus()) {
                    switch (sku.getType().getName()) {
                        case APD_SKU_NAME:
                            apdSku = sku.getValue();
                            break;
                    }
                    solrQuery = new SolrQuery(); //remove catalogID filter
                    solrQuery.set("q", APD_SKU_FIELD + ":\"" + apdSku + "\"");
                    solrQuery.set("fq", VENDOR_NAME_FIELD + ":\""
                            + catXItem.getItem().getVendorCatalog().getVendor().getName() + "\"");
                    response = solr.query(solrQuery);
                    searchResultDocuments = response.getResults();
                    products = SearchUtilities.convertToProductList(searchResultDocuments);
                    if (!products.isEmpty()) {
                        product = products.get(0);
                    }
                }
            }
        }
        catch (SolrServerException ex) {
            LOGGER.error("Could not reach solr");
        }
        if (product == null) {
            try {
                product = this.convertToProduct(catXItem);
            }
            catch (Exception e) {
                LOGGER.debug("Error converting catalogXItem to product");
                LOGGER.trace("Error: ", e);
            }
        }
        return product;
    }

    public Product convertToProduct(CatalogXItem cxItem) {
        Product product = new Product();
        cxItem = catalogXItemBp.findById(cxItem.getId(), CatalogXItem.class);
        product.setId(cxItem.getId());
        Item item = cxItem.getItem();
        product.setName(item.getName());
        product.setDescription(item.getDescription());
        product.setCustomerSku(cxItem.getCustomerSku() != null ? cxItem.getCustomerSku().getValue() : null);
        product.setApdSku(item.getSku("APD"));
        product.setManufacturerSku(item.getSku("manufacturer"));
        product.setVendorSku(item.getSku("vendor"));
        product.setVendorName(item.getVendorCatalog().getVendor().getName());
        product.setManufacturerName(item.getManufacturer().getName());
        //If there are no images add a placeholder image
        product.setImageUrls(new HashSet<String>());
        for (ItemImage i : item.getItemImages()) {
            product.getImageUrls().add(i.getImageUrl());
        }
        if (product.getImageUrls().isEmpty()) {
            product.getImageUrls().add(MarketingImagesBean.getImageUrl("searchUtilities1"));
        }
        //For now set first image as main image
        //TODO choose this by the size of the image once that ETL process is determined
        for (String url : product.getImageUrls()) {
            product.setMainImageUrl(url);
            break;
        }
        //TODO : if needed, but probably won't be since icons are only used for browsing and this method shouldn't be used for browsing
        //product.setIconMap(TODO);
        product.setProperties(new HashMap<String, String>());
        for (CatalogXItemXItemPropertyType ixipt : cxItem.getProperties()) {
            product.getProperties().put(ixipt.getType().getName(), ixipt.getValue());
        }
        product.setPrice(cxItem.getPrice());
        product.setUnits(item.getUnitOfMeasure().getName());
        product.setCoreItemStartDate(cxItem.getCoreItemStartDate());
        product.setCoreItemExpirationDate(cxItem.getCoreItemExpirationDate());
        if (item.getStatus() == null) {
            product.setStatus(ItemStatus.AVAILABLE.name());
        }
        else {
            product.setStatus(item.getStatus().name());
        }
        product.setMinimum(item.getMinimum());
        product.setMultiple(item.getMultiple());
        return product;
    }

    public Product getProductFromVendorNameAndApdSku(String vendorName, String apdSku) {
        Product product = new Product();
        try {
            solr = solrServiceBean.getSolrServer();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", APD_SKU_FIELD + ":\"" + apdSku + "\"");
            solrQuery.set("fq", VENDOR_NAME_FIELD + ":\"" + vendorName + "\"");
            QueryResponse response = solr.query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                product = products.get(0);
            }
        }
        catch (SolrServerException e) {
            LOGGER.error("Could not reach solr");
        }
        return product;
    }

    public Product getCatalogProductFromVendorNameAndApdSku(String vendorName, String apdSku) {
        Product product = new Product();
        try {
            solr = solrServiceBean.getSolrServer();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery.set("q", APD_SKU_FIELD + ":\"" + apdSku + "\"");
            solrQuery.set("fq", VENDOR_NAME_FIELD + ":\"" + vendorName + "\"");
            solrQueryUtilities.addFilterByCatalogID(solrQuery);
            QueryResponse response = solr.query(solrQuery);
            SolrDocumentList searchResultDocuments = response.getResults();
            List<Product> products = SearchUtilities.convertToProductList(searchResultDocuments);
            if (!products.isEmpty()) {
                if (products.get(0) != null && !"no".equals(products.get(0).getProperties().get("availability"))) {
                    product = products.get(0);
                }
            }
        }
        catch (SolrServerException e) {
            LOGGER.error("Could not reach solr");
        }
        return product;
    }

    public static String toCurrency(String rawValue) {
        double doubleValue = 0.0;

        try {
            doubleValue = Double.parseDouble(rawValue);
        }
        catch (Exception e) {
            LOGGER.debug("Could not parse double ", e);
        }

        String currencyFormatValue = new java.text.DecimalFormat("$#,##0.00").format(doubleValue);
        return currencyFormatValue;
    }
}
