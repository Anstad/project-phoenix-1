/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml.request.generators;

import static com.apd.phoenix.service.cxml.CxmlConstants.DEFAULT_CURRENCY;
import static com.apd.phoenix.service.cxml.CxmlConstants.DEFAULT_OPERATION_ALLOWED;
import static com.apd.phoenix.service.cxml.CxmlConstants.XML_LANG;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.integration.cxml.model.cxml.BuyerCookie;
import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.integration.cxml.model.cxml.Classification;
import com.apd.phoenix.service.integration.cxml.model.cxml.Description;
import com.apd.phoenix.service.integration.cxml.model.cxml.Header;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemDetail;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemID;
import com.apd.phoenix.service.integration.cxml.model.cxml.ItemIn;
import com.apd.phoenix.service.integration.cxml.model.cxml.ManufacturerName;
import com.apd.phoenix.service.integration.cxml.model.cxml.Message;
import com.apd.phoenix.service.integration.cxml.model.cxml.Money;
import com.apd.phoenix.service.integration.cxml.model.cxml.ObjectFactory;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessage;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutOrderMessageHeader;
import com.apd.phoenix.service.integration.cxml.model.cxml.Shipping;
import com.apd.phoenix.service.integration.cxml.model.cxml.SupplierPartAuxiliaryID;
import com.apd.phoenix.service.integration.cxml.model.cxml.Tax;
import com.apd.phoenix.service.integration.cxml.model.cxml.Total;
import com.apd.phoenix.service.integration.cxml.model.cxml.UnitPrice;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.CxmlConfiguration.DeploymentMode;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import com.google.common.collect.ImmutableList;

/**
 *
 * @author dnorris
 */
@RequestScoped
public class PunchoutOrderMessageFactory extends BaseCxmlMessageGenerator {

    private static final Logger logger = LoggerFactory.getLogger(PunchoutOrderMessageFactory.class);
    private static final ObjectFactory factory = new ObjectFactory();
    @Inject
    private CxmlRequestHeaderFactory cxmlRequestHeaderFactory;
    @Inject
    private LoginBean loginBean;
    @Inject
    private PunchoutSessionBp punchoutSessionBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private CredentialBp credentialBp;
    @Inject
    private CustomerOrderBp customerOrderBp;

    private com.apd.phoenix.service.model.Credential shoppingCredential;
    private PunchoutSession punchoutSession;
    private PurchaseOrderDto purchaseOrderDto;

    @PostConstruct
    public void init() {
        if (loginBean.getPunchoutSession() != null) {
            punchoutSession = punchoutSessionBp.findById(loginBean.getPunchoutSession().getId(), PunchoutSession.class);
        }
        else {
            throw new IllegalStateException("No punchout session found.");
        }

        shoppingCredential = credentialSelectionBean.getCurrentAXCXU().getCredential();
        shoppingCredential = credentialBp.findById(shoppingCredential.getId(), Credential.class);

        try {
            CustomerOrder customerOrder = customerOrderBp.findById(punchoutSession.getCustomerOrder().getId(),
                    CustomerOrder.class);
            purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
        }
        catch (DtoFactory.ParsingException ex) {
            logger.error("Error generating PurchaseOrderDto", ex);
        }
    }

    @Deprecated
    private void initDto() {
        CustomerOrder customerOrder = punchoutSession.getCustomerOrder();
        customerOrder = customerOrderBp.findById(customerOrder.getId(), CustomerOrder.class);
        try {
            purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
        }
        catch (DtoFactory.ParsingException ex) {
            logger.error("Error generating PurchaseOrderDto", ex);
        }

    }

    public CXML getOutgoingPunchoutOrderMessage() {
        CXML cxml = factory.createCXML();
        Header header = cxmlRequestHeaderFactory.generateReturnCartOutgoingToCustomerHeader(shoppingCredential.getId());
        Message message = generateCxmlMessage();
        cxml.getHeaderOrMessageOrRequestOrResponse().add(header);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(message);
        cxml.setPayloadID(generatePayloadId());
        cxml.setTimestamp(generateTimeStamp());
        cxml.setXmlLang(ENGLISH);
        return cxml;
    }

    @Deprecated
    private void initPunchoutSession() {
        if (loginBean.getPunchoutSession() != null) {
            punchoutSession = punchoutSessionBp.findById(loginBean.getPunchoutSession().getId(), PunchoutSession.class);
        }
        else {
            throw new IllegalStateException("No punchout session found.");
        }
    }

    @Deprecated
    private void initCredential() {
        shoppingCredential = credentialSelectionBean.getCurrentAXCXU().getCredential();
        shoppingCredential = credentialBp.findById(shoppingCredential.getId(),
                com.apd.phoenix.service.model.Credential.class);
    }

    private Message generateCxmlMessage() {
        Message message = factory.createMessage();
        PunchOutOrderMessage punchoutOrderMessage = generatePunchoutOrderMessage();
        message
                .getPunchOutOrderMessageOrProviderDoneMessageOrSubscriptionChangeMessageOrDataAvailableMessageOrSupplierChangeMessageOrOrganizationChangeMessage()
                .add(punchoutOrderMessage);
        if (shoppingCredential.getDeploymentMode() != null) {
            message.setDeploymentMode(shoppingCredential.getDeploymentMode().getLabel());
        }
        else {
            message.setDeploymentMode(CxmlConfiguration.DeploymentMode.TEST.getLabel());
        }
        return message;
    }

    private PunchOutOrderMessage generatePunchoutOrderMessage() {
        PunchOutOrderMessage punchoutOrderMessage = factory.createPunchOutOrderMessage();
        punchoutOrderMessage.setBuyerCookie(getPunchoutOrderMessageBuyerCookie());
        punchoutOrderMessage.setPunchOutOrderMessageHeader(getPunchOutOrderMessageHeader());
        punchoutOrderMessage.getItemIn().addAll(getItemInList());
        return punchoutOrderMessage;
    }

    private BuyerCookie getPunchoutOrderMessageBuyerCookie() {
        BuyerCookie buyerCookie = new BuyerCookie();
        buyerCookie.getContent().add(punchoutSession.getBuyerCookie());
        return buyerCookie;
    }

    private PunchOutOrderMessageHeader getPunchOutOrderMessageHeader() {
        PunchOutOrderMessageHeader header = new PunchOutOrderMessageHeader();
        header.setOperationAllowed(shoppingCredential.getPunchoutOperationAllowed().toString());
        if (StringUtils.isBlank(header.getOperationAllowed())) {
            header.setOperationAllowed(DEFAULT_OPERATION_ALLOWED);
        }
        if (purchaseOrderDto.getOrderTotal() != null) {
            BigDecimal orderTotal = purchaseOrderDto.getOrderTotal().setScale(2, RoundingMode.HALF_UP);
            header.setTotal(getTotal(orderTotal.toPlainString()));
        }
        else {
            header.setTotal(getTotal("0.00"));
        }
        return header;
    }

    private Shipping getShipping(String amount) {
        Shipping shipping = new Shipping();
        Description description = new Description();
        description.setXmlLang(XML_LANG);
        description.setvalue("business");
        shipping.setDescription(description);
        shipping.setMoney(getMoney(amount));
        return shipping;

    }

    private Money getMoney(String amount) {
        Money money = new Money();
        money.setCurrency(DEFAULT_CURRENCY);
        money.setvalue(amount);
        return money;
    }

    private List<ItemIn> getItemInList() {
        ImmutableList.Builder<ItemIn> builder = new ImmutableList.Builder<>();

        for (LineItemDto item : purchaseOrderDto.getItems()) {
            ItemIn itemIn = new ItemIn();
            if (item.getLineNumber() != null) {
                itemIn.setLineNumber(item.getLineNumber().toString());
            }
            
            ItemID itemId = getItemIdFromLineItem(item);
            itemIn.setItemID(itemId);
            
            ItemDetail itemDetail = getItemDetailFromLineItem(item);
            itemIn.setItemDetail(itemDetail);
            
            itemIn.setQuantity(item.getQuantity().toString());
            
            itemIn.setLineNumber(String.valueOf(item.getLineNumber()));

            builder.add(itemIn);
        }

        return builder.build();
    }

    private Tax getTax(String amount) {
        Tax tax = new Tax();
        tax.setMoney(getMoney(amount));
        return tax;
    }

    private Total getTotal(String amount) {
        Total total = new Total();
        total.setMoney(getMoney(amount));
        return total;
    }

    private ItemID getItemIdFromLineItem(LineItemDto dto) {
        ItemID itemId = new ItemID();
        SupplierPartAuxiliaryID supplierPartAuxiliaryID = getSupplierPartAuxiliaryIDFromLineItem(dto);
        itemId.setSupplierPartAuxiliaryID(supplierPartAuxiliaryID);
        if (StringUtils.isNotBlank(dto.getSupplierPartId())) {
            itemId.setSupplierPartID(dto.getSupplierPartId());
        }
        else {
            itemId.setSupplierPartID("unknown");
        }
        return itemId;
    }

    private SupplierPartAuxiliaryID getSupplierPartAuxiliaryIDFromLineItem(LineItemDto dto) {
        SupplierPartAuxiliaryID supplierPartAuxiliaryID = new SupplierPartAuxiliaryID();
        String id = "";
        if (StringUtils.isNotBlank(dto.getApdSku())) {
            id = dto.getApdSku();
        }
        id = id + "|" + purchaseOrderDto.getApdPoNumber();
        supplierPartAuxiliaryID.getContent().add(id);
        return supplierPartAuxiliaryID;
    }

    private ItemDetail getItemDetailFromLineItem(LineItemDto dto) {
        ItemDetail itemDetail = new ItemDetail();
        String manufactureNameValue = dto.getManufacturerName();
        if (StringUtils.isBlank(manufactureNameValue)) {
            manufactureNameValue = "unknown";
        }
        ManufacturerName manufacturerName = new ManufacturerName();
        manufacturerName.setvalue(manufactureNameValue);
        itemDetail.setManufacturerName(manufacturerName);
        if (dto.getUnitOfMeasure() != null && StringUtils.isNotBlank(dto.getUnitOfMeasure().getName())) {
            itemDetail.setUnitOfMeasure(dto.getUnitOfMeasure().getName());
        }
        else {
            itemDetail.setUnitOfMeasure("EA");
        }
        if (StringUtils.isNotBlank(dto.getManufacturerPartId())) {
            itemDetail.setManufacturerPartID(dto.getManufacturerPartId());
        }
        UnitPrice unitPrice = new UnitPrice();
        BigDecimal unitPriceValue = dto.getUnitPrice().setScale(2, RoundingMode.HALF_UP);
        unitPrice.setMoney(getMoney(unitPriceValue.toPlainString()));
        itemDetail.setUnitPrice(unitPrice);
        final Description description = new Description();
        description.setXmlLang(ENGLISH);
        String descriptionValue = dto.getShortName();
        if (StringUtils.isBlank(descriptionValue)) {
            descriptionValue = "No description. Please contact customer service (877) 769-0752";
        }
        description.setvalue(descriptionValue);
        itemDetail.getDescription().add(description);
        if (dto.getLeadTime() != null) {
            itemDetail.setLeadTime(String.valueOf(dto.getLeadTime()));
        }
        String classificationValue = dto.getUnspscClassification();
        if (StringUtils.isBlank(classificationValue)) {
            classificationValue = "unknown";
        }
        itemDetail.getClassification().add(getClassification("UNSPSC", classificationValue));
        return itemDetail;
    }

    private Classification getClassification(String domain, String value) {
        Classification classification = new Classification();
        classification.setDomain(domain);
        classification.setvalue(value);
        return classification;
    }
}
