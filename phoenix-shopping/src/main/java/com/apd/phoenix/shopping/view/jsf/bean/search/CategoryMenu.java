package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.google.common.collect.HashMultimap;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
public class CategoryMenu implements SolrSearcher {

    private static final Logger logger = LoggerFactory.getLogger(CategoryMenu.class);

    private Map<CategoryMenuNode, HashMultimap<String, String>> categoryData;

    private Set<String> topLevelCategories;

    public CategoryMenu(QueryResponse categoryMenuResponse) {
        buildCategoryMenu(categoryMenuResponse);
        initTopLevelCategories();
    }

    /**
     * Parses the Solr QueryResponse object for all hierarchy facet field values
     * Passes all the set of facetFieldValues to along for more processing.
     */
    private void buildCategoryMenu(QueryResponse categoryMenuResponse) {
        FacetField field = categoryMenuResponse.getFacetField(HIERARCHY_PATH_FACET_VALUE);
        Set<String> facetFieldValues = new HashSet<>();
        for (FacetField.Count c : field.getValues()) {
            facetFieldValues.add(c.getName());
        }
        buildCategoryMenu(facetFieldValues);
    }

    /**
     * Builds the internal data structure for storing the category hierarchy
     * data
     */
    private void buildCategoryMenu(Set<String> facetFieldValues) {
        categoryData = new HashMap<>();
        for (String s : facetFieldValues) {
            //remove first ! before splitting
            String[] levels = s.substring(1, s.length()).split(HIERARCHY_DELIMITER);
            if (levels.length == 0) {
                break;
            }
            for (int i = 0; i < levels.length; i++) {
                HashMultimap<String, String> levelData = HashMultimap.create();
                CategoryMenuNode node = new CategoryMenuNode(i, i == 0 ? null : levels[i == 1 ? i-1 : i-2]);
                if (categoryData.keySet().contains(node)) {
                    levelData = categoryData.get(node);
                }
                if (i == 0) {
                    levelData.put(levels[i], levels[i]);
                } else {
                    levelData.put(levels[i - 1], levels[i]);
                }
                categoryData.put(node, levelData);
            }
        }
    }

    /**
     * Return a url for first level category links
     *
     * @return url
     */
    public String getUrl(String categoryName) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        url.append("?").append(HIERARCHY_PATH_PARAMETER);
        url.append("=");
        url.append(reconstructHierarchyPath(0, categoryName, null, null));
        return url.toString();
    }

    /**
     * Return a url for second level category links
     *
     * @return url
     */
    public String getUrl(String categoryName, String categoryParent) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        url.append("?").append(HIERARCHY_PATH_PARAMETER);
        url.append("=");
        url.append(reconstructHierarchyPath(1, categoryName, categoryParent, null));
        return url.toString();
    }

    /**
     * Return a url for third level category links
     *
     * @return url
     */
    public String getUrl(String categoryName, String categoryParent, String categoryAncestor) {
        StringBuilder url = new StringBuilder(SEARCH_PAGE_BASE_URL);
        url.append("?").append(HIERARCHY_PATH_PARAMETER);
        url.append("=");
        url.append(reconstructHierarchyPath(2, categoryName, categoryParent, categoryAncestor));
        return url.toString();
    }

    /**
     * Method to reconstuct the full hierarchy path to support the url
     * construction process
     *
     * @return url encoded string representation of full hierarchy path
     */
    private String reconstructHierarchyPath(int level, String key, String parent, String ancestor) {
        StringBuilder reconstructedHierarchyPath = new StringBuilder();
        reconstructedHierarchyPath.append(HIERARCHY_DELIMITER);
        switch (level) {
            case 0:
                reconstructedHierarchyPath.append(key);
                break;
            case 1:
                reconstructedHierarchyPath.append(parent);
                reconstructedHierarchyPath.append(HIERARCHY_DELIMITER);
                reconstructedHierarchyPath.append(key);
                break;
            case 2:
                reconstructedHierarchyPath.append(ancestor);
                reconstructedHierarchyPath.append(HIERARCHY_DELIMITER);
                reconstructedHierarchyPath.append(parent);
                reconstructedHierarchyPath.append(HIERARCHY_DELIMITER);
                reconstructedHierarchyPath.append(key);
                break;
        }
        String hierarchyString = reconstructedHierarchyPath.toString();
        try {
            hierarchyString = URLEncoder.encode(hierarchyString, "UTF-8");
        }
        catch (UnsupportedEncodingException ex) {
            logger.error("Error url decoding hierarchy path string", ex);
        }
        return hierarchyString;

    }

    /**
     * @return level 0 category names
     */
    public Set<String> getTopLevelCategories() {
        return topLevelCategories;
    }

    public List<String> getTopLevelCategoriesList() {
    	if (this.topLevelCategories == null) {
    		return null;
    	}
    	List<String> toReturn = new ArrayList<>();
    	toReturn.addAll(topLevelCategories);
    	Collections.sort(toReturn);
        return toReturn;
    }

    private void initTopLevelCategories() {
        topLevelCategories = new HashSet<>();
        for (CategoryMenuNode node : categoryData.keySet()) {
            if (node.getLevel() == 0) {
                topLevelCategories.addAll(categoryData.get(node).keySet());
            }
        }
    }

    /**
     * @return Menu items for given category
     */
    public Collection<String> getMenuItems(int level, String parentMenuName, String menuName) {
        List<String> toReturn = new ArrayList<>();
        for (CategoryMenuNode node : categoryData.keySet()) {
            if (node.getLevel() == level && node.getParent() != null && node.getParent().equals(parentMenuName)) {
                toReturn.addAll(categoryData.get(node).get(menuName));
            }
        }
        Collections.sort(toReturn);
        return toReturn;
    }

    /**
     * Calls getMenuItems passing the menuName as the parentMenuName, since menus on this level reference themselves as parents, i.e.
     * the label of the CategoryMenuNode for items on this level is equal to the key of the corresponding levelData HashMultiMap<String, String>
     * @param menuName
     * @return Menu items for a level-one category
     */
    public Collection<String> getLevelOneMenuItems(String menuName) {
        return getMenuItems(1, menuName, menuName);
    }

    /**
     * Method for exposing the menu size for calculating the width of the popup
     * in the ui
     */
    public int getMenuWidth(String menuName) {
        return getLevelOneMenuItems(menuName).size();
    }

    /**
     * @return data structure containing the hierarchy data
     */
    public Map<CategoryMenuNode, HashMultimap<String, String>> getCategoryData() {
        return categoryData;
    }

}