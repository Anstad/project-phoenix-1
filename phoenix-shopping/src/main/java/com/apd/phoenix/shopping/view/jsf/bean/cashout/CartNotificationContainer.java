/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean.PromptAction;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean.PromptIssue;
import java.util.ResourceBundle;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dcnorris
 */
public class CartNotificationContainer {

    private List<ItemJsonContainer> itemList;
    private List<String> specList;
    private String line1;
    private String line2;
    private List<String> actionList;
    private Map<String, String> actionButtonMap;
    private String cartUrl;
    private String header;

    public CartNotificationContainer(List<ItemJsonContainer> itemList, List<String> specList, PromptIssue issue, String messageOverride, String fullMessageOverride, String cartUrl) {
        this.itemList = itemList;
        this.specList = specList;
        actionList = new ArrayList<>();
        actionButtonMap = new HashMap<>();
        for (PromptAction action : issue.getActionArray()) {
        	actionList.add(action.getLabel());
        	actionButtonMap.put(action.getLabel(), action.getButtonText());
        }
        String messageProperty = issue.getMessageProperty();
        ResourceBundle messagePropertyBundle = ResourceBundle.getBundle("com.apd.phoenix.strings.promptIssues");
        if (!StringUtils.isBlank(messageOverride) 
        		&& messagePropertyBundle.containsKey(messageOverride.concat(".header")) 
        		&& messagePropertyBundle.containsKey(messageOverride.concat(".message"))) {
        	messageProperty = messageOverride;
        }
        line1 = messagePropertyBundle.getString(messageProperty.concat(".header"));
        line2 = messagePropertyBundle.getString(messageProperty.concat(".message"));
        if (StringUtils.isNotBlank(fullMessageOverride)) {
        	line2 = fullMessageOverride;
        }
        this.cartUrl = cartUrl;
        this.header = ResourceBundle.getBundle("com.apd.phoenix.strings.promptIssues").getString("window.header");
    }

    @Override
    public String toString() {
        return "specList = " + specList + ", itemList = " + itemList + ", actionList = " + actionList
                + ", actionButtonMap = " + actionButtonMap + ", line1 = " + line1 + ", line2 = " + line2
                + ", cartUrl = " + cartUrl + ", header = " + header;
    }
}
