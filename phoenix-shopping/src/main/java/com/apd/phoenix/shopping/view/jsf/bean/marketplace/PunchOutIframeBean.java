package com.apd.phoenix.shopping.view.jsf.bean.marketplace;

import com.apd.phoenix.service.integration.cxml.model.cxml.CXML;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.shopping.cxml.CxmlHttpClientUtils.CxmlHttpClientUtilsException;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.CHARGE_SALES_TAX;
import static com.apd.phoenix.shopping.cxml.CxmlHttpClientUtils.postCxmlContent;
import com.apd.phoenix.shopping.cxml.request.generators.PunchoutSetupRequestFactory;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringWriter;
import org.apache.commons.lang.StringEscapeUtils;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.security.jacc.PolicyContextException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@SessionScoped
public class PunchOutIframeBean implements Serializable {

    private static final long serialVersionUID = -8796584026581648677L;
    private static final Logger logger = LoggerFactory.getLogger(PunchOutIframeBean.class);

    private String iframeUrl;

    private String punchoutZipCode;

    private boolean renderExternalFrame;

    private String contactEmail;

    private String contactPhoneNumber;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    @Inject
    private PunchoutSetupRequestFactory punchoutSetupRequestFactory;

    public static final String CONTACT_EMAIL_ADDRESS = "contact email address";
    public static final String CONTACT_PHONE_NUMBER = "contact phone number";

    public void initPunchout() throws Exception {

        CXML punchoutSetupRequest;
        if (StringUtils.isNotBlank(punchoutZipCode)) {
            punchoutSetupRequest = punchoutSetupRequestFactory.generatePunchoutSetupRequest(punchoutZipCode);
        }
        else {
            punchoutSetupRequest = punchoutSetupRequestFactory.generatePunchoutSetupRequest();
        }

        if (logger.isDebugEnabled()) {
            JAXBContext cxmlJaxbContext = JAXBContext.newInstance(CXML.class);
            StringWriter sw = new StringWriter();
            String CXML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                    + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.014/cXML.dtd\">";
            Marshaller marshaller = cxmlJaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshaller.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
            marshaller.setProperty("com.sun.xml.bind.xmlHeaders", CXML_HEADER);
            marshaller.marshal(punchoutSetupRequest, sw);
            logger.debug(sw.toString());
        }

        Credential credential = credentialSelectionBean.getCurrentCredential();
        String punchoutUrl = credential.getVendorPunchoutUrl();
        punchOut(punchoutUrl, punchoutSetupRequest);
        this.setContactEmail(getCredentialProperty(credential, CONTACT_EMAIL_ADDRESS));
        this.setContactPhoneNumber(getCredentialProperty(credential, CONTACT_PHONE_NUMBER));
    }

    public void punchOut(String punchoutUrl, CXML punchoutSetupRequest) {
        try (InputStream partnerResponse = postCxmlContent(punchoutUrl, punchoutSetupRequest)) {
            parsePunchoutSetupResponse(partnerResponse);
        } catch (IOException | CxmlHttpClientUtilsException ex) {
            logger.debug(null, ex);
        }
    }

    private void parsePunchoutSetupResponse(InputStream input) throws IOException {
        //TODO consider unmarshalling if more details from this incoming response are needed
        StringWriter writer = new StringWriter();
        IOUtils.copy(input, writer, "UTF-8");
        String responseCXML = writer.toString();
        int beginIndex = responseCXML.indexOf("<URL>") + 5;
        int endIndex = responseCXML.indexOf("</URL>");
        String punchoutCatalogUrl = StringEscapeUtils.unescapeHtml(responseCXML.substring(beginIndex, endIndex));
        iframeUrl = punchoutCatalogUrl;
    }

    private String getCredentialProperty(Credential credential, String property) {
        if (credential != null && credential.getProperties() != null) {
            for (CredentialXCredentialPropertyType cxcpt : credential.getProperties()) {
                if (cxcpt.getType().getName().equals(property)) {
                    if (StringUtils.isEmpty(cxcpt.getValue())) {
                        return cxcpt.getType().getDefaultValue();
                    }
                    return cxcpt.getValue();
                }
            }
        }
        return "";
    }

    public String getIframeUrl() {
        return iframeUrl;
    }

    public boolean isRenderExternalFrame() {
        return renderExternalFrame;
    }

    public void setRenderExternalFrame(boolean renderExternalFrame) {
        this.renderExternalFrame = renderExternalFrame;
    }

    public String getPunchoutZipCode() {
        return punchoutZipCode;
    }

    public void setPunchoutZipCode(String punchoutZipCode) {
        this.punchoutZipCode = punchoutZipCode;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

}