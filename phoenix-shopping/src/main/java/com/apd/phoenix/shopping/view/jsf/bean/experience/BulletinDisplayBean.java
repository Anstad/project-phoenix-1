package com.apd.phoenix.shopping.view.jsf.bean.experience;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;

/**
 * This session-scoped bean stores whether the bulletin modal should be displayed. It displays once per shopping 
 * session. If it should be displayed, it uses the BulletinDisplayAccessBean to load the bulletins.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@SessionScoped
public class BulletinDisplayBean implements Serializable {

    private static final long serialVersionUID = 6505751838383055954L;

    private boolean hasDisplayedBulletins = false;

    @Inject
    private BulletinDisplayAccessBean accessBean;

    @Inject
    private CredentialSelectionBean credentialSelectionBean;

    /**
     * Returns whether the bulletins should be displayed. 
     * 
     * @return
     */
    public boolean getShouldDisplayBulletins() {
        //if either the bulletins have not been marked as being displayed, or the BulletinDisplayAccessBean has the bulletins 
        //loaded (i.e. this is the same request that loaded the bulletins), and the bulletin list is not empty, returns true.
        //if is punchout, returns false
        boolean toReturn = (!hasDisplayedBulletins || accessBean.hasLoadedBulletins())
                && !accessBean.getBulletinList().isEmpty() && !credentialSelectionBean.isPunchoutSession()
                && !credentialSelectionBean.isExternalCatalog();
        hasDisplayedBulletins = true;
        return toReturn;
    }

    /**
     * Returns the list of bulletins to display.
     * 
     * @return
     */
    public List<Bulletin> getBulletinList() {
        return accessBean.getBulletinList();
    }

    public String sanitizedMessage(Bulletin bulletin) {
        return Jsoup.clean(bulletin.getMessage(), Whitelist.basicWithImages());
    }
}
