package com.apd.phoenix.shopping.view.jsf.bean.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.DuplicatePasswordException;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.InsecurePasswordException;
import com.apd.phoenix.service.business.PersonBp;
import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.business.PhoneNumberTypeBp;
import com.apd.phoenix.service.business.RoleBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.UserRequestBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.PasswordResetEntry;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumberType.PhoneNumberTypeEnum;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.shopping.view.jsf.bean.PageCssBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.web.phonenumber.PhoneNumberValidationBean;

@Named
@Stateful
@SessionScoped
public class UserCreationRequestBean {

    private static final Logger LOG = LoggerFactory.getLogger(UserCreationRequestBean.class);

    private static final String csrEmail = "";

    @Inject
    WorkflowService workflowService;

    @Inject
    UserRequestBp userRequestBp;

    @Inject
    PageCssBean pageCssBean;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    PhoneNumberTypeBp phoneNumberTypeBp;

    @Inject
    AccountBp accountBp;

    @Inject
    RoleBp roleBp;

    @Inject
    CredentialBp credentialBp;

    @Inject
    AccountXCredentialXUserBp accountXCredentialXUserBp;

    @Inject
    PersonBp personBp;

    @Inject
    AddressBp addressBp;

    @Inject
    PhoneNumberBp phoneNumberBp;

    @Inject
    private LoginBean loginBean;

    @Inject
    private EmailFactoryBp emailFactoryBp;

    @Inject
    private EmailMessageSender messageSender;

    @Inject
    private PhoneNumberValidationBean phoneNumberValidationBean;

    private static final String REDIRECT_THANKYOU_PAGE = "/ecommerce/user/thankYouUserRegistration?faces-redirect=true";

    private String requestType;
    private String accountId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String ext;
    private String password;

    private String address1New;
    private String address2New;
    private String cityNew;
    private String stateNew;
    private String zipNew;

    private String companyName;

    private String costCenter;
    private String deptName;
    private String locationName;

    private List<String> emailList;

    private Boolean validAddress = Boolean.TRUE;

    @PostConstruct
    public void init() {
        requestType = EmailFactoryBp.USER_REQUEST_CREATE;
        emailList = new ArrayList<>();

    }

    public String submit() {
        boolean sucess = true;
        if (loginBean.getIsMarfieldSubdomain()) {
            createMarfieldUser();
        }
        else {
            sucess = createNewUser();
            //Old method for creating users manually
            //createUserRequest();
        }
        if (sucess) {
            return REDIRECT_THANKYOU_PAGE;
        }
        else {
            return null;
        }
    }

    private boolean createNewUser() {
        if (!systemUserBp.getSystemUserByLogin(email).isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Already exists with that email",
                            "could not create user"));
            return false;
        }
        SystemUser newUser = new SystemUser();
        newUser.setLogin(this.email);
        Person person = new Person();
        person.setFirstName(this.firstName);
        person.setLastName(this.lastName);
        person.setEmail(this.email);
        newUser.setPerson(person);
        newUser.setIsActive(true);
        HashSet<Account> accounts = new HashSet<Account>();
        Account walkInsAccount = accountBp.findByName(EcommercePropertiesLoader.getInstance().getEcommerceProperties()
                .getString("walkInsAccount"));
        if (walkInsAccount == null) {
            LOG.error("WALK-INS Account Name has changed and new users are not able to be " + "added to the account.");
        }
        accounts.add(walkInsAccount);
        newUser.setAccounts(accounts);
        newUser = roleBp.populateUserDefaults(newUser);
        newUser = systemUserBp.create(newUser);
        newUser = systemUserBp.findById(newUser.getId(), SystemUser.class);

        String credentials = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                "walkInsCredentials");
        for (String credential : credentials.split("\\/")) {
            AccountXCredentialXUser accountXCredentialXUser = new AccountXCredentialXUser();
            accountXCredentialXUser.setUser(newUser);
            accountXCredentialXUser.setAccount(walkInsAccount);

            Credential genericCredential = credentialBp.getCredentialByName(credential);
            if (genericCredential == null) {
                LOG.error("Could not find credential with name " + credential);
            }
            accountXCredentialXUser.setCredential(genericCredential);
            accountXCredentialXUserBp.create(accountXCredentialXUser);
        }
        try {
            systemUserBp.encryptAndSetStrongPassword(newUser, password);
            systemUserBp.update(newUser);
        }
        catch (InsecurePasswordException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Password not secure", "Password not secure"));
            return false;
        }
        catch (DuplicatePasswordException e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Duplicate Password", "Duplicate Password"));
            return false;
        }

        List<Message> messages = emailFactoryBp.createUserSetupInfoEmail(newUser);
        for (Message m : messages) {
            messageSender.sendMessage(m, null);
        }
        return true;

    }

    public void validateEmailAddress(FacesContext context, UIComponent component, Object value) {
        this.setValidAddress(Boolean.TRUE);
        String login = (String) value;
        if (loginBean.getIsMarfieldSubdomain()) {
            context.getPartialViewContext().getRenderIds().add("alreadyUsedMessage");
            List<SystemUser> systemUserByLogin = systemUserBp.getSystemUserByLogin(login);
            if (systemUserByLogin != null && !systemUserByLogin.isEmpty()) {
                this.setEmail(login);
                this.setValidAddress(Boolean.FALSE);
                ((UIInput) component).setValid(false);
            }
        }
        pageCssBean.validateEmailDomain(context, component, value);
    }

    private void createMarfieldUser() {
        LOG.info("Creating new Marfield User");
        SystemUser newUser = new SystemUser();
        newUser.setLogin(this.email);
        Person person = new Person();
        person.setFirstName(this.firstName);
        person.setLastName(this.lastName);
        person.setEmail(this.email);
        newUser.setPerson(person);
        newUser.setChangePassword(true);
        newUser.setIsActive(true);
        HashSet<Account> accounts = new HashSet<Account>();
        Account marfieldAccount = accountBp.findByName("Marfield - PepsiCo");
        if (marfieldAccount == null) {
            LOG.error("Marfield - Pepsico Account Name has changed and new users are not able to be "
                    + "added to the account.");
        }
        accounts.add(marfieldAccount);
        newUser.setAccounts(accounts);
        newUser = roleBp.populateUserDefaults(newUser);
        newUser = systemUserBp.create(newUser);
        //Address to person relationship is stored on the address
        Address address = new Address();
        address.setLine1(this.address1New);
        address.setLine2(this.address2New);
        address.setCity(this.cityNew);
        address.setState(this.stateNew);
        address.setZip(this.zipNew);
        address.setCompany(this.companyName);
        address.setPerson(newUser.getPerson());
        address = addressBp.create(address);

        getPhoneNumberValidationBean().getCurrentNumber().setType(
                phoneNumberTypeBp.getTypeFromEnum(PhoneNumberTypeEnum.WORK));
        getPhoneNumberValidationBean().getCurrentNumber().setPerson(newUser.getPerson());
        phoneNumberBp.create(getPhoneNumberValidationBean().getCurrentNumber());

        newUser = systemUserBp.findById(newUser.getId(), SystemUser.class);

        AccountXCredentialXUser accountXCredentialXUser = new AccountXCredentialXUser();
        accountXCredentialXUser.setUser(newUser);
        accountXCredentialXUser.setAccount(marfieldAccount);
        Credential pepsiCredential = credentialBp.getCredentialByName("Pepsi URL");
        if (pepsiCredential == null) {
            LOG.error("Could not find credential with name Pepsi-URL.");
        }
        accountXCredentialXUser.setCredential(pepsiCredential);
        accountXCredentialXUserBp.create(accountXCredentialXUser);

        List<Message> messages = emailFactoryBp.createMarfieldSetupInfoEmail(newUser);
        for (Message m : messages) {
            messageSender.sendMessage(m, null);
        }

    }

    private void createUserRequest() {
		LOG.info("Submitting user modification request");
        UserRequest ur = userRequestBp.createUserRequest();
        Long id = ur.getId();
        String token = ur.getToken();
        LOG.info("Request id {}, token {}", id, token);
        if (id != null) {
            //Add last remaining department
            
            UserModificationRequestDto umr = new UserModificationRequestDto();
            umr.setRequestType(requestType);
            umr.setFirstName(firstName);
            umr.setLastName(lastName);
            umr.setEmail(email);
            umr.setPhone(phone);
            umr.setExt(ext);
            
            umr.setCompanyName(companyName);
            
            umr.setDName(deptName);
            umr.setdCostCenter(costCenter);
            umr.setLocationLocationName(locationName);

            umr.setSAddress1(address1New);
            umr.setSAddress2(address2New);
            umr.setSCity(cityNew);
            umr.setSState(stateNew);
            umr.setSZip(zipNew);

            
            umr.setInternalEmail(getCSREmail());
            
            if (loginBean.getIsMarfieldSubdomain()) {
            	umr.setNoManagerApproval(true);
            }
           
          
            LOG.info("Sending emails to: {}",emailList);
            Map<String,Object> params = new HashMap<>();
            params.put("request", umr);
            params.put("emailList", emailList);

            workflowService.startUserRequest(id,params);
            clearAll();
        }
	}

    public void clearAll() {
        //userType = USER_TYPE_HQ;
        requestType = EmailFactoryBp.USER_REQUEST_CREATE;
        companyName = "";
        firstName = "";
        lastName = "";
        email = "";
        phone = "";
        ext = "";
        address1New = "";
        address2New = "";
        cityNew = "";
        stateNew = "";
        zipNew = "";
        costCenter = "";
        deptName = "";
        locationName = "";
        emailList.clear();
    }

    private String getCSREmail() {

        return pageCssBean.getEmailUsAddress();
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getAddress1New() {
        return address1New;
    }

    public void setAddress1New(String address1New) {
        this.address1New = address1New;
    }

    public String getAddress2New() {
        return address2New;
    }

    public void setAddress2New(String address2New) {
        this.address2New = address2New;
    }

    public String getCityNew() {
        return cityNew;
    }

    public void setCityNew(String cityNew) {
        this.cityNew = cityNew;
    }

    public String getStateNew() {
        return stateNew;
    }

    public void setStateNew(String stateNew) {
        this.stateNew = stateNew;
    }

    public String getZipNew() {
        return zipNew;
    }

    public void setZipNew(String zipNew) {
        this.zipNew = zipNew;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public PhoneNumberValidationBean getPhoneNumberValidationBean() {
        return phoneNumberValidationBean;
    }

    public void setPhoneNumberValidationBean(PhoneNumberValidationBean phoneNumberValidationBean) {
        this.phoneNumberValidationBean = phoneNumberValidationBean;
    }

    public Boolean getValidAddress() {
        return validAddress;
    }

    public void setValidAddress(Boolean validAddress) {
        this.validAddress = validAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
