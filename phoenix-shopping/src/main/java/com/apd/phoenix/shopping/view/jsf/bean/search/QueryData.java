/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.search;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dcnorris
 */
public class QueryData {

    private String searchString;
    private String refineSearchString;
    private String activeFiltersString;
    private String hierarchyPath;
    private float lowPrice = -1;
    private float highPrice = -1;
    private int start = 0;
    private String selectedFilterName = "";
    private String selectedManufacturer;
    private Set<String> activeFilters = new HashSet<>();
    private String companyFavoritesId = "";
    private String matchbookId = "";
    
    public String getSearchString() {
        return searchString;
    }

    public String getSolrSafeSearchString() {
    	return SearchUtilities.makeSolrSafe(searchString);
    }
    
    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getRefineSearchString() {
        if (SearchBean.REFINE_YOUR_SEARCH_STRING.equals(refineSearchString)) {
            return "";
        }
        return refineSearchString;
    }

    public void setRefineSearchString(String refineSearchString) {
        this.refineSearchString = refineSearchString;
    }
    
    public String getSolrSafeRefineSearchString() {
        return SearchUtilities.makeSolrSafe(refineSearchString);
    }

    public String getHierarchyPath() {
        return hierarchyPath;
    }

    public void setHierarchyPath(String hierarchyPath) {
        this.hierarchyPath = hierarchyPath;
    }

    public float getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(float lowPrice) {
        this.lowPrice = lowPrice;
    }

    public float getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(float highPrice) {
        this.highPrice = highPrice;
    }

    public String getFiltersString() {
        return activeFiltersString;
    }

    public void setFiltersString(String activeFiltersString) {
        this.activeFiltersString = activeFiltersString;
    }

    public String getActiveFiltersString() {
        return activeFiltersString;
    }

    public void setActiveFiltersString(String activeFiltersString) {
        this.activeFiltersString = activeFiltersString;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public String getSelectedFilterName() {
        return selectedFilterName;
    }

    public void setSelectedFilterName(String selectedFilterName) {
        this.selectedFilterName = selectedFilterName;
    }

    public String getSelectedManufacturer() {
        return selectedManufacturer;
    }

    public void setSelectedManufacturer(String selectedManufacturer) {
        this.selectedManufacturer = selectedManufacturer;
    }

    public Set<String> getActiveFilters() {
        return activeFilters;
    }
    
    public Set<String> getSolrSafeActiveFilters() {
    	Set<String> solrSafeActiveFilters = new HashSet<String>();
    	for (String s : activeFilters) {
    		solrSafeActiveFilters.add(SearchUtilities.makeSolrSafe(s));
    	}
    	return solrSafeActiveFilters;
    }

    public void setActiveFilters(Set<String> activeFilters) {
        this.activeFilters = activeFilters;
    }

    public String getCompanyFavoritesId() {
        return companyFavoritesId;
    }

    public void setCompanyFavoritesId(String companyFavoritesId) {
        this.companyFavoritesId = companyFavoritesId;
    }

    public static QueryData clone(QueryData object) {
        QueryData data = new QueryData();
        data.setActiveFiltersString(object.getActiveFiltersString());
        data.setFiltersString(object.getActiveFiltersString());
        data.setHierarchyPath(object.getHierarchyPath());
        data.setHighPrice(object.getHighPrice());
        data.setLowPrice(object.getLowPrice());
        data.setSearchString(object.getSearchString());
        data.setSelectedFilterName(object.getSelectedFilterName());
        data.setSelectedManufacturer(object.getSelectedManufacturer());
        data.setStart(object.getStart());
        data.setActiveFilters(object.getActiveFilters());
        data.setRefineSearchString(object.getRefineSearchString());
        data.setCompanyFavoritesId(object.getCompanyFavoritesId());
        data.setMatchbookId(object.getMatchbookId());
        return data;
    }

    public String getMatchbookId() {
        return matchbookId;
    }

    public void setMatchbookId(String matchbookId) {
            this.matchbookId = matchbookId;
    }

    public UTF8EncodedString getSearchStringEncoded() {
            return new UTF8EncodedString(searchString);
    }

    public UTF8EncodedString getActiveFiltersStringEncoded() {
            return new UTF8EncodedString(activeFiltersString);
    }

    public UTF8EncodedString getHierarchyPathEncoded() {
            return new UTF8EncodedString(hierarchyPath);
    }

    public UTF8EncodedString getSelectedFilterNameEncoded() {
            return new UTF8EncodedString(selectedFilterName);
    }

    public UTF8EncodedString getSelectedManufacturerEncoded() {
            return new UTF8EncodedString(selectedManufacturer);
    }

    public UTF8EncodedString getRefineSearchStringEncoded() {
            return new UTF8EncodedString(refineSearchString);
    }
}
