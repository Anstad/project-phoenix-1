/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.product;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.apache.commons.validator.EmailValidator;

/**
 *
 * @author nreidelb
 */
@FacesValidator("emailFieldValidator")
public class EmailFieldValidator implements Validator {

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object inputEmail) throws ValidatorException {
        String email = (String) inputEmail;
        if (email == null) {
            throw new ValidatorException(new FacesMessage("Please enter a valid email address"));
        }
        EmailValidator eValidator = EmailValidator.getInstance();
        if (!eValidator.isValid(email)) {
            throw new ValidatorException(new FacesMessage("Please enter a valid email address"));
        }
    }

}
