/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.cxml;

import com.apd.phoenix.service.business.PunchoutSessionBp;
import com.apd.phoenix.service.model.PunchoutSession;
import javax.ejb.EJB;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dnorris
 */
@WebListener
public class PunchoutSessionListener implements HttpSessionListener {

    private static final String PUNCHOUT_SESSION_ATTRIBUTE = "punchoutSessionToken";

    @EJB
    private PunchoutSessionBp punchoutSessionBp;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        //do nothing
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        cleanUpPunchoutSession(se);
    }

    private void cleanUpPunchoutSession(HttpSessionEvent se) {
        String punchoutSessionToken = (String) se.getSession().getAttribute(PUNCHOUT_SESSION_ATTRIBUTE);
        if (StringUtils.isNotBlank(punchoutSessionToken)) {
            PunchoutSession session = new PunchoutSession();
            session.setSessionToken(punchoutSessionToken);
            if (checkSessionExist(session)) {
                session = punchoutSessionBp.searchByExactExample(session, 0, 0).get(0);
                punchoutSessionBp.delete(session.getId(), PunchoutSession.class);
            }
        }
    }

    private boolean checkSessionExist(PunchoutSession punchoutSession) {
        try {
            punchoutSessionBp.searchByExactExample(punchoutSession, 0, 0).get(0);
        }
        catch (IndexOutOfBoundsException ex) {
            return false;
        }
        return true;
    }

}
