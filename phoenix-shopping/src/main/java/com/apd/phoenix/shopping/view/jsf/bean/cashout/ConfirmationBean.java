package com.apd.phoenix.shopping.view.jsf.bean.cashout;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.web.cashout.CashoutPageContainer;
import com.apd.phoenix.service.model.CustomerOrder;

@Named
@ConversationScoped
public class ConfirmationBean implements Serializable {

    private static final long serialVersionUID = 6904057589417219793L;

    @Inject
    private Conversation conversation;

    private CustomerOrder currentOrder;

    private CashoutPageContainer cashoutPageContainer;

    @PostConstruct
    public void init() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
    }

    public CustomerOrder getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(CustomerOrder currentOrder) {
        this.currentOrder = currentOrder;
    }

    public CashoutPageContainer getPageValues() {
        return cashoutPageContainer;
    }

    public void setCashoutPageContainer(CashoutPageContainer container) {
        cashoutPageContainer = container;
    }
}
