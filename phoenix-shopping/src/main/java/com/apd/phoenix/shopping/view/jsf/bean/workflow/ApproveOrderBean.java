/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.workflow;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.CashoutPageBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.business.PoNumberTypeBp;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.web.cashout.CashoutPageContainer;

@Named
@Stateful
@RequestScoped
public class ApproveOrderBean extends OrderHumanTaskFormBean {

    //TODO: Add ability for manager to include a comment, which is emailed to the user
    @Inject
    AccountBp accountBp;

    @Inject
    private CashoutPageContainer pageValues;

    @Inject
    CashoutPageBp cashoutPageBp;

    @Inject
    PoNumberBp poNumberBp;

    private String customerPo;

    @Inject
    private PoNumberTypeBp poNumberTypeBp;

    @Override
    public void init() {
        PoNumber customerPo = customerOrder.getCustomerPo();
        if (customerPo != null) {
            this.customerPo = customerPo.getValue();
        }
        else {
            this.customerPo = "";
        }

        this.pageValues.init(cashoutPageBp.getFromCredentialForCashout(this.customerOrder.getCredential()));
        this.pageValues.orderToPage(this.customerOrder);
    }

    public void retrieve() {

    }

    public String approve() {
        return this.complete(true);
    }

    public String deny() {
        return this.complete(false);
    }

    private String complete(boolean approved){
    	if (canEditCustomerPo()){
    		PoNumber newCustomerPo;
    		if(customerOrder.getCustomerPo() != null) {
    			newCustomerPo = customerOrder.getCustomerPo();
    		}
    		else {
    			newCustomerPo = new PoNumber();
    			newCustomerPo.getOrders().add(customerOrder);
    			newCustomerPo.setType(poNumberTypeBp.getCustomerPoType());
    			customerOrder.getPoNumbers().add(newCustomerPo);
    		}
    		newCustomerPo.setValue(customerPo);
    	}
        Map<String, Object> params = new HashMap<>();
        customerOrderBp.update(customerOrder);
        params.put("approved", approved);
        return this.completeOrderTask(params);
    }

    public boolean canEditCustomerPo() {
        return accountBp.canEditCustomerPo(customerOrder.getAccount());
    }

    public String getCustomerPo() {
        return customerPo;
    }

    public void setCustomerPo(String customerPo) {
        this.customerPo = customerPo;
    }

    public CashoutPageContainer getPageValues() {
        return this.pageValues;
    }

    public void setPageValues(CashoutPageContainer pageValues) {
        this.pageValues = pageValues;
    }

}
