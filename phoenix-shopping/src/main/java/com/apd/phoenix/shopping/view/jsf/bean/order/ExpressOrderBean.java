/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean.order;

import com.apd.phoenix.service.solr.SolrServiceBean;
import java.io.Serializable;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrQueryUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import org.apache.solr.client.solrj.SolrServer;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@LocalBean
@RequestScoped
public class ExpressOrderBean implements SolrSearcher, Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ExpressOrderBean.class);
    private static final String EXPRESS_ORDER_PAGE_LINK = "/shopping/ecommerce/order/expressOrder.xhtml";
    private String sku;
    private String price;
    private int qty;
    private String units;
    private String name;
    private String errorMessage;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    private QueryResponse response;
    private SolrServer solr;
    @Inject
    private SolrServiceBean solrServiceBean;
    private Product product;
    @Inject
    private AddToCartBean addToCartBean;
    @Inject
    private SolrQueryUtilities solrQueryUtilities;

    public void populateProductData() {
        sku = sku.toUpperCase();
        sku = solrQueryUtilities.escapeSpecialChars(sku);
        if (StringUtils.isEmpty(sku)) {
            resetPage();
            errorMessage = " NO SKU Entered";
            return;
        }
        solr = solrServiceBean.getSolrServer();

        String alternate = "";
        String primary = "";
        String customerQuery = CUSTOMER_SKU_FIELD_S + ":" + sku;
        String apdQuery = APD_SKU_FIELD_S + ":" + sku;
        if (isCustomerSkuTypeAvailable()) {
            primary = customerQuery;
            alternate = apdQuery;
        }
        else {
            //Search by apdSKu
            primary = apdQuery;
            alternate = customerQuery;
        }
        if (queryForSku(primary)) {
            int numFound = (int) response.getResults().getNumFound();
            if (numFound == 1) {
                handleResults();
            }
            else if (numFound > 1) {
                logger.error("Multiple items found with " + (isCustomerSkuTypeAvailable() ? "customer" : "apd")
                        + " sku " + sku);
                handleResults();
            }
            else {
                if (queryForSku(alternate)) {
                    numFound = (int) response.getResults().getNumFound();
                    if (numFound == 1) {
                        handleResults();
                    }
                    else if (numFound > 1) {
                        logger.error("Multiple items found with " + (isCustomerSkuTypeAvailable() ? "apd" : "customer")
                                + " sku " + sku);
                        handleResults();
                    }
                    else {
                        resetPage();
                        errorMessage = " EXACT MATCH NOT FOUND.";
                    }
                }
            }
        }
        sku = sku.replace("\\", "");
    }

    private boolean queryForSku(String queryString) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery = addFilterByCatalogID(solrQuery);
        //Search by Customer Sku
        solrQuery.add("q", queryString);
        executeQuery(solrQuery);
        if (response == null) {
            resetPage();
            errorMessage = " ERROR CONTACTING INDEX SERVER.";
            return false;
        }
        return true;
    }

    private SolrQuery addFilterByCatalogID(SolrQuery solrQuery) {
        solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
        return solrQuery;
    }

    private void executeQuery(SolrQuery solrQuery) {
        try {
            response = solr.query(solrQuery);
        }
        catch (SolrServerException ex) {
            logger.error("Error attempting to query solr", ex);
        }
    }

    private void handleResults() {
        SolrDocumentList searchResultDocuments = response.getResults();
        product = SearchUtilities.convertToProductList(searchResultDocuments).get(0);
        addToCartBean.getDisplayQuantity().put(product, "");
        price = product.getPrice().toPlainString();
        units = product.getUnits();
        name = product.getName();
        errorMessage = "";
    }

    public String getOrderPageLink() {
        return EXPRESS_ORDER_PAGE_LINK;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return ViewUtils.toCurrency(price);
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private void resetPage() {
        sku = "";
        price = "";
        qty = 0;
        units = "";
        errorMessage = "";
        name = "";
        product = null;
    }

    private boolean isCustomerSkuTypeAvailable() {
        return credentialSelectionBean.getCurrentCredential().getSkuType().getName().equals("customer");
    }

    public Product getProduct() {
        return product;
    }

    public void setQuantity() {
        populateProductData();
        if (qty < 1) {
            qty = 1;
        }
        if (product != null) {
            addToCartBean.setDisplayQuantity(product, qty);
        }
    }

    public void addToCart() {
        populateProductData();
        if (qty < 1) {
            qty = 1;
        }
        if (product != null) {
            addToCartBean.addSingleItem(product, qty);
        }
        resetPage();
    }
}
