package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.Serializable;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Size;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ValidationPropertiesLoader;
import com.apd.phoenix.service.business.InsecurePasswordException;
import com.apd.phoenix.service.business.PasswordResetEntryBp;
import com.apd.phoenix.service.business.DuplicatePasswordException;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.PasswordResetEntry;
import com.apd.phoenix.service.model.SystemUser;

@Named
@Stateful
@SessionScoped
public class NewPasswordBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private PasswordResetEntryBp entryBp;

    @Inject
    private SystemUserBp userBp;

    @Size(min = 5, max = 15, message = "Password length must be between {min} and {max} characters.")
    private String password = "";

    private String confirm = "";

    private String emailKey = "";

    private String generatedKey = "";

    private SystemUser user;

    public boolean passwordsMatch() {
        return password.equals(confirm);
    }

    public String storeNewPassword() {

        if (!passwordsMatch()) {
            String message = ValidationPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.notmatch");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            return null;
        }

        try {
            userBp.encryptAndSetStrongPassword(user, password);
            entryBp.delete(entryBp.findByGeneratedKey(generatedKey).getId(), PasswordResetEntry.class);
            userBp.update(user);
        }
        catch (DuplicatePasswordException e) {
            String message = ValidationPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.notdifferent");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            return null;
        }
        catch (InsecurePasswordException e) {
            String message = ValidationPropertiesLoader.getInstance().getValidationProperties().getString(
                    "user.password.error.insecure");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
            return null;
        }
        return LinksBean.PASSWORD_CHANGED_PAGE;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getEmailKey() {
        return emailKey;
    }

    public void setEmailKey(String emailKey) {
        this.emailKey = emailKey;
    }

    public String getGeneratedKey() {
        return generatedKey;
    }

    public void setGeneratedKey(String generatedKey) {
        this.generatedKey = generatedKey;
    }

    /**
     * This method is called before the newPassword page is rendered, and fetches the user to be updated
     */
    public void retrieve() {
        this.setUser(entryBp.userToReset(generatedKey, emailKey));
    }

    public SystemUser getUser() {
        return user;
    }

    public void setUser(SystemUser user) {
        this.user = user;
    }
}