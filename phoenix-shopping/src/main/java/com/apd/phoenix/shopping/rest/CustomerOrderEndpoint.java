/*
 * 
 */
package com.apd.phoenix.shopping.rest;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomerOrderEndpoint.
 */
@Stateless
@Path("/customerorders")
public class CustomerOrderEndpoint {

    @Inject
    CustomerOrderDao customerOrderDao;

    /**
     * Creates the.
     * @param order 
     *
     * @param entity the entity
     * @return the response
     */
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response create(CustomerOrder order) {
        this.customerOrderDao.create(order);
        return Response.created(
                UriBuilder.fromResource(CustomerOrderEndpoint.class).path(String.valueOf(order.getId())).build())
                .build();
    }

    /**
     * Delete by id.
     *
     * @param id the id
     * @return the response
     */
    @DELETE
    @Path("/{id:[0-9][0-9]*}")
    public Response deleteById(@PathParam("id") Long id) {
        CustomerOrder order = this.customerOrderDao.findById(id, CustomerOrder.class);
        if (order == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        this.customerOrderDao.delete(id, CustomerOrder.class);
        return Response.noContent().build();
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return the response
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/xml")
    public Response findById(@PathParam("id") Long id) {
        CustomerOrder order = this.customerOrderDao.findById(id, CustomerOrder.class);
        if (order == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(order).build();
    }

    /**
     * List all.
     *
     * @return the list
     */
    @GET
    @Produces("application/xml")
    public List<CustomerOrder> listAll() {
        final List<CustomerOrder> orders = this.customerOrderDao.findAll(CustomerOrder.class, 0, 0);
        return orders;
    }

    /**
     * Update.
     *
     * @param id the id
     * @param order 
     * @param entity the entity
     * @return the response
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/xml")
    public Response update(@PathParam("id") Long id, CustomerOrder order) {
        this.customerOrderDao.update(order);
        return Response.noContent().build();
    }
}