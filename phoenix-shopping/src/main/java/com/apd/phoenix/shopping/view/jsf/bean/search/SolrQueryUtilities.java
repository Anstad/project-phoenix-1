package com.apd.phoenix.shopping.view.jsf.bean.search;

import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import static com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher.SOLR_OR_DELIMETER;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.solr.client.solrj.SolrQuery;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class SolrQueryUtilities {

    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private CatalogBp catalogBp;

    public SolrQuery addFilterByCatalogID(SolrQuery solrQuery) {
        if (!EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
            Long customerCatalogID = credentialSelectionBean.getCurrentAXCXU().getCredential().getCatalog().getId();
            solrQuery = addFilterByCatalogID(solrQuery, customerCatalogID);
        }
        return solrQuery;
    }

    public SolrQuery addFilterByCatalogID(SolrQuery solrQuery, Long customerCatalogID) {

        if (!EcommercePropertiesLoader.getInstance().getEcommerceProperties().getBoolean("isDevelopmentMode")) {
            List<Long> catalogIdList = catalogBp.getParentCatalogIdList(customerCatalogID);
            //Add customerCatalogID to parent catalog id list
            catalogIdList.add(customerCatalogID);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < catalogIdList.size(); i++) {
                Long id = catalogIdList.get(i);
                sb.append(id);
                if (i != catalogIdList.size() - 1) {
                    sb.append(SOLR_OR_DELIMETER);
                }
            }
            solrQuery.addFilterQuery("CATALOG_ID:(" + sb.toString() + ")");
            //filter out items which are overridden by child catalogs
            for (Long catalogId : catalogIdList) {
                solrQuery.addFilterQuery("-overrides_id:" + catalogId + "");
            }
        }
        return solrQuery;
    }

    public static final String[] specialChars = { "\\", "\"", "/", "!", ":", "(", ")", "~", "*", "{", "}", "[", "]",
            "||", "^", "&&", "-", "+", " " };

    public String escapeSpecialChars(String input) {
        if (input != null) {
            for (String s : specialChars) {
                input = input.replace(s, "\\" + s);
            }
        }
        return input;
    }

}
