package com.apd.phoenix.shopping.view.jsf.bean.experience;

import java.io.Serializable;
import java.util.Date;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.core.EmailSender;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.business.ContactLogBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.IssueLogBp;
import com.apd.phoenix.service.model.ContactLog.TicketStatus;
import com.apd.phoenix.service.model.IssueLog;
import com.apd.phoenix.service.model.IssueLog.IssueCategory;
import com.apd.phoenix.shopping.view.jsf.bean.login.LoginBean;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author RHC
 */
@Named
@Stateful
@RequestScoped
public class IssueLogCreationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = LoggerFactory.getLogger(IssueLogCreationBean.class);

    private String ticketNumber;

    private IssueCategory issueCategory;

    private Date scheduledFollowUp;

    private Date expectedResolution;

    private String contactName;

    private String contactNumber;

    private String contactEmail;

    private String poNumber;

    private String issueDescription;

    private boolean issueCreated = false;

    @Inject
    private ContactLogBp contactLogBp;

    @Inject
    private IssueLogBp issueLogBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private LoginBean loginBean;

    public void create() {
        this.ticketNumber = contactLogBp.newTicketNumber();
        IssueLog newIssueLog = new IssueLog();
        newIssueLog.setContactName(this.contactName);
        newIssueLog.setContactNumber(this.contactNumber);
        newIssueLog.setContactEmail(this.contactEmail);
        newIssueLog.setCustomerOrder(this.customerOrderBp.searchByApdPo(this.poNumber));
        newIssueLog.setExpectedResolution(this.expectedResolution);
        newIssueLog.setIssueCategory(this.issueCategory);
        newIssueLog.setIssueDescription(this.issueDescription);
        newIssueLog.setOpenedDate(new Date());
        newIssueLog.setScheduledFollowUp(this.scheduledFollowUp);
        newIssueLog.setStatus(TicketStatus.OPEN);
        newIssueLog.setTicketNumber(this.ticketNumber);
        newIssueLog.setUpdateTimestamp(new Date());
        newIssueLog.setReporter(loginBean.getSystemUser());
        this.issueLogBp.create(newIssueLog);
        this.issueCreated = true;
        logger.debug("Created issue log: " + this.ticketNumber);
        try {
            EmailSender emailSender = new EmailSender();
            emailSender.setTo(EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString(
                    "issuelog.notification.email"));
            emailSender.setFrom("no-reply@apd.com");
            emailSender.setSubject("Password Reset");
            emailSender.setBody("Issue log " + this.ticketNumber + " has been logged by a customer.");

            //sends the email
            //this line throws an Exception, if applicable
            emailSender.send();
            logger.debug("Sent notification email about issue: " + this.ticketNumber);
        }
        catch (Exception e) {
            //do nothing
        }
    }

    public SelectItem[] getIssueCategories() {
        SelectItem[] items = new SelectItem[IssueCategory.values().length];
        int i = 0;
        for (IssueCategory g : IssueCategory.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

    public boolean isTicketCreated() {
        return StringUtils.isNotBlank(this.ticketNumber);
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public Date getScheduledFollowUp() {
        return null;
    }

    public void setScheduledFollowUp(Date scheduledFollowUp) {
        this.scheduledFollowUp = scheduledFollowUp;
    }

    public Date getExpectedResolution() {
        return null;
    }

    public void setExpectedResolution(Date expectedResolution) {
        this.expectedResolution = expectedResolution;
    }

    public String getIssueDescription() {
        return "";
    }

    public void setIssueDescription(String issueDescription) {
        this.issueDescription = issueDescription;
    }

    public IssueCategory getIssueCategory() {
        return null;
    }

    public void setIssueCategory(IssueCategory issueCategory) {
        this.issueCategory = issueCategory;
    }

    public String getPoNumber() {
        return "";
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getContactName() {
        return "";
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return "";
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return "";
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public boolean isIssueCreated() {
        return this.issueCreated;
    }
}
