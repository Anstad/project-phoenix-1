package com.apd.phoenix.shopping.view.jsf.bean.product;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.service.business.AccountXCredentialXUserBp;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.business.FavoritesListBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.shopping.view.jsf.bean.MarketingImagesBean;
import com.apd.phoenix.shopping.view.jsf.bean.cashout.AddToCartBean;
import com.apd.phoenix.shopping.view.jsf.bean.favorites.FavoritesBean;
import com.apd.phoenix.shopping.view.jsf.bean.login.CredentialSelectionBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.shopping.view.jsf.bean.utils.ViewUtils;
import javax.faces.application.FacesMessage;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

@Named
@Stateful
@LocalBean
@RequestScoped
public class ProductDetailsBean implements Serializable, SolrSearcher {

    private static final long serialVersionUID = 4952590880241861324L;

    private static final Logger logger = LoggerFactory.getLogger(ProductDetailsBean.class);
    private static final String SPACER = "  ";
    private CatalogXItem catalogXItem;
    private ItemImage selectedItemImage;
    private String selectedFavoritesListName;
    private String apdSku;
    private String vendorName;
    private Product product;
    private Product substituteProduct;
    private List<ItemImage> itemImages;
    private List<Product> similarItemList;
    private Map<String, String> specifications;
    private Map<String, String> classifications;
    private List<ItemSellingPoint> sellingPoints;
    @Inject
    private ItemBp itemBp;
    @Inject
    private CatalogXItemBp catalogXItemBp;
    @Inject
    private FavoritesListBp favoritesListBp;
    @Inject
    private AccountXCredentialXUserBp accountXCredentialXUserBp;
    @Inject
    private CredentialSelectionBean credentialSelectionBean;
    @Inject
    private FavoritesBean favoritesBean;
    @Inject
    private ViewUtils viewUtils;
    @Inject
    private CatalogBp catalogBp;
    @Inject
    private LinksBean linksBean;
    @Inject
    private AddToCartBean addToCartBean;
    private Integer qty;
    private boolean productAddedToList = false;
    private String displayFavoritesListName;

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isValidationFailed()) {
            redirectInvalidRequest();
        }
        if (!FacesContext.getCurrentInstance().isPostback()) {
            initializePageData();
        }
    }

    private void redirectInvalidRequest() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(SEARCH_PAGE_BASE_URL);
        }
        catch (Exception ex) {
            logger.error("Error redirecting to product browse page", ex);
        }
    }

    private void initializePageData() {
        retrieveCatalogXItem();
        if (catalogXItem == null) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect(linksBean.getItemNotFound());
            }
            catch (IOException ex) {
                logger.error("IOException during redirect to product not found page.", ex);
            }
        }
        else {
            retrieveProductInfo();
            retrieveItemSpecifications();
            retrieveItemClassifications();
            retrieveItemSellingPoints();
            retrieveItemImages();
            retrieveSubstitutionInfo();
            retrieveSimilarItems();
        }
    }

    private void retrieveSimilarItems() {
        List<CatalogXItem> itemList = new ArrayList<CatalogXItem>();
        itemList = catalogXItemBp.getCatalogXItemsSimilarItems(catalogXItem);

        //set itemList as 'this' similar item list
        this.setSimilarItemList(viewUtils.convertCatalogXItemsToProducts(itemList));
    }

    private CatalogXItem retrieveCatalogXItem() {
        Item resultItem = itemBp.searchItem(apdSku, vendorName);
        catalogXItem = new CatalogXItem();
        catalogXItem.setItem(resultItem);
        catalogXItem.setCatalog(new Catalog());
        long currentCatalogID = credentialSelectionBean.getCurrentAXCXU().getCredential().getCatalog().getId();
        catalogXItem.getCatalog().setId(currentCatalogID);
        List<CatalogXItem> catxItems = catalogXItemBp.searchByExactExample(catalogXItem, 0, 0);
        if (catxItems.isEmpty()) {
            catxItems = checkParentCatalogs(currentCatalogID);
            if (catxItems.isEmpty()) {
                catalogXItem = null;
            }
            else {
                catalogXItem = catxItems.get(0);
                catalogXItem = catalogXItemBp.findById(catalogXItem.getId(), CatalogXItem.class);
            }
        }
        else {
            catalogXItem = catxItems.get(0);
            catalogXItem = catalogXItemBp.findById(catalogXItem.getId(), CatalogXItem.class);
        }
        return catalogXItem;
    }

    private List<CatalogXItem> checkParentCatalogs(long catalogID) {
        List<Long> catalogIdList = catalogBp.getParentCatalogIdList(catalogID);
        List<CatalogXItem> catxItems = new ArrayList<>();
        for (int i = 0; i < catalogIdList.size(); i++) {
            Long id = catalogIdList.get(i);
            Item resultItem = itemBp.searchItem(apdSku, vendorName);
            catalogXItem = new CatalogXItem();
            catalogXItem.setItem(resultItem);
            catalogXItem.setCatalog(new Catalog());
            catalogXItem.getCatalog().setId(id);
            catxItems = catalogXItemBp.searchByExactExample(catalogXItem, 0, 0);
            if (!catxItems.isEmpty()) {
                break;
            }
        }
        return catxItems;
    }

    private void retrieveProductInfo() {
        product = viewUtils.getProductFromCatalogXItem(catalogXItem);
    }

    private void retrieveItemClassifications() {
        classifications = catalogXItem.getItem().getClassificationStringMap();
    }

    private void retrieveItemSellingPoints() {
        this.sellingPoints = new ArrayList<ItemSellingPoint>();
        for (ItemSellingPoint sellingPoint : catalogXItem.getItem().getSellingPoints()) {
            this.sellingPoints.add(sellingPoint);
        }
        Collections.sort(this.sellingPoints, new EntityComparator());
    }

    private void retrieveItemSpecifications() {
        specifications = catalogXItem.getItem().getSpecifications();
    }

    private void retrieveItemImages() {
        itemImages = new ArrayList<>(catalogXItem.getItem().getItemImages());
    }

    public Product getProduct() {
        if (product == null) {
            reinit();
        }
        return product;
    }

    public String getProductName() {
        return product.getName();
    }

    public String getProductDescription() {
        return product.getDescription();
    }

    public String getSafeProductDescription() {
        String description = product.getDescription() == null ? "" : Jsoup.clean(product.getDescription(), Whitelist
                .none());
        description = description.replaceAll("\\&quot;", "\"").replaceAll("\\&amp;", "&");
        return description;
    }

    public String getProductManufacturer() {
        return product.getManufacturerSku();
    }

    public String getProductPrice() {
        BigDecimal price = product.getPrice();
        return price.toString();
    }

    public BigDecimal getProductPriceDecimal() {
        return product.getPrice();
    }

    public String getProductSku() {
        //Note customer sku will be the same as the apdSku if a true customer sku did not exist
        return product.getCustomerSku();
    }

    public String getVendorSku() {
        return product.getVendorSku();
    }

    public List<String> getItemSpecificationKeys() {
        if (specifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(specifications.keySet());
    }

    public String getItemSpecificationValue(String key) {
        return specifications.get(key);
    }

    public List<String> getItemClassificationKeys() {
        if (classifications == null) {
            return new ArrayList<>();
        }
        return new ArrayList<>(classifications.keySet());
    }

    public String getItemClassificationValue(String key) {
        return classifications.get(key);
    }

    public void retrieveSubstitutionInfo() {
        CatalogXItem substitute = catalogXItem.getSubstituteItem();
        if (substitute != null) {
            substituteProduct = viewUtils.getProductFromCatalogXItem(substitute);
        }
    }

    public void addProductToFavorites() {
        this.reinit();
        FavoritesList selectedList = new FavoritesList();
        selectedList.setName(selectedFavoritesListName);
        selectedList.setId(getMatchingListID());
        if (selectedList.getId() == -1) {
            //Add new list
            favoritesBean.addNewUserList(selectedFavoritesListName);
            selectedList.setId(getMatchingListID());
            if (selectedList.getId() == -1) {
                logger.error("Could not add new favorites list.");
                FacesContext.getCurrentInstance().addMessage(null,
                        new FacesMessage("Error creating new favorites list"));
            }
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        else {
            selectedList = favoritesListBp.searchByExactExample(selectedList, 0, 0).get(0);
            selectedList = favoritesListBp.findById(selectedList.getId(), FavoritesList.class);
            selectedList.getUserItems().add(catalogXItem);
            selectedList = favoritesListBp.update(selectedList);
            favoritesBean.refreshPage();
        }
        favoritesBean.updateUserFavoritesList();
        this.initializePageData();
        this.productAddedToList = true;
        try {
            String str = this.product.getDetailsLink();
            str = str + "&productAddedToList=true&displayFavoritesListName="
                    + StringEscape.escapeForUrl(selectedFavoritesListName);
            FacesContext.getCurrentInstance().getExternalContext().redirect(str);
        }
        catch (Exception e) {
            //if an error occurs, there won't be any indication that the item was added to the list 
            logger.warn("Error when redirecting after favorites list add", e);
        }
    }

    private long getMatchingListID() {
        FavoritesList list = accountXCredentialXUserBp.getFavoritesListByNameAndCredential(selectedFavoritesListName,
                this.credentialSelectionBean.getCurrentAXCXU());
        if (list != null) {
            return list.getId();
        }
        return -1;
    }

    public boolean isFilePresent(String path) {
        final File file = new File(path);
        return file.exists();
    }

    public CatalogXItem getCatalogXItem() {
        return this.catalogXItem;
    }

    public void setCatalogXItem(CatalogXItem instance) {
        this.catalogXItem = instance;
    }

    public List<ItemImage> getItemImages() {
        return this.itemImages;
    }

    public void setItemImages(List<ItemImage> itemImages) {
        this.itemImages = itemImages;
    }

    public ItemImage getSelectedItemImage() {
        return this.selectedItemImage;
    }

    public void setSelectedItemImage(ItemImage image) {
        this.selectedItemImage = image;
    }

    public List<Product> getSimilarItemList() {
        return similarItemList;
    }

    public void setSimilarItemList(List<Product> similarItemList) {
        this.similarItemList = similarItemList;
    }

    public String getURLString(Set<String> stringSet) {
        if (stringSet != null && !stringSet.isEmpty()) {
            return (String) stringSet.toArray()[0];
        }
        return "";
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getSelectedFavoritesListName() {
        return selectedFavoritesListName;
    }

    public void setSelectedFavoritesListName(String selectedFavoritesListName) {
        this.selectedFavoritesListName = selectedFavoritesListName;
    }

    public String getSubstituteName() {
        return substituteProduct.getName();
    }

    public String getSubstituteApdSku() {
        return substituteProduct.getApdSku();
    }

    public String getSubstituteImageUrl() {
        for (String imageUrl : substituteProduct.getImageUrls()) {
            return imageUrl;
        }
        return MarketingImagesBean.getImageUrl("productDetailsBean1");
    }

    public String getSubstitutionDetailsLink() {
        return substituteProduct.getDetailsLink();
    }

    public boolean isSubstituteAvailable() {
        if (substituteProduct == null) {
            return false;
        }
        return true;
    }

    public String getSubstituteManufacturer() {
        return substituteProduct.getManufacturerName();
    }

    public String getSubstituteSku() {
        return SPACER + substituteProduct.getCustomerSku();
    }

    public BigDecimal getSubstitutePrice() {
        return substituteProduct.getPrice();
    }

    public CatalogXItem addToCart(String apdSku, String vendorName) {
        this.apdSku = apdSku;
        this.vendorName = vendorName;
        return retrieveCatalogXItem();
    }

    private void reinit() {
        try {
            String url = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer");
            if (StringUtils.isBlank(url)) {
                url = ((HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())
                        .getRequestURL().toString();
            }
            List<NameValuePair> parameters = URLEncodedUtils.parse(new URI(url), "UTF-8");
            for (NameValuePair nvp : parameters) {
                if (nvp.getName().equals("apdSku")) {
                    apdSku = nvp.getValue();
                }
                if (nvp.getName().equals("vendorName")) {
                    vendorName = nvp.getValue();
                }
            }
            if (apdSku != null && vendorName != null) {
                retrieveCatalogXItem();
                retrieveProductInfo();
            }
        }
        catch (URISyntaxException ex) {
            //do nothing
        }
    }

    public void updateQuantity() {
        if (qty != null) {
            if (qty < 1) {
                qty = 1;
            }
            if (product == null) {
                reinit();
            }
            if (qty != null && product != null) {
                addToCartBean.getDisplayQuantity().put(product, qty.toString());
            }
        }
    }

    public void addToCart() {
        if (product == null) {
            reinit();
        }
        if (product != null) {
            addToCartBean.addSingleItem(product);
        }
    }

    /**
     * @return the qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * @param qty the qty to set
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public List<ItemSellingPoint> getSellingPoints() {
        return sellingPoints;
    }

    public void setSellingPoints(List<ItemSellingPoint> sellingPoints) {
        this.sellingPoints = sellingPoints;
    }

    public boolean getProductAddedToList() {
        return this.productAddedToList;
    }

    public void setProductAddedToList(boolean productAddedToList) {
        this.productAddedToList = productAddedToList;
    }

    public String getDisplayFavoritesListName() {
        return displayFavoritesListName;
    }

    public void setDisplayFavoritesListName(String displayFavoritesListName) {
        this.displayFavoritesListName = displayFavoritesListName;
    }

}
