/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.shopping.view.jsf.bean;

import com.apd.phoenix.shopping.view.jsf.bean.search.Product;
import com.apd.phoenix.shopping.view.jsf.bean.search.SearchUtilities;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrSearcher;
import com.apd.phoenix.shopping.view.jsf.bean.utils.LinksBean;
import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.core.PageNumberDisplay.PageSize;
import com.apd.phoenix.service.solr.SolrServiceBean;
import com.apd.phoenix.shopping.view.jsf.bean.search.SolrQueryUtilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dcnorris
 */
@Named
@Stateful
@LocalBean
//TODO consider converting this to conversation scope
@SessionScoped
public class HomeBean implements SolrSearcher, Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private LinksBean linksBean;
    private static final Logger logger = LoggerFactory.getLogger(HomeBean.class);
    private List<Product> featuredProductList = new ArrayList<>();
    private QueryResponse response;
    private SolrServer solr;
    private SolrQuery previousSolrQuery;
    private int previousPage;
    private PageNumberDisplay pageDisplay;
    private int numFound;
    private PageNumberDisplay.PageSize pageSize = PageNumberDisplay.PageSize.FOUR;
    @Inject
    private SolrQueryUtilities solrQueryUtilities;
    @Inject
    private SolrServiceBean solrServiceBean;

    @PostConstruct
    public void init() {
        try {
            solr = solrServiceBean.getSolrServer();
            SolrQuery solrQuery = new SolrQuery();
            solrQuery = solrQueryUtilities.addFilterByCatalogID(solrQuery);
            solrQuery.setStart(pageSize.toInt());
            solrQuery.setRows(pageSize.toInt());
            String startRange = "[" + "NOW-99YEARS" + " TO " + "NOW" + "]";
            solrQuery.add("q", "coreitemstartdate:" + startRange);
            String expireRange = "[" + "NOW+1DAY" + " TO " + "NOW+99YEARS" + "]";
            solrQuery.add("q", "coreitemexpirationdate:" + expireRange);
            Random rand = new Random();
            int value = rand.nextInt(99999);
            solrQuery.addSort("random_" + value, SolrQuery.ORDER.asc);
            response = solr.query(solrQuery);
            numFound = (int) response.getResults().getNumFound();
            previousSolrQuery = solrQuery;
            SolrDocumentList searchResultDocuments = response.getResults();
            featuredProductList = SearchUtilities.convertToProductList(searchResultDocuments);
            pageDisplay = new PageNumberDisplay(numFound);
            pageDisplay.setPageSize(pageSize);
        } catch (SolrServerException ex) {
            logger.error("Error attempting to Query Solr Index Server", ex);
        }
    }
    
    public List<Product> getFeaturedProductList() {
        if (pageDisplay.getPage() != previousPage) {
            try {
                previousPage = pageDisplay.getPage();
                previousSolrQuery.setStart((pageDisplay.getPage() - 1) * pageDisplay.getSize());
                QueryResponse updatedResponse = solr.query(previousSolrQuery);
                SolrDocumentList updatedDocList = updatedResponse.getResults();
                featuredProductList = SearchUtilities.convertToProductList(updatedDocList);
            } catch (SolrServerException ex) {
                logger.error("Error sending query to solr", ex);
            }
        }
        return featuredProductList;
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public int getPreviousPage() {
        return previousPage;
    }

    public void setPreviousPage(int previousPage) {
        this.previousPage = previousPage;
    }

    public PageNumberDisplay getPageDisplay() {
        return pageDisplay;
    }

    public void setPageDisplay(PageNumberDisplay pageDisplay) {
        this.pageDisplay = pageDisplay;
    }

    public String getFavoritesManagementLink() {
        return linksBean.getUserFavorites()+"?showList=true";
    }

}
