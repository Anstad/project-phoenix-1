package com.apd.phoenix.shopping.view.jsf.bean.login;

import java.io.IOException;
import java.io.Serializable;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.apd.phoenix.service.business.IpSubdomainRestriction;

@WebFilter
public class IpSubdomainFilter implements Serializable, Filter {

    @Inject
    private LoginBean loginBean;

    @Inject
    private CredentialSelectionBean credBean;

    @Inject
    private IpSubdomainRestriction restriction;

    @Override
    public void destroy() {
        //do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
            ServletException {

        //gets the subdomain and the ip address
        String[] ipAndSubdomain = IpSubdomainRestriction.ipAndSubdomain((HttpServletRequest) request);
        String ipAddress = ipAndSubdomain[0];
        String subdomain = ipAndSubdomain[1];

        //check if the IP address and subdomains have been used successfully
        if (loginBean.getValidIpAddresses().contains(ipAddress) && loginBean.getValidSubdomains().contains(subdomain)) {
            //if valid, then continues
            chain.doFilter(request, response);
            return;
        }

        //if the IP address and subdomain haven't been used, checks that they are valid with the credential
        if (credBean.getCurrentAXCXU() != null
                && restriction.isAccXCredXUserValid(credBean.getCurrentAXCXU(), (HttpServletRequest) request, credBean
                        .isPunchoutSession())) {
            loginBean.getValidIpAddresses().add(ipAddress);
            loginBean.getValidSubdomains().add(subdomain);
            chain.doFilter(request, response);
            return;
        }

        //if there is no credential assigned, checks that the user can log in
        if (credBean.getCurrentAXCXU() == null
                && loginBean.getSystemUser() != null
                && restriction.isUserValid(loginBean.getSystemUser(), (HttpServletRequest) request, credBean
                        .isPunchoutSession())) {
            loginBean.getValidIpAddresses().add(ipAddress);
            loginBean.getValidSubdomains().add(subdomain);
            chain.doFilter(request, response);
            return;
        }

        //if the ip address or subdomain is different, 403
        ((HttpServletResponse) response).sendError(HttpServletResponse.SC_FORBIDDEN);
        return;
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
        //do nothing
    }

}
