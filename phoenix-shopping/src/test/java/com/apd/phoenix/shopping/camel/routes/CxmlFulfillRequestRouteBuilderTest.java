package com.apd.phoenix.shopping.camel.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.testng.Assert;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeHeader;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeRequest;

public class CxmlFulfillRequestRouteBuilderTest {

    private CamelContext camelContext;

    private ProducerTemplate producer;

    private String shipNoticeRequestSource = "direct:shipNoticeRequestSource";

    private String shipNoticeRequestOutput = "mock:shipNoticeRequestOutput";

    private MockEndpoint mockShipNoticeRequestOutput;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        FulfillRequestTypeRouteBuilder route = new FulfillRequestTypeRouteBuilder();

        mockShipNoticeRequestOutput = camelContext.getEndpoint(shipNoticeRequestOutput, MockEndpoint.class);
        route.setFulfillShipNoticeRequestSource(shipNoticeRequestSource);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void teardown() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testShipNotice() throws InterruptedException {
        ShipNoticeRequest shipNoticeRequest = new ShipNoticeRequest();
        ShipNoticeHeader shipNoticeHeader = new ShipNoticeHeader();
        shipNoticeHeader.setShipmentID("1234");
        shipNoticeRequest.setShipNoticeHeader(shipNoticeHeader);

        mockShipNoticeRequestOutput.setExpectedMessageCount(1);
        com.apd.phoenix.service.integration.cxml.model.fulfill.Response response = (com.apd.phoenix.service.integration.cxml.model.fulfill.Response) producer
                .requestBody(shipNoticeRequestSource, shipNoticeRequest);
        mockShipNoticeRequestOutput.setResultWaitTime(2000);
        mockShipNoticeRequestOutput.assertIsSatisfied();
        Assert.assertEquals("200", response.getStatus().getCode());
        Assert.assertEquals("OK", response.getStatus().getText());
    }

    @Ignore
    @Test
    public void testInvalidShipNotice() throws InterruptedException {
        ShipNoticeRequest shipNoticeRequest = new ShipNoticeRequest();
        ShipNoticeHeader shipNoticeHeader = new ShipNoticeHeader();
        shipNoticeHeader.setShipmentID("");
        shipNoticeRequest.setShipNoticeHeader(shipNoticeHeader);

        mockShipNoticeRequestOutput.setExpectedMessageCount(0);
        com.apd.phoenix.service.integration.cxml.model.fulfill.Response response = (com.apd.phoenix.service.integration.cxml.model.fulfill.Response) producer
                .requestBody(shipNoticeRequestSource, shipNoticeRequest);
        mockShipNoticeRequestOutput.setResultWaitTime(2000);
        mockShipNoticeRequestOutput.assertIsSatisfied();
        Assert.assertEquals("400", response.getStatus().getCode());
        Assert.assertEquals("Failed", response.getStatus().getText());
    }
}
