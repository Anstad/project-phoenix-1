package com.apd.phoenix.shopping.view.selenium.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(id = "j_username")
    private WebElement loginInput;

    @FindBy(id = "j_password")
    private WebElement passwordInput;

    @FindBy(id = "submit")
    private WebElement submitButton;

    /**
     * Presses the "Login" button
     */
    public void loginPress() {
        this.submitButton.click();
    }

    /**
     * Sets the login 
     * 
     * @param login
     */
    public void enterLogin(String login) {

        this.loginInput.clear();
        this.loginInput.sendKeys(login);
    }

    /**
     * Sets the password
     * 
     * @param password
     */
    public void enterPassword(String password) {

        this.passwordInput.clear();
        this.passwordInput.sendKeys(password);
    }

}