package com.apd.phoenix.shopping.view.jsf.bean.login;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.graphene.spi.annotations.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.PasswordResetEntryBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.PasswordResetEntry;
import com.apd.phoenix.service.persistence.jpa.PasswordResetEntryDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;
import com.apd.phoenix.shopping.view.jsf.bean.WebTest;
import com.apd.phoenix.shopping.view.selenium.login.LoginPage;

@RunWith(Arquillian.class)
public class ShoppingLoginTest extends WebTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShoppingLoginTest.class);

    @Page
    private LoginPage loginPage;

    @Deployment(testable = false)
    public static WebArchive createDeployment() {

        WebArchive wa = WebTest.createDeployment().addClasses(LoginBean.class, SystemUserBp.class, SystemUserDao.class,
                PasswordResetEntryBp.class, PasswordResetEntry.class, PasswordResetEntryDao.class);

        return wa;
    }

    /**
     * This method is called before all tests are run. Prompts the user to execute the data load script.
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void prepareTestSuite() throws Exception {
        LOGGER.info("Please execute the data load script, and press enter when ready.");
        System.in.read();
    }

    /**
     * This method is called before each test, and enters the product browse URL into the browser.
     * 
     * @throws Exception
     */
    @Before
    public void prepareTest() throws Exception {
        driver.get(deploymentUrl + "ecommerce/product/browse.xhtml");
    }

    /**
     * Checks that visiting the browse page returns the login page when not logged in
     * 
     * @throws InterruptedException
     */
    @Test
    public void newVisitTest() throws InterruptedException {
        String expectedTitle = "eCommerce Login";
        String pageTitle = driver.getTitle();

        Assert.assertEquals(expectedTitle, pageTitle);
    }

    /**
     * Checks that entering a login that doesn't exist doesn't allow access
     * 
     * @throws InterruptedException
     */
    @Test
    public void incorrectLoginTest() throws InterruptedException {

        loginPage.enterLogin("bad login");
        loginPage.enterPassword("bad password");
        loginPage.loginPress();

        assertNotLoggedIn();
    }

    /**
     * Checks that entering a valid login with an incorrect password doesn't allow access
     * 
     * @throws InterruptedException
     */
    @Test
    public void incorrectPasswordTest() throws InterruptedException {

        loginPage.enterLogin("admin");
        loginPage.enterPassword("bad password");
        loginPage.loginPress();

        assertNotLoggedIn();
    }

    /**
     * Checks that entering a valid login with a valid password allows access
     * 
     * @throws InterruptedException
     */
    @Test
    public void correctLoginTest() throws InterruptedException {

        loginPage.enterLogin("admin");
        loginPage.enterPassword("admin");
        loginPage.loginPress();

        assertLoggedIn();
    }

    /**
     * Test fails if this method is called and the user isn't logged in
     */
    private void assertLoggedIn() {
        Assert.assertTrue(driver.getCurrentUrl().contains("product/browse.xhtml"));
    }

    /**
     * Test fails if this method is called and the user is logged in
     */
    private void assertNotLoggedIn() {
        Assert.assertFalse(driver.getCurrentUrl().contains("product/browse.xhtml"));
    }
}
