package com.apd.phoenix.shopping.camel.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.testng.Assert;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequest;
import com.apd.phoenix.service.integration.cxml.model.cxml.OrderRequestHeader;
import com.apd.phoenix.service.integration.cxml.model.cxml.PunchOutSetupRequest;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class CxmlCxmlRequestRouteBuilderTest {

    private CamelContext camelContext;

    private ProducerTemplate producer;

    private String orderRequestSource = "direct:orderRequestSource";

    private String orderRequestOutput = "mock:orderRequestOutput";

    private MockEndpoint mockOrderRequestOutput;

    private String punchoutSetupRequestSource = "direct:punchoutSetupRequestSource";

    private String punchoutSetupRequestOutput = "mock:punchoutSetupRequestOutput";

    private MockEndpoint mockPunchoutSetupRequestOutput;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        CxmlRequestTypeRouteBuilder route = new CxmlRequestTypeRouteBuilder();

        mockOrderRequestOutput = camelContext.getEndpoint(orderRequestOutput, MockEndpoint.class);
        route.setCxmlOrderRequestSource(orderRequestSource);
        route.setCxmlOrderRequestOutput(orderRequestOutput);

        mockPunchoutSetupRequestOutput = camelContext.getEndpoint(punchoutSetupRequestOutput, MockEndpoint.class);
        route.setCxmlPunchOutSetupRequestSource(punchoutSetupRequestSource);
        route.setCxmlPunchOutSetupRequestOutput(punchoutSetupRequestOutput);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void teardown() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testOrderRequest() throws InterruptedException, IOException, JAXBException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("orderRequest.xml");

        JAXBContext jc = JAXBContext.newInstance("com.apd.phoenix.service.integration.cxml.model.cxml");
        Unmarshaller u = jc.createUnmarshaller();
        OrderRequest orderRequest = (OrderRequest) u.unmarshal(ips);
        mockOrderRequestOutput.setExpectedMessageCount(1);
        com.apd.phoenix.service.integration.cxml.model.cxml.Response response = (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producer
                .requestBody(orderRequestSource, orderRequest);
        mockOrderRequestOutput.setResultWaitTime(2000);
        mockOrderRequestOutput.assertIsSatisfied();
        Assert.assertEquals("200", response.getStatus().getCode());
        Assert.assertEquals("OK", response.getStatus().getText());
    }

    @Ignore
    @Test
    public void testInvalidOrderRequest() throws InterruptedException {
        OrderRequest orderRequest = new OrderRequest();
        OrderRequestHeader orderRequestHeader = new OrderRequestHeader();
        orderRequestHeader.setOrderID("");
        orderRequest.setOrderRequestHeader(orderRequestHeader);

        mockOrderRequestOutput.setExpectedMessageCount(0);
        com.apd.phoenix.service.integration.cxml.model.cxml.Response response = (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producer
                .requestBody(orderRequestSource, orderRequest);
        mockOrderRequestOutput.setResultWaitTime(2000);
        mockOrderRequestOutput.assertIsSatisfied();
        Assert.assertEquals("400", response.getStatus().getCode());
        Assert.assertEquals("Failed", response.getStatus().getText());
    }

    @Ignore
    @Test
    public void testPunchoutSetupRequest() throws InterruptedException {
        PunchOutSetupRequest punchoutSetupRequest = new PunchOutSetupRequest();

        mockPunchoutSetupRequestOutput.setExpectedMessageCount(1);
        com.apd.phoenix.service.integration.cxml.model.cxml.Response response = (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producer
                .requestBody(punchoutSetupRequestSource, punchoutSetupRequest);
        mockPunchoutSetupRequestOutput.setResultWaitTime(2000);
        mockPunchoutSetupRequestOutput.assertIsSatisfied();
        Assert.assertEquals("200", response.getStatus().getCode());
        Assert.assertEquals("OK", response.getStatus().getText());
    }

    @Ignore
    @Test
    public void testInvalidPunchoutSetupRequest() throws InterruptedException {
        PunchOutSetupRequest punchoutSetupRequest = new PunchOutSetupRequest();

        mockPunchoutSetupRequestOutput.setExpectedMessageCount(0);
        com.apd.phoenix.service.integration.cxml.model.cxml.Response response = (com.apd.phoenix.service.integration.cxml.model.cxml.Response) producer
                .requestBody(punchoutSetupRequestSource, punchoutSetupRequest);
        mockPunchoutSetupRequestOutput.setResultWaitTime(2000);
        mockPunchoutSetupRequestOutput.assertIsSatisfied();
        Assert.assertEquals("400", response.getStatus().getCode());
        Assert.assertEquals("Failed", response.getStatus().getText());
    }
}
