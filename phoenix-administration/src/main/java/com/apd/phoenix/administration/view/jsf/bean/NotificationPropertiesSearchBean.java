package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.NotificationPropertiesDao;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class NotificationPropertiesSearchBean extends AbstractSearchBean<NotificationProperties> implements
        Serializable {

    @Inject
    NotificationPropertiesDao bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = {};
        return toReturn;
    }

    @Override
    protected List<NotificationProperties> search() {
        NotificationProperties search = new NotificationProperties();
        return bp.searchByExample(search, 0, 0);
    }

}
