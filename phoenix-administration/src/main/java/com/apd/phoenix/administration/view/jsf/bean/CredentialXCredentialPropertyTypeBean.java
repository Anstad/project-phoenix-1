package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.persistence.jpa.CredentialXCredentialPropertyTypeDao;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for CredentialXCredentialPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all CredentialXCredentialPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CredentialXCredentialPropertyTypeBean extends AbstractControllerBean<CredentialXCredentialPropertyType>
        implements Serializable {

    //   /*
    //    * Support creating and retrieving CredentialXCredentialPropertyType entities
    //    */
    //   
    //   public void validateSelection(FacesContext context, UIComponent component, Object value) {
    //       if(this.currentInstance.getId() == null) {
    //    	   ((UIInput)component).setValid(false);
    //           FacesMessage message = new FacesMessage();
    //           message.setSeverity(FacesMessage.SEVERITY_ERROR);
    //           message.setSummary("Select Credential Property Type to edit");
    //           message.setDetail("You need to select a Credential Property Type to edit.");
    //           context.addMessage(component.getClientId(), message);
    //           return;
    //       }
    //   }

    //   private Long id;
    //
    //   public Long getId()
    //   {
    //      return this.id;
    //   }
    //
    //   public void setId(Long id)
    //   {
    //      this.id = id;
    //   }

    private static final long serialVersionUID = -1112357072600311498L;

    //	private CredentialXCredentialPropertyType currentInstance;
    //
    //   public CredentialXCredentialPropertyType getCredentialProperty()
    //   {
    //      return this.currentInstance;
    //   }

    @Inject
    private CredentialXCredentialPropertyTypeDao dao;

    //   @Inject
    //   private Conversation conversation;

    //   public String create()
    //   {
    //       this.conversation.begin();
    //       return "CredentialXCredentialPropertyTypeAdd?faces-redirect=true";
    //   }

    //   @Override
    //   public void retrieve()
    //   {
    //
    //		if (this.conversation.isTransient())
    //		{
    //		        this.conversation.begin();
    //		}
    //		
    //		if (this.id == null) {
    //		        this.currentInstance = new CredentialXCredentialPropertyType();
    //		} else {
    //		        this.currentInstance = dao.findById(id, CredentialXCredentialPropertyType.class);
    //		}
    //   }

    public CredentialXCredentialPropertyType findById(Long id) {

        return this.dao.findById(id, CredentialXCredentialPropertyType.class);
    }

    //   /*
    //    * Support updating and deleting CredentialXCredentialPropertyType entities
    //    */
    //
    //   @Override
    //public String update()
    //   {
    //           this.conversation.end();
    //           
    //           try
    //           {
    //                  
    //              if (currentInstance.getId() == null)
    //              {
    //            	 dao.create(this.currentInstance);
    //                 return "CredentialXCredentialPropertyTypeEdit?faces-redirect=true";
    //              }
    //              else
    //              {
    //            	 dao.update(this.currentInstance);
    //                 return "CredentialXCredentialPropertyTypeEdit?faces-redirect=true&id=" + currentInstance.getId();
    //              }
    //           }
    //           catch (Exception e)
    //           {
    //              FacesContext.getCurrentInstance().addMessage(null,
    //					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    //              return null;
    //           }
    //   }
    //
    //   @Override
    //public String delete()
    //   {
    //           this.conversation.end();
    //           try { 
    //               dao.delete(this.currentInstance.getId(), CredentialXCredentialPropertyType.class);
    //               return "CredentialXCredentialPropertyTypeEdit?faces-redirect=true";
    //           }
    //           catch (Exception e) {
    //                   FacesContext.getCurrentInstance().addMessage(null,
    //					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    //               return null;
    //           }
    //   }

    /*
     * Support searching CredentialXCredentialPropertyType entities with pagination
     */
    //--------
    //   private int page;
    //   private long count;
    //   private List<CredentialXCredentialPropertyType> pageItems;
    //
    //   private CredentialXCredentialPropertyType example = new CredentialXCredentialPropertyType();
    //
    //   public int getPage()
    //   {
    //      return this.page;
    //   }
    //
    //   public void setPage(int page)
    //   {
    //      this.page = page;
    //   }
    //
    //   public int getPageSize()
    //   {
    //      return 10;
    //   }
    //
    //   public CredentialXCredentialPropertyType getExample()
    //   {
    //      return this.example;
    //   }
    //
    //   public void setExample(CredentialXCredentialPropertyType example)
    //   {
    //      this.example = example;
    //   }
    //
    //   @Override
    //public void search()
    //   {
    //      this.page = 0;
    //   }
    //
    //   public void paginate()
    //   {
    //          this.pageItems = dao.searchByExample(this.example, getPage()*getPageSize(), getPageSize());
    //          
    //          this.count = dao.resultQuantity(this.example);
    //   }
    //
    //   public List<CredentialXCredentialPropertyType> getPageItems()
    //   {
    //      return this.pageItems;
    //   }
    //
    //   public long getCount()
    //   {
    //      return this.count;
    //   }
    //---------
    /*
     * Support listing and POSTing back CredentialXCredentialPropertyType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    //   public List<CredentialXCredentialPropertyType> getAll()
    //   {
    //      return dao.findAll(CredentialXCredentialPropertyType.class, 0, 0);
    //   }
    //   
    //   public List<CredentialXCredentialPropertyType> searchByExample(CredentialXCredentialPropertyType search) {
    //	   return dao.searchByExample(search, 0, 0);
    //   }

    public List<CredentialXCredentialPropertyType> searchByString(String s) {
        CredentialXCredentialPropertyType search = new CredentialXCredentialPropertyType();
        search.setValue(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    @Override
    public Converter getConverter() {

        final CredentialXCredentialPropertyTypeBean ejbProxy = this.sessionContext
                .getBusinessObject(CredentialXCredentialPropertyTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CredentialXCredentialPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    //   private CredentialXCredentialPropertyType add = new CredentialXCredentialPropertyType();
    //
    //   public CredentialXCredentialPropertyType getAdd()
    //   {
    //      return this.add;
    //   }
    //
    //   public CredentialXCredentialPropertyType getAdded()
    //   {
    //      CredentialXCredentialPropertyType added = this.add;
    //      this.add = new CredentialXCredentialPropertyType();
    //      return added;
    //   }
}