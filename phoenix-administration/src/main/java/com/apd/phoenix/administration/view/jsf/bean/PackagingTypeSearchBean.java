package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PackagingTypeDao;
import com.apd.phoenix.service.model.PackagingType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PackagingTypeSearchBean extends AbstractSearchBean<PackagingType> implements Serializable {

    @Inject
    PackagingTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Value" };
        return toReturn;
    }

    @Override
    protected List<PackagingType> search() {
        PackagingType search = new PackagingType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
