package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressPropertyType;

/**
 * Backing bean for AddressXAddressPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all AddressXAddressPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class AddressXAddressPropertyTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving AddressXAddressPropertyType entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private AddressXAddressPropertyType addressXAddressPropertyType;

    public AddressXAddressPropertyType getAddressXAddressPropertyType() {
        return this.addressXAddressPropertyType;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.addressXAddressPropertyType = this.example;
        }
        else {
            this.addressXAddressPropertyType = findById(getId());
        }
    }

    public AddressXAddressPropertyType findById(Long id) {

        return this.entityManager.find(AddressXAddressPropertyType.class, id);
    }

    /*
     * Support updating and deleting AddressXAddressPropertyType entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.addressXAddressPropertyType);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.addressXAddressPropertyType);
                return "view?faces-redirect=true&id=" + this.addressXAddressPropertyType.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AddressXAddressPropertyType entities with pagination
     */

    private int page;
    private long count;
    private List<AddressXAddressPropertyType> pageItems;

    private AddressXAddressPropertyType example = new AddressXAddressPropertyType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AddressXAddressPropertyType getExample() {
        return this.example;
    }

    public void setExample(AddressXAddressPropertyType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<AddressXAddressPropertyType> root = countCriteria.from(AddressXAddressPropertyType.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<AddressXAddressPropertyType> criteria = builder.createQuery(AddressXAddressPropertyType.class);
        root = criteria.from(AddressXAddressPropertyType.class);
        TypedQuery<AddressXAddressPropertyType> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<AddressXAddressPropertyType> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        AddressPropertyType type = this.example.getType();
        if (type != null) {
            predicatesList.add(builder.equal(root.get("type"), type));
        }
        String value = this.example.getValue();
        if (value != null && !"".equals(value)) {
            predicatesList.add(builder.like(root.<String> get("value"), '%' + value + '%'));
        }
        Address address = this.example.getAddress();
        if (address != null) {
            predicatesList.add(builder.equal(root.get("address"), address));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<AddressXAddressPropertyType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AddressXAddressPropertyType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<AddressXAddressPropertyType> getAll() {

        CriteriaQuery<AddressXAddressPropertyType> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                AddressXAddressPropertyType.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(AddressXAddressPropertyType.class)))
                .getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final AddressXAddressPropertyTypeBean ejbProxy = this.sessionContext
                .getBusinessObject(AddressXAddressPropertyTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AddressXAddressPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private AddressXAddressPropertyType add = new AddressXAddressPropertyType();

    public AddressXAddressPropertyType getAdd() {
        return this.add;
    }

    public AddressXAddressPropertyType getAdded() {
        AddressXAddressPropertyType added = this.add;
        this.add = new AddressXAddressPropertyType();
        return added;
    }
}