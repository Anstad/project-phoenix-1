package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.CustomerCost;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.ItemClassification;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.VendorXVendorPropertyType;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for Item entities.
 * <p>
 * This class provides CRUD functionality for all Item entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ItemBean extends AbstractControllerBean<Item> implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

    private Long id;
    private int page;
    private long count;
    private List<Item> pageItems;
    private Item currentInstance;
    private Manufacturer currentManufacturer;
    private Sku currentSku;
    private CustomerCost currentCustomerCost;
    private ItemClassification currentItemClassification;
    private ItemXItemPropertyType currentProperty;
    private Item currentItem;
    private Item example;
    private Item add;

    //   @Resource
    //   private SessionContext sessionContext;

    @Inject
    private ItemBp itemBp;

    @Inject
    private Conversation conversation;

    @Inject
    private OriginalSkuSearchBean skuSearchBean;

    @Inject
    private ValidationBean validationBean;

    @PostConstruct
    public void init() {
        currentInstance = new Item();
        //currentInstance.setManufacturer(new Manufacturer());
        //currentInstance.setItemCategory(new ItemCategory());
        //currentManufacturer = new Manufacturer();
        currentSku = new Sku();
        currentCustomerCost = new CustomerCost();
        currentItemClassification = new ItemClassification();
        currentProperty = new ItemXItemPropertyType();
        currentItem = new Item();

        example = new Item();
        add = new Item();
    }

    private void eagerLoadFull(Item item) {
        itemBp.eagerLoad(item);
        itemBp.eagerLoadCustomerCosts(item.getCustomerCosts());
    }

    public String create() {
        this.conversation.begin();
        return "itemAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (skuSearchBean.getSelection() != null) {
            this.currentInstance = itemBp.findById(skuSearchBean.getSelection().getItem().getId(), Item.class);
            eagerLoadFull(this.currentInstance);

            skuSearchBean.setSelection(null);
        }
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.currentInstance.getId() == null) {
            init();
        }
        else {
            this.currentInstance = itemBp.findById(this.currentInstance.getId(), Item.class);
        }
        if (this.currentInstance.getId() != null) {
            itemBp.eagerLoad(this.currentInstance);
        }

    }

    /*
     * Support updating and deleting Item entities
     */

    public String update() {

        itemBp.setItemStatus(this.currentInstance);

        try {
            itemBp.calculateStreetPrice(this.currentInstance);
            itemBp.update(this.currentInstance);
            this.conversation.end();
            return "itemEdit?faces-redirect=true";
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    @Deprecated
    public String delete() {
        this.conversation.end();
        try {
            itemBp.delete(this.currentInstance.getId(), Item.class);
            return "itemEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return this.currentInstance;
    }

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @SuppressWarnings("static-method")
    public int getPageSize() {
        return 10;
    }

    public Item getExample() {
        return this.example;
    }

    public void setExample(Item example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = itemBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = itemBp.resultQuantity(this.example);
    }

    public List<Item> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    public List<Item> getAll() {

        return itemBp.findAll(Item.class, 0, 0);
    }

    public List<Item> searchByExample(Item search) {
        return itemBp.searchByExample(search, 0, 0);
    }

    /**
     * Once the current instance of this bean has a business key set, the rest of the fields can be 
     * populated using this method.
     */
    public void retrieveByBusinessKey() {
        Item search = new Item();
        search.setName(this.currentInstance.getName());
        this.currentInstance = null;
        this.currentInstance = itemBp.searchByExample(search, 0, 0).get(0);
        itemBp.eagerLoad(this.currentInstance);
    }

    public List<Item> searchByString(String s) {
        Item search = new Item();
        search.setName(s);
        return searchByExample(search);
    }

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return itemBp.findById(Long.valueOf(value), Item.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Item) value).getId());
            }
        };
    }

    public void validateUniqueProperties(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getPropertiesReadOnly()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple properties with the same type");
            message.setDetail("You can't have multiple properties with the same type.");
            context.addMessage(component.getClientId(), message);
        }
    }

    public void validateUniqueSkus(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getSkus()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple SKUs with the same type");
            message.setDetail("You can't have multiple SKUs with the same type.");
            context.addMessage(component.getClientId(), message);
        }
    }

    public Item getAdd() {
        return this.add;
    }

    public Item getAdded() {
        Item added = this.add;
        this.add = new Item();
        return added;
    }

    public Sku getCurrentSku() {
        return currentSku;
    }

    public void setCurrentSku(Sku currentSku) {
        this.currentSku = currentSku;
    }

    public void createSku() {
        this.currentSku = new Sku();
    }

    public void addSku() {
        this.currentInstance.getSkus().add(this.currentSku);
        this.currentSku.setItem(currentInstance);
        this.currentSku = new Sku();
    }

    public void removeSku(Sku sku) {
        this.currentInstance.getSkus().remove(sku);
        this.currentSku.setItem(null);
    }

    public void createManufacturer() {
        this.currentManufacturer = new Manufacturer();
    }

    public void addManufacturer(Manufacturer manufacturerToAdd) {
        this.currentInstance.setManufacturer(manufacturerToAdd);
    }

    public Manufacturer getCurrentManufacturer() {
        return this.currentManufacturer;
    }

    public void setCurrentManufacturer(Manufacturer currentManufacturer) {
        this.currentManufacturer = currentManufacturer;
    }

    public void removeManufacturer(Manufacturer manufacturer) {
        this.currentInstance.setManufacturer(null);
    }

    public void addItemCategory(ItemCategory itemCategoryToAdd) {
        this.currentInstance.setItemCategory(itemCategoryToAdd);
    }

    public CustomerCost getCurrentCustomerCost() {
        return currentCustomerCost;
    }

    public void setCurrentCustomerCost(CustomerCost currentCustomerCost) {
        this.currentCustomerCost = currentCustomerCost;
    }

    public ItemClassification getCurrentItemClassification() {
        return currentItemClassification;
    }

    public void setCurrentItemClassification(ItemClassification currentItemClassification) {
        this.currentItemClassification = currentItemClassification;
    }

    public ItemXItemPropertyType getCurrentProperty() {
        return currentProperty;
    }

    public void setCurrentProperty(ItemXItemPropertyType property) {
        this.currentProperty = property;
    }

    public void createProperty() {
        this.currentProperty = new ItemXItemPropertyType();
    }

    public void addProperty() {
        this.currentInstance = itemBp.addOrModifyItemProperty(this.currentInstance, this.currentProperty);
        this.currentProperty = new ItemXItemPropertyType();
    }

    public void removeProperty(ItemXItemPropertyType property) {
        this.currentInstance = itemBp.removeProperty(this.currentInstance, property);
        //this.currentSku.setItem(null);
    }

    /**
     * Saves the current property onto the current Item instance.
     */
    public void saveProperty() {
        this.currentInstance = itemBp.addOrModifyItemProperty(this.currentInstance, this.currentProperty);
        clearModalPanel(VendorXVendorPropertyType.class.getName());
    }

    public void addItem(Item item) {
        this.currentInstance.setReplacement(item);
        //this.currentItem = new Item();
    }

    /**
     * Sets the value of the item's replacement field to null. Workaround for the fact that we can't call 
     * #{item.setReplacement(null)} in JSF, because you can't use null as a parameter in JSF.
     * 
     * @param toClear
     */
    public void clearReplacement(Item toClear) {
        toClear.setReplacement(null);
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(Item currentItem) {
        this.currentItem = currentItem;
    }

    public void clearSelectedItem() {
        this.currentItem = new Item();
    }

    public Item eagerLoad(Item item) {
        if (item == null) {
            return new Item();
        }
        item = this.itemBp.findById(item.getId(), Item.class);
        this.eagerLoadFull(item);
        return item;
    }

    public String discontinue() {

        try {
            itemBp.discontinue(this.currentInstance);
            return this.update();
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

}