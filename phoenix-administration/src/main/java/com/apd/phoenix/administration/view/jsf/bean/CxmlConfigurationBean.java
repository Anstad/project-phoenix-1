/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.persistence.jpa.CxmlConfigurationDao;
import com.apd.phoenix.web.AbstractControllerBean;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
@Named
@Stateful
@ConversationScoped
public class CxmlConfigurationBean extends AbstractControllerBean<CxmlConfiguration> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CxmlConfiguration currentInstance;

    public CxmlConfiguration getCxmlConfiguration() {
        return currentInstance;
    }

    @Inject
    private CxmlConfigurationDao dao;

    @Inject
    private Conversation conversation;

    @Inject
    private CxmlConfigurationSearchBean searchBean;

    private CxmlCredential currentFromCredential = new CxmlCredential();
    private CxmlCredential currentToCredential = new CxmlCredential();
    private CxmlCredential currentSenderCredential = new CxmlCredential();

    public void createFromCredential() {
        this.currentFromCredential = new CxmlCredential();
    }

    public void createToCredential() {
        this.currentToCredential = new CxmlCredential();
    }

    public void createSenderCredential() {
        this.currentSenderCredential = new CxmlCredential();
    }

    public void saveCurrentFromCredential() {
        this.currentInstance.getFromCredentials().add(currentFromCredential);
    }

    public void saveCurrentSenderCredential() {
        this.currentInstance.getSenderCredentials().add(currentSenderCredential);
    }

    public void saveCurrentToCredential() {
        this.currentInstance.getToCredentials().add(currentToCredential);
    }

    public void removeCurrentFromCredential(CxmlCredential credential) {
        this.currentInstance.getFromCredentials().remove(credential);
    }

    public void removeCurrentSenderCredential(CxmlCredential credential) {
        this.currentInstance.getSenderCredentials().remove(credential);
    }

    public void removeCurrentToCredential(CxmlCredential credential) {
        this.currentInstance.getToCredentials().remove(credential);
    }

    public CxmlCredential getCurrentFromCredential() {
        return currentFromCredential;
    }

    public void setCurrentFromCredential(CxmlCredential currentFromCredential) {
        this.currentFromCredential = currentFromCredential;
    }

    public CxmlCredential getCurrentToCredential() {
        return currentToCredential;
    }

    public void setCurrentToCredential(CxmlCredential currentToCredential) {
        this.currentToCredential = currentToCredential;
    }

    public CxmlCredential getCurrentSenderCredential() {
        return currentSenderCredential;
    }

    public void setCurrentSenderCredential(CxmlCredential currentSenderCredential) {
        this.currentSenderCredential = currentSenderCredential;
    }

    private CxmlConfiguration.DeploymentMode[] deploymentModes;

    public CxmlConfiguration.DeploymentMode[] getDeploymentModes() {
        return CxmlConfiguration.DeploymentMode.values();
    }

    public String create() {
        this.conversation.begin();
        return "cxmlConfigurationAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, CxmlConfiguration.class);
        }

    }

    /*
     * Support updating and deleting CostCenter entities
     */
    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "cxmlConfigurationAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "cxmlConfigurationEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), CxmlConfiguration.class);
            return "cxmlConfigurationEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CostCenter entities with pagination
     */
    private int page;
    private long count;
    private List<CxmlConfiguration> pageItems;

    private CxmlConfiguration example = new CxmlConfiguration();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CxmlConfiguration getExample() {
        return this.example;
    }

    public void setExample(CxmlConfiguration example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<CxmlConfiguration> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    public List<CxmlConfiguration> getAll() {

        return dao.findAll(CxmlConfiguration.class, 0, 0);
    }

    public List<CxmlConfiguration> searchByExample(CxmlConfiguration search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<CxmlConfiguration> searchByString(String s) {
        CxmlConfiguration search = new CxmlConfiguration();
        search.setName(s);
        return searchByExample(search);
    }

    private boolean validToUpdate() {
        CxmlConfiguration searchCxmlConfiguration = new CxmlConfiguration();
        searchCxmlConfiguration.setName(this.currentInstance.getName());
        List<CxmlConfiguration> searchList = this.dao.searchByExactExample(searchCxmlConfiguration, 0, 0);
        if (!searchList.isEmpty() && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), CxmlConfiguration.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CxmlConfiguration) value).getId());
            }
        };
    }
}
