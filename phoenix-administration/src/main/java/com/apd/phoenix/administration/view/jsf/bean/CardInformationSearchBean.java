package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.CardInformationDao;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class CardInformationSearchBean extends AbstractSearchBean<CardInformation> implements Serializable {

    /**
     * Generated serial ID
     */
    private static final long serialVersionUID = -6379624268920244721L;

    @Inject
    CardInformationDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = new String[0];
        return toReturn;
    }

    @Override
    protected List<CardInformation> search() {
        CardInformation search = new CardInformation();
        return dao.searchByExample(search, 0, 0);
    }

}
