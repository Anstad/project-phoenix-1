package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.PermissionXUser;

/**
 * Backing bean for PermissionXUser entities.
 * <p>
 * This class provides CRUD functionality for all PermissionXUser entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class PermissionXUserBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving PermissionXUser entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private PermissionXUser permissionXUser;

    public PermissionXUser getPermissionXUser() {
        return this.permissionXUser;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.permissionXUser = this.example;
        }
        else {
            this.permissionXUser = findById(getId());
        }
    }

    public PermissionXUser findById(Long id) {

        return this.entityManager.find(PermissionXUser.class, id);
    }

    /*
     * Support updating and deleting PermissionXUser entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.permissionXUser);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.permissionXUser);
                return "view?faces-redirect=true&id=" + this.permissionXUser.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching PermissionXUser entities with pagination
     */

    private int page;
    private long count;
    private List<PermissionXUser> pageItems;

    private PermissionXUser example = new PermissionXUser();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public PermissionXUser getExample() {
        return this.example;
    }

    public void setExample(PermissionXUser example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<PermissionXUser> root = countCriteria.from(PermissionXUser.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<PermissionXUser> criteria = builder.createQuery(PermissionXUser.class);
        root = criteria.from(PermissionXUser.class);
        TypedQuery<PermissionXUser> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<PermissionXUser> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<PermissionXUser> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back PermissionXUser entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<PermissionXUser> getAll() {

        CriteriaQuery<PermissionXUser> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                PermissionXUser.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(PermissionXUser.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final PermissionXUserBean ejbProxy = this.sessionContext.getBusinessObject(PermissionXUserBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PermissionXUser) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private PermissionXUser add = new PermissionXUser();

    public PermissionXUser getAdd() {
        return this.add;
    }

    public PermissionXUser getAdded() {
        PermissionXUser added = this.add;
        this.add = new PermissionXUser();
        return added;
    }
}