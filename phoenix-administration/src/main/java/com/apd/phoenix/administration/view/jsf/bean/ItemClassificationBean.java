package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.ItemClassification;
import com.apd.phoenix.service.persistence.jpa.ItemClassificationDao;

/**
 * Backing bean for ItemClassification entities.
 * <p>
 * This class provides CRUD functionality for all ItemClassification entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ItemClassificationBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemClassification entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemClassification currentInstance;

    public ItemClassification getItemClassification() {
        return this.currentInstance;
    }

    @Inject
    private ItemClassificationDao dao;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, ItemClassification.class);
        }

    }

    /*
     * Support updating and deleting ItemClassification entities
     */

    public String update() {
        this.conversation.end();

        try {
            dao.update(this.currentInstance);

            if (currentInstance.getId() == null) {
                return "search?faces-redirect=true";
            }
            else {
                return "view?faces-redirect=true&id=" + currentInstance.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.id, ItemClassification.class);
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemClassification entities with pagination
     */

    private int page;
    private long count;
    private List<ItemClassification> pageItems;

    private ItemClassification example = new ItemClassification();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemClassification getExample() {
        return this.example;
    }

    public void setExample(ItemClassification example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<ItemClassification> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemClassification entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemClassification> getAll() {

        return dao.findAll(ItemClassification.class, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemClassificationBean ejbProxy = this.sessionContext.getBusinessObject(ItemClassificationBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getItemClassification();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemClassification) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemClassification add = new ItemClassification();

    public ItemClassification getAdd() {
        return this.add;
    }

    public ItemClassification getAdded() {
        ItemClassification added = this.add;
        this.add = new ItemClassification();
        return added;
    }
}