package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.AttachmentType;
import com.apd.phoenix.service.persistence.jpa.AttachmentTypeDao;

/**
 * Backing bean for AttachmentType entities.
 * <p>
 * This class provides CRUD functionality for all AttachmentType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AttachmentTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving AttachmentType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.attachmentType.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Attachment Type to edit");
            message.setDetail("You need to select an Attachment Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private AttachmentType attachmentType;

    public AttachmentType getAttachmentType() {
        return this.attachmentType;
    }

    @Inject
    private Conversation conversation;

    @Inject
    private AttachmentTypeDao attachmentTypeDao;

    public String create() {

        this.conversation.begin();
        return "attachmentTypeAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.attachmentType = this.example;
        }
        else {
            this.attachmentType = findById(getId());
        }
    }

    public AttachmentType findById(Long id) {
        return this.attachmentTypeDao.findById(id, AttachmentType.class);
    }

    /*
     * Support updating and deleting AttachmentType entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.attachmentType.getId() == null) {
                this.attachmentTypeDao.create(this.attachmentType);
                return "attachmentTypeAdd?faces-redirect=true";
            }
            else {
                this.attachmentTypeDao.update(this.attachmentType);
                return "attachmentTypeEdit?faces-redirect=true&id=" + this.attachmentType.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.attachmentTypeDao.delete(this.attachmentType.getId(), AttachmentType.class);
            return "attachmentTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AttachmentType entities with pagination
     */

    private int page;
    private long count;
    private List<AttachmentType> pageItems;

    private AttachmentType example = new AttachmentType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AttachmentType getExample() {
        return this.example;
    }

    public void setExample(AttachmentType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        this.pageItems = this.attachmentTypeDao.findAll(AttachmentType.class, getPage() * getPageSize(), getPageSize());
        this.count = this.attachmentTypeDao.resultQuantity(this.example);
    }

    public List<AttachmentType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AttachmentType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<AttachmentType> getAll() {

        return this.attachmentTypeDao.findAll(AttachmentType.class, 0, 0);
    }

    public List<AttachmentType> search(String s) {
        AttachmentType attachmentType = new AttachmentType();
        attachmentType.setName(s);
        return this.attachmentTypeDao.searchByExample(attachmentType, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final AttachmentTypeBean ejbProxy = this.sessionContext.getBusinessObject(AttachmentTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AttachmentType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private AttachmentType add = new AttachmentType();

    public AttachmentType getAdd() {
        return this.add;
    }

    public AttachmentType getAdded() {
        AttachmentType added = this.add;
        this.add = new AttachmentType();
        return added;
    }
}