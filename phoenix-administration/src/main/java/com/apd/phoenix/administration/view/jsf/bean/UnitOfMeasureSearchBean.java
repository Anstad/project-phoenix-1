package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class UnitOfMeasureSearchBean extends AbstractSearchBean<UnitOfMeasure> implements Serializable {

    @Inject
    UnitOfMeasureDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<UnitOfMeasure> search() {
        UnitOfMeasure search = new UnitOfMeasure();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
