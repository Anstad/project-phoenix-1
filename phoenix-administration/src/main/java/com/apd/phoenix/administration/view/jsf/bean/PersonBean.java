package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.PersonBp;
import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.AccountAddressPagenationControllerBean;

/**
 * Backing bean for Person entities.
 */

@Named
@Stateful
@ConversationScoped
public class PersonBean extends AbstractControllerBean<Person> implements Serializable {

    private static final long serialVersionUID = -6874667102205086797L;

    private Person person;

    private String firstName;
    private String lastName;
    private String email;

    private PhoneNumber currentNumber;

    private Set<PhoneNumber> numbers = new HashSet<PhoneNumber>();
    @Inject
    private PersonBp bp;

    @Inject
    PhoneNumberBp phoneNumberBp;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Person persistPerson() {
        if (person == null) {
            Person newPerson = new Person();
            setPersonFields(newPerson);
            person = bp.create(newPerson);
        }
        else {
            setPersonFields(person);
            person = bp.update(person);
        }
        for (PhoneNumber number : numbers) {
            number.setPerson(person);
            phoneNumberBp.update(number);
        }
        Person returnPerson = this.person;
        this.clear();
        return returnPerson;
    }

    private void setPersonFields(Person newPerson) {
        newPerson.setFirstName(firstName);
        newPerson.setLastName(lastName);
        newPerson.setEmail(email);
        newPerson.setPhoneNumbers(numbers);
    }

    public void clear() {
        firstName = null;
        lastName = null;
        email = null;
        numbers = new HashSet<>();
        person = null;
    }

    public void setupPerson(Person person) {
        if (person != null) {
            if (person.getId() != null) {
                person = bp.findById(person.getId(), Person.class);
                //Initialise numbers
                for (PhoneNumber number : person.getPhoneNumbers()) {
                    number.toString();
                }
            }
            firstName = person.getFirstName();
            lastName = person.getLastName();
            email = person.getEmail();
            if (person.getPhoneNumbers() != null) {
                this.numbers = person.getPhoneNumbers();
            }
            this.person = person;
        }
    }

    public Set<PhoneNumber> getNumbers() {
        return numbers;
    }

    public void setNumbers(Set<PhoneNumber> numbers) {
        this.numbers = numbers;
    }

    public PhoneNumber getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(PhoneNumber currentNumber) {
        this.currentNumber = currentNumber;
    }

    public String getPhoneNumberReadable() {
        return phoneNumberBp.getReadableNumber(this.currentNumber);
    }

    public void setPhoneNumberReadable(String phoneNumberReadable) {
        this.phoneNumberBp.setReadableNumber(this.currentNumber, phoneNumberReadable);
    }

    public String getPhoneNumberReadableRegex() {
        return this.phoneNumberBp.getReadableNumberRegex();
    }

    public void addPhoneNumber() {
        this.numbers.add(this.currentNumber);
        this.currentNumber = new PhoneNumber();
    }

    public void createPhoneNumber() {
        this.currentNumber = new PhoneNumber();
    }

    @Override
    public Converter getConverter() {
        return null;
    }
}