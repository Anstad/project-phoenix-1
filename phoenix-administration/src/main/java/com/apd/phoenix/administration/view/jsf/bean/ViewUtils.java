package com.apd.phoenix.administration.view.jsf.bean;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EntityComparator;

/**
 * Utilities for working with Java Server Faces views.
 */

public final class ViewUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewUtils.class);

    public static <T> List<T> asList(Collection<T> collection) {

        if (collection == null) {
            return null;
        }

        List<T> toReturn = new ArrayList<T>(collection);

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }

    public static <T> List<T> asUnsortedList(Collection<T> collection) {
        if (collection == null) {
            return null;
        }

        List<T> toReturn = new ArrayList<T>(collection);
        return toReturn;
    }

    private ViewUtils() {

        // Can never be called
    }

    public static boolean isCurrencyType(String rowType) {
        if (/*Cashout Page*/rowType.equals("shipping cost") || /*Credential*/rowType.equals("shipping rate")
                || rowType.equals("handling fee") || rowType.equals("invoice payments")
                || rowType.equals("minimum order amount") || rowType.equals("minimum order amount required")
                || rowType.equals("minimum order fee") || rowType.equals("unscheduled service fee")
                || /*Vendor Catalog Item & Customer Catalog Item*/rowType.equals("AFprice")
                || rowType.equals("APDcost") || rowType.equals("GSAprice") || rowType.equals("last calculated price")
                || rowType.equals("last calculated profit") || rowType.equals("list price")
                || rowType.equals("standardprice") || rowType.equals("current profit")) {
            return true;
        }
        return false;
    }

    public static String toCurrency(String rawValue) {

        if (StringUtils.isBlank(rawValue)) {
            return "";
        }

        double doubleValue = 0.0;

        try {
            doubleValue = Double.parseDouble(rawValue);
        }
        catch (Exception e) {
            LOGGER.debug("Could not parse double.", e);
            return "[NaN: " + rawValue + " ]";
        }

        DecimalFormat formatter = new DecimalFormat("$ #,##0.00");
        formatter.setRoundingMode(RoundingMode.HALF_UP);

        String currencyFormatValue = formatter.format(doubleValue);
        return currencyFormatValue;
    }
}
