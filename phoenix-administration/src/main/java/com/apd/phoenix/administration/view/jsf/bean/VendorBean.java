package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CommunicationMethod;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.VendorPropertyType;
import com.apd.phoenix.service.model.VendorXVendorPropertyType;
import com.apd.phoenix.service.persistence.jpa.CommunicationMethodDao;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for Vendor entities.
 * <p>
 * This class provides CRUD functionality for all Vendor entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped<\tt> for
 * state management, <tt>PersistenceContext<\tt> for persistence,
 * <tt>CriteriaBuilder<\tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class VendorBean extends AbstractControllerBean<Vendor> implements Serializable {

    static {
        uiPanelIdsMap = new HashMap<String, String>();
        uiPanelIdsMap.put(VendorXVendorPropertyType.class.getName(), "propertyModal");
    }

    public static Map<String, String> getUiPanelIdsMap() {
        return uiPanelIdsMap;
    }

    private static final long serialVersionUID = 6271484613091497781L;

    private static final Logger LOGGER = LoggerFactory.getLogger(VendorBean.class);

    @Inject
    Conversation conversation;

    @Inject
    VendorSearchBean searchBean;

    //private String selectedMethod;

    private Vendor currentInstance = new Vendor();

    public Vendor getVendor() {
        return this.currentInstance;
    }

    public void setVendor(Vendor vendor) {
        this.currentInstance = vendor;
    }

    // The property currently being edited.
    private VendorXVendorPropertyType currentProperty = new VendorXVendorPropertyType();

    private Catalog selectedVendorCatalog = new Catalog();
    private Catalog newVendorCatalog = new Catalog();

    private List<VendorPropertyType> availablePropertyTypes = new ArrayList<VendorPropertyType>();

    @Inject
    private VendorBp bp;

    //	public void manageConversation() {
    //		if (LOG.isDebugEnabled()) {
    //			LOG.debug("\n\tStarting management");
    //		}
    //
    //		if (FacesContext.getCurrentInstance().isPostback()) {
    //			if (LOG.isDebugEnabled()) {
    //				LOG.debug("\n\tWas a postback");
    //			}
    //			return;
    //		}
    //		if (conversation.isTransient()) {
    //			if (LOG.isDebugEnabled()) {
    //				LOG.debug("\n\tStarting conversation");
    //			}
    //			conversation.begin();
    //		}
    //	}

    public void add(VendorXVendorPropertyType property) {
        this.getVendor().getProperties().add(property);
    }

    public void retrieve() {

        if (searchBean.getSelection() != null) {
            this.currentInstance = bp.findById(searchBean.getSelection().getId(), Vendor.class);
            eagerLoad();
            searchBean.setSelection(null);
        }

        //	if (communicationSearchBean.getSelection() != null) {
        //       	this.currentInstance.setCommunicationMethod(communicationSearchBean.getSelection());
        //       	communicationSearchBean.setSelection(null);
        //   }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
            if (this.currentInstance.getId() == null) {
                //setting defaults
                this.currentInstance.setCommunicationDestination("ordermgmt@americanproduct.com");
                this.currentInstance.setCommunicationMessageType(MessageType.EMAIL);
            }
        }
    }

    private void eagerLoad() {
        this.currentInstance.getAddresses().size();
        this.currentInstance.getCatalogs().size();
        //this.currentInstance.getCommunicationMethods().size();
        //        this.currentInstance.getContacts().size();
        this.currentInstance.getNumbers().size();
        this.currentInstance.getProperties().size();
    }

    public void setPerson(Person toAdd) {
        this.currentInstance.setContact(toAdd);
    }

    public void removePerson(Person toRemove) {
        if (this.currentInstance.getContact().equals(toRemove)) {
            this.currentInstance.setContact(null);
        }
    }

    @Inject
    private CommunicationMethodDao communicationMethodDao;

    public void setCommunicationMethod(CommunicationMethod toAdd) {
        toAdd = communicationMethodDao.eagerLoad(toAdd);
        //this.currentInstance.getCommunicationMethods().add(toAdd);
        toAdd.getVendors().add(this.currentInstance);
    }

    public void removeCommunicationMethod(CommunicationMethod toRemove) {
        toRemove = communicationMethodDao.eagerLoad(toRemove);
        //this.currentInstance.getCommunicationMethods().remove(toRemove);
        toRemove.getVendors().remove(this.currentInstance);
    }

    /* Vendor Properties */

    /**
     * @return the currentProperty
     */
    public VendorXVendorPropertyType getCurrentProperty() {
        return this.currentProperty;
    }

    /**
     * @param currentProperty
     *            the currentProperty to set
     */
    public void setCurrentProperty(VendorXVendorPropertyType currentProperty) {
        this.currentProperty = currentProperty;
    }

    /**
     * This method sets the current vendor property to a new VendorXVendorProperty instance and should be
     * bound to action components that start the creation of a new vendor property on the current Vendor instance.
     */
    public void createProperty() {
        this.currentProperty = new VendorXVendorPropertyType();
    }

    /**
     * Removes the provided property from the set of properties on the current Vendor instance.
     * 
     * @param toRemove
     */
    public void removeProperty(VendorXVendorPropertyType toRemove) {
        this.currentInstance.getProperties().remove(toRemove);
        //		if (toRemove.getId() != null) {
        //			this.propertiesToRemove.add(toRemove);
        //		}
    }

    /**
     * Saves the current property onto the current Vendor instance.
     */
    public void saveProperty() {
        this.currentInstance.getProperties().add(this.currentProperty);
        clearModalPanel(VendorXVendorPropertyType.class.getName());
    }

    /*
     * Catalog methods
     */
    /**
     * When the user elects to add a catalog they will be prompted with a modal
     * window. This window will ask the user to enter the catalog name
     * (required), activation date (required), and the expiration date. After
     * submitting the catalog (before saving all vendor changes) the new catalog
     * will display in the catalog grid.
     * 
     * This grid will allow a user to do the following (in the Actions column)
     * 
     * -Deactivate allows a user to deactivate a catalog. The date the catalog
     * will be deactivated is the current date + 1
     * 
     * -Edit (edit the properties of the catalog. This includes making changes
     * to the catalog properties or merging item changes (add, edit, delete)
     * 
     * -Copy/Clone allows a user to create a copy of the current catalog, but
     * allows them to upload a new CSV. This catalog will need to copy the
     * relationships it has with customer catalogs
     * 
     * 
     */

    /**
     * This method is called when the user clicks the "Add Catalog" button
     */
    public void createNewCatalog() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("\n\tcreateNewCatalog");
        }
        newVendorCatalog = new Catalog();
    }

    /**
     * This method is called when a user wants to replace an existing catalog.
     * The user will select the catalog to replace and select the "replace" link
     * in the table. This will create a new catalog for the user to upload a new
     * CSV file.
     * 
     */
    public void replaceCatalog() {
        Catalog replacement = new Catalog();
        replacement = selectedVendorCatalog;

        //if there is an expiration date, then it needs to tick over 1 day
        //this also needs to make sure that the expiration date has not passed
        if (selectedVendorCatalog.getExpirationDate() != null) {
            Date activationDate = new Date(selectedVendorCatalog.getExpirationDate().getTime()
                    + TimeUnit.DAYS.toMillis(1));
            replacement.setStartDate(activationDate);
        }
    }

    /**
     * This method is called once the user has added a catalog from the modal
     * window
     */
    public void addCatalog() {
        LOGGER.info("\n\taddCatalog");
    }

    /**
     * This method is called when a user elects to deactivate a catalog from the
     * grid
     */
    public void deactivateCatalog() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("\n\tdeactivateCatalog");
        }
    }

    /*
     * Save the vendor
     */

    //	public String save() {
    //		conversation.end();
    //		if (LOG.isDebugEnabled()) {
    //			LOG.debug("\n\tsave(): The vendor is being saved\n");
    //			LOG.debug("\n\t save(): 3. The selected vendor has "
    //					+ currentInstance.getVendorXVendorPropertyTypes().size());
    //		}
    //		if (currentInstance.getId() == null || currentInstance.getId() == 0) {
    //			bp.create(currentInstance);
    //			return "vendorAdd?faces-redirect=true";
    //		} else {
    //			bp.update(currentInstance);
    //			return "vendorEdit?faces-redirect=true";
    //		}
    //	}

    public String delete() {
        this.conversation.end();

        try {
            bp.delete(this.currentInstance.getId(), Vendor.class);
            return "vendorEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support updating and deleting Credential entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                this.currentInstance = bp.create(this.currentInstance);
                return "vendorAdd?faces-redirect=true";
            }
            else {
                //	            	  if (this.propertiesToRemove != null) {
                //	            		  for (VendorXVendorPropertyType property : this.propertiesToRemove) {
                //	            			  vendorPropertyDao.delete(property.getId(), VendorXVendorPropertyType.class);
                //	            		  }
                //	            	  }
                this.currentInstance = bp.update(this.currentInstance);
                eagerLoad();
                return "vendorEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    /*
     * Support searching Credential entities with pagination
     */

    private int page;
    private long count;
    private List<Vendor> pageItems;

    private Vendor example = new Vendor();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Vendor getExample() {
        return this.example;
    }

    public void setExample(Vendor example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = bp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = bp.resultQuantity(this.example);
    }

    public List<Vendor> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Credential entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Vendor> getAll() {

        return bp.findAll(Vendor.class, 0, 0);
    }

    public List<Vendor> searchByExample(Vendor search) {
        return bp.searchByExample(search, 0, 0);
    }

    /**
     * Once the current instance of this bean has a business key set, the rest of the fields can be 
     * populated using this method.
     */
    public void retrieveByBusinessKey() {
        Vendor search = new Vendor();
        search.setName(this.currentInstance.getName());
        this.currentInstance = null;
        this.currentInstance = bp.searchByExample(search, 0, 0).get(0);
        eagerLoad();
    }

    //	   /**
    //	    * Once the current instance of this bean has a business key set, the rest of the fields can be 
    //	    * populated using this method.
    //	    */
    //	   public void copyByBusinessKey() {
    //		   Credential search = new Credential();
    //		   search.setName(this.currentInstance.getName());
    //		   this.currentInstance = null;
    //		   this.currentInstance = bp.cloneEntity(bp.searchByExample(search, 0, 0).get(0));
    //		   eagerLoad();
    //	   }

    public List<Vendor> searchByString(String s) {
        Vendor search = new Vendor();
        search.setName(s);
        return searchByExample(search);
    }

    //	   public List<Account> searchAccountByString(String s) {
    //		   Account search = new Account();
    //		   search.setName(s);
    //		   return accountBean.searchByExample(search);
    //	   }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return bp.findById(Long.valueOf(value), Vendor.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Vendor) value).getId());
            }
        };
    }

    /*
     * @Observes methods
     */
    //    @SuppressWarnings("unused")
    //    private void vendorAddressEventListener(@Observes VendorAddressEvent event) {
    //        if (LOG.isDebugEnabled()) {
    //            LOG.debug("\n\tAn event was received and will be added to the vendor");
    //        }
    //    }
    //
    //    @SuppressWarnings("unused")
    //    private void vendorPhoneNumberEventListener(@Observes VendorPhoneNumberEvent event) {
    //        currentInstance.getNumbers().add(event.getNewPhoneNumber());
    //    }

    //	public Converter getVendorPropertyTypeConverter() {
    //
    //		return new Converter() {
    //
    //			/*
    //			 * (non-Javadoc)
    //			 * 
    //			 * @see
    //			 * javax.faces.convert.Converter#getAsString(javax.faces.context
    //			 * .FacesContext , javax.faces.component.UIComponent,
    //			 * java.lang.Object)
    //			 */
    //			@Override
    //			public String getAsString(FacesContext arg0, UIComponent arg1,
    //					Object vendorPropertyType) {
    //				return String.valueOf(vendorPropertyType);
    //			}
    //
    //			/*
    //			 * (non-Javadoc)
    //			 * 
    //			 * @see
    //			 * javax.faces.convert.Converter#getAsObject(javax.faces.context
    //			 * .FacesContext , javax.faces.component.UIComponent,
    //			 * java.lang.String)
    //			 */
    //			@Override
    //			public Object getAsObject(FacesContext arg0, UIComponent arg1,
    //					String propertyName) {
    //				return lookupType(propertyName);
    //			}
    //		};
    //	}

    //	public VendorPropertyType lookupType(String name) {
    //		Iterator<VendorPropertyType> types = availablePropertyTypes.iterator();
    //		while (types.hasNext()) {
    //			VendorPropertyType lookupType = types.next();
    //			if (StringUtils.equalsIgnoreCase(name, lookupType.getName())) {
    //				return lookupType;
    //			}
    //		}
    //		return new VendorPropertyType();
    //	}

    private List<VendorPropertyType> buildAvailablePropertiesTypeList() {
        if (availablePropertyTypes.size() < 1) {
            availablePropertyTypes = bp.retrieveAllPropertyTypes();
        }
        return availablePropertyTypes;
    }

    /**
     * @return the availablePropertyTypes
     */
    public List<VendorPropertyType> getAvailablePropertyTypes() {
        buildAvailablePropertiesTypeList();
        return availablePropertyTypes;
    }

    /**
     * @param availablePropertyTypes
     *            the availablePropertyTypes to set
     */
    public void setAvailablePropertyTypes(List<VendorPropertyType> availablePropertyTypes) {
        this.availablePropertyTypes = availablePropertyTypes;
    }

    /**
     * @return the currentInstance
     */
    public Vendor getCurrentVendor() {
        return currentInstance;
    }

    /**
     * @param currentInstance
     *            the currentInstance to set
     */
    public void setCurrentVendor(Vendor currentInstance) {
        this.currentInstance = currentInstance;
    }

    /**
     * @return the selectedVendorCatalog
     */
    public Catalog getSelectedVendorCatalog() {
        return selectedVendorCatalog;
    }

    /**
     * @param selectedVendorCatalog
     *            the selectedVendorCatalog to set
     */
    public void setSelectedVendorCatalog(Catalog selectedVendorCatalog) {
        this.selectedVendorCatalog = selectedVendorCatalog;
    }

    /**
     * @return the newVendorCatalog
     */
    public Catalog getNewVendorCatalog() {
        return newVendorCatalog;
    }

    /**
     * @param newVendorCatalog
     *            the newVendorCatalog to set
     */
    public void setNewVendorCatalog(Catalog newVendorCatalog) {
        this.newVendorCatalog = newVendorCatalog;
    }

    private PhoneNumber currentNumber = new PhoneNumber();

    @Inject
    private PhoneNumberBp phoneNumberBp;

    public String getPhoneNumberReadable() {
        return phoneNumberBp.getReadableNumber(this.currentNumber);
    }

    public void setPhoneNumberReadable(String phoneNumberReadable) {
        this.phoneNumberBp.setReadableNumber(this.currentNumber, phoneNumberReadable);
    }

    public String getPhoneNumberReadableRegex() {
        return this.phoneNumberBp.getReadableNumberRegex();
    }

    public void removePhoneNumber(PhoneNumber toRemove) {
        this.currentInstance.getNumbers().remove(toRemove);
    }

    public void addPhoneNumber() {
        this.currentInstance.getNumbers().add(this.currentNumber);
        this.currentNumber.setVendor(this.currentInstance);
        this.currentNumber = new PhoneNumber();
    }

    public void createPhoneNumber() {
        this.currentNumber = new PhoneNumber();
    }

    public PhoneNumber getCurrentNumber() {
        return currentNumber;
    }

    public void setCurrentNumber(PhoneNumber currentNumber) {
        this.currentNumber = currentNumber;
    }

    @Inject
    private ValidationBean validationBean;

    public void validateUniqueProperties(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getProperties()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple properties with the same type");
            message.setDetail("You can't have multiple properties with the same type.");
            context.addMessage(component.getClientId(), message);
        }
    }

    private boolean validToUpdate() {
        Vendor searchVendor = new Vendor();
        searchVendor.setName(this.currentInstance.getName());
        List<Vendor> searchList = this.bp.searchByExactExample(searchVendor, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A vendor already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    public SelectItem[] getCommunicationMessageType() {
        SelectItem[] items = new SelectItem[MessageType.values().length];
        int i = 0;
        for (MessageType g : MessageType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

}