package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.persistence.jpa.ManufacturerDao;

/**
 * Backing bean for Manufacturer entities.
 * <p>
 * This class provides CRUD functionality for all Manufacturer entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class ManufacturerBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(AccountBean.class);

    /*
     * Support creating and retrieving Manufacturer entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Manufacturer to edit");
            message.setDetail("You need to select an Manufacturer to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Manufacturer currentInstance;

    public Manufacturer getManufacturer() {
        return this.currentInstance;
    }

    @Inject
    private ManufacturerDao dao;

    @Inject
    private ManufacturerSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "manufacturerAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, Manufacturer.class);
        }

    }

    /*
     * Support updating and deleting Manufacturer entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "manufacturerAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "manufacturerEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), Manufacturer.class);
            return "manufacturerEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Manufacturer entities with pagination
     */

    private int page;
    private long count;
    private List<Manufacturer> pageItems;

    private Manufacturer example = new Manufacturer();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Manufacturer getExample() {
        return this.example;
    }

    public void setExample(Manufacturer example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<Manufacturer> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Manufacturer entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Manufacturer> getAll() {

        return dao.findAll(Manufacturer.class, 0, 0);
    }

    public List<Manufacturer> searchByExample(Manufacturer search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<Manufacturer> searchByString(String s) {
        Manufacturer search = new Manufacturer();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), Manufacturer.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Manufacturer) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Manufacturer add = new Manufacturer();

    public Manufacturer getAdd() {
        return this.add;
    }

    public Manufacturer getAdded() {
        Manufacturer added = this.add;
        this.add = new Manufacturer();
        return added;
    }

    private boolean validToUpdate() {
        Manufacturer searchManufacturer = new Manufacturer();
        searchManufacturer.setName(this.currentInstance.getName());
        List<Manufacturer> searchList = this.dao.searchByExactExample(searchManufacturer, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A manufacturer already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}