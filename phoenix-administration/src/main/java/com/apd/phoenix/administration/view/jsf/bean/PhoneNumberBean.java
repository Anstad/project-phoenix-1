package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PhoneNumberType;

/**
 * Backing bean for PhoneNumber entities.
 * <p>
 * This class provides CRUD functionality for all PhoneNumber entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class PhoneNumberBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving PhoneNumber entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private PhoneNumber phoneNumber;

    public PhoneNumber getPhoneNumber() {
        return this.phoneNumber;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.phoneNumber = this.example;
        }
        else {
            this.phoneNumber = findById(getId());
        }
    }

    public PhoneNumber findById(Long id) {

        return this.entityManager.find(PhoneNumber.class, id);
    }

    /*
     * Support updating and deleting PhoneNumber entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.phoneNumber);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.phoneNumber);
                return "view?faces-redirect=true&id=" + this.phoneNumber.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching PhoneNumber entities with pagination
     */

    private int page;
    private long count;
    private List<PhoneNumber> pageItems;

    private PhoneNumber example = new PhoneNumber();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public PhoneNumber getExample() {
        return this.example;
    }

    public void setExample(PhoneNumber example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<PhoneNumber> root = countCriteria.from(PhoneNumber.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<PhoneNumber> criteria = builder.createQuery(PhoneNumber.class);
        root = criteria.from(PhoneNumber.class);
        TypedQuery<PhoneNumber> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<PhoneNumber> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        String areaCode = this.example.getAreaCode();
        if (areaCode != null && !"".equals(areaCode)) {
            predicatesList.add(builder.like(root.<String> get("areaCode"), '%' + areaCode + '%'));
        }
        String countryCode = this.example.getCountryCode();
        if (countryCode != null && !"".equals(countryCode)) {
            predicatesList.add(builder.like(root.<String> get("countryCode"), '%' + countryCode + '%'));
        }
        String exchange = this.example.getExchange();
        if (exchange != null && !"".equals(exchange)) {
            predicatesList.add(builder.like(root.<String> get("exchange"), '%' + exchange + '%'));
        }
        String lineNumber = this.example.getLineNumber();
        if (lineNumber != null && !"".equals(lineNumber)) {
            predicatesList.add(builder.like(root.<String> get("lineNumber"), '%' + lineNumber + '%'));
        }
        PhoneNumberType type = this.example.getType();
        if (type != null) {
            predicatesList.add(builder.equal(root.get("type"), type));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<PhoneNumber> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back PhoneNumber entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<PhoneNumber> getAll() {

        CriteriaQuery<PhoneNumber> criteria = this.entityManager.getCriteriaBuilder().createQuery(PhoneNumber.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(PhoneNumber.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final PhoneNumberBean ejbProxy = this.sessionContext.getBusinessObject(PhoneNumberBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PhoneNumber) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private PhoneNumber add = new PhoneNumber();

    public PhoneNumber getAdd() {
        return this.add;
    }

    public PhoneNumber getAdded() {
        PhoneNumber added = this.add;
        this.add = new PhoneNumber();
        return added;
    }
}