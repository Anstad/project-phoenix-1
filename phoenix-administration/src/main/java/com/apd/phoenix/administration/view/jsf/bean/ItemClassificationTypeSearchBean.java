package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ItemClassificationTypeDao;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ItemClassificationTypeSearchBean extends AbstractSearchBean<ItemClassificationType> implements
        Serializable {

    @Inject
    ItemClassificationTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<ItemClassificationType> search() {
        ItemClassificationType search = new ItemClassificationType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
