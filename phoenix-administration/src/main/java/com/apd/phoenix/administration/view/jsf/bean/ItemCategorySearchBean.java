package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ItemCategoryDao;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ItemCategorySearchBean extends AbstractSearchBean<ItemCategory> implements Serializable {

    private static final long serialVersionUID = -6090453166259506862L;
    @Inject
    ItemCategoryDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<ItemCategory> search() {
        ItemCategory search = new ItemCategory();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
