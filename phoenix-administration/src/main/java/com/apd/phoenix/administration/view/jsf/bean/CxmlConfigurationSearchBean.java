/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.persistence.jpa.CxmlConfigurationDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author dnorris
 */
@Named
@ConversationScoped
public class CxmlConfigurationSearchBean extends AbstractSearchBean<CxmlConfiguration> implements Serializable {

    @Inject
    CxmlConfigurationDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<CxmlConfiguration> search() {
        CxmlConfiguration search = new CxmlConfiguration();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(CxmlConfiguration searchResult) {
        Map<String, String> toReturn = new HashMap<>();
        toReturn.put(getColumns()[0], searchResult.getName());
        return toReturn;
    }
}
