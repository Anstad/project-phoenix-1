package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.VendorPropertyTypeDao;
import com.apd.phoenix.service.model.VendorPropertyType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class VendorPropertyTypeSearchBean extends AbstractSearchBean<VendorPropertyType> implements Serializable {

    @Inject
    VendorPropertyTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<VendorPropertyType> search() {
        VendorPropertyType search = new VendorPropertyType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
