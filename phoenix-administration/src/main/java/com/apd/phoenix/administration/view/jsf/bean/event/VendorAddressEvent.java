/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

import com.apd.phoenix.service.model.Address;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */
public class VendorAddressEvent {

    private Address address;

    public VendorAddressEvent(Address address) {
        this.setAddress(address);
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

}
