package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateless
public class ValidationBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationBean.class);

    /**
     * This method checks that each of the entries in the objectCollection set has the same value for the getterName 
     * property. 
     * 
     * @param objectCollection
     * @param getterName
     */
    public boolean repeatedEntry(Collection<Object> objectCollection, String getterName) {
        Set<Object> typeSet = new HashSet<Object>();
        if (objectCollection.size() != 0) {
            //Iterates through the collection
            Object example = objectCollection.toArray()[0];
            Method typeGetter;
            //gets the method specified by the getterName parameter
            try {
                typeGetter = example.getClass().getMethod(getterName, (Class<?>[]) null);
            }
            catch (Exception e) {
                LOGGER.error("An error occured:", e);
                return true;
            }
            //For each object, calls the getter, and adds it into the Set typeSet
            for (Object o : objectCollection) {
                try {
                    typeSet.add(typeGetter.invoke(o, (Object[]) null));
                }
                catch (Exception e) {
                    LOGGER.error("An error occured:", e);
                    return true;
                }
            }
        }
        //If any of the objects are the same, then one of them would not have been added, and the sizes would differ
        if (typeSet.size() != objectCollection.size()) {
            return true;
        }
        return false;
    }

}
