package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Field;
import com.apd.phoenix.service.persistence.jpa.FieldDao;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class FieldSearchBean extends AbstractSearchBean<Field> implements Serializable {

    @Inject
    FieldDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Field> search() {
        Field search = new Field();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
