package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.Department;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.DepartmentDao;
import com.apd.phoenix.web.searchbeans.DepartmentSearchBean;

/**
 * Backing bean for Department entities.
 * <p>
 * This class provides CRUD functionality for all Department entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class DepartmentBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving Department entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Department to edit");
            message.setDetail("You need to select a Department to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Department currentInstance;

    public Department getDepartment() {
        return this.currentInstance;
    }

    @Inject
    private DepartmentDao dao;

    @Inject
    private DepartmentSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "departmentAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, Department.class);
        }

    }

    /*
     * Support updating and deleting Department entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "departmentAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "departmentEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), Department.class);
            return "departmentEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Department entities with pagination
     */

    private int page;
    private long count;
    private List<Department> pageItems;

    private Department example = new Department();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Department getExample() {
        return this.example;
    }

    public void setExample(Department example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<Department> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Department entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Department> getAll() {

        return dao.findAll(Department.class, 0, 0);
    }

    public List<Department> searchByExample(Department search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<Department> searchByString(String s) {
        Department search = new Department();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), Department.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Department) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Department add = new Department();

    public Department getAdd() {
        return this.add;
    }

    public Department getAdded() {
        Department added = this.add;
        this.add = new Department();
        return added;
    }

    private boolean validToUpdate() {
        Department searchDepartment = new Department();
        searchDepartment.setName(this.currentInstance.getName());
        List<Department> searchList = this.dao.searchByExactExample(searchDepartment, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A department already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}