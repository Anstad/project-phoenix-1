package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.ProcessBp;
import com.apd.phoenix.service.model.Process;

/**
 * Backing bean for Process entities.
 * <p>
 * This class provides CRUD functionality for all Process entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@ConversationScoped
public class ProcessBean implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ProcessBp processBp;

    @Inject
    private Conversation conversation;

    @Resource
    private SessionContext sessionContext;

    private Long id;
    private Process process;
    private Process example = new Process();
    private Process add = new Process();

    private int page;
    private long count;

    private List<Process> pageItems;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.process.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Process to edit");
            message.setDetail("You need to select an Process to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /*
     * Support creating and retrieving Process entities
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Process getProcess() {
        return this.process;
    }

    public String create() {
        this.conversation.begin();
        return "processAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.process = this.example;
        }
        else {
            this.process = processBp.findById(getId(), Process.class);
        }
    }

    public Process findById(Long id) {

        return this.processBp.findById(id, Process.class);
    }

    /*
     * Support updating and deleting Process entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.process.getId() == null) {
                this.processBp.create(this.process);
                return "processAdd?faces-redirect=true";
            }
            else {
                this.processBp.update(this.process);
                return "processEdit?faces-redirect=true&id=" + this.process.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.processBp.delete(this.process.getId(), Process.class);
            return "processEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Process entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Process getExample() {
        return this.example;
    }

    public void setExample(Process example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<Process> searchByExample(Process search) {
        return processBp.searchByExample(search, 0, 0);
    }

    public List<Process> searchByString(String s) {
        Process search = new Process();
        search.setName(s);
        return searchByExample(search);
    }

    public void paginate() {
        this.count = processBp.resultQuantity(this.example);
        this.pageItems = processBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    public List<Process> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Process entities (e.g. from
     * inside an HtmlSelectOneMenu)
     */

    public List<Process> getAll() {
        return processBp.findAll(Process.class, 0, 0);
    }

    public Converter getConverter() {

        final ProcessBean ejbProxy = this.sessionContext.getBusinessObject(ProcessBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Process) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */
    public Process getAdd() {
        return this.add;
    }

    public Process getAdded() {
        Process added = this.add;
        this.add = new Process();
        return added;
    }
}