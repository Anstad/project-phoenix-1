package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.PackagingType;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PackagingTypeDao;

/**
 * Backing bean for PackagingType entities.
 * <p>
 * This class provides CRUD functionality for all PackagingType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class PackagingTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving PackagingType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Packaging Type to edit");
            message.setDetail("You need to select an Packaging Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private PackagingType currentInstance;

    public PackagingType getPackagingType() {
        return this.currentInstance;
    }

    @Inject
    private PackagingTypeDao dao;

    @Inject
    private PackagingTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "packagingTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, PackagingType.class);
        }

    }

    /*
     * Support updating and deleting PackagingType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "packagingTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "packagingTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), PackagingType.class);
            return "packagingTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching PackagingType entities with pagination
     */

    private int page;
    private long count;
    private List<PackagingType> pageItems;

    private PackagingType example = new PackagingType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public PackagingType getExample() {
        return this.example;
    }

    public void setExample(PackagingType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<PackagingType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back PackagingType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<PackagingType> getAll() {

        return dao.findAll(PackagingType.class, 0, 0);
    }

    public List<PackagingType> searchByExample(PackagingType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<PackagingType> searchByString(String s) {
        PackagingType search = new PackagingType();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final PackagingTypeBean ejbProxy = this.sessionContext.getBusinessObject(PackagingTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getPackagingType();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PackagingType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private PackagingType add = new PackagingType();

    public PackagingType getAdd() {
        return this.add;
    }

    public PackagingType getAdded() {
        PackagingType added = this.add;
        this.add = new PackagingType();
        return added;
    }

    private boolean validToUpdate() {
        PackagingType searchPackagingType = new PackagingType();
        searchPackagingType.setName(this.currentInstance.getName());
        List<PackagingType> searchList = this.dao.searchByExactExample(searchPackagingType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A PackagingType already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}