package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.VendorXVendorPropertyType;
import com.apd.phoenix.service.persistence.jpa.VendorXVendorPropertyTypeDao;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for VendorXVendorPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all VendorXVendorPropertyType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class VendorXVendorPropertyTypeBean extends AbstractControllerBean<VendorXVendorPropertyType> implements
        Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private VendorXVendorPropertyTypeDao dao;

    /*
     * Support creating and retrieving VendorXVendorPropertyType entities
     */

    //   private Long id;
    //
    //   public Long getId()
    //   {
    //      return this.id;
    //   }
    //
    //   public void setId(Long id)
    //   {
    //      this.id = id;
    //   }

    //   private VendorXVendorPropertyType vendorXVendorPropertyType;
    //
    //   public VendorXVendorPropertyType getVendorXVendorPropertyType()
    //   {
    //      return this.vendorXVendorPropertyType;
    //   }
    //
    //   @Inject
    //   private Conversation conversation;
    //
    //   private EntityManager entityManager;

    //   public String create()
    //   {
    //
    //      this.conversation.begin();
    //      return "create?faces-redirect=true";
    //   }

    //   public void retrieve()
    //   {
    //
    //      if (FacesContext.getCurrentInstance().isPostback())
    //      {
    //         return;
    //      }
    //
    //      if (this.conversation.isTransient())
    //      {
    //         this.conversation.begin();
    //      }
    //
    //      if (this.id == null)
    //      {
    //         this.vendorXVendorPropertyType = this.example;
    //      }
    //      else
    //      {
    //         this.vendorXVendorPropertyType = findById(getId());
    //      }
    //   }

    public VendorXVendorPropertyType findById(Long id) {

        return this.dao.findById(id, VendorXVendorPropertyType.class);
    }

    /*
     * Support updating and deleting VendorXVendorPropertyType entities
     */

    //   public String update()
    //   {
    //      this.conversation.end();
    //
    //      try
    //      {
    //         if (this.id == null)
    //         {
    //            this.entityManager.persist(this.vendorXVendorPropertyType);
    //            return "search?faces-redirect=true";
    //         }
    //         else
    //         {
    //            this.entityManager.merge(this.vendorXVendorPropertyType);
    //            return "view?faces-redirect=true&id=" + this.vendorXVendorPropertyType.getId();
    //         }
    //      }
    //      catch (Exception e)
    //      {
    //         FacesContext.getCurrentInstance().addMessage(null,
    //					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    //         return null;
    //      }
    //   }
    //
    //   public String delete()
    //   {
    //      this.conversation.end();
    //
    //      try
    //      {
    //         this.entityManager.remove(findById(getId()));
    //         this.entityManager.flush();
    //         return "search?faces-redirect=true";
    //      }
    //      catch (Exception e)
    //      {
    //         FacesContext.getCurrentInstance().addMessage(null,
    //					new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
    //         return null;
    //      }
    //   }
    //
    //   /*
    //    * Support searching VendorXVendorPropertyType entities with pagination
    //    */
    //
    //   private int page;
    //   private long count;
    //   private List<VendorXVendorPropertyType> pageItems;
    //
    //   private VendorXVendorPropertyType example = new VendorXVendorPropertyType();
    //
    //   public int getPage()
    //   {
    //      return this.page;
    //   }
    //
    //   public void setPage(int page)
    //   {
    //      this.page = page;
    //   }
    //
    //   public int getPageSize()
    //   {
    //      return 10;
    //   }
    //
    //   public VendorXVendorPropertyType getExample()
    //   {
    //      return this.example;
    //   }
    //
    //   public void setExample(VendorXVendorPropertyType example)
    //   {
    //      this.example = example;
    //   }
    //
    //   public void search()
    //   {
    //      this.page = 0;
    //   }
    //
    //   public void paginate()
    //   {
    //
    //      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
    //
    //      // Populate this.count
    //
    //      CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
    //      Root<VendorXVendorPropertyType> root = countCriteria.from(VendorXVendorPropertyType.class);
    //      countCriteria = countCriteria.select(builder.count(root)).where(
    //            getSearchPredicates(root));
    //      this.count = this.entityManager.createQuery(countCriteria)
    //            .getSingleResult();
    //
    //      // Populate this.pageItems
    //
    //      CriteriaQuery<VendorXVendorPropertyType> criteria = builder.createQuery(VendorXVendorPropertyType.class);
    //      root = criteria.from(VendorXVendorPropertyType.class);
    //      TypedQuery<VendorXVendorPropertyType> query = this.entityManager.createQuery(criteria
    //            .select(root).where(getSearchPredicates(root)));
    //      query.setFirstResult(this.page * getPageSize()).setMaxResults(
    //            getPageSize());
    //      this.pageItems = query.getResultList();
    //   }
    //
    //   private Predicate[] getSearchPredicates(Root<VendorXVendorPropertyType> root)
    //   {
    //
    //      CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
    //      List<Predicate> predicatesList = new ArrayList<Predicate>();
    //
    //      return predicatesList.toArray(new Predicate[predicatesList.size()]);
    //   }
    //
    //   public List<VendorXVendorPropertyType> getPageItems()
    //   {
    //      return this.pageItems;
    //   }
    //
    //   public long getCount()
    //   {
    //      return this.count;
    //   }
    //
    //   /*
    //    * Support listing and POSTing back VendorXVendorPropertyType entities (e.g. from inside an
    //    * HtmlSelectOneMenu)
    //    */
    //
    //   public List<VendorXVendorPropertyType> getAll()
    //   {
    //
    //      CriteriaQuery<VendorXVendorPropertyType> criteria = this.entityManager
    //            .getCriteriaBuilder().createQuery(VendorXVendorPropertyType.class);
    //      return this.entityManager.createQuery(
    //            criteria.select(criteria.from(VendorXVendorPropertyType.class))).getResultList();
    //   }

    @Resource
    private SessionContext sessionContext;

    @Override
    public Converter getConverter() {

        final VendorXVendorPropertyTypeBean ejbProxy = this.sessionContext
                .getBusinessObject(VendorXVendorPropertyTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((VendorXVendorPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    //   private VendorXVendorPropertyType add = new VendorXVendorPropertyType();
    //
    //   public VendorXVendorPropertyType getAdd()
    //   {
    //      return this.add;
    //   }
    //
    //   public VendorXVendorPropertyType getAdded()
    //   {
    //      VendorXVendorPropertyType added = this.add;
    //      this.add = new VendorXVendorPropertyType();
    //      return added;
    //   }
}