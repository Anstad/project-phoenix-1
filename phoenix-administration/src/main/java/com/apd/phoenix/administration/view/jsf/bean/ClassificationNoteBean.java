package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ClassificationNote;
import com.apd.phoenix.service.model.ItemClassification;
import com.apd.phoenix.service.model.NoteType;

/**
 * Backing bean for ClassificationNote entities.
 * <p>
 * This class provides CRUD functionality for all ClassificationNote entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ClassificationNoteBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ClassificationNote entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ClassificationNote classificationNote;

    public ClassificationNote getClassificationNote() {
        return this.classificationNote;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.classificationNote = this.example;
        }
        else {
            this.classificationNote = findById(getId());
        }
    }

    public ClassificationNote findById(Long id) {

        return this.entityManager.find(ClassificationNote.class, id);
    }

    /*
     * Support updating and deleting ClassificationNote entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.classificationNote);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.classificationNote);
                return "view?faces-redirect=true&id=" + this.classificationNote.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ClassificationNote entities with pagination
     */

    private int page;
    private long count;
    private List<ClassificationNote> pageItems;

    private ClassificationNote example = new ClassificationNote();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ClassificationNote getExample() {
        return this.example;
    }

    public void setExample(ClassificationNote example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ClassificationNote> root = countCriteria.from(ClassificationNote.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ClassificationNote> criteria = builder.createQuery(ClassificationNote.class);
        root = criteria.from(ClassificationNote.class);
        TypedQuery<ClassificationNote> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ClassificationNote> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        ItemClassification itemClassification = this.example.getItemClassification();
        if (itemClassification != null) {
            predicatesList.add(builder.equal(root.get("itemClassification"), itemClassification));
        }
        NoteType noteType = this.example.getNoteType();
        if (noteType != null) {
            predicatesList.add(builder.equal(root.get("noteType"), noteType));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ClassificationNote> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ClassificationNote entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ClassificationNote> getAll() {

        CriteriaQuery<ClassificationNote> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                ClassificationNote.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ClassificationNote.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ClassificationNoteBean ejbProxy = this.sessionContext.getBusinessObject(ClassificationNoteBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ClassificationNote) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ClassificationNote add = new ClassificationNote();

    public ClassificationNote getAdd() {
        return this.add;
    }

    public ClassificationNote getAdded() {
        ClassificationNote added = this.add;
        this.add = new ClassificationNote();
        return added;
    }
}