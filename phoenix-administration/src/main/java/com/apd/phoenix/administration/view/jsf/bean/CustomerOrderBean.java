package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.SystemUser;

/**
 * Backing bean for CustomerOrder entities.
 * <p>
 * This class provides CRUD functionality for all CustomerOrder entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class CustomerOrderBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving CustomerOrder entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CustomerOrder customerOrder;

    public CustomerOrder getCustomerOrder() {
        return this.customerOrder;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.customerOrder = this.example;
        }
        else {
            this.customerOrder = findById(getId());
        }
    }

    public CustomerOrder findById(Long id) {

        return this.entityManager.find(CustomerOrder.class, id);
    }

    /*
     * Support updating and deleting CustomerOrder entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.customerOrder);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.customerOrder);
                return "view?faces-redirect=true&id=" + this.customerOrder.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CustomerOrder entities with pagination
     */

    private int page;
    private long count;
    private List<CustomerOrder> pageItems;

    private CustomerOrder example = new CustomerOrder();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CustomerOrder getExample() {
        return this.example;
    }

    public void setExample(CustomerOrder example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<CustomerOrder> root = countCriteria.from(CustomerOrder.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<CustomerOrder> criteria = builder.createQuery(CustomerOrder.class);
        root = criteria.from(CustomerOrder.class);
        TypedQuery<CustomerOrder> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<CustomerOrder> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        SystemUser user = this.example.getUser();
        if (user != null) {
            predicatesList.add(builder.equal(root.get("user"), user));
        }
        Account account = this.example.getAccount();
        if (account != null) {
            predicatesList.add(builder.equal(root.get("account"), account));
        }
        Credential credential = this.example.getCredential();
        if (credential != null) {
            predicatesList.add(builder.equal(root.get("credential"), credential));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<CustomerOrder> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CustomerOrder entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CustomerOrder> getAll() {

        CriteriaQuery<CustomerOrder> criteria = this.entityManager.getCriteriaBuilder()
                .createQuery(CustomerOrder.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(CustomerOrder.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final CustomerOrderBean ejbProxy = this.sessionContext.getBusinessObject(CustomerOrderBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CustomerOrder) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CustomerOrder add = new CustomerOrder();

    public CustomerOrder getAdd() {
        return this.add;
    }

    public CustomerOrder getAdded() {
        CustomerOrder added = this.add;
        this.add = new CustomerOrder();
        return added;
    }
}