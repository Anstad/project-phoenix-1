package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AddressPropertyTypeBp;
import com.apd.phoenix.service.model.AddressPropertyType;

/**
 * Backing bean for AddressPropertyType entities.
 * <p>
 * This class provides CRUD functionality for all AddressPropertyType entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AddressPropertyTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private AddressPropertyTypeBp addressPropertyTypeBp;
    @Inject
    private Conversation conversation;

    @Resource
    private SessionContext sessionContext;

    private Long id;
    private AddressPropertyType addressPropertyType;
    private AddressPropertyType example = new AddressPropertyType();
    private AddressPropertyType add = new AddressPropertyType();

    private int page;
    private long count;

    private List<AddressPropertyType> pageItems;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.addressPropertyType.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Address Field Type to edit");
            message.setDetail("You need to select an Address Field Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /*
     * Support creating and retrieving AddressPropertyType entities
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AddressPropertyType getAddressPropertyType() {
        return this.addressPropertyType;
    }

    public String create() {
        this.conversation.begin();
        return "addressPropertyTypeAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.addressPropertyType = this.example;
        }
        else {
            this.addressPropertyType = addressPropertyTypeBp.findById(getId(), AddressPropertyType.class);
        }
    }

    public AddressPropertyType findById(Long id) {

        return this.addressPropertyTypeBp.findById(id, AddressPropertyType.class);
    }

    /*
     * Support updating and deleting AddressPropertyType entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.addressPropertyType.getId() == null) {
                this.addressPropertyTypeBp.create(this.addressPropertyType);
                return "addressPropertyTypeAdd?faces-redirect=true";
            }
            else {
                this.addressPropertyTypeBp.update(this.addressPropertyType);
                return "addressPropertyTypeEdit?faces-redirect=true&id=" + this.addressPropertyType.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.addressPropertyTypeBp.delete(this.addressPropertyType.getId(), AddressPropertyType.class);
            return "addressPropertyTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AddressPropertyType entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AddressPropertyType getExample() {
        return this.example;
    }

    public void setExample(AddressPropertyType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<AddressPropertyType> searchByExample(AddressPropertyType search) {
        return addressPropertyTypeBp.searchByExample(search, 0, 0);
    }

    public List<AddressPropertyType> searchByString(String s) {
        AddressPropertyType search = new AddressPropertyType();
        search.setName(s);
        return searchByExample(search);
    }

    public void paginate() {
        this.count = addressPropertyTypeBp.resultQuantity(this.example);
        this.pageItems = addressPropertyTypeBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    public List<AddressPropertyType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AddressPropertyType entities (e.g. from
     * inside an HtmlSelectOneMenu)
     */

    public List<AddressPropertyType> getAll() {
        return addressPropertyTypeBp.findAll(AddressPropertyType.class, 0, 0);
    }

    public Converter getConverter() {

        final AddressPropertyTypeBean ejbProxy = this.sessionContext.getBusinessObject(AddressPropertyTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AddressPropertyType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */
    public AddressPropertyType getAdd() {
        return this.add;
    }

    public AddressPropertyType getAdded() {
        AddressPropertyType added = this.add;
        this.add = new AddressPropertyType();
        return added;
    }
}