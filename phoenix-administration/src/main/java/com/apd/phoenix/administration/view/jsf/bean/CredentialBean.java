package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.Credential.Terms;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CredentialXVendor;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.CxmlConfiguration.DeploymentMode;
import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.NotificationLimit;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.MessageMetadata.PunchoutType;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.NotificationProperties.Addressing;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.NotificationLimit.LimitType;
import com.apd.phoenix.service.persistence.jpa.CredentialPropertyTypeDao;
import com.apd.phoenix.service.persistence.jpa.CredentialXVendorDao;
import com.apd.phoenix.service.persistence.jpa.PermissionDao;
import com.apd.phoenix.service.persistence.jpa.SkuTypeDao;
import com.apd.phoenix.service.persistence.jpa.VendorDao;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for Credential entities.
 * <p>
 * This class provides CRUD functionality for all Credential entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CredentialBean extends AbstractControllerBean<Credential> implements Serializable {

    private static final String BACKORDERED_ITEM_NOTICE_RECIPIENT_DEFAULT = "customerservice@americanproduct.com";

	private static final String LATE_SHIPMENT_NOTICE_RECIPIENT_DEFAULT = BACKORDERED_ITEM_NOTICE_RECIPIENT_DEFAULT;

	static {
        uiPanelIdsMap = new HashMap<String, String>();
        uiPanelIdsMap.put(CredentialXCredentialPropertyType.class.getName(), "propertyModal");
    }
    private static final long serialVersionUID = 1L;
    
    private final String DEFAULT_SKU_TYPE = "customer";
    private final String DEFAULT_CSR_PHONE = "704-522-9411";
	private final String DEFAULT_CSR_EMAIL = LATE_SHIPMENT_NOTICE_RECIPIENT_DEFAULT;
	private final String DEFAULT_PO_ACK_DESTINATION = "sales@americanproduct.com";
	private final String DEFAULT_SHIPMENT_NOTIFICATION_DESTINATION = "sales@americanproduct.com";
	private final String DEFAULT_INVOICE_DESTINATION = "ar@americanproduct.com";
	private final String DEFAULT_CREDIT_INVOICE_DESTINATION = "ar@americanproduct.com";
	private final MessageType DEFAULT_PO_ACK_MESSAGE_TYPE = MessageType.EMAIL;
	private final MessageType DEFAULT_SHIPMENT_NOTIFICATION_MESSAGE_TYPE = MessageType.EMAIL;
	private final MessageType DEFAULT_INVOICE_MESSAGE_TYPE = MessageType.EMAIL;
	private final MessageType DEFAULT_CREDIT_INVOICE_MESSAGE_TYPE = MessageType.EMAIL;
	private final Addressing DEFAULT_ADDRESSING_NOTIFICATION_PROPERTY_1 = Addressing.BCC;
	private final EventType DEFAULT_EVENT_TYPE_NOTIFICATION_PROPERTY_1 = EventType.ORDER_ACKNOWLEDGEMENT;
	private final String ORDER_ACKNOWLEDGEMENT_RECIPIENT_DEFAULT = "sales@americanproduct.com";
	private final Addressing DEFAULT_ADDRESSING_NOTIFICATION_PROPERTY_2 = Addressing.BCC;
	private final EventType DEFAULT_EVENT_TYPE_NOTIFICATION_PROPERTY_2 = EventType.CUSTOMER_INVOICED;
	private final String CUSTOMER_INVOICED_RECIPIENT_DEFAULT = "ar@americanproduct.com";
	private final String[] DEFAULT_PERMISSIONS = {"shopping add to cart",
												  "shopping live credential",
												  "shopping maintain user favorites",
												  "shopping order backordered items",
												  "shopping view custom order form",
												  "shopping view special order form"};
	private final String USSCO_VENDOR_NAME = "958119";
	private final String USSCO_ACCOUNT_ID_DEFAULT = "USSCO";
	private final String APD_VENDOR_NAME = "00000";
	private final String APD_ACCOUNT_ID_DEFAULT = "American Product Distributors";
	private final String DEFAULT_BILLING_ZIPCODE = "28273";
	private final DeploymentMode DEFAULT_DEPLOYMENT_MODE = DeploymentMode.PRODUCTION;
	
	@Inject
    private CredentialBp bp;

    @Inject
    private AccountBean accountBean;

    @Inject
    private Conversation conversation;

    @Inject
    private CredentialSearchBean searchBean;
    
    @Inject
    private SkuTypeBp skuTypeBp;
    
	@Inject
	private CredentialPropertyTypeDao propertyTypeDao;
	
	@Inject
	private VendorDao vendorDao;
	
	@Inject
	private PermissionDao permissionDao;
	
	@Inject
    private CredentialXVendorDao cxvDao;
	
    private static final Logger LOG = LoggerFactory.getLogger(CredentialBean.class);

    public static Map<String, String> getUiPanelIdsMap() {
        return uiPanelIdsMap;
    }
    
    private PunchoutType selectedPunchoutType;

    public PunchoutType getSelectedPunchoutType() {
        return selectedPunchoutType;
    }

    public void setSelectedPunchoutType(PunchoutType selectedPunchoutType) {
        this.selectedPunchoutType = selectedPunchoutType;
    }
    
    public PunchoutType[] getPunchoutTypes() {
        return PunchoutType.values();
    }
    
    public PunchoutOrderOperation[] getPunchoutOperationsAllowed() {
    	return PunchoutOrderOperation.values();
    }
    
    public CxmlConfiguration.DeploymentMode[] getDeploymentModes() {
        return CxmlConfiguration.DeploymentMode.values();
    }

    private CxmlCredential currentFromCredential = new CxmlCredential();
    private CxmlCredential currentToCredential = new CxmlCredential();
    private CxmlCredential currentSenderCredential = new CxmlCredential();
    private CxmlCredential currentCustomerFromCredential = new CxmlCredential();
    private CxmlCredential currentCustomerToCredential = new CxmlCredential();
    private CxmlCredential currentCustomerSenderCredential = new CxmlCredential();
    
    public void createFromCredential() {
        this.currentFromCredential = new CxmlCredential();
    }

    public void createToCredential() {
        this.currentToCredential = new CxmlCredential();
    }

    public void createSenderCredential() {
        this.currentSenderCredential = new CxmlCredential();
    }
    
    public void createCustomerFromCredential() {
        this.currentCustomerFromCredential = new CxmlCredential();
    }

    public void createCustomerToCredential() {
        this.currentCustomerToCredential = new CxmlCredential();
    }

    public void createCustomerSenderCredential() {
        this.currentCustomerSenderCredential = new CxmlCredential();
    }

    public void saveCurrentFromCredential() {
        this.currentInstance.getOutgoingFromCredentials().add(currentFromCredential);
    }

    public void saveCurrentSenderCredential() {
        this.currentInstance.getOutgoingSenderCredentials().add(currentSenderCredential);
    }

    public void saveCurrentToCredential() {
        this.currentInstance.getOutgoingToCredentials().add(currentToCredential);
    }

    public void saveCurrentCustomerFromCredential() {
        this.currentInstance.getCustomerOutoingFromCred().add(currentCustomerFromCredential);
    }

    public void saveCurrentCustomerSenderCredential() {
        this.currentInstance.getCustomerOutoingSenderCred().add(currentCustomerSenderCredential);
    }

    public void saveCurrentCustomerToCredential() {
        this.currentInstance.getCustomerOutoingToCred().add(currentCustomerToCredential);
    }

    public void removeCurrentFromCredential(CxmlCredential credential) {
        this.currentInstance.getOutgoingFromCredentials().remove(credential);
    }

    public void removeCurrentSenderCredential(CxmlCredential credential) {
        this.currentInstance.getOutgoingSenderCredentials().remove(credential);
    }

    public void removeCurrentToCredential(CxmlCredential credential) {
        this.currentInstance.getOutgoingToCredentials().remove(credential);
    }
    
    public void removeCurrentCustomerFromCredential(CxmlCredential credential) {
        this.currentInstance.getCustomerOutoingFromCred().remove(credential);
    }

    public void removeCurrentCustomerSenderCredential(CxmlCredential credential) {
        this.currentInstance.getCustomerOutoingSenderCred().remove(credential);
    }

    public void removeCurrentCustomerToCredential(CxmlCredential credential) {
        this.currentInstance.getCustomerOutoingToCred().remove(credential);
    }

    public CxmlCredential getCurrentFromCredential() {
        return currentFromCredential;
    }

    public void setCurrentFromCredential(CxmlCredential currentFromCredential) {
        this.currentFromCredential = currentFromCredential;
    }

    public CxmlCredential getCurrentToCredential() {
        return currentToCredential;
    }

    public void setCurrentToCredential(CxmlCredential currentToCredential) {
        this.currentToCredential = currentToCredential;
    }

    public CxmlCredential getCurrentSenderCredential() {
        return currentSenderCredential;
    }

    public void setCurrentSenderCredential(CxmlCredential currentSenderCredential) {
        this.currentSenderCredential = currentSenderCredential;
    }

    public CxmlCredential getCurrentCustomerFromCredential() {
        return currentCustomerFromCredential;
    }

    public void setCurrentCustomerFromCredential(CxmlCredential currentCustomerFromCredential) {
        this.currentCustomerFromCredential = currentCustomerFromCredential;
    }

    public CxmlCredential getCurrentCustomerToCredential() {
        return currentCustomerToCredential;
    }

    public void setCurrentCustomerToCredential(CxmlCredential currentCustomerToCredential) {
        this.currentCustomerToCredential = currentCustomerToCredential;
    }

    public CxmlCredential getCurrentCustomerSenderCredential() {
        return currentCustomerSenderCredential;
    }

    public void setCurrentCustomerSenderCredential(CxmlCredential currentCustomerSenderCredential) {
        this.currentCustomerSenderCredential = currentCustomerSenderCredential;
    }

    /*
     * Support creating and retrieving Credential entities
     */

    //   public void validateSelection(FacesContext context, UIComponent component, Object value) {
    //       if(this.currentInstance.getId() == null) {
    //    	   ((UIInput)component).setValid(false);
    //           FacesMessage message = new FacesMessage();
    //           message.setSeverity(FacesMessage.SEVERITY_ERROR);
    //           message.setSummary("Select Credential to edit");
    //           message.setDetail("You need to select a Credential to edit.");
    //           context.addMessage(component.getClientId(), message);
    //           return;
    //       }
    //   }

    private Credential currentInstance = new Credential();

    public Credential getCredential() {
        return this.currentInstance;
    }

    public void setCredential(Credential credential) {
        this.currentInstance = credential;
    }

    // The property currently being edited.
    private CredentialXCredentialPropertyType currentProperty = new CredentialXCredentialPropertyType();

    // The PO number currently being edited.
    private PoNumber currentPoNumber = new PoNumber();

    // The Notification Property currently being edited.
    private NotificationProperties currentNotification = new NotificationProperties();

    // The bulletin currently being edited.
    private Bulletin currentBulletin = new Bulletin();

    // The IP address currently being edited.
    private IpAddress currentIpAddress = new IpAddress();
    
    // The limit approval currently being edited.
    private LimitApproval currentLimitApproval = new LimitApproval();      

    // The Payment Information currently being edited.
    private PaymentInformation currentPaymentInformation = new PaymentInformation();
    
    private List<CredentialXVendor> credXVendors = new ArrayList<>();
    
    private List<CredentialXVendor> credXVendorsToDelete = new ArrayList<>();

    public CredentialXCredentialPropertyType getCurrentProperty() {
        return this.currentProperty;
    }

    public void setCurrentProperty(CredentialXCredentialPropertyType property) {
        this.currentProperty = property;
    }

    /**
     * This method is called when the user presses the "New Property" button, and creates 
     * a new Property to modify.
     */
    public void createProperty() {
        this.currentProperty = new CredentialXCredentialPropertyType();
    }

    /**
     * Removes the selected property from the set of properties on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeProperty(CredentialXCredentialPropertyType toRemove) {
        this.currentInstance.getProperties().remove(toRemove);
    }

    /**
     * Saves the selected property onto the current Credential instance.
     */
    public void saveProperty() {
        this.currentInstance.getProperties().add(this.currentProperty);
    }

    public PoNumber getCurrentPoNumber() {
        return this.currentPoNumber;
    }

    public void setCurrentPoNumber(PoNumber po) {
        this.currentPoNumber = po;
    }

    /**
     * This method is called when the user presses the "New PO Number" button, and creates 
     * a new PO number to modify.
     */
    public void createPoNumber() {
        this.currentPoNumber = new PoNumber();
    }

    /**
     * Removes the selected PO Number from the set of pos on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removePoNumber(PoNumber toRemove) {
        this.currentInstance.getBlanketPos().remove(toRemove);
    }

    /**
     * Saves the selected PO Number onto the current Credential instance.
     */
    public void savePoNumber() {
        this.currentInstance.getBlanketPos().add(this.currentPoNumber);
    }

    public Bulletin getCurrentBulletin() {
        return this.currentBulletin;
    }

    public void setCurrentBulletin(Bulletin bulletin) {
        this.currentBulletin = bulletin;
    }

    /**
     * This method is called when the user presses the "New Bulletin" button, and creates 
     * a new bulletin to modify.
     */
    public void createBulletin() {
        this.currentBulletin = new Bulletin();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeBulletin(Bulletin toRemove) {
        this.currentInstance.getBulletins().remove(toRemove);
    }

    /**
     * Saves the selected Bulletin onto the current Credential instance.
     */
    public void saveBulletin() {
        this.currentInstance.getBulletins().add(this.currentBulletin);
    }

    public NotificationProperties getCurrentNotification() {
        return this.currentNotification;
    }

    public void setCurrentNotification(NotificationProperties notification) {
        this.currentNotification = notification;
    }

    /**
     * This method is called when the user presses the "New Notification" button, and creates 
     * a new Notification to modify.
     */
    public void createNotification() {
        this.currentNotification = new NotificationProperties();
    }

    /**
     * Removes the selected bulletin from the set of bulletins on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeNotification(NotificationProperties toRemove) {
        this.currentInstance.getNotificationProperties().remove(toRemove);
    }

    /**
     * Saves the selected Notification onto the current Credential instance.
     */
    public void saveNotification() {
        this.currentInstance.getNotificationProperties().add(this.currentNotification);
    }

    public IpAddress getCurrentIpAddress() {
        return this.currentIpAddress;
    }

    public void setCurrentIpAddress(IpAddress address) {
        this.currentIpAddress = address;
    }

    /**
     * This method is called when the user presses the "Add IP Address" button, and creates 
     * a new IP Address to modify.
     */
    public void createIpAddress() {
        this.currentIpAddress = new IpAddress();
    }

    /**
     * Removes the selected address from the set of adressess on the current Credential instance.
     * 
     * @param toRemove
     */
    public void removeIpAddress(IpAddress toRemove) {
        this.currentInstance.getIps().remove(toRemove);
    }

    /**
     * Saves the selected IP Address onto the current Credential instance.
     */
    public void saveIpAddress() {
        this.currentInstance.getIps().add(this.currentIpAddress);
    }

    public PaymentInformation getCurrentPaymentInformation() {
        return this.currentPaymentInformation;
    }

    public void setCurrentPaymentInformation(PaymentInformation paymentInformation) {
        this.currentPaymentInformation = paymentInformation;
    }

    /**
     * This method is called when the user presses the "Edit Payment Information" button, and creates 
     * a new Payment Information or presents the existing Payment Information.
     */
    public void fetchPaymentInformation() {
        if (this.currentInstance.getPaymentInformation() == null) {
            this.currentPaymentInformation = new PaymentInformation();
        }
        else {
            this.currentPaymentInformation = this.currentInstance.getPaymentInformation();
        }
    }
    
    public void togglePaymentInfoAddress() {
    	if (this.currentPaymentInformation.getBillingAddress() == null) {
    		this.currentPaymentInformation.setBillingAddress(new Address());
    	}
    	else {
    		this.currentPaymentInformation.setBillingAddress(null);
    	}
    }

    /**
     * Saves the Payment Information onto the current Credential instance.
     */
    public void savePaymentInformation() {
        this.currentInstance.setPaymentInformation(this.currentPaymentInformation);
    }
    
    public void clearPaymentInformation() {
    	this.currentInstance.setPaymentInformation(null);
    }
    
    public void removeCatalogFromCredential() {
    	this.currentInstance.setCatalog(null);
    }
    
    @Override
    public String create() {
        this.conversation.begin();
        return "credentialAdd?faces-redirect=true";
    }

    @Override
    public void retrieve() {
        if (searchBean.getSelection() != null && useSearchResult) {
            this.currentInstance = bp.findById(searchBean.getSelection().getId(), Credential.class);
            currentInstance = this.bp.loadCredentialForMaintenance(currentInstance);
            searchBean.setSelection(null);
            CredentialXVendor searchCxv = new CredentialXVendor();
            searchCxv.setCredential(new Credential());
            searchCxv.getCredential().setId(this.currentInstance.getId());
            this.credXVendors = cxvDao.searchByExactExample(searchCxv, 0, 0);
        }
        
        this.populateNotificationLimits();

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
            if (this.currentInstance.getId() == null) {
            	setCredentialDefaults();
            }
        }

    }

    public void setAccount(Account toAdd) {
        //TODO: validation to prevent null
        if (toAdd.getRootAccount() != null) {
            this.currentInstance.setRootAccount(toAdd.getRootAccount());
        }
        else {
            this.currentInstance.setRootAccount(toAdd);
        }
    }

    /*
     * Support updating and deleting Credential entities
     */

    @Override
    public String update() {
        if (!validToUpdate()) {
            return null;
        }
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

        	boolean isCreating = false;
            if (currentInstance.getId() == null) {
                this.currentInstance = bp.create(this.currentInstance);
                isCreating = true;
            }
            this.currentInstance = bp.update(this.currentInstance);
            List<CredentialXVendor> cxvsToRemove = new ArrayList<>();
            Set<Vendor> vendorsWithCxvs = new HashSet<>();
            for (CredentialXVendor cxv : this.credXVendors) {
            	if (cxv.getVendor() == null || vendorsWithCxvs.contains(cxv.getVendor())) {
            		cxvsToRemove.add(cxv);
            		credXVendorsToDelete.add(cxv);
            	}
            	vendorsWithCxvs.add(cxv.getVendor());
            }
            this.credXVendors.removeAll(cxvsToRemove);
            for (CredentialXVendor cxv : credXVendorsToDelete) {
            	if (cxv.getId() != null) {
            		cxvDao.delete(cxv.getId(), CredentialXVendor.class);
            	}
            }
            for (CredentialXVendor cxv : credXVendors) {
            	if (cxv.getId() == null) {
            		cxvDao.create(cxv);
            	}
            	cxvDao.update(cxv);
            }
            if (isCreating) {
                return "credentialAdd?faces-redirect=true";
            }
            else {
            	return "credentialEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
        	LOG.error(e.getMessage());
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unexpected Error", "Unexpected Error"));
            return null;
        }
    }

    @Override
    public String delete() {
        this.conversation.end();

        try {
            bp.delete(this.currentInstance.getId(), Credential.class);
            return "credentialEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Credential entities with pagination
     */

    private int page;
    private long count;
    private List<Credential> pageItems;

    private Credential example = new Credential();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Credential getExample() {
        return this.example;
    }

    public void setExample(Credential example) {
        this.example = example;
    }

    @Override
    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = bp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = bp.resultQuantity(this.example);
    }

    public List<Credential> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Credential entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    @Override
    public List<Credential> getAll() {

        return bp.findAll(Credential.class, 0, 0);
    }

    @Override
    public List<Credential> searchByExample(Credential search) {
        return bp.searchByExample(search, 0, 0);
    }

    public List<Credential> searchByString(String s) {
        Credential search = new Credential();
        search.setName(s);
        return searchByExample(search);
    }

    public List<Account> searchAccountByString(String s) {
        Account search = new Account();
        search.setName(s);
        return accountBean.searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    @Override
    public Converter getConverter() {

        final CredentialBean ejbProxy = this.sessionContext.getBusinessObject(CredentialBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (ejbProxy.currentInstance == null) {
                    ejbProxy.currentInstance = new Credential();
                }
                ejbProxy.retrieve();
                return ejbProxy.getCredential();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Credential) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Credential add = new Credential();

    public Credential getAdd() {
        return this.add;
    }

    public Credential getAdded() {
        Credential added = this.add;
        this.add = new Credential();
        return added;
    }

    @Inject
    private ValidationBean validationBean;

    public void validateUniqueProperties(FacesContext context, UIComponent component, Object value) {
        Collection<Object> toReturn = new HashSet<Object>();
        for (Object o : this.currentInstance.getProperties()) {
            toReturn.add(o);
        }
        if (this.validationBean.repeatedEntry(toReturn, "getType")) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Multiple properties with the same type");
            message.setDetail("You can't have multiple properties with the same type.");
            context.addMessage(null, message);
        }
    }

    private boolean validToUpdate() {
        Credential searchCredential = new Credential();
        searchCredential.setName(this.currentInstance.getName());
        List<Credential> searchList = this.bp.searchByExactExample(searchCredential, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A credential already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        if (this.currentInstance.getRootAccount() == null) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Account tree is required");
            message.setDetail("Credentials must be assigned to an account tree.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

    private boolean useSearchResult = true;

    public void ignoreSearchResult() {
        useSearchResult = false;
    }

    public String cloneSelectedCredential(Credential selected) {
        this.currentInstance = this.bp.cloneEntity(selected);
        this.currentInstance.setName(this.currentInstance.getName() + " copy");
        useSearchResult = true;
        CredentialXVendor searchCxv = new CredentialXVendor();
        searchCxv.setCredential(new Credential());
        searchCxv.getCredential().setId(selected.getId());
        credXVendors = new ArrayList<>();
        credXVendorsToDelete = new ArrayList<>();
        for (CredentialXVendor cxv : this.cxvDao.searchByExactExample(searchCxv, 0, 0)) {
        	CredentialXVendor newCxv = new CredentialXVendor();
        	newCxv.setAccountId(cxv.getAccountId());
        	newCxv.setCredential(this.currentInstance);
        	newCxv.setVendor(cxv.getVendor());
        	credXVendors.add(newCxv);
        }
        return "credentialAdd?faces-redirect=true";
    }

    /**
     * This method validates an IP address. Invalidates if the address is blank, or there is already an address with 
     * that value.
     * 
     * @param context
     * @param component
     * @param value
     */
    public void addressValidator(FacesContext context, UIComponent component, Object value) {
        if (value == null || value.toString().isEmpty()) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("No Address Specified");
            message.setDetail("Please enter an IP address.");
            context.addMessage(component.getClientId(), message);
            return;
        }
        for (IpAddress address : this.getCredential().getIps()) {
            if (!address.equals(this.getCurrentIpAddress()) && address.getValue().equals(value.toString())) {
                ((UIInput) component).setValid(false);
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Duplicate Address");
                message.setDetail("This address has already been added.");
                context.addMessage(component.getClientId(), message);
                return;
            }
        }
    }

    public void addPermission(Permission permission) {
        if (permission != null) {
            this.currentInstance.getPermissions().add(permission);
        }
    }

    public void removePermission(Permission permission) {
        if (permission != null) {
            this.currentInstance.getPermissions().remove(permission);
        }
    }

    public void addRole(Role role) {
        if (role != null) {
            this.currentInstance.getRoles().add(role);
        }
    }

    public void removeRole(Role role) {
        if (role != null) {
            this.currentInstance.getRoles().remove(role);
        }
    }
    
    public void removeLimitApproval(LimitApproval limitApproval) {
        if (limitApproval != null) {
            this.currentInstance.getLimitApproval().remove(limitApproval);
        }
    }    
    
    public void saveLimitApproval() {
        this.currentInstance.getLimitApproval().add(this.currentLimitApproval);
    }
    
    public void createLimitApproval() {
        this.currentLimitApproval = new LimitApproval();
    }
    
    public LimitApproval getCurrentLimitApproval() {
		return this.currentLimitApproval;
	}

	public void setCurrentLimitApproval(LimitApproval currentLimitApproval) {
		this.currentLimitApproval = currentLimitApproval;
	}
    
    private CredentialXVendor currentCxv;

	public List<CredentialXVendor> getCredXVendors() {
		return credXVendors;
	}

	public void setCredXVendors(List<CredentialXVendor> credXVendorSet) {
		this.credXVendors = credXVendorSet;
	}

	public void removeCredXVendor(CredentialXVendor credXVendor) {
		this.credXVendors.remove(credXVendor);
		this.credXVendorsToDelete.add(credXVendor);
	}

	public void addCurrentCredXVendor() {
		if (!this.credXVendors.contains(this.currentCxv)) {
			this.credXVendors.add(this.currentCxv);
		}
	}

	public void newCurrentCredXVendor() {
		this.currentCxv = new CredentialXVendor();
		this.currentCxv.setCredential(this.currentInstance);
	}

	public CredentialXVendor getCurrentCredXVendor() {
		return this.currentCxv;
	}

	public void setCurrentCredXVendor(CredentialXVendor credXVendor) {
		this.currentCxv = credXVendor;
	}
	
    public SelectItem[] getCommunicationMessageType() {
        SelectItem[] items = new SelectItem[MessageType.values().length];
        int i = 0;
        for (MessageType g : MessageType.values()) {
            items[i++] = new SelectItem(g, g.getLabel());
        }
        return items;
    }

    public SelectItem[] getLimitTypes() {
        SelectItem[] items = new SelectItem[LimitType.values().length];
        int i = 0;
        for (LimitType g : LimitType.values()) {
            items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }
    
    public void populateNotificationLimits() {
    	Set<EventType> alreadyAddedTypes = new HashSet<>();
    	for (NotificationLimit limit : this.currentInstance.getNotificationLimits()) {
    		alreadyAddedTypes.add(limit.getEventType());
    	}
    	for (EventType type : EventType.values()) {
    		if (!alreadyAddedTypes.contains(type)) {
    			NotificationLimit newLimit = new NotificationLimit();
    			newLimit.setEventType(type);
    			this.currentInstance.getNotificationLimits().add(newLimit);
    			newLimit.setLimitType(LimitType.ALL);
    			if (type.equals(EventType.RESENT_NOTFICATION) || type.equals(EventType.CREDIT_CARD_TRANSACTION) || type.equals(EventType.PUNCHOUT_SETUP_REQUEST) || type.equals(EventType.DELIVERY_SIGNATURE) || type.equals(EventType.DELIVERY_DRIVER_COMMENTS) || type.equals(EventType.DELIVERY_DRIVER_PHOTO) || type.equals(EventType.PURCHASE_ORDER_PARTNER_RESPONSE)) {
        			newLimit.setLimitType(LimitType.NONE);
    			}
    			if (type.equals(EventType.DECLINED) || type.equals(EventType.VOUCHED) || type.equals(EventType.INVOICED) || type.equals(EventType.FUNCTIONAL_ACKNOWLEDGEMENT) || type.equals(EventType.PURCHASE_ORDER_SENT) || type.equals(EventType.VENDOR_NOTIFICATION) || type.equals(EventType.CUSTOMER_NOTIFICATION) || type.equals(EventType.TECH_NOTIFICATION) || type.equals(EventType.VOUCHER_ERRORS) || type.equals(EventType.INVOICE_ERRORS) || type.equals(EventType.ORDER_ACKNOWLEDGEMENT_ERRORS) || type.equals(EventType.SHIPMENT_NOTIFICATION_ERRORS) || type.equals(EventType.JUMPTRACK_STOP_NOTIFICATION) || type.equals(EventType.JUMPTRACK_MANIFEST) || type.equals(EventType.CC_DECLINED_NOTICE) || type.equals(EventType.ORDER_REQUEST) || type.equals(EventType.SHIPMENT_NOTICE)) {
        			newLimit.setLimitType(LimitType.BCC_ONLY);
    			}
    		}
    	}
	}
    			
    //TODO:  A lot of calls to database.  Might be better to amalgamate some calls into a couple hql queries.
    //TODO:  Also might be worth externalizing some of these into a properties file in the future, or even a separate class to manage defaults easily.
    public void setCredentialDefaults() {
    	
    	this.currentInstance.setSkuType(skuTypeBp.getSkuType(DEFAULT_SKU_TYPE));  //customer
    	this.currentInstance.setStoreCredit(BigDecimal.ZERO);
    	
    	this.currentInstance.setCsrPhone(DEFAULT_CSR_PHONE);
    	this.currentInstance.setCsrEmail(DEFAULT_CSR_EMAIL);
    	this.currentInstance.setPoAcknowledgementDestination(DEFAULT_PO_ACK_DESTINATION);
    	this.currentInstance.setPoAcknowledgementMessageType(DEFAULT_PO_ACK_MESSAGE_TYPE);
    	this.currentInstance.setShipmentNotificationDestination(DEFAULT_SHIPMENT_NOTIFICATION_DESTINATION);
    	this.currentInstance.setShipmentNotificationMessageType(DEFAULT_SHIPMENT_NOTIFICATION_MESSAGE_TYPE);
    	this.currentInstance.setInvoiceDestination(DEFAULT_INVOICE_DESTINATION);
    	this.currentInstance.setInvoiceMessageType(DEFAULT_INVOICE_MESSAGE_TYPE);
    	this.currentInstance.setCreditInvoiceDestination(DEFAULT_CREDIT_INVOICE_DESTINATION);
    	this.currentInstance.setCreditInvoiceMessageType(DEFAULT_CREDIT_INVOICE_MESSAGE_TYPE);
    	
    	//----------Default Notifications----------

    	this.addDefaultNotificationProperty(EventType.ORDER_ACKNOWLEDGEMENT, ORDER_ACKNOWLEDGEMENT_RECIPIENT_DEFAULT);
    	this.addDefaultNotificationProperty(EventType.BACKORDERED_ITEM_NOTICE, BACKORDERED_ITEM_NOTICE_RECIPIENT_DEFAULT);
    	this.addDefaultNotificationProperty(EventType.LATE_SHIPMENT_NOTICE, LATE_SHIPMENT_NOTICE_RECIPIENT_DEFAULT);
    	this.addDefaultNotificationProperty(EventType.CUSTOMER_INVOICED, CUSTOMER_INVOICED_RECIPIENT_DEFAULT);
    	
    	//----------Default Permissions----------
    	Set<Permission> permissions = new HashSet<>();
    	
    	for(String defaultPermission : DEFAULT_PERMISSIONS) {
    		Permission permission = new Permission();
        	permission.setName(defaultPermission);
        	permissions.add(permissionDao.searchByExactExample(permission, 0, 0).get(0));
    	}

    	this.currentInstance.setPermissions(permissions);

    	this.addDefaultVendorId(USSCO_ACCOUNT_ID_DEFAULT, USSCO_VENDOR_NAME);
    	this.addDefaultVendorId(APD_ACCOUNT_ID_DEFAULT, APD_VENDOR_NAME);
    	
    	//----------Default Billing Zip Code----------
    	this.currentInstance.setDefaultPunchoutZipCode(DEFAULT_BILLING_ZIPCODE);
    	
    	//----------Default Credential Property Types----------
    	for (CredentialPropertyType type : propertyTypeDao.findAll(CredentialPropertyType.class, 0, 0)) {
    		if (StringUtils.isNotBlank(type.getDefaultValue())) {
    			CredentialXCredentialPropertyType newProperty = new CredentialXCredentialPropertyType();
    			newProperty.setType(type);
    			newProperty.setValue(type.getDefaultValue());
    			this.currentInstance.getProperties().add(newProperty);
    		}
    	}
    	
    	this.currentInstance.setDeploymentMode(DEFAULT_DEPLOYMENT_MODE);
    }
    
    private void addDefaultNotificationProperty(EventType event, String recipient) {
    	NotificationProperties notification = new NotificationProperties();
    	notification.setEventType(event);
    	notification.setRecipients(recipient);
		notification.setAddressing(Addressing.BCC);
		this.currentInstance.getNotificationProperties().add(notification);
    }
    
    private void addDefaultVendorId(String vendorName, String accountId) {
    	
    	CredentialXVendor defaultCredXVendor = new CredentialXVendor();
    	defaultCredXVendor.setAccountId(accountId);
    	Vendor searchVendor = new Vendor();
    	searchVendor.setName(vendorName);
    	List<Vendor> searchList = vendorDao.searchByExactExample(searchVendor, 0, 0);
    	if(searchList != null && !searchList.isEmpty()) {
    		defaultCredXVendor.setVendor(searchList.get(0));
    		defaultCredXVendor.setCredential(this.currentInstance);

        	this.credXVendors.add(defaultCredXVendor);
    	}
    }

    public SelectItem[] getAllTerms() {
        SelectItem[] items = new SelectItem[Terms.values().length];
        int i = 0;
        for (Terms g : Terms.values()) {
            items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }
    
    public void clearApprover() {
    	if (this.currentInstance.getUser() != null) {
    		this.currentInstance.setUser(null);
    	}
    }

}