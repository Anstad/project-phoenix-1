package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ItemClassificationImage;
import com.apd.phoenix.service.model.ImageType;

/**
 * Backing bean for ItemClassificationImage entities.
 * <p>
 * This class provides CRUD functionality for all ItemClassificationImage entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ItemClassificationImageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemClassificationImage entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemClassificationImage itemClassificationImage;

    public ItemClassificationImage getItemClassificationImage() {
        return this.itemClassificationImage;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.itemClassificationImage = this.example;
        }
        else {
            this.itemClassificationImage = findById(getId());
        }
    }

    public ItemClassificationImage findById(Long id) {

        return this.entityManager.find(ItemClassificationImage.class, id);
    }

    /*
     * Support updating and deleting ItemClassificationImage entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.itemClassificationImage);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.itemClassificationImage);
                return "view?faces-redirect=true&id=" + this.itemClassificationImage.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemClassificationImage entities with pagination
     */

    private int page;
    private long count;
    private List<ItemClassificationImage> pageItems;

    private ItemClassificationImage example = new ItemClassificationImage();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemClassificationImage getExample() {
        return this.example;
    }

    public void setExample(ItemClassificationImage example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ItemClassificationImage> root = countCriteria.from(ItemClassificationImage.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ItemClassificationImage> criteria = builder.createQuery(ItemClassificationImage.class);
        root = criteria.from(ItemClassificationImage.class);
        TypedQuery<ItemClassificationImage> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ItemClassificationImage> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        String imageUrl = this.example.getImageUrl();
        if (imageUrl != null && !"".equals(imageUrl)) {
            predicatesList.add(builder.like(root.<String> get("imageUrl"), '%' + imageUrl + '%'));
        }
        ImageType imageType = this.example.getImageType();
        if (imageType != null) {
            predicatesList.add(builder.equal(root.get("imageType"), imageType));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ItemClassificationImage> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemClassificationImage entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemClassificationImage> getAll() {

        CriteriaQuery<ItemClassificationImage> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                ItemClassificationImage.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ItemClassificationImage.class)))
                .getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemClassificationImageBean ejbProxy = this.sessionContext
                .getBusinessObject(ItemClassificationImageBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemClassificationImage) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemClassificationImage add = new ItemClassificationImage();

    public ItemClassificationImage getAdd() {
        return this.add;
    }

    public ItemClassificationImage getAdded() {
        ItemClassificationImage added = this.add;
        this.add = new ItemClassificationImage();
        return added;
    }
}