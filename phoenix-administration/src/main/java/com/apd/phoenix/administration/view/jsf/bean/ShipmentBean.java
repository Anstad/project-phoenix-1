package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.ShippingPartner;

/**
 * Backing bean for Shipment entities.
 * <p>
 * This class provides CRUD functionality for all Shipment entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ShipmentBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving Shipment entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Shipment shipment;

    public Shipment getShipment() {
        return this.shipment;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.shipment = this.example;
        }
        else {
            this.shipment = findById(getId());
        }
    }

    public Shipment findById(Long id) {

        return this.entityManager.find(Shipment.class, id);
    }

    /*
     * Support updating and deleting Shipment entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.shipment);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.shipment);
                return "view?faces-redirect=true&id=" + this.shipment.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Shipment entities with pagination
     */

    private int page;
    private long count;
    private List<Shipment> pageItems;

    private Shipment example = new Shipment();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Shipment getExample() {
        return this.example;
    }

    public void setExample(Shipment example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<Shipment> root = countCriteria.from(Shipment.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<Shipment> criteria = builder.createQuery(Shipment.class);
        root = criteria.from(Shipment.class);
        TypedQuery<Shipment> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<Shipment> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        ShippingPartner shippingPartner = this.example.getShippingPartner();
        if (shippingPartner != null) {
            predicatesList.add(builder.equal(root.get("shippingPartner"), shippingPartner));
        }
        String trackingNumber = this.example.getTrackingNumber();
        if (trackingNumber != null && !"".equals(trackingNumber)) {
            predicatesList.add(builder.like(root.<String> get("trackingNumber"), '%' + trackingNumber + '%'));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<Shipment> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Shipment entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Shipment> getAll() {

        CriteriaQuery<Shipment> criteria = this.entityManager.getCriteriaBuilder().createQuery(Shipment.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(Shipment.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ShipmentBean ejbProxy = this.sessionContext.getBusinessObject(ShipmentBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Shipment) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Shipment add = new Shipment();

    public Shipment getAdd() {
        return this.add;
    }

    public Shipment getAdded() {
        Shipment added = this.add;
        this.add = new Shipment();
        return added;
    }
}