package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.SpecificationDescription;
import com.apd.phoenix.service.model.DescriptionType;

/**
 * Backing bean for SpecificationDescription entities.
 * <p>
 * This class provides CRUD functionality for all SpecificationDescription entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class SpecificationDescriptionBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving SpecificationDescription entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private SpecificationDescription specificationDescription;

    public SpecificationDescription getSpecificationDescription() {
        return this.specificationDescription;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.specificationDescription = this.example;
        }
        else {
            this.specificationDescription = findById(getId());
        }
    }

    public SpecificationDescription findById(Long id) {

        return this.entityManager.find(SpecificationDescription.class, id);
    }

    /*
     * Support updating and deleting SpecificationDescription entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.specificationDescription);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.specificationDescription);
                return "view?faces-redirect=true&id=" + this.specificationDescription.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching SpecificationDescription entities with pagination
     */

    private int page;
    private long count;
    private List<SpecificationDescription> pageItems;

    private SpecificationDescription example = new SpecificationDescription();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public SpecificationDescription getExample() {
        return this.example;
    }

    public void setExample(SpecificationDescription example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<SpecificationDescription> root = countCriteria.from(SpecificationDescription.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<SpecificationDescription> criteria = builder.createQuery(SpecificationDescription.class);
        root = criteria.from(SpecificationDescription.class);
        TypedQuery<SpecificationDescription> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<SpecificationDescription> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        DescriptionType descriptionType = this.example.getDescriptionType();
        if (descriptionType != null) {
            predicatesList.add(builder.equal(root.get("descriptionType"), descriptionType));
        }
        String description = this.example.getDescription();
        if (description != null && !"".equals(description)) {
            predicatesList.add(builder.like(root.<String> get("description"), '%' + description + '%'));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<SpecificationDescription> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back SpecificationDescription entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<SpecificationDescription> getAll() {

        CriteriaQuery<SpecificationDescription> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                SpecificationDescription.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(SpecificationDescription.class)))
                .getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final SpecificationDescriptionBean ejbProxy = this.sessionContext
                .getBusinessObject(SpecificationDescriptionBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((SpecificationDescription) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private SpecificationDescription add = new SpecificationDescription();

    public SpecificationDescription getAdd() {
        return this.add;
    }

    public SpecificationDescription getAdded() {
        SpecificationDescription added = this.add;
        this.add = new SpecificationDescription();
        return added;
    }
}