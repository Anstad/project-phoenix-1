/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

import com.apd.phoenix.service.model.PhoneNumberType;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 *
 */
public class PhoneNumberEvent {

    private String countryCode;
    private String areaCode;
    private String exchange;
    private String lineNumber;
    private PhoneNumberType selectedType;

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * @param areaCode the areaCode to set
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * @return the exchange
     */
    public String getExchange() {
        return exchange;
    }

    /**
     * @param exchange the exchange to set
     */
    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    /**
     * @return the lineNumber
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * @param lineNumber the lineNumber to set
     */
    public void setLineNumber(String lineNumber) {
        this.lineNumber = lineNumber;
    }

    /**
     * @return the selectedType
     */
    public PhoneNumberType getSelectedType() {
        return selectedType;
    }

    /**
     * @param selectedType the selectedType to set
     */
    public void setSelectedType(PhoneNumberType selectedType) {
        this.selectedType = selectedType;
    }

}
