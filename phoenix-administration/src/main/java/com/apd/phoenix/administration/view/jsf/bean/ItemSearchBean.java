package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class ItemSearchBean extends AbstractSearchBean<Item> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemSearchBean.class);
    @Inject
    ItemBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name", "SKU Type", "SKU" };
        return toReturn;
    }

    @Override
    protected List<Item> search() {
        Item search = new Item();
        search.setName(this.getValues().get(0).getValue());
        SkuType skuType = new SkuType();
        skuType.setName(this.getValues().get(1).getValue());
        Sku sku = new Sku();
        sku.setValue(this.getValues().get(2).getValue());
        sku.setType(skuType);

        search.getSkus().add(sku);
        return bp.searchByExample(search, 0, 0);
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Name", "SKU" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(Item searchResult) {
        searchResult = bp.eagerLoad(searchResult);
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        if (searchResult.getSkus() != null) {
            StringBuffer skuString = new StringBuffer();
            for (Sku sku : searchResult.getSkus()) {
                skuString.append("[");
                skuString.append(sku.getType().getName());
                skuString.append(": ");
                skuString.append(sku.getValue());
                skuString.append("]");
            }
            toReturn.put(getColumns()[1], skuString.toString());
        }
        LOGGER.info(toReturn.toString());
        return toReturn;
    }

}
