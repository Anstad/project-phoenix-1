package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PaymentInformationDao;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PaymentInformationSearchBean extends AbstractSearchBean<PaymentInformation> implements Serializable {

    /**
     * Generated serial ID
     */
    private static final long serialVersionUID = -6379624268920244721L;

    @Inject
    PaymentInformationDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = new String[0];
        return toReturn;
    }

    @Override
    protected List<PaymentInformation> search() {
        PaymentInformation search = new PaymentInformation();
        return dao.searchByExample(search, 0, 0);
    }

}
