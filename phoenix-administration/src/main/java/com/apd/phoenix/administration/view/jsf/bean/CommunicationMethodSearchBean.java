package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.CommunicationMethodDao;
import com.apd.phoenix.service.model.CommunicationMethod;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class CommunicationMethodSearchBean extends AbstractSearchBean<CommunicationMethod> implements Serializable {

    @Inject
    CommunicationMethodDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<CommunicationMethod> search() {
        CommunicationMethod search = new CommunicationMethod();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
