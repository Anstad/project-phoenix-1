package com.apd.phoenix.administration.view.jsf.bean.audit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.audit.EntityChange;
import com.apd.phoenix.service.business.AuditLogBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountPropertyType;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.ActionType;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AddressPropertyType;
import com.apd.phoenix.service.model.AddressType;
import com.apd.phoenix.service.model.AttachmentType;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXCategoryXPricingType;
import com.apd.phoenix.service.model.CategoryMarkup;
import com.apd.phoenix.service.model.CommunicationMethod;
import com.apd.phoenix.service.model.ContactMethod;
import com.apd.phoenix.service.model.CostCenter;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CredentialXVendor;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.Department;
import com.apd.phoenix.service.model.Entity;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.model.PasswordResetEntry;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PeriodType;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.PermissionXUser;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.service.model.PoNumberType;
import com.apd.phoenix.service.model.PricingType;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.RoleXUser;
import com.apd.phoenix.service.model.ShippingPartner;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.VendorPropertyType;
import com.apd.phoenix.service.model.VendorXVendorPropertyType;

@Named
@RequestScoped
public class AuditLogBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditLogBean.class);
    private static final String ERROR = "Email address is needed";
    public static final long DAY_IN_MS = 1000 * 60 * 60 * 24;
    private List<Class<? extends Entity>> auditTables;
    private List<EntityChange> results;

    @Inject
    AuditLogBp auditLogBp;

    private Date startDate = new Date();
    private Date endDate = new Date(new Date().getTime() + DAY_IN_MS); //Defaults to tomorrow
    private Class<? extends Entity> clazz;
    private String email;

    @PostConstruct
    public void init() {
        auditTables = new ArrayList<Class<? extends Entity>>();

        auditTables.add(Account.class);
        auditTables.add(AccountXAccountPropertyType.class);
        auditTables.add(AccountXCredentialXUser.class);
        auditTables.add(AccountPropertyType.class);
        auditTables.add(ActionType.class);
        auditTables.add(AddressPropertyType.class);
        auditTables.add(AddressType.class);
        auditTables.add(Address.class);
        auditTables.add(AttachmentType.class);
        auditTables.add(Bulletin.class);
        auditTables.add(CardInformation.class);
        auditTables.add(CashoutPageXField.class);
        auditTables.add(Catalog.class);
        auditTables.add(CatalogXCategoryXPricingType.class);
        auditTables.add(CategoryMarkup.class);
        auditTables.add(CommunicationMethod.class);
        auditTables.add(ContactMethod.class);
        auditTables.add(CostCenter.class);
        auditTables.add(Credential.class);
        auditTables.add(CredentialPropertyType.class);
        auditTables.add(CredentialXCredentialPropertyType.class);
        auditTables.add(CredentialXVendor.class);
        auditTables.add(CxmlConfiguration.class);
        auditTables.add(Department.class);
        auditTables.add(IpAddress.class);
        auditTables.add(ItemPropertyType.class);
        auditTables.add(LimitApproval.class);
        auditTables.add(Manufacturer.class);
        auditTables.add(Matchbook.class);
        auditTables.add(PasswordResetEntry.class);
        auditTables.add(PaymentInformation.class);
        auditTables.add(PaymentType.class);
        auditTables.add(PeriodType.class);
        auditTables.add(PermissionXUser.class);
        auditTables.add(Permission.class);
        auditTables.add(Person.class);
        auditTables.add(PhoneNumberType.class);
        auditTables.add(PhoneNumber.class);
        auditTables.add(PoNumberType.class);
        auditTables.add(PricingType.class);
        auditTables.add(RoleXUser.class);
        auditTables.add(Role.class);
        auditTables.add(ShippingPartner.class);
        auditTables.add(SkuType.class);
        auditTables.add(SystemUser.class);
        auditTables.add(UnitOfMeasure.class);
        auditTables.add(VendorPropertyType.class);
        auditTables.add(VendorXVendorPropertyType.class);
        auditTables.add(Vendor.class);
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void generate() {
        results = auditLogBp.getChangesByDateRange(clazz, startDate, endDate);
    }

    public Class<? extends Entity> getClazz() {
        return clazz;
    }

    public void setClazz(Class<? extends Entity> clazz) {
        this.clazz = clazz;
    }

    public List<Class<? extends Entity>> getAuditTables() {
        return auditTables;
    }

    public void setAuditTables(List<Class<? extends Entity>> auditTables) {
        this.auditTables = auditTables;
    }

    public List<EntityChange> getResults() {
        return results;
    }

    public void setResults(List<EntityChange> results) {
        this.results = results;
    }

    public Converter getClassConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                try {
                    if (value != null) {
                        return Class.forName("com.apd.phoenix.service.model." + value);
                    }
                    else {
                        return null;
                    }
                }
                catch (ClassNotFoundException e) {
                    return null;
                }
            }

            @SuppressWarnings("unchecked")
            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                return getSimpleName((Class<? extends Entity>) value);
            }

        };
    }

    public String getSimpleName(Class<? extends Entity> clazz) {
        String fullClassName = ((Class<? extends Entity>) clazz).getName();
        String simpleName = fullClassName.substring(fullClassName.lastIndexOf(".") + 1, fullClassName.length());
        return simpleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void sendEmail() {
        if (StringUtils.isEmpty(this.email)) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, ERROR, ERROR));
        }
        else {
            auditLogBp.sendEmail(clazz, startDate, endDate, this.email);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Request Processing", "Request Processing"));
        }
    }
}
