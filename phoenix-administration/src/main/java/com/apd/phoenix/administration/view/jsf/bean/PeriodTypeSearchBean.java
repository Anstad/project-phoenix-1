package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.PeriodTypeDao;
import com.apd.phoenix.service.model.PeriodType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class PeriodTypeSearchBean extends AbstractSearchBean<PeriodType> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3789958454021131699L;
    @Inject
    PeriodTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<PeriodType> search() {
        PeriodType search = new PeriodType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
