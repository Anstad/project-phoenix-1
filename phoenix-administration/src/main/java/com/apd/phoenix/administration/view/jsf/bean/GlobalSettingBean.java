package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.GlobalSettingBp;
import com.apd.phoenix.service.model.GlobalSetting;

/**
 * Backing bean for GlobalSetting entities.
 * <p>
 * This class provides CRUD functionality for all GlobalSetting entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class GlobalSettingBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private GlobalSettingBp globalSettingBp;
    @Inject
    private Conversation conversation;

    @Resource
    private SessionContext sessionContext;

    private Long id;
    private GlobalSetting globalSetting;
    private GlobalSetting example = new GlobalSetting();
    private GlobalSetting add = new GlobalSetting();

    private int page;
    private long count;

    private List<GlobalSetting> pageItems;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.globalSetting.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Global Setting to edit");
            message.setDetail("You need to select an Global Setting to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /*
     * Support creating and retrieving GlobalSetting entities
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GlobalSetting getGlobalSetting() {
        return this.globalSetting;
    }

    public String create() {
        this.conversation.begin();
        return "globalSettingAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.globalSetting = this.example;
        }
        else {
            this.globalSetting = globalSettingBp.findById(getId(), GlobalSetting.class);
        }
    }

    public GlobalSetting findById(Long id) {

        return this.globalSettingBp.findById(id, GlobalSetting.class);
    }

    /*
     * Support updating and deleting GlobalSetting entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.globalSetting.getId() == null) {
                this.globalSettingBp.create(this.globalSetting);
                return "globalSettingAdd?faces-redirect=true";
            }
            else {
                this.globalSettingBp.update(this.globalSetting);
                return "globalSettingEdit?faces-redirect=true&id=" + this.globalSetting.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.globalSettingBp.delete(this.globalSetting.getId(), GlobalSetting.class);
            return "globalSettingEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching GlobalSetting entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public GlobalSetting getExample() {
        return this.example;
    }

    public void setExample(GlobalSetting example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<GlobalSetting> searchByExample(GlobalSetting search) {
        return globalSettingBp.searchByExample(search, 0, 0);
    }

    public List<GlobalSetting> searchByString(String s) {
        GlobalSetting search = new GlobalSetting();
        search.setName(s);
        return searchByExample(search);
    }

    public void paginate() {
        this.count = globalSettingBp.resultQuantity(this.example);
        this.pageItems = globalSettingBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    public List<GlobalSetting> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back GlobalSetting entities (e.g. from
     * inside an HtmlSelectOneMenu)
     */

    public List<GlobalSetting> getAll() {
        return globalSettingBp.findAll(GlobalSetting.class, 0, 0);
    }

    public Converter getConverter() {

        final GlobalSettingBean ejbProxy = this.sessionContext.getBusinessObject(GlobalSettingBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((GlobalSetting) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */
    public GlobalSetting getAdd() {
        return this.add;
    }

    public GlobalSetting getAdded() {
        GlobalSetting added = this.add;
        this.add = new GlobalSetting();
        return added;
    }
}