/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Address;

/**
 * @author Red Hat Consulting - Red Hat, Inc.
 * 
 */

@Named("va_messenger")
@Stateless
public class VendorAddressMessenger {

    @Inject
    Event<Address> events;

    public void message() {
        events.fire(new Address());
    }

}
