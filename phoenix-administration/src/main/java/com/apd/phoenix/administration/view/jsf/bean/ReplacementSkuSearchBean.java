package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

@Named
@ConversationScoped
public class ReplacementSkuSearchBean extends SkuSearchBean implements Serializable {

    private static final long serialVersionUID = -7362475408515325617L;

}
