package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.persistence.jpa.CashoutPageXFieldDao;

/**
 * Backing bean for CashoutPageXField entities.
 * <p>
 * This class provides CRUD functionality for all CashoutPageXField entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CashoutPageXFieldBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving CashoutPageXField entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Event Type to edit");
            message.setDetail("You need to select an Event Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private CashoutPageXField currentInstance;

    public CashoutPageXField getCashoutPageXField() {
        return this.currentInstance;
    }

    @Inject
    private CashoutPageXFieldDao dao;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "eventTypeAdd?faces-redirect=true";
    }

    /*
     * Support updating and deleting CashoutPageXField entities
     */

    public String update() {
        this.conversation.end();

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "eventTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "eventTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), CashoutPageXField.class);
            return "eventTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching CashoutPageXField entities with pagination
     */

    private int page;
    private long count;
    private List<CashoutPageXField> pageItems;

    private CashoutPageXField example = new CashoutPageXField();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public CashoutPageXField getExample() {
        return this.example;
    }

    public void setExample(CashoutPageXField example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<CashoutPageXField> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back CashoutPageXField entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<CashoutPageXField> getAll() {

        return dao.findAll(CashoutPageXField.class, 0, 0);
    }

    public List<CashoutPageXField> searchByExample(CashoutPageXField search) {
        return dao.searchByExample(search, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), CashoutPageXField.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((CashoutPageXField) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private CashoutPageXField add = new CashoutPageXField();

    public CashoutPageXField getAdd() {
        return this.add;
    }

    public CashoutPageXField getAdded() {
        CashoutPageXField added = this.add;
        this.add = new CashoutPageXField();
        return added;
    }

}
