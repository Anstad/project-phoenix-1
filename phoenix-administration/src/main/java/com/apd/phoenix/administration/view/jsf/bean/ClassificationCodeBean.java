package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ClassificationCode;
import com.apd.phoenix.service.model.CodeType;

/**
 * Backing bean for ClassificationCode entities.
 * <p>
 * This class provides CRUD functionality for all ClassificationCode entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ClassificationCodeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ClassificationCode entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ClassificationCode classificationCode;

    public ClassificationCode getClassificationCode() {
        return this.classificationCode;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.classificationCode = this.example;
        }
        else {
            this.classificationCode = findById(getId());
        }
    }

    public ClassificationCode findById(Long id) {

        return this.entityManager.find(ClassificationCode.class, id);
    }

    /*
     * Support updating and deleting ClassificationCode entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.classificationCode);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.classificationCode);
                return "view?faces-redirect=true&id=" + this.classificationCode.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ClassificationCode entities with pagination
     */

    private int page;
    private long count;
    private List<ClassificationCode> pageItems;

    private ClassificationCode example = new ClassificationCode();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ClassificationCode getExample() {
        return this.example;
    }

    public void setExample(ClassificationCode example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ClassificationCode> root = countCriteria.from(ClassificationCode.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ClassificationCode> criteria = builder.createQuery(ClassificationCode.class);
        root = criteria.from(ClassificationCode.class);
        TypedQuery<ClassificationCode> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ClassificationCode> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        CodeType codeType = this.example.getCodeType();
        if (codeType != null) {
            predicatesList.add(builder.equal(root.get("codeType"), codeType));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ClassificationCode> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ClassificationCode entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ClassificationCode> getAll() {

        CriteriaQuery<ClassificationCode> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                ClassificationCode.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ClassificationCode.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ClassificationCodeBean ejbProxy = this.sessionContext.getBusinessObject(ClassificationCodeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ClassificationCode) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ClassificationCode add = new ClassificationCode();

    public ClassificationCode getAdd() {
        return this.add;
    }

    public ClassificationCode getAdded() {
        ClassificationCode added = this.add;
        this.add = new ClassificationCode();
        return added;
    }
}