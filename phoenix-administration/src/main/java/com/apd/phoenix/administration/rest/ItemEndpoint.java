package com.apd.phoenix.administration.rest;

import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemBp.NoVendorCatalogSpecifiedException;
import com.apd.phoenix.service.business.ItemRollbackException;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
@Path("/webitems")
public class ItemEndpoint {

    @Inject
    ItemBp itemBp;

    @Inject
    CatalogBp catalogBp;

    /**
     * Creates the.
     * @param itemData 
     * @return the response
     * @throws NoVendorCatalogSpecifiedException 
     * @throws ItemRollbackException 
     */
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    @Path("/create")
    public Response create(Map<String, String> itemData) throws ItemRollbackException,
            NoVendorCatalogSpecifiedException {
        Catalog catalog = null;
        if (itemData.containsKey("catalogId")) {
            catalog = catalogBp.findById(Long.parseLong(itemData.remove("catalogId")), Catalog.class);
        }
        SyncItemResultSummary summary = new SyncItemResultSummary();
        SyncItemResult result = itemBp.syncItem(itemData, catalog, SyncAction.NEW, summary, true);
        if (result.getErrors() != null && !result.getErrors().isEmpty()) {
            return Response.serverError().build();
        }
        return Response.created(
                UriBuilder.fromResource(ItemEndpoint.class).path(String.valueOf((result.getResultApdSku()))).build())
                .build();
    }

    /**
     * Creates or updates the item.
     * @param itemData 
     * @return the response
     * @throws NoVendorCatalogSpecifiedException 
     * @throws ItemRollbackException 
     */
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    @Path("/update")
    public Response update(Map<String, String> itemData) throws ItemRollbackException,
            NoVendorCatalogSpecifiedException {
        Catalog catalog = null;
        if (itemData.containsKey("catalogId")) {
            catalog = catalogBp.findById(Long.parseLong(itemData.remove("catalogId")), Catalog.class);
        }
        SyncItemResultSummary summary = new SyncItemResultSummary();
        SyncItemResult result = itemBp.syncItem(itemData, catalog, SyncAction.UPDATE, summary, true);
        if (result.getErrors() != null && !result.getErrors().isEmpty()) {
            return Response.notModified().build();
        }
        return Response.created(
                UriBuilder.fromResource(ItemEndpoint.class).path(String.valueOf((result.getResultApdSku()))).build())
                .build();
    }

    /**
     * Replaces the item.
     * @param itemData 
     * @return the response
     * @throws NoVendorCatalogSpecifiedException 
     * @throws ItemRollbackException 
     */
    @POST
    @Consumes("application/xml")
    @Produces("application/xml")
    @Path("/replace")
    public Response replace(Map<String, String> itemData) throws ItemRollbackException,
            NoVendorCatalogSpecifiedException {
        Catalog catalog = null;
        if (itemData.containsKey("catalogId")) {
            catalog = catalogBp.findById(Long.parseLong(itemData.remove("catalogId")), Catalog.class);
        }
        SyncItemResultSummary summary = new SyncItemResultSummary();
        SyncItemResult result = itemBp.syncItem(itemData, catalog, SyncAction.REPLACE, summary, true);
        if (result.getErrors() != null && !result.getErrors().isEmpty()) {
            return Response.notModified().build();
        }
        return Response.created(
                UriBuilder.fromResource(ItemEndpoint.class).path(String.valueOf((result.getResultApdSku()))).build())
                .build();
    }

    /**
     * Find by id.
     *
     * @param id the id
     * @return the response
     */
    @GET
    @Path("/{id:[0-9][0-9]*}")
    @Produces("application/xml")
    public Response findById(@PathParam("id") Long id) {
        Item item = this.itemBp.findById(id, Item.class);
        if (item == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(item).build();
    }

    /**
     * Search for item by APD SKU and Vendor Name.
     * @param apdSku 
     * @param vendorName 
     *
     * @return the item
     */
    @GET
    @Produces("application/xml")
    public Response findBySkuAndVendor(@QueryParam("apdsku") String apdSku, @QueryParam("vendor") String vendorName) {
        final Item item = this.itemBp.searchItem(apdSku, vendorName);
        if (item == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok(item).build();
    }

    /**
     * Update.
     *
     * @param id the id
     * @param item 
     * @return the response
     */
    @PUT
    @Path("/{id:[0-9][0-9]*}")
    @Consumes("application/xml")
    public Response update(@PathParam("id") Long id, Item item) {
        this.itemBp.update(item);
        return Response.noContent().build();
    }
}
