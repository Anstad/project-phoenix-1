package com.apd.phoenix.administration.view.jsf.bean;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.SkuBp;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class CatalogXItemSearchBean extends AbstractSearchBean<Object[]> implements Serializable {

    private static final long serialVersionUID = -4773488903760333386L;

    @Inject
    private SkuBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Vendor Catalog", "Customer", "Customer Catalog", "SKU Type", "SKU", "Keyword" };
        return toReturn;
    }

    @Override
    protected List<Object[]> search() {
        return bp.customerSkuList(this.getValues().get(0).getValue(), this.getValues().get(1).getValue(), this
                .getValues().get(2).getValue(), new ArrayList<Long>(), this.getValues().get(3).getValue(), this
                .getValues().get(4).getValue(), this.getValues().get(5).getValue(),
                (this.getPageDisplay().getPage() - 1) * this.getPageDisplay().getSize(), this.getPageDisplay()
                        .getSize());
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Vendor Catalog Name", "Customer", "Customer Catalog", "SKU Type", "SKU", "Name",
                "Calculated Price" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(Object[] searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], ((CatalogXItem) searchResult[0]).getItem().getVendorCatalog().getName());
        toReturn.put(getColumns()[1], ((CatalogXItem) searchResult[0]).getCatalog().getCustomer().getName());
        toReturn.put(getColumns()[2], ((CatalogXItem) searchResult[0]).getCatalog().getName());
        toReturn.put(getColumns()[3], ((Sku) searchResult[1]).getType().getName());
        toReturn.put(getColumns()[4], ((Sku) searchResult[1]).getValue());
        toReturn.put(getColumns()[5], ((CatalogXItem) searchResult[0]).getItem().getName());
        toReturn.put(getColumns()[6], ((CatalogXItem) searchResult[0]).getPrice().toString());
        return toReturn;
    }

    @Override
    public int getResultQuantity() {
        return bp.customerSearchCount(this.getValues().get(0).getValue(), this.getValues().get(1).getValue(), this
                .getValues().get(2).getValue(), new ArrayList<Long>(), this.getValues().get(3).getValue(), this
                .getValues().get(4).getValue(), this.getValues().get(5).getValue());
    }

    @Override
    public File resultsCsv() {
        return this.bp.customerSkuCsv(getColumns(), this.getValues().get(0).getValue(), this.getValues().get(1)
                .getValue(), this.getValues().get(2).getValue(), new ArrayList<Long>(), this.getValues().get(3)
                .getValue(), this.getValues().get(4).getValue(), this.getValues().get(5).getValue());
    }

}
