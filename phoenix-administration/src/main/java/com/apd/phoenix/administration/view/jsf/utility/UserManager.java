package com.apd.phoenix.administration.view.jsf.utility;

import java.io.Serializable;
import java.security.Principal;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Named
@SessionScoped
public class UserManager implements Serializable {

    private static final long serialVersionUID = 109604220750289742L;

    @Inject
    Principal user = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();

    public static String logout() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index.xhtml?faces-redirect=true";
    }
}
