package com.apd.phoenix.administration.view.jsf.bean;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.service.business.CatalogCsvBp;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.business.PricingTypeBp;
import com.apd.phoenix.service.business.SyncItemResultSummaryBp;
import com.apd.phoenix.service.catalog.CatalogResultAggregator;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogCsv;
import com.apd.phoenix.service.model.CatalogXCategoryXPricingType;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.PricingType;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.jpa.CatalogXCategoryXPricingTypeDao;
import com.apd.phoenix.service.utility.FileDownloader;
import com.apd.phoenix.web.AbstractControllerBean;

/**
 * Backing bean for Catalog entities.
 * <p>
 * This class provides CRUD functionality for all Catalog entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class CatalogBean extends AbstractControllerBean<Catalog> implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogBean.class);

    private static final int CONVERSATION_TIMEOUT_MINUTES = 30;

    static {
        uiPanelIdsMap = new HashMap<String, String>();
        uiPanelIdsMap.put(Vendor.class.getName(), "selectVendorModal");
    }

    public static Map<String, String> getUiPanelIdsMap() {
        return uiPanelIdsMap;
    }

    private static final long serialVersionUID = -78054265257038385L;

    /*
     * Support creating and retrieving Catalog entities
     */
    private static final Logger LOG = LoggerFactory.getLogger(CatalogBean.class);

    private boolean isVendorCatalog = true;

    private boolean changeImmediate = false;

    private Catalog parentCatalog;

    private CatalogXItem currentCatalogXItem;

    private Item currentItem;

    private String customerName;

    private SyncAction action = SyncAction.NEW;

    private boolean processingCsv = false;

    private boolean generateCsvOnSave = false;

    private boolean generateSmartOciOnSave = false;

    private boolean reindexOnSave = false;

    public SyncAction getAction() {
        return this.action;
    }

    public void setAction(SyncAction action) {
        this.action = action;
    }

    public void setActionName(String name) {
        switch (name) {
            case "REPLACE":
                this.action = CatalogBp.SyncAction.REPLACE;
                break;
            case "NEW":
                this.action = CatalogBp.SyncAction.NEW;
                break;
            case "UPDATE":
                this.action = CatalogBp.SyncAction.UPDATE;
                break;
            case "CREATE_DIFF":
                this.action = CatalogBp.SyncAction.CREATE_DIFF;
                break;
            default:
                this.action = CatalogBp.SyncAction.NEW;
                break;
        }
    }

    @Inject
    private Conversation conversation;

    @Inject
    private CatalogSearchBean catalogSearchBean;

    @Inject
    private CustomerCatalogSearchBean customerCatalogSearchBean;

    @Inject
    private VendorCatalogSearchBean vendorCatalogSearchBean;

    private Catalog currentInstance = emptyCatalog();

    private Vendor currentVendor;

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private ItemBp itemBp;

    /*
     * Support updating and deleting Catalog entities
     */

    @Resource
    private SessionContext sessionContext;

    /*
     * Support searching Catalog entities with pagination
     */

    @Override
    public String delete() {
        this.conversation.end();

        try {
            catalogBp.delete(this.currentInstance.getId(), Catalog.class);
            return "vendorCatalogEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public Catalog getCatalog() {
        return this.currentInstance;
    }

    /**
     * @return the currentVendor
     */
    public Vendor getCurrentVendor() {
        return currentVendor;
    }

    /**
     * @param currentVendor
     *            the currentVendor to set
     */
    public void setCurrentVendor(Vendor currentVendor) {
        this.currentVendor = currentVendor;
    }

    @Override
    public Converter getConverter() {

        final CatalogBean ejbProxy = this.sessionContext.getBusinessObject(CatalogBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return ejbProxy.catalogBp.findById(Long.valueOf(value), Catalog.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Catalog) value).getId());
            }
        };
    }

    /*
     * Support listing and POSTing back Catalog entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    @Override
    public void retrieve() {
        if (catalogSearchBean.getSelection() != null) {
            this.currentInstance = catalogBp.findById(catalogSearchBean.getSelection().getId(), Catalog.class);
            this.currentInstance = eagerLoad(currentInstance);
            catalogSearchBean.setSelection(null);
        }
        if (customerCatalogSearchBean.getSelection() != null) {
            this.currentInstance = catalogBp.findById(customerCatalogSearchBean.getSelection().getId(), Catalog.class);
            this.currentInstance = eagerLoad(currentInstance);
            customerCatalogSearchBean.setSelection(null);
        }
        if (vendorCatalogSearchBean.getSelection() != null) {
            this.currentInstance = catalogBp.findById(vendorCatalogSearchBean.getSelection().getId(), Catalog.class);
            this.currentInstance = eagerLoad(currentInstance);
            vendorCatalogSearchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
            this.removeTempCsv();
        }
        //timeout is in milliseconds, so multiplies by the number of seconds in a minute,
        //and the number milliseconds in a second
        this.conversation.setTimeout(CONVERSATION_TIMEOUT_MINUTES * 60 * 1000);
    }

    private Catalog eagerLoad(Catalog catalog) {
        return this.catalogBp.eagerLoad(catalog);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        try {
            if (currentInstance.getId() == null) {
                this.currentInstance = catalogBp.create(this.currentInstance);
            }
            else {
                this.currentInstance = catalogBp.update(this.currentInstance);
            }

            this.updatePricing();

            this.currentInstance = eagerLoad(currentInstance);

            StringBuilder toReturn = new StringBuilder();
            toReturn.append(isVendorCatalog ? "vendor" : "customer");
            toReturn.append("Catalog");
            switch (action) {
                case NEW:
                    toReturn.append("Add");
                    break;
                case UPDATE:
                    toReturn.append("Edit");
                    break;
                case REPLACE:
                    this.parentCatalog = eagerLoad(this.parentCatalog);
                    this.currentInstance.setParent(this.parentCatalog);
                    this.parentCatalog.getChildren().add(this.currentInstance);
                    toReturn.append("Replace");
                    break;
                case CREATE_DIFF:
                    this.parentCatalog = eagerLoad(this.parentCatalog);
                    this.currentInstance.setParent(this.parentCatalog);
                    this.currentInstance.setCustomer(this.parentCatalog.getCustomer());
                    this.parentCatalog.getChildren().add(this.currentInstance);
                    toReturn.append("Child");
                    break;
                default:
                    toReturn.append("Edit");
                    break;
            }
            toReturn.append("?faces-redirect=true");

            catalogBp.update(this.currentInstance);

            //If the "Process CSV on save" box is checked, processes the CSV.
            if (this.processingCsv) {
                this.processStream();
                this.currentInstance = catalogBp.update(this.currentInstance);
                if (changeImmediate) {
                    noTransactionBp.changeCatalog(this.currentInstance, true, false);
                }
                else {
                    noTransactionBp.setScheduledAction(this.currentInstance, this.currentInstance.getChangeDate());
                }
            }

            if (generateCsvOnSave) {
                this.generateCatalogCsv();
            }

            if (generateSmartOciOnSave) {
                this.generateSmartOci();
            }

            if (reindexOnSave) {
                this.reindexCatalog();
            }

            if (!this.conversation.isTransient()) {
                this.conversation.end();
            }

            return toReturn.toString();
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            this.currentInstance = eagerLoad(currentInstance);
        }
        return null;
    }

    /**
     * Listen for file uploading event for catalog product upload, and stores the stream on the bean
     */
    public void productUploadListener(FileUploadEvent event) throws Exception {
        this.processingCsv = true;
        this.catalogBp.setTempCsv(event.getUploadedFile().getInputStream(), this.conversation.getId(), this.action);
    }

    /**
     * Stores the stream in the file system.
     */
    private void processStream() {
        LOG.info("reached file upload listener");
        if (this.currentInstance.getId() != null) {
            this.currentInstance = eagerLoad(currentInstance);
        }
        try {
            //Stores the CSV
            this.catalogBp.setDiffCsv(this.conversation.getId(), this.currentInstance, this.action);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            ;
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
        }
    }

    @Inject
    private CatalogBpNoTransaction noTransactionBp;

    /**
     * This method returns the "dry run report" that is produced when this catalog is updated with a CSV.
     * 
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String dryRunReport() throws Exception {
        Catalog newCatalog = catalogBp.findById(this.currentInstance.getId(), Catalog.class);
        if (this.catalogBp.shouldGenerateReport(this.currentInstance)) {
            noTransactionBp.changeCatalog(newCatalog, false, false);
            return "";
        }
        else {
            return FileDownloader.downloadFile(catalogBp.getReportCsv(newCatalog));
        }
    }

    public String downloadStagedCatalog() {
        return FileDownloader.downloadFile(catalogBp.getDiffCsv(this.currentInstance));
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String downloadCatalog() {
        if (!catalogBp.isCatalogCsvExists(this.currentInstance)) {
            this.currentInstance = eagerLoad(currentInstance);
            catalogBp.setCatalogCsv(this.currentInstance);
        }
        InputStream catalogCsv = catalogBp.getCatalogCsv(this.currentInstance);
        return FileDownloader.downloadFile(catalogCsv);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private void generateCatalogCsv() {
        this.catalogBp.setCatalogCsv(this.currentInstance);
    }

    public Catalog getParentCatalog() {
        return parentCatalog;
    }

    public void setParentCatalog(Catalog parentCatalog) {
        this.parentCatalog = parentCatalog;
        this.currentInstance.setVendor(this.parentCatalog.getVendor());
        this.currentInstance.setCustomer(this.parentCatalog.getCustomer());
    }

    public boolean isVendorCatalog() {
        return isVendorCatalog;
    }

    public void setVendorCatalog(boolean isVendorCatalog) {
        this.isVendorCatalog = isVendorCatalog;
    }

    public void setAccount(Account account) {
        Account customer = account.getRootAccount() == null ? account : account.getRootAccount();
        this.currentInstance.setCustomer(customer);
    }

    private boolean validToUpdate() {
        Catalog searchCatalog = new Catalog();
        searchCatalog.setName(this.currentInstance.getName());
        List<Catalog> searchList = this.catalogBp.searchByExactExample(searchCatalog, 0, 0);
        //checks name uniqueness
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A catalog already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        //checks that a vendor or customer is specified
        if (this.currentInstance.getVendor() == null && this.currentInstance.getCustomer() == null) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Customer or Vendor not selected");
            message.setDetail("You need to specify a customer or a vendor for this catalog.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        //if a CSV is being uploaded
        if (this.processingCsv) {
            //checks that the processing date is specified
            if (this.currentInstance.getChangeDate() == null) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Select a date");
                message.setDetail("The date for processing the CSV must selected.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return false;
            }
            //checks that the processing date is in the future
            if (this.currentInstance.getChangeDate().before(new Date()) && !this.getChangeImmediate()) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Select a date in the future");
                message.setDetail("The date for processing the CSV must be in the future.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return false;
            }
        }
        //checks that pricing parameters are set
        for (String categoryName : pricingMap.keySet()) {
            if (pricingMap.get(categoryName) == null || pricingMap.get(categoryName).getParameter() == null
                    || pricingMap.get(categoryName).getType() == null) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Pricing not set for one of the categories");
                message.setDetail("Pricing not set for one of the categories");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return false;
            }
        }
        //checks the formatting for the pricing parameters
        for (String categoryName : pricingMap.keySet()) {
            if (!pricingMap.get(categoryName).getParameter().matches(
                    pricingMap.get(categoryName).getType().getParameterRegEx())) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Pricing parameter for " + categoryName + " doesn't match "
                        + pricingMap.get(categoryName).getType().getParameterRegEx());
                message.setDetail("The pricing parameter for the category \"" + categoryName + "\" doesn't match "
                        + pricingMap.get(categoryName).getType().getParameterRegEx());
                FacesContext.getCurrentInstance().addMessage(null, message);
                return false;
            }
        }
        return true;
    }

    public Item getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(Item currentItem) {
        this.currentItem = itemBp.eagerLoad(currentItem);
    }

    public String vendorTemplate() {
        return FileDownloader.downloadFile(catalogBp.getVendorTemplate());
    }

    public String customerTemplate() {
        return FileDownloader.downloadFile(catalogBp.getCustomerTemplate());
    }

    public CatalogXItem getCurrentCatalogXItem() {
        return currentCatalogXItem;
    }

    public void setCurrentCatalogXItem(CatalogXItem currentCatalogXItem) {
        this.currentCatalogXItem = currentCatalogXItem;
    }

    public boolean getChangeImmediate() {
        return changeImmediate;
    }

    public void setChangeImmediate(boolean isChangeImmediate) {
        this.changeImmediate = isChangeImmediate;
        if (isChangeImmediate) {
            this.currentInstance.setChangeDate(new Date());
        }
    }

    @Inject
    private CatalogXCategoryXPricingTypeDao categoryXPricingDao;

    @Inject
    private ItemCategoryBp categoryBp;

    @Inject
    private PricingTypeBp pricingTypeBp;

    private Map<String, CatalogXCategoryXPricingType> pricingMap;

    private Catalog pricingCatalog;

    public Map<String, CatalogXCategoryXPricingType> getPricingMap() {
        if (this.pricingMap == null || !this.currentInstance.equals(pricingCatalog)) {
            this.pricingCatalog = this.currentInstance;
            pricingMap = new HashMap<>();
            if (this.currentInstance.getId() != null) {
                CatalogXCategoryXPricingType searchPricing = new CatalogXCategoryXPricingType();
                searchPricing.setCatalog(new Catalog());
                searchPricing.getCatalog().setId(this.currentInstance.getId());
                List<CatalogXCategoryXPricingType> pricingList = categoryXPricingDao.searchByExactExample(
                        searchPricing, 0, 0);
                for (CatalogXCategoryXPricingType pricing : pricingList) {
                    pricingMap.put(pricing.getCategory().getName(), pricing);
                }
            }
            PricingType defaultPricingType = pricingTypeBp.getTypeByName(PricingTypeBp.COST_PLUS);
            String defaultPricingParameter = "25";
            for (ItemCategory category : categoryBp.findAll(ItemCategory.class, 0, 0)) {
                if (!pricingMap.containsKey(category.getName())) {
                    CatalogXCategoryXPricingType newPricing = new CatalogXCategoryXPricingType();
                    newPricing.setCatalog(this.currentInstance);
                    newPricing.setCategory(category);
                    newPricing.setType(defaultPricingType);
                    newPricing.setParameter(defaultPricingParameter);
                    pricingMap.put(category.getName(), newPricing);
                }
            }
        }
        return this.pricingMap;
    }

    private void updatePricing() {
        for (String category : getPricingMap().keySet()) {
            CatalogXCategoryXPricingType pricing = getPricingMap().get(category);
            pricing.setCatalog(this.currentInstance);
            if (pricing.getId() != null) {
                categoryXPricingDao.update(pricing);
            }
            else {
                categoryXPricingDao.create(pricing);
            }
        }
    }

    public List<ItemCategory> getAllCategories() {
        return this.categoryBp.findAll(ItemCategory.class, 0, 0);
    }

    private Catalog emptyCatalog() {
        Catalog toReturn = new Catalog();

        Date changeDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(changeDate);
        c.add(Calendar.DATE, 2);
        changeDate = c.getTime();
        toReturn.setChangeDate(changeDate);

        return toReturn;
    }

    public long getCatalogSize() {
        return this.catalogBp.getCatalogSize(this.currentInstance);
    }

    public String getCustomerName() {
        if (customerName == null) {
            Catalog catalog = catalogBp.hydrateCustomerName(currentInstance);
            if (catalog != null && catalog.getCustomer() != null) {
                customerName = catalog.getCustomer().getName();
            }
        }
        return customerName;
    }

    /**
     * Returns true if the catalog should be allowed to be updated
     * 
     * @return
     */
    public boolean isUpdatable(Catalog catalog) {
        if (catalog == null) {
            //if a null catalog, true
            return true;
        }
        if (catalog.getLastProcessStart() == null) {
            //if processing hasn't been started, returns true
            return true;
        }
        if (catalog.getLastProcessEnd() == null) {
            //if the process start date is not null, and the end date hasn't been set, returns false
            return false;
        }
        //returns whether the end date is after the start date
        return catalog.getLastProcessEnd().after(catalog.getLastProcessStart());
    }

    /**
     * Sets the catalog to be updatable
     */
    public String forceEdit() {
        if (this.currentInstance != null) {
            this.currentInstance.setLastProcessEnd(null);
            this.currentInstance.setLastProcessStart(null);
        }
        return null;
    }

    @Inject
    CatalogUploadBp catalogUploadBp;

    public String correlationId(Catalog catalog) {
        return catalogUploadBp.getCorrelationId(catalog);
    }

    public Long totalItems(Catalog catalog) {
        String correlationId = catalogUploadBp.getCorrelationId(catalog);
        return catalogUploadBp.getTotalItems(correlationId);
    }

    @Inject
    SyncItemResultSummaryBp syncItemResultSummaryBp;

    public Long processedItems(Catalog catalog) {
        String correlationId = catalogUploadBp.getCorrelationId(catalog);
        return syncItemResultSummaryBp.getCount(correlationId);
    }

    @Inject
    CatalogCsvBp catalogCsvBp;

    public Long totalRows(Catalog catalog) {
        if (catalog != null) {
            List<String> correlationId = catalogCsvBp.getCorrelationId(catalog.getId());
            if (!correlationId.isEmpty()) {
                return catalogCsvBp.getTotalItems(correlationId.get(0));
            }
        }
        return 1L;
    }

    public Long processedRows(Catalog catalog) {
        if (catalog != null) {
            List<String> correlationId = catalogCsvBp.getCorrelationId(catalog.getId());
            if (!correlationId.isEmpty()) {
                CatalogCsv catalogCsv = catalogCsvBp.getByCorrelationId(correlationId.get(0));
                return catalogCsv.getProcessedItems();
            }
        }
        return 0L;
    }

    @PreDestroy
    protected void destroy() {
        removeTempCsv();
    }

    @Inject
    CatalogResultAggregator catalogResultAggregator;

    public void createCSV(Catalog catalog) {
        LOGGER.info("Starting csv creation manually");
        String correlationId = catalogUploadBp.getCorrelationId(catalog);
        catalogResultAggregator.finished(correlationId);
    }

    public boolean canCreateCSV(Catalog catalog) {
        String correlationId = catalogUploadBp.getCorrelationId(catalog);
        return catalogUploadBp.needsFinishing(correlationId);
    }

    public void removeTempCsv() {
        LOGGER.debug("Deleting temporary CSV");
        this.processingCsv = false;
        catalogBp.deleteTempCsv(conversation.getId());
    }

    private void reindexCatalog() {
        catalogBp.reindexCatalog(this.currentInstance);
    }

    public boolean isGenerateCsvOnSave() {
        return generateCsvOnSave;
    }

    public void setGenerateCsvOnSave(boolean generateCsvOnSave) {
        this.generateCsvOnSave = generateCsvOnSave;
    }

    public boolean isReindexOnSave() {
        return reindexOnSave;
    }

    public void setReindexOnSave(boolean reindexOnSave) {
        this.reindexOnSave = reindexOnSave;
    }

    public boolean isGenerateSmartOciOnSave() {
        return generateSmartOciOnSave;
    }

    public void setGenerateSmartOciOnSave(boolean generateSmartOciOnSave) {
        this.generateSmartOciOnSave = generateSmartOciOnSave;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private void generateSmartOci() {
        this.catalogBp.setSmartOci(this.currentInstance);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String downloadSmartOci() {
        if (!catalogBp.isSmartOciExists(this.currentInstance)) {
            this.currentInstance = eagerLoad(currentInstance);
            catalogBp.setSmartOci(this.currentInstance);
        }
        InputStream catalogCsv = catalogBp.getSmartOci(this.currentInstance);
        return FileDownloader.downloadFile(catalogCsv);
    }
}