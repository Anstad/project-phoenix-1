package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import com.apd.phoenix.service.model.ItemIdentifier;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemIdentifierType;

/**
 * Backing bean for ItemIdentifier entities.
 * <p>
 * This class provides CRUD functionality for all ItemIdentifier entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
@Deprecated
public class ItemIdentifierBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving ItemIdentifier entities
     */

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private ItemIdentifier itemIdentifier;

    public ItemIdentifier getItemIdentifier() {
        return this.itemIdentifier;
    }

    @Inject
    private Conversation conversation;

    private EntityManager entityManager;

    public String create() {

        this.conversation.begin();
        return "create?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.itemIdentifier = this.example;
        }
        else {
            this.itemIdentifier = findById(getId());
        }
    }

    public ItemIdentifier findById(Long id) {

        return this.entityManager.find(ItemIdentifier.class, id);
    }

    /*
     * Support updating and deleting ItemIdentifier entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.id == null) {
                this.entityManager.persist(this.itemIdentifier);
                return "search?faces-redirect=true";
            }
            else {
                this.entityManager.merge(this.itemIdentifier);
                return "view?faces-redirect=true&id=" + this.itemIdentifier.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.entityManager.remove(findById(getId()));
            this.entityManager.flush();
            return "search?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching ItemIdentifier entities with pagination
     */

    private int page;
    private long count;
    private List<ItemIdentifier> pageItems;

    private ItemIdentifier example = new ItemIdentifier();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public ItemIdentifier getExample() {
        return this.example;
    }

    public void setExample(ItemIdentifier example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        // Populate this.count

        CriteriaQuery<Long> countCriteria = builder.createQuery(Long.class);
        Root<ItemIdentifier> root = countCriteria.from(ItemIdentifier.class);
        countCriteria = countCriteria.select(builder.count(root)).where(getSearchPredicates(root));
        this.count = this.entityManager.createQuery(countCriteria).getSingleResult();

        // Populate this.pageItems

        CriteriaQuery<ItemIdentifier> criteria = builder.createQuery(ItemIdentifier.class);
        root = criteria.from(ItemIdentifier.class);
        TypedQuery<ItemIdentifier> query = this.entityManager.createQuery(criteria.select(root).where(
                getSearchPredicates(root)));
        query.setFirstResult(this.page * getPageSize()).setMaxResults(getPageSize());
        this.pageItems = query.getResultList();
    }

    private Predicate[] getSearchPredicates(Root<ItemIdentifier> root) {

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        List<Predicate> predicatesList = new ArrayList<Predicate>();

        String value = this.example.getValue();
        if (value != null && !"".equals(value)) {
            predicatesList.add(builder.like(root.<String> get("value"), '%' + value + '%'));
        }
        Item item = this.example.getItem();
        if (item != null) {
            predicatesList.add(builder.equal(root.get("item"), item));
        }
        ItemIdentifierType itemIdentifierType = this.example.getItemIdentifierType();
        if (itemIdentifierType != null) {
            predicatesList.add(builder.equal(root.get("itemIdentifierType"), itemIdentifierType));
        }

        return predicatesList.toArray(new Predicate[predicatesList.size()]);
    }

    public List<ItemIdentifier> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back ItemIdentifier entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<ItemIdentifier> getAll() {

        CriteriaQuery<ItemIdentifier> criteria = this.entityManager.getCriteriaBuilder().createQuery(
                ItemIdentifier.class);
        return this.entityManager.createQuery(criteria.select(criteria.from(ItemIdentifier.class))).getResultList();
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final ItemIdentifierBean ejbProxy = this.sessionContext.getBusinessObject(ItemIdentifierBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemIdentifier) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private ItemIdentifier add = new ItemIdentifier();

    public ItemIdentifier getAdd() {
        return this.add;
    }

    public ItemIdentifier getAdded() {
        ItemIdentifier added = this.add;
        this.add = new ItemIdentifier();
        return added;
    }
}