package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ItemPropertyTypeDao;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ItemPropertyTypeSearchBean extends AbstractSearchBean<ItemPropertyType> implements Serializable {

    @Inject
    ItemPropertyTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<ItemPropertyType> search() {
        ItemPropertyType search = new ItemPropertyType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
