package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.ItemIdentifierTypeDao;
import com.apd.phoenix.service.model.ItemIdentifierType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class ItemIdentifierTypeSearchBean extends AbstractSearchBean<ItemIdentifierType> implements Serializable {

    @Inject
    ItemIdentifierTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<ItemIdentifierType> search() {
        ItemIdentifierType search = new ItemIdentifierType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
