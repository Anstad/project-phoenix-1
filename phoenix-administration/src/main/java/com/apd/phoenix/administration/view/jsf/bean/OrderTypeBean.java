package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.OrderTypeBp;
import com.apd.phoenix.service.model.OrderType;

/**
 * Backing bean for OrderType entities.
 * <p>
 * This class provides CRUD functionality for all OrderType entities. It
 * focuses purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt>
 * for state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD
 * framework or custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class OrderTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;
    @Inject
    private OrderTypeBp orderTypeBp;
    @Inject
    private Conversation conversation;

    @Resource
    private SessionContext sessionContext;

    private Long id;
    private OrderType orderType;
    private OrderType example = new OrderType();
    private OrderType add = new OrderType();

    private int page;
    private long count;

    private List<OrderType> pageItems;

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.orderType.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Order Type to edit");
            message.setDetail("You need to select an Order Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /*
     * Support creating and retrieving OrderType entities
     */
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderType getOrderType() {
        return this.orderType;
    }

    public String create() {
        this.conversation.begin();
        return "orderTypeAdd?faces-redirect=true";
    }

    public void retrieve() {

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.orderType = this.example;
        }
        else {
            this.orderType = orderTypeBp.findById(getId(), OrderType.class);
        }
    }

    public OrderType findById(Long id) {

        return this.orderTypeBp.findById(id, OrderType.class);
    }

    /*
     * Support updating and deleting OrderType entities
     */

    public String update() {
        this.conversation.end();

        try {
            if (this.orderType.getId() == null) {
                this.orderTypeBp.create(this.orderType);
                return "orderTypeAdd?faces-redirect=true";
            }
            else {
                this.orderTypeBp.update(this.orderType);
                return "orderTypeEdit?faces-redirect=true&id=" + this.orderType.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            this.orderTypeBp.delete(this.orderType.getId(), OrderType.class);
            return "orderTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching OrderType entities with pagination
     */
    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public OrderType getExample() {
        return this.example;
    }

    public void setExample(OrderType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public List<OrderType> searchByExample(OrderType search) {
        return orderTypeBp.searchByExample(search, 0, 0);
    }

    public List<OrderType> searchByString(String s) {
        OrderType search = new OrderType();
        search.setValue(s);
        return searchByExample(search);
    }

    public void paginate() {
        this.count = orderTypeBp.resultQuantity(this.example);
        this.pageItems = orderTypeBp.searchByExample(this.example, getPage() * getPageSize(), getPageSize());
    }

    public List<OrderType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back OrderType entities (e.g. from
     * inside an HtmlSelectOneMenu)
     */

    public List<OrderType> getAll() {
        return orderTypeBp.findAll(OrderType.class, 0, 0);
    }

    public Converter getConverter() {

        final OrderTypeBean ejbProxy = this.sessionContext.getBusinessObject(OrderTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                return ejbProxy.findById(Long.valueOf(value));
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((OrderType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */
    public OrderType getAdd() {
        return this.add;
    }

    public OrderType getAdded() {
        OrderType added = this.add;
        this.add = new OrderType();
        return added;
    }
}