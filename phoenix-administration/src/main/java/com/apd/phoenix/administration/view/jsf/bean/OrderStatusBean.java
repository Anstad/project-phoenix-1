package com.apd.phoenix.administration.view.jsf.bean;

import com.apd.phoenix.service.model.OrderStatus;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.OrderStatusDao;

/**
 * Backing bean for OrderStatus entities.
 * <p>
 * This class provides CRUD functionality for all OrderStatus entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class OrderStatusBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving OrderStatus entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Order Status to edit");
            message.setDetail("You need to select an Order Status to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private OrderStatus currentInstance;

    public OrderStatus getOrderStatus() {
        return this.currentInstance;
    }

    @Inject
    private OrderStatusDao dao;

    @Inject
    private Conversation conversation;

    public String create() {
        this.conversation.begin();
        return "orderStatusAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, OrderStatus.class);
        }

    }

    /*
     * Support updating and deleting OrderStatus entities
     */

    public String update() {
        this.conversation.end();

        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "orderStatusAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "orderStatusEdit?faces-redirect=true&id=" + currentInstance.getId();
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), OrderStatus.class);
            return "orderStatusEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching OrderStatus entities with pagination
     */

    private int page;
    private long count;
    private List<OrderStatus> pageItems;

    private OrderStatus example = new OrderStatus();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public OrderStatus getExample() {
        return this.example;
    }

    public void setExample(OrderStatus example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<OrderStatus> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back OrderStatus entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<OrderStatus> getAll() {

        return dao.findAll(OrderStatus.class, 0, 0);
    }

    public List<OrderStatus> searchByExample(OrderStatus search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<OrderStatus> searchByString(String s) {
        OrderStatus search = new OrderStatus();
        search.setValue(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final OrderStatusBean ejbProxy = this.sessionContext.getBusinessObject(OrderStatusBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {

                ejbProxy.setId(Long.valueOf(value));
                ejbProxy.retrieve();
                return ejbProxy.getOrderStatus();
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((OrderStatus) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private OrderStatus add = new OrderStatus();

    public OrderStatus getAdd() {
        return this.add;
    }

    public OrderStatus getAdded() {
        OrderStatus added = this.add;
        this.add = new OrderStatus();
        return added;
    }
}