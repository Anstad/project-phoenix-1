package com.apd.phoenix.administration.view.jsf.bean;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.HierarchyNodeBp;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;

@Named
@ConversationScoped
public class HierarchyNodeSearchBean extends AbstractSearchBean<HierarchyNode> implements Serializable {

    private static final long serialVersionUID = -7794202530278440044L;
    @Inject
    HierarchyNodeBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Description", "Public ID" };
        return toReturn;
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "Description", "Public ID", "Version", "Hierarchy Path" };
        return toReturn;
    }

    @Override
    protected List<HierarchyNode> search() {
        HierarchyNode search = new HierarchyNode();
        search.setDescription(this.getValues().get(0).getValue());
        if (!StringUtils.isEmpty(this.getValues().get(1).getValue())) {
            search.setPublicId(Long.valueOf(this.getValues().get(1).getValue()));
        }
        return bp.searchByExample(search, (this.getPageDisplay().getPage() - 1) * this.getPageDisplay().getSize(), this
                .getPageDisplay().getSize());
    }

    @Override
    public Map<String, String> resultRow(HierarchyNode searchResult) {
        searchResult = bp.findById(searchResult.getId(), HierarchyNode.class);
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getDescription());
        toReturn.put(getColumns()[1], "" + searchResult.getPublicId());
        toReturn.put(getColumns()[2], "" + searchResult.getVersion());
        toReturn.put(getColumns()[3], "" + searchResult.getIndexedPaths());
        return toReturn;
    }

    @Override
    public int getResultQuantity() {
        HierarchyNode search = new HierarchyNode();
        search.setDescription(this.getValues().get(0).getValue());
        if (!StringUtils.isEmpty(this.getValues().get(1).getValue())) {
            search.setPublicId(Long.valueOf(this.getValues().get(1).getValue()));
        }
        return (int) bp.resultQuantity(search);
    }

    @Override
    public File resultsCsv() {
        HierarchyNode search = new HierarchyNode();
        search.setDescription(this.getValues().get(0).getValue());
        if (!StringUtils.isEmpty(this.getValues().get(1).getValue())) {
            search.setPublicId(Long.valueOf(this.getValues().get(1).getValue()));
        }
        List<Object[]> results = new ArrayList<Object[]>();
        for (HierarchyNode result : bp.searchByExample(search, 0, 0)) {
            Object[] row = new Object[this.getColumns().length];
            for (int i = 0; i < this.getColumns().length; i++) {
                row[i] = this.getCell(result, this.getColumns()[i]);
            }
            results.add(row);
        }
        return resultsCsv.getFile(this.getColumns(), results);
    }

}
