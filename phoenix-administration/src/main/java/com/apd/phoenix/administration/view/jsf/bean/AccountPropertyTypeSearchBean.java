package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.AccountPropertyTypeDao;
import com.apd.phoenix.service.model.AccountPropertyType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class AccountPropertyTypeSearchBean extends AbstractSearchBean<AccountPropertyType> implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7157409480337986846L;

    @Inject
    AccountPropertyTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<AccountPropertyType> search() {
        AccountPropertyType search = new AccountPropertyType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);

    }

}
