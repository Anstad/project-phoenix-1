/**
 * 
 */
package com.apd.phoenix.administration.view.jsf.bean.event;

import java.io.Serializable;
import com.apd.phoenix.service.model.PhoneNumber;

/**
 * @author Red Hat Consulting, 2013
 *
 */
public class VendorPhoneNumberEvent extends PhoneNumberEvent implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6636143921900644995L;

    public PhoneNumber newPhoneNumber;

    public VendorPhoneNumberEvent(PhoneNumber phone) {
        setNewPhoneNumber(phone);
    }

    /**
     * @return the newPhoneNumber
     */
    public PhoneNumber getNewPhoneNumber() {
        return newPhoneNumber;
    }

    /**
     * @param newPhoneNumber the newPhoneNumber to set
     */
    public void setNewPhoneNumber(PhoneNumber newPhoneNumber) {
        this.newPhoneNumber = newPhoneNumber;
    }

}
