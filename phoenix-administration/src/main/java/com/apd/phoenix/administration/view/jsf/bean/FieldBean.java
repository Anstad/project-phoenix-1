package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.model.Field;
import com.apd.phoenix.service.model.FieldOptions;
import com.apd.phoenix.service.persistence.jpa.FieldDao;

/**
 * Backing bean for Field entities.
 * <p>
 * This class provides CRUD functionality for all Field entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class FieldBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Field currentInstance = new Field();

    public Field getField() {
        return this.currentInstance;
    }

    public void setField(Field field) {
        this.currentInstance = field;
    }

    // The Field currently being edited.
    private FieldOptions currentFieldOptions = new FieldOptions();

    public FieldOptions getCurrentFieldOptions() {
        return this.currentFieldOptions;
    }

    public void setCurrentFieldOptions(FieldOptions property) {
        this.currentFieldOptions = property;
    }

    /**
     * This method is called when the user presses the "New Property" button, and creates 
     * a new Property to modify.
     */
    public void createFieldOptions() {
        this.currentFieldOptions = new FieldOptions();
    }

    /**
     * Removes the selected property from the set of properties on the current Field instance.
     * 
     * @param toRemove
     */
    public void removeFieldOptions(FieldOptions toRemove) {
        this.currentInstance.getOptions().remove(toRemove);
    }

    /**
     * Saves the selected property onto the current Field instance.
     */
    public void saveFieldOptions() {
        this.currentInstance.getOptions().add(this.currentFieldOptions);
    }

    @Inject
    private FieldDao dao;

    @Inject
    private Conversation conversation;

    @Inject
    private FieldSearchBean searchBean;

    public String create() {
        this.conversation.begin();
        return "fieldAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (searchBean.getSelection() != null) {
            this.currentInstance = dao.findById(searchBean.getSelection().getId(), Field.class);
            eagerLoad();
            searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

    }

    /*
     * Support updating and deleting Field entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }
        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }

        try {

            if (currentInstance.getId() == null) {
                this.currentInstance = dao.update(this.currentInstance);
                return "fieldAdd?faces-redirect=true";
            }
            else {
                this.currentInstance = dao.update(this.currentInstance);
                eagerLoad();
                return "fieldEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();

        try {
            dao.delete(this.currentInstance.getId(), Field.class);
            return "fieldEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching Field entities with pagination
     */

    private int page;
    private long count;
    private List<Field> pageItems;

    private Field example = new Field();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public Field getExample() {
        return this.example;
    }

    public void setExample(Field example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<Field> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back Field entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<Field> getAll() {

        return dao.findAll(Field.class, 0, 0);
    }

    public List<Field> searchByExample(Field search) {
        return dao.searchByExample(search, 0, 0);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), Field.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Field) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private Field add = new Field();

    public Field getAdd() {
        return this.add;
    }

    public Field getAdded() {
        Field added = this.add;
        this.add = new Field();
        return added;
    }

    private void eagerLoad() {
        this.currentInstance.getOptions().size();
    }

    private boolean validToUpdate() {
        Field searchField = new Field();
        searchField.setName(this.currentInstance.getName());
        List<Field> searchList = this.dao.searchByExactExample(searchField, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("A field already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }

}