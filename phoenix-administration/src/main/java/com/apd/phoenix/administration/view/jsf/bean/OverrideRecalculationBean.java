package com.apd.phoenix.administration.view.jsf.bean;

import java.io.Serializable;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;

@Named
@Stateless
public class OverrideRecalculationBean implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Inject
    private CatalogXItemBp bp;

    private static final Logger LOG = LoggerFactory.getLogger(OverrideRecalculationBean.class);

    public boolean canRecalculateOverrides() {
        //returns true, assuming that if the user is in admin and viewing this page, they 
        //can start this large database action
        return true;
    }

    public void recalculateOverrides() {
        LOG.warn("Recalculating overrides");
        bp.calculateAllOverrides();
    }
}