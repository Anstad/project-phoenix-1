package com.apd.phoenix.administration.view.jsf.bean;

import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.FieldTypeDao;
import com.apd.phoenix.service.model.FieldType;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class FieldTypeSearchBean extends AbstractSearchBean<FieldType> implements Serializable {

    @Inject
    FieldTypeDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<FieldType> search() {
        FieldType search = new FieldType();
        search.setName(this.getValues().get(0).getValue());
        return dao.searchByExample(search, 0, 0);
    }

}
