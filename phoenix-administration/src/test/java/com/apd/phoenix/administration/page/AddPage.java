package com.apd.phoenix.administration.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.jboss.arquillian.graphene.Graphene.*;
import com.apd.phoenix.administration.fragment.NavigationBar;

public class AddPage {

    //XPath example
    //    private static final By LOGGED_IN = By.xpath("//li[contains(text(),'Welcome')]");
    //    private static final By LOGGED_OUT = By.xpath("//li[contains(text(),'Goodbye')]");
    // 

    //	 private static final By NAME_FIELD = By.id("credentialPropertyTypeAddForm:credentialPropertyTypeBeanCredentialPropertyTypeName");
    //   private static final By ADD_BUTTON = By.id("credentialPropertyTypeAddForm:save");

    @FindBy(id = "navigationForm:administrationNavigation")
    private NavigationBar navigationBar;

    @FindBy(id = "credentialPropertyTypeAddForm:credentialPropertyTypeBeanCredentialPropertyTypeName")
    private WebElement nameInput;

    @FindBy(id = "credentialPropertyTypeAddForm:save")
    private WebElement saveButton;

    //Adding the record to the database
    public void persistRecord() {
        this.saveButton.click();

        //Long startTimeInMillis = System.currentTimeMillis();

        //By waiting till NameInput is empty, we ensure that the record persisted.
        waitModel().until().element(nameInput).value().equalTo("");

        //Long endTimeInMillis = System.currentTimeMillis();
        //System.out.println("Time Taken for Save: " + (endTimeInMillis - startTimeInMillis));
    }

    //Sending a String to the Input field
    public void enterName(String name) {

        //System.out.println("Typing the name:" + name);
        this.nameInput.clear();
        this.nameInput.sendKeys(name);
    }

    //getters for all fields of this Page Object
    public WebElement getNameInput() {
        return nameInput;
    }

    public void setNameInput(WebElement nameInput) {
        this.nameInput = nameInput;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(WebElement saveButton) {
        this.saveButton = saveButton;
    }

    public NavigationBar getNavigationBar() {
        return navigationBar;
    }

    public void setNavigationBar(NavigationBar navigationBar) {
        this.navigationBar = navigationBar;
    }

}