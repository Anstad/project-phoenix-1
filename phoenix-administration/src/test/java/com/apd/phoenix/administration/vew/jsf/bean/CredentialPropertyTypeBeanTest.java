package com.apd.phoenix.administration.vew.jsf.bean;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.graphene.spi.annotations.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.persistence.jpa.AbstractDao;
import com.apd.phoenix.service.persistence.jpa.CredentialPropertyTypeDao;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import com.apd.phoenix.administration.page.AddPage;
import com.apd.phoenix.administration.page.EditPage;
import com.apd.phoenix.administration.view.jsf.bean.CredentialPropertyTypeBean;
import com.apd.phoenix.administration.view.jsf.bean.CredentialPropertyTypeSearchBean;
import com.apd.phoenix.administration.view.jsf.bean.ViewUtils;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.thoughtworks.selenium.DefaultSelenium;

@RunWith(Arquillian.class)
public class CredentialPropertyTypeBeanTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialPropertyTypeBeanTest.class);
    private static final String WEBAPP_SRC = "src/main/webapp";
    private static final String PAGE_ROOT_NAME = "credentialPropertyType";

    @Deployment(testable = false)
    public static WebArchive createDeployment() {

        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        WebArchive wa = ShrinkWrap.create(WebArchive.class, "test.war").addClasses(Dao.class, AbstractDao.class,
                CredentialPropertyTypeDao.class).addClass(CredentialPropertyType.class).addClasses(
                AbstractControllerBean.class, CredentialPropertyTypeBean.class).addClasses(AbstractSearchBean.class,
                CredentialPropertyTypeSearchBean.class).addClass(ViewUtils.class).addAsLibraries(
                resolver.artifacts("commons-lang:commons-lang", "org.richfaces.ui:richfaces-components-ui",
                        "org.richfaces.core:richfaces-core-impl").resolveAsFiles()).merge(
                ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class).importDirectory(WEBAPP_SRC).as(
                        GenericArchive.class), "/", Filters.includeAll()).addAsResource("test-persistence.xml",
                "META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsWebInfResource(
                new StringAsset("<faces-config version=\"2.0\"/>"), "faces-config.xml");

        LOGGER.info("**Archive follows**");
        LOGGER.info(wa.toString(Formatters.VERBOSE));
        LOGGER.info("**Archive above**");
        return wa;
    }

    @Before
    public void prepareViewTest() throws Exception {

        driver.get(deploymentUrl + "admin/" + PAGE_ROOT_NAME + "/" + PAGE_ROOT_NAME + "Add.xhtml");

        //+ "admin/credentialPropertyType/credentialPropertyTypeAdd.xhtml");

    }

    @Drone
    WebDriver driver;

    @ArquillianResource
    URL deploymentUrl;

    @Page
    AddPage addPage;

    @Page
    EditPage editPage;

    @Test
    public void visitPageTest() throws InterruptedException {

        String Title = "Phoenix Administration - Data Administration";
        String pageTitle = driver.getTitle();

        LOGGER.info("Page Title is: " + pageTitle);

        Assert.assertEquals(Title, pageTitle);
    }

    @Test
    public void addPageTest() throws InterruptedException {

        String testName = "demo";

        //		WebElement element = driver.findElement(By.id("credentialPropertyTypeAddForm:credentialPropertyTypeBeanCredentialPropertyTypeName"));
        //		System.out.println("Element is::  Displayed:" + element.isDisplayed() +" Enabled:" + element.isEnabled() + " Selected:" + element.isSelected());

        //Testing Graphene Wait Util and SearchTable's search funtions with multiple names.
        //		addPage.enterName("Frank");
        //		addPage.persistRecord();
        //		
        //		addPage.enterName("Bob");
        //		addPage.persistRecord();

        addPage.enterName(testName);
        addPage.persistRecord();

        //		addPage.enterName("Judy");
        //		addPage.persistRecord();
        //		
        //		addPage.enterName("Bilbo");
        //		addPage.persistRecord();

        //Navigate to the corresponding Edit page
        WebElement mainNavDiv = addPage.getNavigationBar().findHeading("systemData");
        mainNavDiv.click();

        WebElement subNavDiv = addPage.getNavigationBar().findSubHeading(PAGE_ROOT_NAME + "Group:hdr");
        subNavDiv.click();

        WebElement editPageLink = addPage.getNavigationBar().findNavLink(PAGE_ROOT_NAME + "EditLink");
        editPageLink.click();

        //Return all results.  
        //editPage.getSearchTable().search("");

        //Return the single result we are looking for.
        editPage.getSearchTable().search(testName);

        //		System.out.println("****Current List Results (Total = " + editPage.getSearchTable().getResultList().size() +"):");

        //		for (int i=0; i<editPage.getSearchTable().getResultList().size(); i ++){
        //			
        //			System.out.println("Entire Row" + i + ":\n"); 
        //			System.out.println(editPage.getSearchTable().getResultList().get(i).getText() + "\n");
        //			
        //			//System.out.println("\nName Value:" + i + ":" + editPage.getSearchTable().getResultListValues().get(i).getText());
        //			
        //			System.out.println("\nLink Value:" + i + ":" + editPage.getSearchTable().getResultListLinks().get(i).getText());
        //			
        //		}

        int searchIndex = editPage.getSearchTable().findRowIndexByValue(testName);
        editPage.getSearchTable().clickRow(searchIndex);

        LOGGER.info("Answer is:" + editPage.getNameInput().getAttribute("value"));

        Assert.assertEquals(testName, editPage.getNameInput().getAttribute("value"));
    }
}
