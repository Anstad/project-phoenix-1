package com.apd.phoenix.administration.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.administration.fragment.NavigationBar;
import com.apd.phoenix.administration.fragment.SearchTable;

public class EditPage {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditPage.class);
    //    private static final By LOGGED_IN = By.xpath("//li[contains(text(),'Welcome')]");
    //    private static final By LOGGED_OUT = By.xpath("//li[contains(text(),'Goodbye')]");

    //	private static final By NAME_FIELD = By.id("credentialPropertyTypeAddForm:credentialPropertyTypeBeanCredentialPropertyTypeName");
    //    private static final By ADD_BUTTON = By.id("credentialPropertyTypeAddForm:save");

    @FindBy(id = "navigationForm:administrationNavigation")
    private NavigationBar navigationBar;

    @FindBy(id = "credentialPropertyTypeEditForm:credentialPropertyTypeSearch:searchCredentialPropertyType")
    private SearchTable searchTable;

    @FindBy(id = "credentialPropertyTypeEditForm:credentialPropertyTypeBeanCredentialPropertyTypeName")
    private WebElement nameInput;

    @FindBy(id = "credentialPropertyTypeEditForm:save")
    private WebElement saveButton;

    @FindBy(id = "credentialPropertyTypeEditForm:deleteButton")
    private WebElement deleteButton;

    //Sending a String to the Input field
    public void changeName(String name) {

        this.nameInput.clear();

        LOGGER.info("Typing the name:" + name);
        this.nameInput.sendKeys(name);
    }

    //Adding or Updating the record in the database
    public void persistRecord() {
        this.saveButton.click();
    }

    //Removing the currently selected record from the database
    public void deleteRecord() {
        this.deleteButton.click();
    }

    //getters for all fields of this Page Object
    public WebElement getNameInput() {
        return nameInput;
    }

    public void setNameInput(WebElement nameInput) {
        this.nameInput = nameInput;
    }

    public WebElement getSaveButton() {
        return saveButton;
    }

    public void setSaveButton(WebElement saveButton) {
        this.saveButton = saveButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(WebElement deleteButton) {
        this.deleteButton = deleteButton;
    }

    public SearchTable getSearchTable() {
        return searchTable;
    }

    public void setSearchTable(SearchTable searchTable) {
        this.searchTable = searchTable;
    }

    public NavigationBar getNavigationBar() {
        return navigationBar;
    }

    public void setNavigationBar(NavigationBar navigationBar) {
        this.navigationBar = navigationBar;
    }
}