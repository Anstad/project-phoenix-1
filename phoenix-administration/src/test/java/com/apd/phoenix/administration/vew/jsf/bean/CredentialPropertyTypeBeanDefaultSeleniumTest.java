package com.apd.phoenix.administration.vew.jsf.bean;

import java.net.URL;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.administration.view.jsf.bean.CredentialPropertyTypeBean;
import com.apd.phoenix.administration.view.jsf.bean.CredentialPropertyTypeSearchBean;
import com.apd.phoenix.administration.view.jsf.bean.ViewUtils;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.persistence.jpa.AbstractDao;
import com.apd.phoenix.service.persistence.jpa.CredentialPropertyTypeDao;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.web.AbstractControllerBean;
import com.apd.phoenix.web.searchbeans.AbstractSearchBean;
import com.thoughtworks.selenium.DefaultSelenium;

@RunWith(Arquillian.class)
public class CredentialPropertyTypeBeanDefaultSeleniumTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialPropertyTypeBeanDefaultSeleniumTest.class);
    private static final String WEBAPP_SRC = "src/main/webapp";

    @Deployment(testable = false)
    public static WebArchive createDeployment() {

        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        WebArchive wa = ShrinkWrap.create(WebArchive.class, "test.war").addClasses(Dao.class, AbstractDao.class,
                CredentialPropertyTypeDao.class).addClass(CredentialPropertyType.class).addClasses(
                AbstractControllerBean.class, CredentialPropertyTypeBean.class).addClasses(AbstractSearchBean.class,
                CredentialPropertyTypeSearchBean.class).addClass(ViewUtils.class).addAsLibraries(
                resolver.artifacts("commons-lang:commons-lang", "org.richfaces.ui:richfaces-components-ui",
                        "org.richfaces.core:richfaces-core-impl").resolveAsFiles())
        //				.addAsWebResource(
                //						new File(WEBAPP_SRC,
                //								"admin/credentialPropertyType/credentialPropertyTypeAdd.xhtml"))
                //								.addAsWebResource(
                //						new File(WEBAPP_SRC,
                //								"admin/credentialPropertyType/credentialPropertyTypeEdit.xhtml"))								
                //				.addAsWebResource(
                //						new File(WEBAPP_SRC,
                //								"WEB-INF/classes/META-INF/forge.taglib.xml"))
                .merge(
                        ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class).importDirectory(WEBAPP_SRC)
                                .as(GenericArchive.class), "/", Filters.includeAll()).addAsResource(
                        "test-persistence.xml", "META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE,
                        "beans.xml").addAsWebInfResource(new StringAsset("<faces-config version=\"2.0\"/>"),
                        "faces-config.xml");

        LOGGER.info("**Archive follows**");
        LOGGER.info(wa.toString(Formatters.VERBOSE));
        LOGGER.info("**Archive above**");
        return wa;
    }

    @Before
    public void prepareViewTest() throws Exception {

    }

    @Drone
    DefaultSelenium browser;

    @ArquillianResource
    URL deploymentUrl;

    @Test
    public void visitPageTest() throws InterruptedException {

        String TestName = "demo";

        browser.open(deploymentUrl + "admin/credentialPropertyType/credentialPropertyTypeAdd.xhtml");

        browser.type("id=credentialPropertyTypeAddForm:credentialPropertyTypeBeanCredentialPropertyTypeName", TestName);
        browser.click("id=credentialPropertyTypeAddForm:save");
        browser.waitForPageToLoad("15000");

        //browser.click("id=navigationForm:credentialPropertyTypeGroup:expanded");
        browser.click("id=navigationForm:credentialPropertyTypeEditLink");

        browser.waitForPageToLoad("15000");

        for (int i = 0; i < browser.getAllLinks().length; i++) {
            String currLink = browser.getAllLinks()[i];

            if (currLink.contains("credentialPropertyTypeEditForm:credentialPropertyTypeSearch")) {
                LOGGER.info("FOUND Search Link:" + currLink);
                browser.click(currLink);
                break;
            }
        }

        //TODO: Temp Solution.  Need to rework this to use Graphene and Web Driver vs Selenium
        int second = 0;
        while (!browser.isElementPresent("credentialPropertyTypeEditForm:credentialPropertyTypeSearch:resultList:b")) {
            if (second >= 5)
                break;
            Thread.sleep(1000);
            second++;
        }

        //List<String> stringList = new ArrayList<String>();

        //stringList = (ArrayList<String>) Arrays.asList(browser.getAllLinks());

        //Collections.addAll(stringList, allLinks); 

        for (int i = 0; i < browser.getAllLinks().length; i++) {
            String currLink = browser.getAllLinks()[i];
            if (currLink.contains("credentialPropertyTypeEditForm:credentialPropertyTypeSearch:resultList")) {
                LOGGER.info("FOUND!:" + currLink);
                //stringList.add(currLink);
                browser.click(currLink);
                break;
            }

        }

        String credPropTypeEditName = "credentialPropertyTypeEditForm:credentialPropertyTypeBeanCredentialPropertyTypeName";

        //TODO: Temp Solution.  Need to rework this to use Graphene and Web Driver vs Selenium
        second = 0;
        while (browser.getValue(credPropTypeEditName) == null || browser.getValue(credPropTypeEditName).isEmpty()) {
            if (second >= 5)
                break;
            Thread.sleep(1000);
            second++;
        }

        LOGGER.info("Selected Value is: " + browser.getValue(credPropTypeEditName));

        Assert.assertEquals(TestName, browser.getValue(credPropTypeEditName));
    }
}
