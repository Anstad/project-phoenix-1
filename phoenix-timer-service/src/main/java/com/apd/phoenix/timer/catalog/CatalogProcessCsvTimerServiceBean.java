package com.apd.phoenix.timer.catalog;

import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;
import org.jboss.ejb3.annotation.TransactionTimeout;

/**
 * @author <a href="mailto:wfink@redhat.com">Wolf-Dieter Fink</a>
 */
@Named
@Singleton
public class CatalogProcessCsvTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogProcessCsvTimerServiceBean.class);

    @Inject
    private CatalogBpNoTransaction catalogBp;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("catalogprocesscsv", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        Properties properties = PropertiesLoader.getAsProperties("catalog.upload.integration");
        ScheduleExpression se = new ScheduleExpression();
        se.hour(properties.getProperty("csvTimerHour")).minute(properties.getProperty("csvTimerMinute")).second(
                properties.getProperty("csvTimerSecond"));
        return se;
    }

    @Timeout
    @TransactionTimeout(value = 7200)
    public void timeout(Timer timer) {
        LOGGER.info("CSV Processing Timeout started!");
        catalogBp.executeScheduledActions();
        LOGGER.info("CSV Processing Timeout ended!");
    }
}