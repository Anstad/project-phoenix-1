/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.awaitingapproval;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.email.impl.ApproveOrderReminderEmailTemplate;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.impl.DenyOrderMessageSender;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author nreidelb
 */
@Named
@Singleton
public class AwaitingApprovalTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwaitingApprovalTimerServiceBean.class);

    private static final int TIMEOUT_IN_HOURS = 12;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private DenyOrderMessageSender messageSender;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    MessageService messageService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("quickstart", "awaitingApprovalTimerService", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        // Run at 3:00AM
        se.hour("3").minute("0").second("0");
        return se;
    }

    @Timeout
    @TransactionTimeout(value = (60 * 60 * TIMEOUT_IN_HOURS))
    public void timeout(Timer timer) {
        List<CustomerOrder> orders = customerOrderBp.getOrdersPendingApproval();
        LOGGER.info("found: " + orders.size() + " Orders Pending Approval");
        for (CustomerOrder order : orders) {

            Date dateRequestedApproval = customerOrderBp.getDateRequestedApproval(order);
            if (dateRequestedApproval == null) {
                LOGGER
                        .error("No pending approval order log found for order with APD PO " + order.getApdPo() == null ? "unknown"
                                : order.getApdPo().getValue());
                return;
            }
            Calendar dateOrderStartedAwaitingApproval = Calendar.getInstance();
            dateOrderStartedAwaitingApproval.setTime(dateRequestedApproval);

            Calendar tenDaysAgo = Calendar.getInstance();
            tenDaysAgo.add(Calendar.DAY_OF_YEAR,
                    -ApproveOrderReminderEmailTemplate.DAYS_UNTIL_ORDER_IS_DENIED_AUTOMATICALLY);
            //If ten days ago is after the date that the order first needed approval deny the order
            if (tenDaysAgo.after(dateOrderStartedAwaitingApproval)) {
                messageSender.sendDenyOrder(order);
            }
            else {
                sendApproveOrderReminder(order);
            }
        }
    }

    public void sendApproveOrderReminder(CustomerOrder order) {
        order = customerOrderBp.findById(order.getId(), CustomerOrder.class);
        List<Message> emails = emailFactoryBp.createOrderEmails(order.getId(), null, EventType.APPROVE_ORDER_REMINDER);
        for (Message message : emails) {
            if (message != null) {
                messageService.sendOrderMessage(order, message, EventType.APPROVE_ORDER_REMINDER);
            }
            else {
                LOGGER.error("Tried send an email with a null message");
            }
        }

    }
}
