package com.apd.phoenix.timer.catalogupload;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Startup
public class CatalogUploadServiceActivator extends AbstractTimerServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogUploadServiceActivator.class);

    @EJB
    private CatalogUploadServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
