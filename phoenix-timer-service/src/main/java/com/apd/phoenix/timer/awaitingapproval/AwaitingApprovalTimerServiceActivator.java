/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.awaitingapproval;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author nreidelb
 */
@Singleton
@Startup
public class AwaitingApprovalTimerServiceActivator extends AbstractTimerServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(AwaitingApprovalTimerServiceActivator.class);

    @EJB
    private AwaitingApprovalTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
