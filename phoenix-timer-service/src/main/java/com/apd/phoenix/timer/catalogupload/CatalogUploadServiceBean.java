package com.apd.phoenix.timer.catalogupload;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.catalog.CatalogResultAggregator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;
import org.jboss.ejb3.annotation.TransactionTimeout;

@Named
@Singleton
public class CatalogUploadServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogUploadServiceBean.class);

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("catalog-upload", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("*").minute("*/5").second("0");
        return se;
    }

    @Inject
    CatalogUploadBp catalogUploadBp;

    @Inject
    CatalogResultAggregator catalogResultAggregator;

    @TransactionTimeout(unit = TimeUnit.HOURS, value = 2)
    @Timeout
    public void timeout(Timer timer) {
        LOGGER.info("Checking for completed catalog uploads");
        List<String> correlationIds = catalogUploadBp.getCorrelationIds();
        for (String correlationId : catalogUploadBp.getCorrelationIds()) {
            LOGGER.trace("Checking catalog upload " + correlationId);
            if (catalogUploadBp.needsFinishing(correlationId)) {
                catalogResultAggregator.finished(correlationId);
                catalogUploadBp.markAsFinished(correlationId);
                break;
            }
        }
    }

}
