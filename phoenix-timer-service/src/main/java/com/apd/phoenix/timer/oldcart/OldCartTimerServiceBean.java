package com.apd.phoenix.timer.oldcart;

import java.util.List;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
public class OldCartTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(OldCartTimerServiceBean.class);
    private static int THRESHOLD = 5;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private WorkflowService workflowService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("OldCart", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("23").minute("0").second("0");
        return se;
    }

    @Timeout
    public void timeout(Timer timer) {
        LOGGER.info("Checking for old carts....");
        List<CustomerOrder> customerOrders = customerOrderBp.getQuotesOlderThan(THRESHOLD);
        for (CustomerOrder customerOrder : customerOrders) {
            if (!"PROCUREMENT".equals(customerOrder.getType().getValue())) {
                LOGGER.info("Starting old cart workflow for order {}", customerOrder.getId());
                workflowService.processOldCart(customerOrder);
            }
        }
    }

}
