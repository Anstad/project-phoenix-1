/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.timer.jumptrack;

import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 * 
 * @author rhc
 */
@Named
@Singleton
public class StopNotificationTimerServiceBean extends AbstractTimerServiceBean {

    @Inject
    private ShipManifestService shipManifestService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("jumptrack-stop-notification", "ha", "singleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        Properties jumptrackProperties = PropertiesLoader.getAsProperties("jumptrack.integration");
        // starts timer
        ScheduleExpression se = new ScheduleExpression();

        se.dayOfMonth(jumptrackProperties.getProperty("stopNotificationTimerDayOfMonth")).hour(
                jumptrackProperties.getProperty("stopNotificationTimerHour")).minute(
                jumptrackProperties.getProperty("stopNotificationTimerMin")).second(
                jumptrackProperties.getProperty("stopNotificationTimerSec"));
        return se;
    }

    @Timeout
    public void timeout(Timer timer) {
        shipManifestService.sendStopNotificationRequest();
    }
}
