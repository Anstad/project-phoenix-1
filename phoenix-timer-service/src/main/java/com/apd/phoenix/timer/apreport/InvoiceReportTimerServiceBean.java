package com.apd.phoenix.timer.apreport;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
public class InvoiceReportTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceReportTimerServiceBean.class);

    @Inject
    private EmailMessageSender emailMessageSender;

    @Inject
    private ReportService reportService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("InvoiceReport", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("2").minute("0").second("0");
        return se;
    }

    @Timeout
    public void timeout(Timer timer) {
        List<Attachment> apAttachments = new ArrayList<Attachment>();
        Attachment apReport = new Attachment();
        apReport.setFileName("ap-report" + ".csv");
        apReport.setMimeType(MimeType.csv);
        InputStream apis = reportService.generateApReport();
        if (apis != null) {
            apReport.setContent(apis);
            apAttachments.add(apReport);
        }
        emailMessageSender.sendSimpleMessage("no-reply@apdmarketplace.com", "evadinion@americanproduct.com",
                "AP Report", "Please see the Attached AP Report.", apAttachments);

        List<Attachment> arAttachments = new ArrayList<Attachment>();
        Attachment arReport = new Attachment();
        arReport.setFileName("ar-report" + ".csv");
        arReport.setMimeType(MimeType.csv);
        InputStream aris = reportService.generateArReport();
        if (aris != null) {
            arReport.setContent(aris);
            arAttachments.add(arReport);
        }
        emailMessageSender.sendSimpleMessage("no-reply@apdmarketplace.com", "acctsreceivable@americanproduct.com",
                "AR Report", "Please see the Attached AR Report.", arAttachments);
    }

}
