package com.apd.phoenix.timer.shipmanifest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.jboss.msc.service.ServiceName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Named
public class ShipManifestTimerServiceBean extends AbstractTimerServiceBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShipManifestTimerServiceBean.class);

    @Inject
    private EmailMessageSender emailMessageSender;

    @Inject
    private ReportService reportService;

    @Override
    public ServiceName getServiceName() {
        return ServiceName.JBOSS.append("ShipManifest", "poll", "hasingleton");
    }

    @Override
    public ScheduleExpression getScheduleExpression() {
        ScheduleExpression se = new ScheduleExpression();
        se.hour("20").minute("30").second("0");
        se.dayOfWeek("Mon-Fri");
        return se;
    }

    @Timeout
    public void timeout(Timer timer) {
        String[] shipmanifestLocations = new String[0];
        Map<String, String> locationToRecipients = new HashMap<>();
        Map<String, List<Attachment>> recipientsToAttachments = new HashMap<>();

        //first, gets properties, splitting on commas
    	Properties usscoProperties = PropertiesLoader.getAsProperties("integration");
    	shipmanifestLocations = usscoProperties.getProperty("shipManifestCustomers", "").split(",");
    	String[] shipmanifestRecipients = usscoProperties.getProperty("shipManifestRecipients", "").split(",");
    	
    	//for each location, maps the location to a destination, and maps each destination to a list of attachments
    	for (int i = 0; i < shipmanifestLocations.length; i++) {
    		String location = shipmanifestLocations[i];
    		String recipient = (shipmanifestRecipients.length > i && StringUtils.isNotBlank(shipmanifestRecipients[i])) ? shipmanifestRecipients[i] : "newmetrodelivery@americanproduct.com";
    		locationToRecipients.put(location, recipient);
    		recipientsToAttachments.put(recipient, new ArrayList<Attachment>());
    	}

    	//then, for each location, generates the attachment, and adds it to the list of attachments for that location's recipient
        for (String rootAccountName : shipmanifestLocations) {
            Attachment shipManifest = new Attachment();
            shipManifest.setFileName(rootAccountName + ".pdf");
            shipManifest.setMimeType(MimeType.pdf);
            InputStream is = reportService.generateShipManifestPdf(rootAccountName);
            if (is != null) {
                shipManifest.setContent(is);
                recipientsToAttachments.get(locationToRecipients.get(rootAccountName)).add(shipManifest);
            }
            Attachment shipManifestCsv = new Attachment();
            shipManifestCsv.setFileName(rootAccountName + ".csv");
            shipManifestCsv.setMimeType(MimeType.csv);
            InputStream csvIs = reportService.generateShipManifestCsv(rootAccountName);
            if (csvIs != null) {
                shipManifestCsv.setContent(csvIs);
                recipientsToAttachments.get(locationToRecipients.get(rootAccountName)).add(shipManifestCsv);
            }
        }

        //then, for each recipient, creates an email and adds the attachments
        for (String recipient : recipientsToAttachments.keySet()) {
	        emailMessageSender.sendSimpleMessage("no-reply@apdmarketplace.com", recipient,
	                "Ship Manifest", "Attached is the PDF for tomorrow's Manifest. Also please find the excel versions attached", recipientsToAttachments.get(recipient));
        }
    }
}
