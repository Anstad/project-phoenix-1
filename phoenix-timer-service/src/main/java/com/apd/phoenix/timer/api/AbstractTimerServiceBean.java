package com.apd.phoenix.timer.api;

import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractTimerServiceBean implements Service<String> {

    public abstract ServiceName getServiceName();

    public abstract ScheduleExpression getScheduleExpression();

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTimerServiceBean.class);

    private final AtomicBoolean started = new AtomicBoolean(false);

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();

        //starts timer
        timerService.createCalendarTimer(getScheduleExpression(), new TimerConfig(this.getTimerName(), false));
    }

    public void stop(StopContext arg0) {
    }

    @Resource
    private TimerService timerService;

    private String getTimerName() {
        return "timer for " + this.nodeName;
    }

}
