package com.apd.phoenix.timer.lateshipment;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

/**
 *
 * @author anicholson
 */
@Startup
@Singleton
public class LateShipmentTimerServiceActivator extends AbstractTimerServiceActivator {

    private static final Logger LOGGER = LoggerFactory.getLogger(LateShipmentTimerServiceActivator.class);

    @EJB
    private LateShipmentTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
