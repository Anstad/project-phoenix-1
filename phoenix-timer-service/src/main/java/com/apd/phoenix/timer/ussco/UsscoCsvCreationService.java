package com.apd.phoenix.timer.ussco;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;

public interface UsscoCsvCreationService extends Service<String> {

    public static final ServiceName SINGLETON_SERVICE_NAME = ServiceName.JBOSS.append("ussco-csv-creation", "ha",
            "singleton");

    public Injector<ServerEnvironment> getEnvInjector();
}
