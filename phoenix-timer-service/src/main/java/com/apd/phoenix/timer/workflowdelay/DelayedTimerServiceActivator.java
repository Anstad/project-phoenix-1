package com.apd.phoenix.timer.workflowdelay;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Singleton
@Startup
public class DelayedTimerServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    private DelayedTimerServiceBean service;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return service;
    }
}
