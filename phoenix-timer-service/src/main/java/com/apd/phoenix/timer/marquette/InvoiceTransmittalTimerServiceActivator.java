package com.apd.phoenix.timer.marquette;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import com.apd.phoenix.timer.api.AbstractTimerServiceActivator;
import com.apd.phoenix.timer.api.AbstractTimerServiceBean;

@Startup
@Singleton
public class InvoiceTransmittalTimerServiceActivator extends AbstractTimerServiceActivator {

    @EJB
    private InvoiceTransmittalTimerServiceBean bean;

    @Override
    protected AbstractTimerServiceBean getServiceBean() {
        return bean;
    }

}
