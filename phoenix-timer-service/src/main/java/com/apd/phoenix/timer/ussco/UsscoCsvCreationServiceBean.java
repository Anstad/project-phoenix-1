package com.apd.phoenix.timer.ussco;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.annotation.Resource;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import javax.inject.Named;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.service.model.Catalog;
import org.jboss.ejb3.annotation.TransactionTimeout;

@Named
@Singleton
public class UsscoCsvCreationServiceBean implements UsscoCsvCreationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsscoCsvCreationServiceBean.class.getCanonicalName());
    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();

        //starts timer
        ScheduleExpression se = new ScheduleExpression();
        se.hour("23").minute("30").second("0");
        timerService.createCalendarTimer(se, new TimerConfig(this.getTimerName(), false));
    }

    public void stop(StopContext arg0) {
        if (!started.compareAndSet(true, false)) {
            LOGGER.warn("The service '" + this.getClass().getName() + "' is not active!");
        }
        else {
            LOGGER.info("Stop service '" + this.getClass().getName() + "'");
        }
        for (Timer t : timerService.getTimers()) {
            if (t.getInfo().equals(this.getTimerName())) {
                t.cancel();
            }
        }
    }

    @Inject
    private CatalogBp catalogBp;

    @Inject
    private CatalogBpNoTransaction catalogBpNoTransaction;

    @Timeout
    @TransactionTimeout(value = 3600)
    public void scheduler(Timer timer) {
        LOGGER.debug("Checking uploaded USSCO XML items");
        Catalog catalog = catalogBp.storeXmlCatalogFile();
        if (catalog != null) {
            catalogBpNoTransaction.setScheduledAction(catalog, new Date());
        }
        LOGGER.debug("Finished scheduling uploaded USSCO XML items");
    }

    @Resource
    private TimerService timerService;

    private String getTimerName() {
        return "timer for " + this.nodeName;
    }
}
