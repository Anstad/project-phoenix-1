/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * The Class Comment.
 */
@Entity
@XmlRootElement
public class OrderAttachment implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 5913362578737612019L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The user. */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private SystemUser user;

    /** The comment date. */
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date attachmentTimestamp = new Date();

    @Column(nullable = false)
    private String fileName;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((OrderAttachment) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public SystemUser getUser() {
        return this.user;
    }

    /**
     * Sets the user.
     *
     * @param user the new user
     */
    public void setUser(final SystemUser user) {
        this.user = user;
    }

    /**
     * Gets the attachment timestamp.
     *
     * @return the attachment timestamp
     */
    public Date getAttachmentTimestamp() {
        return this.attachmentTimestamp;
    }

    /**
     * Sets the attachment timestamp.
     *
     * @param attachmentTimestamp the new attachment timestamp
     */
    public void setAttachmentTimestamp(final Date attachmentTimestamp) {
        this.attachmentTimestamp = attachmentTimestamp;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        return result;
    }

    public static class OrderAttachmentComparator implements Comparator<OrderAttachment> {

        @Override
        public int compare(OrderAttachment log0, OrderAttachment log1) {

            if (log0.getAttachmentTimestamp() != null && log1.getAttachmentTimestamp() != null) {
                return log0.getAttachmentTimestamp().compareTo(log1.getAttachmentTimestamp());
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}