package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderAcknowledgmentErrorsEmailTemplate extends EmailTemplate<POAcknowledgementDto> {

    private static final String TEMPLATE = "order.acknowledgment.errors";

    @Override
	public String createBody(POAcknowledgementDto poAcknowledgmentDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		params.put("apdPo", poAcknowledgmentDto.getApdPo());
		params.put("items", poAcknowledgmentDto.getLineItems());

		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(POAcknowledgementDto poAcknowledgmentDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
