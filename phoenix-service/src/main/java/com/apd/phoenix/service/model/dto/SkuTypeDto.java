/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class SkuTypeDto implements Serializable {

    private static final long serialVersionUID = -4538026968267703561L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}