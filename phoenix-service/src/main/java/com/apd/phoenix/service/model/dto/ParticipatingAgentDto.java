package com.apd.phoenix.service.model.dto;

import java.util.List;
import com.apd.phoenix.service.model.dto.PurchaseOrderRequestDto.EDIPurchaseOrder.ShippingBillingDetails.ContactDto;

public class ParticipatingAgentDto {

    private String name;

    private String additionalNameInfo;

    private AddressDto address;

    private List<ContactDto> contacts;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdditionalNameInfo() {
        return additionalNameInfo;
    }

    public void setAdditionalNameInfo(String additionalNameInfo) {
        this.additionalNameInfo = additionalNameInfo;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public List<ContactDto> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDto> contacts) {
        this.contacts = contacts;
    }

}
