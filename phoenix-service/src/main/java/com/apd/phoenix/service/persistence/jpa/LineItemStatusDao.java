package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.OrderStatus;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author RHC
 */
@Stateless
@LocalBean
public class LineItemStatusDao extends AbstractDao<LineItemStatus> {

    public LineItemStatus getStatusByValue(String status) {
        //Sanity check
        if (status == null || status.equals("")) {
            return null;
        }
        Query query = entityManager
                .createQuery("SELECT status FROM LineItemStatus AS status WHERE status.value=:value");
        query.setParameter("value", status);

        return getSingleResultOrNull(query);
    }

    // This does the same thing as super.findById(), which is more efficient
    @Deprecated
    public LineItemStatus getLineItemStatusById(Long id) {
        String hql = "SELECT s FROM LineItemStatus AS s WHERE s.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return (LineItemStatus) query.getSingleResult();
    }

    //  Use super.findAll() instead, which does the same thing
    @Deprecated
    @SuppressWarnings("unchecked")
    public List<LineItemStatus> getStatusList() {
        String hql = "SELECT s FROM LineItemStatus AS s";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }
}