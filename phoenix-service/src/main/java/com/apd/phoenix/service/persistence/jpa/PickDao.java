package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Pick;

@Stateless
@LocalBean
public class PickDao extends AbstractDao<Pick> {

    public List<Pick> findPicksByShipperIdOrApdPo(String shipperId, String apdPo) {
        return this.findPicks(shipperId, apdPo, false);
    }

    public List<Pick> findShippedPicksByShipperIdOrApdPo(String shipperId, String apdPo) {
        return this.findPicks(shipperId, apdPo, true);
    }

    @SuppressWarnings("unchecked")
    private List<Pick> findPicks(String shipperId, String apdPo, boolean shipped) {
        String hql = "SELECT pick FROM Pick AS pick " + "LEFT JOIN FETCH pick.lineItem AS li "
                + "LEFT JOIN li.order AS co " + "LEFT JOIN co.poNumbers AS poNumber "
                + "WHERE pick.isShipped=:shipped AND pick.shipperId=:shipperId AND poNumber.value=:apdPoNumber";

        Query query = entityManager.createQuery(hql);
        query.setParameter("shipperId", shipperId);
        query.setParameter("apdPoNumber", apdPo);
        query.setParameter("shipped", shipped);
        List<Pick> results = query.getResultList();
        if (results == null || results.size() == 0) {
            return null;
        }
        return results;
    }

}
