/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client;

/**
 *
 * @author dnorris
 */
public enum TransactionType {

    AUTHORIZE("Authorization Transaction"), AUTHORIZE_CAPTURE("AuthorizeAndCapture Transaction"), CAPTURE(
            "Capture Transaction"), CREDIT("Credit Transaction"), VERIFICATION("Verification Transaction");

    private final String transactionTypeValue;

    private TransactionType(String s) {
        transactionTypeValue = s;
    }

    public String getTransactionValue() {
        return transactionTypeValue;
    }

}
