package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Department;

/**
 * Department DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class DepartmentDao extends AbstractDao<Department> {

    private static final Logger LOG = LoggerFactory.getLogger(DepartmentDao.class);

    /**
     * Takes an AccountXCredentialXUser, and returns a List of Departments. If the 
     * AccountXCredentialXUser has at least one department, returns those departments; otherwise, 
     * returns the departments associated with the Credential.
     * 
     * @param axcxu
     * @return
     */
    public List<Department> departmentsForCashout(AccountXCredentialXUser axcxu) {
        List<Object> user = deptResults(axcxu.getId(), "user");
        List<Object> cred = deptResults(axcxu.getId(), "cred");
        List<Department> toReturn = new ArrayList<>();
        if (user.size() != 0 && user.get(0) != null) {
        	for (Object obj : user) {
        		toReturn.add((Department) obj);
        	}
        }
        else if (cred.size() != 0 && cred.get(0) != null) {
        	for (Object obj : cred) {
        		toReturn.add((Department) obj);
        	}
        }
        return toReturn;
    }

    @SuppressWarnings("unchecked")
    private List<Object> deptResults(Long axcxuId, String userOrCred) {
        String hql = "SELECT " + userOrCred + "Depts FROM AccountXCredentialXUser AS axcxu LEFT JOIN "
                + "axcxu.departments AS userAssignedDepts LEFT JOIN userAssignedDepts.department AS "
                + "userDepts LEFT JOIN axcxu.credential.departments AS credDepts WHERE axcxu.id = :axcxuId "
                + "AND userAssignedDepts.active = true";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxuId);

        setItemsReturned(query, 0, 0);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Department> filteredDepartmentList(String name, List<Long> accountWhiteList) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT department FROM Account AS account"
                + " LEFT OUTER JOIN account.credentials AS credential"
                + " LEFT OUTER JOIN account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN credential.departments AS department");

        hql.append(" WHERE 1=1");
        if (accountWhiteList != null) {
            hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
        }
        hql.append(" AND upper(department.name) like :name");

        Query query = entityManager.createQuery(hql.toString());
        LOG.info("Searching for department using '{}'", hql.toString());

        if (accountWhiteList != null) {
            query.setParameter("accountWhiteList", accountWhiteList);
        }
        query.setParameter("name", "%" + name.toUpperCase() + "%");

        final List<Department> resultList = query.getResultList();
        return resultList;
    }
}
