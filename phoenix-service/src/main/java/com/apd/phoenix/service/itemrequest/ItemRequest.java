/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.itemrequest;

import java.io.Serializable;

public class ItemRequest implements Serializable {

    private String requestor;
    private String email;
    private String partNumber;
    private String description;
    private String howOften;
    private String quantity;
    private String divDept;

    public ItemRequest(String requestor, String email, String partNumber, String description, String howOften,
            String quantity, String divDept) {
        this.requestor = requestor;
        this.email = email;
        this.partNumber = partNumber;
        this.description = description;
        this.howOften = howOften;
        this.quantity = quantity;
        this.divDept = divDept;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHowOften() {
        return howOften;
    }

    public void setHowOften(String howOften) {
        this.howOften = howOften;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getDivDept() {
        return divDept;
    }

    public void setDivDept(String divDept) {
        this.divDept = divDept;
    }

}
