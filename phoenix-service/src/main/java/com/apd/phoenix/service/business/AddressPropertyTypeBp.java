package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.AddressPropertyType;
import com.apd.phoenix.service.persistence.jpa.AddressPropertyTypeDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AddressPropertyTypeBp extends AbstractBp<AddressPropertyType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AddressPropertyTypeDao dao) {
        this.dao = dao;
    }
}
