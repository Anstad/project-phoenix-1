package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Index;

/**
 * This entity is used to store the information relevant to a single invoice.
 * 
 * @author RHC
 *
 */
@Entity
@XmlRootElement
@Table(name = "INVOICE")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "FROM_CLASS", discriminatorType = DiscriminatorType.STRING)
public class Invoice implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = -2204391531477963079L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    //Used for vendor invoices

    //Null for USSCo vendor invoices

    @Column
    private String invoiceNumber;

    @Column
    private BigDecimal amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "INVOICE_CREATEDATE_IDX")
    private Date createdDate;

    //Used in USCo invoicing
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "INVOICE_SPECIAL", joinColumns = @JoinColumn(name = "INVOICE_ID"))
    @Column(name = "INSTRUCTION")
    private List<String> specialInstructions;

    @Column
    private String billingTypeCode;

    //only used for vendor invoices
    @ManyToMany
    private List<LineItem> actualItems;

    @Temporal(TemporalType.TIMESTAMP)
    private Date approvalDate;

    //used for vendor invoices
    @Temporal(TemporalType.DATE)
    private Date invoiceDate;

    //used for vendor invoices
    @Temporal(TemporalType.DATE)
    private Date dueDate;

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Invoice) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getDate() {
        return createdDate;
    }

    public void setDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the specialInstructions
     */
    public List<String> getSpecialInstructions() {
        return specialInstructions;
    }

    /**
     * @param specialInstructions the specialInstructions to set
     */
    public void setSpecialInstructions(List<String> specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    /**
     * @return the billingTypeCode
     */
    public String getBillingTypeCode() {
        return billingTypeCode;
    }

    /**
     * @param billingTypeCode the billingTypeCode to set
     */
    public void setBillingTypeCode(String billingTypeCode) {
        this.billingTypeCode = billingTypeCode;
    }

    public List<LineItem> getActualItems() {
        return actualItems;
    }

    public void setActualItems(List<LineItem> actualItems) {
        this.actualItems = actualItems;
    }

    public Date getApprovalDate() {
        return this.approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    public Date getInvoiceDate() {
        return this.invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Date getDueDate() {
        return this.dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getVendorInvoiceTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;

        if (this.getActualItems() != null) {
            for (LineItem item : this.getActualItems()) {
                if (item != null && item.getQuantity() != null && item.getCost() != null) {
                    toReturn = toReturn.add(item.getCost().multiply(new BigDecimal("" + item.getQuantity())));
                    if (item.getMaximumTaxToCharge() != null) {
                        toReturn = toReturn.add(item.getMaximumTaxToCharge());
                    }
                    if (item.getEstimatedShippingAmount() != null) {
                        toReturn = toReturn.add(item.getEstimatedShippingAmount());
                    }
                }
            }
        }

        return toReturn;
    }

}