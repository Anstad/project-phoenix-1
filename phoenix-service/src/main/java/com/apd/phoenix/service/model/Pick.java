package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Pick implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = 634970615889573540L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne
    private LineItem lineItem;

    @Column(updatable = false, nullable = false)
    private String shipperId;

    @Column
    private String orderNumber;

    @Column
    private String customerPoNumber;

    @Column
    private String apdOrderNumber;

    @Column
    private String inventoryId;

    @Column
    private String lineReference;

    @Column
    private BigInteger qtyToPick;

    @Column
    private BigInteger qtyBo;

    @Column
    private boolean isShipped = false;

    public String getShipperId() {
        return shipperId;
    }

    public void setShipperId(String shipperId) {
        this.shipperId = shipperId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerPoNumber() {
        return customerPoNumber;
    }

    public void setCustomerPoNumber(String customerPoNumber) {
        this.customerPoNumber = customerPoNumber;
    }

    public String getApdOrderNumber() {
        return apdOrderNumber;
    }

    public void setApdOrderNumber(String apdOrderNumber) {
        this.apdOrderNumber = apdOrderNumber;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getLineReference() {
        return lineReference;
    }

    public void setLineReference(String lineReference) {
        this.lineReference = lineReference;
    }

    public BigInteger getQtyToPick() {
        return qtyToPick;
    }

    public void setQtyToPick(BigInteger qtyToPick) {
        this.qtyToPick = qtyToPick;
    }

    public BigInteger getQtyBo() {
        return qtyBo;
    }

    public void setQtyBo(BigInteger qtyBo) {
        this.qtyBo = qtyBo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public LineItem getLineItem() {
        return lineItem;
    }

    public void setLineItem(LineItem lineItem) {
        this.lineItem = lineItem;
    }

    public boolean isShipped() {
        return isShipped;
    }

    public void setShipped(boolean isShipped) {
        this.isShipped = isShipped;
    }

}
