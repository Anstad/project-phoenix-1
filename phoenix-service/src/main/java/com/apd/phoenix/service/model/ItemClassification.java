/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;

/**
 * The Class ItemClassification.
 */
@Entity
@XmlRootElement
public class ItemClassification implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 656324954427007261L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemClassification) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ItemClassification [version=" + version + ", itemClassificationImage=" + itemClassificationImage + "]";
    }

    /** The item classification image. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private ItemClassificationImage itemClassificationImage;

    /**
     * Gets the item classification image.
     *
     * @return the item classification image
     */
    public ItemClassificationImage getItemClassificationImage() {
        return this.itemClassificationImage;
    }

    /**
     * Sets the item classification image.
     *
     * @param itemClassificationImage the new item classification image
     */
    public void setItemClassificationImage(final ItemClassificationImage itemClassificationImage) {
        this.itemClassificationImage = itemClassificationImage;
    }

    /** The classification code. */
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private ClassificationCode classificationCode;

    /**
     * Gets the classification code.
     *
     * @return the classification code
     */
    public ClassificationCode getClassificationCode() {
        return this.classificationCode;
    }

    /**
     * Sets the classification code.
     *
     * @param classificationCode the classification code
     */
    public void setClassificationCode(final ClassificationCode classificationCode) {
        this.classificationCode = classificationCode;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "ItemClassType_id")
    private ItemClassificationType itemClassificationType;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "itemClassification")
    private Set<ClassificationNote> classificationNotes;

    public ItemClassificationType getItemClassificationType() {
        return this.itemClassificationType;
    }

    public void setItemClassificationType(final ItemClassificationType itemClassificationType) {
        this.itemClassificationType = itemClassificationType;
    }

    public Set<ClassificationNote> getClassificationNotes() {
        return this.classificationNotes;
    }

    public void setClassificationNotes(Set<ClassificationNote> classificationNotes) {
        this.classificationNotes = classificationNotes;
    }
}