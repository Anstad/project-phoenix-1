package com.apd.phoenix.service.email.api;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.SingletonPropertiesLoader;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.report.ReportService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class EmailTemplate<C> {

    @Inject
    protected ReportService reportService;

    private static final Logger logger = LoggerFactory.getLogger(EmailTemplate.class);

    protected static final String directory = "email/";

    protected static final String PDF_EXT = ".pdf";

    public abstract String createBody(C content) throws IOException, TemplateException;

    public abstract Attachment createAttachment(C content) throws NoAttachmentContentException;

    public String getProperty(String key) {
        Properties templateProperties = PropertiesLoader.getAsProperties(EmailFactoryBp.class, this
                .getTemplateLocation());
        String tenant = SingletonPropertiesLoader.getTenant();
        String tenantKey = tenant + "." + key;
        if (templateProperties.containsKey(tenantKey)) {
            return templateProperties.getProperty(tenantKey);
        }
        return templateProperties.getProperty(key);
    }

    protected abstract String getTemplate();

    protected String getTemplateLocation() {
        return directory + getTemplate();
    }

    protected String create(Map<String, Object> params) throws IOException, TemplateException {
        String templateFile = getTemplate() + ".ftl";

        Configuration configuration = new Configuration();
        configuration.setClassForTemplateLoading(EmailTemplate.class, "/");
        Template freemarkerTemplate = configuration.getTemplate("/" + directory + templateFile);
        Writer writer = new StringWriter();
        freemarkerTemplate.process(params, writer);

        return writer.toString();
    }

    protected AddressDto getSafeAddress(AddressDto addressDto) {
        AddressDto toReturn;
        if (addressDto == null) {
            toReturn = new AddressDto();
        }
        else {
            toReturn = addressDto;
        }
        if (toReturn.getCity() == null) {
            toReturn.setCity("");
        }
        if (toReturn.getLine1() == null) {
            toReturn.setLine1("None");
        }
        if (toReturn.getState() == null) {
            toReturn.setState("");
        }
        if (toReturn.getZip() == null) {
            toReturn.setZip("");
        }
        return toReturn;
    }

    protected String getAddressFieldValue(String fieldLabelAndValue) {
        if (StringUtils.isBlank(fieldLabelAndValue)) {
            return null;
        }
        if (fieldLabelAndValue.contains("|")) {
            return fieldLabelAndValue.substring(fieldLabelAndValue.indexOf("|") + 1);
        }
        return fieldLabelAndValue;
    }

    protected String getAddressFieldLabel(String fieldLabelAndValue) {
        if (StringUtils.isBlank(fieldLabelAndValue)) {
            return null;
        }
        if (fieldLabelAndValue.contains("|")) {
            return fieldLabelAndValue.substring(0, fieldLabelAndValue.indexOf("|"));
        }
        return "";
    }
}
