package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.IpAddress;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.persistence.jpa.AccountDao;
import com.apd.phoenix.service.persistence.jpa.AddressDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

/**
 * This class provides business process methods for Accounts.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AccountBp extends AbstractBp<Account> {

    private static final String CAN_EDIT_CUSTOMER_PO = "Can Edit Customer PO";
    private static final String REGISTRATION_LINK_HIDDEN = "hide user registration link";
    private static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";
    private static final String YES = "yes";
    private static final String USER_REGISTRATION_DOMAIN_PROPERTY = "user registration domain";
    private static final String RESET_PASSWORD_LINK_HIDDEN = "hide user password reset link";

    @Inject
    AddressDao addressDao;

    @Inject
    SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(AccountDao dao) {
        this.dao = dao;
    }

    /**
     * Adds an account as a child of another account. This doesn't check correctness, make sure to call the 
     * methods canAddParentToChild and isCyclicTree first.
     * 
     * @param parent - the parent account in the new relationship
     * @param child - the child account in the new relationship
     * @return the persisted, managed parent account
     */
    public Account addChildAccount(Account parent, Account child) {

        /*
         * Adds the credentials that the child has and the account tree 
         * doesn't have to the account tree.
         * 
         * Might be better to use AccountXCredentialXUser instead.
         */
        for (Credential c : child.getCredentials()) {
            if (!parent.getRootAccount().getCredentials().contains(c)) {
                parent.getRootAccount().getCredentials().add(c);
            }
        }

        //Adds the child to the parent
        child.setParentAccount(parent);

        //Sets the child's root account to the parent's root account
        if (parent.getRootAccount() == null) {
            child.setRootAccount(parent);
        }
        else {
            child.setRootAccount(parent.getRootAccount());
        }

        //dao.update(child);
        //return dao.update(parent);
        return parent;
    }

    /**
     * Deletes the entity, determined by the id from the database.
     * 
     * @param id - the id of the entity to be deleted
     */
    @Override
    public void delete(Long id, Class<Account> type) {
        Account toDelete = dao.findById(id, type);
        boolean hasOrders = ((AccountDao) dao).hasOrders(toDelete);
        //Checks that the account has no children, and no orders have been placed with it
        if (hasOrders) {
            return;
        }

        dao.delete(id, type);
    }

    /**
     * This method is used to add an address to an Account or Account tree.
     * 
     * @param account - recipient account or member of recipient account tree for the new address
     * @param toAdd - the address to be added to the provided account or root of the provided account
     */
    public Account addAddress(Account account, Address toAdd, boolean addToRoot) {
        LOG.info("Adding address " + toAdd + " to account " + account.getId());
        Account toUpdate = null;
        if (addToRoot && account.getRootAccount() != null) {
            toUpdate = account.getRootAccount();
        }
        else {
            toUpdate = account;
        }
        if (!addressDao.isAddressAlreadyOnAccount(toAdd.getId(), account.getId())) {
            toAdd.setAccount(toUpdate);
            toAdd = addressDao.create(toAdd);
        }
        return ((AccountDao) dao).findById(toUpdate.getId(), Account.class);
    }

    public boolean canAddParentToChild(Account parent, Account child) {
        if (child.getId() != null) {
            child = this.findById(child.getId(), Account.class);
        }
        if (parent.getId() != null) {
            parent = this.findById(parent.getId(), Account.class);
        }
        if (child.getId() == null) {
            return true; //A new account can be added as a child to any other account
        }
        if (child.getRootAccount() == null) {
            return false; //The child is the root of an account tree
        }
        if (child.getRootAccount().equals(parent.getRootAccount())) {
            return true; //If the root accounts match
        }
        if (parent.getRootAccount() == null && child.getRootAccount().equals(parent)) {
            return true; //The parent is the root account
        }
        return false;
    }

    public boolean isCyclicTree(Account parent, Account child) {

        Account ancestor = parent;
        while (ancestor != null) {
            ancestor = this.findById(ancestor.getId(), Account.class);
            if (ancestor.equals(child)) {
                return true;
            }
            ancestor = ancestor.getParentAccount();
        }

        return false;
    }

    public List<Address> getAddresses(Account account) {
        List<Address> toReturn;
        List<Address> toReturnRoot;
        Account rootAccount = account.getRootAccount();
        
        //Get addresses on this account
        toReturn = addressDao.getAddresses(account);
        if (toReturn == null || toReturn.isEmpty()) {
            toReturn = new ArrayList<>();
        }
        
        //Get addresses on the root account
        toReturnRoot = addressDao.getAddresses(rootAccount);
        if (toReturnRoot == null || toReturnRoot.isEmpty()) {
            toReturnRoot = new ArrayList<>();
        }

        toReturn.addAll(toReturnRoot);
        return toReturn;
    }

    public List<Address> getAddressesByZip(Account account, String zipCode) {
        return addressDao.getAccountAddressesByZip(account, zipCode);
    }

    public boolean canEditCustomerPo(Account account) {
        for (AccountXAccountPropertyType property : account.getProperties()) {
            if (property.getType().getName().equals(CAN_EDIT_CUSTOMER_PO) && property.getValue().equals(YES)) {
                return true;
            }
        }
        return false;
    }

    public boolean isRegistrationLinkHidden(String subdomain) {

        List<Account> accounts = ((AccountDao) dao).getAccountsUnderSubdomain(subdomain);

        //if it's a real subdomain, but it doesn't match any account, returns false
        if (accounts == null || accounts.isEmpty()) {
            return false;
        }

        for (Account account : accounts) {
            if (account.getProperties() != null) {
                for (AccountXAccountPropertyType property : account.getProperties()) {
                    if (property.getType().getName().equals(REGISTRATION_LINK_HIDDEN)
                            && property.getValue().equals(Boolean.TRUE.toString())) {
                        return true;
                    }
                }
            }
        }

        return false;

    }

    public Account retrieveAccount(AccountDto accountDto) {
        Account search = new Account();
        search.setName(accountDto.getName());
        List<Account> results = this.dao.searchByExactExample(search, 0, 0);
        if (results == null || results.isEmpty()) {
            return null;
        }
        else {
            return results.get(0);
        }
    }

    public Account hydrateForSearchResults(Account searchResult) {
        return ((AccountDao) dao).hydrateForSearchResults(searchResult);
    }

    public List<Account> filteredAccountList(String name) {
        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            return ((AccountDao) dao).filteredAccountList(name, systemUserDao.getAccountIds(user));
        }
        else {
            return ((AccountDao) dao).filteredAccountList(name, null);
        }
    }

    public Account findByName(String name) {
        if (StringUtils.isBlank(name)) {
            return null;
        }
        Account searchAccount = new Account();
        searchAccount.setName(name);
        List<Account> accountSearchList = this.searchByExactExample(searchAccount, 0, 0);
        if (accountSearchList.isEmpty()) {
            return null;
        }
        return accountSearchList.get(0);
    }

    //    public Account findByAssignedIdOrDuns(String identity) {
    //    	if (StringUtils.isBlank(identity)) {
    //    		return null;
    //    	}
    //    	return ((AccountDao)dao).getAccountsUnderSubdomain(identity);
    //    }

    /**
     * This method takes a String email address and subdomain, and determines whether that email 
     * address is valid for accounts on that subdomain.
     * 
     * @param emailAddress
     * @param subdomain
     * @return
     */
    public boolean validEmailAddress(String emailAddress, String subdomain) {
        //returns true if the current subdomain is blank, or "www"
        if (StringUtils.isBlank(subdomain) || subdomain.equals(IpSubdomainRestriction.GENERIC_SUBDOMAIN)) {
            return true;
        }

        //returns false if the email address is not valid
        if (StringUtils.isBlank(emailAddress) || !emailAddress.contains("@")) {
            return false;
        }

        //gets accounts with the given subdomain
        List<Account> accounts = ((AccountDao) dao).getAccountsUnderSubdomain(subdomain);

        //if it's a real subdomain, but it doesn't match any account, returns false
        if (accounts == null || accounts.isEmpty()) {
            return false;
        }

        //iterates through the accounts, if any is valid, returns true
        String emailDomain = emailAddress.substring(emailAddress.indexOf("@") + 1);
        for (Account account : accounts) {
            if (isDomainValidWithAccount(emailDomain, account)) {
                return true;
            }
        }
        //returns false, if not valid for any account
        return false;
    }

    /**
     * Checks whether an email domain is valid for a single account
     * 
     * @param emailDomain
     * @param account
     * @return
     */
    private boolean isDomainValidWithAccount(String emailDomain, Account account) {
        for (AccountXAccountPropertyType property : account.getProperties()) {
            if (property.getType().getName().equals(USER_REGISTRATION_DOMAIN_PROPERTY)) {
                if (StringUtils.isBlank(property.getValue())) {
                    //returns true if the domain property is specified, but it's blank
                    return true;
                }
                //boolean indicating whether a valid domain matches the email's domain
                boolean foundMatch = false;
                for (String validDomain : property.getValue().split(",")) {
                    //trims email domain in case the user included spaces
                    validDomain = validDomain.trim();
                    if (emailDomain.toLowerCase().equals(validDomain.toLowerCase())) {
                        //sets the boolean to true if the email's domain matches a valid domain
                        foundMatch = true;
                        break;
                    }
                }
                //returns whether a valid domain matches the email's
                return foundMatch;
            }
        }
        //If registration subdomain is not specified on account and there is a root account, 
        // use the root account's registration subdomain
        if (account.getRootAccount() != null) {
            return isDomainValidWithAccount(emailDomain, account.getRootAccount());
        }
        return true;
    }

    public boolean validIpAddress(String ipAddress, String subdomain) {
        //returns true if the current subdomain is blank, or "www"
        if (StringUtils.isBlank(subdomain) || subdomain.equals(IpSubdomainRestriction.GENERIC_SUBDOMAIN)) {
            return true;
        }

        //returns false if the email address is not valid
        if (StringUtils.isBlank(ipAddress)) {
            return true;
        }

        //gets accounts with the given subdomain
        List<Account> accounts = ((AccountDao) dao).getAccountsUnderSubdomain(subdomain);

        //if it's a real subdomain, but it doesn't match any account, returns false
        if (accounts == null || accounts.isEmpty()) {
            return false;
        }

        Set<IpAddress> validAddresses = new HashSet<IpAddress>();

        for (Account account : accounts) {
            if (account.getIps() != null && !account.getIps().isEmpty()) {
                validAddresses.addAll(account.getIps());
            }
        }

        //if there's no IPs in the whitelists, returns true
        if (validAddresses.isEmpty()) {
            return true;
        }

        for (IpAddress validAddress : validAddresses) {
            if (ipAddress.equalsIgnoreCase(validAddress.getValue())) {
                return true;
            }
        }

        return false;
    }

    public Account findChildByLocationCode(String locationCode, Account parent) {
        //If this account has the proper location code, return it.
        if (locationCode.equals(parent.getLocationCode())) {
            return parent;
        }
        Collection<Account> children = this.getChildren(parent);
        if (children == null) {
            return null;
        }
        //For each child recursively search for a child with a matching location code
        for (Account child : children) {
            Account matchingChild = findChildByLocationCode(locationCode, child);
            //If the child tree had the correct location code return that
            if (matchingChild != null) {
                return matchingChild;
            }
        }
        return null;
    }

    public Collection<SystemUser> getUsers(Account account) {
        return ((AccountDao) dao).getUsers(account);
    }

    public Collection<Account> getChildren(Account account) {
        if (account == null || account.getId() == null) {
            return null;
        }
        Account searchAccount = new Account();
        searchAccount.setParentAccount(new Account());
        searchAccount.getParentAccount().setId(account.getId());
        return this.searchByExactExample(searchAccount, 0, 0);
    }

    public boolean isResetPasswordLinkHidden(String subdomain) {

        List<Account> accounts = ((AccountDao) dao).getAccountsUnderSubdomain(subdomain);

        //if it's a real subdomain, but it doesn't match any account, returns false
        if (accounts == null || accounts.isEmpty()) {
            return false;
        }

        for (Account account : accounts) {
            if (account.getProperties() != null) {
                for (AccountXAccountPropertyType property : account.getProperties()) {
                    if (property.getType().getName().equals(RESET_PASSWORD_LINK_HIDDEN)
                            && property.getValue().equals(Boolean.TRUE.toString())) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    //Don't load addresses as there may be thousands
    public Account eagerLoad(Account account) {
        if (account == null || account.getId() == null) {
            //Don't try to load transients
            return account;
        }
        account = ((AccountDao) dao).findById(account.getId(), Account.class);
        load(account.getBlanketPos());
        load(account.getBulletins());
        load(account.getCredentials());
        load(account.getGroups());
        load(account.getPaymentInformation());
        load(account.getProcessConfigurations());
        load(account.getBlanketPos());
        load(account.getProperties());
        load(account.getIps());
        load(account.getNotificationProperties());
        load(account.getPrimaryContact());
        load(account.getCxmlConfiguration());
        load(account.getIconUrls());
        load(account.getRootAccount());
        load(account.getNumbers());
        if (account.getParentAccount() != null) {
            account.setParentAccount(eagerLoad(account.getParentAccount()));
        }
        return account;

    }

    public List<Account> getAccountsForLoginValidation(SystemUser user) {
        return ((AccountDao) dao).getAccountsForLoginValidation(user);
    }
}
