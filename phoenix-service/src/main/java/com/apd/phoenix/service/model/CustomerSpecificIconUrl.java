package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Check;
import org.hibernate.envers.Audited;

/**
 * This entity is used store the different types of properties of a CatalogXItem relationship.
 * 
 * @author RHC
 *
 */
@Entity
@Cacheable
@Check(constraints = "(PROPERTYTYPE_ID IS NULL AND CLASSIFICATIONTYPE_ID IS NOT NULL) OR (PROPERTYTYPE_ID IS NOT NULL AND CLASSIFICATIONTYPE_ID IS NULL)")
public class CustomerSpecificIconUrl implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -8677391992028120219L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String overrideIconUrl;

    @ManyToOne(fetch = FetchType.EAGER)
    private ItemClassificationType classificationType;

    @ManyToOne(fetch = FetchType.EAGER)
    private ItemPropertyType propertyType;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public String getOverrideIconUrl() {
        return overrideIconUrl;
    }

    public void setOverrideIconUrl(String overrideIconUrl) {
        this.overrideIconUrl = overrideIconUrl;
    }

    public ItemClassificationType getClassificationType() {
        return classificationType;
    }

    public void setClassificationType(ItemClassificationType classificationType) {
        this.classificationType = classificationType;
    }

    public ItemPropertyType getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(ItemPropertyType propertyType) {
        this.propertyType = propertyType;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CustomerSpecificIconUrl) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

}
