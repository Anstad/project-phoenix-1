/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author nreidelb
 */
public class PaymentInformationDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4470350079598926742L;

    private String paymentType;

    private AddressDto billToAddress;

    //TODO move the following properties to a CardInformationDto class since they are only applicable to credit cards
    private String cardNumber;
    private Date expiration;
    private String identity;
    private String cardVaultToken;
    private String nameOnCard;
    private String cardType;

    /**
     * @return the paymentType
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType the paymentType to set
     */
    public void setPaymentType(final String paymentType) {
        this.paymentType = paymentType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(final Date expiration) {
        this.expiration = expiration;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(final String identity) {
        this.identity = identity;
    }

    /**
     * @return the nameOnCard
     */
    public String getNameOnCard() {
        return nameOnCard;
    }

    /**
     * @param nameOnCard the nameOnCard to set
     */
    public void setNameOnCard(final String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    /**
     * @return the billToAddress
     */
    public AddressDto getBillToAddress() {
        return billToAddress;
    }

    /**
     * @param billToAddress the billToAddress to set
     */
    public void setBillToAddress(AddressDto billToAddress) {
        this.billToAddress = billToAddress;
    }

    /**
     * @return the cardVaultToken
     */
    public String getCardVaultToken() {
        return cardVaultToken;
    }

    /**
     * @param cardVaultToken the cardVaultToken to set
     */
    public void setCardVaultToken(final String cardVaultToken) {
        this.cardVaultToken = cardVaultToken;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(final String cardType) {
        this.cardType = cardType;
    }

    public enum Type {
        INVOICE("Invoice"), CC("Credit Card");

        private String value;

        private Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }
}
