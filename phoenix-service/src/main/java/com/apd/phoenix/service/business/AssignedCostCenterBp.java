package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.persistence.jpa.AssignedCostCenterDao;

@Stateless
@LocalBean
public class AssignedCostCenterBp extends AbstractBp<AssignedCostCenter> {

    @Inject
    public void initDao(AssignedCostCenterDao dao) {
        this.dao = dao;
    }

    /**
     * This method takes an AssignedCostCenter and a BigDecimal amount, and returns true if an order with 
     * that amount can be purchased under this cost center.
     * 
     * @param costCenter
     * @param amount
     * @return
     */
    public Boolean isAmountAllowed(AssignedCostCenter costCenter, BigDecimal amount) {
        if (costCenter == null || amount == null) {
            return true;
        }

        //finds the currently charged amount on the cost center. If it's set on the assigned cost center, 
        //uses that; otherwise, checks the main cost center; otherwise, assumes it's zero.
        BigDecimal currentCharge = null;
        if (costCenter.getCurrentCharge() != null) {
            currentCharge = costCenter.getCurrentCharge();
        }
        else if (costCenter.getCostCenter() != null && costCenter.getCostCenter().getTotalSpent() != null) {
            currentCharge = costCenter.getCostCenter().getTotalSpent();
        }
        else {
            currentCharge = BigDecimal.ZERO;
        }

        //finds the pending charge from the assigned cost center, defaults to zero if null
        BigDecimal pendingCharge = (costCenter.getPendingCharge() != null ? costCenter.getPendingCharge()
                : BigDecimal.ZERO);

        //finds the total limit on the cost center. If it's set on the assigned cost center, uses that; 
        //otherwise, checks the main cost center; otherwise, there's no limit, and the method returns true.
        BigDecimal totalLimit = null;
        if (costCenter.getTotalLimit() != null) {
            totalLimit = costCenter.getTotalLimit();
        }
        else if (costCenter.getCostCenter() != null && costCenter.getCostCenter().getSpendingLimit() != null) {
            totalLimit = costCenter.getCostCenter().getSpendingLimit();
        }
        else {
            return true;
        }

        //finds the amount that would be on the cost center, if the amount is added
        BigDecimal potentialAmount = currentCharge.add(pendingCharge.add(amount));

        //returns whether the potential amount is less than the total limit on the cost center
        return potentialAmount.compareTo(totalLimit) < 0;
    }
}
