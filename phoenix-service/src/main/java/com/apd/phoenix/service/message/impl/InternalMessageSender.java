package com.apd.phoenix.service.message.impl;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

public class InternalMessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(InternalMessageSender.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    protected ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/internal-validation-queue")
    private Destination internalValidationQueue;

    public Message sendPurchaseOrderForValidation(PurchaseOrderDto purchaseOrderDto, Long orderId) {
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(internalValidationQueue);
                messageProducer.send(createPurchaseOrderMessage(msession, purchaseOrderDto, orderId));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating vendor purchase order message", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after vendor purchase order message", e);
        }
        return null;
    }

    private javax.jms.Message createPurchaseOrderMessage(Session session, PurchaseOrderDto purchaseOrderDto,
            Long orderId) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = session.createObjectMessage(purchaseOrderDto);
            objectMessage.setLongProperty("orderId", orderId);
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }
}
