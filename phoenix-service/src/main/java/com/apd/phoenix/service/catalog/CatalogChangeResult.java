package com.apd.phoenix.service.catalog;

import java.io.Serializable;
import java.util.UUID;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

public class CatalogChangeResult implements Serializable {

    private SyncItemResultSummary summary;
    private SyncItemResult result;
    private String correlationId;
    private Long catalogId;
    private Long oldCatalogId;
    private Long totalItems;
    private SyncAction action;
    private Boolean persistChanges;
    private Boolean rollbackChanges = false;

    public CatalogChangeResult(SyncItemResultSummary summary, SyncItemResult result, String correlationId,
            Long catalogId, Long oldCatalogId, Long totalItems, SyncAction action, Boolean persistChanges,
            Boolean rollbackChanges) {
        super();
        this.summary = summary;
        this.result = result;
        this.correlationId = correlationId;
        this.catalogId = catalogId;
        this.oldCatalogId = oldCatalogId;
        this.totalItems = totalItems;
        this.action = action;
        this.persistChanges = persistChanges;
        this.rollbackChanges = rollbackChanges;
    }

    public CatalogChangeResult() {
        super();
    }

    public SyncItemResultSummary getSummary() {
        return summary;
    }

    public void setSummary(SyncItemResultSummary summary) {
        this.summary = summary;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public SyncItemResult getResult() {
        return result;
    }

    public void setResult(SyncItemResult result) {
        this.result = result;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public Long getOldCatalogId() {
        return oldCatalogId;
    }

    public void setOldCatalogId(Long oldCatalogId) {
        this.oldCatalogId = oldCatalogId;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public SyncAction getAction() {
        return action;
    }

    public void setAction(SyncAction action) {
        this.action = action;
    }

    public Boolean getPersistChanges() {
        return persistChanges;
    }

    public void setPersistChanges(Boolean persistChanges) {
        this.persistChanges = persistChanges;
    }

    public Boolean getRollbackChanges() {
        return rollbackChanges;
    }

    public void setRollbackChanges(Boolean rollbackChanges) {
        this.rollbackChanges = rollbackChanges;
    }
}
