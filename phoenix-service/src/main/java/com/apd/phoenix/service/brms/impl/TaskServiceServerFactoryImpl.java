package com.apd.phoenix.service.brms.impl;

import com.apd.phoenix.service.brms.api.TaskServiceServerFactory;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.drools.SystemEventListenerFactory;
import org.jbpm.task.service.TaskService;
import org.jbpm.task.service.UserGroupCallbackManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class TaskServiceServerFactoryImpl implements TaskServiceServerFactory {

    private static final Logger LOG = LoggerFactory.getLogger(TaskServiceServerFactoryImpl.class);

    @PersistenceContext(unitName = "jbpm")
    private EntityManager em;

    @Inject
    private UserGroupCallbackImpl callback;

    @Override
    public TaskService createTaskServiceServer() {
        if (!UserGroupCallbackManager.getInstance().existsCallback()) {
            UserGroupCallbackManager.getInstance().setCallback(callback);
        }
        TaskService taskService = new TaskService(em.getEntityManagerFactory(), SystemEventListenerFactory
                .getSystemEventListener());
        return taskService;
    }
}
