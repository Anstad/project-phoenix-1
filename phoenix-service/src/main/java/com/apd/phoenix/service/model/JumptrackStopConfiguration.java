/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class JumptrackStopConfiguration.
 */
@Entity
@Cacheable
@XmlRootElement
public class JumptrackStopConfiguration implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The image type name. */
    @Column(nullable = false, unique = true)
    private String zip;

    @Column(nullable = false)
    private boolean allowShipSunday = false;

    @Column(nullable = false)
    private boolean allowShipMonday = true;

    @Column(nullable = false)
    private boolean allowShipTuesday = true;

    @Column(nullable = false)
    private boolean allowShipWednesday = true;

    @Column(nullable = false)
    private boolean allowShipThursday = true;

    @Column(nullable = false)
    private boolean allowShipFriday = true;

    @Column(nullable = false)
    private boolean allowShipSaturday = false;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public boolean isAllowShipSunday() {
        return allowShipSunday;
    }

    public void setAllowShipSunday(boolean allowShipSunday) {
        this.allowShipSunday = allowShipSunday;
    }

    public boolean isAllowShipMonday() {
        return allowShipMonday;
    }

    public void setAllowShipMonday(boolean allowShipMonday) {
        this.allowShipMonday = allowShipMonday;
    }

    public boolean isAllowShipTuesday() {
        return allowShipTuesday;
    }

    public void setAllowShipTuesday(boolean allowShipTuesday) {
        this.allowShipTuesday = allowShipTuesday;
    }

    public boolean isAllowShipWednesday() {
        return allowShipWednesday;
    }

    public void setAllowShipWednesday(boolean allowShipWednesday) {
        this.allowShipWednesday = allowShipWednesday;
    }

    public boolean isAllowShipThursday() {
        return allowShipThursday;
    }

    public void setAllowShipThursday(boolean allowShipThursday) {
        this.allowShipThursday = allowShipThursday;
    }

    public boolean isAllowShipFriday() {
        return allowShipFriday;
    }

    public void setAllowShipFriday(boolean allowShipFriday) {
        this.allowShipFriday = allowShipFriday;
    }

    public boolean isAllowShipSaturday() {
        return allowShipSaturday;
    }

    public void setAllowShipSaturday(boolean allowShipSaturday) {
        this.allowShipSaturday = allowShipSaturday;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((JumptrackStopConfiguration) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (zip != null && !zip.trim().isEmpty()) {
            result += zip;
        }
        return result;
    }

}
