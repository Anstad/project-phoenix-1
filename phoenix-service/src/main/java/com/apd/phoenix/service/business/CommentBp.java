package com.apd.phoenix.service.business;

import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.persistence.jpa.CommentDao;

@Stateless
@LocalBean
public class CommentBp extends AbstractBp<Comment> {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    public void initDao(CommentDao dao) {
        this.dao = dao;
    }

    public CustomerOrder addComment(CustomerOrder customerOrder, Comment comment) {
        customerOrder.getComments().add(comment);
        return customerOrderBp.update(customerOrder);
    }

    public CustomerOrder addSystemComment(CustomerOrder customerOrder, String content) {
        Comment comment = new Comment();
        comment.setCommentDate(new Date());
        comment.setContent(content);
        comment.setMadeBySystem(true);
        return this.addComment(customerOrder, comment);
    }
}