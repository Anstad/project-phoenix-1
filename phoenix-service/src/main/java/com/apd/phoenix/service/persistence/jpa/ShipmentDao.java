package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.Shipment;
import javax.persistence.Query;

/**
 * Shipment DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ShipmentDao extends AbstractDao<Shipment> {

    public Shipment searchShipmentByTrackingNumber(String trackingNumber) {
        String queryString = "SELECT shipment FROM shipment " + "LEFT JOIN shipment.order "
                + "WHERE shipment.trackingNumber = :trackingNumber";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("trackingNumber", trackingNumber);
        return (Shipment) query.getSingleResult();
    }

}
