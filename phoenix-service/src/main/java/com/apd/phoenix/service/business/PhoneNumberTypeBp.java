package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.PhoneNumberType;
import com.apd.phoenix.service.persistence.jpa.PhoneNumberTypeDao;
import com.apd.phoenix.service.persistence.jpa.UserRequestDao;

@Stateless
@LocalBean
public class PhoneNumberTypeBp extends AbstractBp<PhoneNumberType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PhoneNumberTypeDao dao) {
        this.dao = dao;
    }

    public PhoneNumberType getTypeFromEnum(PhoneNumberType.PhoneNumberTypeEnum phoneNumberTypeEnum) {
        return ((PhoneNumberTypeDao) dao).findByType(phoneNumberTypeEnum);

    }

}
