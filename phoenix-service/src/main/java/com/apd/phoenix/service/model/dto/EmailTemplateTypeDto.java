package com.apd.phoenix.service.model.dto;

public class EmailTemplateTypeDto {

    public static final int INVOICE_ERRORS = 0;
    public static final int ORDER_ACKNOWLEDGMENT_ERRORS = 1;
    public static final int ORDER_APPROVAL_NEEDED = 2;
    public static final int ORDER_CANCELED = 3;
    public static final int ORDER_DENIED = 4;
    public static final int SHIPMENT_NOTIFICATION_ERRORS = 5;
}
