package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.OrderBRMSRecord;

/**
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class OrderBRMSRecordDao extends AbstractDao<OrderBRMSRecord> {

    private static final Logger LOG = LoggerFactory.getLogger(OrderBRMSRecordDao.class);

    @SuppressWarnings("unchecked")
    public List<Boolean> isActive(Long orderId) {
        LOG.trace("EM(orderbrmsdao) is active:  " + entityManager.getClass());
        String hql = "SELECT record.active FROM OrderBRMSRecord AS record WHERE record.entryId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getSessionId(Long orderId) {
        LOG.trace("EM session: " + entityManager.getClass());
        String hql = "SELECT record.sessionId FROM OrderBRMSRecord AS record WHERE record.entryId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getProcessId(Long orderId) {
        LOG.trace("EM session: " + entityManager.getClass());
        String hql = "SELECT record.processId FROM OrderBRMSRecord AS record WHERE record.entryId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<OrderBRMSRecord> getRecordByOrderId(Long orderId) {
        LOG.trace("EM by order id: " + entityManager.getClass());
        String hql = "SELECT record FROM OrderBRMSRecord AS record WHERE record.entryId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<OrderBRMSRecord> getRecordBySessionId(Integer sessionId) {
        LOG.trace("EM by session id: " + entityManager.getClass());
        String hql = "SELECT record FROM OrderBRMSRecord AS record WHERE record.sessionId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", sessionId);
        return (List<OrderBRMSRecord>) query.getSingleResult();
    }

    public List<String> getSnapshot(long orderId) {
        String hql = "SELECT record.snapshot FROM OrderBRMSRecord AS record WHERE record.entryId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", orderId);
        return query.getResultList();
    }

    public void setActive(Long orderId, Boolean active) {
        if (orderId == null || active == null) {
            return;
        }
        String hql = "UPDATE OrderBRMSRecord record SET active = :active WHERE record.entryId = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("active", active);
        query.setParameter("id", orderId);
        query.executeUpdate();
    }
}
