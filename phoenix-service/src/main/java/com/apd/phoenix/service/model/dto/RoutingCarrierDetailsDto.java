package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class RoutingCarrierDetailsDto implements Serializable {

    private static final long serialVersionUID = -3054551321522423446L;

    private String routingSequenceCode;

    private String identificationCode;

    private String transportationMethod;

    private String routing;

    private String shipmentorderStatusCode;

    public String getRoutingSequenceCode() {
        return routingSequenceCode;
    }

    public void setRoutingSequenceCode(String routingSequenceCode) {
        this.routingSequenceCode = routingSequenceCode;
    }

    public String getIdentificationCode() {
        return identificationCode;
    }

    public void setIdentificationCode(String identificationCode) {
        this.identificationCode = identificationCode;
    }

    public String getTransportationMethod() {
        return transportationMethod;
    }

    public void setTransportationMethod(String transportationMethod) {
        this.transportationMethod = transportationMethod;
    }

    public String getRouting() {
        return routing;
    }

    public void setRouting(String routing) {
        this.routing = routing;
    }

    public String getShipmentorderStatusCode() {
        return shipmentorderStatusCode;
    }

    public void setShipmentorderStatusCode(String shipmentorderStatusCode) {
        this.shipmentorderStatusCode = shipmentorderStatusCode;
    }
}
