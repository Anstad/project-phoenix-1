package com.apd.phoenix.service.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;

public class InvoiceService {

    public void startInvoiceRoute() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new VendorInvoiceRouteBuilder());
        context.start();
        context.createProducerTemplate().sendBody("direct:foo", "Some mock data");
    }
}
