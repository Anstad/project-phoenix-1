/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

public class VendorInvoiceDto extends InvoiceDto {

    private static final long serialVersionUID = 3508293466483589282L;
    private PurchaseOrderDto customerOrderDto;

    public PurchaseOrderDto getCustomerOrderDto() {
        return customerOrderDto;
    }

    public void setCustomerOrderDto(PurchaseOrderDto customerOrderDto) {
        this.customerOrderDto = customerOrderDto;
    }
}
