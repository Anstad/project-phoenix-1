/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.persistence.jpa.PersonDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class PersonBp extends AbstractBp<Person> {

    @Inject
    public void initDao(PersonDao dao) {
        this.dao = dao;
    }

    @Override
    public Person cloneEntity(Person person) {
        if (person == null) {
            return null;
        }
        Person toReturn = new Person();
        toReturn.setEmail(person.getEmail());
        toReturn.setFirstName(person.getFirstName());
        toReturn.setLastName(person.getLastName());
        toReturn.setMiddleInitial(person.getMiddleInitial());
        toReturn.setPrefix(person.getPrefix());
        toReturn.setSuffix(person.getSuffix());
        return toReturn;
    }
}
