package com.apd.phoenix.service.edi;

import java.io.Serializable;
import java.util.List;
import com.apd.phoenix.service.invoice.BilledLineItem;

public class EDI810Message implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8546560511670210474L;
    private List<BilledLineItem> billedItems;

    public List<BilledLineItem> getBilledItems() {
        return billedItems;
    }

    public void setBilledItems(List<BilledLineItem> billedItems) {
        this.billedItems = billedItems;
    }
}
