package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
/**
 * This entity is used to store the information relevant to a single shipment.
 * 
 * @author RHC
 *
 */
@Entity
@XmlRootElement
public class Shipment implements Serializable, com.apd.phoenix.service.model.Entity{

    private static final long serialVersionUID = -5688002089658993224L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The shipping partner. */
    @ManyToOne(fetch = FetchType.LAZY)
    private ShippingPartner shippingPartner;

    /** The tracking number. */
    @Column
    private String trackingNumber;
    
    @Column(nullable = false)
    private BigDecimal totalFreightCost = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal totalFreightPrice = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal totalTaxPrice = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal totalShippingCost = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal totalShippingPrice = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal subTotalCost = BigDecimal.ZERO;
    @Column(nullable = false)
    private BigDecimal subTotalPrice = BigDecimal.ZERO;
    

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @ManyToOne
    @Index(name = "SHIPMENT_CUSTORDER_IDX")
    private CustomerOrder order;

    @OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "address_id")	
    @Fetch(FetchMode.JOIN)
    private Address shipFromAddress;
    
    @OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @Fetch(FetchMode.JOIN)
    private Address customerServiceAddress;

    @OneToMany(mappedBy = "shipment")
    private Set<Comment> comments = new HashSet<Comment>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Shipment parentShipment;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "SHIPMENT_SHIPTIME_IDX")
    private Date shipTime = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveredTime;

    @OneToMany(mappedBy = "shipments", cascade = CascadeType.ALL)
    private Set<LineItemXShipment> itemXShipments = new HashSet<>();

    @OneToOne(fetch = FetchType.LAZY)
    private Invoice apdInvoice;
    
    @Column
    private Boolean toCharge;

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Shipment) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the shipping partner.
     *
     * @return the shipping partner
     */
    public ShippingPartner getShippingPartner() {
        return this.shippingPartner;
    }

    /**
     * Gets the tracking number.
     *
     * @return the tracking number
     */
    public String getTrackingNumber() {
        return this.trackingNumber;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Get the parent shipment
     * 
     * @return the parent shipment
     */
    public Shipment getParentShipment() {
        return this.parentShipment;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the shipping partner.
     *
     * @param shippingPartner the new shipping partner
     */
    public void setShippingPartner(final ShippingPartner shippingPartner) {
        this.shippingPartner = shippingPartner;
    }

    /**
     * Sets the tracking number.
     *
     * @param trackingNumber the new tracking number
     */
    public void setTrackingNumber(final String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public CustomerOrder getOrder() {
        return this.order;
    }

    public void setOrder(final CustomerOrder order) {
        this.order = order;
    }

    public Address getShipFromAddress() {
        return this.shipFromAddress;
    }

    public void setShipFromAddress(final Address shipFromAddress) {
        this.shipFromAddress = shipFromAddress;
    }

    public Set<Comment> getComments() {
        return this.comments;
    }

    public void setComments(final Set<Comment> comments) {
        this.comments = comments;
    }

    public Date getShipTime() {
        return this.shipTime;
    }

    public void setShipTime(final Date shipTime) {
        this.shipTime = shipTime;
    }

    public Date getDeliveredTime() {
        return this.deliveredTime;
    }

    public void setDeliveredTime(final Date deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    /**
     * Set the parent shipment
     * 
     * @param parentShipment
     */
    public void setParentShipment(final Shipment parentShipment) {
        this.parentShipment = parentShipment;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (trackingNumber != null && !trackingNumber.trim().isEmpty())
            result += "trackingNumber: " + trackingNumber;
        return result;
    }

    public void newAddress() {
        this.shipFromAddress = new Address();
    }

    public Set<LineItemXShipment> getItemXShipments() {
        return this.itemXShipments;
    }

    public void setItemXShipments(final Set<LineItemXShipment> itemXShipments) {
        this.itemXShipments = itemXShipments;
    }

    public Invoice getInvoice() {
        return apdInvoice;
    }

    public void setInvoice(Invoice invoice) {
        this.apdInvoice = invoice;
    }

    public BigDecimal getTotalFreightCost() {
        return totalFreightCost;
    }

    public void setTotalFreightCost(BigDecimal totalFreightCost) {
        this.totalFreightCost = totalFreightCost;
    }

    public BigDecimal getTotalFreightPrice() {
        return totalFreightPrice;
    }

    public void setTotalFreightPrice(BigDecimal totalFreightPrice) {
        this.totalFreightPrice = totalFreightPrice;
    }

    public BigDecimal getTotalTaxPrice() {
        return totalTaxPrice;
    }

    public void setTotalTaxPrice(BigDecimal totalTaxPrice) {
        this.totalTaxPrice = totalTaxPrice;
    }

    public BigDecimal getTotalShippingCost() {
        return totalShippingCost;
    }

    public void setTotalShippingCost(BigDecimal totalShippingCost) {
        this.totalShippingCost = totalShippingCost;
    }

    public BigDecimal getTotalShippingPrice() {
        return totalShippingPrice;
    }

    public void setTotalShippingPrice(BigDecimal totalShippingPrice) {
        this.totalShippingPrice = totalShippingPrice;
    }    

    public BigDecimal getSubTotalCost() {
        return subTotalCost;
    }

    public void setSubTotalCost(BigDecimal subTotalCost) {
        this.subTotalCost = subTotalCost;
    }

    public BigDecimal getSubTotalPrice() {
        return subTotalPrice;
    }

    public void setSubTotalPrice(BigDecimal subTotalPrice) {
        this.subTotalPrice = subTotalPrice;
    }
    
    public Address getCustomerServiceAddress() {
		return customerServiceAddress;
	}

    public void setCustomerServiceAddress(Address customerServiceAddress) {
            this.customerServiceAddress = customerServiceAddress;
    }

	public Boolean getToCharge() {
		return toCharge;
	}

	public void setToCharge(Boolean toCharge) {
		this.toCharge = toCharge;
	}
    
}
