package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CustomerInvoiceBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.Credential.Terms;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.DtoUtils;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class CustomerInvoiceEmailTemplate extends EmailTemplate<CustomerInvoiceDto> {

    private static final String TEMPLATE = "customer.invoice";
    private static final String ATTACHMENT_PREFIX = "customer-invoice";

    private static final String CONTENT_URL = PropertiesLoader.getAsProperties("aws.integration").getProperty(
            "content.service.secure.domain");
    private static final String APD_LOGO = CONTENT_URL + "/images/logos/APD-logo.png";
    private static final String FACEBOOK = CONTENT_URL + "/images/socialMediaIcons/Facebook.jpg";
    private static final String GOOGLE_PLUS = CONTENT_URL + "/images/socialMediaIcons/GooglePlus.jpg";
    private static final String LINKEDIN = CONTENT_URL + "/images/socialMediaIcons/LinkedIn.jpg";
    private static final String WORDPRESS = CONTENT_URL + "/images/socialMediaIcons/WordPress.jpg";
    private static final String YOUTUBE = CONTENT_URL + "/images/socialMediaIcons/YouTube.jpg";

    private static final String ORDERS_BY_MARQ_CHK = "orders placed under marquette";
    private static final String YES = "yes";
    private static final String RMT_MARQ_NAME = "Marquette Commercial Finance";
    private static final String RMT_APD_NAME = "American Product Distributors";
    private static final String RMT_MARQ_ST = "NW 6333; P.O. Box 1450";
    private static final String RMT_APD_ST = "8350 Arrowridge Blvd";
    private static final String RMT_MARQ_CITY_STATE = "Minneapolis, MN 55485-6333";
    private static final String RMT_APD_CITY_STATE = "Charlotte, NC 28273";
    private static final String SUMMARY_MESSAGE_PROPERTY = "summaryMessage";
    public static final String MINIMUM_ORDER_FEE_SKU = "min_order_fee";

    @Inject
    CustomerInvoiceBp customerInvoiceBp;

    @Override
	public String createBody(CustomerInvoiceDto customerInvoiceDto) throws IOException, TemplateException {
            Map<String, Object> params = new HashMap<>();
            CustomerInvoice customerInvoice = customerInvoiceBp.retrieveCustomerInvoice(customerInvoiceDto);
            CustomerOrder customerOrder = customerInvoice.getShipment().getOrder();
            PurchaseOrderDto purchaseOrderDto = customerInvoiceDto.getShipment().getCustomerOrderDto();
            ShipmentDto shipmentDto = customerInvoiceDto.getShipment();
            if (purchaseOrderDto.retrieveCustomerPoNumber() != null && purchaseOrderDto.retrieveCustomerPoNumber().getValue() != null) {
                params.put("customerPo", purchaseOrderDto.retrieveCustomerPoNumber().getValue());
            }
            else
            {
                params.put("customerPo", "N/A");
            }
            params.put("apdPo", purchaseOrderDto.retrieveApdPoNumber().getValue());
            params.put("skuOnEmailFlag", purchaseOrderDto.getCredential().getProperties().get(CredentialPropertyTypeEnum.SKU_ON_EMAIL.getValue()));
            params.put("trackingNumber", customerInvoice.getShipment().getTrackingNumber());

            params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
            params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
            BigDecimal minimumOrderFee = BigDecimal.ZERO;
            List<LineItemXShipmentDto> validItems = new ArrayList<>();
            for (LineItemXShipmentDto lixs : shipmentDto.getLineItemXShipments()) {
                if (lixs.getLineItemDto().getFee() && MINIMUM_ORDER_FEE_SKU.equals(lixs.getLineItemDto().getApdSku())) {
                    minimumOrderFee = lixs.getLineItemDto().getUnitPrice();
                } else {
                    validItems.add(lixs);
                }
            }
            params.put("items", validItems);
            params.put("minimumOrderFee", minimumOrderFee);
            params.put("merchandiseTotal", shipmentDto.calculateLIXSTotalNoFees());
            params.put("shippingTotal", shipmentDto.calculateShippingTotal());	
            params.put("taxTotal", shipmentDto.calculateTaxTotal());
            params.put("orderTotal", (shipmentDto.calculateLIXSTotal().add(shipmentDto.calculateShippingTotal().add(shipmentDto.calculateTaxTotal()))));

            String terms = Terms.TEN.getLabel();
            if (customerOrder.getCredential().getTerms() != null) {
            	terms = customerOrder.getCredential().getTerms().getLabel();
            }
            
            params.put("paymentTerms", terms);
            params.put("orderDate", purchaseOrderDto.getOrderDate());
            params.put("invoiceDate", customerInvoiceDto.getCreatedDate());
            params.put("invoiceNumber", customerInvoiceDto.getInvoiceNumber());
            params.put("invoiceNumber_woAPD", customerInvoiceDto.getInvoiceNumber().replace("APD", ""));

            /*Logic For Remit Address Account. If it is a Marquette Commercial Finance, then we want to show that account. If not then
             * we should show APD.*/		 
            Boolean isMarquette = false; //Initialize the Boolean Flag to 'false'.

            for(CredentialXCredentialPropertyType property: customerOrder.getCredential().getProperties())
            {
                if(ORDERS_BY_MARQ_CHK.equals(property.getType().getName()) && YES.equals(property.getValue().toLowerCase()))
                {
                    isMarquette = true;
                    break;
                }
            }

            params.put("csPhone", customerOrder.getCredential().getCsrPhone());
            params.put("remitName", (isMarquette? RMT_MARQ_NAME: RMT_APD_NAME));
            params.put("remitStreet", (isMarquette? RMT_MARQ_ST: RMT_APD_ST));
            params.put("remitCityState", (isMarquette? RMT_MARQ_CITY_STATE: RMT_APD_CITY_STATE));

            /*Check to see if the customer order contains a desktop and department with the ship to address. If so, we need show those values in
             * the email.*/
            String desktopString = null;
            String departmentString = null;
            String costCenter = null;
            String phoneNumber = null;
            String divDept = null;
            String mailStop = null;
            String pole = null;
            String shipName = null;
            if(customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null) {
                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getDesktop()))
                {
                    desktopString =  customerOrder.getAddress().getMiscShipTo().getDesktop();
                }

                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getDepartment())){
                    departmentString = customerOrder.getAddress().getMiscShipTo().getDepartment();
                }

                if(customerOrder.getAssignedCostCenter() != null && customerOrder.getAssignedCostCenter().getCostCenter() != null)
                {
                    costCenter = customerOrder.getAssignedCostCenter().getCostCenter().getName();
                }

                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getMailStop()))
                {
                    mailStop = customerOrder.getAddress().getMiscShipTo().getMailStop();
                }

                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getPol()))
                {
                    pole = customerOrder.getAddress().getMiscShipTo().getPol();
                }

                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getRequesterPhone()))
                {
                    phoneNumber = customerOrder.getAddress().getMiscShipTo().getRequesterPhone();
                }

                if(StringUtils.isNotEmpty(customerOrder.getAddress().getMiscShipTo().getDeliverToName()))
                {
                    shipName = customerOrder.getAddress().getMiscShipTo().getDeliverToName();
                }
            }
            
            Boolean summary = customerOrder.getPaymentInformation().isSummary();
            Properties templateProperties = PropertiesLoader.getAsProperties(EmailFactoryBp.class, this.getTemplateLocation());
            String summaryMessage = templateProperties.getProperty(SUMMARY_MESSAGE_PROPERTY);
            params.put("summary", summary ? summaryMessage : null);
            params.put("desktop", this.getAddressFieldValue(desktopString));
    		params.put("shipName", StringUtils.isNotBlank(shipName) ? shipName : customerOrder.getAddress().getName());
            String desktopLabel = "Desktop";
            if(customerOrder.getCredential()!=null && customerOrder.getCredential().getCashoutPage() != null){
                CashoutPageXFieldDto deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop");
                if (deskFieldDefn == null) {
                	deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop combobox");
                }
                if (deskFieldDefn == null) {
                	deskFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "desktop free");
                }
            	if (deskFieldDefn != null) {
            		if (deskFieldDefn.isIncludeLabelInMapping() != null && deskFieldDefn.isIncludeLabelInMapping()) {
            			desktopLabel = this.getAddressFieldLabel(desktopString);
            		} else if (StringUtils.isNotBlank(deskFieldDefn.getLabel())) {
                		desktopLabel = deskFieldDefn.getLabel();
                	}
            	}
            }
            params.put("desktopLabel", desktopLabel);
            params.put("department", this.getAddressFieldValue(departmentString));
            String departmentLabel = "Department";
            if(customerOrder.getCredential()!=null && customerOrder.getCredential().getCashoutPage() != null){
                CashoutPageXFieldDto deptFieldDefn = DtoUtils.findCashoutPageField(customerOrder.getCredential(), "department");
                if (deptFieldDefn != null) {
    	        	if (deptFieldDefn.isIncludeLabelInMapping() != null && deptFieldDefn.isIncludeLabelInMapping()) {
    	        		departmentLabel = this.getAddressFieldLabel(departmentString);
    	        	} else if (StringUtils.isNotBlank(deptFieldDefn.getLabel())) {
    	        		departmentLabel = deptFieldDefn.getLabel();
    	        	}
                }
            }
            params.put("departmentLabel", departmentLabel);
            params.put("costcenter", this.getAddressFieldValue(costCenter));
            params.put("costCenterLabel", StringUtils.isBlank(customerOrder.getAssignedCostCenterLabel()) ? customerOrder.getAssignedCostCenterLabel() : "Cost Center");
            params.put("mailStop", this.getAddressFieldValue(mailStop));
            params.put("mailStopLabel", StringEscape.getMappingLabel(mailStop, "Mail Stop"));
            params.put("pole", this.getAddressFieldValue(pole));
            params.put("poleLabel", StringEscape.getMappingLabel(pole, "Pole"));
            params.put("phone", this.getAddressFieldValue(phoneNumber));
            params.put("phoneLabel", StringEscape.getMappingLabel(phoneNumber, "Phone"));

            //Logos
            params.put("apdLogo", APD_LOGO);
            params.put("facebookLogo", FACEBOOK);
            params.put("googlePlusLogo", GOOGLE_PLUS);
            params.put("linkedInLogo", LINKEDIN);
            params.put("wordpressLogo", WORDPRESS);
            params.put("youTubeLogo", YOUTUBE);

            return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(CustomerInvoiceDto customerInvoiceDto) throws NoAttachmentContentException {
        CustomerInvoice customerInvoice = customerInvoiceBp.retrieveCustomerInvoice(customerInvoiceDto);
        Attachment attachment = new Attachment();
        if (customerInvoice != null) {
            attachment.setFileName(ATTACHMENT_PREFIX + customerInvoiceDto.getInvoiceNumber() + PDF_EXT);
            attachment.setMimeType(MimeType.pdf);
            InputStream is = reportService.generateInvoiceDebitPdf(customerInvoice.getId().toString());
            if (is == null) {
                throw new NoAttachmentContentException();
            }
            attachment.setContent(is);
        }
        return attachment;
    }

}
