package com.apd.phoenix.service.business;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;

/**
 * This is a comparator for sorting a list of entities in our data model. It defaults to checking the "name" and the "type" of the 
 * entity, if such fields exist, and comparing the entity.toString() values. If you want to implement an entity-specific 
 * comparator, add the comparator as a public static inner class to the entity. Here's an example of the implementation, for SystemUser:
 *
 * <p/>
 * <pre>
 *  public static class SystemUserComparator implements Comparator&lt;SystemUser&gt; {
 *
 *      {@literal @}Override
 *      public int compare(SystemUser arg0, SystemUser arg1) {
 *          if (!StringUtils.isBlank(arg0.getLogin()) && !StringUtils.isBlank(arg1.getLogin())) {
 *              return arg0.getLogin().compareTo(arg1.getLogin());
 *          }
 *          return (new GenericComparator()).compare(arg0, arg1);
 *      }
 *  }</pre>
 * 
 * @author RHC
 *
 */
public class EntityComparator implements Comparator<Object> {

    @Override
    public int compare(Object arg0, Object arg1) {
        //null entries are last
        if (arg0 == null) {
            return 1;
        }
        if (arg1 == null) {
            return -1;
        }
        //If both classes are strings, returns the comparison of the strings
        if (arg0.getClass().equals(String.class) && arg1.getClass().equals(String.class)) {
            return ((String) arg0).compareToIgnoreCase((String) arg1);
        }
        if (arg0.getClass().equals(arg1.getClass())) {
            //first, checks all classes on the entity's class itself
            for (Class<?> innerClass : arg0.getClass().getDeclaredClasses()) {
                Integer compareResult = this.compareWithInnerClass(innerClass, arg0, arg1);
                if (compareResult != null) {
                    return compareResult;
                }
            }
            //then checks all inherited classes as well
            for (Class<?> innerClass : arg0.getClass().getClasses()) {
                Integer compareResult = this.compareWithInnerClass(innerClass, arg0, arg1);
                if (compareResult != null) {
                    return compareResult;
                }
            }
        }

        return (new GenericComparator()).compare(arg0, arg1);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	private Integer compareWithInnerClass(Class<?> innerClass, Object arg0, Object arg1) {
		for (Class<?> innerClassInterface : innerClass.getInterfaces()) {
			if (innerClassInterface.isAssignableFrom(Comparator.class)) {
				try {
					Comparator innerClassComparator = (Comparator) innerClass.getConstructor().newInstance();
					return innerClassComparator.compare(arg0, arg1);
				} catch (InstantiationException
						| IllegalAccessException
						| IllegalArgumentException
						| InvocationTargetException
						| NoSuchMethodException 
						| SecurityException
						| ClassCastException e) {
					//use default
				}
			}
		}
		return null;
    }

    public static class GenericComparator implements Comparator<Object> {

        @Override
        public int compare(Object arg0, Object arg1) {
            try {
                //if the classes have a method "getName" that returns a String, returns comparison of the names
                Method nameGetter = arg0.getClass().getMethod("getName", (Class<?>[]) null);
                String name0 = (String) nameGetter.invoke(arg0, (Object[]) null);
                String name1 = (String) nameGetter.invoke(arg1, (Object[]) null);
                return name0.compareToIgnoreCase(name1);
            }
            catch (Exception e1) {
                try {
                    //if the classes have a method "getType", returns the comparison of the types
                    Method typeGetter = arg0.getClass().getMethod("getType", (Class<?>[]) null);
                    Object type0 = typeGetter.invoke(arg0, (Object[]) null);
                    Object type1 = typeGetter.invoke(arg1, (Object[]) null);
                    return (new EntityComparator()).compare(type0, type1);
                }
                catch (Exception e2) {
                    //otherwise, calls toString and compares the results.
                    return (arg0.toString()).compareToIgnoreCase(arg1.toString());
                }
            }
        }

    }
}
