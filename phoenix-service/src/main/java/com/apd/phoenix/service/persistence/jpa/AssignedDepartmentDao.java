package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.AssignedDepartment;

/**
 * Session Bean implementation class AssignedDepartmentDao
 */
@Stateless
@LocalBean
public class AssignedDepartmentDao extends AbstractDao<AssignedDepartment> {

}
