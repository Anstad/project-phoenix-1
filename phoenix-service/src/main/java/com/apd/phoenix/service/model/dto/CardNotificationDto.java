/*
 * 
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class CardNotificationDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long orderId;

    private String transactionKey;

    private EventTypeDto eventType;

    private ShipmentDto shipmentDto;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getTransactionKey() {
        return transactionKey;
    }

    public void setTransactionKey(String transactionKey) {
        this.transactionKey = transactionKey;
    }

    public EventTypeDto getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeDto eventType) {
        this.eventType = eventType;
    }

    public ShipmentDto getShipmentDto() {
        return shipmentDto;
    }

    public void setShipmentDto(ShipmentDto shipmentDto) {
        this.shipmentDto = shipmentDto;
    }

}