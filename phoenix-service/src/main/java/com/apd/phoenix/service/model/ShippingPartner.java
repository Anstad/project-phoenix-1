/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

// TODO: Auto-generated Javadoc
/**
 * The Class ShippingPartner.
 */
@Entity
@XmlRootElement
@Audited
public class ShippingPartner implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -3727919577945264012L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The name. */
    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false, unique = true)
    private String scacCode;

    @Column(nullable = false, unique = true)
    private String duns;

    /** The shipments. */
    @NotAudited
    private @OneToMany(mappedBy = "shippingPartner", cascade = CascadeType.ALL)
    Set<Shipment> shipments = new HashSet<Shipment>();

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ShippingPartner) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the shipments.
     *
     * @return the shipments
     */
    public Set<Shipment> getShipments() {
        return this.shipments;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the shipments.
     *
     * @param shipments the new shipments
     */
    public void setShipments(final Set<Shipment> shipments) {
        this.shipments = shipments;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }

    public String getScacCode() {
        return scacCode;
    }

    public void setScacCode(String scacCode) {
        this.scacCode = scacCode;
    }

    public String getDuns() {
        return duns;
    }

    public void setDuns(String duns) {
        this.duns = duns;
    }

}