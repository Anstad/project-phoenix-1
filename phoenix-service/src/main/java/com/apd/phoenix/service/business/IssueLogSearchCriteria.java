/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.List;
import com.apd.phoenix.service.model.ContactLog.TicketStatus;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.IssueLog.IssueCategory;

public class IssueLogSearchCriteria {

    private String ticketNumber;
    private String department;
    private String contactName;
    private String contactNumber;
    private String contactEmail;
    private CustomerOrder customerOrder;
    private Date startDate;
    private Date endDate;
    private IssueCategory issueCategory;
    private TicketStatus ticketStatus;
    private Integer start;
    private Integer pageSize;
    private List<Long> accountWhiteList;

    public IssueLogSearchCriteria(String ticketNumber, String department, String contactName, String contactNumber,
            String contactEmail, CustomerOrder customerOrder, Date startDate, Date endDate,
            IssueCategory issueCategory, TicketStatus ticketStatus, Integer inStart, Integer inPageSize,
            List<Long> accountWhiteList) {
        this.ticketNumber = ticketNumber;
        this.department = department;
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactEmail = contactEmail;
        this.setCustomerOrder(customerOrder);
        this.startDate = startDate;
        this.endDate = endDate;
        this.issueCategory = issueCategory;
        this.ticketStatus = ticketStatus;
        this.start = inStart;
        this.pageSize = inPageSize;
        this.accountWhiteList = accountWhiteList;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public IssueCategory getIssueCategory() {
        return issueCategory;
    }

    public void setIssueCategory(IssueCategory issueCategory) {
        this.issueCategory = issueCategory;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public List<Long> getAccountWhiteList() {
        return accountWhiteList;
    }

    public void setAccountWhiteList(List<Long> accountWhiteList) {
        this.accountWhiteList = accountWhiteList;
    }

}
