package com.apd.phoenix.service.workflow;

import java.util.Date;
import org.jbpm.task.Status;
import org.jbpm.task.User;
import org.jbpm.task.query.TaskSummary;

public class PhoenixTaskSummary extends TaskSummary {

    public PhoenixTaskSummary(Long id, Long processInstanceId, String name, String subject, String description,
            Status status, int priority, boolean skippable, User actualOwner, User createdBy, Date createdOn,
            Date activationTime, Date expirationTime, String processId, int processSessionId, String account) {
        super(id, processInstanceId, name, subject, description, status, priority, skippable, actualOwner, createdBy,
                createdOn, activationTime, expirationTime, processId, processSessionId);
        this.account = account;
    }

    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
