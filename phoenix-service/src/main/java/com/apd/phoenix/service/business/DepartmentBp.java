package com.apd.phoenix.service.business;

import java.util.List;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Department;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.persistence.jpa.DepartmentDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

/**
 * This class provides business process methods for System Users.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class DepartmentBp extends AbstractBp<Department> {

    private static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";

    @Inject
    private SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(DepartmentDao dao) {
        this.dao = dao;
    }

    public List<Department> filteredDepartmentList(String name) {
        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            return ((DepartmentDao) dao).filteredDepartmentList(name, systemUserDao.getAccountIds(user));
        }
        else {
            return ((DepartmentDao) dao).filteredDepartmentList(name, null);
        }
    }
}
