package com.apd.phoenix.service.business;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.TaxRateLog;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogAction;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogStatus;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogType;
import com.apd.phoenix.service.persistence.jpa.TaxRateDao;

/**
 * This class provides business process methods for TaxRate.
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class TaxRateBp extends AbstractBp<TaxRate> {

    @Inject
    private ZipPlusFourBp plusFourBp;

    @Inject
    private TaxRateLogBp logBp;

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(TaxRateDao dao) {
        this.dao = dao;
    }

    /**
     * Given several parameters, returns the appropriate TaxRate object
     *
     * @param zip
     * @param county
     * @param city
     * @param state
     * @param geocode
     * @return
     */
    public TaxRate getTaxRate(String zip, String county, String city, String state, String geocode) {
        TaxRate search = new TaxRate();
        String[] parsedZip = this.parseRawZip(zip);
        search.setZip(parsedZip[0]);
        String plusFour = parsedZip[1];
        if (!StringUtils.isEmpty(county)) {
            search.setCounty(county.toUpperCase());
        }
        if (!StringUtils.isEmpty(city)) {
            search.setCity(city.toUpperCase());
        }
        if (!StringUtils.isEmpty(state)) {
            search.setState(state.toUpperCase());
        }
        if (!StringUtils.isEmpty(geocode)) {
            search.setGeocode(geocode);
        }
        //if the county isn't specified, but the +4 value is, uses the county FIPs from the +4 value
        if (StringUtils.isEmpty(geocode) && StringUtils.isEmpty(county) && !StringUtils.isEmpty(plusFour)) {
            search.setCountyFips(plusFourBp.countyFipsFromValues(search.getZip(), plusFour));
        }
        List<TaxRate> resultList = this.dao.searchByExactExample(search, 0, 0);
        //iterates through the results, removing any that have expired
        Iterator<TaxRate> rateIterator = resultList.iterator();
        while (rateIterator.hasNext()) {
            TaxRate rate = rateIterator.next();
            if (rate.getExpireDate() != null) {
                rateIterator.remove();
            }
        }
        //for multiple results, if one is the general default, uses it
        for (TaxRate rate : resultList) {
            if ("y".equalsIgnoreCase(rate.getGeneralDefault())) {
                return rate;
            }
        }
        //otherwise if one is the county default, uses it
        for (TaxRate rate : resultList) {
            if ("y".equalsIgnoreCase(rate.getCountyDefault())) {
                return rate;
            }
        }
        //if there are no defaults, uses the first element of the list
        if (!resultList.isEmpty()) {
            return resultList.get(0);
        }
        //if no results, but more than just the zip is set, finds the tax rate with just the zip
        if (StringUtils.isNotBlank(zip)
                && (StringUtils.isNotBlank(county) || StringUtils.isNotBlank(city) || StringUtils.isNotBlank(state) || StringUtils
                        .isNotBlank(geocode))) {
            return this.getTaxRate(zip, null, null, null, null);
        }
        //finally, returns a blank TaxRate object if there were no results
        return new TaxRate();
    }

    public List<TaxRate> getTaxRates(String zip) {
        TaxRate search = new TaxRate();
        //will store the +4 value, if there is one
        String plusFour = null;
        if (!StringUtils.isEmpty(zip)) {
            String[] parsedZip = this.parseRawZip(zip);
            search.setZip(parsedZip[0]);
            plusFour = parsedZip[1];
        }
        //if the county isn't specified, but the +4 value is, uses the county FIPs from the +4 value
        if (!StringUtils.isEmpty(plusFour)) {
            search.setCountyFips(plusFourBp.countyFipsFromValues(search.getZip(), plusFour));
        }
        List<TaxRate> resultList = this.dao.searchByExactExample(search, 0, 0);

        Collections.sort(resultList, new EntityComparator());
        //iterates through the results, removing any that have expired
        Iterator<TaxRate> rateIterator = resultList.iterator();
        while (rateIterator.hasNext()) {
            TaxRate rate = rateIterator.next();
            if (rate.getExpireDate() != null) {
                rateIterator.remove();
            }
        }
        return resultList;
    }

    /**
     * This takes a list of Objects and persists them
     *
     * @param rates
     */
    public void createTaxRates(List<TaxRate> rates) {
        for (TaxRate o : rates) {
            if (o == null) {
                continue;
            }
            try {
                this.create(o);
                logBp.create(new TaxRateLog(TaxRateLogType.TAX_RATE, TaxRateLogAction.INSERT, TaxRateLogStatus.SUCCESS,
                        o.getZip(), o.getGeocode()));
            }
            catch (Exception e) {
                LOG.error("Error when adding tax rate, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.TAX_RATE, TaxRateLogAction.INSERT, TaxRateLogStatus.FAILURE,
                        o.getZip(), o.getGeocode()));
            }
        }
    }

    public boolean isValidZipCode(String zipCode) {
        if (StringUtils.isBlank(zipCode)) {
            return false;
        }
        return ((TaxRateDao) dao).isValidZipCode(zipCode);
    }

    public void deleteTaxRate(TaxRate rate) {
        TaxRate searchRate = new TaxRate();
        //zip, geocode, and city location uniquely specifies a record
        searchRate.setZip(rate.getZip());
        searchRate.setGeocode(rate.getGeocode());
        List<TaxRate> searchList = this.searchByExactExample(searchRate, 0, 0);
        for (TaxRate foundRate : searchList) {
            try {
                this.delete(foundRate.getId(), TaxRate.class);
                logBp.create(new TaxRateLog(TaxRateLogType.TAX_RATE, TaxRateLogAction.DELETE, TaxRateLogStatus.SUCCESS,
                        rate.getZip(), rate.getGeocode()));
            }
            catch (Exception e) {
                LOG.error("Error when deleting tax rate, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.TAX_RATE, TaxRateLogAction.DELETE, TaxRateLogStatus.FAILURE,
                        rate.getZip(), rate.getGeocode()));
            }
        }
    }

    private String[] parseRawZip(String rawZip) {
        String[] toReturn = new String[2];
        toReturn[0] = null;
        toReturn[1] = null;

        if (!StringUtils.isEmpty(rawZip)) {
            switch (rawZip.length()) {
                case 5:
                    //standard five-digit zip
                    toReturn[0] = rawZip;
                    break;
                case 10:
                    //zip with dash and +4
                    toReturn[0] = rawZip.substring(0, 5);
                    toReturn[1] = rawZip.substring(6, 10);
                    break;
                case 9:
                    //EDI-style zip plus four without dash
                    toReturn[0] = rawZip.substring(0, 5);
                    toReturn[1] = rawZip.substring(5, 9);
                    break;
                default:
                    //default action
                    toReturn[0] = rawZip;
                    break;
            }
        }

        return toReturn;
    }
}
