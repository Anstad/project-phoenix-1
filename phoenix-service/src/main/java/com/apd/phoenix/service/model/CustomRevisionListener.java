package com.apd.phoenix.service.model;

import java.util.Date;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.hibernate.envers.RevisionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomRevisionListener implements RevisionListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomRevisionListener.class);

    public void newRevision(Object revisionEntity) {
        CustomRevision revision = (CustomRevision) revisionEntity;
        revision.setRevisionDate(new Date());
        try {
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
            revision.setUsername(context.getUserPrincipal().getName());
        }
        catch (Exception e) {
            revision.setUsername("SYSTEM");
        }
    }

}