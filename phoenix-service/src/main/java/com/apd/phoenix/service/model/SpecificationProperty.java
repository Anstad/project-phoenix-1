/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

// TODO: Get clarification on the relationships.  Namely Descriptions, name, and value.  http://www.oagi.org/oagis/9_2/Documentation/OAGIS/9_2/BODs/Standalone/SyncItemMaster.htm#element_Specification
/**
 * The Class SpecificationProperty.
 */
@Entity
@XmlRootElement
public class SpecificationProperty implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7247874812474299657L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The sequence. */
    @Column
    private Integer sequence;

    /** The property name. */
    @Column
    private String name;

    /** The property value. */
    @Column(length = 400)
    private String value;

    /** The specification description. */
    @OneToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "specificationproperty_id")
    @Index(name = "sd_spid_ix")
    private Set<SpecificationDescription> specificationDescriptions;

    /**
     * Gets the specification description.
     *
     * @return the specification description
     */
    public Set<SpecificationDescription> getSpecificationDescriptions() {
        return this.specificationDescriptions;
    }

    /**
     * Sets the specification description.
     *
     * @param specificationDescription the new specification description
     */
    public void setSpecificationDescriptions(final Set<SpecificationDescription> specificationDescriptions) {
        this.specificationDescriptions = specificationDescriptions;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((SpecificationProperty) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the sequence.
     *
     * @return the sequence
     */
    public Integer getSequence() {
        return this.sequence;
    }

    /**
     * Sets the sequence.
     *
     * @param sequence the new sequence
     */
    public void setSequence(final Integer sequence) {
        this.sequence = sequence;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SpecificationProperty [version=" + version + ", sequence=" + sequence + ", name=" + name + ", value="
                + value + "]";
    }

}