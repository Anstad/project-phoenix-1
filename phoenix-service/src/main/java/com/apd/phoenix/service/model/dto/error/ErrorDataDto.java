package com.apd.phoenix.service.model.dto.error;

import java.util.List;

public class ErrorDataDto {

    private List<String> errorSpecifications;
    private List<String> recommendedActions;
    private String techErrorDescription;

    /**
     * @return the errorSpecifications
     */
    public List<String> getErrorSpecifications() {
        return errorSpecifications;
    }

    /**
     * @param errorSpecifications the errorSpecifications to set
     */
    public void setErrorSpecifications(List<String> errorSpecifications) {
        this.errorSpecifications = errorSpecifications;
    }

    /**
     * @return the recommendedActions
     */
    public List<String> getRecommendedActions() {
        return recommendedActions;
    }

    /**
     * @param recommendedActions the recommendedActions to set
     */
    public void setRecommendedActions(List<String> recommendedActions) {
        this.recommendedActions = recommendedActions;
    }

    /**
     * @return the techErrorDescription
     */
    public String getTechErrorDescription() {
        return techErrorDescription;
    }

    /**
     * @param techErrorDescription the techErrorDescription to set
     */
    public void setTechErrorDescription(String techErrorDescription) {
        this.techErrorDescription = techErrorDescription;
    }

}
