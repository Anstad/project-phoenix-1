package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class OrderStatusDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String description;

    private String value;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
