package com.apd.phoenix.service.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountPropertyType.AccountPropertyTypeEnum;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.dto.AccountDto;

public class AccountPropertyUtils extends AbstractPropertyUtils {

    public static boolean equals(AccountPropertyTypeEnum propertyType, Object value, Account accountEntity) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && accountEntity != null && accountEntity.getProperties() != null) {
            for (AccountXAccountPropertyType property : accountEntity.getProperties()) {
                if (property.getType().getName().equalsIgnoreCase(propertyType.getValue())) {
                    lhs = normalizeValue(property.getValue());
                    break;
                }
            }
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        return StringUtils.equalsIgnoreCase(lhs, rhs) || semanticEquals(lhs, rhs);
    }

    public static boolean equals(AccountPropertyTypeEnum propertyType, Object value, AccountDto accountDto) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && accountDto != null && accountDto.getProperties() != null
                && accountDto.getProperties().containsKey(propertyType.getValue())) {
            lhs = normalizeValue((String) accountDto.getProperties().get(propertyType.getValue()));
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        return StringUtils.equalsIgnoreCase(lhs, rhs) || semanticEquals(lhs, rhs);
    }

    public static boolean contains(AccountPropertyTypeEnum propertyType, Object value, Account accountEntity,
            boolean multiValueProperty) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && accountEntity != null && accountEntity.getProperties() != null) {
            for (AccountXAccountPropertyType property : accountEntity.getProperties()) {
                if (property.getType().getName().equalsIgnoreCase(propertyType.getValue())) {
                    lhs = normalizeValue(property.getValue());
                    break;
                }
            }
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        else if (StringUtils.isBlank(lhs) && StringUtils.isBlank(rhs)) {
            return true;
        }
        if (multiValueProperty) {
            List<String> elements = new ArrayList<String>();
            for (String element : Arrays.asList(lhs.split(","))) {
                String normalizedElement = normalizeValue(element);
                if (normalizedElement != null) {
                    elements.add(normalizedElement);
                }
            }
            return elements.contains(rhs);
        }
        else {
            return lhs.contains(rhs);
        }
    }

    public static boolean contains(AccountPropertyTypeEnum propertyType, Object value, AccountDto accountDto,
            boolean multiValueProperty) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && accountDto != null && accountDto.getProperties() != null
                && accountDto.getProperties().containsKey(propertyType.getValue())) {
            lhs = normalizeValue((String) accountDto.getProperties().get(propertyType.getValue()));
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        else if (StringUtils.isBlank(lhs) && StringUtils.isBlank(rhs)) {
            return true;
        }
        if (multiValueProperty) {
            List<String> elements = new ArrayList<String>();
            for (String element : Arrays.asList(lhs.split(","))) {
                String normalizedElement = normalizeValue(element);
                if (normalizedElement != null) {
                    elements.add(normalizedElement);
                }
            }
            return elements.contains(rhs);
        }
        else {
            return lhs.contains(rhs);
        }
    }
}
