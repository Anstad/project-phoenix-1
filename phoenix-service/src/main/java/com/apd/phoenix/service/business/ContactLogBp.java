package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ServiceLog;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;
import com.apd.phoenix.service.persistence.jpa.ServiceLogDao;

/**
 * This class provides business process methods for CustomeCost.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ContactLogBp extends AbstractBp<ServiceLog> {

    @Inject
    private SequenceDao sequenceDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ServiceLogDao dao) {
        this.dao = dao;
    }

    public String newTicketNumber() {
        return sequenceDao.nextVal(Sequence.ISSUE_LOG_TICKET_NUM) + "";
    }
}
