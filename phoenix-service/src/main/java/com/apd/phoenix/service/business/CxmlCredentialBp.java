/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.persistence.jpa.CxmlCredentialDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CxmlCredentialBp extends AbstractBp<CxmlCredential> {

    private CxmlCredentialDao dao;

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(CxmlCredentialDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    @Override
    public CxmlCredential cloneEntity(CxmlCredential toClone) {
        CxmlCredential newCxmlCred = new CxmlCredential();
        newCxmlCred.setDomain(toClone.getDomain());
        newCxmlCred.setIdentifier(toClone.getIdentifier());
        return newCxmlCred;
    }
}
