package com.apd.phoenix.service.solr;

import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import java.net.MalformedURLException;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.CloudSolrServer;

@Startup
@Singleton
public class SolrServiceBean {

    //should be setup in EJB configuration.
    //TODO look into EJB ocnfiguration
    private final String zooKeeperNodeUrlList = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("zooKeeperUrlList");
    private final String productCollection = EcommercePropertiesLoader.getInstance().getEcommerceProperties()
            .getString("defaultSearchCollection");
    private CloudSolrServer solrServer;

    public SolrServer getSolrServer() {
        return solrServer;
    }

    @PostConstruct
    private void setup() throws MalformedURLException {
        //per: http://wiki.apache.org/solr/Solrj#Directly_adding_POJOs_to_Solr
        solrServer = new PhoenixSolrServer(zooKeeperNodeUrlList);
        solrServer.setDefaultCollection(productCollection);
        //     solrServer = new ConcurrentUpdateSolrServer(url, 1, 2);
    }

    @PreDestroy
    private void destroy() {
        solrServer.shutdown();
    }
}
