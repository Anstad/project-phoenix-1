package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import freemarker.template.TemplateException;

/**
 *
 * @author anicholson
 */
@Stateless
@LocalBean
public class BackorderNoticeEmailTemplate extends EmailTemplate<POAcknowledgementDto> {

    private static final String template = "backorder.notice";
    private static final String BO_NOTICE_FILE_PREFIX = "backorder-notice-";

    @Override
    public String createBody(POAcknowledgementDto poAckDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        params.put("apdPo", poAckDto.getApdPo());
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return template;
    }

    @Override
    public Attachment createAttachment(POAcknowledgementDto poAckDto) throws NoAttachmentContentException {
        String apdPo = poAckDto.getApdPo();
        Attachment boNotice = new Attachment();
        boNotice.setFileName(BO_NOTICE_FILE_PREFIX + apdPo + PDF_EXT);
        boNotice.setMimeType(Attachment.MimeType.pdf);
        InputStream is = reportService.generateBackorderNoticePdf(apdPo);
        if (is != null) {
            boNotice.setContent(is);
        }
        else {
            throw new NoAttachmentContentException();
        }
        return boNotice;
    }

}
