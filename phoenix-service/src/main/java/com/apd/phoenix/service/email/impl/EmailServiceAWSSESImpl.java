package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.activation.DataHandler;
import javax.ejb.Stateless;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amazonaws.services.simpleemail.AWSJavaMailTransport;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;

/**
 * Amazon Simple Email Service Implementation
 * 
 * @author RHC
 * 
 */
@Stateless
public class EmailServiceAWSSESImpl implements EmailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceAWSSESImpl.class);

    public MimeMessage prepareRawMessage(String from, Set<String> replyTo, Set<String> bcc, Set<String> to,
            Set<String> cc, String subject, String body, List<Attachment> attachments) {
        Session session = getSession();
        MimeMessage msg = new MimeMessage(session);

        try {
            msg.setFrom(new InternetAddress(from));
            this.setReplyTo(msg, replyTo);
            this.setRecipients(msg, to, Message.RecipientType.TO);
            this.setRecipients(msg, cc, Message.RecipientType.CC);
            this.setRecipients(msg, bcc, Message.RecipientType.BCC);
            // Subject
            msg.setSubject(subject);
        }
        catch (AddressException e) {
            LOGGER.error("Error creating address array", e);
            this.setCustomerServiceRecipients(msg);
        }
        catch (MessagingException e) {
            LOGGER.error("error creating message", e);
            this.setCustomerServiceRecipients(msg);
        }

        // Add a MIME part to the message
        MimeMultipart mp = new MimeMultipart();

        // Add Text Body

        BodyPart textPart = new MimeBodyPart();
        String textBody = body;
        try {
            textPart.setContent(textBody, "text/html");
            mp.addBodyPart(textPart);

        }
        catch (MessagingException e) {
            LOGGER.error("Error create main message body");
            LOGGER.error("An error occured:", e);
            ;
            return null;
        }

        // Add Attachments
        if (attachments != null) {
            for (Attachment attachment : attachments) {
                try {
                    MimeBodyPart attachmentPart = createAttachmentPart(attachment);
                    mp.addBodyPart(attachmentPart);
                }
                catch (Exception e) {
                    LOGGER.error("Error create attachment");
                    LOGGER.error("An error occured:", e);
                    ;
                    return null;
                }
            }
        }

        // Set the content of the message
        try {
            msg.setContent(mp);

        }
        catch (MessagingException e) {
            LOGGER.error("Error setting message content from parts");
            LOGGER.error("An error occured:", e);
            ;
            return null;
        }
        return msg;
    }

    private Session getSession() {
        // Prepare session for AWS Transport
        Properties awsProperties = PropertiesLoader.getAsProperties("aws.integration");
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "aws");
        props.setProperty("mail.aws.user", awsProperties.getProperty("email.service.access-key"));
        props.setProperty("mail.aws.password", awsProperties.getProperty("email.service.secret-key"));

        Session session = Session.getInstance(props);
        return session;
    }

    @Override
    public boolean sendEmail(MimeMessage message) {
        Session session = getSession();
        //printRawMessage(message);
        try {
            sendEmailToAWS(session, message);
        }
        catch (MessagingException e) {
            LOGGER.info("Error sending email to AWS");
            LOGGER.error("An error occured:", e);
            ;
            return false;

        }

        return true;
    }

    @Override
    public boolean sendEmail(String from, Set<String> to, String subject, String body, List<Attachment> attachments) {

        MimeMessage message = prepareRawMessage(from, null, null, to, null, subject, body, attachments);

        return sendEmail(message);
    }

    private void sendEmailToAWS(Session session, MimeMessage message) throws MessagingException {

        Transport t = new AWSJavaMailTransport(session, null);
        t.connect();
        t.sendMessage(message, null);
        t.close();
    }

    private MimeBodyPart createAttachmentPart(Attachment attachment) throws MessagingException, IOException {
        MimeBodyPart attachmentPart = new MimeBodyPart();
        ByteArrayDataSource attachmentDataSource = new ByteArrayDataSource(attachment.getContent(), attachment
                .getMimeType().toString());
        attachmentPart.setDataHandler(new DataHandler(attachmentDataSource));
        attachmentPart.setDisposition(Part.ATTACHMENT);
        attachmentPart.setFileName(attachment.getFileName());
        return attachmentPart;
    }

    private void setRecipients(MimeMessage msg, Set<String> addresses, Message.RecipientType recipientType)
            throws MessagingException {
        InternetAddress[] inetAddresses;
        if (addresses != null) {
            List<String> addressList = new ArrayList<String>();
            addressList.addAll(addresses);
            inetAddresses = new InternetAddress[addressList.size()];
            for (int i = 0; i < addressList.size(); i++) {
                inetAddresses[i] = new InternetAddress(addressList.get(i));
            }
            msg.addRecipients(recipientType, inetAddresses);
        }
    }

    private void setReplyTo(MimeMessage msg, Set<String> replyToAddresses) throws MessagingException {
        InternetAddress[] inetAddresses;
        if (replyToAddresses != null) {
            List<String> addressList = new ArrayList<String>();
            addressList.addAll(replyToAddresses);
            inetAddresses = new InternetAddress[addressList.size()];
            for (int i = 0; i < addressList.size(); i++) {
                inetAddresses[i] = new InternetAddress(addressList.get(i));
            }
            msg.setReplyTo(inetAddresses);
        }
    }

    private void setCustomerServiceRecipients(MimeMessage message) {
    	Properties emailOverride = PropertiesLoader.getAsProperties("email.override");
    	Set<String> csrRecipients = new HashSet<>();
    	for (String address : emailOverride.getProperty("exceptionsAddress", "apdexceptions@americanproduct.com").split(",")) {
    		csrRecipients.add(address);
    	}
    	try {
			this.setRecipients(message, csrRecipients, Message.RecipientType.TO);
		} catch (MessagingException e) {
			LOGGER.error("Unable to set specified Customer Service email addresses", e);
		}
    }
}
