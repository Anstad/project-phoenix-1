package com.apd.phoenix.service.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class AbstractPropertyUtils {
	
private static List<List<String>> semanticGroups = new ArrayList<>();
	
	private static List<String> TRUE = Arrays.asList(new String[]{
			"TRUE", "true", "T", "t", "YES", "yes", "Y", "y"
	}); 
	
	private static List<String> FALSE = Arrays.asList(new String[]{
			"FALSE", "false", "F", "f", "NO", "no", "N", "n"
	}); 
	
	static {
		semanticGroups.add(TRUE);
		semanticGroups.add(FALSE);
	}
	
	public static boolean semanticEquals(String lhs, String rhs) {
		for (List<String> group : semanticGroups) {
			if (group.contains(lhs) && group.contains(rhs)) {
				return true;
			}
		}
		return false;
	}
	
	protected static String normalizeValue(Object value) {
		String normalizedValue = null;
		if (value != null) {
			if (value instanceof String) {
				// Locale used in capitalization to remove platform/locale dependence
				normalizedValue = StringUtils.equalsIgnoreCase(value.toString(), "NULL") ? null 
						: StringUtils.isBlank(value.toString()) ? null :value.toString().trim(); 
			} else {
				normalizedValue = value.toString();
			}
		}
		return normalizedValue;
	}
	
	public static boolean isTrue(String string){
		if(string != null){
			for(String trueString:TRUE){
				if(string.equals(trueString)){
					return true;
				}
			}
		}
		return false;
	}
	
}
