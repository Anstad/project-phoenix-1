package com.apd.phoenix.service.solr;

public class IndexingException extends RuntimeException {

    private static final long serialVersionUID = -4600182320876286932L;

    public IndexingException(String message, Throwable t) {
        super(message, t);
    }
}
