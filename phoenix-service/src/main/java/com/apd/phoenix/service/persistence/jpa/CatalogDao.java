package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.FavoritesList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Catalog DAO stub
 *
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogDao extends AbstractDao<Catalog> {

    private static final String USPS_CUSTOMER_NAME = "usps";

    @Override
    public Catalog update(Catalog catalog) {
        Catalog oldCatalog = findById(catalog.getId(), Catalog.class);
        catalog = super.update(catalog);
        if (catalog.getCustomer() != null) {
            if (USPS_CUSTOMER_NAME.equals(catalog.getCustomer().getApdAssignedAccountId())) {
                if (changedRelevantFields(catalog, oldCatalog)) {
                    catalog.setChangedItemRelevantFields(Boolean.TRUE);
                }
            }
        }
        return super.update(catalog);
    }

    public Catalog hydrateForSearchResults(Catalog toHydrate) {
        String hql = "SELECT catalog FROM Catalog AS catalog" + " LEFT JOIN FETCH catalog.customer"
                + " LEFT JOIN FETCH catalog.vendor " + " WHERE catalog.id = " + toHydrate.getId().toString();
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setItemsReturned(query, 0, 0);

        return getSingleResultOrNull(query);
    }

    public List<FavoritesList> getFavoritesLists(Catalog catalog) {
    	List<FavoritesList> toReturn = new ArrayList<>();
        String hql = "SELECT DISTINCT catalog FROM Catalog catalog"
                + " LEFT JOIN FETCH catalog.favorites LEFT JOIN FETCH catalog.parent "
                + " where catalog.id = :catalogId ";
        while (catalog != null) {
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalog.getId());
	        catalog = (Catalog) query.getSingleResult();
	        toReturn.addAll(catalog.getFavorites());
	        if (catalog.equals(catalog.getParent())) {
	        	break;
	        }
	        catalog = catalog.getParent();
        }
        Collections.sort(toReturn, new EntityComparator());
        return toReturn;
    }

    public long getCatalogSize(Catalog catalog) {
        String itemHql = "SELECT COUNT(DISTINCT item) FROM Item item WHERE item.vendorCatalog.id = :catalogId";
        //Creates the query
        Query itemQuery = entityManager.createQuery(itemHql);
        itemQuery.setParameter("catalogId", catalog.getId());
        long vendorQuantity = (long) itemQuery.getSingleResult();
        if (vendorQuantity != 0) {
            return vendorQuantity;
        }

        String customerHql = "SELECT COUNT(DISTINCT item) FROM CatalogXItem item WHERE item.catalog.id = :catalogId";
        //Creates the query
        Query customerQuery = entityManager.createQuery(customerHql);
        customerQuery.setParameter("catalogId", catalog.getId());
        long customerQuantity = (long) customerQuery.getSingleResult();
        return customerQuantity;
    }

    public Long getParentCatalogId(long catalogID) {
        String hql = "SELECT parent.id FROM Catalog cat JOIN cat.parent AS parent " + "WHERE cat.id = :catalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalogID);

        Long parentId = getSingleResultOrNull(query);
        if (parentId != null) {
            return parentId;
        }
        return null;
    }

    public Catalog hydrateCustomerName(Catalog catalog) {
        String hql = "SELECT cat FROM Catalog cat LEFT JOIN cat.customer " + "WHERE cat.id = :catalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogId", catalog.getId());

        return getSingleResultOrNull(query);
    }

    private boolean changedRelevantFields(Catalog catalog, Catalog oldCatalog) {
        return (oldCatalog == null || !datesMatch(catalog.getExpirationDate(), oldCatalog.getExpirationDate()) || !datesMatch(
                oldCatalog.getStartDate(), catalog.getStartDate()));

    }

    private boolean datesMatch(Date date1, Date date2) {
        return (date1 == null && date2 == null) || (date1.equals(date2));
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> searchCustomerCatalogByName(String name) {
        String hql = "SELECT cat FROM Catalog cat JOIN cat.customer " + "WHERE LOWER(cat.name) LIKE :catalogName";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catalogName", "%" + name.toLowerCase() + "%");

        return (List<Catalog>) query.getResultList();
    }

    public String vendorNameOfCatalog(Catalog vendorCatalog) {
        String hql = "SELECT cat.vendor.name FROM Catalog cat WHERE cat.id = :vendorCatalogId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorCatalogId", vendorCatalog.getId());

        return getSingleResultOrNull(query);
    }

    public List<Long> getParentCatalogIdList(long catalogID) {
        List<Long> parentCatalogIDs = new ArrayList<>();
        while (this.getParentCatalogId(catalogID) != null) {
            catalogID = this.getParentCatalogId(catalogID);
            parentCatalogIDs.add(catalogID);
        }        
        return parentCatalogIDs;
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getCatalogsToReindex() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.reindexNightly = true OR cat.reindexDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getCatalogsToRegenerateCsv() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.regenerateCsvNightly = true OR cat.regenerateCsvDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Catalog> getSmartOciToRegenerate() {
        String hql = "SELECT cat FROM Catalog cat WHERE cat.regenerateSmartOciDate = :today";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("today", new Date(), TemporalType.DATE);

        return (List<Catalog>) query.getResultList();
    }
}
