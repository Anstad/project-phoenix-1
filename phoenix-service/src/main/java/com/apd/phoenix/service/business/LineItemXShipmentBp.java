package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.TaxRateAtPurchase;
import com.apd.phoenix.service.model.TaxType.TaxTypeEnum;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.LineItemXShipmentDao;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class LineItemXShipmentBp extends AbstractBp<LineItemXShipment> {

    private static final Logger logger = LoggerFactory.getLogger(LineItemXShipmentBp.class);

    private static final String EXCHANGED = "EXCHANGED";

    @Inject
    private CustomerOrderDao customerOrderDao;

    @Inject
    private LineItemBp lineItemBp;

    @Inject
    private TaxRateBp taxRateBp;

    @Inject
    private TaxTypeBp taxTypeBp;

    @Inject
    private LineItemXShipmentXTaxTypeBp lineItemXShipmentXTaxTypeBp;

    @Inject
    private TaxRateAtPurchaseBp taxRateAtPurchaseBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */

    @Inject
    public void initDao(LineItemXShipmentDao dao) {
        this.dao = dao;
    }

    /**
     * This method takes a LineItemXShipmentDto and a shipment, and adds the LineItemXShipment to the shipment.
     * 
     * @param lineItemXShipmentDto
     * @param shipment
     * @return
     */
    public LineItemXShipment persistLineItemXShipment(LineItemXShipmentDto lineItemXShipmentDto, Shipment shipment) {
        LineItemXShipment lineItemXShipment = new LineItemXShipment();

        CustomerOrder customerOrder= customerOrderDao.findById(shipment.getOrder().getId(), CustomerOrder.class);
        LineItem lineItem = lineItemBp.retrieveLineItem(lineItemXShipmentDto.getLineItemDto(), customerOrder);
        
        if (lineItem == null) {
        	logger.error("LineItem for the shipment could not be found.");
        }

        lineItemXShipment.setLineItem(lineItem);
        if (lineItem.getStatus() != null && lineItem.getStatus().getValue().equals(EXCHANGED)) {
            lineItemXShipment.setPaid(true);
        }
        else {
            lineItemXShipment.setPaid(false);
        }
        lineItemXShipment.setQuantity(lineItemXShipmentDto.getQuantity());
        lineItemXShipment.setShipping(lineItemXShipmentDto.getShipping());
        lineItemXShipment.setShipments(shipment);
        lineItemXShipment = this.create(lineItemXShipment);

        if (lineItem.getTaxable() != null && lineItem.getTaxable()) {
        	//within this if clause, sales tax calculation takes place.
        	//An overview: sales tax is set on the order and the individual line items when the order is placed. 
        	//
        	//The rate is stored on order.taxRatesAtPurchase, and the tax amounts are stored on item.maximumTaxToCharge and 
        	//order.maximumTaxToCharge
        	//
        	//When the shipment is created, tax is calculated for that lineitem, on that shipment. However, the sum of all 
        	//taxes for a lineitem must add up to item.maximumTaxToCharge. So, the tax is calculated for the item, and if the 
        	//amount is greater than maximumTaxToCharge (or it's less than maximumTaxToCharge and this is the last shipment of 
        	//the item), the tax amount is set so that the sum of all taxes for that item will be maximumTaxToCharge.
        	//
        	//Then, once the tax for an item is obtained, the tax is assigned to each jurisdiction. The amount is assigned to 
        	//each jurisdiciton by their tax rate, rounded up, until the tax amount for the item runs out (in which case the 
        	//last jurisdiction(s) will not get the full amount) or there are no more jurisdictions left (in which case the 
        	//last jurisdiction will get any remaining amount).
        	
        	//tax rates at purchase are obtained
            List<TaxRateAtPurchase> jurisdictionTaxRates = new ArrayList<TaxRateAtPurchase>(this.taxRates(shipment.getOrder().getId()));
            
            //total tax percentage is the sum of each jurisdiction's tax rate. It will be something like 0.07.
            BigDecimal totalTaxPercentage = BigDecimal.ZERO;
            
            //adds the tax percentages together to get the total tax amount, and selects the State tax, which will be 
            //moved to the end of the list.
            TaxRateAtPurchase toSetLast = null;
            List<TaxRateAtPurchase> toRemove = new ArrayList<>();
            for (TaxRateAtPurchase jurisdictionTaxRate : jurisdictionTaxRates) {
            	//removes any tax rates with zero or null values
            	if (jurisdictionTaxRate.getValue() != null && jurisdictionTaxRate.getValue().compareTo(BigDecimal.ZERO) != 0) {
                	totalTaxPercentage = totalTaxPercentage.add(jurisdictionTaxRate.getValue());
            	}
            	else {
            		toRemove.add(jurisdictionTaxRate);
            	}
            	//if this tax rate is the state tax, moves it to the end of the list
            	if (jurisdictionTaxRate.getType() != null && jurisdictionTaxRate.getType().getEnumValue().equals(TaxTypeEnum.STATE)) {
            		toSetLast = jurisdictionTaxRate;
            	}
            }
            
            if (toSetLast != null) {
	            //moves state tax to the end of the list; this tax is the last that should be calculated
            	jurisdictionTaxRates.remove(toSetLast);
            	jurisdictionTaxRates.add(toSetLast);
            }
            
            jurisdictionTaxRates.removeAll(toRemove);
            
            //lixs tax amount is the dollar amount of taxes that will be paid for this item, on this shipment.
            //it is roughly equal to the unit price of the item, times the quantity on this shipment, plus the shipping 
            //charged for the item, all times the value of totalTaxPercentage.
            //(however it could be greater or less, depending on the value of maximumTaxToCharge)
        	BigDecimal lixsTaxAmount = this.getTaxOnLixs(lineItemXShipment, lineItem, totalTaxPercentage);
            
        	//added tax keeps track of how much of lixsTaxAmount has been added to the LineItemXShipment as a 
        	//LineItemXShipmentXTaxType
            BigDecimal addedTax = BigDecimal.ZERO;
            //using an explicit for loop instead of extended, in order to determine of element is the last in the list
            for (int i = 0; i < jurisdictionTaxRates.size(); i++) {
            	//gets the current tax rate
            	TaxRateAtPurchase jurisdictionTaxRate = jurisdictionTaxRates.get(i);
            	//check that the total tax percentage is nonzero, so we don't divide by zero
            	if (totalTaxPercentage.compareTo(BigDecimal.ZERO) != 0) {
            		//remaining tax is the amount of tax on this lineitemxshipment that hasn't beed added as a 
            		//LineItemXShipmentXTaxType. It is recalculated each time in the loop.
            		BigDecimal remainingTax = lixsTaxAmount.subtract(addedTax);
            		//boolean indicating if this is the last tax type (and therefore if it gets the remaining tax for the 
            		//item, rather than the calculated tax)
            		boolean lastTaxType = (i + 1 == jurisdictionTaxRates.size());
            		//the calculated tax for this jurisdiction. Calculated using the following algorithm:
            		//	lixs tax amount * jurisdiction tax rate / total tax rate
            		//rounds up
            		BigDecimal jurisdictionTax = lixsTaxAmount.multiply(jurisdictionTaxRate.getValue()).divide(totalTaxPercentage, 2, RoundingMode.UP);
            		//if the jurisdiction tax is more than the remaining tax, or this is the last jurisdiction, sets the 
            		//value of the jurisdiction's tax to the remaining tax
            		if (jurisdictionTax.compareTo(remainingTax) > 0 || lastTaxType) {
            			jurisdictionTax = remainingTax;
            		}
            		//adds this jurisdiction's tax to the lineitemxshipment as a lineitemxshipmentxtaxtype
            		addedTax = addedTax.add(jurisdictionTax);
            		lineItemXShipment.getTaxes().add(this.getTax(lineItemXShipment, jurisdictionTax, jurisdictionTaxRate.getType().getEnumValue()));
            	}
            }
        }

        lineItem.getItemXShipments().add(lineItemXShipment);
        lineItemBp.update(lineItem);
        update(lineItemXShipment);
        return lineItemXShipment;
    }

    /**
     * Given an order ID, gets the tax rates for that order. This method will attempt to get the tax rates at the time the 
     * order was placed, but if those rates aren't set, uses the current tax rates for the order's ship destination
     * 
     * @param orderId
     * @return
     */
    public Set<TaxRateAtPurchase> taxRates(Long orderId) {
        CustomerOrder order = this.customerOrderDao.findById(orderId, CustomerOrder.class);
        if (order.getTaxRatesAtPurchase() != null && !order.getTaxRatesAtPurchase().isEmpty()) {
            return order.getTaxRatesAtPurchase();
        }

        LOG
                .debug("Order with no tax rates at purchase, probably placed before tax rate changes. Using tax rate at ship time.");

        Address shipAddress = order.getAddress();
        TaxRate rate = taxRateBp.getTaxRate(shipAddress.getZip(), shipAddress.getCounty(), shipAddress.getCity(),
                shipAddress.getState(), shipAddress.getGeoCode());

        if (rate.getId() == null) {
            LOG.warn("Order's zip code " + shipAddress.getZip() + " didn't have a tax rate, cannot charge sales tax.");
        }

        return taxRateAtPurchaseBp.getRateSet(rate, order);
    }

    /**
     * Takes a LineItemXShipment, a LineItem, and the total tax percentage of an order, and returns the amount of tax that 
     * should be charged for the given LineItem, for the given LineItemXShipment.
     * 
     * @param lineItemXShipment
     * @param lineItem
     * @param totalTaxPercentage
     * @return
     */
    private BigDecimal getTaxOnLixs(LineItemXShipment lineItemXShipment, LineItem lineItem,
            BigDecimal totalTaxPercentage) {
        //first, calculates the remaining tax
        BigDecimal remainingTax = lineItem.getMaximumTaxToCharge().subtract(lineItem.getPaidTax());
        //then determines if this is the last shipment of this item, by checking if the quantity is equal 
        //to the quantity ordered minus the quantity shipped so far.
        boolean lastShipment = lineItemXShipment.getQuantity().compareTo(
                new BigInteger("" + (lineItem.getQuantity() - lineItem.getQuantityShipped()))) == 0;
        //then, calculates via arithmetic the tax amount, multiplying the quantity by the unit price, adding shipping, 
        //and multiplying by tax rate.
        BigDecimal lixsTaxAmount = lineItem.getUnitPrice().multiply(
                new BigDecimal(lineItemXShipment.getQuantity().toString())).add(lineItemXShipment.getShipping())
                .multiply(totalTaxPercentage).setScale(2, RoundingMode.UP);

        //finally, checks if the calculated tax is greater than the remaining tax or this is the last shipment of this 
        //item. If so, the remaining tax is used, rather than the calculated tax.
        if (lixsTaxAmount.compareTo(remainingTax) > 0 || lastShipment) {
            lixsTaxAmount = remainingTax;
        }

        return lixsTaxAmount;
    }

    private LineItemXShipmentXTaxType getTax(LineItemXShipment lineItemXShipment, BigDecimal value, TaxTypeEnum taxType) {
        LineItemXShipmentXTaxType toReturn = new LineItemXShipmentXTaxType();
        toReturn.setLineItemXShipment(lineItemXShipment);
        toReturn.setValue(value);
        toReturn.setType(taxTypeBp.getByName(taxType.getDbValue()));
        return lineItemXShipmentXTaxTypeBp.create(toReturn);
    }

    //Not used
    @Deprecated
    public LineItemXShipment registerPayment(LineItemXShipment lixs) {
        lixs.setPaid(true);
        return this.update(lixs);
    }

    public LineItemXShipment createFee(LineItem lineItem, Shipment shipment) {
        LineItemXShipment lineItemXShipment = new LineItemXShipment();

        lineItemXShipment.setLineItem(lineItem);
        lineItemXShipment.setPaid(false);
        lineItemXShipment.setShipments(shipment);
        lineItemXShipment.setQuantity(BigInteger.valueOf(lineItem.getQuantity()));
        lineItemXShipment.setShipping(BigDecimal.ZERO);

        return this.create(lineItemXShipment);
    }

    public void refresh(LineItemXShipment entity) {
        ((LineItemXShipmentDao) this.dao).refresh(entity);
    }

}
