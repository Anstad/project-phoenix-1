package com.apd.phoenix.service.business;

import java.util.HashSet;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.persistence.jpa.CashoutPageDao;

@Stateless
@LocalBean
public class CashoutPageBp extends AbstractBp<CashoutPage> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CashoutPageDao dao) {
        this.dao = dao;
    }

    @Override
    public CashoutPage cloneEntity(CashoutPage toClone) {
        CashoutPage toReturn = new CashoutPage();
        toClone = this.eagerLoad(toClone);
        toReturn.setCancelDate(toClone.getCancelDate());
        toReturn.setDeliveryDate(toClone.getDeliveryDate());
        toReturn.setExpectedDate(toClone.getExpectedDate());
        toReturn.setHideCCNumber(toClone.getHideCCNumber());
        toReturn.setHigherPriceNotice(toClone.getHigherPriceNotice());
        toReturn.setMultipleShipTo(toClone.getMultipleShipTo());
        toReturn.setName(toClone.getName());
        toReturn.setServiceDate(toClone.getServiceDate());
        toReturn.setUpdateUserInformation(toClone.getUpdateUserInformation());
        toReturn.setPageXFields(new HashSet<CashoutPageXField>());
        for (CashoutPageXField field : toClone.getPageXFields()) {
            CashoutPageXField newField = new CashoutPageXField();
            newField.setDefaultValue(field.getDefaultValue());
            newField.setField(field.getField());
            newField.setLabel(field.getLabel());
            newField.setRequired(field.getRequired());
            toReturn.getPageXFields().add(newField);
        }
        return toReturn;
    }

    public CashoutPage getFromCredentialForCashout(Credential cred) {
        return ((CashoutPageDao) this.dao).getFromCredentialForCashout(cred);
    }

    @Override
    public CashoutPage eagerLoad(CashoutPage page) {
        return ((CashoutPageDao) this.dao).eagerLoad(page);
    }
}
