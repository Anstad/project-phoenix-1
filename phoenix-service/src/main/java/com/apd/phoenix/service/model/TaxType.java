package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 * This entity is used to store the type of Tax for a LineItemXShipment. It has a one-to-many 
 * relationship with LineItemXShipment_TaxType.
 * 
 * @author RHC
 *
 */
@Entity
@Cacheable
public class TaxType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -5284251008710613106L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String name;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public TaxTypeEnum getEnumValue() {
        for (TaxTypeEnum enumValue : TaxTypeEnum.values()) {
            if (enumValue.getDbValue().equals(this.getName())) {
                return enumValue;
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((TaxType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (name != null && !name.trim().isEmpty())
            result += "name: " + name;
        return result;
    }

    public enum TaxTypeEnum {

        STATE("State Tax"), COUNTY("County Tax"), CITY("City Tax"), COUNTY_LOCAL("County Local Tax"), CITY_LOCAL(
                "City Local Tax");

        private String dbValue;

        private TaxTypeEnum(String dbValue) {
            this.dbValue = dbValue;
        }

        public String getDbValue() {
            return this.dbValue;
        }
    }
}
