package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * 
 * @author RHC
 *
 */
@Entity
public class TaxRateLog implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -4602503734145720219L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Enumerated(EnumType.STRING)
    private TaxRateLogType type;

    @Enumerated(EnumType.STRING)
    private TaxRateLogAction action;

    @Enumerated(EnumType.STRING)
    private TaxRateLogStatus status;

    @Column
    private String zip;

    @Column
    private String identifier;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "TAXLOG_TIME_IDX")
    private Date timestamp = new Date();

    public TaxRateLog() {
        //default constructor
    }

    public TaxRateLog(TaxRateLogType type, TaxRateLogAction action, TaxRateLogStatus status, String zip,
            String identifier) {
        this.type = type;
        this.action = action;
        this.status = status;
        this.zip = zip;
        this.identifier = identifier;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public TaxRateLogType getType() {
        return type;
    }

    public void setType(TaxRateLogType type) {
        this.type = type;
    }

    public TaxRateLogAction getAction() {
        return action;
    }

    public void setAction(TaxRateLogAction action) {
        this.action = action;
    }

    public TaxRateLogStatus getStatus() {
        return status;
    }

    public void setStatus(TaxRateLogStatus status) {
        this.status = status;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((TaxRateLog) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public static enum TaxRateLogType {
        TAX_RATE, ZIP_PLUS_FOUR, MILITARY_ZIP;
    }

    public static enum TaxRateLogAction {
        INSERT, DELETE;
    }

    public static enum TaxRateLogStatus {
        SUCCESS, FAILURE;
    }

    public static class TaxRateLogComparator implements Comparator<TaxRateLog> {

        @Override
        public int compare(TaxRateLog log0, TaxRateLog log1) {

            Date date0 = log0.getTimestamp();
            Date date1 = log1.getTimestamp();
            if (date0 != null && date1 != null) {
                return date0.compareTo(date1);
            }
            if (date0 != null) {
                return -1;
            }
            if (date1 != null) {
                return 1;
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}