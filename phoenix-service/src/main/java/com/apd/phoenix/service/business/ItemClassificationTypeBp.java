package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.persistence.jpa.ItemClassificationTypeDao;

@Stateless
@LocalBean
public class ItemClassificationTypeBp extends AbstractBp<ItemClassificationType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemClassificationTypeDao dao) {
        this.dao = dao;
    }

}
