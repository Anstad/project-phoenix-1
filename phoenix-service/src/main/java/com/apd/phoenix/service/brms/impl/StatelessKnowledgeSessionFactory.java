package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.drools.KnowledgeBase;
import org.drools.runtime.StatelessKnowledgeSession;
import com.apd.phoenix.service.brms.api.KnowledgeBaseFactory;

public class StatelessKnowledgeSessionFactory {

    @Inject
    KnowledgeBaseFactory knowledgeBaseFactory;

    public StatelessKnowledgeSession getStatelessKnowledgeSession() {
        KnowledgeBase kbase = knowledgeBaseFactory.getKnowledgeBase();
        return kbase.newStatelessKnowledgeSession();
    }
}
