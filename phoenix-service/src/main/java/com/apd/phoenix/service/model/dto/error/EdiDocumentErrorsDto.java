package com.apd.phoenix.service.model.dto.error;

import java.util.List;
import java.util.Date;

public class EdiDocumentErrorsDto {

    private Date timestamp;

    private List<RelevantNumberDto> relevantNumberDtos;
    private List<EdiErrorDto> ediErrorDtos;
    private String rawEdi;

    public List<EdiErrorDto> getEdiErrorDtos() {
        return ediErrorDtos;
    }

    public void setEdiErrorDtos(List<EdiErrorDto> ediErrorDtos) {
        this.ediErrorDtos = ediErrorDtos;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the relevantNumberDtos
     */
    public List<RelevantNumberDto> getRelevantNumberDto() {
        return relevantNumberDtos;
    }

    /**
     * @param relevantNumberDtos the relevantNumberDtos to set
     */
    public void setRelevantNumberDto(List<RelevantNumberDto> referenceDto) {
        this.relevantNumberDtos = referenceDto;
    }

    /**
     * @return the rawEdi
     */
    public String getRawEdi() {
        return rawEdi;
    }

    /**
     * @param rawEdi the rawEdi to set
     */
    public void setRawEdi(String rawEdi) {
        this.rawEdi = rawEdi;
    }

}