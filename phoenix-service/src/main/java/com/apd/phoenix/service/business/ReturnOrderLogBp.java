package com.apd.phoenix.service.business;

import javax.inject.Inject;
import com.apd.phoenix.service.model.ReturnOrderLog;
import com.apd.phoenix.service.persistence.jpa.ReturnOrderLogDao;

/**
 *
 * @author RHC
 */
public class ReturnOrderLogBp extends AbstractBp<ReturnOrderLog> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ReturnOrderLogDao dao) {
        this.dao = dao;
    }
}
