/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.PaymentType;

/**
 *
 * @author nreidelb
 */
public class CustomerOrderSearchCriteria {

    private OrderStatus changeStatus;
    private Date selectedStartDate;
    private Date selectedEndDate;
    //exact date shouldn't be used in UI searching, since it is non-inclusive.
    private Date exactEndDate;
    private OrderStatus selectedStatus;
    private String selectedPO;
    private Account account;
    private String userId;
    private String email;
    private List<String> skuList;
    private BigDecimal matchPrice;
    private Integer start;
    private Integer pageSize;
    private LineItemStatus lineItemStatus;
    private PaymentType paymentType;
    private String salesPerson;
    private Credential credential;
    private List<Long> accountWhiteList;
    private String apdPo;
    private String poType;
    private String partnerId;

    public CustomerOrderSearchCriteria(Date inSelectedStartDate, Date inSelectedEndDate, OrderStatus inSelectedStatus,
            String inSelectedPO, Account inAccount, String inUserId, String inEmail, List<String> inSkuList,
            BigDecimal inMatchPrice, Integer inStart, Integer inPageSize, List<Long> accountWhiteList) {
        this.selectedStartDate = inSelectedStartDate;
        this.selectedEndDate = inSelectedEndDate;
        this.selectedStatus = inSelectedStatus;
        this.selectedPO = inSelectedPO;
        this.account = inAccount;
        this.userId = inUserId;
        this.email = inEmail;
        this.skuList = inSkuList;
        this.matchPrice = inMatchPrice;
        this.start = inStart;
        this.pageSize = inPageSize;
        this.accountWhiteList = accountWhiteList;
    }

    public CustomerOrderSearchCriteria(Date inSelectedStartDate, Date inSelectedEndDate, OrderStatus inSelectedStatus,
            String inSelectedPO, Account inAccount, Credential credential, String inUserId, String inEmail,
            List<String> inSkuList, BigDecimal inMatchPrice, Integer inStart, Integer inPageSize,
            List<Long> accountWhiteList) {
        this.selectedStartDate = inSelectedStartDate;
        this.selectedEndDate = inSelectedEndDate;
        this.selectedStatus = inSelectedStatus;
        this.selectedPO = inSelectedPO;
        this.account = inAccount;
        this.credential = credential;
        this.userId = inUserId;
        this.email = inEmail;
        this.skuList = inSkuList;
        this.matchPrice = inMatchPrice;
        this.start = inStart;
        this.pageSize = inPageSize;
        this.accountWhiteList = accountWhiteList;
    }

    public CustomerOrderSearchCriteria(OrderStatus inChangeStatus, Date inSelectedStartDate, Date inSelectedEndDate,
            OrderStatus inSelectedStatus, String inSelectedPO, Account inAccount, String inUserId, String inEmail,
            List<String> inSkuList, BigDecimal inMatchPrice, LineItemStatus lineItemStatus, PaymentType paymentType,
            String salesPerson, List<Long> accountWhiteList) {
        this.changeStatus = inChangeStatus;
        this.selectedStartDate = inSelectedStartDate;
        this.selectedEndDate = inSelectedEndDate;
        this.selectedStatus = inSelectedStatus;
        this.selectedPO = inSelectedPO;
        this.account = inAccount;
        this.userId = inUserId;
        this.email = inEmail;
        this.skuList = inSkuList;
        this.matchPrice = inMatchPrice;
        this.start = 0;
        this.pageSize = 0;
        this.setLineItemStatus(lineItemStatus);
        this.setPaymentType(paymentType);
        this.salesPerson = salesPerson;
        this.accountWhiteList = accountWhiteList;
    }

    public CustomerOrderSearchCriteria(OrderStatus inChangeStatus, Date inSelectedStartDate, Date inSelectedEndDate,
            Date inExactEndDate, OrderStatus inSelectedStatus, String inSelectedPO, Account inAccount, String inUserId,
            String inEmail, List<String> inSkuList, BigDecimal inMatchPrice, LineItemStatus lineItemStatus,
            PaymentType paymentType, String salesPerson, List<Long> accountWhiteList) {
        this(inChangeStatus, inSelectedStartDate, inSelectedEndDate, inSelectedStatus, inSelectedPO, inAccount,
                inUserId, inEmail, inSkuList, inMatchPrice, lineItemStatus, paymentType, salesPerson, accountWhiteList);
        this.setExactEndDate(inExactEndDate);

    }

    public CustomerOrderSearchCriteria(String apdPo) {
        this.apdPo = apdPo;
        this.start = 0;
        this.pageSize = 0;
    }

    public CustomerOrderSearchCriteria(String po, String poType, String partnerId) {
        this.poType = poType;
        this.selectedPO = po;
        this.setPartnerId(partnerId);
    }

    /**
     * @return the selectedStartDate
     */
    public Date getSelectedStartDate() {
        return selectedStartDate;
    }

    /**
     * @param selectedStartDate the selectedStartDate to set
     */
    public void setSelectedStartDate(Date selectedStartDate) {
        this.selectedStartDate = selectedStartDate;
    }

    /**
     * @return the selectedEndDate
     */
    public Date getSelectedEndDate() {
        return selectedEndDate;
    }

    /**
     * @param selectedEndDate the selectedEndDate to set
     */
    public void setSelectedEndDate(Date selectedEndDate) {
        this.selectedEndDate = selectedEndDate;
    }

    public Date getExactEndDate() {
        return exactEndDate;
    }

    public void setExactEndDate(Date exactEndDate) {
        this.exactEndDate = exactEndDate;
    }

    /**
     * @return the selectedStatus
     */
    public OrderStatus getSelectedStatus() {
        return selectedStatus;
    }

    /**
     * @param selectedStatus the selectedStatus to set
     */
    public void setSelectedStatus(OrderStatus selectedStatus) {
        this.selectedStatus = selectedStatus;
    }

    /**
     * @return the selectedPO
     */
    public String getSelectedPO() {
        return selectedPO;
    }

    /**
     * @param selectedPO the selectedPO to set
     */
    public void setSelectedPO(String selectedPO) {
        this.selectedPO = selectedPO;
    }

    /**
     * @return the account
     */
    public Account getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the skuList
     */
    public List<String> getSkuList() {
        return skuList;
    }

    /**
     * @param skuList the skuList to set
     */
    public void setSkuList(List<String> skuList) {
        this.skuList = skuList;
    }

    /**
     * @return the matchPrice
     */
    public BigDecimal getMatchPrice() {
        return matchPrice;
    }

    /**
     * @param matchPrice the matchPrice to set
     */
    public void setMatchPrice(BigDecimal matchPrice) {
        this.matchPrice = matchPrice;
    }

    /**
     * @return the start
     */
    public Integer getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Integer start) {
        this.start = start;
    }

    /**
     * @return the pageSize
     */
    public Integer getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public OrderStatus getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(OrderStatus currentStatus) {
        this.changeStatus = currentStatus;
    }

    public LineItemStatus getLineItemStatus() {
        return lineItemStatus;
    }

    public void setLineItemStatus(LineItemStatus lineItemStatus) {
        this.lineItemStatus = lineItemStatus;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public List<Long> getAccountWhiteList() {
        return accountWhiteList;
    }

    public void setAccountWhiteList(List<Long> accountWhiteList) {
        this.accountWhiteList = accountWhiteList;
    }

    public String getApdPo() {
        return this.apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public String getPoType() {
        return poType;
    }

    public void setPoType(String poType) {
        this.poType = poType;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
