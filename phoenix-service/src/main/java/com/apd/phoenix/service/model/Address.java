package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

/**
 * This class is used to store an address. It has a one-to-many relationship
 * with AddressField, which are used to store the values in the address (such as
 * city, state, and so forth).
 * 
 * @author RHC
 * 
 */
@Entity
@Audited
public class Address implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 4010126251619639131L;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private AddressType type;

    @ManyToOne
    private Vendor vendor;

    @ManyToOne
    private Person person;

    @ManyToOne
    private Account account;

    @Column
    private String name;

    @Column
    private String company;

    @Column
    private String line1;

    @Column
    private String line2;

    @Column
    private String city;

    @Column
    private String state;

    @Column
    private String zip;

    @Column
    private String shipToId;

    @Column
    private Boolean pendingShipToAuthorization = Boolean.FALSE;

    @Column
    private String country;

    //Geocode added by CCH tax files from Wolters Kluers 
    @Column
    private String geoCode;

    //County added by CCH tax files from Wolters Kluers
    @Column
    private String county;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private boolean primary = false;

    @OneToMany(mappedBy = "address", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private Set<AddressXAddressPropertyType> fields = new HashSet<AddressXAddressPropertyType>();

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @NotAudited
    private MiscShipTo miscShipTo;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Address) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Set<AddressXAddressPropertyType> getFields() {
        return this.fields;
    }

    public void setFields(final Set<AddressXAddressPropertyType> fields) {
        this.fields = fields;
    }

    public AddressType getType() {
        return this.type;
    }

    public void setType(final AddressType type) {
        this.type = type;
    }

    public Vendor getVendor() {
        return this.vendor;
    }

    public void setVendor(final Vendor vendor) {
        this.vendor = vendor;
    }

    public Person getPerson() {
        return this.person;
    }

    public void setPerson(final Person person) {
        this.person = person;
    }

    public Account getAccount() {
        return this.account;
    }

    public void setAccount(final Account account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getShipToId() {
        return shipToId;
    }

    public void setShipToId(String shipToId) {
        this.shipToId = shipToId;
    }

    public Boolean getPendingShipToAuthorization() {
        return pendingShipToAuthorization;
    }

    public void setPendingShipToAuthorization(Boolean pendingShipToAuthorization) {
        this.pendingShipToAuthorization = pendingShipToAuthorization;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (line1 != null && !line1.trim().isEmpty())
            result += ", line1: " + line1;
        if (line2 != null && !line2.trim().isEmpty())
            result += ", line2: " + line2;
        if (city != null && !city.trim().isEmpty())
            result += ", city: " + city;
        if (state != null && !state.trim().isEmpty())
            result += ", state: " + state;
        if (zip != null && !zip.trim().isEmpty())
            result += ", zip: " + zip;
        if (geoCode != null && !geoCode.trim().isEmpty())
            result += ", geoCode: " + geoCode;
        if (county != null && !county.trim().isEmpty())
            result += ", county: " + county;
        result += ", primary: " + getPrimary();
        return result;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public MiscShipTo getMiscShipTo() {
        return miscShipTo;
    }

    public void setMiscShipTo(MiscShipTo miscShipTo) {
        this.miscShipTo = miscShipTo;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    //duplicated method for backwards compatibility with serialized objects in workflow
    public boolean isPrimary() {
        return primary;
    }

    //duplicated method for backwards compatibility with serialized objects in workflow
    public void setPrimary(boolean primary) {
        this.primary = primary;
    }

    //FIXME: received an IllegalArgumentException when comparing addresses on batch node 69 at 10:17:01,190 on August 13th. 
    //"Comparison method violates its general contract!"
    //Until this is resolved, comparison of addresses should be wrapped in a try-catch
    public static class AddressComparator implements Comparator<Address> {

        @Override
        public int compare(Address item0, Address item1) {

            String shipToId0 = item0.getShipToId();
            if (StringUtils.isBlank(shipToId0) && item0.getMiscShipTo() != null) {
                shipToId0 = item0.getMiscShipTo().getAddressId();
            }

            String shipToId1 = item1.getShipToId();
            if (StringUtils.isBlank(shipToId1) && item1.getMiscShipTo() != null) {
                shipToId1 = item1.getMiscShipTo().getAddressId();
            }

            if (!StringUtils.isBlank(shipToId0) && !StringUtils.isBlank(shipToId1)) {
                return shipToId0.compareTo(shipToId1);
            }
            else if (StringUtils.isBlank(shipToId0) && !StringUtils.isBlank(shipToId1)) {
                return 1;
            }
            else if (!StringUtils.isBlank(shipToId0) && StringUtils.isBlank(shipToId1)) {
                return -1;
            }
            else if (!StringUtils.isBlank(item0.getZip()) && !StringUtils.isBlank(item1.getZip())) {
                return item0.getZip().compareTo(item1.getZip());
            }
            else if (!StringUtils.isBlank(item0.getZip()) && StringUtils.isBlank(item1.getZip())) {
                return 1;
            }
            else if (StringUtils.isBlank(item0.getZip()) && !StringUtils.isBlank(item1.getZip())) {
                return -1;
            }
            return (new GenericComparator()).compare(item0, item1);
        }
    }

    public static enum AddressFormatField {
        NAME("getName"), COMPANY("getCompany"), LINE1("getLine1"), LINE2("getLine2"), CITY("getCity"), STATE("getState"), ZIP(
                "getZip"), COUNTRY("getCountry"), COUNTY("getCounty"), GEOCODE("getGeoCode");

        private Method getterMethod;

        private AddressFormatField(String getter) {
            try {
                getterMethod = Address.class.getMethod(getter, (Class<?>[]) null);
            }
            catch (Exception e) {
                //method wasn't found
            }
        }

        public String getLabelInFormat() {
            return "#" + this.toString();
        }

        public String getValue(Address address) {
            String toReturn = "";
            if (getterMethod != null) {
                try {
                    toReturn = StringUtils.defaultIfBlank((String) getterMethod.invoke(address, (Object[]) null), "");
                }
                catch (Exception e) {
                    //could not find value
                }
            }
            return toReturn;
        }
    }

}
