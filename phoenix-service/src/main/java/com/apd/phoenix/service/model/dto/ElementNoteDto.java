package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ElementNoteDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private BigInteger position;
    private BigInteger referenceNumber;
    private BigInteger syntaxErrorCode;
    private String syntaxErrorDescription;
    private String copyOfBadDataElement;

    public BigInteger getPosition() {
        return position;
    }

    public void setPosition(BigInteger position) {
        this.position = position;
    }

    public BigInteger getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(BigInteger referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public BigInteger getSyntaxErrorCode() {
        return syntaxErrorCode;
    }

    public void setSyntaxErrorCode(BigInteger syntaxErrorCode) {
        this.syntaxErrorCode = syntaxErrorCode;
    }

    public String getSyntaxErrorDescription() {
        return syntaxErrorDescription;
    }

    public void setSyntaxErrorDescription(String syntaxErrorDescription) {
        this.syntaxErrorDescription = syntaxErrorDescription;
    }

    public String getCopyOfBadDataElement() {
        return copyOfBadDataElement;
    }

    public void setCopyOfBadDataElement(String copyOfBadDataElement) {
        this.copyOfBadDataElement = copyOfBadDataElement;
    }

}
