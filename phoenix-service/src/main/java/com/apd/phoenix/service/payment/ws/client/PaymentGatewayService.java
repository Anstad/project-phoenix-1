package com.apd.phoenix.service.payment.ws.client;

import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createAuthorizeAndCaptureParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createAuthorizeParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createCaptureParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createClientCredentials;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createCreditParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createTerminalIdentifier;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createVerificationParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayObjectFactory.createXMLGregorianCalendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CreditCardTransactionLogBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemXReturnBp;
import com.apd.phoenix.service.business.LineItemXShipmentBp;
import com.apd.phoenix.service.business.PersonBp;
import com.apd.phoenix.service.card.management.AddStoredCardParams;
import com.apd.phoenix.service.card.management.CreditCardManagementFailureReason;
import com.apd.phoenix.service.card.management.CreditCardManagementResult;
import com.apd.phoenix.service.card.management.CreditCardManagementService;
import com.apd.phoenix.service.card.management.CustomerIdentifier;
import com.apd.phoenix.service.card.management.GetStoredCreditCardParams;
import com.apd.phoenix.service.card.management.GetTokenForCardNumberParams;
import com.apd.phoenix.service.card.management.ICreditCardManagement;
import com.apd.phoenix.service.card.management.ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementGetStoredCreditCardApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementUpdateStoredCreditCardExpirationDateApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.LocationIdentifier;
import com.apd.phoenix.service.card.management.StoredCardCreditCardManagementResult;
import com.apd.phoenix.service.card.management.StoredCreditCard;
import com.apd.phoenix.service.card.management.TokenCreditCardManagementResult;
import com.apd.phoenix.service.card.management.UpdateStoredCardExpirationDateParams;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.payment.ws.AuthorizeAndCaptureParams;
import com.apd.phoenix.service.payment.ws.AuthorizeParams;
import com.apd.phoenix.service.payment.ws.CaptureParams;
import com.apd.phoenix.service.payment.ws.CardVerificationParams;
import com.apd.phoenix.service.payment.ws.ClientCredentials;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionService;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionStatusResult;
import com.apd.phoenix.service.payment.ws.CreditParams;
import com.apd.phoenix.service.payment.ws.GetTransactionStatusParams;
import com.apd.phoenix.service.payment.ws.ICreditCardTransaction;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionGetTransactionStatusApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.ObjectFactory;
import com.apd.phoenix.service.payment.ws.TerminalIdentifier;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class PaymentGatewayService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayService.class);
    private CreditCardTransactionService ss;
    private ICreditCardTransaction port;
    protected CreditCardManagementService cardManagementService;
    protected ICreditCardManagement cardManagementPort;
    private static final PropertiesConfiguration props = PaymentGatewayPropertiesLoader.getInstance().getProperties();
    private static final String clientCode = props.getString("ClientCode");
    private static final String username = props.getString("UserName");
    private static final String password = props.getString("Password");
    private static final String locationCode = props.getString("LocationCode");
    private static final String merchantCode = props.getString("MerchantCode");
    private static final String terminalCode = props.getString("TerminalCode");
    private static final String DEFAULT_CARD_VAULT_CUSTOMER_CODE = props.getString("DEFAULT_CARD_VAULT_CUSTOMER_CODE");
    protected static final String CUSTOMER_CODE_1 = "APD";
    private static final long DEFAULT_TIMEOUT_SECONDS = 10;
    private TerminalIdentifier terminalIdentifier;
    private ClientCredentials clientCredentials;
    private ObjectFactory factory;
    @Inject
    private CreditCardTransactionLogBp creditCardTransactionLogBp;
    @Inject
    private SequenceDao sequenceDao;
    @Inject
    private CustomerOrderBp customerOrderBp;
    @Inject
    private PersonBp personBp;
    @Inject
    private LineItemXShipmentBp lineItemXShipmentBp;
    @Inject
    private LineItemXReturnBp lineItemXReturnBp;
    protected com.apd.phoenix.service.card.management.ClientCredentials cardManagementclientCredentials;
    protected LocationIdentifier locationIdentifier;
    protected com.apd.phoenix.service.card.management.ObjectFactory cardManagerFactory;
    private CustomerIdentifier defaultAPDCustomerID;

    public PaymentGatewayService() {
    }

    @PostConstruct
    public void init() {
        this.factory = new ObjectFactory();
        this.cardManagerFactory = new com.apd.phoenix.service.card.management.ObjectFactory();
        ss = new CreditCardTransactionService();
        port = ss.getCreditCardTransationService();
        long serviceConnectTimeout = props.getLong("serviceConnectTimeoutSeconds", DEFAULT_TIMEOUT_SECONDS) * 1000;
        long serviceReceiveTimeout = props.getLong("serviceReceiveTimeoutSeconds", DEFAULT_TIMEOUT_SECONDS) * 1000;
        ((BindingProvider) port).getRequestContext().put("javax.xml.ws.client.connectionTimeout",
                serviceConnectTimeout + "");
        ((BindingProvider) port).getRequestContext().put("javax.xml.ws.client.receiveTimeout",
                serviceReceiveTimeout + "");
        cardManagementService = new CreditCardManagementService();
        cardManagementPort = cardManagementService.getCreditCardManagementService();
        long cardConnectTimeout = props.getLong("cardStorageConnectTimeoutSeconds", DEFAULT_TIMEOUT_SECONDS) * 1000;
        long cardReceiveTimeout = props.getLong("cardStorageReceiveTimeoutSeconds", DEFAULT_TIMEOUT_SECONDS) * 1000;
        ((BindingProvider) cardManagementPort).getRequestContext().put("javax.xml.ws.client.connectionTimeout",
                cardConnectTimeout + "");
        ((BindingProvider) cardManagementPort).getRequestContext().put("javax.xml.ws.client.receiveTimeout",
                cardReceiveTimeout + "");
        initTerminalIdentifier();
        initClientCredentials();
        initCardManagementClientCredentials();
        initLocationIdentifier();
        initDefaultCustomerIdentifier();
    }

    private void initTerminalIdentifier() {
        terminalIdentifier = createTerminalIdentifier(locationCode, merchantCode, terminalCode);
    }

    private void initClientCredentials() {
        clientCredentials = createClientCredentials(clientCode, username, password);
    }

    private void initCardManagementClientCredentials() {
        cardManagementclientCredentials = cardManagerFactory.createClientCredentials();
        cardManagementclientCredentials.setClientCode(cardManagerFactory.createClientCredentialsClientCode(clientCode));
        cardManagementclientCredentials.setUserName(cardManagerFactory.createClientCredentialsUserName(username));
        cardManagementclientCredentials.setPassword(cardManagerFactory.createClientCredentialsPassword(password));
    }

    private void initLocationIdentifier() {
        locationIdentifier = cardManagerFactory.createLocationIdentifier();
        locationIdentifier.setLocationCode(cardManagerFactory.createLocationIdentifierLocationCode(locationCode));
        locationIdentifier.setMerchantCode(cardManagerFactory.createLocationIdentifierMerchantCode(merchantCode));
    }

    private CustomerIdentifier initDefaultCustomerIdentifier() {
        defaultAPDCustomerID = cardManagerFactory.createCustomerIdentifier();
        defaultAPDCustomerID.setCustomerCode(cardManagerFactory
                .createCustomerIdentifierCustomerCode(DEFAULT_CARD_VAULT_CUSTOMER_CODE));
        defaultAPDCustomerID.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));
        defaultAPDCustomerID.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
        return defaultAPDCustomerID;
    }

    public PaymentGatewayTransaction createPaymentGatewayTransaction(CustomerOrder customerOrder) {
        return PaymentGatewayObjectFactory.createPaymentGatewayTransaction(customerOrder);
    }

    public PaymentGatewayTransaction createPaymentGatewayTransaction(Long customerOrderId) {
        CustomerOrder customerOrder = customerOrderBp.findById(customerOrderId, CustomerOrder.class);
        return createPaymentGatewayTransaction(customerOrder);
    }

    public VerificationTransaction createVerificationTransaction(CustomerOrder customerOrder) {
        return PaymentGatewayObjectFactory.createVerificationTransaction(customerOrder);
    }

    public VerificationTransaction createVerificationTransaction(CustomerOrder customerOrder, String cvv) {
        return PaymentGatewayObjectFactory.createVerificationTransaction(customerOrder, cvv);
    }

    public PaymentGatewayTransaction createPaymentGatewayTransaction(CustomerOrder customerOrder,
            Set<LineItemXShipment> contents) {
        customerOrder = customerOrderBp.findById(customerOrder.getId(), CustomerOrder.class);
        for (LineItemXShipment lineItemXShipment : contents) {
            lineItemXShipment = lineItemXShipmentBp.findById(lineItemXShipment.getId(), LineItemXShipment.class);
        }
        return PaymentGatewayObjectFactory.createPaymentGatewayTransaction(customerOrder, contents);
    }

    public CreditCardTransactionResult authorize(PaymentGatewayTransaction transaction) {
        if (logger.isDebugEnabled()) {
            logger.debug("Starting authorization transaction");
        }
        String transactionKey = createTransactionKey(transaction);
        CreditCardTransactionLog transactionLog = createCreditCardTransactionLog(transaction, transactionKey);
        transactionLog.setTransactionType(TransactionType.AUTHORIZE.getTransactionValue());
        CreditCardTransactionResult result = null;
        try {
            CreditCardTransaction creditCardTransaction = transaction.getCreditCardTransaction();
            creditCardTransaction.setTransactionKey(factory.createCreditCardTransactionTransactionKey(transactionKey));
            AuthorizeParams params = createAuthorizeParams(terminalIdentifier, creditCardTransaction);
            result = port.authorize(clientCredentials, params);
            transactionLog.setTransactionSuccess(result.isSucceeded());
            if (!result.isSucceeded()) {
                transactionLog.setFailureReason(result.getFailureReason().value());
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    transactionLog.setFailureDetails(failureReasons);
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (result instanceof CreditCardTransactionAuthorizationResult) {
                    JAXBElement<String> authCode = ((CreditCardTransactionAuthorizationResult) result).getAuthCode();
                    transactionLog.setAuthCode(authCode == null ? null : authCode.getValue());
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Authorization transaction processed successfully.");
                }
            }
        }
        catch (Exception ex) {
            transactionLog.setTransactionSuccess(false);
            transactionLog.setFailureReason(StringUtils.defaultIfBlank(ex.getMessage(), ex.getClass().getName()));
            logger.error("Error occurred while attempting to Authorize transaction.", ex);
        }
        creditCardTransactionLogBp.update(transactionLog);
        return result;
    }

    public CreditCardTransactionResult authorizeAndCapture(PaymentGatewayTransaction transaction,
            Set<LineItemXShipment> items) {
        String transactionKey = createTransactionKey(transaction);
        CreditCardTransactionLog transactionLog = createCreditCardTransactionLog(transaction, transactionKey);
        transactionLog.setTransactionType(TransactionType.AUTHORIZE_CAPTURE.getTransactionValue());
        transactionLog.getLineItemXShipments().addAll(items);
        CreditCardTransactionResult result = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("AuthorizeAndCapture transaction started.");
            }
            CreditCardTransaction creditCardTransaction = transaction.getCreditCardTransaction();
            creditCardTransaction.setTransactionKey(factory.createCreditCardTransactionTransactionKey(transactionKey));
            AuthorizeAndCaptureParams params = createAuthorizeAndCaptureParams(terminalIdentifier,
                    creditCardTransaction);
            result = port.authorizeAndCapture(clientCredentials, params);
            transactionLog.setTransactionSuccess(result.isSucceeded());
            if (!result.isSucceeded()) {
                transactionLog.setFailureReason(result.getFailureReason().value());
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    transactionLog.setFailureDetails(failureReasons);
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (result instanceof CreditCardTransactionAuthorizationResult) {
                    JAXBElement<String> authCode = ((CreditCardTransactionAuthorizationResult) result).getAuthCode();
                    transactionLog.setAuthCode(authCode == null ? null : authCode.getValue());
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("AuthorizeAndCapture transaction processed successfully.");
                }
            }
        }
        catch (Exception ex) {
            transactionLog.setTransactionSuccess(false);
            transactionLog.setFailureReason(StringUtils.defaultIfBlank(ex.getMessage(), ex.getClass().getName()));
            logger.error("Error occurred while attempting AuthorizeAndCapture transaction.", ex);
        }
        creditCardTransactionLogBp.update(transactionLog);
        return result;
    }

    public CreditCardTransactionResult capture(PaymentGatewayTransaction transaction, Set<LineItemXShipment> items)
            throws TransactionKeyNotFoundException {
        CreditCardTransactionResult result = null;
        CreditCardTransactionLog transactionLog = null;
        String transactionKey;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Capture transaction started.");
            }
            CreditCardTransaction creditCardTransaction = transaction.getCreditCardTransaction();
            if (creditCardTransaction.getTransactionKey() == null
                    || StringUtils.isBlank(creditCardTransaction.getTransactionKey().getValue())) {
                throw new TransactionKeyNotFoundException(
                        "Could not locate transaction key which is required for capturing pre-authorized funds.");
            }
            transactionKey = creditCardTransaction.getTransactionKey().getValue();
            transactionLog = createCreditCardTransactionLog(transaction, transactionKey);
            transactionLog.getLineItemXShipments().addAll(items);
            transactionLog.setTransactionType(TransactionType.CAPTURE.getTransactionValue());
            CaptureParams params = createCaptureParams(terminalIdentifier, creditCardTransaction);
            result = port.capture(clientCredentials, params);
            transactionLog.setTransactionSuccess(result.isSucceeded());
            if (!result.isSucceeded()) {
                transactionLog.setFailureReason(result.getFailureReason().value());
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    transactionLog.setFailureDetails(failureReasons);
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Capture transaction processed successfully.");
                }
            }
        }
        catch (Exception ex) {
            if (ex instanceof TransactionKeyNotFoundException) {
                throw (TransactionKeyNotFoundException) ex;
            }
            logger.error("Error during capture transaction", ex);
            if (transactionLog != null) {
                transactionLog.setTransactionSuccess(false);
                transactionLog.setFailureReason(StringUtils.defaultIfBlank(ex.getMessage(), ex.getClass().getName()));
            }
            else {
                logger.error("Severe error in capture transaction, no creditCardTransactionLog was created.");
            }

        }
        creditCardTransactionLogBp.update(transactionLog);
        return result;
    }

    public CreditCardTransactionStatusResult getTransactionStatus(String transactionId) {
        GetTransactionStatusParams getTransactionStatusParams = PaymentGatewayObjectFactory
                .createGetTransactionStatusParams(terminalIdentifier, transactionId);
        try {
            return port.getTransactionStatus(clientCredentials, getTransactionStatusParams);
        }
        catch (ICreditCardTransactionGetTransactionStatusApplicationFaultFaultFaultMessage e) {
            logger.error("Could not get Transaction Status: " + e.getLocalizedMessage());
            return null;
        }
    }

    public CreditCardTransactionResult credit(PaymentGatewayTransaction transaction, ReturnOrder returnOrder) {
        String transactionKey = createTransactionKey(transaction);
        CreditCardTransactionLog transactionLog = createCreditCardTransactionLog(transaction, transactionKey);
        transactionLog.setReturnOrder(returnOrder);
        transactionLog.setTransactionType(TransactionType.CREDIT.getTransactionValue());
        CreditCardTransactionResult result = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Credit transaction started.");
            }
            CreditCardTransaction creditCardTransaction = transaction.getCreditCardTransaction();
            creditCardTransaction.setTransactionKey(factory.createCreditCardTransactionTransactionKey(transactionKey));
            CreditParams params = createCreditParams(terminalIdentifier, creditCardTransaction);
            result = port.credit(clientCredentials, params);
            transactionLog.setTransactionSuccess(result.isSucceeded());
            if (!result.isSucceeded()) {
                transactionLog.setFailureReason(result.getFailureReason().value());
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    transactionLog.setFailureDetails(failureReasons);
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Credit transaction processed successfully.");
                }
            }
        }
        catch (Exception ex) {
            transactionLog.setTransactionSuccess(false);
            transactionLog.setFailureReason(StringUtils.defaultIfBlank(ex.getMessage(), ex.getClass().getName()));
            logger.error("Error occurred while attempting Credit transaction.", ex);
        }
        creditCardTransactionLogBp.update(transactionLog);
        return result;
    }

    public CreditCardTransactionResult verification(VerificationTransaction transaction) {
        String transactionKey = createTransactionKey(transaction);
        CreditCardTransactionResult result = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Verification transaction started.");
            }
            CreditCardTransactionCreditCard creditCard = transaction.getCreditCard();
            CardVerificationParams params = createVerificationParams(terminalIdentifier, creditCard);
            params.setTransactionKey(factory.createCardVerificationParamsTransactionKey(transactionKey));
            result = port.cardVerification(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Verification transaction processed successfully.");
                }
            }
        }
        catch (Exception ex) {
            logger.error("Error occurred while attempting Verification transaction.", ex);
        }
        return result;
    }

    private CreditCardTransactionLog createCreditCardTransactionLog(PaymentGatewayTransaction transaction,
            String transactionKey) {
        CreditCardTransactionLog transactionLog = new CreditCardTransactionLog();
        transactionLog.setTimestamp(new Date());
        transactionLog.setEventType(OrderLog.EventType.CREDIT_CARD_TRANSACTION);
        CustomerOrder customerOrder = customerOrderBp.findById(transaction.getCustomerOrder().getId(),
                CustomerOrder.class);
        transactionLog.setCardExpriation(customerOrder.getPaymentInformation().getCard().getExpiration());
        transactionLog.setCustomerOrder(customerOrder);
        transactionLog.setApdPo(transaction.getApdPoNumber());
        if (StringUtils.isNotBlank(transaction.getCustomerPoNumber())) {
            transactionLog.setCustPo(transaction.getCustomerPoNumber());
        }
        if (StringUtils.isNotBlank(transaction.getNameOnCard())) {
            transactionLog.setNameOnCard(transaction.getNameOnCard());
        }
        if (StringUtils.isNotBlank(transaction.getCardIdentifier())) {
            transactionLog.setCardIdentifier(transaction.getCardIdentifier());
        }

        if (StringUtils.isBlank(transaction.getStoredCardToken())) {
            throw new IllegalStateException(
                    "A stored card token is required before a credit card transaction can be processed.");
        }
        transactionLog.setEventType(OrderLog.EventType.CREDIT_CARD_TRANSACTION);
        transactionLog.setOrderStatus(customerOrder.getStatus());
        transactionLog.setFreightAmount(transaction.getTotalFreightAmount());
        transactionLog.setSalesTax(transaction.getSalesTaxAmount());
        transactionLog.setSubtotal(transaction.getSubTotalAmount());

        transactionLog.setTransactionKey(transactionKey);

        //total amount and transaction amount are duplicates
        transactionLog.setTotalAmount(transaction.getTotalAmount());
        //total amount and transaction amount are duplicates
        transactionLog.setTransactionAmount(transaction.getTotalAmount());

        if (transaction.getArrayOfLineItemDetail() != null) {
            transactionLog.setItemCount(transaction.getArrayOfLineItemDetail().getLineItemDetail().size());
        }
        if (StringUtils.isNotBlank(transaction.getTrackingNumber())) {
            transactionLog.setTrackingNum(transaction.getTrackingNumber());
        }
        return transactionLog;
    }

    private String createTransactionKey(PaymentGatewayTransaction transaction) {
        return transaction.getApdPoNumber() + "-" + sequenceDao.nextVal(SequenceDao.Sequence.TRANSACTION_KEY);
    }

    /**
     * This method takes a CardInformation object, and returns that same object,
     * such that the card will be compliant with regulations and project
     * requirements.
     *
     * @param paymentInformation
     * @return
     * @throws com.apd.phoenix.service.payment.ws.client.PaymentGatewayService.CardVaultStorageException
     */
    public CardInformation storeCreditCard(PaymentInformation paymentInformation) throws CardVaultStorageException {
        CardInformation card = paymentInformation.getCard();
        StoredCreditCard storedCreditCard = createStoredCreditCard(paymentInformation);
        String token = storeOrRetrieveCard(storedCreditCard);
        card.setCardVaultToken(token);
        card.setIdentifier(getTruncatedCardNumber(card.getNumber()));
        card.setNumber(null);
        return card;
    }

    private String storeOrRetrieveCard(StoredCreditCard storedCreditCard) {
        String vaultToken = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Attempting to Store Card in Vault");
            }
            AddStoredCardParams addStoredCardParams;
            addStoredCardParams = cardManagerFactory.createAddStoredCardParams();
            addStoredCardParams.setCustomerIdentifier(cardManagerFactory
                    .createAddStoredCardParamsCustomerIdentifier(defaultAPDCustomerID));
            addStoredCardParams.setCreditCard(cardManagerFactory.createAddStoredCardParamsCreditCard(storedCreditCard));
            TokenCreditCardManagementResult result = cardManagementPort.addStoredCreditCard(
                    cardManagementclientCredentials, addStoredCardParams);
            if (!result.isSucceeded()) {
                if (result.getFailureReason().equals(CreditCardManagementFailureReason.CARD_NUMBER_IN_USE)) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Card is already stored in vault.");
                    }
                    vaultToken = getTokenForExistingCard(storedCreditCard.getCardAccountNumber().getValue());
                    //This check is needed to handle possible existing card with the same card number not being an exact match
                    updatedStoredCardIfNotExactMatch(storedCreditCard, vaultToken);
                }
                else {
                    if (result.getValidationFailures().getValue() != null) {
                        List<String> failureReasons = result.getValidationFailures().getValue().getString();
                        for (String failureDetail : failureReasons) {
                            logger
                                    .error("Error in soap exchange with payment gateway. FAILURE REASON:"
                                            + failureDetail);
                        }
                    }
                    else {
                        logger.error("Credit card store failed without giving a reason.");
                    }

                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Card successfully stored in vault.");
                }
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage ex) {
            logger.error("Error occurred while attempting to store or retrieve credit card.", ex);
        }
        if (StringUtils.isEmpty(vaultToken)) {
            logger.error("No vault token returned.");
        }
        return vaultToken;
    }

    //This method is needed to handle possible existing card with the same card number not being an exact match
    private void updatedStoredCardIfNotExactMatch(StoredCreditCard storedCreditCard, String vaultToken) {
        if (logger.isDebugEnabled()) {
            logger.debug("Retrieving stored card from vault to validate exact match.");
        }
        try {
            GetStoredCreditCardParams getStoredCreditCardParams;
            getStoredCreditCardParams = cardManagerFactory.createGetStoredCreditCardParams();
            getStoredCreditCardParams.setCustomerIdentifier(cardManagerFactory
                    .createGetCustomerParamsCustomerIdentifier(defaultAPDCustomerID));
            getStoredCreditCardParams.setToken(cardManagerFactory.createGetStoredCreditCardParamsToken(vaultToken));
            StoredCardCreditCardManagementResult result = cardManagementPort.getStoredCreditCard(
                    cardManagementclientCredentials, getStoredCreditCardParams);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                StoredCreditCard cardFromVault = result.getCreditCard().getValue();
                if (!isExpirationDateExactMatch(storedCreditCard, cardFromVault)) {
                    updateStoredCreditCardExpiratation(storedCreditCard, vaultToken);
                }
            }
        }
        catch (ICreditCardManagementGetStoredCreditCardApplicationFaultFaultFaultMessage ex) {
            logger.error("Error occurred while attempting to retrieve stored credit card.", ex);
        }
    }

    private void updateStoredCreditCardExpiratation(StoredCreditCard storedCreditCard, String vaultToken) {
        if (logger.isDebugEnabled()) {
            logger.debug("Updating stored card expriation date.");
        }
        try {
            UpdateStoredCardExpirationDateParams updateStoredCardExpirationDateParams = cardManagerFactory
                    .createUpdateStoredCardExpirationDateParams();
            updateStoredCardExpirationDateParams.setCustomerIdentifier(cardManagerFactory
                    .createUpdateStoredCardParamsCustomerIdentifier(defaultAPDCustomerID));
            updateStoredCardExpirationDateParams.setToken(cardManagerFactory
                    .createUpdateStoredCardExpirationDateParamsToken(vaultToken));
            updateStoredCardExpirationDateParams.setExpirationMonth(storedCreditCard.getExpirationMonth());
            updateStoredCardExpirationDateParams.setExpirationYear(storedCreditCard.getExpirationYear());
            CreditCardManagementResult result = cardManagementPort.updateStoredCreditCardExpirationDate(
                    cardManagementclientCredentials, updateStoredCardExpirationDateParams);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }

        }
        catch (ICreditCardManagementUpdateStoredCreditCardExpirationDateApplicationFaultFaultFaultMessage ex) {
            logger.error("Error occurred while attempting to update stored credit card.", ex);
        }
    }

    //This mechanism is needed to handle possible existing card with the same card number not being an exact match
    private boolean isExpirationDateExactMatch(StoredCreditCard storedCreditCard, StoredCreditCard cardFromVault) {
        //compare expiration dates
        if (storedCreditCard.getExpirationMonth().equals(cardFromVault.getExpirationMonth())) {
            if (storedCreditCard.getExpirationYear().equals(cardFromVault.getExpirationYear())) {
                return true;
            }
        }
        return false;
    }

    private String getTokenForExistingCard(String cardNumber) {
        String vaultToken = null;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving token for existing card from the vault.");
            }
            GetTokenForCardNumberParams getParams = createGetTokenForCardNumberParams(cardNumber);
            TokenCreditCardManagementResult result = cardManagementPort.getTokenForCardNumber(
                    cardManagementclientCredentials, getParams);
            if (!result.isSucceeded()) {
                logger.error("Unable to retrieve token from the vault.");
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Token retrieved successfully from the vault.");
                }
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage ex) {
            logger.error("Error occurred while attempting to get token for card.", ex);
        }
        return vaultToken;
    }

    private GetTokenForCardNumberParams createGetTokenForCardNumberParams(String cardNumber) {
        GetTokenForCardNumberParams getParams = cardManagerFactory.createGetTokenForCardNumberParams();
        getParams.setAccountNumber(cardManagerFactory.createGetTokenForCardNumberParamsAccountNumber(cardNumber));
        getParams.setCustomerIdentifier(cardManagerFactory
                .createGetTokenForCardNumberParamsCustomerIdentifier(defaultAPDCustomerID));
        return getParams;
    }

    private String getTruncatedCardNumber(String cardNumber) {
        cardNumber = cardNumber.trim();
        int len = cardNumber.length() - 4;
        StringBuilder replace = new StringBuilder();
        for (int i = 0; i < len; ++i) {
            replace.append("X");
        }
        replace.append(len >= 0 ? cardNumber.substring(len) : "");
        return replace.toString();
    }

    private com.apd.phoenix.service.card.management.Address createCardVaultAddress(
            com.apd.phoenix.service.model.Address address) {
        if (address == null) {
            return null;
        }
        com.apd.phoenix.service.card.management.Address toReturn = cardManagerFactory.createAddress();
        if (StringUtils.isNotBlank(address.getLine1())) {
            toReturn.setAddressLine1(cardManagerFactory.createAddressAddressLine1(address.getLine1().trim()));
        }
        if (StringUtils.isNotBlank(address.getLine2())) {
            toReturn.setAddressLine2(cardManagerFactory.createAddressAddressLine2(address.getLine2().trim()));
        }
        if (StringUtils.isNotBlank(address.getCity())) {
            toReturn.setCity(cardManagerFactory.createAddressCity(address.getCity().trim()));
        }
        if (StringUtils.isNotBlank(address.getCountry())) {
            toReturn.setCountryCode(com.apd.phoenix.service.card.management.CountryCode.fromValue(address.getCountry()
                    .trim()));
        }
        if (StringUtils.isNotBlank(address.getZip())) {
            toReturn.setPostalCode(cardManagerFactory.createAddressPostalCode(address.getZip().trim()));
        }
        if (StringUtils.isNotBlank(address.getState())) {
            toReturn.setStateProvinceCode(com.apd.phoenix.service.card.management.StateProvinceCode.fromValue(address
                    .getState().trim()));
        }
        return toReturn;
    }

    private StoredCreditCard createStoredCreditCard(PaymentInformation paymentInformation)
            throws CardVaultStorageException {
        CardInformation cardInformation = paymentInformation.getCard();
        StoredCreditCard storedCreditCard;
        storedCreditCard = cardManagerFactory.createStoredCreditCard();
        try {
            if (paymentInformation.getBillingAddress() != null) {
                com.apd.phoenix.service.card.management.Address billingAddress = createCardVaultAddress(paymentInformation
                        .getBillingAddress());
                storedCreditCard.setBillingAddress(cardManagerFactory.createCreditCardBillingAddress(billingAddress));
            }

            if (StringUtils.isNotBlank(cardInformation.getNumber())) {
                storedCreditCard.setCardAccountNumber(cardManagerFactory
                        .createCreditCardCardAccountNumber(cardInformation.getNumber().trim()));
            }
            else {
                throw new IllegalStateException("Card NUmber is Required for storage in Card vault");
            }

            if (paymentInformation.getContact() != null && paymentInformation.getContact().getId() != null) {
                com.apd.phoenix.service.card.management.Contact contact = createCardVaultContact(paymentInformation
                        .getContact());
                storedCreditCard.setCardholder(cardManagerFactory.createCreditCardCardholder(contact));
            }

            if (cardInformation.getExpiration() != null) {
                XMLGregorianCalendar expirationDate = createXMLGregorianCalendar(cardInformation.getExpiration());
                storedCreditCard.setExpirationMonth(expirationDate.getMonth());
                storedCreditCard.setExpirationYear(expirationDate.getYear());
            }

            if (StringUtils.isNotBlank(cardInformation.getNameOnCard())) {
                storedCreditCard.setNameOnCard(cardManagerFactory.createCreditCardNameOnCard(cardInformation
                        .getNameOnCard().trim()));
            }
        }
        catch (IllegalStateException ex) {
            logger.error(ex.getMessage(), ex);
            throw new CardVaultStorageException(ex.getMessage());
        }

        return storedCreditCard;
    }

    private com.apd.phoenix.service.card.management.Contact createCardVaultContact(Person person) {
        person = personBp.findById(person.getId(), Person.class);
        com.apd.phoenix.service.card.management.Contact contact = cardManagerFactory.createContact();
        if (StringUtils.isNotBlank(person.getEmail())) {
            com.apd.phoenix.service.card.management.Email email = cardManagerFactory.createEmail();
            email.setAddress(cardManagerFactory.createEmailAddress(person.getEmail()));
            contact.setEmail(cardManagerFactory.createContactEmail(email));
        }
        if (StringUtils.isNotBlank(person.getFirstName())) {
            contact.setFirstName(cardManagerFactory.createContactFirstName(person.getFirstName().trim()));
        }
        if (StringUtils.isNotBlank(person.getMiddleInitial())) {
            contact.setMiddleName(cardManagerFactory.createContactMiddleName(person.getMiddleInitial().trim()));
        }
        if (StringUtils.isNotBlank(person.getLastName())) {
            contact.setLastName(cardManagerFactory.createContactLastName(person.getLastName().trim()));
        }
        if (!person.getPhoneNumbers().isEmpty()) {
            for (PhoneNumber phoneNumber : person.getPhoneNumbers()) {
                String number = phoneNumber.toString();
                contact.setPhone(cardManagerFactory.createContactPhone(number.trim()));
                break;
            }
        }
        return contact;
    }

    public PaymentGatewayTransaction createReturnPaymentGatewayTransaction(CustomerOrder customerOrder,
            Set<LineItemXReturn> items) {
        customerOrder = customerOrderBp.findById(customerOrder.getId(), CustomerOrder.class);
        for (LineItemXReturn lineItemXReturn : items) {
            lineItemXReturn = lineItemXReturnBp.findById(lineItemXReturn.getId(), LineItemXReturn.class);
        }
        return PaymentGatewayObjectFactory.createReturnPaymentGatewayTransaction(customerOrder, items);
    }

    public class CardVaultStorageException extends Exception {

        private static final long serialVersionUID = 7613658254725698556L;

        public CardVaultStorageException() {
            super();
        }

        public CardVaultStorageException(String message) {
            super(message);
        }
    }
}
