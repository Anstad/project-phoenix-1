package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.NotAudited;

/**
 * This entity contains the information relevant for an account.
 * 
 * @author RHC
 * 
 */
@Entity
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Audited
public class Account implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7185662040258275545L;
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Column(nullable = false)
    private boolean isActive = true;

    //Sometimes referred to as sector for NGC/HII
    @Column
    private String apdAssignedAccountId;

    @Column(nullable = false)
    private String solomonCustomerId;

    @Temporal(TemporalType.DATE)
    private Date creationDate = new Date();

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @XmlTransient
    private Set<Address> addresses = new HashSet<Address>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PaymentInformation paymentInformation;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.DETACH })
    @XmlTransient
    private Set<Bulletin> bulletins = new HashSet<Bulletin>();

    @ManyToMany(fetch = FetchType.LAZY)
    @XmlTransient
    private Set<Group> groups = new HashSet<Group>();

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @XmlTransient
    @NotAudited
    private Set<ProcessConfiguration> processConfigurations;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.DETACH })
    @XmlTransient
    @NotAudited
    private Set<PoNumber> blanketPos = new HashSet<PoNumber>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id")
    @XmlTransient
    @AuditJoinTable(name = "acxacpt_AUD")
    @NotAudited
    private Set<AccountXAccountPropertyType> properties = new HashSet<AccountXAccountPropertyType>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "account_id")
    private Set<IpAddress> ips = new HashSet<IpAddress>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "acc_id")
    @AuditJoinTable(name = "axnp_AUD")
    @NotAudited
    private Set<NotificationProperties> notificationProperties = new HashSet<NotificationProperties>();

    @OneToOne(fetch = FetchType.LAZY)
    private Person primaryContact;

    @OneToOne(fetch = FetchType.LAZY)
    private CxmlConfiguration cxmlConfiguration;

    @Column
    private String csrPhone;

    @Column
    private String csrEmail;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    @NotAudited
    private Set<CustomerSpecificIconUrl> iconUrls = new HashSet<CustomerSpecificIconUrl>();

    // Number used for inbound CXML order requests to designate which child account will be used.
    @Column
    private String locationCode;

    // Whether to use this account or a child account designated by its location code
    @Column
    private Boolean useLocationCode;

    public boolean getIsActive() {
        return this.isActive;
    }

    /**
     * Used to set whether the Account is active. Do not use this method, use
     * the deactivate and activate methods on AccountBp.
     * 
     * @param isActive
     */
    public void setIsActive(final boolean isActive) {
        this.isActive = isActive;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        try {
            that = (Account) that;
            if (this.getId() != null) {
                return this.getId().equals(((Account) that).getId());
            }
            return super.equals(that);
        }
        catch (Exception e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private Account rootAccount;

    public Account getRootAccount() {
        return this.rootAccount;
    }

    public void setRootAccount(final Account rootAccount) {
        this.rootAccount = rootAccount;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @XmlTransient
    private Account parentAccount;

    public Account getParentAccount() {
        return this.parentAccount;
    }

    public void setParentAccount(final Account parentAccount) {
        this.parentAccount = parentAccount;
    }

    @Column(nullable = false, unique = true)
    @Index(name = "ACCOUNT_NAME_IDX")
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    private @OneToMany(mappedBy = "rootAccount", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @XmlTransient
    Set<Credential> credentials = new HashSet<Credential>();

    public Set<Credential> getCredentials() {
        return this.credentials;
    }

    public void setCredentials(final Set<Credential> credentials) {
        this.credentials = credentials;
    }

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @XmlTransient
    private Set<PhoneNumber> numbers = new HashSet<PhoneNumber>();

    public Set<PhoneNumber> getNumbers() {
        return this.numbers;
    }

    public void setNumbers(final Set<PhoneNumber> numbers) {
        this.numbers = numbers;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }

    /*
     * Be wary of getting all addresses for an account-- for some accounts there can be thousands
     * of Addresses, leading to performance reduction.
     */
    @Deprecated
    public Set<Address> getAddresses() {
        return this.addresses;
    }

    @Deprecated
    public void setAddresses(final Set<Address> addresses) {
        this.addresses = addresses;
    }

    public PaymentInformation getPaymentInformation() {
        return this.paymentInformation;
    }

    public void setPaymentInformation(final PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    //Removed.  Replaced with link to PaymentType.
    //	public PaymentOption getPaymentOption() {
    //		return this.paymentOption;
    //	}
    //
    //	public void setPaymentOption(final PaymentOption paymentOption) {
    //		this.paymentOption = paymentOption;
    //	}

    public Set<Bulletin> getBulletins() {
        return this.bulletins;
    }

    public void setBulletins(final Set<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    public Set<Group> getGroups() {
        return this.groups;
    }

    public void setGroups(final Set<Group> groups) {
        this.groups = groups;
    }

    public Set<ProcessConfiguration> getProcessConfigurations() {
        return this.processConfigurations;
    }

    public void setProcessConfiguration(final Set<ProcessConfiguration> processConfigurations) {
        this.processConfigurations = processConfigurations;
    }

    public Set<PoNumber> getBlanketPos() {
        return this.blanketPos;
    }

    public void setBlanketPos(final Set<PoNumber> blanketPos) {
        this.blanketPos = blanketPos;
    }

    public Set<AccountXAccountPropertyType> getProperties() {
        return this.properties;
    }

    public void setProperties(final Set<AccountXAccountPropertyType> properties) {
        this.properties = properties;
    }

    public Set<IpAddress> getIps() {
        return this.ips;
    }

    public void setIps(final Set<IpAddress> ips) {
        this.ips = ips;
    }

    public String getApdAssignedAccountId() {
        return apdAssignedAccountId;
    }

    public void setApdAssignedAccountId(String apdAssignedAccountId) {
        this.apdAssignedAccountId = apdAssignedAccountId;
    }

    public Set<NotificationProperties> getNotificationProperties() {
        return this.notificationProperties;
    }

    public void setNotificationProperties(final Set<NotificationProperties> notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    public Person getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(Person primaryContact) {
        this.primaryContact = primaryContact;
    }

    public CxmlConfiguration getCxmlConfiguration() {
        return cxmlConfiguration;
    }

    public void setCxmlConfiguration(CxmlConfiguration cxmlConfiguration) {
        this.cxmlConfiguration = cxmlConfiguration;
    }

    public String getCsrPhone() {
        return csrPhone;
    }

    public void setCsrPhone(String csrPhone) {
        this.csrPhone = csrPhone;
    }

    public String getCsrEmail() {
        return csrEmail;
    }

    public void setCsrEmail(String csrEmail) {
        this.csrEmail = csrEmail;
    }

    public Set<CustomerSpecificIconUrl> getIconUrls() {
        return iconUrls;
    }

    public void setIconUrls(Set<CustomerSpecificIconUrl> iconUrls) {
        this.iconUrls = iconUrls;
    }

    public String getSolomonCustomerId() {
        return solomonCustomerId;
    }

    public void setSolomonCustomerId(String solomonCustomerId) {
        this.solomonCustomerId = solomonCustomerId;
    }

    /**
     * @return the locationCode
     */
    public String getLocationCode() {
        return locationCode;
    }

    /**
     * @param locationCode the locationCode to set
     */
    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    /**
     * @return the useLocationCode
     */
    public Boolean getUseLocationCode() {
        return useLocationCode;
    }

    /**
     * @param useLocationCode the useLocationCode to set
     */
    public void setUseLocationCode(Boolean useLocationCode) {
        this.useLocationCode = useLocationCode;
    }

}