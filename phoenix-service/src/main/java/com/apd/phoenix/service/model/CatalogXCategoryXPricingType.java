package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

@Entity
@Audited
@AuditTable(value = "cxcxpt_AUD")
public class CatalogXCategoryXPricingType implements Serializable, com.apd.phoenix.service.model.Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog catalog;

    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    private ItemCategory category;

    @ManyToOne(fetch = FetchType.EAGER)
    private PricingType type;

    @Column
    private String parameter;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CatalogXCategoryXPricingType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Catalog getCatalog() {
        return this.catalog;
    }

    public void setCatalog(final Catalog catalog) {
        this.catalog = catalog;
    }

    public ItemCategory getCategory() {
        return this.category;
    }

    public void setCategory(final ItemCategory category) {
        this.category = category;
    }

    public PricingType getType() {
        return this.type;
    }

    public void setType(final PricingType type) {
        this.type = type;
    }

    public String getParameter() {
        return this.parameter;
    }

    public void setParameter(final String parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (parameter != null && !parameter.trim().isEmpty())
            result += "parameter: " + parameter;
        return result;
    }
}