package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.annotations.Cache;
import org.hibernate.envers.Audited;

/**
 * This entity is used store the different types of properties of a CatalogXItem relationship.
 * 
 * @author RHC
 *
 */
@Entity
@Cacheable
@Audited
public class ItemPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -8376938520036961039L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false, unique = true)
    private String name;

    @Column
    private String iconUrl;

    @Column
    private String toolTipLabel;

    // TODO: Make this a true boolean field in the database.  (i.e. Char Y/N or String true/false)
    @Column(nullable = false)
    private boolean supplementary;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getToolTipLabel() {
        return toolTipLabel;
    }

    public void setToolTipLabel(String toolTipLabel) {
        this.toolTipLabel = toolTipLabel;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }

    public boolean isSupplementary() {
        return this.supplementary;
    }

    public void setSupplementary(boolean supplementary) {
        this.supplementary = supplementary;
    }

}
