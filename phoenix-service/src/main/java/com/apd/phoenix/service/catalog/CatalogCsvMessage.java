package com.apd.phoenix.service.catalog;

import java.io.Serializable;
import java.util.ArrayList;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.SkuType;

public class CatalogCsvMessage implements Serializable {

    private ArrayList<Long> ids;
    private ArrayList<ItemPropertyType> propertyTypeList;
    private ArrayList<SkuType> skuTypeList;
    private ArrayList<String> customerList;
    private String correlationId;
    private String header;
    private Long totalItems;
    private long catalogId;
    private CatalogCsvTypeEnum catalogCsvType;

    public enum CatalogCsvTypeEnum {
        VENDOR_CATALOG, CUSTOMER_CATALOG_CSV, SMART_OCI_CATALOG_CSV("-SmartOci"), REPORT_CSV("-report");

        private final String fileNameSuffix;

        private CatalogCsvTypeEnum() {
            this.fileNameSuffix = "";
        }

        private CatalogCsvTypeEnum(String fileNameSuffix) {
            this.fileNameSuffix = fileNameSuffix;
        }

        public String getFileNameSuffix() {
            return this.fileNameSuffix;
        }
    }

    public ArrayList<Long> getIds() {
        return ids;
    }

    public void setIds(ArrayList<Long> ids) {
        this.ids = ids;
    }

    public ArrayList<ItemPropertyType> getPropertyTypeList() {
        return propertyTypeList;
    }

    public void setPropertyTypeList(ArrayList<ItemPropertyType> propertyTypeList) {
        this.propertyTypeList = propertyTypeList;
    }

    public ArrayList<SkuType> getSkuTypeList() {
        return skuTypeList;
    }

    public void setSkuTypeList(ArrayList<SkuType> skuTypeList) {
        this.skuTypeList = skuTypeList;
    }

    public ArrayList<String> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<String> customerList) {
        this.customerList = customerList;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(Long totalItems) {
        this.totalItems = totalItems;
    }

    public long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(long catalogId) {
        this.catalogId = catalogId;
    }

    public CatalogCsvTypeEnum getCatalogCsvType() {
        return this.catalogCsvType;
    }

    public void setCatalogCsvType(CatalogCsvTypeEnum catalogCsvType) {
        this.catalogCsvType = catalogCsvType;
    }
}
