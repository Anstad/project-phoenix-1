package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.WorkflowLog;
import com.apd.phoenix.service.model.WorkflowLog.TaskEvent;
import com.apd.phoenix.service.persistence.jpa.WorkflowLogDao;

@Stateless
@LocalBean
public class WorkflowLogBp extends AbstractBp<WorkflowLog> {

    @Inject
    public void initDao(WorkflowLogDao dao) {
        this.dao = dao;
    }

    public void createWorkflowLog(long taskId, String user, TaskEvent event) {
        this.createWorkflowLog(taskId, user, null, event);
    }

    public void createWorkflowLog(long taskId, String user, String target, TaskEvent event) {
        WorkflowLog newLog = new WorkflowLog();
        newLog.setChangeDate(new Date());
        newLog.setCsrUser(user);
        newLog.setTarget(target);
        newLog.setTaskEvent(event);
        newLog.setTaskId(taskId);
        this.dao.create(newLog);
    }

    public List<WorkflowLog> getLogsForTask(long taskId) {
        return ((WorkflowLogDao) this.dao).getLogsForTask(taskId);
    }
}
