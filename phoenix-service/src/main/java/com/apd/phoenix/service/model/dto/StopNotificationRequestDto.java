package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class StopNotificationRequestDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private boolean encodeImage = true;

    private BigInteger max = BigInteger.valueOf(1);

    public boolean isEncodeImage() {
        return encodeImage;
    }

    public void setEncodeImage(boolean encodeImage) {
        this.encodeImage = encodeImage;
    }

    public BigInteger getMax() {
        return max;
    }

    public void setMax(BigInteger max) {
        this.max = max;
    }

}
