package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class PoNumberDto implements Serializable {

    private static final long serialVersionUID = 7265353940493286694L;
    public static final String APD = "APD";
    public static final String CUSTOMER = "Customer";
    public static final String BLANKET = "blanket";

    public PoNumberDto() {
    }

    private String value;

    private String type;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}