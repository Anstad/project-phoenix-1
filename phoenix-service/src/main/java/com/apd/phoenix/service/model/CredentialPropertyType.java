package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * This entity is used to store the different types of properties that are used by Credential.
 * 
 * @author RHC
 *
 */
@Entity
@Cacheable
@Audited
public class CredentialPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6272309289832532932L;

    /**
     * This is an enum listing the different properties that were associated with Credentials.
     * 
     * @author RHC
     *
     */
    public enum CredentialPropertyTypeEnum {
        APD_SALES_PERSON("APD sales person"), APD_ACCOUNTING_ID("APD_AccountingID"), AR_GL_ACCOUNT("AR GL account"), AR_SUBACCOUNT(
                "AR subaccount"), DISPLAY_UPS_SHIPPING_NOTICE("Display ups shipping notice on cashout"), DUNS_NUMBER(
                "DUNS number (for ariba)"), EDI_GS_RECEIVER_ID("EDI: GS receiver ID"), EDI_GS_SENDER_ID(
                "EDI: GS sender ID"), EDI_ISA_RECEIVER_ID("EDI: ISA receiver ID"), EDI_ISA_RECEIVER_QUALIFIER(
                "EDI: ISA receiver qualifier"), EDI_ISA_SENDER_ID("EDI: ISA sender ID"), EDI_ISA_SENDER_QUALIFIER(
                "EDI: ISA sender qualifier"), EDI_DEPT_NUM_ID("EDI: departmnet number identification"), EDI_FIELD_SEPARATOR(
                "EDI: field separator"), EDI_ORDER_TYPE_ID("EDI: order type identification"), EDI_RECORD_SEPARATOR(
                "EDI: record separator"), EDI_SUPPLIER_NUM_ID("EDI: supplier number identification"), SKU_ON_EMAIL(
                "SKU on emails"), US_ACCT_NUM("US Account #"), ADMIN_ORDER_SUPPLIER_SELECTION_STRATEGY(
                "admin orders: pick supplier"), ALLOW_PO_EDITING("allow PO editing"), ALLOW_ADDITIONAL_EMAILS(
                "allow additional emails"), ALLOW_ATTACHMENTS_TO_ORDERS("allow attachments to orders"), ALLOW_MILITARY_ZIP_CODES(
                "allow military zip codes"), ALLOW_REJECTED_ITEMS_TO_SHIP("allow rejected items to ship"), ALLOWED_COUNTRY_CODES(
                "allowed country codes"), ALLOWED_INVOICE_TERMS("allowed invoice terms"), AUTO_PO_EVALUATION(
                "auto PO evaluation"), BANK_ABA_NUM("bank ABA number"), BANK_ACCT_NUM("bank account number"), BATCH_CC_TRANSACTIONS(
                "batch credit card transactions"), BILLTO_HEADING_TITLE("billto heading title"), REMOVE_AUTH_SHIPTO_ADDRESS_2_3(
                "blank address 2/3 when authorized"), BULLETIN_BUTTON_LABEL("bulletin button text"), CC_PAYMENTS_PERMITTED(
                "cc payments"), CC_PAYMENTS_ALLOWED_TYPES("cc payments allowed types"), CC_PREAUTH_STRATEGY(
                "cc preauth strategy (full, one, none)"), CC_PROC_FEE_FLAT("cc proc flat per order"), CC_PROC_FEE_PERCENTAGE(
                "cc proc perc of order"), CC_MERCHANT_CODE("credit card merchant code"), CC_USE_DEFAULT_CARD(
                "use default credit card number"), CHARGE_SALES_TAX("charge sales tax"), CONCATENATE_LINE_NUM_AND_DESC(
                "concatenate order item line # to description"), CONTRACT_NUMBER("contract number"), COST_OF_GOODS(
                "cost of goods"), MAX_COST_PRICE_DISCREPANCY("cost-price discrepancy threshold"), CXML_DEFAULT_PRICE_MARGIN(
                "cxml default price margin"), CXML_UNIT_PRICE("cxml send unit price"), DAYS_TO_KEEP_HELD_ORDER(
                "days to keep order on hold"), DEFAULT_ADDRESS_WHEN_ADDING_USER("default address when adding user"), DEFAULT_INVOICE_TERMS(
                "default invoice terms"), ORDER_HISTORY_MAX_PREVIOUS_DAYS("default order history timeframe"), DEFAULT_SHIPTO_EMAIL_CONTACT(
                "default shipto email contact"), DEPLOYMENT("deployment"), DISCOUNT_ERROR_ALLOWANCE(
                "discount error allowance"), SHIPPING_RATE("shipping rate"), EMAIL_TEMPLATE("email template"), FULFILLMENT(
                "fulfillment"), HANDLING_FEE("handling fee"), INVOICE_PAYENTS_PERMITTED("invoice payments"), INVOICE_NUMBER_REQUIRED(
                "invoice number required"), MERCHANDISE_DISCOUNT("merchandise discount"), MIN_ORDER_FEE(
                "minimum order fee"), MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE("minimum order amount"), MIN_ORDER_AMT_ALLOWED(
                "minimum order amount required"), MULTIPLE_BILLTO_DROPDOWN_LABEL("multiple billto pull down heading"), MULTIPLE_SHIPTO_DROPDOWN_LABEL(
                "multiple shipto pull down heading"), USPS_USSCO_ADDITIONAL_PROCESSING("needs usps/ussco workflow"), NOTES_COMMENTS(
                "notes/comments"), REMIT_TO_MARQUETTE("orders placed under marquette"), PROTECT_SHIPTOS(
                "protect shiptos"), MIN_ORDER_FEE_COMMODITY_CODE("min_order_fee_commodity_code"), HANDLING_FEE_COMMODITY_CODE(
                "handling_fee_commodity_code"),
        //        PUNCH_FRAME_TEMPLATE("punch frame template"),
        PUNCHOUT_RETURN_BUTTON_LABEL("return button text"), REQUIRE_AUTHORIZED_SHIPTOS("require authorized shiptos"), RETRY_REFUSED_ORDER(
                "retry refused order"), ROUTE_CODE("route code"), SEND_PDF_ATTACHMENT("send PDF attachment"), SEND_TXT_ATTACHMENT(
                "send TXT attachment"), SEND_BUYER_EMAIL_TO_VENDOR("send buyer email to vendor"), SEND_DECLINES_TO(
                "send declines to"), SESSION_TIMEOUT("session timeout"), SET_LINEITEM_SHIPTO_ON_OUTBOUND_ORDER(
                "set line item shipto on outbound order"), SHIPTO_HEADING_TITLE("shipto heading title"), SKU_ON_SHOPPING_CART(
                "sku on shopping cart"), SPENDING_LIMIT_MESSAGE("spending limit message"), STOCK_CHECK_ALL_FACILITIES_ADOT(
                "check all facilities (ADOT)"), STOCK_CHECK_ZIPCODE("zip code for stock check"), TAX_EXEMPT_STATES(
                "state exemptions for sales tax"), TAX_EXEMPT_ZIPCODES("zip code exemptions for sales tax"), UNSCHEDULED_SERVICE_FEE(
                "unscheduled service fee"), USE_ISO_UOM("use ISO units of measure"), USE_ALT_BILLTOS(
                "use alternate billtos"), USE_ALT_SHIPTOS("use alternate shiptos"), USE_BILLING_PO_FOR_INVOICES(
                "use billing PO for invoices"), USE_COMMON_UOM("use common units of measure"),
        //USE_LAST_SHIPTO("use last shipto"),
        USE_ORDER_COMMENTS("use order comments"), XCBL_BUYER_MPID("xCBL buyer MPID"), XCBL_SELLER_MPID(
                "xCBL seller MPID"), ORDERS_PLACED_UNDER_MARQUETTE("orders placed under marquette"), ORDERS_PLACED_UNDER_APD_MARQUETTE(
                "orders placed under APD/Marquette"), BANNER_NAME("banner file name"), CONTACT_ADDRESSID(
                "contact address id"), DISPLAY_PRICE_INCREASE_MESSAGE("display price increase message");

        String value;

        private CredentialPropertyTypeEnum() {
            this.value = this.name();
        }

        private CredentialPropertyTypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false, unique = true)
    private String name;

    @Column
    private String defaultValue;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CredentialPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }
}
