/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import com.apd.phoenix.service.order.OrderLogConstants;

/**
 * The Class OrderLog.
 */
@Entity
@XmlRootElement
@Table(name = "ORDER_LOG")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "FROM_CLASS", discriminatorType = DiscriminatorType.STRING)
@Check(constraints = "(FROM_CLASS!='CREDITCARDTRANSACTION_LOG') OR ((ITEMCOUNT IS NOT NULL) AND (TRANSACTIONSUCCESS IS NOT NULL))")
public class OrderLog implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 5137132805044823453L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The customer order. */
    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "ORDERLOG_CUSTORDER_IDX")
    private CustomerOrder customerOrder;

    /** The order status. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private OrderStatus orderStatus;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    //    @Column
    //    private String description;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
    private MessageMetadata messageMetadata;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "ORDERLOG_UPDATETIME_IDX")
    private Date updateTimestamp = new Date();

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((OrderLog) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the customer order.
     *
     * @return the customer order
     */
    public CustomerOrder getCustomerOrder() {
        return this.customerOrder;
    }

    /**
     * Sets the customer order.
     *
     * @param customerOrder the new customer order
     */
    public void setCustomerOrder(final CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    /**
     * Gets the order status.
     *
     * @return the order status
     */
    public OrderStatus getOrderStatus() {
        return this.orderStatus;
    }

    /**
     * Sets the order status.
     *
     * @param orderStatus the new order status
     */
    public void setOrderStatus(final OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    //    public String getDescription() {
    //        return this.description;
    //    }
    //
    //    public void setDescription(String description) {
    //        this.description = description;
    //    }

    public Date getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(final Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public MessageMetadata getMessageMetadata() {
        return messageMetadata;
    }

    public void setMessageMetadata(MessageMetadata messageMetadata) {
        this.messageMetadata = messageMetadata;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public enum EventType {

        RESENT_NOTFICATION(OrderLogConstants.RESENT_NOTIFICATION), ORDER_ACKNOWLEDGEMENT(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT), ADVANCED_SHIPMENT_NOTICE(
                OrderLogConstants.ADVANCED_SHIPMENT_NOTICE), ADVANCED_SHIPMENT_NOTICE_RESEND(
                OrderLogConstants.ADVANCED_SHIPMENT_NOTICE_RESEND), DECLINED(OrderLogConstants.DECLINED), @Deprecated
        /** Use VOUCHED instead **/INVOICED(OrderLogConstants.INVOICED), VOUCHED(OrderLogConstants.VOUCHED), CUSTOMER_INVOICED(
                OrderLogConstants.CUSTOMER_INVOICED), CREDIT_CARD_RECEIPT(OrderLogConstants.CREDIT_CARD_RECEIPT), CUSTOMER_CREDIT_INVOICED(
                OrderLogConstants.CUSTOMER_CREDIT_INVOICED), CREDIT_CARD_CREDIT(OrderLogConstants.CREDIT_CARD_CREDIT), FUNCTIONAL_ACKNOWLEDGEMENT(
                OrderLogConstants.FUNCTIONAL_ACKNOWLEDGEMENT), PURCHASE_ORDER_SENT(
                OrderLogConstants.PURCHASE_ORDER_SENT), VENDOR_NOTIFICATION(OrderLogConstants.VENDOR_NOTIFICATION), CUSTOMER_NOTIFICATION(
                OrderLogConstants.CUSTOMER_NOTIFICATION), TECH_NOTIFICATION(OrderLogConstants.TECH_NOTIFICATION), CREDIT_CARD_TRANSACTION(
                OrderLogConstants.CREDIT_CARD_TRANSACTION), @Deprecated
        /** Use VOUCHER_ERRORS instead **/INVOICE_ERRORS(OrderLogConstants.INVOICE_ERRORS), VOUCHER_ERRORS(
                OrderLogConstants.VOUCHER_ERRORS), ORDER_ACKNOWLEDGEMENT_ERRORS(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_ERRORS), SHIPMENT_NOTIFICATION_ERRORS(
                OrderLogConstants.SHIPMENT_NOTIFICATION_ERRORS), ORDER_CANCELED(OrderLogConstants.ORDER_CANCELED), ORDER_DENIED(
                OrderLogConstants.ORDER_DENIED), ORDER_APPROVAL_NEEDED(OrderLogConstants.ORDER_APPROVAL_NEEDED), JUMPTRACK_STOP_NOTIFICATION(
                "Stop Notification"), JUMPTRACK_MANIFEST("Manifest"), CREDIT_CARD_DECLINED(
                OrderLogConstants.CREDIT_CARD_DECLINED), ORDER_REQUEST(OrderLogConstants.ORDER_REQUEST), PUNCHOUT_SETUP_REQUEST(
                OrderLogConstants.PUNCHOUT_SETUP_REQUEST), PUNCHOUT_ORDER_MESSAGE(
                OrderLogConstants.PUNCHOUT_ORDER_MESSAGE), BACKORDERED_ITEM_NOTICE(
                OrderLogConstants.BACKORDERED_ITEM_NOTICE), SHIPMENT_NOTICE(OrderLogConstants.SHIPMENT_NOTICE), CC_DECLINED_NOTICE(
                OrderLogConstants.CC_DECLINED_NOTICE), LATE_SHIPMENT_NOTICE(OrderLogConstants.LATE_SHIPMENT_NOTICE), DELIVERY_SIGNATURE(
                OrderLogConstants.DELIVERY_SIGNATURE), DELIVERY_DRIVER_COMMENTS(
                OrderLogConstants.DELIVERY_DRIVER_COMMENTS), DELIVERY_DRIVER_PHOTO(
                OrderLogConstants.DELIVERY_DRIVER_PHOTO), PURCHASE_ORDER_PARTNER_RESPONSE(
                OrderLogConstants.PURCHASE_ORDER_PARTNER_RESPONSE), RETURNED(OrderLogConstants.RETURNED), ORDER_FAILED(
                OrderLogConstants.ORDER_FAILED), INVOICE_PRE_ENCRYPTION(OrderLogConstants.INVOICE_PRE_ENCRYTPION), ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION), SHIPMENT_NOTICE_PRE_ENCRYPTION(
                OrderLogConstants.SHIPMENT_NOTICE_PRE_ENCRYPTION), INTERNAL_ERROR(OrderLogConstants.INTERNAL_ERROR), AP_CREDIT(
                OrderLogConstants.AP_CREDIT), REVERT_STATUS(OrderLogConstants.REVERT_STATUS), PARTNER_REPORTED_ERRORS(
                OrderLogConstants.PARTNER_REPORTED_ERRORS), APPROVE_ORDER_REMINDER(
                OrderLogConstants.APPROVE_ORDER_REMINDER), ORDER_ACKNOWLEDGEMENT_RESEND(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_RESEND), ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION_RESEND), SHIPMENT_NOTICE_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.SHIPMENT_NOTICE_PRE_ENCRYPTION_RESEND), INVOICE_PRE_ENCRYPTION_RESEND(
                OrderLogConstants.INVOICE_PRE_ENCRYPTION_RESEND), CUSTOMER_INVOICED_RESEND(
                OrderLogConstants.CUSTOMER_INVOICED_RESEND), CUSTOMER_CREDIT_INVOICED_RESEND(
                OrderLogConstants.CUSTOMER_CREDIT_INVOICED_RESEND);

        private final String label;

        private EventType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public static class OrderLogComparator implements Comparator<OrderLog> {

        @Override
        public int compare(OrderLog log0, OrderLog log1) {

            Date date0 = log0.getUpdateTimestamp();
            if (date0 == null && log0.getMessageMetadata() != null) {
                date0 = log0.getMessageMetadata().getMessageDate();
            }
            Date date1 = log1.getUpdateTimestamp();
            if (date1 == null && log1.getMessageMetadata() != null) {
                date1 = log1.getMessageMetadata().getMessageDate();
            }
            if (date0 != null && date1 != null) {
                return date0.compareTo(date1);
            }
            if (date0 != null) {
                return -1;
            }
            if (date1 != null) {
                return 1;
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}
