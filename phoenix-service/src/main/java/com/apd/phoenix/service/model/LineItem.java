package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.listener.LineItemListener;

/**
 * This entity stores the information relevant for a single line item of an order. It is associated with an Order, 
 * and might be associated with a Shipment. If the LineItem corresponds to an item in the Phoenix database, the Item 
 * field will be non-null. If the order is a manual order or placed through punchout, though, it might not correspond 
 * to an Item in the system, and that field would be null. This entity, therefore, stores all transactional data 
 * necessary to place an order, without needing a corresponding Item or CatalogXItem.
 * <p />
 * A value (possibly from Item or CatalogXItem) should be stored on this entity if one or both of the following 
 * conditions are met:
 * <ul>
 * <li>
 * the data is necessary transactional data for all line items, including items that aren't on the Phoenix database
 * </li>
 * <li>
 * when the value is read, the value returned should reflect the state of the item when the order was placed, not the 
 * (possibly different) state of the item when read takes place
 * </li>
 * </ul>
 * UnitOfMeasure, Description, and Manufacturer are data that meet the first requirement; core meets the second 
 * requirement; Cost, unitPrice and category meet both requirements; and whether the item is recyclable meets neither 
 * (which is why it's not on this entity).
 * <p />
 * Cost for Manual Orders may not be known ahead of time, and should be treated as null until the value is known.  
 * (0 implies no cost to APD, which is incorrect)
 * <p />
 * @author RHC
 *
 */
@Entity
@EntityListeners(LineItemListener.class)
public class LineItem implements Serializable, com.apd.phoenix.service.model.Entity {

    public static final String FEE_ITEM_CATEGORY = "Fee";

    private static final long serialVersionUID = -5463754245386248718L;

    @OneToMany(mappedBy = "lineItem", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.DETACH })
    @Fetch(FetchMode.SUBSELECT)
    private Set<LineItemXShipment> itemXShipments = new HashSet<LineItemXShipment>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "lineItem_id")
    private Set<LineItemLog> logs = new HashSet<LineItemLog>();

    @OneToMany(mappedBy = "lineItem", cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.DETACH })
    @Fetch(FetchMode.SUBSELECT)
    private Set<LineItemXReturn> itemXReturns = new HashSet<LineItemXReturn>();

    @ElementCollection
    @CollectionTable(name = "lineItem_vendorComments", joinColumns = @JoinColumn(name = "lineItem_id"))
    @Column(name = "vendorComment")
    private List<String> vendorComments;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private LineItem expected;

    /*
     * Used to store line numbers recieved in edi and cxml orders which may not 
     * conform to the formatt of APD assigned line numbers 
     */
    @Column
    private String customerLineNumber;

    /*
     * These three fields are values which are recieved on inbound Walmart Dept 
     * 99 orders, and are needed for the outbound 810 but are not included in the
     * catalog information for individual items.
     */
    @Column
    private String extraneousCustomerSku;

    @Column
    private String globalTradeItemNumber;

    @Column
    private String universalProductCode;

    @Column
    private String globalTradeIdentificatonNumber;

    @Column
    private Boolean returnShipping;

    /**
     * @return the vendorComments
     */
    public List<String> getVendorComments() {
        return vendorComments;
    }

    /**
     * @param vendorComments the vendorComments to set
     */
    public void setVendorComments(List<String> vendorComments) {
        this.vendorComments = vendorComments;
    }

    /**
     * @return the customerLineNumber
     */
    public String getCustomerLineNumber() {
        return customerLineNumber;
    }

    /**
     * @param customerLineNumber the customerLineNumber to set
     */
    public void setCustomerLineNumber(String customerLineNumber) {
        this.customerLineNumber = customerLineNumber;
    }

    /**
     * @return the quantityInvoicedByVendor
     */
    public BigInteger getQuantityInvoicedByVendor() {
        if (quantityInvoicedByVendor == null) {
            quantityInvoicedByVendor = BigInteger.ZERO;
        }
        return quantityInvoicedByVendor;
    }

    /**
     * @param quantityInvoicedByVendor the quantityInvoicedByVendor to set
     */
    public void setQuantityInvoicedByVendor(BigInteger quantityInvoicedByVendor) {
        this.quantityInvoicedByVendor = quantityInvoicedByVendor;
    }

    /**
     * @return the extraneousCustomerSku
     */
    public String getExtraneousCustomerSku() {
        return extraneousCustomerSku;
    }

    /**
     * @param extraneousCustomerSku the extraneousCustomerSku to set
     */
    public void setExtraneousCustomerSku(String extraneousCustomerSku) {
        this.extraneousCustomerSku = extraneousCustomerSku;
    }

    /**
     * @return the globalTradeItemNumber
     */
    public String getGlobalTradeItemNumber() {
        return globalTradeItemNumber;
    }

    /**
     * @param globalTradeItemNumber the globalTradeItemNumber to set
     */
    public void setGlobalTradeItemNumber(String globalTradeItemNumber) {
        this.globalTradeItemNumber = globalTradeItemNumber;
    }

    /**
     * @return the universalProductCode
     */
    public String getUniversalProductCode() {
        return universalProductCode;
    }

    /**
     * @param universalProductCode the universalProductCode to set
     */
    public void setUniversalProductCode(String universalProductCode) {
        this.universalProductCode = universalProductCode;
    }

    /**
     * This enum lists the different item types in the cXML specification.
     * 
     * @author RHC
     *
     */
    public enum ItemType {
        COMPOSITE("composite"), ITEM("item");

        private final String label;

        private ItemType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @OneToMany(mappedBy = "lineItem", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<LineItemHistory> lineItemHistories = new HashSet<LineItemHistory>();

    @OneToMany(mappedBy = "lineItem", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.REFRESH, CascadeType.DETACH })
    @Fetch(FetchMode.SUBSELECT)
    private Set<CanceledQuantity> canceledQuantities = new HashSet<CanceledQuantity>();

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    private LineItemStatus status;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    private LineItemStatus paymentStatus;

    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "LI_ITEM_IX")
    private Item item;

    @ManyToOne(fetch = FetchType.EAGER)
    private ItemCategory category;

    /**
     * equivalent to Vendor SKU
     */
    @Column
    private String supplierPartId;

    /**
     * Maps to NEW_ITEM-MANUFACTMAT in OCI
     */
    @Column
    private String manufacturerPartId;

    @Column
    private String apdSku;

    @ManyToOne(fetch = FetchType.EAGER)
    private Vendor vendor;

    @Column
    private String supplierPartAuxiliaryId;

    //TODO: replace with BigInteger
    @Column(nullable = false)
    private Integer quantity = 0;

    @Column
    @Index(name = "LINEITEM_LINENUMBER_IDX")
    private Integer lineNumber = null;

    @Column
    private Integer parentLineNumber = null;

    @Column(nullable = true)
    private BigDecimal cost = null;

    @Column(nullable = false)
    private BigDecimal unitPrice = BigDecimal.ZERO;

    @Column(nullable = false)
    private BigDecimal estimatedShippingAmount = BigDecimal.ZERO;

    @Column
    private BigInteger backorderedQuantity;

    @Column
    private Boolean taxable;

    @Column
    private String shortName;

    @Column(nullable = false, length = 4000)
    private String description;

    @Column
    private Boolean core = false;

    @Column
    private Integer originalQuantity;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private UnitOfMeasure unitOfMeasure;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "customerExpectedUoM_id")
    private UnitOfMeasure customerExpectedUnitOfMeasure;

    /**
     * Maps to  <Classification domain="UNSPSC"> in cXML and to NEW_ITEM-MATGROUP in OCI 
     */
    @Column
    private String unspscClassification;

    /**
     * Maps to NEW_ITEM-MANUFACTCODE in OCI
     */
    @Column
    private String manufacturerName;

    @Column
    private String url;

    @Column
    private Integer leadTime = null;

    @ManyToOne(fetch = FetchType.EAGER)
    @Index(name = "LINEITEM_CUSTORDER_IDX")
    private CustomerOrder order;

    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    //TODO: replace with a flat text field
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Sku customerSku;

    @Column(nullable = true)
    private BigDecimal maximumTaxToCharge;

    @Column
    private String purchaseComment;

    @Column
    private String originallyAddedSku;

    @Column
    private String originallyAddedName;

    @Column(length = 4000)
    private String shipToExtrasArtifact;

    @Column
    private BigInteger quantityInvoicedByVendor;

    @Column
    private BigDecimal customerExpectedUnitPrice;

    //indicates whether the item has ever been rejected by the vendor
    @Column
    private Boolean hasBeenRejected = false;

    @Transient
    private boolean recalculateLineNumberOnPersist = true;

    public LineItemLog findLogForCurrentStatus() {
        if (this.status == null) {
            return null;
        }
        List<LineItemLog> logList = new ArrayList<LineItemLog>();
        logList.addAll(logs);
        //Sort by time to get the most recent order log first
        Collections.sort(logList, getSortByTimeComarator());
        for (LineItemLog lineItemLog : logList) {
            if (lineItemLog.getLineItemStatus() != null
                    && lineItemLog.getLineItemStatus().getValue().equals(this.status.getValue())) {
                return lineItemLog;
            }
        }
        return null;
    }

    private Comparator<LineItemLog> getSortByTimeComarator() {
        return new Comparator<LineItemLog>() {

            @Override
            public int compare(LineItemLog arg0, LineItemLog arg1) {
                return arg1.getUpdateTimestamp().compareTo(arg0.getUpdateTimestamp());
            }

        };
    }

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((LineItem) that).id);
        }
        return super.equals(that);
    }

    public boolean matchesItemOnOrder(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (that instanceof LineItem) {
            if (id != null) {
                return id.equals(((LineItem) that).getId());
            }
            else {
                return super.equals(that);
            }
        }
        if (that instanceof LineItemDto) {
            LineItemDto dtoThat = (LineItemDto) that;
            boolean skuMatches = (this.apdSku != null && this.apdSku.equals(dtoThat.getApdSku()))
                    || (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getSupplierPartId()))
                    || (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getBuyerPartNumber()))
                    || (this.supplierPartId != null && this.supplierPartId.equals(dtoThat.getSupplierPartAuxiliaryId()))
                    || (this.supplierPartAuxiliaryId != null && this.supplierPartAuxiliaryId.equals(dtoThat
                            .getBuyerPartNumber()))
                    || (this.supplierPartAuxiliaryId != null && this.supplierPartAuxiliaryId.equals(dtoThat
                            .getSupplierPartId()))
                    || (this.customerSku != null && this.customerSku.getValue() != null && this.customerSku.getValue()
                            .equals(dtoThat.getBuyerPartNumber()))
                    || (this.customerSku != null && this.customerSku.getValue() != null
                            && dtoThat.getCustomerSku() != null && this.customerSku.getValue().equals(
                            dtoThat.getCustomerSku().getValue()))
                    || (this.manufacturerPartId != null && this.manufacturerPartId.equals(dtoThat
                            .getManufacturerPartId()));
            return skuMatches
                    && (dtoThat.getLineNumber() == null || this.getLineNumber() == null || dtoThat.getLineNumber()
                            .equals(this.getLineNumber()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public Item getItem() {
        return this.item;
    }

    public void setItem(final Item item) {
        this.item = item;
    }

    public String getSupplierPartAuxiliaryId() {
        return this.supplierPartAuxiliaryId;
    }

    public void setSupplierPartAuxiliaryId(final String supplierPartAuxiliaryId) {
        this.supplierPartAuxiliaryId = supplierPartAuxiliaryId;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantityShipped() {

        Integer totalShipped = 0;
        for (LineItemXShipment lineItemXShipment : this.getItemXShipments()) {
            totalShipped = lineItemXShipment.getQuantity().intValue() + totalShipped;
        }

        return totalShipped;
    }

    public Integer getQuantityCancelled() {
        Integer totalCancelled = 0;
        for (CanceledQuantity quantity : this.getCanceledQuantities()) {
            totalCancelled = quantity.getQuantity().intValue() + totalCancelled;
        }
        return totalCancelled;
    }

    public Integer getQuantityReturned() {
        Integer totalReturned = 0;
        for (LineItemXReturn lixr : this.getItemXReturns()) {
            for (ReturnOrder returnOrder : this.getOrder().getReturnOrders()) {
                if (!returnOrder.getStatus().equals(ReturnOrderStatus.CLOSED) && returnOrder.getItems().contains(lixr)) {
                    totalReturned = lixr.getQuantity().intValue() + totalReturned;
                }
            }
        }
        return totalReturned;
    }

    public Integer getQuantityRemaining() {
        return this.getQuantity() - this.getQuantityShipped() - this.getQuantityCancelled();
    }

    public Integer getLineNumber() {
        return this.lineNumber;
    }

    public void setLineNumber(final Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Integer getParentLineNumber() {
        return this.parentLineNumber;
    }

    public void setParentLineNumber(final Integer parentLineNumber) {
        this.parentLineNumber = parentLineNumber;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getUnitPrice() {
        return this.unitPrice;
    }

    public void setUnitPrice(final BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(final String shortName) {
        this.shortName = shortName;
    }

    public String getDescription() {
        return this.description;
    }

    public String getDescriptionLength40() {
        if (this.description != null) {
            if (this.description.length() > 40) {
                return this.description.substring(0, 40);
            }
            else {
                return this.description;
            }
        }
        return "";
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return this.unitOfMeasure;
    }

    public void setUnitOfMeasure(final UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public UnitOfMeasure getCustomerExpectedUnitOfMeasure() {
        return customerExpectedUnitOfMeasure;
    }

    public void setCustomerExpectedUnitOfMeasure(UnitOfMeasure customerExpectedUnitOfMeasure) {
        this.customerExpectedUnitOfMeasure = customerExpectedUnitOfMeasure;
    }

    public String getUnspscClassification() {
        return this.unspscClassification;
    }

    public void setUnspscClassification(final String unspscClassification) {
        this.unspscClassification = unspscClassification;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Integer getLeadTime() {
        return this.leadTime;
    }

    public void setLeadTime(final Integer leadTime) {
        this.leadTime = leadTime;
    }

    public ItemType getItemType() {
        return this.itemType;
    }

    public void setItemType(final ItemType itemType) {
        this.itemType = itemType;
    }

    public CustomerOrder getOrder() {
        return this.order;
    }

    public void setOrder(final CustomerOrder order) {
        this.order = order;
    }

    public Vendor getVendor() {
        return this.vendor;
    }

    public void setVendor(final Vendor vendor) {
        this.vendor = vendor;
    }

    public BigDecimal getEstimatedShippingAmount() {
        return this.estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(final BigDecimal estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public ItemCategory getCategory() {
        return this.category;
    }

    public void setCategory(final ItemCategory category) {
        this.category = category;
    }

    public boolean isCore() {
        if (this.core != null) {
            return this.core;
        }
        else {
            return false;
        }
    }

    public Boolean getCore() {
        return this.core;
    }

    public void setCore(final Boolean core) {
        this.core = core;
    }

    public String getSupplierPartId() {
        return supplierPartId;
    }

    public void setSupplierPartId(String supplierPartId) {
        this.supplierPartId = supplierPartId;
    }

    public String getManufacturerPartId() {
        return manufacturerPartId;
    }

    public void setManufacturerPartId(String manufacturerPartId) {
        this.manufacturerPartId = manufacturerPartId;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public Boolean isTaxable() {
        return taxable;
    }

    public void setTaxable(Boolean taxable) {
        this.taxable = taxable;
    }

    public Boolean getTaxable() {
        return taxable;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (description != null && !description.trim().isEmpty())
            result += ", description: " + description;
        return result;
    }

    /**
     * This is a utility method, to return the total price of this line item on this order.
     * 
     * @return
     */
    public BigDecimal getTotalPrice() {
        if (this.getUnitPrice() == null) {
            return BigDecimal.ZERO;
        }
        return this.getUnitPrice().multiply(BigDecimal.valueOf(this.getQuantity()));
    }

    public String getVendorMat() {
        String vendorMat = this.getSupplierPartId();
        if (StringUtils.isNotBlank(this.getSupplierPartAuxiliaryId())) {
            vendorMat += "|" + this.getSupplierPartAuxiliaryId();
            vendorMat += "|" + this.getOrder().getApdPo().getValue();
        }
        return vendorMat;
    }

    public Set<LineItemXShipment> getItemXShipments() {
        return this.itemXShipments;
    }

    public void setItemXShipments(final Set<LineItemXShipment> itemXShipments) {
        this.itemXShipments = itemXShipments;
    }

    public Set<LineItemLog> getLogs() {
        return logs;
    }

    public void setLogs(Set<LineItemLog> logs) {
        this.logs = logs;
    }

    public Set<LineItemXReturn> getItemXReturns() {
        return this.itemXReturns;
    }

    public void setItemXReturns(final Set<LineItemXReturn> itemXReturns) {
        this.itemXReturns = itemXReturns;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getPurchaseComment() {
        return purchaseComment;
    }

    public void setPurchaseComment(String purchaseComment) {
        this.purchaseComment = purchaseComment;
    }

    public Set<LineItemHistory> getLineItemHistories() {
        return lineItemHistories;
    }

    public void setLineItemHistories(Set<LineItemHistory> lineItemHistories) {
        this.lineItemHistories = lineItemHistories;
    }

    public LineItemStatus getStatus() {
        return status;
    }

    public void setStatus(LineItemStatus status) {
        this.status = status;
    }

    public LineItemStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(LineItemStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Sku getCustomerSku() {
        return customerSku;
    }

    /**
     * Note: when setting an existing SKU to the customer sku, use lineItem.setCustomerSku(skuBp.copyToNewCustomerSku(originalSku))
     * 
     * @param customerSku
     */
    public void setCustomerSku(Sku customerSku) {
        this.customerSku = customerSku;
    }

    public String getOriginallyAddedSku() {
        return originallyAddedSku;
    }

    public void setOriginallyAddedSku(String originallyAddedSku) {
        this.originallyAddedSku = originallyAddedSku;
    }

    public String getOriginallyAddedName() {
        return originallyAddedName;
    }

    public void setOriginallyAddedName(String originallyAddedName) {
        this.originallyAddedName = originallyAddedName;
    }

    public LineItem getExpected() {
        return expected;
    }

    public void setExpected(LineItem expected) {
        this.expected = expected;
    }

    public BigDecimal getMaximumTaxToCharge() {
        return maximumTaxToCharge;
    }

    public void setMaximumTaxToCharge(BigDecimal maximumTaxToCharge) {
        this.maximumTaxToCharge = maximumTaxToCharge;
    }

    /**
     * Helper method, returns the total amount of tax that has been paid for this line item so far.
     * 
     * @return
     */
    public BigDecimal getPaidTax() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipment shipment : this.itemXShipments) {
            for (LineItemXShipmentXTaxType tax : shipment.getTaxes()) {
                toReturn = toReturn.add(tax.getValue());
            }
        }
        return toReturn;
    }

    /**
     * Helper method, returns the total amount of tax for the item, estimated amount 
     * for unpaid items and paid amount for paid items.
     * 
     * @return
     */
    @Deprecated
    public BigDecimal getTotalTax() {
        return this.maximumTaxToCharge;
    }

    /**
     * Helper method, returns the total amount of shipping that has been paid for this line item so far.
     * 
     * @return
     */
    public BigDecimal getPaidShipping() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipment shipment : this.itemXShipments) {
            if (shipment.getShipping() != null) {
                toReturn = toReturn.add(shipment.getShipping());
            }
        }
        toReturn = toReturn.subtract(calculateTotalReturnShipping());
        return toReturn;
    }

    public BigDecimal calculateTotalReturnShipping() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXReturn returnItem : this.itemXReturns) {
            if (returnItem.getReturnShipping() && returnItem.getReturnShippingAmount() != null) {
                toReturn = toReturn.add(returnItem.getReturnShippingAmount());
            }
        }
        return toReturn;
    }

    public boolean isFee() {
        return FEE_ITEM_CATEGORY.equals(this.manufacturerName)
                || (category != null && FEE_ITEM_CATEGORY.equals(category.getName()));
    }

    /**
     * Returns true if this item was added to the cart as a substitute.
     * 
     * @return
     */
    public boolean isSubstituteForOriginal() {
        return (this.originallyAddedName != null || this.originallyAddedSku != null);
    }

    public BigInteger getBackorderedQuantity() {
        return backorderedQuantity;
    }

    public void setBackorderedQuantity(BigInteger backorderedQuantity) {
        this.backorderedQuantity = backorderedQuantity;
    }

    public String getShipToExtrasArtifact() {
        return shipToExtrasArtifact;
    }

    public void setShipToExtrasArtifact(String shipToExtrasArtifact) {
        this.shipToExtrasArtifact = shipToExtrasArtifact;
    }

    public Set<CanceledQuantity> getCanceledQuantities() {
        return canceledQuantities;
    }

    public void setCanceledQuantities(Set<CanceledQuantity> canceledQuantities) {
        this.canceledQuantities = canceledQuantities;
    }

    public Integer getOriginalQuantity() {
        return originalQuantity;
    }

    public void setOriginalQuantity(Integer originalQuantity) {
        this.originalQuantity = originalQuantity;
    }

    public BigDecimal getCustomerExpectedUnitPrice() {
        return customerExpectedUnitPrice;
    }

    public void setCustomerExpectedUnitPrice(BigDecimal customerExpectedUnitPrice) {
        this.customerExpectedUnitPrice = customerExpectedUnitPrice;
    }

    public Boolean getReturnShipping() {
        if (returnShipping == null) {
            return Boolean.FALSE;
        }
        return returnShipping;
    }

    public void setReturnShipping(Boolean returnShipping) {
        this.returnShipping = returnShipping;
    }

    public Boolean getHasBeenRejected() {
        return hasBeenRejected;
    }

    public void setHasBeenRejected(Boolean hasBeenRejected) {
        this.hasBeenRejected = hasBeenRejected;
    }

    public boolean isRecalculateLineNumberOnPersist() {
        return recalculateLineNumberOnPersist;
    }

    public void setRecalculateLineNumberOnPersist(boolean recalculateLineNumberOnPersist) {
        this.recalculateLineNumberOnPersist = recalculateLineNumberOnPersist;
    }

    public String getGlobalTradeIdentificatonNumber() {
        return globalTradeIdentificatonNumber;
    }

    public void setGlobalTradeIdentificatonNumber(String globalTradeIdentificatonNumber) {
        this.globalTradeIdentificatonNumber = globalTradeIdentificatonNumber;
    }

    public static class LineItemComparator implements Comparator<LineItem> {

        @Override
        public int compare(LineItem item0, LineItem item1) {
            Integer item = nullCompare(item0, item1);
            if (item == null) {
                if (item0.isFee() != item1.isFee()) {
                    if (item0.isFee()) {
                        return 1;
                    }
                    else {
                        return -1;
                    }
                }
                Integer number = nullCompare(item0.getLineNumber(), item1.getLineNumber());
                if (number == null) {
                    return item0.getLineNumber() - item1.getLineNumber();
                }
                else {
                    return number;
                }
            }
            else {
                return item;
            }
        }

        //This method will return !null value if it can determine the ordering of the objects
        private Integer nullCompare(Object o1, Object o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            else if (o1 == null) {
                return -1;
            }
            else if (o2 == null) {
                return 1;
            }
            else {
                return null;
            }
        }
    }

}
