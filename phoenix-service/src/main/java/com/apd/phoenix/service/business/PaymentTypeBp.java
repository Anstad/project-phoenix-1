/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.persistence.jpa.LineItemStatusDao;
import com.apd.phoenix.service.persistence.jpa.PaymentTypeDao;

/**
 *
 * @author RHC
 */
@Stateless
@LocalBean
public class PaymentTypeBp extends AbstractBp<PaymentType> {

    @Inject
    PaymentTypeDao paymentTypeDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PaymentTypeDao dao) {
        this.dao = dao;
    }

    public PaymentType getTypeByValue(String value) {
        List<PaymentType> resultList = paymentTypeDao.getPaymentTypeByValue(value);
        if (resultList != null && resultList.size() > 0) {
            return resultList.get(0);
        }
        else {
            return null;
        }
    }

    public List<PaymentType> getPaymentTypeList() {
        return paymentTypeDao.getPaymentTypeList();
    }
}
