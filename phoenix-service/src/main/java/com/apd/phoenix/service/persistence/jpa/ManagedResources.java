package com.apd.phoenix.service.persistence.jpa;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * This class uses CDI to alias Java EE resources, such as the persistence context, to CDI beans
 * 
 * <p>
 * Example injection on a managed bean field:
 * </p>
 * 
 * <pre>
 * &#064;Inject
 * private EntityManager em;
 * </pre>
 */

public class ManagedResources {

    @SuppressWarnings("unused")
    @Produces
    @PersistenceContext(unitName = "Phoenix")
    private EntityManager em;

    //    @Produces
    //    @PersistenceContext(unitName = "jbpm")
    //    @JbpmEm
    //    private EntityManager jbpmEm;

    //        @SuppressWarnings("static-method")
    //        @Produces
    //        public Logger produceLog(InjectionPoint injectionPoint) {
    //            return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass().getName());
    //        }

    //    @Produces
    //    @RequestScoped
    //    public FacesContext produceFacesContext() {
    //        return FacesContext.getCurrentInstance();
    //    }

}
