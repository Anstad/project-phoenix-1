package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.SystemUser;

/**
 * Bulletin DAO 
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class BulletinDao extends AbstractDao<Bulletin> {

    /**
     * This method returns the set of bulletins that are associated with the given account, credential, and user, 
     * that have not yet expired.
     * @param axcxu 
     * @return the list of bulletins
     */
    @SuppressWarnings("unchecked")
    public List<Bulletin> getBulletinsToDisplay(AccountXCredentialXUser axcxu) {
        String hql = "SELECT bulletin FROM AccountXCredentialXUser AS axcxu, Bulletin AS bulletin "
                + " WHERE (bulletin IN elements(axcxu.account.bulletins) OR bulletin IN elements(axcxu.credential.bulletins) "
                + " OR bulletin IN elements(axcxu.user.bulletins)) AND (bulletin.expirationDate IS NULL OR "
                + " bulletin.expirationDate > :currentDate) AND axcxu.id = :axcxuId AND not bulletin.csr = true";
        Query query = entityManager.createQuery(hql);
        query.setParameter("axcxuId", axcxu.getId());
        query.setParameter("currentDate", new Date(), TemporalType.TIMESTAMP);
        return addGlobalBulletins(query.getResultList());
    }

    /**
     * This method returns the set of bulletins that are associated with the given account, credential, and user, 
     * that have not yet expired.
     * 
     * @param account
     * @param credential
     * @param user
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Bulletin> getBulletinsToDisplay(SystemUser user) {
        String hql = "SELECT bulletin FROM SystemUser AS user JOIN user.bulletins AS bulletin "
                + " WHERE (bulletin.expirationDate IS NULL OR bulletin.expirationDate > :currentDate) "
                + " AND user.id = :userId AND not bulletin.csr = true";
        Query query = entityManager.createQuery(hql);
        query.setParameter("userId", user.getId());
        query.setParameter("currentDate", new Date(), TemporalType.TIMESTAMP);
        return addGlobalBulletins(query.getResultList());
    }

    /**
     * This method returns the set of bulletins that are associated with the given account, credential, and user, 
     * that have not yet expired.
     * 
     * @param account
     * @param credential
     * @param user
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Bulletin> getGlobalBulletins() {
        String hql = "SELECT bulletin FROM Bulletin AS bulletin WHERE (bulletin.expirationDate IS NULL OR"
                + " bulletin.expirationDate > :currentDate) AND bulletin.global = true AND not bulletin.csr = true";
        Query query = entityManager.createQuery(hql);
        query.setParameter("currentDate", new Date(), TemporalType.TIMESTAMP);
        return query.getResultList();
    }

    private List<Bulletin> addGlobalBulletins(List<Bulletin> bulletinList) {
        bulletinList.addAll(this.getGlobalBulletins());
        return bulletinList;
    }

    public List<Bulletin> getCSRBulletins() {
        String hql = "SELECT bulletin FROM Bulletin AS bulletin WHERE (bulletin.expirationDate IS NULL OR"
                + " bulletin.expirationDate > :currentDate) AND bulletin.csr = true";
        Query query = entityManager.createQuery(hql);
        query.setParameter("currentDate", new Date(), TemporalType.TIMESTAMP);
        return query.getResultList();
    }
}
