package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class SkuDto implements Serializable {

    private static final long serialVersionUID = 1869662656405428574L;

    private SkuTypeDto type;

    private String value;

    public SkuTypeDto getType() {
        return type;
    }

    public void setType(SkuTypeDto type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}