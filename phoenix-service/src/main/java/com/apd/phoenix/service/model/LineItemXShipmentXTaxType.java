package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import org.hibernate.annotations.Check;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

/**
 * This entity stores the value of a Tax for a LineItemXShipment or LineItemXReturn. 
 * This entity has a many-to-one relationship with LineItemXShipment and a many-to-one relationship with 
 * TaxType.
 * 
 * @author RHC
 *
 */
@Entity
@Table(name = "LineItemXShipment_TaxType")
@Check(constraints = "LINEITEMXSHIPMENT_ID IS NOT NULL OR LINEITEMXRETURN_ID IS NOT NULL AND NOT (LINEITEMXSHIPMENT_ID IS NOT NULL AND LINEITEMXRETURN_ID IS NOT NULL)")
public class LineItemXShipmentXTaxType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6192783371178843325L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private TaxType type;

    @Column(nullable = false)
    private BigDecimal value = BigDecimal.ZERO;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @Index(name = "LIXSXTT_LINEITEMXSHIPMENT_IDX")
    private LineItemXShipment lineItemXShipment;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @Index(name = "LIXSXTT_LINEITEMXRETURN_IDX")
    private LineItemXReturn lineItemXReturn;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((LineItemXShipmentXTaxType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public TaxType getType() {
        return this.type;
    }

    public void setType(final TaxType type) {
        this.type = type;
    }

    public BigDecimal getValue() {
        return this.value;
    }

    public void setValue(final BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null)
            result += "value: " + value;
        return result;
    }

    public LineItemXShipment getLineItemXShipment() {
        return lineItemXShipment;
    }

    public void setLineItemXShipment(LineItemXShipment lineItemXShipment) {
        this.lineItemXShipment = lineItemXShipment;
    }

    public LineItemXReturn getLineItemXReturn() {
        return lineItemXReturn;
    }

    public void setLineItemXReturn(LineItemXReturn lineItemXReturn) {
        this.lineItemXReturn = lineItemXReturn;
    }

}