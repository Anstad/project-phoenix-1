package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * This field is used to hold the different tax rates, for a certain location
 * 
 * @author RHC
 *
 */
@Entity
public class MilitaryZip implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -4602503734145720219L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String zip;

    @Column
    private String city;

    @Column
    private String county;

    @Column
    private String state;

    @Column
    private String uspsCity;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((MilitaryZip) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getCounty() {
        return this.county;
    }

    public void setCounty(final String county) {
        this.county = county;
    }

    public String getState() {
        return this.state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getUspsCity() {
        return uspsCity;
    }

    public void setUspsCity(String uspsCity) {
        this.uspsCity = uspsCity;
    }

    public static class MilitaryZipComparator implements Comparator<MilitaryZip> {

        @Override
        public int compare(MilitaryZip zip0, MilitaryZip zip1) {

            if (!StringUtils.isBlank(zip0.getZip()) && !StringUtils.isBlank(zip1.getZip())) {
                return zip0.getZip().compareTo(zip1.getZip());
            }
            return (new GenericComparator()).compare(zip0, zip1);
        }
    }
}