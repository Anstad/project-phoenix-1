package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;

@Stateless
@LocalBean
public class LineItemXShipmentXTaxTypeDao extends AbstractDao<LineItemXShipmentXTaxType> {
}
