package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import com.apd.phoenix.service.message.api.MessageConstants;

@Entity
@XmlRootElement
public class MessageMetadata implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -5436212761361922834L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The communication method. */

    /** The date of the message */
    @Temporal(TemporalType.DATE)
    private Date messageDate;

    @Column
    private String filePath;

    @Column
    private String contentType;

    @Column(nullable = false)
    private long contentLength;

    @Enumerated(EnumType.STRING)
    private MessageType messageType;

    @Enumerated(EnumType.STRING)
    private CommunicationType communicationType;

    @Column(length = 2000)
    private String destination;

    @Column(length = 2000)
    private String ccRecipients;

    @Column(length = 2000)
    private String bccRecipients;

    @Column
    private String transactionId;

    @Column
    private String senderId;

    @Column
    private String receiverId;

    @Column
    /**
     * Unique identifier for the message/document/file exchange (APD messageId/exchangeId = EDI interchangeId = CXML payloadId = XCBL envelopeId) 
     * TODO rename this to messageId or exchangeId
     */
    private String interchangeId;

    @Column
    private String groupId;

    @Transient
    private boolean isContentUrl = false;

    @Transient
    private URL presignedUrl;

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH })
    private MessageObjectsWrapper objectsWrapper;

    @Column
    private String attachmentName;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    /**
     * @return the messageDate
     */
    public Date getMessageDate() {
        return messageDate;
    }

    /**
     * @param messageDate
     *            the messageDate to set
     */
    public void setMessageDate(final Date messageDate) {
        this.messageDate = messageDate;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public enum MessageType {
        EMAIL(MessageConstants.EMAIL), EDI(MessageConstants.EDI), CXML(MessageConstants.CXML), XCBL(
                MessageConstants.XCBL), OCI(MessageConstants.OCI), CSV(MessageConstants.CSV), XML(MessageConstants.XML), IMAGE(
                MessageConstants.IMAGE), TEXT(MessageConstants.TEXT);

        private final String label;

        private MessageType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum PunchoutType {

        CXML(MessageConstants.CXML), OCI(MessageConstants.OCI);

        private final String label;

        private PunchoutType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum CommunicationType {
        INBOUND(MessageConstants.INBOUND), OUTBOUND(MessageConstants.OUTBOUND);

        private final String label;

        private CommunicationType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((MessageMetadata) that).id);
        }
        return super.equals(that);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCcRecipients() {
        return ccRecipients;
    }

    public void setCcRecipients(String ccRecipients) {
        this.ccRecipients = ccRecipients;
    }

    public String getBccRecipients() {
        return bccRecipients;
    }

    public void setBccRecipients(String bccRecipients) {
        this.bccRecipients = bccRecipients;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(CommunicationType communicationType) {
        this.communicationType = communicationType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    /**
     * Gets the unique identifier for the message/document/file exchange (APD messageId/exchangeId = EDI interchangeId = CXML payloadId = XCBL envelopeId) 
     * TODO rename this to getMessageId or getExchangeId
     */
    public String getInterchangeId() {
        return interchangeId;
    }

    /**
     * Set the unique identifier for the message/document/file exchange (APD messageId/exchangeId = EDI interchangeId = CXML payloadId = XCBL envelopeId) 
     * TODO rename this to setMessageId or setExchangeId
     */
    public void setInterchangeId(String interchangeId) {
        this.interchangeId = interchangeId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean isContentUrl() {
        return isContentUrl;
    }

    public void setContentUrl(boolean isContentUrl) {
        this.isContentUrl = isContentUrl;
    }

    public URL getPresignedUrl() {
        return presignedUrl;
    }

    public void setPresignedUrl(URL presignedUrl) {
        this.presignedUrl = presignedUrl;
    }

    public MessageObjectsWrapper getObjectsWrapper() {
        return objectsWrapper;
    }

    public void setObjectsWrapper(MessageObjectsWrapper objectsWrapper) {
        this.objectsWrapper = objectsWrapper;
    }

    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

}
