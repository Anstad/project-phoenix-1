/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.List;
import com.apd.phoenix.service.model.ContactLog.TicketStatus;

public class ServiceLogSearchCriteria {

    private String ticketNumber;
    private String accountName;
    private String exactAccountName;
    private String department;
    private String contactName;
    private String contactNumber;
    private String contactEmail;
    private String apdRep;
    private Date startDate;
    private Date endDate;
    private String poNumber;
    private TicketStatus ticketStatus;
    private Integer start;
    private Integer pageSize;
    private List<Long> accountWhiteList;

    public ServiceLogSearchCriteria(String ticketNumber, String accountName, String exactAccountName,
            String department, String contactName, String contactNumber, String contactEmail, String apdRep,
            Date startDate, Date endDate, TicketStatus ticketStatus, String poNumber, Integer inStart,
            Integer inPageSize, List<Long> accountWhiteList) {
        this.ticketNumber = ticketNumber;
        this.accountName = accountName;
        this.setExactAccountName(exactAccountName);
        this.department = department;
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactEmail = contactEmail;
        this.apdRep = apdRep;
        this.startDate = startDate;
        this.endDate = endDate;
        this.poNumber = poNumber;
        this.ticketStatus = ticketStatus;
        this.start = inStart;
        this.pageSize = inPageSize;
        this.accountWhiteList = accountWhiteList;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getExactAccountName() {
        return exactAccountName;
    }

    public void setExactAccountName(String exactAccountName) {
        this.exactAccountName = exactAccountName;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getApdRep() {
        return apdRep;
    }

    public void setApdRep(String apdRep) {
        this.apdRep = apdRep;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public TicketStatus getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(TicketStatus ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public List<Long> getAccountWhiteList() {
        return accountWhiteList;
    }

    public void setAccountWhiteList(List<Long> accountWhiteList) {
        this.accountWhiteList = accountWhiteList;
    }

}
