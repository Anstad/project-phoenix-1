package com.apd.phoenix.service.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "LINEITEM_LOG")
public class LineItemLog extends OrderLog {

    /**
     * 
     */
    private static final long serialVersionUID = 7146387732022032666L;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    private LineItemStatus lineItemStatus;

    @Column
    private String note;

    public LineItemStatus getLineItemStatus() {
        return lineItemStatus;
    }

    public void setLineItemStatus(LineItemStatus lineItemStatus) {
        this.lineItemStatus = lineItemStatus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
