package com.apd.phoenix.service.storage.api;

public interface StorageService {

    public boolean createObject(StorageObject object);

    public StorageObject retrieveObject(StorageObjectRequest request);

    public boolean updateObject(StorageObject object);

    public boolean deleteObject(StorageObjectRequest request);

}
