package com.apd.phoenix.service.payment.ws.client;

import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.payment.ws.AdditionalAmounts;
import com.apd.phoenix.service.payment.ws.Address;
import com.apd.phoenix.service.payment.ws.ArrayOfLineItemDetail;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author dnorris
 */
public class PaymentGatewayTransaction {

    private CreditCardTransaction creditCardTransaction;
    private StoredCardIdentifier storedCardIdentifier;
    private String storedCardToken;
    private String nameOnCard;
    private String cardIdentifier;
    private ArrayOfLineItemDetail arrayOfLineItemDetail;
    private AdditionalAmounts additionalAmounts;
    private Address billingAddress;
    private Address shippingAddress;
    private String shipFromZip;
    private BigDecimal totalAmount;
    private BigDecimal salesTaxAmount;
    private BigDecimal subTotalAmount;
    private BigDecimal totalFreightAmount;
    private String apdPoNumber;
    private String customerPoNumber;
    private CustomerOrder customerOrder;
    private String trackingNumber;

    PaymentGatewayTransaction() {
        totalAmount = new BigDecimal(0);
        salesTaxAmount = new BigDecimal(0);
        subTotalAmount = new BigDecimal(0);
        totalFreightAmount = new BigDecimal(0);
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public void setBillingAddress(com.apd.phoenix.service.model.Address billingAddress) {
        this.billingAddress = PaymentGatewayObjectFactory.createAddress(billingAddress);
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public void setShippingAddress(com.apd.phoenix.service.model.Address shippingAddress) {
        this.shippingAddress = PaymentGatewayObjectFactory.createAddress(shippingAddress);
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        totalAmount = totalAmount.setScale(2, RoundingMode.CEILING);
        this.totalAmount = totalAmount;
    }

    public BigDecimal getSalesTaxAmount() {
        return salesTaxAmount;
    }

    public void setSalesTaxAmount(BigDecimal salesTaxAmount) {
        salesTaxAmount = salesTaxAmount.setScale(2, RoundingMode.CEILING);
        this.salesTaxAmount = salesTaxAmount;
    }

    public BigDecimal getSubTotalAmount() {
        return subTotalAmount;
    }

    public void setSubTotalAmount(BigDecimal subTotalAmount) {
        subTotalAmount = subTotalAmount.setScale(2, RoundingMode.CEILING);
        this.subTotalAmount = subTotalAmount;
    }

    public CreditCardTransaction getCreditCardTransaction() {
        return creditCardTransaction;
    }

    public void setCreditCardTransaction(CreditCardTransaction creditCardTransaction) {
        this.creditCardTransaction = creditCardTransaction;
    }

    public String getStoredCardToken() {
        return storedCardToken;
    }

    public void setStoredCardToken(String storedCardToken) {
        this.storedCardToken = storedCardToken;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getCardIdentifier() {
        return cardIdentifier;
    }

    public void setCardIdentifier(String cardIdentifier) {
        this.cardIdentifier = cardIdentifier;
    }

    public StoredCardIdentifier getStoredCardIdentifier() {
        return storedCardIdentifier;
    }

    public void setStoredCardIdentifier(StoredCardIdentifier storedCardIdentifier) {
        this.storedCardIdentifier = storedCardIdentifier;
    }

    public ArrayOfLineItemDetail getArrayOfLineItemDetail() {
        return arrayOfLineItemDetail;
    }

    public void setArrayOfLineItemDetail(ArrayOfLineItemDetail arrayOfLineItemDetail) {
        this.arrayOfLineItemDetail = arrayOfLineItemDetail;
    }

    public AdditionalAmounts getAdditionalAmounts() {
        return additionalAmounts;
    }

    public void setAdditionalAmounts(AdditionalAmounts additionalAmounts) {
        this.additionalAmounts = additionalAmounts;
    }

    public BigDecimal getTotalFreightAmount() {
        return totalFreightAmount;
    }

    public void setTotalFreightAmount(BigDecimal totalFreightAmount) {
        totalFreightAmount = totalFreightAmount.setScale(2, RoundingMode.CEILING);
        this.totalFreightAmount = totalFreightAmount;
    }

    public String getApdPoNumber() {
        return apdPoNumber;
    }

    public void setApdPoNumber(String apdPoNumber) {
        this.apdPoNumber = apdPoNumber;
    }

    public String getCustomerPoNumber() {
        return customerPoNumber;
    }

    public void setCustomerPoNumber(String customerPoNumber) {
        this.customerPoNumber = customerPoNumber;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getShipFromZip() {
        return shipFromZip;
    }

    public void setShipFromZip(String shipFromZip) {
        this.shipFromZip = shipFromZip;
    }

}
