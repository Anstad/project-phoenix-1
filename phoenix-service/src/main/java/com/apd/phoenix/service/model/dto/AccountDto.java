package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountDto implements Serializable {

    private static final long serialVersionUID = 7185662040258275545L;
    
    private Map<String, Serializable> properties = new HashMap<String, Serializable>();

    private String solomonCustomerID;

    private boolean isActive;

    private String apdAssignedAccountId;

    private Date creationDate;

    private List<AddressDto> addresses = new ArrayList<>();

    private String terms;
    
    private String name;
    
    private String databaseId;
    
    private CXMLConfigurationDto cXMLConfigurationDto;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getApdAssignedAccountId() {
        return apdAssignedAccountId;
    }

    public void setApdAssignedAccountId(String apdAssignedAccountId) {
        this.apdAssignedAccountId = apdAssignedAccountId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<AddressDto> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressDto> addresses) {
        this.addresses = addresses;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the databaseId
     */
    public String getDatabaseId() {
        return databaseId;
    }

    /**
     * @param databaseId the databaseId to set
     */
    public void setDatabaseId(String databaseId) {
        this.databaseId = databaseId;
    }

    /**
     * @return the cXMLConfigurationDto
     */
    public CXMLConfigurationDto getcXMLConfigurationDto() {
        return cXMLConfigurationDto;
    }

    /**
     * @param cXMLConfigurationDto the cXMLConfigurationDto to set
     */
    public void setcXMLConfigurationDto(CXMLConfigurationDto cXMLConfigurationDto) {
        this.cXMLConfigurationDto = cXMLConfigurationDto;
    }

	public String getSolomonCustomerID() {
		return solomonCustomerID;
	}

	public void setSolomonCustomerID(String solomonCustomerID) {
		this.solomonCustomerID = solomonCustomerID;
	}

	public Map<String, Serializable> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, Serializable> properties) {
		this.properties = properties;
	}

}