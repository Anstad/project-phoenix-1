package com.apd.phoenix.service.persistence.jpa;

import java.math.BigDecimal;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Account;

/**
 * Bean to get values in sequences
 */
@Stateless
@LocalBean
public class SequenceDao extends AbstractDao<Account> {

    @PersistenceContext(unitName = "Phoenix")
    protected EntityManager entityManager;

    public static enum Sequence {
        //if you need to create additional sequences, add it here, and to sequences.sql
        USSCO_STOCKCHECK("stockcheck"), TRANSACTION_KEY("transactionKey"),
        //Message service transaction control id
        TRANSACTION_CONTROL_ID("transactionControlId"),
        //Message service group header control id
        GROUP_CONTROL_ID("groupControlId"),
        //Message service interchange header control  id
        INTERCHANGE_CONTROL_ID("interchangeControlId"),
        //APD order number generator
        APD_PO_GENERATOR("apdPo"),
        //Ticket Number for Issue Logs
        ISSUE_LOG_TICKET_NUM("ticketNum");

        private final String label;

        private Sequence(String label) {
            this.label = label;
        }

        private String getLabel() {
            return this.label;
        }
    }

    public Long nextVal(Sequence sequence) {
        Query q = entityManager.createNativeQuery("SELECT " + sequence.getLabel() + ".nextval FROM DUAL");
        return ((BigDecimal) q.getSingleResult()).longValue();
    }
}
