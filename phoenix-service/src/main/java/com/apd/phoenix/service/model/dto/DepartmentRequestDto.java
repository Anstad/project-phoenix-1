package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class DepartmentRequestDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3037572801181979381L;
    private String divDept;
    private String managerName;
    private String managerEmail;
    private String deptAction;

    /*Customized User Request Department Management*/
    private String departmentName;
    private String departmentDesktop;
    private String departmentCostCenter;
    private String departmentSameAddDefault;
    private String departmentShipAddress;
    private String departmentShipCity;
    private String departmentShipState;
    private String departmentShipZip;

    public String getDivDept() {
        return divDept;
    }

    public void setDivDept(String divDept) {
        this.divDept = divDept;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getDeptAction() {
        return deptAction;
    }

    public void setDeptAction(String deptAction) {
        this.deptAction = deptAction;
    }

    public String getName() {
        return departmentName;
    }

    public void setName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDesktop() {
        return departmentDesktop;
    }

    public void setDesktop(String departmentDesktop) {
        this.departmentDesktop = departmentDesktop;
    }

    public String getCostCenter() {
        return departmentCostCenter;
    }

    public void setCostCenter(String departmentCostCenter) {
        this.departmentCostCenter = departmentCostCenter;
    }

    public String getIsSameAddress() {
        return departmentSameAddDefault;
    }

    public void setIsSameAddress(String departmentSameAddDefault) {
        this.departmentSameAddDefault = departmentSameAddDefault;
    }

    public String getShipAddress() {
        return departmentShipAddress;
    }

    public void setShipAddress(String departmentShipAddress) {
        this.departmentShipAddress = departmentShipAddress;
    }

    public String getShipCity() {
        return departmentShipCity;
    }

    public void setShipCity(String departmentShipCity) {
        this.departmentShipCity = departmentShipCity;
    }

    public String getShipState() {
        return departmentShipState;
    }

    public void setShipState(String departmentShipState) {
        this.departmentShipState = departmentShipState;
    }

    public String getShipZip() {
        return departmentShipZip;
    }

    public void setShipZip(String departmentShipZip) {
        this.departmentShipZip = departmentShipZip;
    }

}
