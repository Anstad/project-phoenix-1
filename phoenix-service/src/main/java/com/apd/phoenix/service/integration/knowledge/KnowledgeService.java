package com.apd.phoenix.service.integration.knowledge;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.StatelessKnowledgeSession;
import com.apd.phoenix.service.brms.impl.StatelessKnowledgeSessionFactory;

@Stateless
@LocalBean
public class KnowledgeService {

    @Inject
    StatelessKnowledgeSessionFactory statelessKnowledgeSessionFactory;

    public void execute(Object fact) {
        StatelessKnowledgeSession session = statelessKnowledgeSessionFactory.getStatelessKnowledgeSession();
        session.execute(fact);
    }

    public void execute(Iterable facts) {
        StatelessKnowledgeSession session = statelessKnowledgeSessionFactory.getStatelessKnowledgeSession();
        session.execute(facts);
    }
}
