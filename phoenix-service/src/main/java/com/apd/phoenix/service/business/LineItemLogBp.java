package com.apd.phoenix.service.business;

import javax.inject.Inject;
import com.apd.phoenix.service.model.LineItemLog;
import com.apd.phoenix.service.persistence.jpa.LineItemLogDao;

/**
 *
 * @author RHC
 */
public class LineItemLogBp extends AbstractBp<LineItemLog> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(LineItemLogDao dao) {
        this.dao = dao;
    }
}
