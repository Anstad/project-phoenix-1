/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

/**
 *
 * @author nreidelb
 */
public class CashoutPageXFieldDto implements Serializable {

    private static final long serialVersionUID = 68421423159247380L;

    public enum CxmlMapping {
        ADDRESS_ID("AddressID"), ADDRESS_ID_BASE_36("AddressID(base 36)"), CONTACT_NAME("Contact Name"), CONTACT_PHONE(
                "Contact Phone"), REQUESTER("Requester"), REQUESTER_PHONE("Requester Phone"), HEADER_COMMENTS(
                "Header Comments"), DESKTOP("Desktop"), STREET_2("Street 2"), STREET_3("Street 3"), ORDER_EMAIL(
                "Order Email"), PR_NO("PR No."), NOVANT_PART_NUMBER("Novant Part Number"), MANUFACTURER_PART_NUMBER(
                "Manufacturer Part Number"), TOTAL_EACHES("Total Eaches"), NO_CHARGE_ITEM("No Charge Item"), REF_PO(
                "refpo"), APD_ACCOUNTING_ID("APD_AccountingID"), FULFILLMENT("fulfillment"), REQUESTER_NAME(
                "requestername"), APD_INTERCHANGE_ID("APD_InterchangeID"), CARRIER_ROUTING("CarrierRouting"), PAYMENT_TYPE(
                "paymenttype"), TAXABLE("Taxable"), COUNTY("County");

        CxmlMapping(String mapping) {
            this.mapping = mapping;
        }

        private String mapping;

        public String getMapping() {
            return mapping;
        }

        public void setMapping(String mapping) {
            this.mapping = mapping;
        }

    }

    private String messageMapping;
    private String label;
    private String field;
    private String defaultValue;
    private Boolean includeLabelInMapping;

    /**
     * @return the messageMapping
     */
    public String getMessageMapping() {
        return messageMapping;
    }

    /**
     * @param messageMapping the messageMapping to set
     */
    public void setMessageMapping(String messageMapping) {
        this.messageMapping = messageMapping;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field the field to set
     */
    public void setField(String field) {
        this.field = field;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Boolean isIncludeLabelInMapping() {
        return includeLabelInMapping;
    }

    public void setIncludeLabelInMapping(Boolean includeLabelInMapping) {
        this.includeLabelInMapping = includeLabelInMapping;
    }
}
