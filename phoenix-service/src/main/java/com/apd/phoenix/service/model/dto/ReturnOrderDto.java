package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

public class ReturnOrderDto implements Serializable {

    private String raNumber;
    private BigDecimal restockingFee;
    private PurchaseOrderDto order;
    private Set<LineItemXReturnDto> items;
    private String status;
    private Date createdDate;
    private String invoiceNumber;

    public String getRaNumber() {
        return raNumber;
    }

    public void setRaNumber(String raNumber) {
        this.raNumber = raNumber;
    }

    public BigDecimal getRestockingFee() {
        return restockingFee;
    }

    public void setRestockingFee(BigDecimal restockingFee) {
        this.restockingFee = restockingFee;
    }

    public PurchaseOrderDto getOrder() {
        return order;
    }

    public void setOrder(PurchaseOrderDto order) {
        this.order = order;
    }

    public Set<LineItemXReturnDto> getItems() {
        return items;
    }

    public void setItems(Set<LineItemXReturnDto> items) {
        this.items = items;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the invoiceNumber
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * @param invoiceNumber the invoiceNumber to set
     */
    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

}
