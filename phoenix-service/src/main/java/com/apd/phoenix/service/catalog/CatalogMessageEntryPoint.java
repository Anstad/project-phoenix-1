package com.apd.phoenix.service.catalog;

import java.util.ArrayList;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;

@Stateless
public class CatalogMessageEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogMessageEntryPoint.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/catalog-unprocessed")
    private Queue queue;

    private int retries = 3;

    public void scheduleRequest(ArrayList<CatalogChange> changes) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            message.setObject(changes);
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            ;
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
    }

    public void scheduleRequest(String correlationId, Map<String, String> itemData, long catalogId, SyncAction action,
            Boolean persistChanges, Long totalItems) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            CatalogChange catalogChange = new CatalogChange();
            catalogChange.setCorrelationId(correlationId);
            catalogChange.setAction(action);
            catalogChange.setCatalogId(catalogId);
            catalogChange.setItemData(itemData);
            catalogChange.setPersistChanges(persistChanges);
            catalogChange.setTotalItems(totalItems);
            message.setObject(catalogChange);
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("Could not schedule request for JMS", e);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection for JMS", e);
            }
        }
    }

    public void scheduleRequest(String correlationId, Map<String, String> itemData, long catalogId, long oldCatalogId,
            SyncAction action, Boolean persistChanges, Integer totalItems) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            ObjectMessage message = msession.createObjectMessage();
            CatalogChange catalogChange = new CatalogChange();
            catalogChange.setCorrelationId(correlationId);
            catalogChange.setAction(action);
            catalogChange.setCatalogId(catalogId);
            catalogChange.setItemData(itemData);
            catalogChange.setPersistChanges(persistChanges);
            catalogChange.setOldCatalogId(oldCatalogId);
            message.setObject(catalogChange);
            messageProducer.send(message);
        }
        catch (Exception e) {
            LOGGER.error("Could not schedule request for JMS", e);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection for JMS", e);
            }
        }
    }

}
