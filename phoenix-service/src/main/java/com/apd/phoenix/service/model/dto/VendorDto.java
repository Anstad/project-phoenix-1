package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class VendorDto implements Serializable {

    private static final long serialVersionUID = -1251611734998761850L;
    private String name;
    private AddressDto address;
    private DeploymentModeDto deploymentModeDto;

    @Deprecated
    // DO NOT USE THIS ENUM NOR REMOVE
    public enum DeploymentModeDto {
        TEST("test"), PRODUCTION("production");

        private final String label;

        private DeploymentModeDto(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressDto getAddress() {
        return address;
    }

    public void setAddress(AddressDto address) {
        this.address = address;
    }

    public DeploymentModeDto getDeploymentModeDto() {
        return deploymentModeDto;
    }

    public void setDeploymentModeDto(DeploymentModeDto deploymentModeDto) {
        this.deploymentModeDto = deploymentModeDto;
    }

}
