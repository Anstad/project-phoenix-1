package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.ShipManifest;

@Stateless
@LocalBean
public class ShipManifestDao extends AbstractDao<ShipManifest> {

    @SuppressWarnings("unchecked")
    public List<ShipManifest> getNewManifestEntries() {
        String hql = "SELECT DISTINCT shipManifest FROM ShipManifest AS shipManifest JOIN FETCH shipManifest.customerOrder AS customerOrder "
                + "JOIN FETCH customerOrder.address AS address JOIN FETCH customerOrder.account AS account "
                + "JOIN FETCH customerOrder.poNumbers AS poNumbers " + "WHERE shipManifest.isManifested=false";
        Query query = entityManager.createQuery(hql);
        List<ShipManifest> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }

    @SuppressWarnings("unchecked")
    public List<Long> getOrdersWithNewManifestEntries() {
        String hql = "SELECT co.id FROM ShipManifest AS shipManifest " + "LEFT JOIN shipManifest.customerOrder as co "
                + "WHERE shipManifest.isManifested=false " + "GROUP BY co.id";
        Query query = entityManager.createQuery(hql);
        List<Long> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results;
    }

}
