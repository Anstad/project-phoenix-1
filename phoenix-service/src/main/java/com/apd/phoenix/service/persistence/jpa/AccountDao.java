package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.SystemUser;

/**
 * Session Bean implementation class AccountDao
 */
@Stateless
@LocalBean
public class AccountDao extends AbstractDao<Account> {

    private static final Logger LOG = LoggerFactory.getLogger(AccountDao.class);

    public Account hydrateForSearchResults(Account toHydrate) {
        String hql = "SELECT account FROM Account AS account" + " WHERE account.id = " + toHydrate.getId().toString();
        //Creates the query
        Query query = entityManager.createQuery(hql);
        setItemsReturned(query, 0, 0);

        return getSingleResultOrNull(query);
    }

    public List<Account> searchAllAccounts(Account search, int start, int quantity) {
        return this.searchByExample(search, start, quantity);
    }

    public List<Account> searchAllAccountsWithRoots(Account search, Collection<Account> rootAccounts, int start, int quantity) {
    	List<Account> toReturn = new ArrayList<>();
    	for (Account account : rootAccounts) {
    		search.setRootAccount(new Account());
    		search.getRootAccount().setId(account.getId());
    		toReturn.addAll(this.searchByExample(search, start, quantity));
    	}
    	toReturn.addAll(rootAccounts);
    	return toReturn;
    }

    @SuppressWarnings("unchecked")
    public List<Account> filteredAccountList(String name, List<Long> accountWhiteList) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT account FROM Account AS account"
                + " LEFT OUTER JOIN account.rootAccount AS rootAccount");

        hql.append(" WHERE 1=1");
        if (accountWhiteList != null) {
            hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
        }
        hql.append(" AND upper(account.name) like :name");
        hql.append(" ORDER BY account.name asc");

        Query query = entityManager.createQuery(hql.toString());
        LOG.info("Searching for account using '{}'", hql.toString());

        if (accountWhiteList != null) {
            query.setParameter("accountWhiteList", accountWhiteList);
        }
        query.setParameter("name", "%" + name.toUpperCase() + "%");

        final List<Account> resultList = query.getResultList();
        return resultList;
    }

    public boolean hasOrders(Account toDelete) {
        String hql = "SELECT count(customerOrder.id) FROM CustomerOrder AS customerOrder WHERE customerOrder.account = :account";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("account", toDelete);
        setItemsReturned(query, 0, 0);
        Long count = (Long) query.getSingleResult();
        return (count != null && count < 0);
    }

    @SuppressWarnings("unchecked")
    public List<Account> getAccountsUnderSubdomain(String subdomain) {
        String hql = "SELECT account FROM Account AS account JOIN FETCH account.properties AS fetchedProperties "
                + "JOIN account.properties AS subdomainProperty WHERE subdomainProperty.value = :subdomain AND "
                + "subdomainProperty.type.name = :subdomainPropertyType";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("subdomain", subdomain);
        query.setParameter("subdomainPropertyType", "Subdomain");
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public Collection<SystemUser> getUsers(Account account) {
        if (account == null || account.getId() == null) {
            return null;
        }
        String hql = "SELECT DISTINCT user FROM SystemUser AS user LEFT JOIN user.accounts AS account WHERE account.id = :accountId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("accountId", account.getId());
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> getRootAccounts() {
        String hql = "SELECT account.name FROM Account AS account WHERE account.rootAccount is null";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Account> getAccountsForLoginValidation(SystemUser user) {
        String hql = "SELECT account FROM SystemUser AS user JOIN user.accounts AS account "
                + "LEFT JOIN FETCH account.properties LEFT JOIN FETCH account.ips " + "WHERE user.id = :userId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("userId", user.getId());
        return query.getResultList();
    }
}
