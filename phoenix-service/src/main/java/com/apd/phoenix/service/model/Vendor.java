package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.CommunicationMethod;
import org.hibernate.envers.Audited;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.NotAudited;

/**
 * The Class Vendor.
 */
@Entity
@Cacheable
@XmlRootElement
@Audited
public class Vendor implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7754476984583547842L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    /** The name. */
    @Column(nullable = false, unique = true)
    private String name;

    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    @Temporal(TemporalType.DATE)
    private Date activationDate;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")
    private Set<Address> addresses = new HashSet<Address>();

    /** The catalogs. */
    @NotAudited
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "vendor")
    private Set<Catalog> catalogs = new HashSet<Catalog>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "vendor_id")
    @AuditJoinTable(name = "vxvxvpt_AUD")
    private Set<VendorXVendorPropertyType> properties = new HashSet<VendorXVendorPropertyType>();

    // This is needed because a vendor may have a general phone number
    /** The numbers. */
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "vendor_id")
    private Set<PhoneNumber> numbers = new HashSet<PhoneNumber>();

    @ManyToOne
    @JoinColumn(name = "contact_id")
    private Person contact;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MessageType communicationMessageType = MessageType.EMAIL;

    @Column(nullable = false)
    private String communicationDestination;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<CommunicationMethod> communicationMethods = new HashSet<CommunicationMethod>();

    @Column
    private String solomonVendorId;

    @Column
    private String remitTo;

    @Column
    private String controlAccountNumber;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the version
     */
    public int getVersion() {
        return version;
    }

    /**
     * @param version
     *            the version to set
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @return the expirationDate
     */
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate
     *            the expirationDate to set
     */
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return the addresses
     */
    public Set<Address> getAddresses() {
        return addresses;
    }

    /**
     * @param addresses
     *            the addresses to set
     */
    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    /**
     * @return the catalogs
     */
    public Set<Catalog> getCatalogs() {
        return catalogs;
    }

    /**
     * @param catalogs
     *            the catalogs to set
     */
    public void setCatalogs(Set<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    /**
     * @return the vendorXVendorPropertyTypes
     */
    public Set<VendorXVendorPropertyType> getProperties() {
        return properties;
    }

    /**
     * @param vendorXVendorPropertyTypes
     *            the vendorXVendorPropertyTypes to set
     */
    public void setProperties(Set<VendorXVendorPropertyType> vendorXVendorPropertyTypes) {
        this.properties = vendorXVendorPropertyTypes;
    }

    /**
     * @return the numbers
     */
    public Set<PhoneNumber> getNumbers() {
        return numbers;
    }

    /**
     * @param numbers
     *            the numbers to set
     */
    public void setNumbers(Set<PhoneNumber> numbers) {
        this.numbers = numbers;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Vendor) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {

        String result = "";
        if (name != null && !name.trim().isEmpty())
            result += name;
        return result;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Person getContact() {
        return contact;
    }

    public void setContact(Person contact) {
        this.contact = contact;
    }

    public MessageType getCommunicationMessageType() {
        return communicationMessageType;
    }

    public void setCommunicationMessageType(MessageType communicationMessageType) {
        this.communicationMessageType = communicationMessageType;
    }

    public String getCommunicationDestination() {
        return communicationDestination;
    }

    public void setCommunicationDestination(String communicationDestination) {
        this.communicationDestination = communicationDestination;
    }

    public Set<CommunicationMethod> getCommunicationMethods() {
        return communicationMethods;
    }

    public void setCommunicationMethods(Set<CommunicationMethod> communicationMethods) {
        this.communicationMethods = communicationMethods;
    }

    public String getSolomonVendorId() {
        return solomonVendorId;
    }

    public void setSolomonVendorId(String solomonVendorId) {
        this.solomonVendorId = solomonVendorId;
    }

    public String getRemitTo() {
        return remitTo;
    }

    public void setRemitTo(String remitTo) {
        this.remitTo = remitTo;
    }

    public String getControlAccountNumber() {
        return controlAccountNumber;
    }

    public void setControlAccountNumber(String controlAccountNumber) {
        this.controlAccountNumber = controlAccountNumber;
    }
}
