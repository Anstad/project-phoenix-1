/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemLog;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.ItemDao;
import com.apd.phoenix.service.persistence.jpa.LineItemDao;
import com.apd.phoenix.service.persistence.jpa.VendorDao;

/**
 *
 * @author RHC
 */
@Stateless
@LocalBean
public class LineItemBp extends AbstractBp<LineItem> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LineItemBp.class);

    private static final String LINE_ITEM_NO_DESCRIPTION_MESSAGE = "No description for the item was provided.";
    private static final String LINE_ITEM_NO_SHORT_DESCRIPTION_MESSAGE = "No short description for the item was provided.";

    private static final String DEFAULT_UNSPSC_CLASSIFICATION_CODE = "44120000";

    @Inject
    private LineItemStatusBp lineItemStatusBp;

    @Inject
    private LineItemLogBp lineItemLogBp;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private ItemCategoryBp categoryBp;

    @Inject
    private UnitOfMeasureBp uomBp;

    @Inject
    private VendorDao vendorDao;

    @Inject
    private ItemDao itemDao;

    @Inject
    private ItemBp itemBp;

    @Inject
    private SkuTypeBp skuTypeBp;

    @Inject
    private SkuBp skuBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(LineItemDao dao) {
        this.dao = dao;
    }

    public void cancelLineItem(LineItem toCancel) {
        LineItemStatus canceled = lineItemStatusBp.getStatus(LineItemStatusEnum.CANCELED);
        this.setStatus(toCancel, canceled);
    }

    public void setStatus(LineItem lineItem, LineItemStatus lineItemStatus) {
        setStatus(lineItem, lineItemStatus, null);
    }

    public void setStatus(LineItem lineItem, LineItemStatus lineItemStatus, String note) {
        boolean isTransient = (lineItem.getId() == null) || (lineItem.getOrder() == null)
                || (lineItem.getOrder().getId() == null) || (this.findById(lineItem.getId(), LineItem.class) == null);
        if (lineItem.getQuantityShipped() > 0
                && LineItemStatusEnum.ACCEPTED.getValue().equals(lineItemStatus.getValue())) {
            LOGGER.warn("Not marking as ACCEPTED item that has already been shipped");
            return;
        }

        if (lineItemStatus != null
                && (lineItemStatus.equals(lineItem.getStatus()) || lineItemStatus.equals(lineItem.getPaymentStatus()))) {
            //won't create duplicate logs of line item status
            return;
        }

        if (lineItemStatus.isPaymentStatus()) {
            lineItem.setPaymentStatus(lineItemStatus);
        }
        else {
            lineItem.setStatus(lineItemStatus);
        }
        if (lineItemStatus != null && LineItemStatusEnum.REJECTED.getValue().equals(lineItemStatus.getValue())) {
            lineItem.setHasBeenRejected(true);
        }

        LineItemLog lineItemLog = new LineItemLog();
        lineItem.getLogs().add(lineItemLog);
        lineItemLog.setLineItemStatus(lineItemStatus);
        lineItemLog.setCustomerOrder(lineItem.getOrder());
        //Newly created line items and line items on returns that could not be matched to orders will not have SEE PHOEN-4878
        if (lineItem.getOrder() != null) {
            lineItemLog.setOrderStatus(lineItem.getOrder().getStatus());
        }
        lineItemLog.setUpdateTimestamp(new Date());
        lineItemLog.setNote(note);
        //does not persist the item log if the order or item is transient
        if (!isTransient) {
            lineItem = this.update(lineItem);
        }

        LOGGER.info("Setting line item status to:" + lineItemStatus.getValue());
    }

    /**
     * Takes an item and marks it as rejected, without changing the "hasBeenRejected" flag (which indicates 
     * that a vendor has rejected the item.
     * 
     * @param item
     * @return
     */
    public void internallyReject(LineItem item) {
        Boolean hasBeenRejected = item.getHasBeenRejected();
        this.setStatus(item, lineItemStatusBp.getStatus(LineItemStatusEnum.REJECTED));
        item.setHasBeenRejected(hasBeenRejected);
    }

    public LineItemLog createLineItemLog(LineItemLog log) {
        return lineItemLogBp.create(log);
    }

    public LineItem createFee() {
        LineItem toReturn = new LineItem();
        toReturn.setUnitPrice(BigDecimal.ZERO);
        toReturn.setEstimatedShippingAmount(BigDecimal.ZERO);
        toReturn.setMaximumTaxToCharge(BigDecimal.ZERO);
        toReturn.setQuantity(1);
        toReturn.setOriginalQuantity(1);
        toReturn.setCategory(categoryBp.getCategoryByName(LineItem.FEE_ITEM_CATEGORY));
        toReturn.setManufacturerName(LineItem.FEE_ITEM_CATEGORY);
        toReturn.setUnitOfMeasure(uomBp.getDefaultLineItemUom());
        toReturn.setUnspscClassification(DEFAULT_UNSPSC_CLASSIFICATION_CODE);
        return toReturn;
    }

    public LineItem retrieveLineItem(LineItemDto lineItemDto, CustomerOrder customerOrder) {
        for (LineItem item : customerOrder.getItems()) {
            if (item != null && item.matchesItemOnOrder(lineItemDto)) {
                return item;
            }
        }
        return null;
    }

    public LineItem createLineItem(LineItemDto lineItemDto, Credential credential) {

        boolean hostedCatalog = (credential != null && credential.isHostedCatalog());
        Item hostedVendorItem = null;
        CatalogXItem hostedCustomerItem = null;

        if (credential != null && credential.getCatalog() != null) {

            if (lineItemDto.getCustomerSku() != null && StringUtils.isNotEmpty(lineItemDto.getCustomerSku().getValue())) {
                hostedCustomerItem = catalogXItemBp.searchByCustomerSku(lineItemDto.getCustomerSku(), credential
                        .getCatalog());
            }

            // Additional steps to retrieve the hosted customer item if the initial search failed.
            if (hostedCustomerItem == null) {

                if (lineItemDto.getItem() != null) {
                    hostedVendorItem = itemBp.retrieveItem(lineItemDto.getItem(), credential.getCatalog());
                }
                // If the line item data did not reference a hosted vendor item,
                // Use the line item supplier part ID and catalog linked 
                // to the credential to retrieve all matching hosted vendor items
                if (hostedVendorItem == null && StringUtils.isNotBlank(lineItemDto.getSupplierPartId())) {
                    List<Item> matchingVendorItems = itemDao.searchBySku(lineItemDto.getSupplierPartId(),
                            SkuTypeBp.SKUTYPE_VENDOR, credential.getCatalog());
                    if (matchingVendorItems != null && !matchingVendorItems.isEmpty()) {
                        if (matchingVendorItems.size() > 1 && hostedCatalog) {
                            LOG.warn("Multiple vendor items found with vendor sku: '" + lineItemDto.getSupplierPartId()
                                    + "' in the APD hosted customer catalog: " + credential.getCatalog().getName()
                                    + ".  Selecting the first result found, which "
                                    + "could potentially cause non-idempotent behavior.");
                        }
                        hostedVendorItem = matchingVendorItems.get(0);
                    }
                }

                // If the customer sku was not provided or did not match a hosted customer item,
                // but a matching hosted vendor item was found, use it to find a hosted customer 
                // item 
                if (hostedCustomerItem == null && hostedVendorItem != null) {

                    // If matching hosted vendor items were found, use them to find 
                    // the relevant hosted customer items.
                    CatalogXItem customerItemSearch = new CatalogXItem();
                    Catalog catalogSearch = new Catalog();
                    catalogSearch.setId(credential.getCatalog().getId());
                    Item itemSearch = new Item();
                    itemSearch.setId(hostedVendorItem.getId());
                    customerItemSearch.setCatalog(catalogSearch);
                    customerItemSearch.setItem(itemSearch);
                    List<CatalogXItem> matchingCustomerItems = catalogXItemBp.searchByExample(customerItemSearch, 0, 0);
                    if (matchingCustomerItems != null) {
                        hostedCustomerItem = matchingCustomerItems.get(0);
                    }
                }
            }
        }

        LineItem lineItem = null;

        if (hostedCustomerItem != null) {
            lineItem = catalogXItemBp.getFromCatalogXItem(hostedCustomerItem);
            if (hostedCatalog) {
                lineItem = copyInboundSpecificDetails(lineItem, lineItemDto);
            }
            else {
                // In this case, the catalog associated with the credential is a pricing catalog and the customer item
                // was found in the pricing catalog, but the product catalog is still non-hosted (i.e. the product data maintained locally may be stale).
                // So here we still need to copy over ALL of the line item data from the customer request, so that it isn't rejected 
                // downstream from data mismatch.
                lineItem = copyAllDetails(lineItemDto, lineItem);
            }
        }
        else {
            LOG.warn("Requested item with neither APD sku: '"
                    + lineItemDto.getApdSku()
                    + "' nor vendor sku: '"
                    + lineItemDto.getSupplierPartId()
                    + "' could be found in customer catalog: "
                    + (credential == null || credential.getCatalog() == null
                            || credential.getCatalog().getName() == null ? "" : credential.getCatalog().getName()));
            lineItem = new LineItem();
            lineItem = copyAllDetails(lineItemDto, lineItem);
        }

        lineItem = validateLineItemData(lineItem, !hostedCatalog);

        if (hostedCustomerItem == null && hostedCatalog && !lineItem.isFee()) {
            this.setStatus(lineItem, lineItemStatusBp.getStatus(LineItemStatusEnum.REJECTED));
            LOG.info("REJECTING customer item that could not be found on hosted catalog " + lineItem.getApdSku());
        }
        else {
            this.setStatus(lineItem, lineItemStatusBp.getStatus(LineItemStatusEnum.CREATED));
        }
        if (lineItemDto.getUnitOfMeasure() != null && !StringUtils.isEmpty(lineItemDto.getUnitOfMeasure().getName())) {
            UnitOfMeasure customerUoM = uomBp.getByName(lineItemDto.getUnitOfMeasure().getName());
            if (customerUoM == null) {
                LOG.error("Unexpected customer UoM," + lineItemDto.getUnitOfMeasure().getName()
                        + " found. Creating it in the database.");
                UnitOfMeasure customerUoMEntity = new UnitOfMeasure();
                customerUoMEntity.setName(lineItemDto.getUnitOfMeasure().getName());
                customerUoM = uomBp.create(customerUoMEntity);
            }
            lineItem.setCustomerExpectedUnitOfMeasure(customerUoM);
        }
        return lineItem;
    }

    //This method copies the 'flat' properties of a line item from the dto onto the entity
    public LineItem copyAllDetails(LineItemDto lineItemDto, LineItem lineItem) {
        lineItem.setApdSku(lineItemDto.getApdSku());
        lineItem.setShortName(lineItemDto.getShortName());
        lineItem.setDescription(lineItemDto.getDescription());
        lineItem.setManufacturerName(lineItemDto.getManufacturerName());
        lineItem.setManufacturerPartId(lineItemDto.getManufacturerPartId());
        lineItem.setParentLineNumber(lineItemDto.getParentLineNumber());
        lineItem.setSupplierPartAuxiliaryId(lineItemDto.getSupplierPartAuxiliaryId());
        lineItem.setSupplierPartId(lineItemDto.getSupplierPartId());
        if (lineItemDto.getUnitOfMeasure() != null) {
            lineItem.setUnitOfMeasure(uomBp.getByName(lineItemDto.getUnitOfMeasure().getName()));
        }
        lineItem.setUnspscClassification(lineItemDto.getUnspscClassification());
        if (StringUtils.isNotBlank(lineItemDto.getVendorName())) {
            lineItem.setVendor(vendorDao.getByName(lineItemDto.getVendorName()));
        }
        if (lineItemDto.getFee() != null && lineItemDto.getFee()) {
            lineItem.setCategory(categoryBp.getCategoryByName(LineItem.FEE_ITEM_CATEGORY));
        }
        if (lineItemDto.getCustomerSku() != null) {
            Sku sku = new Sku();
            sku.setType(skuTypeBp.getCustomerSkuType());
            sku.setValue(lineItemDto.getCustomerSku().getValue());
            skuBp.create(sku);
            lineItem.setCustomerSku(sku);
        }
        lineItem = copyInboundSpecificDetails(lineItem, lineItemDto);
        return lineItem;
    }

    private LineItem copyInboundSpecificDetails(LineItem lineItem, LineItemDto lineItemDto) {
        if (lineItemDto.getQuantity() != null) {
            lineItem.setQuantity(lineItemDto.getQuantity().intValue());
        }
        /** 
         * Price on integrated order will always overwrite the price from the catalog.
         *  It is up to the approver to resolve any inconsistencies.
         */
        if (lineItemDto.getUnitPrice() != null) {
            lineItem.setUnitPrice(lineItemDto.getUnitPrice());
        }
        lineItem.setEstimatedShippingAmount(lineItemDto.getEstimatedShippingAmount());
        lineItem.setLineNumber(lineItemDto.getLineNumber());
        lineItem.setMaximumTaxToCharge(lineItemDto.getMaximumTaxToCharge());
        lineItem.setLeadTime(lineItem.getLeadTime());
        lineItem.setCustomerLineNumber(lineItemDto.getCustomerLineNumber());
        lineItem.setExtraneousCustomerSku(lineItemDto.getExtraneousCustomerSku());
        lineItem.setUniversalProductCode(lineItemDto.getUniversalProductCode());
        lineItem.setGlobalTradeIdentificatonNumber(lineItemDto.getGlobalTradeIdentificatonNumber());
        lineItem.setGlobalTradeItemNumber(lineItemDto.getGlobalTradeItemNumber());
        lineItem.setVendorComments(lineItemDto.getVendorComments());
        lineItem.setCustomerExpectedUnitPrice(lineItemDto.getUnitPrice());
        lineItem.setTaxable(lineItemDto.getTaxable());
        return lineItem;
    }

    public LineItem validateLineItemData(LineItem lineItem, boolean replaceInvalidDataWithDefaults) {
        // Set default values for any non-null fields that are null
        if (lineItem.getEstimatedShippingAmount() == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("LineItem estimated shipping amount is null.  Using default value of $0.");
            }
            lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
        }
        if (lineItem.getQuantity() == null) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("LineItem quantity is null.  Using default value of 0.");
            }
            lineItem.setQuantity(0);
        }
        if (lineItem.getUnitPrice() == null) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("LineItem unit price is null.  Using default value of 0.");
            }
            lineItem.setUnitPrice(BigDecimal.ZERO);
        }
        if (lineItem.getUnitOfMeasure() == null) {
            UnitOfMeasure defaultUom = uomBp.getDefaultLineItemUom();
            if (LOG.isWarnEnabled()) {
                LOG.warn("LineItem unit of measure is null.  Using default value of " + defaultUom.getName() + ".");
            }
            lineItem.setUnitOfMeasure(defaultUom);
        }

        if (StringUtils.isBlank(lineItem.getDescription())) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("LineItem description is blank.  Using default value of \"" + LINE_ITEM_NO_DESCRIPTION_MESSAGE
                        + "\".");
            }
            lineItem.setDescription(LINE_ITEM_NO_DESCRIPTION_MESSAGE);
        }
        if (StringUtils.isBlank(lineItem.getShortName())) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("LineItem short description is blank.  Using default value of \""
                        + LINE_ITEM_NO_SHORT_DESCRIPTION_MESSAGE + "\".");
            }
            lineItem.setShortName(LINE_ITEM_NO_SHORT_DESCRIPTION_MESSAGE);
        }
        if (lineItem.getCore() == null) {
            lineItem.setCore(false);
        }
        if (StringUtils.isBlank(lineItem.getApdSku())) {
            // Set the apdSku = the supplier part aux id or supplier part id if blank.
            String apdSku = null;
            if (StringUtils.isNotBlank(lineItem.getSupplierPartAuxiliaryId())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("LineItem APD Sku is null.  Using supplier part auxiliary ID, "
                            + lineItem.getSupplierPartAuxiliaryId() + ", as the APD Sku instead.");
                }
                apdSku = lineItem.getSupplierPartAuxiliaryId();
            }
            else {
                if (LOG.isDebugEnabled()) {
                    LOG
                            .debug("LineItem APD Sku is null and supplier part auxiliary ID is blank.  Using supplier part ID, "
                                    + lineItem.getSupplierPartId() + ", as the APD Sku instead.");
                }
                apdSku = lineItem.getSupplierPartId();
            }
            lineItem.setApdSku(apdSku);
        }
        return lineItem;
    }

    //Used to get a line item with a fully loaded item object
    public LineItem hydrateLineItem(LineItem item) {
        return ((LineItemDao) dao).hydrateLineItem(item.getId());
    }

    public LineItemXShipmentDto createLineItemXShipmentDto(LineItem lineItem, int quantity, BigDecimal shipping) {
        LineItemXShipmentDto lixs = new LineItemXShipmentDto();
        if (lineItem.getItem() != null) {
            Item item = itemBp.searchById(lineItem.getItem().getId());
            lixs.setLineItemDto(DtoFactory.createLineItemDto(lineItem, item));
        }
        else {
            lixs.setLineItemDto(DtoFactory.createLineItemDto(lineItem));
        }
        lixs.setQuantity(BigInteger.valueOf(quantity));
        lixs.setShipping(shipping);
        return lixs;
    }

}
