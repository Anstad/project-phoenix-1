package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.io.InputStream;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CreditCardTransactionLogBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.payment.ws.TransactionType;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class CreditCardCreditEmailTemplate extends EmailTemplate<CardNotificationDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardCreditEmailTemplate.class);

    private static final String TEMPLATE = "credit.card.credit";
    private static final String ATTACHMENT_PREFIX = "credit-card-credit-";

    @Inject
    CreditCardTransactionLogBp creditCardTransactionLogBp;

    @Override
	public String createBody(CardNotificationDto cardNotificationDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		CreditCardTransactionLog log = creditCardTransactionLogBp.getLogByType(cardNotificationDto.getTransactionKey(), TransactionType.CREDIT);
		if (log != null) {
			params.put("result", log);
			return this.create(params);
		} else {
			LOGGER.error("Could not find an authorize-and-capture or capture log");
			return "There was an error generating your receipt.";
		}
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(CardNotificationDto cardNotificationDto) throws NoAttachmentContentException {
        CreditCardTransactionLog creditCardTransactionLog = creditCardTransactionLogBp.getLogByType(cardNotificationDto
                .getTransactionKey(), TransactionType.CAPTURE);
        if (creditCardTransactionLog == null) {
            creditCardTransactionLog = creditCardTransactionLogBp.getLogByType(cardNotificationDto.getTransactionKey(),
                    TransactionType.AUTHORIZE_AND_CAPTURE);
        }
        if (creditCardTransactionLog != null && creditCardTransactionLog.getReturnOrder() != null) {
            Attachment orderAck = new Attachment();
            orderAck.setFileName(ATTACHMENT_PREFIX + creditCardTransactionLog.getTrackingNum() + PDF_EXT);
            orderAck.setMimeType(MimeType.pdf);
            InputStream is = reportService.generateCardCreditPdf(creditCardTransactionLog.getId().toString(),
                    creditCardTransactionLog.getReturnOrder().getId().toString());
            if (is != null) {
                orderAck.setContent(is);
            }
            else {
                throw new NoAttachmentContentException();
            }
            return orderAck;
        }
        else {
            LOGGER.error("Could not find an authorize-and-capture or capture log");
            throw new NoAttachmentContentException();
        }
    }
}
