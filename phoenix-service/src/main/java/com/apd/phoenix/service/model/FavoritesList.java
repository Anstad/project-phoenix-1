package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "axcxu_id", "name", "catalog_id" }))
public class FavoritesList implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7134212826921036374L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "favoriteslist_catalogxitem", joinColumns = { @JoinColumn(name = "favoriteslist_id") }, inverseJoinColumns = { @JoinColumn(name = "items_id") })
    private Set<CatalogXItem> userItems = new HashSet<CatalogXItem>();

    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog catalog;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((FavoritesList) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (name != null && !name.trim().isEmpty())
            result += "name: " + name;
        return result;
    }

    public Set<CatalogXItem> getUserItems() {
        return userItems;
    }

    public void setUserItems(Set<CatalogXItem> userItems) {
        this.userItems = userItems;
    }

    public Catalog getCatalog() {
        return this.catalog;
    }

    public void setCatalog(final Catalog catalog) {
        this.catalog = catalog;
    }

    public static class FavoritesListComparator implements Comparator<FavoritesList> {

        @Override
        public int compare(FavoritesList list0, FavoritesList list1) {

            if (StringUtils.isNotBlank(list0.getName()) && StringUtils.isNotBlank(list1.getName())) {
                return list0.getName().compareTo(list1.getName());
            }
            return (new GenericComparator()).compare(list0, list1);
        }
    }

    public boolean isCompanyFavoritesList() {
        return this.catalog != null;
    }
}