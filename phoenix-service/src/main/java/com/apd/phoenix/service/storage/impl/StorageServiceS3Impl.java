package com.apd.phoenix.service.storage.impl;

import java.util.Date;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.storage.api.StorageObject;
import com.apd.phoenix.service.storage.api.StorageObjectRequest;
import com.apd.phoenix.service.storage.api.StorageService;

@Named
@Stateless
public class StorageServiceS3Impl implements StorageService {

    private static final Logger LOG = LoggerFactory.getLogger(StorageServiceS3Impl.class);

    AmazonS3Client amazonS3Client;

    boolean encryptData = true;

    @PostConstruct
    public void init() {
        Properties awsProperties = PropertiesLoader.getAsProperties("aws.integration");
        AWSCredentials credentials = new BasicAWSCredentials(awsProperties.getProperty("storage.service.access-key"),
                awsProperties.getProperty("storage.service.secret-key"));
        amazonS3Client = new AmazonS3Client(credentials);
        encryptData = awsProperties.getProperty("encryptS3Data", "true").equals("true");
    }

    @Override
    public StorageObject retrieveObject(StorageObjectRequest request) {
        String bucketName = request.getAttributes().get("bucketName");

        StorageObject storageObject = new StorageObject();
        S3Object object;
        try {
            object = amazonS3Client.getObject(bucketName, request.getPath());

            if (request.isContentUrl()) {
                Date expDate = new Date(System.currentTimeMillis() + (60000 * 30));
                storageObject.setPresignedUrl(amazonS3Client.generatePresignedUrl(bucketName, request.getPath(),
                        expDate));
            }
        }
        catch (AmazonClientException e) {
            LOG.error("Error fetching object from storage. Enable debug logging to view.");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Error fetching object", e);
            }
            return null;
        }

        if (object == null) {
            LOG.error("Null object retrieved from storage.");
            return null;
        }
        ObjectMetadata metadata = object.getObjectMetadata();
        storageObject.setContent(object.getObjectContent());
        storageObject.setContentLength(metadata.getContentLength());
        storageObject.setPath(object.getKey());
        storageObject.setContentType(metadata.getContentType());
        storageObject.setLastModified(object.getObjectMetadata().getLastModified());
        storageObject.getAttributes().put("bucketName", object.getBucketName());
        return storageObject;
    }

    @Override
    public boolean createObject(StorageObject object) {
        String bucketName = object.getAttributes().get("bucketName");
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(object.getContentLength());
        metadata.setContentType(object.getContentType());
        if (encryptData) {
            metadata.setServerSideEncryption(ObjectMetadata.AES_256_SERVER_SIDE_ENCRYPTION);
        }
        try {
            amazonS3Client.putObject(bucketName, object.getPath(), object.getContent(), metadata);
        }
        catch (AmazonClientException e) {
            LOG.error("Error creating object in storage. Enable debug logging to view.");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Error creating object", e);
            }
            return false;
        }

        return true;
    }

    @Override
    public boolean deleteObject(StorageObjectRequest request) {
        if (!doesObjectExist(request)) {
            return false;
        }
        String bucketName = request.getAttributes().get("bucketName");
        try {
            amazonS3Client.deleteObject(bucketName, request.getPath());
        }
        catch (AmazonClientException e) {
            LOG.error("Error deleting object in storage. Enable debug logging to view.");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Error deleting object", e);
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean updateObject(StorageObject object) {
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(object.getPath());
        request.setAttributes(object.getAttributes());
        if (!doesObjectExist(request)) {
            return false;
        }
        if (!createObject(object)) {
            return false;
        }
        return true;
    }

    private boolean doesObjectExist(StorageObjectRequest request) {
        String bucketName = request.getAttributes().get("bucketName");
        ObjectMetadata metadata;
        try {
            metadata = amazonS3Client.getObjectMetadata(bucketName, request.getPath());
        }
        catch (AmazonClientException e) {
            LOG.error("Error determining if object exists. Enable debug logging to view.");
            if (LOG.isDebugEnabled()) {
                LOG.debug("Error determining if object exists", e);
            }
            return false;
        }

        if (metadata == null) {
            return false;
        }
        return true;
    }

}
