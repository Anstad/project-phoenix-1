package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Sku;

/**
 * Item DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ItemDao extends AbstractDao<Item> {

    //Tunable parameter via heap size and catalog size
    private static final int BATCH_SIZE = 100;

    private static final String COST_PROPERTY_TYPE = "APDcost";

    @Inject
    CatalogXItemDao catalogXItemDao;

    @Override
    public Item update(Item item) {
        Item originalItem = findById(item.getId(), Item.class);
        if (Boolean.TRUE.equals(item.getExternalCatalogRelevant())) {
            if (itemChangedInExternalCatalogRelevantWay(item, originalItem)) {
                item.setExternalCatalogChange(Boolean.TRUE);
            }
        }
        if (checkIfItemPropertiesHaveUpdated(item, originalItem)) {
            item.setLastModified(new Date());
        }
        if (checkifItemSKUHaveUpdated(item, originalItem)) {
            item.setLastModified(new Date());
        }
        if (checkifItemCostHaveUpdated(item, originalItem)) {
            item.setLastCostChangeDate(new Date());
        }
        return super.update(item);
    }

    public void updateCatalogXItems(Item item) {
        String hql = "SELECT c FROM CatalogXItem c where c.item.id = :id AND c.catalog.customer.name = 'usps'";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", item.getId());
        List<CatalogXItem> items = query.getResultList();
        for (Iterator<CatalogXItem> it = items.iterator(); it.hasNext();) {
            CatalogXItem catalogXItem = it.next();
            catalogXItemDao.update(catalogXItem);
        }
    }

    /**
     * Takes a vendor id, and returns all items that are assigned to that vendor's current catalog.
     * @param vendorId 
     * 
     * @param vendorName - the id of the vendor 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Item> getCurrentCatalogItems(Long vendorId) {
        String hql = "SELECT item FROM Item AS item WHERE item.vendorCatalog.vendor.id = :vendorId AND "
                + "item.vendorCatalog.replacement IS null";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorId", vendorId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public ScrollableResults batchForCsv(Catalog catalog, StatelessSession session) {
        String preHql = fullHydrateString() + " WHERE item.vendorCatalog.id = :id";
        session = ((Session) entityManager.getDelegate()).getSessionFactory().openStatelessSession();
        org.hibernate.Query query = session.createQuery(preHql);
        query.setReadOnly(true);
        query.setFetchSize(BATCH_SIZE);
        query.setParameter("id", catalog.getId());
        ScrollableResults sr = query.scroll(ScrollMode.FORWARD_ONLY);
        return sr;
    }

    public ArrayList<Long> batchForJMS(Catalog catalog, int batch) {
        String preHql = "SELECT item.id FROM Item AS item WHERE item.vendorCatalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * BATCH_SIZE, BATCH_SIZE);
        return new ArrayList<Long>(preQuery.getResultList());
    }

    @SuppressWarnings("unchecked")
    public List<Item> batchForStreetPrice(Catalog catalog, int batch) {
        String preHql = "SELECT item.id FROM Item AS item WHERE item.vendorCatalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * BATCH_SIZE, BATCH_SIZE);
        List<Double> idList = preQuery.getResultList();
        if (idList.size() > 0) {
	        String hql = "SELECT DISTINCT item FROM Item AS item LEFT JOIN FETCH item.properties LEFT JOIN FETCH "
	        		+ "item.vendorCatalog WHERE item.id IN (:idList)";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("idList", idList);
	        return query.getResultList();
        }
        return new ArrayList<>();
    }

    /**
     * Calls the entityManager refresh method.
     * 
     * @param item
     */
    public void refresh(Item item) {
        this.entityManager.refresh(item);
    }

    public void detach(Item item) {
        this.entityManager.detach(item);
    }

    private static String fullHydrateString() {
        return "SELECT DISTINCT item FROM Item AS item "
                + "LEFT JOIN FETCH item.manufacturer AS itemManufacturer LEFT JOIN FETCH item.skus LEFT JOIN FETCH "
                + "item.itemCategory LEFT JOIN FETCH item.unitOfMeasure LEFT JOIN FETCH item.vendorCatalog "
                + "AS catalog LEFT JOIN FETCH catalog.vendor AS vendor LEFT JOIN FETCH item.itemClassifications "
                + "AS itemClass LEFT JOIN FETCH itemClass.itemClassificationType LEFT JOIN FETCH itemClass.classificationCode "
                + "AS code LEFT JOIN FETCH code.codeType LEFT JOIN FETCH item.itemImages LEFT JOIN FETCH "
                + "item.replacement AS rep LEFT JOIN FETCH rep.skus LEFT JOIN FETCH item.properties LEFT JOIN FETCH item.hierarchyNode "
                + "LEFT JOIN FETCH item.itemSpecifications AS spec LEFT JOIN FETCH spec.specificationProperties "
                + "LEFT JOIN FETCH item.customerCosts AS cost LEFT JOIN FETCH cost.customer LEFT JOIN FETCH item.matchbook";
    }

    /**
     * Takes a vendor id, and returns all items that are assigned to that vendor's current catalog.
     * @param itemId 
     * 
     * @param vendorName - the id of the vendor 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<CatalogXItem> getCatalogXItemsForPricing(Long itemId) {
        String hql = "SELECT catalogXItem FROM CatalogXItem AS catalogXItem WHERE catalogXItem.item.id = :itemId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("itemId", itemId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Item> searchByApdSku(String value, Catalog catalog) {
        return searchBySku(value, "APD", catalog);
    }

    public Item findItemByApdSku(String value) {
        String hql = "SELECT item FROM Item AS item INNER JOIN item.skus AS sku INNER JOIN  sku.type AS skutype WHERE sku.value = :sku AND skutype.name = 'APD'";
        Query query = entityManager.createQuery(hql);
        query.setParameter("sku", value);
        return getSingleResultOrNull(query);
    }

    @SuppressWarnings("unchecked")
    public List<Item> findItemByVendorSkuAndName(String vendorSku, String vendorName) {
        String hql = "SELECT item FROM Item AS item left join item.skus as sku WHERE item.vendorCatalog.vendor.name = :vendorName AND "
                + "sku.value = :vendorSKU AND sku.type.name = 'vendor'";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorName", vendorName);
        query.setParameter("vendorSKU", vendorSku);
        return query.getResultList();
    }

    private boolean itemChangedInExternalCatalogRelevantWay(Item item, Item originalItem) {
        Map<String, String> itemClassificationStringMap = item.getClassificationStringMap();
        Map<String, String> originalClassificationStringMap = originalItem.getClassificationStringMap();
        //fields usps needs for integration
        if (valuesMatch(item.getItemImages(), originalItem.getItemImages())
                && valuesMatch(item.getSku("APD"), originalItem.getSku("APD"))
                && valuesMatch(item.getName(), originalItem.getName())
                && valuesMatch(itemClassificationStringMap.get("UNSPC"), originalClassificationStringMap.get("UNSPC"))
                && valuesMatch(item.getPropertyReadOnly("list price"), originalItem.getPropertyReadOnly("list price"))
                && (itemClassificationStringMap.containsKey("WBE") == originalClassificationStringMap
                        .containsKey("WBE"))
                && (itemClassificationStringMap.containsKey("MBE") == originalClassificationStringMap
                        .containsKey("MBE"))
                && valuesMatch(itemClassificationStringMap.get("JWOD"), originalClassificationStringMap.get("JWOD"))
                && valuesMatch(item.getSku("manufacturer"), originalItem.getSku("manufacturer"))
                && valuesMatch(item.getManufacturer().getName(), originalItem.getManufacturer().getName())
                && valuesMatch(item.getDescription(), originalItem.getDescription())) {
            return false;

        }
        else {
            return true;
        }
    }

    private boolean valuesMatch(Object object1, Object object2) {
        if (object1 == null) {
            return object2 == null;
        }
        return object1.equals(object2);
    }

    public List<Item> searchBySku(String value, String type, Catalog catalog) {
        String hql = "SELECT item FROM CatalogXItem as catX INNER JOIN catX.item AS item INNER JOIN item.skus AS sku "
                + "INNER JOIN sku.type AS type WHERE sku.value = :sku AND type.name = :type "
                + "AND catX.catalog.id = :catalogId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("sku", value);
        query.setParameter("type", type);
        query.setParameter("catalogId", catalog.getId());
        return query.getResultList();
    }

    private boolean checkIfItemPropertiesHaveUpdated(Item item, Item originalItem) {
        if (item.getPropertiesReadOnly() == null || originalItem.getPropertiesReadOnly() == null) {
            return false;
        }
        Set<ItemXItemPropertyType> itemProperties = item.getPropertiesReadOnly();
        Set<ItemXItemPropertyType> originalitemPropertieds = originalItem.getPropertiesReadOnly();
        for (ItemXItemPropertyType itemProperty : itemProperties) {
            for (ItemXItemPropertyType orginalItemProperty : originalitemPropertieds) {
                if (itemProperty.getType().getName().equals(orginalItemProperty.getType().getName())
                        && !itemProperty.getValue().equals(orginalItemProperty.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean checkifItemSKUHaveUpdated(Item item, Item originalItem) {
        if (item.getSkus() == null || originalItem.getSkus() == null) {
            return false;
        }
        Set<Sku> skus = item.getSkus();
        Set<Sku> originalSKUs = originalItem.getSkus();
        for (Sku sku : skus) {
            for (Sku orginalSKU : originalSKUs) {
                if (sku.getType().getName().equals(orginalSKU.getType().getName())
                        && !sku.getValue().equals(orginalSKU.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    public String getCurrentApdCost(Item item) {
        String hql = "SELECT prop.value FROM Item AS item LEFT JOIN item.properties as prop"
                + " WHERE item.id =:id and prop.type.name = :apdCost ";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", item.getId());
        query.setParameter("apdCost", ItemBp.APD_COST_PROPERTYTYPE);
        return (String) query.getSingleResult();
    }

    private boolean checkifItemCostHaveUpdated(Item item, Item originalItem) {
        if (item.getSkus() == null || originalItem.getSkus() == null) {
            return false;
        }

        Set<ItemXItemPropertyType> itemProperties = item.getPropertiesReadOnly();
        Set<ItemXItemPropertyType> originalitemPropertieds = originalItem.getPropertiesReadOnly();
        for (ItemXItemPropertyType itemProperty : itemProperties) {
            for (ItemXItemPropertyType orginalItemProperty : originalitemPropertieds) {
                if (itemProperty.getType().getName().equals(COST_PROPERTY_TYPE)
                        && itemProperty.getType().getName().equals(orginalItemProperty.getType().getName())
                        && !itemProperty.getValue().equals(orginalItemProperty.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<Item> findItemsByHierarchyNode(Long hierarchyNode) {
        String hql = "SELECT item FROM Item AS item WHERE item.hierarchyNode.id = :hierarchyNode ";
        Query query = entityManager.createQuery(hql);
        query.setParameter("hierarchyNode", hierarchyNode);
        return query.getResultList();
    }
}
