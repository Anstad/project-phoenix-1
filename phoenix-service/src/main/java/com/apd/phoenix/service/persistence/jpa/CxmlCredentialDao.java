/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.CxmlCredential;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CxmlCredentialDao extends AbstractDao<CxmlCredential> {

}
