package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderFailedEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final String template = "order.failed";

    @Override
    public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        params.put("apdPo", purchaseOrderDto.getApdPoNumber());
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return template;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto poAckDto) throws NoAttachmentContentException {
        //No attachment for this email
        return null;
    }

}
