/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
import org.hibernate.envers.AuditJoinTable;

/**
 * The Class ItemSpecification.
 */
@Entity
@XmlRootElement
public class ItemSpecification implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6503671047870524072L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The item specification id. */
    @Column
    private int itemSpecificationId;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ItemSpecification) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the item specification id.
     *
     * @return the item specification id
     */
    public int getItemSpecificationId() {
        return this.itemSpecificationId;
    }

    /**
     * Sets the item specification id.
     *
     * @param itemSpecificationId the new item specification id
     */
    public void setItemSpecificationId(final int itemSpecificationId) {
        this.itemSpecificationId = itemSpecificationId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "itemSpecificationId: " + itemSpecificationId;
        return result;
    }

    /** The item. */
    @ManyToOne(fetch = FetchType.LAZY)
    @Index(name = "is_item_id")
    private Item item;

    /**
     * Gets the item.
     *
     * @return the item
     */
    public Item getItem() {
        return this.item;
    }

    /**
     * Sets the item.
     *
     * @param item the new item
     */
    public void setItem(final Item item) {
        this.item = item;
    }

    /** The specification property. */
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(FetchMode.JOIN)
    @Index(name = "sp_is_id")
    @JoinColumn(name = "itemspecification_id")
    @AuditJoinTable(name = "isxsp_AUD")
    private Set<SpecificationProperty> specificationProperties;

    /**
     * Gets the specification property.
     *
     * @return the specification property
     */
    public Set<SpecificationProperty> getSpecificationProperties() {
        return this.specificationProperties;
    }

    /**
     * Sets the specification property.
     *
     * @param specificationProperty the new specification property
     */
    public void setSpecificationProperties(final Set<SpecificationProperty> specificationProperties) {
        this.specificationProperties = specificationProperties;
    }
}