package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import com.apd.phoenix.service.model.OrderLog.EventType;

/**
 * This entity holds the information for sending email notifications when certain events occur, such 
 * as an order being cancelled or a card being charged.
 * 
 * It has a many-to-one relationship with EventType, which is the event that causes this notification 
 * email to be sent, a many-to-many relationship with the SystemUser recipients, and a field for the 
 * addressing method used (To, CC, or BCC). It also has a many-to-one relationship with Credential 
 * and with Order.
 * 
 * @author RHC
 *
 */
@Entity
public class NotificationProperties implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 2302401424403016004L;

    @ManyToOne
    private CustomerOrder order;

    /**
     * This enum lists the different ways a recipient of an email can be addressed: TO, CC, or BCC.
     * 
     * @author RHC
     *
     */
    public static enum Addressing {
        TO("TO"), CC("CC"), BCC("BCC");

        private final String label;

        private Addressing(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Enumerated(EnumType.STRING)
    private Addressing addressing;

    @Column(nullable = false, length = 2000)
    @NotNull
    private String recipients;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((NotificationProperties) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    //    public EventType getType() {
    //        return this.type;
    //    }
    //
    //    public void setType(final EventType type) {
    //        this.type = type;
    //    }

    public Addressing getAddressing() {
        return this.addressing;
    }

    public void setAddressing(final Addressing addressing) {
        this.addressing = addressing;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        return result;
    }

    public String getRecipients() {
        return this.recipients;
    }

    public void setRecipients(final String recipients) {
        this.recipients = recipients;
    }

    public CustomerOrder getOrder() {
        return this.order;
    }

    public void setOrder(final CustomerOrder order) {
        this.order = order;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public NotificationProperties clone() {
        NotificationProperties clone = new NotificationProperties();
        clone.setAddressing(this.getAddressing());
        clone.setEventType(this.getEventType());
        clone.setOrder(this.getOrder());
        clone.setRecipients(this.getRecipients());
        return clone;
    }
}