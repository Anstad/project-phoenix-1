package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

public class POAcknowledgementDto extends TransactionDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 470650820807207274L;

    private List<LineItemDto> lineItems;

    private String apdPo;

    private PurchaseOrderDto purchaseOrderDto;

    private String partnerId;

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public List<LineItemDto> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItemDto> lineItems) {
        this.lineItems = lineItems;
    }

    /**
     * @return the partnerId
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId the partnerId to set
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the purchaseOrderDto
     */
    public PurchaseOrderDto getPurchaseOrderDto() {
        return purchaseOrderDto;
    }

    /**
     * @param purchaseOrderDto the purchaseOrderDto to set
     */
    public void setPurchaseOrderDto(PurchaseOrderDto purchaseOrderDto) {
        this.purchaseOrderDto = purchaseOrderDto;
    }

}
