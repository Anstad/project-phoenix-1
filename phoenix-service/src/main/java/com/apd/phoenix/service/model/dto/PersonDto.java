package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class PersonDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8878600871808162505L;

    private Set<PhoneNumberDto> phoneNumberDtos = new HashSet<PhoneNumberDto>();

    private String firstName;

    private String lastName;

    private String email;

    private String middleInitial;

    private String prefix;

    private String suffix;

    public Set<PhoneNumberDto> getPhoneNumberDtos() {
        if (phoneNumberDtos == null) {
            phoneNumberDtos = new HashSet<PhoneNumberDto>();
        }
        return phoneNumberDtos;
    }

    public void setPhoneNumberDtos(Set<PhoneNumberDto> phoneNumberDtos) {
        this.phoneNumberDtos = phoneNumberDtos;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}
