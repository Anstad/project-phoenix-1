/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

/**
 *
 * @author nreidelb
 */
public class MiscShipToDto implements Serializable {

    private static final long serialVersionUID = -2110780011851566023L;

    private String addressId;

    private String addressId36;

    private String airportCode;

    private String apdInterchangeId;

    private String apdSalesPerson;

    private String area;

    private String carrierName;

    private String carrierRouting;

    private String contactName;

    private String contractingOfficer;

    private String contractNumber;

    @Deprecated
    private String costCenter;

    private String dcNumber;

    private String deliverToName;

    private String deliverToPhoneNumber;

    private String deliveryDate;

    private String department;

    private String dept;

    private String desktop;

    private String district;

    private String division;

    private String divDept;

    private String ediID;

    private String facility;

    private String fedexNumber;

    private String financeNumber;

    private String glnID;

    private String glNumber;

    private String headerComments;

    private String lob;

    private String locationId;

    private String locations;

    private String mailStop;

    private String manager;

    private String nameOfHospital;

    private String nowl;

    private String oldRelease;

    private String orderEmail;

    private String origin;

    private String pol;

    private String purchasingGroup;

    private String requesterName;

    private String requesterPhone;

    private String shippingMethod;

    private String solomonId;

    private String storeNumber;

    private String street2;

    private String street3;

    private String unit;

    private String usAccount;

    private String uspsUnder13Oz;

    private String vendor;

    private String fedstripNumber;

    public MiscShipToDto() {
    }

    public MiscShipToDto(MiscShipToDto miscShipToDto) {
        addressId = miscShipToDto.addressId;
        addressId36 = miscShipToDto.addressId36;
        airportCode = miscShipToDto.airportCode;
        apdInterchangeId = miscShipToDto.apdInterchangeId;
        apdSalesPerson = miscShipToDto.apdSalesPerson;
        carrierName = miscShipToDto.carrierName;
        carrierRouting = miscShipToDto.carrierRouting;
        contactName = miscShipToDto.contactName;
        contractNumber = miscShipToDto.contractNumber;
        contractingOfficer = miscShipToDto.contractingOfficer;
        costCenter = miscShipToDto.costCenter;
        dcNumber = miscShipToDto.dcNumber;
        deliverToName = miscShipToDto.deliverToName;
        deliverToPhoneNumber = miscShipToDto.deliverToPhoneNumber;
        deliveryDate = miscShipToDto.deliveryDate;
        department = miscShipToDto.department;
        dept = miscShipToDto.dept;
        desktop = miscShipToDto.desktop;
        district = miscShipToDto.district;
        division = miscShipToDto.division;
        ediID = miscShipToDto.ediID;
        facility = miscShipToDto.facility;
        fedexNumber = miscShipToDto.fedexNumber;
        financeNumber = miscShipToDto.financeNumber;
        glNumber = miscShipToDto.glNumber;
        glnID = miscShipToDto.glnID;
        headerComments = miscShipToDto.headerComments;
        lob = miscShipToDto.lob;
        locationId = miscShipToDto.locationId;
        locations = miscShipToDto.locations;
        mailStop = miscShipToDto.mailStop;
        manager = miscShipToDto.manager;
        nameOfHospital = miscShipToDto.nameOfHospital;
        nowl = miscShipToDto.nowl;
        oldRelease = miscShipToDto.oldRelease;
        orderEmail = miscShipToDto.orderEmail;
        origin = miscShipToDto.origin;
        pol = miscShipToDto.pol;
        purchasingGroup = miscShipToDto.purchasingGroup;
        requesterName = miscShipToDto.requesterName;
        requesterPhone = miscShipToDto.requesterPhone;
        shippingMethod = miscShipToDto.shippingMethod;
        solomonId = miscShipToDto.solomonId;
        storeNumber = miscShipToDto.storeNumber;
        street2 = miscShipToDto.street2;
        street3 = miscShipToDto.street3;
        unit = miscShipToDto.unit;
        usAccount = miscShipToDto.usAccount;
        uspsUnder13Oz = miscShipToDto.uspsUnder13Oz;
        vendor = miscShipToDto.vendor;
        fedstripNumber = miscShipToDto.fedstripNumber;
    }

    public String getAddressId() {
        return addressId;
    }

    public String getAddressId36() {
        return addressId36;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public String getApdInterchangeId() {
        return apdInterchangeId;
    }

    public String getApdSalesPerson() {
        return apdSalesPerson;
    }

    public String getArea() {
        return area;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public String getCarrierRouting() {
        return carrierRouting;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContractingOfficer() {
        return contractingOfficer;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    @Deprecated
    public String getCostCenter() {
        return costCenter;
    }

    public String getDcNumber() {
        return dcNumber;
    }

    public String getDeliverToName() {
        return deliverToName;
    }

    /**
     * @return the deliverToPhoneNumber
     */
    public String getDeliverToPhoneNumber() {
        return deliverToPhoneNumber;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public String getDepartment() {
        return department;
    }

    public String getDept() {
        return dept;
    }

    public String getDesktop() {
        return desktop;
    }

    public String getDistrict() {
        return district;
    }

    public String getDivision() {
        return division;
    }

    public String getEdiID() {
        return ediID;
    }

    public String getFacility() {
        return facility;
    }

    public String getFedexNumber() {
        return fedexNumber;
    }

    /**
     * @return the financeNumber
     */
    public String getFinanceNumber() {
        return financeNumber;
    }

    public String getGlnID() {
        return glnID;
    }

    public String getGlNumber() {
        return glNumber;
    }

    public String getHeaderComments() {
        return headerComments;
    }

    public String getLob() {
        return lob;
    }

    public String getLocationId() {
        return locationId;
    }

    public String getLocations() {
        return locations;
    }

    public String getMailStop() {
        return mailStop;
    }

    public String getManager() {
        return manager;
    }

    public String getNameOfHospital() {
        return nameOfHospital;
    }

    public String getNowl() {
        return nowl;
    }

    public String getOldRelease() {
        return oldRelease;
    }

    public String getOrderEmail() {
        return orderEmail;
    }

    public String getOrigin() {
        return origin;
    }

    public String getPol() {
        return pol;
    }

    public String getPurchasingGroup() {
        return purchasingGroup;
    }

    public String getRequesterName() {
        return requesterName;
    }

    /**
     * @return the requesterPhoneNumber
     */
    public String getRequesterPhone() {
        return requesterPhone;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public String getSolomonId() {
        return solomonId;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public String getStreet2() {
        return street2;
    }

    public String getStreet3() {
        return street3;
    }

    public String getUnit() {
        return unit;
    }

    public String getUsAccount() {
        return usAccount;
    }

    public String getUspsUnder13Oz() {
        return uspsUnder13Oz;
    }

    public String getVendor() {
        return vendor;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public void setAddressId36(String addressId36) {
        this.addressId36 = addressId36;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public void setApdInterchangeId(String apdInterchangeId) {
        this.apdInterchangeId = apdInterchangeId;
    }

    public void setApdSalesPerson(String apdSalesPerson) {
        this.apdSalesPerson = apdSalesPerson;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public void setCarrierRouting(String carrierRouting) {
        this.carrierRouting = carrierRouting;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public void setContractingOfficer(String contractingOfficer) {
        this.contractingOfficer = contractingOfficer;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    @Deprecated
    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public void setDcNumber(String dcNumber) {
        this.dcNumber = dcNumber;
    }

    public void setDeliverToName(String deliverToName) {
        this.deliverToName = deliverToName;
    }

    /**
     * @param deliverToPhoneNumber the deliverToPhoneNumber to set
     */
    public void setDeliverToPhoneNumber(String deliverToPhoneNumber) {
        this.deliverToPhoneNumber = deliverToPhoneNumber;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public void setDesktop(String desktop) {
        this.desktop = desktop;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public void setEdiID(String ediID) {
        this.ediID = ediID;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public void setFedexNumber(String fedexNumber) {
        this.fedexNumber = fedexNumber;
    }

    public void setFinanceNumber(String financeNumber) {
        this.financeNumber = financeNumber;
    }

    public void setGlnID(String glnID) {
        this.glnID = glnID;
    }

    public void setGlNumber(String glNumber) {
        this.glNumber = glNumber;
    }

    public void setHeaderComments(String headerComments) {
        this.headerComments = headerComments;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public void setMailStop(String mailStop) {
        this.mailStop = mailStop;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public void setNameOfHospital(String nameOfHospital) {
        this.nameOfHospital = nameOfHospital;
    }

    public void setNowl(String nowl) {
        this.nowl = nowl;
    }

    public void setOldRelease(String oldRelease) {
        this.oldRelease = oldRelease;
    }

    public void setOrderEmail(String orderEmail) {
        this.orderEmail = orderEmail;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public void setPurchasingGroup(String purchasingGroup) {
        this.purchasingGroup = purchasingGroup;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    /**
     * @param requesterPhoneNumber the requesterPhoneNumber to set
     */
    public void setRequesterPhone(String requesterPhoneNumber) {
        this.requesterPhone = requesterPhoneNumber;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public void setSolomonId(String solomonId) {
        this.solomonId = solomonId;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public void setStreet3(String street3) {
        this.street3 = street3;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setUsAccount(String usAccount) {
        this.usAccount = usAccount;
    }

    public void setUspsUnder13Oz(String uspsUnder13Oz) {
        this.uspsUnder13Oz = uspsUnder13Oz;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * @return the fedstripNumber
     */
    public String getFedstripNumber() {
        return fedstripNumber;
    }

    /**
     * @param fedstripNumber the fedstripNumber to set
     */
    public void setFedstripNumber(String fedstripNumber) {
        this.fedstripNumber = fedstripNumber;
    }
}
