package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class ItemImageDto implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6095045718901321751L;

    /** The image url. */
    private String imageUrl;

    /** The image name. */
    private String imageName;

    /** Whether this is the primary image for the item */
    private Boolean primary;

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl
     *            the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the imageName
     */
    public String getImageName() {
        return imageName;
    }

    /**
     * @param imageName
     *            the imageName to set
     */
    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    /**
     * @return the primary
     */
    public Boolean getPrimary() {
        return primary;
    }

    /**
     * @param primary
     *            the primary to set
     */
    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

}
