/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

/**
 * The Class CsvUploadedItem
 */
@Entity
@XmlRootElement
public class CsvUploadedItem implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 4974168007508067457L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "uploadeditem_id")
    @Index(name = "uploadeditem_field_idx")
    private Set<CsvUploadedItemField> csvUploadedItemFields = new HashSet<CsvUploadedItemField>();

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CsvUploadedItem) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    public Set<CsvUploadedItemField> getCsvUploadedItemFields() {
        return csvUploadedItemFields;
    }

    public void setCsvUploadedItemFields(Set<CsvUploadedItemField> hierarchyNodes) {
        this.csvUploadedItemFields = hierarchyNodes;
    }

}
