package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.UserRequest;

@Stateless
@LocalBean
public class UserRequestDao extends AbstractDao<UserRequest> {

    @SuppressWarnings("unchecked")
    public List<Long> getIdByToken(String token) {
        String hql = "SELECT record.id FROM UserRequest AS record WHERE record.token=:token";
        Query query = entityManager.createQuery(hql);
        query.setParameter("token", token);
        return query.getResultList();
    }
}
