package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
@LocalBean
public class SyncItemResultSummaryDao extends AbstractDao<SyncItemResultSummary> {

    public Long getCount(String correlationId) {
        String hql = "SELECT count(s) FROM SyncItemResultSummary s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        return (Long) query.getSingleResult();
    }

    public List<SyncItemResultSummary> getSummaries(String correlationId) {
        String hql = "SELECT s FROM SyncItemResultSummary s WHERE s.correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        List<SyncItemResultSummary> results = (List<SyncItemResultSummary>) query.getResultList();
        if (results != null && !results.isEmpty()) {
            return results;
        }
        else {
            return null;
        }
    }

    public void removeRecords(String correlationId) {
        String hql = "DELETE FROM SyncItemResultSummary WHERE correlationId = :correlationId";
        Query query = entityManager.createQuery(hql);
        query.setParameter("correlationId", correlationId);
        query.executeUpdate();
    }

    @Override
    public SyncItemResultSummary update(SyncItemResultSummary syncItemResult) {
        throw new UnsupportedOperationException("SyncItemResultSummary cannot be updated");
    }

}
