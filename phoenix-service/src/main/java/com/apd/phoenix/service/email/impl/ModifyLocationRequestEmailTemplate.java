/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.UserModificationRequestDto;
import com.apd.phoenix.service.utility.EmailUtils;
import freemarker.template.TemplateException;

/**
 *
 * @author kwashington
 */
@Stateless
@LocalBean
public class ModifyLocationRequestEmailTemplate extends EmailTemplate<Object> {

    private static final String TEMPLATE = "modify.location.request";

    @Override
    public String createBody(Object content) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        UserModificationRequestDto umrDTO = (UserModificationRequestDto) content;

        params.put("requestType", umrDTO.getRequestType());
        params.put("userId", umrDTO.getUserId());
        params.put("firstName", umrDTO.getFirstName());
        params.put("lastName", umrDTO.getLastName());
        params.put("email", umrDTO.getEmail());
        params.put("phone", umrDTO.getPhone());
        params.put("ext", umrDTO.getExt());
        //Old Address
        params.put("sAddress1", umrDTO.getAddress1Old());
        params.put("sAddress2", umrDTO.getAddress2Old());
        params.put("sCity", umrDTO.getCityOld());
        params.put("sState", umrDTO.getStateOld());
        params.put("sZip", umrDTO.getZipOld());
        params.put("sComments", umrDTO.getCommentsOld());
        //New Address
        params.put("sAddress1New", umrDTO.getAddress1New());
        params.put("sAddress2New", umrDTO.getAddress2New());
        params.put("sCityNew", umrDTO.getCityNew());
        params.put("sStateNew", umrDTO.getStateNew());
        params.put("sZipNew", umrDTO.getZipNew());
        params.put("sCommentsNew", umrDTO.getCommentsNew());
        
        
        params.put("rName", umrDTO.getRequesterName());
        params.put("rEmail", umrDTO.getRequesterEmail());
        params.put("rPhone", umrDTO.getRequesterPhone());
        params.put("rMgrEmails", umrDTO.getRequesterManagerEmail());
        
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(Object content) throws NoAttachmentContentException {
        return null;
    }

}
