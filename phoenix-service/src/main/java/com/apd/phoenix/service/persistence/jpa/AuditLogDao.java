package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.query.AuditEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Entity;

@Stateless
@LocalBean
public class AuditLogDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditLogDao.class);

    @PersistenceContext(unitName = "Phoenix")
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    public List<Object[]> getEntityByRevision(Class<? extends Entity> clazz, Date start, Date end) {
        LOGGER.info("Getting audit of entities");
        LOGGER.info("Audit parameters: class = " + clazz.getName() + " , start = " + start + " , end = " + end);
        AuditReader reader = AuditReaderFactory.get(entityManager);
        return reader.createQuery().forRevisionsOfEntity(clazz, false, true).addOrder(
                AuditEntity.revisionProperty("revisionDate").asc()).add(
                AuditEntity.revisionProperty("revisionDate").ge(start)).add(
                AuditEntity.revisionProperty("revisionDate").le(end)).getResultList();
    }
}
