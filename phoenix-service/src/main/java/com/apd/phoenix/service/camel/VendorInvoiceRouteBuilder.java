package com.apd.phoenix.service.camel;

import org.apache.camel.builder.RouteBuilder;

public class VendorInvoiceRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        from("direct:foo").to("stream:out");
    }

}
