package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * 
 * @author RHC
 * 
 */
@Entity
@Table(name = "AccXCredXUser_Dept")
@Audited
public class AssignedDepartment implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "departments_id")
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "AccountXCredentialXUser_id")
    private AccountXCredentialXUser axcxu;

    @Column
    private String recipientsEmails;

    @Column
    private String recipientsNames;

    @Column(nullable = false)
    private boolean active = true;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public AccountXCredentialXUser getAxcxu() {
        return axcxu;
    }

    public void setAxcxu(AccountXCredentialXUser axcxu) {
        this.axcxu = axcxu;
    }

    public String getRecipientsEmails() {
        return recipientsEmails;
    }

    public void setRecipientsEmails(String recipientsEmails) {
        this.recipientsEmails = recipientsEmails;
    }

    public String getRecipientsNames() {
        return recipientsNames;
    }

    public void setRecipientsNames(String recipientsNames) {
        this.recipientsNames = recipientsNames;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((AssignedDepartment) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName();
        return result;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public static class AssignedDepartmentComparator implements Comparator<AssignedDepartment> {

        @Override
        public int compare(AssignedDepartment arg0, AssignedDepartment arg1) {
            if (StringUtils.isNotBlank(arg0.getDepartment().getName())
                    && StringUtils.isNotBlank(arg1.getDepartment().getName())) {
                return arg0.getDepartment().getName().compareToIgnoreCase(arg1.getDepartment().getName());
            }
            return (new GenericComparator()).compare(arg0, arg1);

        }
    }
}