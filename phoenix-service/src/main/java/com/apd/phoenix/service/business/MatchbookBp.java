package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.persistence.jpa.MatchbookDao;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class MatchbookBp extends AbstractBp<Matchbook> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(MatchbookDao dao) {
        this.dao = dao;
    }

    @Inject
    private ManufacturerBp manufacturerBp;

    /**
     * Takes an array of Strings, and uses it to find/build a Matchbook. If a matching Matchbook can't be found, and the 
     * Manufacturer doesn't exist, returns null.
     * 
     * @param parameter
     * @return
     */
    public Matchbook findFromArray(String[] parameter) {
        //gets the values from the parameter
        int modelPosition = 0;
        int manufacturerNamePosition = 1;
        int devicePosition = 2;
        int familyPosition = 3;
        String[] values = new String[4];
        for (int i = 0; i < parameter.length; i++) {
            if (parameter[i] != null && parameter[i].length() != 0) {
                values[i] = parameter[i];
            }
        }
        //stores the values on a search matchbook
        Matchbook searchMatchbook = new Matchbook();
        searchMatchbook.setModel(values[modelPosition]);
        searchMatchbook.setManufacturer(new Manufacturer());
        searchMatchbook.getManufacturer().setName(values[manufacturerNamePosition]);
        //searches
        List<Matchbook> searchList = this.searchByExactExample(searchMatchbook, 0, 0);
        //if there is exactly one result, returns in
        if (searchList.size() == 1) {
            return searchList.get(0);
        }
        //otherwise, searches for the manufacturer
        Manufacturer searchManufacturer = new Manufacturer();
        searchManufacturer.setName(values[manufacturerNamePosition]);
        List<Manufacturer> manufacturerSearchList = this.manufacturerBp.searchByExactExample(searchManufacturer, 0, 0);
        if (manufacturerSearchList.size() <= 1) {
            //if there is one or zero manufacturers, creates a new Matchbook and returns it
            Matchbook toReturn = new Matchbook();
            if (manufacturerSearchList.size() == 0) {
                Manufacturer newManufacturer = new Manufacturer();
                newManufacturer.setName(values[manufacturerNamePosition]);
                toReturn.setManufacturer(newManufacturer);
            }
            else {
                toReturn.setManufacturer(manufacturerSearchList.get(0));
            }
            toReturn.setDevice(values[devicePosition]);
            toReturn.setFamily(values[familyPosition]);
            toReturn.setModel(values[modelPosition]);
            return toReturn;
        }
        //if a manufacturer isn't uniquely specified, returns null
        return null;
    }

    public List<Matchbook> searchByModelAndManufacturer(String model, Manufacturer manufacturer) {
    	if (StringUtils.isBlank(model) || manufacturer == null || manufacturer.getId() == null) {
    		return new ArrayList<>();
    	}
        Matchbook searchMatchbook = new Matchbook();
        searchMatchbook.setManufacturer(new Manufacturer());
        searchMatchbook.getManufacturer().setId(manufacturer.getId());
        searchMatchbook.setModel(model);
        return this.searchByExactExample(searchMatchbook, 0, 0);
    }

    public Set<Matchbook> searchByCatalogAndManufacturer(Catalog catalog, Manufacturer manufacturer) {
        return ((MatchbookDao) dao).searchByCatalogAndManufacturer(catalog, manufacturer);
    }

    public Set<Manufacturer> matchbookManufacturersFromCatalog(Catalog catalog) {
        return ((MatchbookDao) dao).matchbookManufacturersFromCatalog(catalog);
    }
}
