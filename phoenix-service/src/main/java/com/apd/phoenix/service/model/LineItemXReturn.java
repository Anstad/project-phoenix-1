package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import org.hibernate.annotations.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
public class LineItemXReturn implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final Logger LOGGER = LoggerFactory.getLogger(LineItemXReturn.class);

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST,
            CascadeType.REFRESH })
    @Index(name = "LIXR_LINEITEM_IDX")
    private LineItem lineItem;

    @Column(nullable = false)
    private BigInteger quantity;

    @Column
    private String reason;

    @OneToMany(mappedBy = "lineItemXReturn", fetch = FetchType.EAGER)
    private Set<LineItemXShipmentXTaxType> taxes = new HashSet<LineItemXShipmentXTaxType>();

    @Column
    private BigDecimal restockingFee = BigDecimal.ZERO;

    //Stores return shipping in case total shipping for an item changes
    @Column
    private Boolean returnShipping;

    @Column
    private BigDecimal returnShippingAmount;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((LineItemXReturn) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public LineItem getLineItem() {
        return this.lineItem;
    }

    public void setLineItem(final LineItem lineItem) {
        this.lineItem = lineItem;
    }

    public BigInteger getQuantity() {
        return this.quantity;
    }

    public void setQuantity(final BigInteger quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "quantity: " + quantity;
        return result;
    }

    public BigDecimal getTotalTaxAmount() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentXTaxType tax : this.taxes) {
            if (tax != null && tax.getValue() != null) {
                toReturn = toReturn.add(tax.getValue());
            }
        }
        return toReturn;
    }

    public BigDecimal getSubTotalAmount() {
        return lineItem.getUnitPrice().multiply(new BigDecimal(quantity));
    }

    public BigDecimal getTotalAmount() {
        BigDecimal totalTaxAmount = getTotalTaxAmount();
        BigDecimal subTotalAmount = getSubTotalAmount();
        BigDecimal totalAmount = totalTaxAmount.add(subTotalAmount);
        totalAmount = totalAmount.add(calculateReturnShippingAmount());
        return totalAmount;
    }

    public BigDecimal calculateReturnShippingAmount() {
        if (returnShippingAmount != null) {
            return returnShippingAmount;
        }
        if (getReturnShipping() && lineItem != null && lineItem.getPaidShipping() != null) {
            setReturnShippingAmount(lineItem.getPaidShipping());
        }
        else {
            setReturnShippingAmount(BigDecimal.ZERO);
        }
        return returnShippingAmount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * This enum lists the different return reasons.
     * 
     * @author RHC
     *
     */
    public enum ReturnReason {
        APD_ERROR("APD Error"), CUSTOMER_ERROR("Customer Error"), DAMAGED_DEFECTIVE("Damaged / Defective Item"), TONDER_DEFECTIVE(
                "Toner Defective"), DELIVERY_ISSUE("Delivery Issue"), DELIVERY_REFUSED("Delivery Refused"), VENDOR_ERROR(
                "Vendor Error");

        private final String label;

        private ReturnReason(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }

    }

    public ReturnReason[] getReturnReasons() {
        return ReturnReason.values();
    }

    public Set<LineItemXShipmentXTaxType> getTaxes() {
        return taxes;
    }

    public void setTaxes(Set<LineItemXShipmentXTaxType> taxes) {
        this.taxes = taxes;
    }

    public BigDecimal getRestockingFee() {
        return restockingFee;
    }

    public void setRestockingFee(BigDecimal restockingFee) {
        this.restockingFee = restockingFee;
    }

    public Boolean getReturnShipping() {
        if (returnShipping == null) {
            return Boolean.FALSE;
        }
        return returnShipping;
    }

    public void setReturnShipping(Boolean returnShipping) {
        this.returnShipping = returnShipping;
    }

    public BigDecimal getReturnShippingAmount() {
        return returnShippingAmount;
    }

    public void setReturnShippingAmount(BigDecimal returnShippingAmount) {
        this.returnShippingAmount = returnShippingAmount;
    }

}
