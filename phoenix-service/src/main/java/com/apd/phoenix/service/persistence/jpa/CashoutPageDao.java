package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.service.model.Credential;

/**
 * CashoutPage DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CashoutPageDao extends AbstractDao<CashoutPage> {

    public CashoutPage getFromCredentialForCashout(Credential cred) {
        String hql = "SELECT cred.cashoutPage FROM Credential AS cred WHERE cred.id = :credId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("credId", cred.getId());

        return eagerLoad((CashoutPage) query.getSingleResult());
    }

    @Override
    public CashoutPage eagerLoad(CashoutPage page) {
        String hql = "SELECT page FROM CashoutPage AS page LEFT JOIN FETCH "
                + "page.pageXFields AS pageFields LEFT JOIN FETCH pageFields.field AS field LEFT JOIN FETCH "
                + "field.options WHERE page.id = :pageId";

        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("pageId", page.getId());
        return (CashoutPage) query.getSingleResult();
    }
}
