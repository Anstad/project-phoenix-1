package com.apd.phoenix.service.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import au.com.bytecode.opencsv.CSVReader;
import com.apd.phoenix.core.EncryptionUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.Bulletin;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PermissionXUser;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.RoleXUser;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.dto.SystemUserDto;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

/**
 * This class provides business process methods for System Users.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class SystemUserBp extends AbstractBp<SystemUser> {

    private static final int MAX_USERS_IN_UPLOAD = 1000;

    private static final String BILLTO_ZIP_COLUMN_HEADER = "billto zip";

    private static final String BILLTO_STATE_COLUMN_HEADER = "billto state";

    private static final String BILLTO_CITY_COLUMN_HEADER = "billto city";

    private static final String BILLTO_ADD2_COLUMN_HEADER = "billto add2";

    private static final String BILLTO_ADDRESS_TYPE = "BILLTO";

    private static final String BILLTO_ADD1_COLUMN_HEADER = "billto add1";

    private static final String SHIPTO_ZIP_COLUMN_HEADER = "shipto zip";

    private static final String SHIPTO_STATE_COLUMN_HEADER = "shipto state";

    private static final String SHIPTO_CITY_COLUMN_HEADER = "shipto city";

    private static final String SHIPTO_ADD2_COLUMN_HEADER = "shipto add2";

    private static final String SHIPTO_ADD1_COLUMN_HEADER = "shipto add1";

    private static final String EMAIL_COLUMN_HEADER = "email";

    private static final String EXPIRATION_DATE_FORMAT = "MM/yyyy";

    private static final String CC_EXPIRE_DATE_COLUMN_HEADER = "cc expire date";

    private static final String LAST_NAME_COLUMN_HEADER = "last name";

    private static final String FIRST_NAME_COLUMN_HEADER = "first name";

    private static final String CC_NUMBER_COLUMN_HEADER = "cc number";

    private static final String PASSWORD_COLUMN_HEADER = "password";

    private static final String LOGIN_COLUMN_HEADER = "login";

    private static final String PHONE_COLUMN_HEADER = "phone";

    private static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";

    private static final String APD_CUSTOMER_SERVICE = "APD Customer Service";

    private static final String CUSTOMER_SERVICE = " Customer Service";

    @Inject
    private AccountXCredentialXUserBp axcxuBp;

    @Inject
    private AddressBp addressBp;

    @Inject
    private AddressTypeBp addressTypeBp;

    @Inject
    private PaymentGatewayService paymentService;

    @Inject
    private PhoneNumberBp phoneNumberBp;

    @Inject
    private SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SystemUserDao dao) {
        this.dao = dao;
    }

    public boolean isLoginAvailable(String login) {
        List<SystemUser> su = ((SystemUserDao) dao).getSystemUserByLogin(login);
        if (su.isEmpty()) {
            return true;
        }
        return false;
    }

    public List<SystemUser> getSystemUserByLogin(String login) {
        return ((SystemUserDao) dao).getSystemUserByLogin(login);
    }

    public List<SystemUser> getFilteredSystemUserByLogin(String login) {
        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            return ((SystemUserDao) dao).getSystemUserByLogin(login, systemUserDao.getAccountIds(user));
        }
        else {
            return ((SystemUserDao) dao).getSystemUserByLogin(login);
        }
    }

    //TODO: This needs to be moved to the object
    public void deActivateUser(SystemUser user) {
        if (user.getIsActive()) {
            user.setIsActive(false);
            this.dao.update(user);
        }
    }

    //TODO: This needs to be moved to the object
    public void activateUser(SystemUser user) {
        if (!user.getIsActive()) {
            user.setIsActive(true);
            this.dao.update(user);
        }
    }

    public List<String> getRolesForUser(String user){
        List<String> toReturn = new ArrayList<>();
        List<Role> roles = ((SystemUserDao)dao).getRoles(user);
        if (roles != null){
            for (Role role : roles){
                toReturn.add(role.getName());
            }
        }
        return toReturn;
    }

    public List<String> getAccountsForUser(String user) {
    	List<String> toReturn = new ArrayList<>();
        List<Account> accounts = ((SystemUserDao)dao).getAccounts(user);
        if (accounts != null){
            for (Account account : accounts){
                toReturn.add(account.getName());
            }
        }
        return toReturn;
    }

    public List<Long> getAccountIdsForUser(String user) {
        return ((SystemUserDao) dao).getAccountIds(user);
    }

    /**
     * This method is called to set a new password for a SystemUser. This method takes the plaintext password, 
     * generates a salt, hashes the password with that salt, and stores the salt and the hash onto the SystemUser 
     * entity.
     * <p>
     * So, if you had a SystemUser exampleUser, and you wanted to set the password to "example", you would call 
     * bp.encryptAndSetPassword(exampleUser, "example"). To persist the change, you would then call 
     * bp.update(exampleUser), like you would after any other change to an entity.
     * </p>
     * <p>
     * Later, the user can login by entering the value of exampleUser.getLogin() into the "Username" field, and 
     * "example" into the "Password" field.
     * </p>
     * 
     * @param user - the user whose password is being set
     * @param password - the new password
     * @param requireChange - boolean flag indicating whether the user should be forced to change the password 
     * when they next login
     * @throws DuplicatePasswordException - if the new password is equivalent to the previous password
     */
    private void encryptAndSetPassword(SystemUser user, String password) throws DuplicatePasswordException {
        if (user.getPassword() != null && !user.getPassword().isEmpty() && user.getSalt() != null
                && !user.getSalt().isEmpty()
                && user.getPassword().equals(EncryptionUtils.hash(password, user.getSalt()))) {
            throw new DuplicatePasswordException();
        }
        user.setSalt(EncryptionUtils.salt());
        user.setPassword(EncryptionUtils.hash(password, user.getSalt()));
        user.setLoginAttempts(0);
    }

    public void encryptAndSetStrongPassword(SystemUser user, String password) throws DuplicatePasswordException,
            InsecurePasswordException {
        if (!this.isPasswordSecure(password)) {
            throw new InsecurePasswordException();
        }
        this.encryptAndSetPassword(user, password);
        this.resetPasswordFlags(user);
    }

    public void encryptAndSetTemporaryPassword(SystemUser user, String password) throws DuplicatePasswordException {
        this.encryptAndSetPassword(user, password);
        user.setChangePassword(true);
    }

    /**
     * This method takes a plaintext password, and determines if it is a sufficiently strong
     * password. If it is secure, returns true, otherwise returns false.
     * 
     * TODO: implement this method. Currently just checks if the password is not blank.
     * 
     * @param plaintext
     * @return
     */
    private boolean isPasswordSecure(String plaintext) {
        return StringUtils.isNotBlank(plaintext);
    }

    private void resetPasswordFlags(SystemUser user) {
        user.setChangePassword(false);
        user.setPasswordCreationDate(new Date());
    }

    public void beginUserRegistration(SystemUser newUser) {
        //TODO: Will be integrated with order workflow service.
        //    	processParameters.put("newUser", newUser)
        //    	ksession = produceKnowledgeSession();
        //    	ksession.startProcess("com.apd.userregistration.bpmn", processParameters);
    }

    public List<String> createFromCsvWithDefault(InputStream stream, SystemUser defaultUser, Set<AccountXCredentialXUser> credSet, String defaultPass, boolean overrideDefaultShipping, boolean overrideDefaultBilling) {
    	List<String> errorMessages = new ArrayList<>();
	    try (
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
	    	) {
	        String[] header = csvReader.readNext();
	        if (header != null) {
	            //Iterates through each line in the CSV
	            String[] line;
	            int lineNum = 2;
	            while ((line = csvReader.readNext()) != null) {
	            	if (lineNum > MAX_USERS_IN_UPLOAD) {
	            		errorMessages.add("The maximum number of users is " + MAX_USERS_IN_UPLOAD);
	            		break;
	            	}
	            	Map<String, String> valueMap = new HashMap<>();
	            	for (int i = 0; i < line.length && i < header.length; i++) {
	            		if (StringUtils.isNotEmpty(line[i])) {
	            			valueMap.put(header[i], line[i]);
	            		}
	            	}
	            	try {
	            		persistUserWithDefaults(valueMap, defaultUser, credSet, defaultPass, overrideDefaultShipping, overrideDefaultBilling);
	            	}
	            	catch (Exception e) {
	            		errorMessages.add("Error in line " + lineNum);
	            	}
	            	lineNum++;
	            }
	        }
		} catch (IOException e) {
			//do nothing
		}
	    return errorMessages;
    }

    //TODO this method should be refactored to not take six parameters and be ~150 lines long
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private void persistUserWithDefaults(Map<String, String> values, SystemUser defaultUser, Set<AccountXCredentialXUser> credSet, String defaultPass, boolean overrideDefaultShipping, boolean overrideDefaultBilling) throws PaymentGatewayService.CardVaultStorageException {
    	SystemUser newUser = new SystemUser();
    	newUser.setLogin(values.get(LOGIN_COLUMN_HEADER));
    	newUser.getAccounts().addAll(defaultUser.getAccounts());
    	newUser.setCreationDate(defaultUser.getCreationDate());
    	for (Bulletin bulletin : defaultUser.getBulletins()) {
    		Bulletin newBulletin = new Bulletin();
    		newBulletin.setExpirationDate(bulletin.getExpirationDate());
    		newBulletin.setMessage(bulletin.getMessage());
    		newBulletin.setName(bulletin.getName());
    		newUser.getBulletins().add(newBulletin);
    	}
    	newUser.setChangePassword(defaultUser.getChangePassword());
    	newUser.setConcurrentAccess(defaultUser.getConcurrentAccess());
    	newUser.setIsActive(defaultUser.getIsActive());
    	try {
        	if (StringUtils.isEmpty(defaultPass)) {
        		this.encryptAndSetTemporaryPassword(newUser, defaultPass);
        	}
    		if (values.containsKey(PASSWORD_COLUMN_HEADER)) {
    			this.encryptAndSetTemporaryPassword(newUser, values.get(PASSWORD_COLUMN_HEADER));
    		}
    	}
    	catch (DuplicatePasswordException e) {
    		//do nothing
    	}
    	if (defaultUser.getPaymentInformation() != null) {
    		newUser.setPaymentInformation(new PaymentInformation());
    		PaymentInformation defaultInfo = defaultUser.getPaymentInformation();
    		PaymentInformation newInfo = newUser.getPaymentInformation();
    		newInfo.setContact(defaultInfo.getContact());
    		newInfo.setBillingAddress(defaultInfo.getBillingAddress());
    		newInfo.setInvoiceSendMethod(defaultInfo.getInvoiceSendMethod());
    		if (defaultInfo.getCard() != null) {
    			newInfo.setCard(new CardInformation());
    			CardInformation defaultCard = defaultInfo.getCard();
    			CardInformation newCard = newInfo.getCard();
    			newCard.setDebit(defaultCard.getDebit());
    			newCard.setExpiration(defaultCard.getExpiration());
    			newCard.setNameOnCard(defaultCard.getNameOnCard());
    			newCard.setNumber(defaultCard.getNumber());
    			newCard.setGhost(defaultCard.isGhost());
    			newCard.setCvvIndicator(defaultCard.getCvvIndicator());
    		}
    	}
		if (values.containsKey(CC_NUMBER_COLUMN_HEADER)) {
			if (newUser.getPaymentInformation() == null) {
				newUser.setPaymentInformation(new PaymentInformation());
			}
			PaymentInformation newInfo = newUser.getPaymentInformation();
			newInfo.setCard(new CardInformation());
			newInfo.getCard().setNameOnCard(values.get(FIRST_NAME_COLUMN_HEADER) + " " + values.get(LAST_NAME_COLUMN_HEADER));
			newInfo.getCard().setNumber(values.get(CC_NUMBER_COLUMN_HEADER));
			newInfo.getCard().setGhost(true);
			try {
				newInfo.getCard().setExpiration(new SimpleDateFormat(EXPIRATION_DATE_FORMAT).parse(values.get(CC_EXPIRE_DATE_COLUMN_HEADER)));
			} catch (ParseException e) {
				//do nothing
			}
		}
		if (newUser.getPaymentInformation() != null && newUser.getPaymentInformation().getCard() != null) {                      
			paymentService.storeCreditCard(newUser.getPaymentInformation());
		}
    	newUser.setPerson(new Person());
    	newUser.getPerson().setFirstName(values.get(FIRST_NAME_COLUMN_HEADER));
    	newUser.getPerson().setLastName(values.get(LAST_NAME_COLUMN_HEADER));
    	newUser.getPerson().setEmail(values.get(EMAIL_COLUMN_HEADER));
    	for (Address address : defaultUser.getPerson().getAddresses()) {
    		Address newAddress = addressBp.cloneEntity(address);
    		newAddress.setPerson(newUser.getPerson());
    		newUser.getPerson().getAddresses().add(newAddress);
    	}
    	if (overrideDefaultShipping && values.containsKey(SHIPTO_ADD1_COLUMN_HEADER)) {
    		Set<Address> addressesToRemove = new HashSet<>();
    		for (Address address : newUser.getPerson().getAddresses()) {
    			if (address.getType().getName().equals(AddressTypeBp.SHIPTO_ADDRESS_TYPE)) {
    				addressesToRemove.add(address);
    			}
    		}
    		newUser.getPerson().getAddresses().removeAll(addressesToRemove);
    		Address newAddress = new Address();
    		newUser.getPerson().getAddresses().add(newAddress);
    		newAddress.setPerson(newUser.getPerson());
    		newAddress.setPrimary(true);
    		newAddress.setType(addressTypeBp.getByName(AddressTypeBp.SHIPTO_ADDRESS_TYPE));
    		newAddress.setLine1(values.get(SHIPTO_ADD1_COLUMN_HEADER));
    		newAddress.setLine2(values.get(SHIPTO_ADD2_COLUMN_HEADER));
    		newAddress.setCity(values.get(SHIPTO_CITY_COLUMN_HEADER));
    		newAddress.setState(values.get(SHIPTO_STATE_COLUMN_HEADER));
    		newAddress.setZip(values.get(SHIPTO_ZIP_COLUMN_HEADER));
    	}
    	if (overrideDefaultBilling && values.containsKey(BILLTO_ADD1_COLUMN_HEADER)) {
    		Set<Address> addressesToRemove = new HashSet<>();
    		for (Address address : newUser.getPerson().getAddresses()) {
    			if (address.getType().getName().equals(BILLTO_ADDRESS_TYPE)) {
    				addressesToRemove.add(address);
    			}
    		}
    		newUser.getPerson().getAddresses().removeAll(addressesToRemove);
    		Address newAddress = new Address();
    		newUser.getPerson().getAddresses().add(newAddress);
    		newAddress.setPerson(newUser.getPerson());
    		newAddress.setPrimary(true);
    		newAddress.setType(addressTypeBp.getByName(BILLTO_ADDRESS_TYPE));
    		newAddress.setLine1(values.get(BILLTO_ADD1_COLUMN_HEADER));
    		newAddress.setLine2(values.get(BILLTO_ADD2_COLUMN_HEADER));
    		newAddress.setCity(values.get(BILLTO_CITY_COLUMN_HEADER));
    		newAddress.setState(values.get(BILLTO_STATE_COLUMN_HEADER));
    		newAddress.setZip(values.get(BILLTO_ZIP_COLUMN_HEADER));
    	}
    	if (values.containsKey(PHONE_COLUMN_HEADER) && values.get(PHONE_COLUMN_HEADER).matches(phoneNumberBp.getReadableNumberRegex())) {
	    	PhoneNumber number = new PhoneNumber();
	    	phoneNumberBp.setReadableNumber(number, values.get(PHONE_COLUMN_HEADER));
	    	number.setPerson(newUser.getPerson());
	    	newUser.getPerson().getPhoneNumbers().add(number);
    	}
    	for (PermissionXUser permission : defaultUser.getPermissionXUsers()) {
    		PermissionXUser newPermission = new PermissionXUser();
    		newPermission.setStartDate(permission.getStartDate());
    		newPermission.setExpirationDate(permission.getExpirationDate());
    		newPermission.setPermission(permission.getPermission());
    		newUser.getPermissionXUsers().add(permission);
    	}
    	for (RoleXUser role : defaultUser.getRoleXUsers()) {
    		RoleXUser newRole = new RoleXUser();
    		newRole.setStartDate(role.getStartDate());
    		newRole.setExpirationDate(role.getExpirationDate());
    		newRole.setRole(role.getRole());
    		newUser.getRoleXUsers().add(role);
    	}
    	newUser = this.create(newUser);
    	for (AccountXCredentialXUser axcxu : credSet) {
    		AccountXCredentialXUser newAxcxu = new AccountXCredentialXUser();
    		newAxcxu.setAccount(axcxu.getAccount());
    		newAxcxu.setApproverUser(axcxu.getApproverUser());
    		newAxcxu.setCredential(axcxu.getCredential());
    		newAxcxu.getDepartments().addAll(axcxu.getDepartments());
    		newAxcxu.setPunchLevel(axcxu.getPunchLevel());
    		newAxcxu.setUsAccountNumber(axcxu.getUsAccountNumber());
    		newAxcxu.setUser(newUser);
    		for (AssignedCostCenter costCenter : axcxu.getAllowedCostCenters()) {
    			AssignedCostCenter newCenter = new AssignedCostCenter();
    			newCenter.setCurrentCharge(costCenter.getCurrentCharge());
    			newCenter.setPendingCharge(costCenter.getPendingCharge());
    			newCenter.setTotalLimit(costCenter.getTotalLimit());
    			newAxcxu.getAllowedCostCenters().add(newCenter);
    		}
    		axcxuBp.create(newAxcxu);
    	}
    }

    public SystemUser retrieveUser(SystemUserDto systemUserDto) {
        List<SystemUser> results = this.getSystemUserByLogin(systemUserDto.getLogin());
        if (results == null || results.isEmpty()) {
            return null;
        }
        else {
            return results.get(0);
        }
    }

    public Set<Account> getRootAccountsOfUser(SystemUser systemUser) {
    	Set<Account> toReturn = new HashSet<>();
    	for (Account account : systemUser.getAccounts()) {
    		if (account.getRootAccount() == null) {
    			toReturn.add(account);
    		} else {
    			toReturn.add(account.getRootAccount());
    		}
    	}
    	return toReturn;
	}

    public List<SystemUser> filteredUserList(String firstName, String lastName, String login, String email) {
        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            return ((SystemUserDao) dao).filteredUserList(firstName, lastName, login, email, systemUserDao
                    .getAccountIds(user));
        }
        else {
            return ((SystemUserDao) dao).filteredUserList(firstName, lastName, login, email, null);
        }
    }

    public String getWorkflowGroups(SystemUser user, String genericGroup, String suffix) {
        StringBuilder groups = new StringBuilder(genericGroup);
        if (user == null) {
            return groups.toString();
        }
        else {
            for (String accountName : this.getAccountsForUser(user.getLogin())) {
                groups.append("," + accountName + suffix);
            }
            return groups.toString();
        }
    }

    /**
     * Note: Don't use this method for systemusers. Use getSystemUserByLogin.
     */
    @Override
    @Deprecated
    public List<SystemUser> searchByExactExample(SystemUser search, int start, int quantity) {
        return super.searchByExactExample(search, start, quantity);
    }

    public SystemUser retrieveUserWithPhoneNumbers(SystemUserDto systemUserDto) {
        SystemUser toReturn = this.retrieveUser(systemUserDto);
        toReturn.getPerson().getPhoneNumbers().size();
        return toReturn;
    }

    public List<SystemUser> usersWithNoPassword() {
        return ((SystemUserDao) this.dao).usersWithNoPassword();
    }
}
