package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;
/**
 * The Class ReturnOrder.
 */
@Entity
@XmlRootElement
public class ReturnOrder implements Serializable, com.apd.phoenix.service.model.Entity{

    private static final long serialVersionUID = 6155014814050730364L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The ra number. */
    @Column
    private String raNumber;

    /** The restocking fee. */
    @ManyToOne
    private LineItem restockingFee;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @Index(name = "RETURNORDER_CUSTORDER_IDX")
    private CustomerOrder order;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "RETURNORDER_ID")
    @Index(name = "RETURNORDER_LIXR_IDX")
    private Set<LineItemXReturn> items = new HashSet<>();
    
    @OneToMany(mappedBy = "returnOrder", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,
            CascadeType.DETACH })
    private Set<ReturnOrderLog> returnOrderLogs = new HashSet<ReturnOrderLog>();
    
    @Enumerated(EnumType.STRING)
    private ReturnOrderStatus status = ReturnOrderStatus.UNRECONCILED;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "RETURN_CREATEDATE_IDX")
    private Date createdDate = new Date();

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "RETURN_RECONCILEDATE_IDX")
    private Date reconciledDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Index(name = "RETURN_CLOSEDATE_IDX")
    private Date closeDate;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRaNumber() {
		return raNumber;
	}

	public void setRaNumber(String raNumber) {
		this.raNumber = raNumber;
	}

	public LineItem getRestockingFee() {
		return restockingFee;
	}

	public void setRestockingFee(LineItem restockingFee) {
		this.restockingFee = restockingFee;
	}

	public CustomerOrder getOrder() {
		return order;
	}

	public void setOrder(CustomerOrder order) {
		this.order = order;
	}

	public Set<LineItemXReturn> getItems() {
		return items;
	}

	public void setItems(Set<LineItemXReturn> items) {
		this.items = items;
	}

	public ReturnOrderStatus getStatus() {
		return status;
	}

	public void setStatus(ReturnOrderStatus status) {
		this.status = status;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getReconciledDate() {
		return reconciledDate;
	}

	public void setReconciledDate(Date reconciledDate) {
		this.reconciledDate = reconciledDate;
	}

	public Date getCloseDate() {
		return closeDate;
	}

	public void setCloseDate(Date closeDate) {
		this.closeDate = closeDate;
	}
	
	public Set<ReturnOrderLog> getReturnOrderLogs() {
        return this.returnOrderLogs;
    }

    public void setReturnOrderLogs(Set<ReturnOrderLog> returnOrderLogs) {
        this.returnOrderLogs = returnOrderLogs;
    }

	/*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ReturnOrder) that).id);
        }
        return super.equals(that);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (raNumber != null && !raNumber.trim().isEmpty())
            result += raNumber;
        return result;
    }
    
    public enum ReturnOrderStatus {
    	RECONCILED, UNRECONCILED, CLOSED;
    }
}