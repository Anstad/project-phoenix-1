package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class InvoiceErrorsEmailTemplate extends EmailTemplate<InvoiceDto> {

    private static final String TEMPLATE = "invoice.errors";

    @Override
	public String createBody(InvoiceDto invoiceDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("invoiceNumber", invoiceDto.getInvoiceNumber());
		params.put("items", invoiceDto.getActualItems());

		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(InvoiceDto invoiceDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
