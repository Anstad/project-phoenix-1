/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.impl;

import com.apd.phoenix.core.StringEscape;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ejb.Stateful;
import javax.enterprise.context.ApplicationScoped;

import org.drools.KnowledgeBase;
import org.drools.agent.KnowledgeAgent;
import org.drools.agent.KnowledgeAgentConfiguration;
import org.drools.agent.KnowledgeAgentFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.io.impl.UrlResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.brms.api.KnowledgeBaseFactory;

@Stateful
@ApplicationScoped
public class KnowledgeBaseFactoryImpl implements KnowledgeBaseFactory {

    private static final Logger LOG = LoggerFactory.getLogger(KnowledgeBaseFactoryImpl.class);
    private static final String DEFAULT_URL = "http://localhost:8080/jboss-brms/org.drools.guvnor.Guvnor/package/phoenix/";
    private static final String DEFAULT_AGENT_NAME = "phoenixAgent";
    private static final String DEFAULT_AGENT_INSTANCE = "false";
    private static final Integer CACHE_SIZE = 5;
    private Properties guvnor = PropertiesLoader.getAsProperties("guvnor");
    
    private List<String> keys = new LinkedList<String>();
    private Map<String, KnowledgeAgent> knowledgeAgents = new HashMap<>();

    @Override
    public void rebuild() {
        this.rebuild(guvnor.getProperty("snapshot"));
    }

    @Override
    public void rebuild(String snapshot) {
        this.build(snapshot);
    }

    @Override
    public KnowledgeBase getKnowledgeBase() {
        return this.getKnowledgeBase(guvnor.getProperty("snapshot"));
    }

    @Override
    public KnowledgeBase getKnowledgeBase(String snapshot) {
        if (this.knowledgeAgents.get(snapshot) == null) {
            this.build(snapshot);
        }

        return this.knowledgeAgents.get(snapshot).getKnowledgeBase();
    }

    private void build(String snapshot) {
        Properties commonProperties = PropertiesLoader.getAsProperties("guvnor");
        String url = commonProperties.getProperty("url", DEFAULT_URL) + StringEscape.escapeForUrl(snapshot);

        String defaulAgentName = commonProperties.getProperty("default.agent.name", DEFAULT_AGENT_NAME);
        String droolsAgentInstance = commonProperties.getProperty("drools.agent.instance", DEFAULT_AGENT_INSTANCE);
        LOG.info("Building KnowledgeBase (" + snapshot + ")");
        UrlResource urlResource = this.setupUrlResource(url);

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        kbuilder.add(urlResource, ResourceType.PKG);

        KnowledgeBase kbase = kbuilder.newKnowledgeBase();
        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        KnowledgeAgentConfiguration kaconf = KnowledgeAgentFactory.newKnowledgeAgentConfiguration();
        kaconf.setProperty("drools.agent.newInstance", droolsAgentInstance);
        KnowledgeAgent kagent = KnowledgeAgentFactory.newKnowledgeAgent(defaulAgentName, kbase, kaconf);
        kagent.applyChangeSet(urlResource);

        ResourceFactory.getResourceChangeNotifierService().start();
        ResourceFactory.getResourceChangeScannerService().start();

        if (!knowledgeAgents.containsKey(snapshot) && knowledgeAgents.size() == CACHE_SIZE) {
        	knowledgeAgents.remove(keys.remove(0));
        }
        keys.add(snapshot);
        this.knowledgeAgents.put(snapshot, kagent);
    }

    private UrlResource setupUrlResource(String url) {
        Properties commonProperties = PropertiesLoader.getAsProperties("guvnor");
        String username = commonProperties.getProperty("username");
        String password = commonProperties.getProperty("password");
        LOG.debug("\n\tsetupUrlResource");
        UrlResource resource = (UrlResource) ResourceFactory.newUrlResource(url);
        resource.setBasicAuthentication("enabled");
        resource.setUsername(username);
        resource.setPassword(password);
        return resource;
    }

}
