package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

@Entity
public class SyncItemResult implements Serializable, com.apd.phoenix.service.model.Entity {

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "sir_errors", joinColumns = @JoinColumn(name = "sir_id"))
    @Column(name = "errors", length = 4000)
    @Fetch(FetchMode.SUBSELECT)
    //Indexes have been added to this table
    private Set<String> errors = new HashSet<String>();

    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "sir_warnings", joinColumns = @JoinColumn(name = "sir_id"))
    @Column(name = "warnings", length = 4000)
    @Fetch(FetchMode.SUBSELECT)
    //Indexes have been added to this table
    private Set<String> warnings = new HashSet<String>();

    @Column
    private String resultApdSku;

    @Column
    private String resultVendor;

    @Index(name = "sir_corrId")
    private String correlationId;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Set<String> getErrors() {
        return errors;
    }

    public void setErrors(Set<String> errors) {
        this.errors = errors;
    }

    public Set<String> getWarnings() {
        return warnings;
    }

    public void setWarnings(Set<String> warnings) {
        this.warnings = warnings;
    }

    public String getResultApdSku() {
        return resultApdSku;
    }

    public void setResultApdSku(String resultApdSku) {
        this.resultApdSku = resultApdSku;
    }

    public String getResultVendor() {
        return resultVendor;
    }

    public void setResultVendor(String resultVendor) {
        this.resultVendor = resultVendor;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }
}
