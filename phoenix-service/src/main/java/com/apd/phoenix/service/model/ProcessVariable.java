package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * This entity stores the type of some processVariable. It has a one-to-many
 * relationship with processVariable, and can have values such as "approvers" or
 * "minimum acceptable value".
 * 
 * @author RHC
 * 
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "processConfiguration_id", "name" }))
public class ProcessVariable implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1295765487638980414L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(nullable = false)
    private String name;

    @Column
    private String value;

    @ManyToOne
    private ProcessConfiguration processConfiguration;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ProcessVariable) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * @return the processConfiguration
     */
    public ProcessConfiguration getProcessConfiguration() {
        return processConfiguration;
    }

    /**
     * @param processConfiguration
     *            the processConfiguration to set
     */
    public void setProcessConfiguration(ProcessConfiguration processConfiguration) {
        this.processConfiguration = processConfiguration;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (name != null && !name.trim().isEmpty())
            result += "type: " + name;
        return result;
    }

}