package com.apd.phoenix.service.persistence.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.ServiceLogSearchCriteria;
import com.apd.phoenix.service.model.ServiceLog;

/**
 * ServiceLog DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ServiceLogDao extends AbstractDao<ServiceLog> {

    @SuppressWarnings("unchecked")
    public List<ServiceLog> getServiceLogs(ServiceLogSearchCriteria criteria) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT serviceLog FROM ServiceLog AS serviceLog"
                + " LEFT OUTER JOIN FETCH serviceLog.customerOrder AS customerOrder"
                + " LEFT OUTER JOIN FETCH customerOrder.poNumbers AS poNumber"
                + " LEFT OUTER JOIN FETCH serviceLog.account AS account"
                + " LEFT OUTER JOIN FETCH account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN FETCH serviceLog.apdCsrRep AS apdRep"
                + " LEFT OUTER JOIN FETCH serviceLog.contactMethod AS contactMethod"
                + " LEFT OUTER JOIN FETCH serviceLog.comments"
                + " LEFT OUTER JOIN FETCH serviceLog.serviceReason AS servicereason");

        hql.append(" WHERE 1=1");
        Map<String, Object> paramMap = new HashMap<>();
        if (criteria.getStartDate() != null) {
            hql.append(" AND serviceLog.openedDate > :startDate");
            paramMap.put("startDate", criteria.getStartDate());
        }
        if (criteria.getEndDate() != null) {
            hql.append(" AND serviceLog.updateTimestamp < :endDate");
            paramMap.put("endDate", criteria.getEndDate());
        }
        if (criteria.getTicketStatus() != null) {
            hql.append(" AND serviceLog.status=:ticketStatus");
            paramMap.put("ticketStatus", criteria.getTicketStatus());
        }
        if (StringUtils.isNotBlank(criteria.getTicketNumber())) {
            hql.append(" AND serviceLog.ticketNumber=:ticketNumber");
            paramMap.put("ticketNumber", criteria.getTicketNumber());
        }
        if (StringUtils.isNotBlank(criteria.getPoNumber()) ) {
            hql.append(" AND poNumber.value=:poNumberValue");
            paramMap.put("poNumberValue", criteria.getPoNumber());
        }
        if (StringUtils.isNotBlank(criteria.getAccountName())) {
            hql.append(" AND LOWER(account.name) LIKE :account");
            paramMap.put("account", "%" + criteria.getAccountName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getDepartment())) {
            hql.append(" AND LOWER(serviceLog.department) LIKE :department");
            paramMap.put("department", "%" + criteria.getDepartment().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactName())) {
            hql.append(" AND LOWER(contactName) LIKE :contactName");
            paramMap.put("contactName", "%" + criteria.getContactName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactNumber())) {
            hql.append(" AND LOWER(contactNumber) LIKE :contactNumber");
            paramMap.put("contactNumber", "%" + criteria.getContactNumber().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactEmail())) {
            hql.append(" AND LOWER(contactEmail) LIKE :contactEmail");
            paramMap.put("contactEmail", "%" + criteria.getContactEmail().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getApdRep())) {
            hql.append(" AND LOWER(apdRep.login) LIKE :apdRep");
            paramMap.put("apdRep", "%" + criteria.getApdRep().toLowerCase() + "%");
        }
        if (criteria.getAccountWhiteList() != null && !criteria.getAccountWhiteList().isEmpty() && !criteria.getAccountWhiteList().contains(null)) {
        	hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
            paramMap.put("accountWhiteList", criteria.getAccountWhiteList());
        }
        
        hql.append(" ORDER BY serviceLog.id desc");
        if (paramMap.keySet().size() == 0) {
        	//no parameters set
        	return null;
        }

        Query query = entityManager.createQuery(hql.toString());
        
        for (String key : paramMap.keySet()) {
                if(key.equals("startDate")){
                    query.setParameter(key, criteria.getStartDate(), TemporalType.TIMESTAMP);
                }
                else if(key.equals("endDate")){
                    query.setParameter(key, criteria.getEndDate(), TemporalType.TIMESTAMP);
                }
                else {
                    query.setParameter(key, paramMap.get(key));
                }
        }
        final List<ServiceLog> resultList = query.getResultList();
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<ServiceLog> getServiceLogsPaginated(ServiceLogSearchCriteria criteria) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT serviceLog FROM ServiceLog AS serviceLog"
                + " LEFT OUTER JOIN serviceLog.customerOrder AS customerOrder"
                + " LEFT OUTER JOIN customerOrder.poNumbers AS poNumber"
                + " LEFT OUTER JOIN serviceLog.account AS account"
                + " LEFT OUTER JOIN FETCH account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN serviceLog.apdCsrRep AS apdRep"
                + " LEFT OUTER JOIN serviceLog.contactMethod AS contactMethod"
                + " LEFT OUTER JOIN FETCH serviceLog.comments"
                + " LEFT OUTER JOIN serviceLog.serviceReason AS servicereason");

        hql.append(" WHERE 1=1");
        Map<String, Object> paramMap = new HashMap<>();
        if (criteria.getStartDate() != null) {
            hql.append(" AND serviceLog.openedDate > :startDate");
            paramMap.put("startDate", criteria.getStartDate());
        }
        if (criteria.getEndDate() != null) {
            hql.append(" AND serviceLog.updateTimestamp < :endDate");
            paramMap.put("endDate", criteria.getEndDate());
        }
        if (criteria.getTicketStatus() != null) {
            hql.append(" AND serviceLog.status=:ticketStatus");
            paramMap.put("ticketStatus", criteria.getTicketStatus());
        }
        if (StringUtils.isNotBlank(criteria.getTicketNumber())) {
            hql.append(" AND serviceLog.ticketNumber=:ticketNumber");
            paramMap.put("ticketNumber", criteria.getTicketNumber());
        }
        if (StringUtils.isNotBlank(criteria.getPoNumber()) ) {
            hql.append(" AND poNumber.value=:poNumberValue");
            paramMap.put("poNumberValue", criteria.getPoNumber());
        }
        if (StringUtils.isNotBlank(criteria.getAccountName())) {
            hql.append(" AND LOWER(account.name) LIKE :account");
            paramMap.put("account", "%" + criteria.getAccountName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getDepartment())) {
            hql.append(" AND LOWER(serviceLog.department) LIKE :department");
            paramMap.put("department", "%" + criteria.getDepartment().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactName())) {
            hql.append(" AND LOWER(contactName) LIKE :contactName");
            paramMap.put("contactName", "%" + criteria.getContactName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactNumber())) {
            hql.append(" AND LOWER(contactNumber) LIKE :contactNumber");
            paramMap.put("contactNumber", "%" + criteria.getContactNumber().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactEmail())) {
            hql.append(" AND LOWER(contactEmail) LIKE :contactEmail");
            paramMap.put("contactEmail", "%" + criteria.getContactEmail().toLowerCase() + "%");
        }
        if (criteria.getApdRep() != null) {
            hql.append(" AND LOWER(apdRep.login) LIKE :apdRep");
            paramMap.put("apdRep", "%" + criteria.getApdRep().toLowerCase() + "%");
        }
        if (criteria.getAccountWhiteList() != null) {
        	hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
            paramMap.put("accountWhiteList", criteria.getAccountWhiteList());
        }
        
        hql.append(" ORDER BY serviceLog.id desc");
        if (paramMap.keySet().size() == 0) {
        	//no parameters set
        	return null;
        }

        Query query = entityManager.createQuery(hql.toString());
        
        for (String key : paramMap.keySet()) {
                if(key.equals("startDate")){
                    query.setParameter(key, criteria.getStartDate(), TemporalType.TIMESTAMP);
                }
                else if(key.equals("endDate")){
                    query.setParameter(key, criteria.getEndDate(), TemporalType.TIMESTAMP);
                }
                else {
                    query.setParameter(key, paramMap.get(key));
                }
        }
        setItemsReturned(query, criteria.getStart(), criteria.getPageSize());
        final List<ServiceLog> resultList = query.getResultList();
        return resultList;
    }
}
