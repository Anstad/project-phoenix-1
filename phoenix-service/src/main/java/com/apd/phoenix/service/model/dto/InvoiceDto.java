package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public abstract class InvoiceDto extends TransactionDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4227928585514804728L;
    private Long databaseId;
    private Date createdDate;
    private String invoiceNumber;
    private BigDecimal amount;
    private List<String> specalInstructions;
    private List<LineItemDto> actualItems;
    private String billingTypeCode;
    private Date dueDate;
    private Date invoiceDate;
    private String exception;

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the invoiceNumber
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * @param invoiceNumber the invoiceNumber to set
     */
    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    /**
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * @return the specalInstructions
     */
    public List<String> getSpecalInstructions() {
        return specalInstructions;
    }

    /**
     * @param specalInstructions the specalInstructions to set
     */
    public void setSpecalInstructions(List<String> specalInstructions) {
        this.specalInstructions = specalInstructions;
    }

    /**
     * @return the billingTypeCode
     */
    public String getBillingTypeCode() {
        return billingTypeCode;
    }

    /**
     * @param billingTypeCode the billingTypeCode to set
     */
    public void setBillingTypeCode(String billingTypeCode) {
        this.billingTypeCode = billingTypeCode;
    }

    /**
     * @return the actualItems
     */
    public List<LineItemDto> getActualItems() {
        return actualItems;
    }

    /**
     * @param actualItems the actualItems to set
     */
    public void setActualItems(List<LineItemDto> actualItems) {
        this.actualItems = actualItems;
    }

    /**
     * @return the invoiceDate
     */
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * @param invoiceDate the invoiceDate to set
     */
    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * @return the dueDate
     */
    public Date getDueDate() {
        return dueDate;
    }

    /**
     * @param dueDate the dueDate to set
     */
    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Long getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(Long databaseId) {
        this.databaseId = databaseId;
    }

    public void setException(String exceptionInput) {
        this.exception = exceptionInput;

    }

    public String getException() {
        return exception;
    }

}
