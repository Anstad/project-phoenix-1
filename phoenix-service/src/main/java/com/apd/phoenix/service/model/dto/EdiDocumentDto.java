/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

/**
 *
 * @author rhc
 * used to populate an email detailing the failure to match an inbound edi documet with an existing order
 * 
 */
public class EdiDocumentDto {

    private String apdPo;
    private String rawMessage;

    /**
     * @return the apdPo
     */
    public String getApdPo() {
        return apdPo;
    }

    /**
     * @param apdPo the apdPo to set
     */
    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    /**
     * @return the rawMessage
     */
    public String getRawMessage() {
        return rawMessage;
    }

    /**
     * @param rawMessage the rawMessage to set
     */
    public void setRawMessage(String rawMessage) {
        this.rawMessage = rawMessage;
    }
}
