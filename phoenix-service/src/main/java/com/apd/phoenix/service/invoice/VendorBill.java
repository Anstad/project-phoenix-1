package com.apd.phoenix.service.invoice;

import java.util.List;

public class VendorBill {

    private List<BilledLineItem> billedItems;

    public List<BilledLineItem> getBilledItems() {
        return billedItems;
    }

    public void setBilledItems(List<BilledLineItem> billedItems) {
        this.billedItems = billedItems;
    }

}
