package com.apd.phoenix.service.model.dto;

import java.io.Serializable;

public class LineItemStatusDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8164732677525502251L;

    private String description;

    private String value;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
