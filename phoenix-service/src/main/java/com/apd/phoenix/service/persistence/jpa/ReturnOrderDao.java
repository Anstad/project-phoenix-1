package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;

/**
 * ReturnOrder DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ReturnOrderDao extends AbstractDao<ReturnOrder> {

    @SuppressWarnings("unchecked")
    public List<ReturnOrder> returnSearch(String raNumber, String poNumber, String login, Date startDate, Date endDate,
            Boolean closed, Boolean reconciled, Boolean unreconciled) {
        String hql = "SELECT DISTINCT returnOrder FROM ReturnOrder AS returnOrder JOIN FETCH returnOrder.order AS customerOrder "
                + "LEFT JOIN FETCH customerOrder.poNumbers AS fetchedPos LEFT JOIN FETCH returnOrder.items AS liXReturns "
                + "LEFT JOIN FETCH liXReturns.lineItem LEFT JOIN customerOrder.poNumbers AS matchPos JOIN customerOrder.user AS "
                + "systemUser "
                + "WHERE (:raNumber IS NULL OR LOWER(:raNumber) LIKE LOWER(returnOrder.raNumber)) "
                + "AND (:poNumber IS NULL OR LOWER(:poNumber) LIKE LOWER(matchPos.value))"
                + "AND (:login IS NULL OR LOWER(:login) LIKE LOWER(systemUser.login))"
                + "AND (:startDate IS NULL OR :startDate < returnOrder.createdDate)"
                + "AND (:endDate IS NULL OR :endDate > returnOrder.createdDate)"
                + "AND (:closed is true OR NOT returnOrder.status = :closedStatus)"
                + "AND (:reconciled is true OR NOT returnOrder.status = :reconciledStatus)"
                + "AND (:unreconciled is true OR NOT returnOrder.status = :unreconciledStatus)";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("raNumber", raNumber);
        query.setParameter("poNumber", poNumber);
        query.setParameter("login", login);
        query.setParameter("startDate", startDate, TemporalType.TIMESTAMP);
        query.setParameter("endDate", endDate, TemporalType.TIMESTAMP);
        query.setParameter("closed", closed);
        query.setParameter("closedStatus", ReturnOrderStatus.CLOSED);
        query.setParameter("reconciled", reconciled);
        query.setParameter("reconciledStatus", ReturnOrderStatus.RECONCILED);
        query.setParameter("unreconciled", unreconciled);
        query.setParameter("unreconciledStatus", ReturnOrderStatus.UNRECONCILED);

        return query.getResultList();
    }

    public void detach(ReturnOrder toDetach) {
        entityManager.detach(toDetach);
    }
}
