package com.apd.phoenix.service.utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialPropertyType;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.dto.CredentialDto;

public class CredentialPropertyUtils extends AbstractPropertyUtils {

    public static boolean equals(CredentialPropertyTypeEnum propertyType, Object value, Credential credentialEntity) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && credentialEntity != null && credentialEntity.getProperties() != null) {
            for (CredentialXCredentialPropertyType property : credentialEntity.getProperties()) {
                if (property.getType().getName().equalsIgnoreCase(propertyType.getValue())) {
                    lhs = normalizeValue(property.getValue());
                    break;
                }
            }
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        return StringUtils.equalsIgnoreCase(lhs, rhs) || semanticEquals(lhs, rhs);
    }

    public static boolean equals(CredentialPropertyTypeEnum propertyType, Object value, CredentialDto credentialDto) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && credentialDto != null && credentialDto.getProperties() != null
                && credentialDto.getProperties().containsKey(propertyType.getValue())) {
            lhs = normalizeValue((String) credentialDto.getProperties().get(propertyType.getValue()));
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        return StringUtils.equalsIgnoreCase(lhs, rhs) || semanticEquals(lhs, rhs);
    }

    public static boolean contains(CredentialPropertyTypeEnum propertyType, Object value, Credential credentialEntity,
            boolean multiValueProperty) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && credentialEntity != null && credentialEntity.getProperties() != null) {
            for (CredentialXCredentialPropertyType property : credentialEntity.getProperties()) {
                if (property.getType().getName().equalsIgnoreCase(propertyType.getValue())) {
                    lhs = normalizeValue(property.getValue());
                    break;
                }
            }
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        else if (StringUtils.isBlank(lhs) && StringUtils.isBlank(rhs)) {
            return true;
        }
        if (multiValueProperty) {
            List<String> elements = new ArrayList<String>();
            for (String element : Arrays.asList(lhs.split(","))) {
                String normalizedElement = normalizeValue(element);
                if (normalizedElement != null) {
                    elements.add(normalizedElement);
                }
            }
            return elements.contains(rhs);
        }
        else {
            return lhs.contains(rhs);
        }
    }

    public static boolean contains(CredentialPropertyTypeEnum propertyType, Object value, CredentialDto credentialDto,
            boolean multiValueProperty) {
        String lhs = null;
        String rhs = normalizeValue(value);
        if (propertyType != null && credentialDto != null && credentialDto.getProperties() != null
                && credentialDto.getProperties().containsKey(propertyType.getValue())) {
            lhs = normalizeValue((String) credentialDto.getProperties().get(propertyType.getValue()));
        }
        if (lhs == null && rhs != null || lhs != null && rhs == null) {
            return false;
        }
        else if (StringUtils.isBlank(lhs) && StringUtils.isBlank(rhs)) {
            return true;
        }
        if (multiValueProperty) {
            List<String> elements = new ArrayList<String>();
            for (String element : Arrays.asList(lhs.split(","))) {
                String normalizedElement = normalizeValue(element);
                if (normalizedElement != null) {
                    elements.add(normalizedElement);
                }
            }
            return elements.contains(rhs);
        }
        else {
            return lhs.contains(rhs);
        }
    }
}
