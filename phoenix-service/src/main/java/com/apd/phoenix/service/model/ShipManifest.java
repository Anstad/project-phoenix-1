package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class ShipManifest implements Serializable, com.apd.phoenix.service.model.Entity{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -6257832300991545636L;

	/** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

	@OneToMany
	private Set<Pick> picks = new HashSet<>();
	
	@Column
	private BigInteger numBoxes;
	
	@Temporal(TemporalType.DATE)
	private Date manifestDate;
	
	@Column
	private boolean isManifested = false;
	
	@Enumerated(EnumType.STRING)
	private ManifestType type;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private CustomerOrder customerOrder;
	
	
	//Ussco
	@Column
	private String tradingPartnerID;
	
	@Column
	private String documentID;
	
	@Column
	private String selectionTypeCode;
	
	@Temporal(TemporalType.DATE)
	private Date dateTimeSent;
	
	@Column
	private String facilityNumber;
	
	@Column
	private String facilityAbbreviation;
	
	@Column
	private String facilityName;
	
	@Column
	private String facilityAddress1;
	
	@Column
	private String facilityAddress2;
	
	@Column
	private String facilityCity;
	
	@Column
	private String facilityState;
	
	@Column
	private String facilityPostalCode;
	
	@Column
	private String orderNumber;
	
	@Column
	private String subOrderNumber;
	
	@Column
	private String carrierCode;
	
	@Column
	private String shipTruckCode;
	
	@Column
	private String shipTruckDockId;
	
	@Column
	private String truckCodeDescription;
	
	@Column
	private String customerNumber;
	
	@Column
	private String customerName;
	
	@Column
	private String customerBarCode;
	
	@Column
	private String customerReferenceData;
	
	@Column
	private String customerRouteDescription;
	
	@Column
	private String customerRouteData;
	
	@Column
	private String customerPurchaseOrderNumber;
	
	@Column
	private String deliveryTypeCode;
	
	@Column
	private String deliveryMethodCode;
	
	@Column
	private String customerAddress1;
	
	@Column
	private String customerAddress2;
	
	@Column
	private String customerCity;
	
	@Column
	private String customerState;
	
	@Column
	private String customerPostalCode;
	
	@Column
	private String endConsumerPurchaseOrderData;
	
	@Column
	private String endConsumerName;
	
	@Column
	private String endConsumerAddress1;
	
	@Column
	private String endConsumerAddress2;
	
	@Column
	private String endConsumerAddress3;
	
	@Column
	private String endConsumerCity;
	
	@Column
	private String endConsumerState;
	
	@Column
	private String endConsumerPostalCode;
	
	@Column
	private String fillFacilityNumber;
	
	@Column
	private String hazardousMaterialIndicator;
	
	@Column
	private String primaryOrderNumber;
	
	@Temporal(TemporalType.DATE)
	private Date dateProcessed;
	
	@Column
	private String specialInstructions1;
	
	@Column
	private String specialInstructions2;
	
	@Column
	private String dealerInformation1;
	
	@Column
	private String dealerInformation2;
	
	@Column
	private String dealerInformation3;
	
	@Column
	private String dealerInformation4;
	
	@Column
	private String dealerInformation5;
	
	@Column
	private String dealerInformation6;
	
	@Column
	private String shippingInformation1;
	
	@Column
	private String shippingInformation2;
	
	@Column
	private String shippingInformation3;
	
	@Column
	private String shippingInformation4;
	
	@Column
	private String shippingInformation5;
	
	@Column
	private String shippingInformation6;
	
	@Column(unique=true)
	private String cartonID;
	
	@Column
	private String bulkCartonIndicator;
	
	@Column
	private String cartonWeight;
	
	@Column
	private String itemPrefix;
	
	@Column
	private String itemStock;
	
	@Temporal(TemporalType.DATE)
	private Date scanDateTime;
	
	@Column
	private String cartonBarcode;
	
	@Temporal(TemporalType.DATE)
	private Date deliveryDate;
	
	public enum ManifestType {
        APD("APD"), USSCO("USSCO");

        private final String label;

        private ManifestType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Set<Pick> getPicks() {
		return picks;
	}

	public void setPicks(Set<Pick> picks) {
		this.picks = picks;
	}

	public BigInteger getNumBoxes() {
		return numBoxes;
	}

	public void setNumBoxes(BigInteger numBoxes) {
		this.numBoxes = numBoxes;
	}

	public Date getManifestDate() {
		return manifestDate;
	}

	public void setManifestDate(Date manifestDate) {
		this.manifestDate = manifestDate;
	}

	public boolean isManifested() {
		return isManifested;
	}

	public void setManifested(boolean isManifested) {
		this.isManifested = isManifested;
	}

	public String getTradingPartnerID() {
		return tradingPartnerID;
	}

	public void setTradingPartnerID(String tradingPartnerID) {
		this.tradingPartnerID = tradingPartnerID;
	}

	public String getDocumentID() {
		return documentID;
	}

	public void setDocumentID(String documentID) {
		this.documentID = documentID;
	}

	public String getSelectionTypeCode() {
		return selectionTypeCode;
	}

	public void setSelectionTypeCode(String selectionTypeCode) {
		this.selectionTypeCode = selectionTypeCode;
	}


	public String getFacilityNumber() {
		return facilityNumber;
	}

	public void setFacilityNumber(String facilityNumber) {
		this.facilityNumber = facilityNumber;
	}

	public String getFacilityAbbreviation() {
		return facilityAbbreviation;
	}

	public void setFacilityAbbreviation(String facilityAbbreviation) {
		this.facilityAbbreviation = facilityAbbreviation;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getFacilityAddress1() {
		return facilityAddress1;
	}

	public void setFacilityAddress1(String facilityAddress1) {
		this.facilityAddress1 = facilityAddress1;
	}

	public String getFacilityAddress2() {
		return facilityAddress2;
	}

	public void setFacilityAddress2(String facilityAddress2) {
		this.facilityAddress2 = facilityAddress2;
	}

	public String getFacilityCity() {
		return facilityCity;
	}

	public void setFacilityCity(String facilityCity) {
		this.facilityCity = facilityCity;
	}

	public String getFacilityState() {
		return facilityState;
	}

	public void setFacilityState(String facilityState) {
		this.facilityState = facilityState;
	}

	public String getFacilityPostalCode() {
		return facilityPostalCode;
	}

	public void setFacilityPostalCode(String facilityPostalCode) {
		this.facilityPostalCode = facilityPostalCode;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getSubOrderNumber() {
		return subOrderNumber;
	}

	public void setSubOrderNumber(String subOrderNumber) {
		this.subOrderNumber = subOrderNumber;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getShipTruckCode() {
		return shipTruckCode;
	}

	public void setShipTruckCode(String shipTruckCode) {
		this.shipTruckCode = shipTruckCode;
	}

	public String getShipTruckDockId() {
		return shipTruckDockId;
	}

	public void setShipTruckDockId(String shipTruckDockId) {
		this.shipTruckDockId = shipTruckDockId;
	}

	public String getTruckCodeDescription() {
		return truckCodeDescription;
	}

	public void setTruckCodeDescription(String truckCodeDescription) {
		this.truckCodeDescription = truckCodeDescription;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}

	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerBarCode() {
		return customerBarCode;
	}

	public void setCustomerBarCode(String customerBarCode) {
		this.customerBarCode = customerBarCode;
	}

	public String getCustomerReferenceData() {
		return customerReferenceData;
	}

	public void setCustomerReferenceData(String customerReferenceData) {
		this.customerReferenceData = customerReferenceData;
	}

	public String getCustomerRouteDescription() {
		return customerRouteDescription;
	}

	public void setCustomerRouteDescription(String customerRouteDescription) {
		this.customerRouteDescription = customerRouteDescription;
	}

	public String getCustomerRouteData() {
		return customerRouteData;
	}

	public void setCustomerRouteData(String customerRouteData) {
		this.customerRouteData = customerRouteData;
	}

	public String getCustomerPurchaseOrderNumber() {
		return customerPurchaseOrderNumber;
	}

	public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
		this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
	}

	public String getDeliveryTypeCode() {
		return deliveryTypeCode;
	}

	public void setDeliveryTypeCode(String deliveryTypeCode) {
		this.deliveryTypeCode = deliveryTypeCode;
	}

	public String getDeliveryMethodCode() {
		return deliveryMethodCode;
	}

	public void setDeliveryMethodCode(String deliveryMethodCode) {
		this.deliveryMethodCode = deliveryMethodCode;
	}

	public String getCustomerAddress1() {
		return customerAddress1;
	}

	public void setCustomerAddress1(String customerAddress1) {
		this.customerAddress1 = customerAddress1;
	}

	public String getCustomerAddress2() {
		return customerAddress2;
	}

	public void setCustomerAddress2(String customerAddress2) {
		this.customerAddress2 = customerAddress2;
	}

	public String getCustomerCity() {
		return customerCity;
	}

	public void setCustomerCity(String customerCity) {
		this.customerCity = customerCity;
	}

	public String getCustomerState() {
		return customerState;
	}

	public void setCustomerState(String customerState) {
		this.customerState = customerState;
	}

	public String getCustomerPostalCode() {
		return customerPostalCode;
	}

	public void setCustomerPostalCode(String customerPostalCode) {
		this.customerPostalCode = customerPostalCode;
	}

	public String getEndConsumerPurchaseOrderData() {
		return endConsumerPurchaseOrderData;
	}

	public void setEndConsumerPurchaseOrderData(String endConsumerPurchaseOrderData) {
		this.endConsumerPurchaseOrderData = endConsumerPurchaseOrderData;
	}

	public String getEndConsumerName() {
		return endConsumerName;
	}

	public void setEndConsumerName(String endConsumerName) {
		this.endConsumerName = endConsumerName;
	}

	public String getEndConsumerAddress1() {
		return endConsumerAddress1;
	}

	public void setEndConsumerAddress1(String endConsumerAddress1) {
		this.endConsumerAddress1 = endConsumerAddress1;
	}

	public String getEndConsumerAddress2() {
		return endConsumerAddress2;
	}

	public void setEndConsumerAddress2(String endConsumerAddress2) {
		this.endConsumerAddress2 = endConsumerAddress2;
	}

	public String getEndConsumerAddress3() {
		return endConsumerAddress3;
	}

	public void setEndConsumerAddress3(String endConsumerAddress3) {
		this.endConsumerAddress3 = endConsumerAddress3;
	}

	public String getEndConsumerCity() {
		return endConsumerCity;
	}

	public void setEndConsumerCity(String endConsumerCity) {
		this.endConsumerCity = endConsumerCity;
	}

	public String getEndConsumerState() {
		return endConsumerState;
	}

	public void setEndConsumerState(String endConsumerState) {
		this.endConsumerState = endConsumerState;
	}

	public String getEndConsumerPostalCode() {
		return endConsumerPostalCode;
	}

	public void setEndConsumerPostalCode(String endConsumerPostalCode) {
		this.endConsumerPostalCode = endConsumerPostalCode;
	}

	public String getFillFacilityNumber() {
		return fillFacilityNumber;
	}

	public void setFillFacilityNumber(String fillFacilityNumber) {
		this.fillFacilityNumber = fillFacilityNumber;
	}

	public String getHazardousMaterialIndicator() {
		return hazardousMaterialIndicator;
	}

	public void setHazardousMaterialIndicator(String hazardousMaterialIndicator) {
		this.hazardousMaterialIndicator = hazardousMaterialIndicator;
	}

	public String getPrimaryOrderNumber() {
		return primaryOrderNumber;
	}

	public void setPrimaryOrderNumber(String primaryOrderNumber) {
		this.primaryOrderNumber = primaryOrderNumber;
	}


	public String getSpecialInstructions1() {
		return specialInstructions1;
	}

	public void setSpecialInstructions1(String specialInstructions1) {
		this.specialInstructions1 = specialInstructions1;
	}

	public String getSpecialInstructions2() {
		return specialInstructions2;
	}

	public void setSpecialInstructions2(String specialInstructions2) {
		this.specialInstructions2 = specialInstructions2;
	}

	public String getDealerInformation1() {
		return dealerInformation1;
	}

	public void setDealerInformation1(String dealerInformation1) {
		this.dealerInformation1 = dealerInformation1;
	}

	public String getDealerInformation2() {
		return dealerInformation2;
	}

	public void setDealerInformation2(String dealerInformation2) {
		this.dealerInformation2 = dealerInformation2;
	}

	public String getDealerInformation3() {
		return dealerInformation3;
	}

	public void setDealerInformation3(String dealerInformation3) {
		this.dealerInformation3 = dealerInformation3;
	}

	public String getDealerInformation4() {
		return dealerInformation4;
	}

	public void setDealerInformation4(String dealerInformation4) {
		this.dealerInformation4 = dealerInformation4;
	}

	public String getDealerInformation5() {
		return dealerInformation5;
	}

	public void setDealerInformation5(String dealerInformation5) {
		this.dealerInformation5 = dealerInformation5;
	}

	public String getDealerInformation6() {
		return dealerInformation6;
	}

	public void setDealerInformation6(String dealerInformation6) {
		this.dealerInformation6 = dealerInformation6;
	}

	public String getShippingInformation1() {
		return shippingInformation1;
	}

	public void setShippingInformation1(String shippingInformation1) {
		this.shippingInformation1 = shippingInformation1;
	}

	public String getShippingInformation2() {
		return shippingInformation2;
	}

	public void setShippingInformation2(String shippingInformation2) {
		this.shippingInformation2 = shippingInformation2;
	}

	public String getShippingInformation3() {
		return shippingInformation3;
	}

	public void setShippingInformation3(String shippingInformation3) {
		this.shippingInformation3 = shippingInformation3;
	}

	public String getShippingInformation4() {
		return shippingInformation4;
	}

	public void setShippingInformation4(String shippingInformation4) {
		this.shippingInformation4 = shippingInformation4;
	}

	public String getShippingInformation5() {
		return shippingInformation5;
	}

	public void setShippingInformation5(String shippingInformation5) {
		this.shippingInformation5 = shippingInformation5;
	}

	public String getShippingInformation6() {
		return shippingInformation6;
	}

	public void setShippingInformation6(String shippingInformation6) {
		this.shippingInformation6 = shippingInformation6;
	}

	public String getCartonID() {
		return cartonID;
	}

	public void setCartonID(String cartonID) {
		this.cartonID = cartonID;
	}

	public String getBulkCartonIndicator() {
		return bulkCartonIndicator;
	}

	public void setBulkCartonIndicator(String bulkCartonIndicator) {
		this.bulkCartonIndicator = bulkCartonIndicator;
	}

	public String getCartonWeight() {
		return cartonWeight;
	}

	public void setCartonWeight(String cartonWeight) {
		this.cartonWeight = cartonWeight;
	}

	public String getItemPrefix() {
		return itemPrefix;
	}

	public void setItemPrefix(String itemPrefix) {
		this.itemPrefix = itemPrefix;
	}

	public String getItemStock() {
		return itemStock;
	}

	public void setItemStock(String itemStock) {
		this.itemStock = itemStock;
	}

	public String getCartonBarcode() {
		return cartonBarcode;
	}

	public void setCartonBarcode(String cartonBarcode) {
		this.cartonBarcode = cartonBarcode;
	}

	public ManifestType getType() {
		return type;
	}

	public void setType(ManifestType type) {
		this.type = type;
	}

	public Date getDateTimeSent() {
		return dateTimeSent;
	}

	public void setDateTimeSent(Date dateTimeSent) {
		this.dateTimeSent = dateTimeSent;
	}

	public Date getDateProcessed() {
		return dateProcessed;
	}

	public void setDateProcessed(Date dateProcessed) {
		this.dateProcessed = dateProcessed;
	}

	public Date getScanDateTime() {
		return scanDateTime;
	}

	public void setScanDateTime(Date scanDateTime) {
		this.scanDateTime = scanDateTime;
	}

	public CustomerOrder getCustomerOrder() {
		return customerOrder;
	}

	public void setCustomerOrder(CustomerOrder customerOrder) {
		this.customerOrder = customerOrder;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
	
}
