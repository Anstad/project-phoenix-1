package com.apd.phoenix.service.persistence.jpa;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.PendingWorkflowCommand;

@Stateless
@LocalBean
public class PendingWorkflowCommandDao extends AbstractDao<PendingWorkflowCommand> {

    public List<PendingWorkflowCommand> getExpiredCommands(Date date) {
        String hql = "SELECT pwcommand FROM PendingWorkflowCommand AS pwcommand"
                + " WHERE pwcommand.expires < :date AND pwcommand.executed = false";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("date", date);
        setItemsReturned(query, 0, 0);

        return (List<PendingWorkflowCommand>) query.getResultList();
    }
}
