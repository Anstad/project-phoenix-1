package com.apd.phoenix.service.message.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;

public class DefaultMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMessageSender.class);

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        LOGGER.info("Sending Message");
        logMessage(message, LOGGER);
        return true;
    }

}
