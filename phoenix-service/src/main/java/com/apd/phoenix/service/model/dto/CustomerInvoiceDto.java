/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

public class CustomerInvoiceDto extends InvoiceDto {

    private static final long serialVersionUID = -4756078352696541349L;
    private ShipmentDto shipment;
    private String apdPo;
    private String receiverCode;
    private String destination;
    private String ediVersion;
    private String partnerId;

    public ShipmentDto getShipment() {
        return shipment;
    }

    public void setShipment(ShipmentDto shipment) {
        this.shipment = shipment;
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public String getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getEdiVersion() {
        return ediVersion;
    }

    public void setEdiVersion(String ediVersion) {
        this.ediVersion = ediVersion;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }
}
