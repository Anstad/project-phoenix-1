package com.apd.phoenix.service.business;

import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.PendingWorkflowCommand;
import com.apd.phoenix.service.persistence.jpa.PendingWorkflowCommandDao;

@Stateless
@LocalBean
public class PendingWorkflowCommandBp extends AbstractBp<PendingWorkflowCommand> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PendingWorkflowCommandDao dao) {
        this.dao = dao;
    }

    public List<PendingWorkflowCommand> getExpiredCommands(Date date) {
        return ((PendingWorkflowCommandDao) dao).getExpiredCommands(date);
    }
}
