package com.apd.phoenix.service.business;

import javax.ejb.ApplicationException;
import com.apd.phoenix.service.model.SyncItemResult;

//This annotation is used to ensure that the following exception will not be logged, but will still cause the transaction to be set as rollback
@ApplicationException(rollback = true)
public class ItemRollbackException extends RuntimeException {

    private static final long serialVersionUID = 1902915365267621283L;

    private SyncItemResult result;

    public SyncItemResult getResult() {
        return result;
    }

    public void setResult(SyncItemResult result) {
        this.result = result;
    }
}
