/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

/**
 * The Class CategoryMarkup.
 */
@Entity
@XmlRootElement
@Audited
public class CategoryMarkup implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -7863066215112602622L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @NotAudited
    private ItemCategory category;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CategoryMarkup) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The markup percentage. */
    @Column
    private BigDecimal markupPercentage;

    /**
     * Gets the markup percentage.
     *
     * @return the markup percentage
     */
    public BigDecimal getMarkupPercentage() {
        return this.markupPercentage;
    }

    /**
     * Sets the markup percentage.
     *
     * @param markupPercentage the new markup percentage
     */
    public void setMarkupPercentage(final BigDecimal markupPercentage) {
        this.markupPercentage = markupPercentage;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (markupPercentage != null)
            result += markupPercentage.toPlainString();
        return result;
    }

    /** The catalog. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog catalog;

    /**
     * Gets the catalog.
     *
     * @return the catalog
     */
    public Catalog getCatalog() {
        return this.catalog;
    }

    /**
     * Sets the catalog.
     *
     * @param catalog the new catalog
     */
    public void setCatalog(final Catalog catalog) {
        this.catalog = catalog;
    }

    public ItemCategory getCategory() {
        return this.category;
    }

    public void setCategory(final ItemCategory category) {
        this.category = category;
    }

}