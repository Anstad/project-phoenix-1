package com.apd.phoenix.service.model.listener;

import java.util.Date;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import com.apd.phoenix.service.model.Item;

/**
 *
 * @author RHC
 */
public class ItemListener extends SolrEntityListener {

    @PrePersist
    @PreUpdate
    public void preUpdateEventListener(Item entity) {
        entity.setLastModified(new Date());
    }

    @PostPersist
    @PostUpdate
    public void addUpdateItem(final Item entity) throws NamingException, JMSException {
        doSolrDocumentAction(entity.getId(), SolrDocumentAction.ITEM_UPDATE);
    }

    public static void addUpdateSolrDocument(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.ITEM_UPDATE);
    }

    @PostRemove
    public void deleteItem(final Item entity) throws JMSException, NamingException {
        doSolrDocumentAction(entity.getId(), SolrDocumentAction.ITEM_DELETE);
    }
}
