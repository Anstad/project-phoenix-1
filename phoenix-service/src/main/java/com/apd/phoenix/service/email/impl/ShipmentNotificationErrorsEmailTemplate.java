package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class ShipmentNotificationErrorsEmailTemplate extends EmailTemplate<ShipmentDto> {

    private static final String TEMPLATE = "shipment.notification.errors";

    @Override
	public String createBody(ShipmentDto shipmentDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		params.put("apdPo", shipmentDto.getCustomerOrderDto().retrieveApdPoNumber().getValue());
		params.put("trackingNumber", shipmentDto.getTrackingNumber());
		List<LineItemXShipmentDto> lineItemDtos = new ArrayList<>(shipmentDto.getLineItemXShipments());
		params.put("items", lineItemDtos);

		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(ShipmentDto shipmentDto) {
        // no attachment for this message type
        return null;
    }
}
