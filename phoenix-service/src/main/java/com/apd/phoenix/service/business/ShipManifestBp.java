package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ShipManifest;
import com.apd.phoenix.service.persistence.jpa.ShipManifestDao;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ShipManifestBp extends AbstractBp<ShipManifest> {

    @Inject
    public void init(ShipManifestDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    public Map<String,List<ShipManifest>> getNewShipmentManifestsByZip() {
		Map<String,List<ShipManifest>> results = new HashMap<String,List<ShipManifest>>();
		List<ShipManifest> shipManifests = ((ShipManifestDao)dao).getNewManifestEntries();
		if(shipManifests == null) {
			return results;
		}
		for(ShipManifest shipManifest : shipManifests) {
			if(shipManifest.getCustomerOrder() != null 
					&& shipManifest.getCustomerOrder().getAddress() != null 
					&& shipManifest.getCustomerOrder().getAddress().getZip() != null
					&& !shipManifest.getCustomerOrder().getAddress().getZip().isEmpty()) {
				String zip = shipManifest.getCustomerOrder().getAddress().getZip();
				if(results.get(zip) == null) {
					List<ShipManifest> newList = new ArrayList<>();
					newList.add(shipManifest);
					results.put(zip, newList);
				}
				else {
					List<ShipManifest> currentList = results.get(zip);
					currentList.add(shipManifest);
				}
			}
			else {
				LOG.error("Missing zip code for order");
			}
			
		}
		
		return results;
	}
}
