package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXVendor;
import com.apd.phoenix.service.model.Vendor;

@Stateless
@LocalBean
public class CredentialXVendorDao extends AbstractDao<CredentialXVendor> {

    @SuppressWarnings("unchecked")
    public String getAccountIdByCredentialAndVendor(Vendor vendor, Credential credential) {
        if (vendor == null || credential == null) {
            return null;
        }
        String hql = "SELECT cxv.accountId FROM CredentialXVendor as cxv WHERE cxv.vendor.id=:vid AND cxv.credential.id=:cid";
        Query query = entityManager.createQuery(hql);
        query.setParameter("vid", vendor.getId());
        query.setParameter("cid", credential.getId());
        List<String> result = query.getResultList();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

    @SuppressWarnings("unchecked")
    public String getAccountIdByCredentialAndVendorName(String vendorName, Credential credential) {
        String hql = "SELECT cxv.accountId FROM CredentialXVendor as cxv WHERE cxv.vendor.name=:vendorName AND cxv.credential.id=:cid";
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorName", vendorName);
        query.setParameter("cid", credential.getId());
        List<String> result = query.getResultList();
        if (result == null || result.isEmpty()) {
            return null;
        }
        return result.get(0);
    }

}
