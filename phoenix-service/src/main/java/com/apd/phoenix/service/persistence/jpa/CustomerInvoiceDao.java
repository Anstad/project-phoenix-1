package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.Invoice;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Session Bean implementation class CustomerInvoiceDao
 */
@Stateless
@LocalBean
public class CustomerInvoiceDao extends AbstractDao<CustomerInvoice> {

    public List<CustomerInvoice> findAllByApdPo(String apdPo) {
        String hql = "SELECT distinct i FROM CustomerInvoice as i LEFT JOIN  i.shipment.order as o left join o.poNumbers as po"
                + " WHERE po.type.name = 'APD' AND po.value =:apdPo";
        Query query = entityManager.createQuery(hql);
        query.setParameter("apdPo", apdPo);
        List<CustomerInvoice> invoices = (List<CustomerInvoice>) query.getResultList();
        if (invoices == null) {
            return new ArrayList<CustomerInvoice>();
        }
        else {
            return invoices;
        }
    }

}
