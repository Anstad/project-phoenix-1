package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;
import com.apd.phoenix.service.model.TaxType.TaxTypeEnum;

/**
 * This field is used to hold the different tax rates, for a certain location
 * 
 * @author RHC
 *
 */
@Entity
@FixedLengthRecord(length = 182, paddingChar = ' ')
public class TaxRate implements Serializable, com.apd.phoenix.service.model.Entity {

    public enum LocationInCity {
        I("Inside"), O("Outside"), B("Both");

        private final String label;

        private LocationInCity(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    @DataField(pos = 1, length = 5)
    private String zip;

    @Column
    @DataField(pos = 6, length = 28, trim = true)
    private String city;

    @Column
    @DataField(pos = 34, length = 25, trim = true)
    private String county;

    @Column
    @DataField(pos = 59, length = 2)
    private String state;

    @Column
    @DataField(pos = 61, length = 1, trim = true)
    private String countyDefault;

    @Column
    @DataField(pos = 62, length = 1, trim = true)
    private String generalDefault;

    @Column
    @DataField(pos = 63, length = 5)
    private String countyFips;

    @Column(scale = 6)
    @DataField(pos = 68, length = 8, precision = 6)
    private BigDecimal stateSales;

    @Column(scale = 6)
    @DataField(pos = 76, length = 8, precision = 6)
    private BigDecimal countySales;

    @Column(scale = 6)
    @DataField(pos = 84, length = 8, precision = 6)
    private BigDecimal countyLocalSales;

    @Column(scale = 6)
    @DataField(pos = 92, length = 8, precision = 6)
    private BigDecimal citySales;

    @Column(scale = 6)
    @DataField(pos = 100, length = 8, precision = 6)
    private BigDecimal cityLocalSales;

    @Column(scale = 6)
    @DataField(pos = 108, length = 8, precision = 6)
    private BigDecimal combinedSales;

    @Column(scale = 6)
    @DataField(pos = 116, length = 8, precision = 6)
    private BigDecimal stateUse;

    @Column(scale = 6)
    @DataField(pos = 124, length = 8, precision = 6)
    private BigDecimal countyUse;

    @Column(scale = 6)
    @DataField(pos = 132, length = 8, precision = 6)
    private BigDecimal countyLocalUse;

    @Column(scale = 6)
    @DataField(pos = 140, length = 8, precision = 6)
    private BigDecimal cityUse;

    @Column(scale = 6)
    @DataField(pos = 148, length = 8, precision = 6)
    private BigDecimal cityLocalUse;

    @Column(scale = 6)
    @DataField(pos = 156, length = 8, precision = 6)
    private BigDecimal combinedUse;

    @Temporal(TemporalType.DATE)
    @DataField(pos = 164, length = 8, pattern = "yyyyMMdd")
    private Date effectiveDate;

    @Temporal(TemporalType.DATE)
    private Date expireDate;

    @Column
    @DataField(pos = 172, length = 10)
    private String geocode;

    @Column
    @DataField(pos = 182, length = 1)
    private String locationInCity;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((TaxRate) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(final String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getCounty() {
        return this.county;
    }

    public void setCounty(final String county) {
        this.county = county;
    }

    public String getState() {
        return this.state;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public String getCountyDefault() {
        return countyDefault;
    }

    public void setCountyDefault(String countyDefault) {
        this.countyDefault = countyDefault;
    }

    public String getGeneralDefault() {
        return generalDefault;
    }

    public void setGeneralDefault(String generalDefault) {
        this.generalDefault = generalDefault;
    }

    public String getCountyFips() {
        return this.countyFips;
    }

    public void setCountyFips(final String countyFips) {
        this.countyFips = countyFips;
    }

    public BigDecimal getStateSales() {
        return this.stateSales;
    }

    public void setStateSales(final BigDecimal stateSales) {
        this.stateSales = stateSales;
    }

    public BigDecimal getCountySales() {
        return this.countySales;
    }

    public void setCountySales(final BigDecimal countySales) {
        this.countySales = countySales;
    }

    public BigDecimal getCountyLocalSales() {
        return this.countyLocalSales;
    }

    public void setCountyLocalSales(final BigDecimal countyLocalSales) {
        this.countyLocalSales = countyLocalSales;
    }

    public BigDecimal getCitySales() {
        return this.citySales;
    }

    public void setCitySales(final BigDecimal citySales) {
        this.citySales = citySales;
    }

    public BigDecimal getCityLocalSales() {
        return this.cityLocalSales;
    }

    public void setCityLocalSales(final BigDecimal cityLocalSales) {
        this.cityLocalSales = cityLocalSales;
    }

    public BigDecimal getCombinedSales() {
        return this.combinedSales;
    }

    public void setCombinedSales(final BigDecimal combinedSales) {
        this.combinedSales = combinedSales;
    }

    public BigDecimal getStateUse() {
        return this.stateUse;
    }

    public void setStateUse(final BigDecimal stateUse) {
        this.stateUse = stateUse;
    }

    public BigDecimal getCountyUse() {
        return this.countyUse;
    }

    public void setCountyUse(final BigDecimal countyUse) {
        this.countyUse = countyUse;
    }

    public BigDecimal getCountyLocalUse() {
        return this.countyLocalUse;
    }

    public void setCountyLocalUse(final BigDecimal countyLocalUse) {
        this.countyLocalUse = countyLocalUse;
    }

    public BigDecimal getCityUse() {
        return this.cityUse;
    }

    public void setCityUse(final BigDecimal cityUse) {
        this.cityUse = cityUse;
    }

    public BigDecimal getCityLocalUse() {
        return this.cityLocalUse;
    }

    public void setCityLocalUse(final BigDecimal cityLocalUse) {
        this.cityLocalUse = cityLocalUse;
    }

    public BigDecimal getCombinedUse() {
        return this.combinedUse;
    }

    public void setCombinedUse(final BigDecimal combinedUse) {
        this.combinedUse = combinedUse;
    }

    public Date getEffectiveDate() {
        return this.effectiveDate;
    }

    public void setEffectiveDate(final Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setEffectiveDateString(final String effectiveDate) {
        int year = Integer.parseInt(effectiveDate.substring(0, 4));
        int month = Integer.parseInt(effectiveDate.substring(4, 6));
        int date = Integer.parseInt(effectiveDate.substring(6, 8));
        this.setEffectiveDate((new GregorianCalendar(year, month - 1, date)).getTime());
    }

    public Date getExpireDate() {
        return this.expireDate;
    }

    public void setExpireDate(final Date expireDate) {
        this.expireDate = expireDate;
    }

    public String getGeocode() {
        return this.geocode;
    }

    public void setGeocode(final String geocode) {
        this.geocode = geocode;
    }

    public String getLocationInCity() {
        return locationInCity;
    }

    public void setLocationInCity(String locationInCity) {
        this.locationInCity = locationInCity;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (zip != null && !zip.trim().isEmpty())
            result += ", zip: " + zip;
        if (city != null && !city.trim().isEmpty())
            result += ", city: " + city;
        if (county != null && !county.trim().isEmpty())
            result += ", county: " + county;
        if (state != null && !state.trim().isEmpty())
            result += ", state: " + state;
        result += ", countyDefault: " + countyDefault;
        result += ", generalDefault: " + generalDefault;
        if (countyFips != null && !countyFips.trim().isEmpty())
            result += ", countyFips: " + countyFips;
        if (stateSales != null)
            result += ", stateSales: " + stateSales;
        if (countySales != null)
            result += ", countySales: " + countySales;
        if (countyLocalSales != null)
            result += ", countyLocalSales: " + countyLocalSales;
        if (citySales != null)
            result += ", citySales: " + citySales;
        if (cityLocalSales != null)
            result += ", cityLocalSales: " + cityLocalSales;
        if (combinedSales != null)
            result += ", combinedSales: " + combinedSales;
        if (stateUse != null)
            result += ", stateUse: " + stateUse;
        if (countyUse != null)
            result += ", countyUse: " + countyUse;
        if (countyLocalUse != null)
            result += ", countyLocalUse: " + countyLocalUse;
        if (cityUse != null)
            result += ", cityUse: " + cityUse;
        if (cityLocalUse != null)
            result += ", cityLocalUse: " + cityLocalUse;
        if (combinedUse != null)
            result += ", combinedUse: " + combinedUse;
        if (geocode != null && !geocode.trim().isEmpty())
            result += ", geocode: " + geocode;
        return result;
    }

    public static class TaxRateComparator implements Comparator<TaxRate> {

        @Override
        public int compare(TaxRate rate0, TaxRate rate1) {

            if (!StringUtils.isBlank(rate0.getGeocode()) && !StringUtils.isBlank(rate1.getGeocode())) {
                return rate0.getGeocode().compareTo(rate1.getGeocode());
            }

            return (new GenericComparator()).compare(rate0, rate1);
        }
    }

    public BigDecimal getFromTaxType(TaxTypeEnum taxType) {
        if (taxType == null) {
            return BigDecimal.ZERO;
        }
        switch (taxType) {
            case CITY:
                return this.citySales;
            case CITY_LOCAL:
                return this.cityLocalSales;
            case COUNTY:
                return this.countySales;
            case COUNTY_LOCAL:
                return this.countyLocalSales;
            case STATE:
                return this.stateSales;
            default:
                return BigDecimal.ZERO;
        }
    }
}