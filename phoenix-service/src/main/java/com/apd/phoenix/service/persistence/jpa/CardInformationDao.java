package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.CardInformation;

/**
 * Session Bean implementation class CardInformationDao
 */
@Stateless
@LocalBean
public class CardInformationDao extends AbstractDao<CardInformation> {

}
