package com.apd.phoenix.service.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;

@Dependent
public class CxmlMessageUtils {

    public static final String CXML_VERSION = "1.2.024";

    public static final String DEFAULT_LOCALE = "en-US";

    // Original DF: "yyyy-MM-dd'T'HH:mm:ssXXX"
    protected static final String ISO_8601_DATETIME_FORMAT = "YYYY-MM-dd'T'hh:mm:ss-hh:mm";
    protected static final String DEFAULT_TIME_ZONE = "UTC";
    protected static final String DEFAULT_HOSTNAME = "apdmarketplace.com";

    @Inject
    SequenceDao sequenceDao;

    public static String generateTimeStamp() {
        return generateTimeStamp(new Date());
    }

    public static String generateTimeStamp(Date date) {
        TimeZone tz = TimeZone.getTimeZone(DEFAULT_TIME_ZONE);
        SimpleDateFormat iso8601DF = new SimpleDateFormat(ISO_8601_DATETIME_FORMAT);
        iso8601DF.setTimeZone(tz);
        return iso8601DF.format(date);
    }

    public String generatePayloadId() {
        Long interchangeId = sequenceDao.nextVal(Sequence.INTERCHANGE_CONTROL_ID);
        return generateTimeStamp() + "." + interchangeId + "@"
                + System.getProperty("jboss.qualified.hostname", DEFAULT_HOSTNAME);
    }

    public String generationTransactionId() {
        Long transactionId = sequenceDao.nextVal(Sequence.TRANSACTION_CONTROL_ID);
        return "trans" + transactionId;
    }

}
