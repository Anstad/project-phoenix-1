package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.PasswordResetEntry;

/**
 * Session Bean implementation class AccountDao
 */
@Stateless
@LocalBean
public class PasswordResetEntryDao extends AbstractDao<PasswordResetEntry> {
}
