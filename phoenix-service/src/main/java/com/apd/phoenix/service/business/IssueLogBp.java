package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.IssueLog;
import com.apd.phoenix.service.persistence.jpa.IssueLogDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

@Stateless
@LocalBean
public class IssueLogBp extends AbstractBp<IssueLog> {

    private static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";

    @Inject
    IssueLogDao issueLogDao;

    @Inject
    private SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    @Inject
    public void initDao(IssueLogDao dao) {
        this.dao = dao;
    }

    public List<IssueLog> getIssueLogs(IssueLogSearchCriteria criteria) {
        List<IssueLog> coList;

        String user = securityContext.getUserLogin();
        ;
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criteria.setAccountWhiteList(systemUserDao.getAccountIds(user));
            coList = ((IssueLogDao) dao).getIssueLogs(criteria);
        }
        else {
            coList = ((IssueLogDao) dao).getIssueLogs(criteria);
        }

        if (coList == null) {
            coList = new ArrayList<IssueLog>();
        }

        return coList;
    }
}
