package com.apd.phoenix.service.message.api;

import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.impl.EDIMessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;

public abstract class MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    protected ConnectionFactory connectionFactory;

    @Inject
    protected MessageUtils messageUtils;

    @Inject
    protected SequenceDao sequenceDao;

    protected Queue outboundPurchaseOrderQueue;

    protected Queue outboundPurchaseOrderQueueResend;

    protected Queue outboundShipNoticeQueue;

    protected Queue outboundFunctionalAckQueueResend;

    protected Queue outboundInvoiceQueue;

    protected Queue outboundCreditInvoiceQueue;

    protected Queue outboundOrderAckQueue;

    protected Queue outboundShipConfirmationQueue;

    protected Queue jumptrackManifestQueue;

    protected Queue jumptrackStopNotificationQueue;

    @SuppressWarnings("static-method")
    protected void logMessage(Message message, Logger logger) {
        if (logger.isDebugEnabled()) {
            byte fileContent[] = new byte[(int) message.getMetadata().getContentLength()];
            try {
                message.getContent().reset();
                message.getContent().read(fileContent);
                message.getContent().reset();
            }
            catch (IOException e) {
                logger.debug("IO Exception when logging message", e);
            }
            logger.debug(new String(fileContent));
        }
    }

    public Message sendPurchaseOrder(PurchaseOrderDto purchaseOrderDto) {
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundPurchaseOrderQueue);
                messageProducer.send(createPurchaseOrderMessage(msession, purchaseOrderDto));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating vendor purchase order message", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after vendor purchase order message", e);
        }
        return null;
    }

    public Message sendPOAcknowledgement(POAcknowledgementDto poAcknowledgementDto) {
        return sendPOAcknowledgement(poAcknowledgementDto, Boolean.FALSE);
    }

    public Message sendPOAcknowledgement(POAcknowledgementDto poAcknowledgementDto, Boolean resend) {
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundOrderAckQueue);
                messageProducer.send(createPOAcknowledgementMessage(msession, poAcknowledgementDto, resend));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating po ack", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after po ack", e);
        }
        return null;
    }

    public Message sendShipmentNotice(ShipmentDto shipmentDto) {
        return sendShipmentNotice(shipmentDto, Boolean.FALSE);
    }

    public Message sendShipmentNotice(ShipmentDto shipmentDto, Boolean resend) {
        LOGGER.info("Sending Shipment");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundShipNoticeQueue);
                javax.jms.Message message = createShipmentNoticeMessage(msession, shipmentDto, resend);
                if (this instanceof EDIMessageSender) {
                    LOGGER.info("sendShipmentNotice setting Delay for EDI");
                    message.setLongProperty("AMQ_SCHEDULED_DELAY", 86400000);
                }
                messageProducer.send(message);
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating shipment notice", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after shipment notice", e);
        }
        return null;
    }

    public Message sendInvoice(CustomerInvoiceDto invoiceDto) {
        return sendInvoice(invoiceDto, Boolean.FALSE);
    }

    public Message sendInvoice(CustomerInvoiceDto invoiceDto, Boolean resend) {
        LOGGER.info("Sending Customer Invoice");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundInvoiceQueue);
                javax.jms.Message message = createInvoiceMessage(msession, invoiceDto, resend);
                if (this instanceof EDIMessageSender) {
                    LOGGER.info("sendInvoice setting Delay for EDI");
                    message.setLongProperty("AMQ_SCHEDULED_DELAY", 172800000);
                }
                messageProducer.send(message);
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating invoice", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after invoice", e);
        }
        return null;
    }

    public Message sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto) {
        return sendCreditInvoice(customerCreditInvoiceDto, Boolean.FALSE);
    }

    public Message sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, Boolean resend) {
        LOGGER.info("Sending Credit Invoice.");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundInvoiceQueue);
                messageProducer.send(createCreditInvoiceMessage(msession, customerCreditInvoiceDto, resend));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating credit invoice", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after credit invoice", e);
        }
        return null;
    }

    public Message sendShipConfirmation(List<PickDto> pickList) {
        LOGGER.info("Sending ship confirmation.");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundShipConfirmationQueue);
                messageProducer.send(createShipConfirmationMessage(msession, pickList));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating ship confirmation", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after ship confirmation", e);
        }
        return null;
    }

    public Message sendCardNotification(CardNotificationDto cardNotificationDto) {
        LOGGER.info("Sending card notification.");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(outboundShipConfirmationQueue);
                messageProducer.send(createCardNotification(msession, cardNotificationDto));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating card notification", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after card notification", e);
        }
        return null;
    }

    public Message sendJumptrackStopNotificationRequest(StopNotificationRequestDto stopNotificationRequestDto) {
        LOGGER.info("Sending jumptrack stop notification.");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(jumptrackStopNotificationQueue);
                messageProducer
                        .send(createJumptrackStopNotificationRequestMessage(msession, stopNotificationRequestDto));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating jumptrack stop notification request", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after jumptrack stop notification request", e);
        }
        return null;
    }

    public Message sendJumptrackManifest(ManifestRequestDto manifestRequestDto) {
        LOGGER.info("Sending jumptrack ship manifest notification.");
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(jumptrackManifestQueue);
                messageProducer.send(createJumptrackManifestMessage(msession, manifestRequestDto));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating jumptrack manifest", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after jumptrack manifest", e);
        }
        return null;
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createPurchaseOrderMessage(Session session, PurchaseOrderDto poDto) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createPOAcknowledgementMessage(Session session, POAcknowledgementDto poAcknowledgementDto) {
        return createPOAcknowledgementMessage(session, poAcknowledgementDto, Boolean.FALSE);
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createPOAcknowledgementMessage(Session session, POAcknowledgementDto poAcknowledgementDto,
            Boolean resend) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createShipmentNoticeMessage(Session session, ShipmentDto poDto) {
        return createShipmentNoticeMessage(session, poDto, Boolean.FALSE);
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createShipmentNoticeMessage(Session session, ShipmentDto poDto, Boolean resend) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createInvoiceMessage(Session session, CustomerInvoiceDto poDto) {
        return createInvoiceMessage(session, poDto, Boolean.FALSE);
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createInvoiceMessage(Session session, CustomerInvoiceDto poDto, Boolean resend) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createCreditInvoiceMessage(Session session, CustomerCreditInvoiceDto creditInvoiceDto) {
        return createCreditInvoiceMessage(session, creditInvoiceDto, Boolean.FALSE);
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createCreditInvoiceMessage(Session session, CustomerCreditInvoiceDto creditInvoiceDto,
            Boolean resend) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createShipConfirmationMessage(Session session, List<PickDto> pickList) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createCardNotification(Session msession, CardNotificationDto cardNotificationDto) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createJumptrackStopNotificationRequestMessage(Session session,
            StopNotificationRequestDto stopNotificationRequestDto) {
        throw new NotImplementedException();
    }

    @SuppressWarnings("static-method")
    public javax.jms.Message createJumptrackManifestMessage(Session session, ManifestRequestDto manifestRequestDto) {
        throw new NotImplementedException();
    }

    public abstract boolean sendMessage(Message message, EventType eventType);

    public boolean sendMarquetteInvoice(List<InvoiceDto> invoice, Boolean credit) {
        throw new NotImplementedException();

    }
}
