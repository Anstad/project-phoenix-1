package com.apd.phoenix.service.catalog;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogCsvBp;

@Stateless
public class CatalogCsvPurge {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogCsvPurge.class);

    @Inject
    CatalogCsvBp catalogCsvBp;

    public void purgeOldRecords(long catalogId) {
        List<String> correlationIds = catalogCsvBp.getCorrelationId(catalogId);
        for (String correlationId : correlationIds) {
            catalogCsvBp.removeRecords(correlationId);
        }
    }
}
