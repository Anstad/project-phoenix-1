package com.apd.phoenix.service.model.dto;

public class NoteErrorsDto {

    private String noteRefCode; // REF error code
    private String noteRefDescription; // REF error description

    public String getNoteRefCode() {
        return noteRefCode;
    }

    public void setNoteRefCode(String noteRefCode) {
        this.noteRefCode = noteRefCode;
    }

    public String getNoteRefDescription() {
        return noteRefDescription;
    }

    public void setNoteRefDescription(String noteRefDescription) {
        this.noteRefDescription = noteRefDescription;
    }
}
