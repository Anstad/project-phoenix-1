package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for storing miscellaneous shipping data with addresses
 * @author nreidelb
 *
 */
@Entity
public class MiscShipTo implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final Logger LOGGER = LoggerFactory.getLogger(MiscShipTo.class);
    private static final long serialVersionUID = 1590678180341434170L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Column
    private String addressId;

    @Column
    private String addressId36;

    @Column
    private String contactName;

    @Column
    private String desktop;

    @Column
    private String apdInterchangeId;

    //this field is the string value of the assigned Department entity
    @Column
    private String department;

    //this field is what is mapped to the "dept" extrinsic field
    @Column
    private String dept;

    @Column
    private String carrierRouting;

    @Column
    private String deliveryDate;

    @Column
    private String facility;

    @Column
    private String fedexNumber;

    @Column
    private String locationId;

    @Column
    private String mailStop;

    @Column
    private String nowl;

    //Possibly pole number
    @Column
    private String pol;

    @Column
    private String solomonId;

    //See CustomerOrderBp.getUsAccountNumber()
    @Column
    private String usAccount;

    @Column
    private String headerComments;

    @Column
    private String oldRelease;

    @Column
    private String orderEmail;

    @Column
    private String street2;

    @Column
    private String street3;

    @Column
    private String airportCode;

    @Column
    private String ediID;

    @Column
    private String customerShipToID;

    @Column
    private String storeNumber;

    @Column
    private String glnID;

    @Column
    private String unit;

    @Column
    private String locations;

    @Column
    private String deliverToName;

    @Column
    private String requesterName;

    @Column
    private String requesterPhone;

    @Column
    private String lob;

    @Column
    private String division;

    @Column
    private String dcNumber;

    @Column
    private String financeNumber;

    @Column
    private String glNumber;

    @Column
    private String Area;

    @Column
    private String nameOfHospital;

    @Column
    private String purchasingGroup;

    @Column
    private String district;

    @Column
    private String contractingOfficer;

    @Column
    private String manager;

    @Column
    private String carrierName;

    //Whether the sum of all line items sent through USPS is under 13 ounces
    @Column
    private String uspsUnder13Oz;

    //Only used for manual orders so that a vendor can be specified at the order rather than lineitem level
    @Column
    private String vendor;

    @Column
    private String origin;

    @Column
    private String shippingMethod;

    @Column
    private String contractNumber;

    //Previously stored on addressId, but differentiated to avoid confusion when sending messages to Vendor vs Customer
    @Column
    private String fedstripNumber;

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder();
    	for (java.lang.reflect.Field f : MiscShipTo.class.getDeclaredFields()) {
    		f.setAccessible(true);
    		try {
				stringBuilder.append(f.getName() + ": " + (f.get(this) == null ? " " : f.get(this) + " "));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				LOGGER.error("An error occured:", e);
			}
    	}
    	return stringBuilder.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressId36() {
        return addressId36;
    }

    public void setAddressId36(String addressId36) {
        this.addressId36 = addressId36;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getDesktop() {
        return desktop;
    }

    public void setDesktop(String desktop) {
        this.desktop = desktop;
    }

    public String getApdInterchangeId() {
        return apdInterchangeId;
    }

    public void setApdInterchangeId(String apdInterchangeId) {
        this.apdInterchangeId = apdInterchangeId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getCarrierRouting() {
        return carrierRouting;
    }

    public void setCarrierRouting(String carrierRouting) {
        this.carrierRouting = carrierRouting;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getFedexNumber() {
        return fedexNumber;
    }

    public void setFedexNumber(String fedexNumber) {
        this.fedexNumber = fedexNumber;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getMailStop() {
        return mailStop;
    }

    public void setMailStop(String mailStop) {
        this.mailStop = mailStop;
    }

    public String getNowl() {
        return nowl;
    }

    public void setNowl(String nowl) {
        this.nowl = nowl;
    }

    public String getPol() {
        return pol;
    }

    public void setPol(String pol) {
        this.pol = pol;
    }

    public String getSolomonId() {
        return solomonId;
    }

    public void setSolomonId(String solomonId) {
        this.solomonId = solomonId;
    }

    public String getUsAccount() {
        return usAccount;
    }

    public void setUsAccount(String usAccount) {
        this.usAccount = usAccount;
    }

    public String getHeaderComments() {
        return headerComments;
    }

    public void setHeaderComments(String headerComments) {
        this.headerComments = headerComments;
    }

    public String getOldRelease() {
        return oldRelease;
    }

    public void setOldRelease(String oldRelease) {
        this.oldRelease = oldRelease;
    }

    public String getOrderEmail() {
        return orderEmail;
    }

    public void setOrderEmail(String orderEmail) {
        this.orderEmail = orderEmail;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public String getStreet3() {
        return street3;
    }

    public void setStreet3(String street3) {
        this.street3 = street3;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getEdiID() {
        return ediID;
    }

    public void setEdiID(String ediID) {
        this.ediID = ediID;
    }

    public String getStoreNumber() {
        return storeNumber;
    }

    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }

    public String getCustomerShipToID() {
        return customerShipToID;
    }

    public void setCustomerShipToID(String customerShipToID) {
        this.customerShipToID = customerShipToID;
    }

    public String getGlnID() {
        return glnID;
    }

    public void setGlnID(String glnID) {
        this.glnID = glnID;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getLocations() {
        return locations;
    }

    public void setLocations(String locations) {
        this.locations = locations;
    }

    public String getDeliverToName() {
        return deliverToName;
    }

    public void setDeliverToName(String deliverToName) {
        this.deliverToName = deliverToName;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getLob() {
        return lob;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    public String getDcNumber() {
        return dcNumber;
    }

    public void setDcNumber(String dcNumber) {
        this.dcNumber = dcNumber;
    }

    public String getFinanceNumber() {
        return financeNumber;
    }

    public void setFinanceNumber(String financeNumber) {
        this.financeNumber = financeNumber;
    }

    public String getGlNumber() {
        return glNumber;
    }

    public void setGlNumber(String glNumber) {
        this.glNumber = glNumber;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getNameOfHospital() {
        return nameOfHospital;
    }

    public void setNameOfHospital(String nameOfHospital) {
        this.nameOfHospital = nameOfHospital;
    }

    public String getPurchasingGroup() {
        return purchasingGroup;
    }

    public void setPurchasingGroup(String purchasingGroup) {
        this.purchasingGroup = purchasingGroup;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getContractingOfficer() {
        return contractingOfficer;
    }

    public void setContractingOfficer(String contractingOfficer) {
        this.contractingOfficer = contractingOfficer;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getUspsUnder13Oz() {
        return uspsUnder13Oz;
    }

    public void setUspsUnder13Oz(String uspsUnder13Oz) {
        this.uspsUnder13Oz = uspsUnder13Oz;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * @return the requesterPhoneNumber
     */
    public String getRequesterPhone() {
        return requesterPhone;
    }

    /**
     * @param requesterPhoneNumber the requesterPhoneNumber to set
     */
    public void setRequesterPhone(String requesterPhoneNumber) {
        this.requesterPhone = requesterPhoneNumber;
    }

    /**
     * @return the contractNumber
     */
    public String getContractNumber() {
        return contractNumber;
    }

    /**
     * @param contractNumber the contractNumber to set
     */
    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    /**
     * @return the fedstripNumber
     */
    public String getFedstripNumber() {
        return fedstripNumber;
    }

    /**
     * @param fedstripNumber the fedstripNumber to set
     */
    public void setFedstripNumber(String fedstripNumber) {
        this.fedstripNumber = fedstripNumber;
    }

}
