package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import freemarker.template.TemplateException;

/**
 *
 * @author anicholson
 */

@Stateless
@LocalBean
public class ShipmentNoticeEmailTemplate extends EmailTemplate<ShipmentDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardCreditEmailTemplate.class);

    private static final String TEMPLATE = "shipment.notice";
    private static final String SHIP_NOTICE_FILE_PREFIX = "shipment-notification-pdf";

    @Override
    public String createBody(ShipmentDto shipDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        params.put("apdPo",shipDto.getCustomerOrderDto().getApdPoNumber());
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(ShipmentDto shipDto) throws NoAttachmentContentException {
        String trackingNo = shipDto.getTrackingNumber();
        Attachment shipNotice = new Attachment();
        shipNotice.setFileName(SHIP_NOTICE_FILE_PREFIX + trackingNo + PDF_EXT);
        shipNotice.setMimeType(Attachment.MimeType.pdf);
        InputStream is = reportService.generateShipmentNoticePdf(trackingNo);
        if (is != null) {
            shipNotice.setContent(is);
        }
        else {
            throw new NoAttachmentContentException();
        }

        return shipNotice;
    }
}
