package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Index;

@Entity
public class PoNumber implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 7265353940493286694L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    @Index(name = "PO_NUM_IDX")
    private String value;

    @ManyToMany(fetch = FetchType.LAZY)
    //this join table is indexed, however this is not indicated in the JPA annotations. 
    //JPA 2.1/Hibernate 4.3 can have @Index annotations within @JoinTables.
    private Set<CustomerOrder> orders = new HashSet<CustomerOrder>();

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private PoNumberType type;

    @Temporal(TemporalType.DATE)
    private Date startDate;

    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PoNumber) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public Set<CustomerOrder> getOrders() {
        return this.orders;
    }

    public void setOrders(final Set<CustomerOrder> orders) {
        this.orders = orders;
    }

    public PoNumberType getType() {
        return this.type;
    }

    public void setType(final PoNumberType type) {
        this.type = type;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null && !value.trim().isEmpty())
            result += "value: " + value;
        return result;
    }
}
