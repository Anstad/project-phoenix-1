/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditJoinTable;

/**
 * The Class CashoutPage.
 */
@Entity
@XmlRootElement
public class CashoutPage implements Serializable, com.apd.phoenix.service.model.Entity {

    //TODO regenerate serialVersionUID
    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @Column
    private String name;

    @Enumerated(EnumType.STRING)
    private NoCardAction noCardAction;

    @Column
    private String shiptoAddressDisplayFormat;

    @Column
    private String billtoAddressDisplayFormat;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CashoutPage) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The multiple ship to. */
    @Column
    private boolean multipleShipTo = false;

    /**
     * Gets the multiple ship to.
     *
     * @return the multiple ship to
     */
    public boolean getMultipleShipTo() {
        return this.multipleShipTo;
    }

    /**
     * Sets the multiple ship to.
     *
     * @param multipleShipTo the new multiple ship to
     */
    public void setMultipleShipTo(final boolean multipleShipTo) {
        this.multipleShipTo = multipleShipTo;
    }

    /** The higher price notice. */
    @Column
    private boolean higherPriceNotice = false;

    /**
     * Gets the higher price notice.
     *
     * @return the higher price notice
     */
    public boolean getHigherPriceNotice() {
        return this.higherPriceNotice;
    }

    /**
     * Sets the higher price notice.
     *
     * @param higherPriceNotice the new higher price notice
     */
    public void setHigherPriceNotice(final boolean higherPriceNotice) {
        this.higherPriceNotice = higherPriceNotice;
    }

    /** The update user information. */
    @Column
    private boolean updateUserInformation = false;

    /**
     * Gets the update user information.
     *
     * @return the update user information
     */
    public boolean getUpdateUserInformation() {
        return this.updateUserInformation;
    }

    /**
     * Sets the update user information.
     *
     * @param updateUserInformation the new update user information
     */
    public void setUpdateUserInformation(final boolean updateUserInformation) {
        this.updateUserInformation = updateUserInformation;
    }

    /** The delivery date. */
    @Column
    private boolean deliveryDate = false;

    /**
     * Gets the delivery date.
     *
     * @return the delivery date
     */
    public boolean getDeliveryDate() {
        return this.deliveryDate;
    }

    /**
     * Sets the delivery date.
     *
     * @param deliveryDate the new delivery date
     */
    public void setDeliveryDate(final boolean deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    /** The expected date. */
    @Column
    private boolean expectedDate = false;

    /**
     * Gets the expected date.
     *
     * @return the expected date
     */
    public boolean getExpectedDate() {
        return this.expectedDate;
    }

    /**
     * Sets the expected date.
     *
     * @param expectedDate the new expected date
     */
    public void setExpectedDate(final boolean expectedDate) {
        this.expectedDate = expectedDate;
    }

    /** The cancel date. */
    @Column
    private boolean cancelDate = false;

    /**
     * Gets the cancel date.
     *
     * @return the cancel date
     */
    public boolean getCancelDate() {
        return this.cancelDate;
    }

    /**
     * Sets the cancel date.
     *
     * @param cancelDate the new cancel date
     */
    public void setCancelDate(final boolean cancelDate) {
        this.cancelDate = cancelDate;
    }

    /** The hide cc number. */
    @Column
    private boolean hideCCNumber = false;

    /**
     * Gets the hide cc number.
     *
     * @return the hide cc number
     */
    public boolean getHideCCNumber() {
        return this.hideCCNumber;
    }

    /**
     * Sets the hide cc number.
     *
     * @param hideCCNumber the new hide cc number
     */
    public void setHideCCNumber(final boolean hideCCNumber) {
        this.hideCCNumber = hideCCNumber;
    }

    /** The service date. */
    @Column
    private boolean serviceDate = false;

    /**
     * Gets the service date.
     *
     * @return the service date
     */
    public boolean getServiceDate() {
        return this.serviceDate;
    }

    /**
     * Sets the service date.
     *
     * @param serviceDate the new service date
     */
    public void setServiceDate(final boolean serviceDate) {
        this.serviceDate = serviceDate;
    }

    /** The fields */
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "page_id")
    @AuditJoinTable(name = "cpxcpxf_AUD")
    private Set<CashoutPageXField> pageXFields = new HashSet<CashoutPageXField>();

    /**
     * Gets the fields.
     *
     * @return the pageXFields
     */
    public Set<CashoutPageXField> getPageXFields() {
        return this.pageXFields;
    }

    /**
     * Sets the fields.
     *
     * @param pageXFields the new fields
     */
    public void setPageXFields(final Set<CashoutPageXField> pageXFields) {
        this.pageXFields = pageXFields;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public NoCardAction getNoCardAction() {
        return this.noCardAction;
    }

    public void setNoCardAction(final NoCardAction action) {
        this.noCardAction = action;
    }

    public String getShiptoAddressDisplayFormat() {
        return shiptoAddressDisplayFormat;
    }

    public void setShiptoAddressDisplayFormat(String shiptoAddressDisplayFormat) {
        this.shiptoAddressDisplayFormat = shiptoAddressDisplayFormat;
    }

    public String getBilltoAddressDisplayFormat() {
        return billtoAddressDisplayFormat;
    }

    public void setBilltoAddressDisplayFormat(String billtoAddressDisplayFormat) {
        this.billtoAddressDisplayFormat = billtoAddressDisplayFormat;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }

    /**
     * This enum lists the different actions that can be taken if the user doesn't provide a credit card.
     * 
     * @author RHC
     *
     */
    public enum NoCardAction {
        USE_ASSIGNED("Assigned", "Check if there is assigned payment information, otherwise pay by invoice"), INVOICE(
                "Invoice", "Pay by invoice");

        private final String label;
        private final String description;

        private NoCardAction(String label, String description) {
            this.label = label;
            this.description = description;
        }

        public String getLabel() {
            return this.label;
        }

        public String getDescription() {
            return this.description;
        }
    }
}