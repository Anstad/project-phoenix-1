package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.time.DateUtils;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao.Sequence;

/**
 * This class provides business process methods for PoNumber.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class PoNumberBp extends AbstractBp<PoNumber> {

    @Inject
    PoNumberTypeBp poNumberTypeBp;

    @Inject
    SequenceDao sequenceDao;

    @Inject
    private ApdPoLogBp apdPoLogBp;

    public static final String APD_PO_NUMBER_PREFIX = "APD";
    public static final String APD_PO_NUMBER_PADDING_CHAR = "0";
    public static final short APD_PO_NUMBER_MAX_LENGTH = 9;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(PoNumberDao dao) {
        this.dao = dao;
    }

    public List<PoNumber> poNumbersForCashout(AccountXCredentialXUser axcxu) {
        List<PoNumber> poNumbers = ((PoNumberDao) this.dao).poNumbersForCashout(axcxu);
        List<PoNumber> validPoNumbers = new ArrayList<PoNumber>();
        Date today = new Date();
        for (PoNumber poNumber : poNumbers) {
            if (poNumber.getStartDate() != null && poNumber.getExpirationDate() != null) {
                if ((poNumber.getStartDate().before(today) || DateUtils.isSameDay(poNumber.getStartDate(), today))
                        && (poNumber.getExpirationDate().after(today) || DateUtils.isSameDay(poNumber
                                .getExpirationDate(), today))) {
                    validPoNumbers.add(poNumber);
                }
            }
            else {
                validPoNumbers.add(poNumber);
            }
        }
        return validPoNumbers;
    }

    public PoNumber nextApdPoDetached() {
        StringBuffer poNumber = new StringBuffer();
        poNumber.append(APD_PO_NUMBER_PREFIX);
        String poSequenceValue = sequenceDao.nextVal(Sequence.APD_PO_GENERATOR).toString();
        for (int i = poSequenceValue.length(); i < APD_PO_NUMBER_MAX_LENGTH; i++) {
            poNumber.append(APD_PO_NUMBER_PADDING_CHAR);
        }
        poNumber.append(poSequenceValue);
        return this.apdPoLogBp.createLogAndApdPo(poNumber.toString());
    }

    public PoNumber nextApdPo() {
        PoNumber toReturn = this.nextApdPoDetached();
        return this.findById(toReturn.getId(), PoNumber.class);
    }

    public PoNumber getPoNumber(String value) {
        return ((PoNumberDao) this.dao).getPoNumber(value);
    }
}
