/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "VENDOR_INVOICE")
public class VendorInvoice extends Invoice {

    @ManyToOne
    private CustomerOrder customerOrder;

    @Column
    private Boolean valid = true;

    /**
     * @return the customerOrder
     */
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    /**
     * @param customerOrder the customerOrder to set
     */
    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public Boolean getValid() {
        return this.valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }
}
