/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.utility;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CustomerOrder;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author nreidelb
 */
@FacesValidator("customerPoValidator")
public class CustomerPoValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (!(value instanceof String)) {
            throw new ValidatorException(new FacesMessage("Please enter a customer PO."));
        }
        String customerPo = (String) value;
        CustomerOrder order = (CustomerOrder) component.getAttributes().get("order");
        if (StringUtils.isNotBlank(customerPo) && order != null && order.getAccount() != null) {
            try {
                InitialContext initContext = new InitialContext();
                CustomerOrderBp customerOrderBp = (CustomerOrderBp) initContext.lookup("java:module/CustomerOrderBp");
                if (customerOrderBp.numberOfOrdersWithCustomerPo(customerPo, order.getAccount()) > 0) {
                    throw new ValidatorException(new FacesMessage("A customer order with customer PO " + (String) value
                            + " already exists"));
                }
            }
            catch (NamingException ex) {
                throw new ValidatorException(
                        new FacesMessage("Internal Error while trying to validate customer order."));
            }
        }
    }

}
