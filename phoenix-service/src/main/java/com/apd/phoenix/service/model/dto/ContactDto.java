package com.apd.phoenix.service.model.dto;

import java.util.List;

public class ContactDto {

    private String name;

    private List<ContactNumberDto> contactNumbers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ContactNumberDto> getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(List<ContactNumberDto> contactNumbers) {
        this.contactNumbers = contactNumbers;
    }
}
