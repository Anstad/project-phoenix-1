package com.apd.phoenix.service.invoice;

import java.util.ArrayList;
import java.util.List;

public class CustomerOrderUpdate {

    private String orderId;
    private String status;
    private String trackingNumber;
    private String trackingProvider;
    private List<MessageLineItem> items;

    public CustomerOrderUpdate() {
        items = new ArrayList<MessageLineItem>();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingProvider() {
        return trackingProvider;
    }

    public void setTrackingProvider(String trackingProvider) {
        this.trackingProvider = trackingProvider;
    }

    public List<MessageLineItem> getItems() {
        return items;
    }

    public void setItems(List<MessageLineItem> items) {
        this.items = items;
    }

}
