/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.api;

import org.drools.KnowledgeBase;

public interface KnowledgeBaseFactory {

    public void rebuild();

    public void rebuild(String snapshot);

    public KnowledgeBase getKnowledgeBase();

    public KnowledgeBase getKnowledgeBase(String snapshot);
}
