package com.apd.phoenix.service.persistence.jpa;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EntityComparator;

/**
 * The abstract class for a Data Access Object, with some general functionality. 
 * Implements Dao<T>, extended by *Dao.
 * 
 * @author RHC
 *
 * @param <T>
 */
public abstract class AbstractDao<T> implements Dao<T> {

    private static final String JAVAX_PERSISTENCE = "javax.persistence.";

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);

    private static final String DESC = " desc";

    private static final String ASC = " asc";

    // The fields on the entity that shouldn't be searched for
    protected static final String[] ignoredFields = { "serialVersionUID", "version" };

    protected static final String longIndicator = " long";

    // @Inject
    @PersistenceContext(unitName = "Phoenix")
    protected EntityManager entityManager;

    public Session getHibernateSession() {
        return (Session) entityManager.getDelegate();
    }

    @Override
    public T create(T t) {
        this.entityManager.persist(t);
        this.entityManager.flush();
        this.entityManager.refresh(t);
        return t;
    }

    @Override
    public void createNoRefresh(T t) {
        this.entityManager.persist(t);
        this.entityManager.flush();
    }

    @Override
    public T findById(Long id, Class<T> type) {
        return this.entityManager.find(type, id);
    }

    @Override
    public void delete(Long id, Class<T> type) {
        Object ref = this.entityManager.getReference(type, id);
        this.entityManager.remove(ref);
    }

    @Override
    public T update(T t) {
        return this.entityManager.merge(t);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> findAll(Class<T> type, int start, int quantity) {
        Query query = entityManager.createQuery("FROM " + type.getName());
        setItemsReturned(query, start, quantity);
        List<T> toReturn = query.getResultList();
        try {
            Collections.sort(toReturn, new EntityComparator());
        }
        catch (Exception e) {
            LOGGER.error("Could not sort a collection of entity elements", e);
        }
        return toReturn;
    }

    @SuppressWarnings("unchecked")
    @Override
    public T eagerLoad(T toLoad) {
        try {
            //gets the id
            Method idGetter = toLoad.getClass().getMethod("getId", (Class<?>[]) null);
            Long id = (Long) idGetter.invoke(toLoad, (Object[]) null);
            if (id == null) {
                //transient entity passed in
                return toLoad;
            }
            StringBuffer queryString = new StringBuffer();
            queryString.append("SELECT S FROM " + toLoad.getClass().getName() + " S ");

            queryString.append(joinQuery("S", toLoad.getClass()));

            queryString.append("WHERE S.id = " + id);

            Query query = entityManager.createQuery(queryString.toString(), toLoad.getClass());
            setItemsReturned(query, 0, 0);
            T loaded = (T) query.getResultList().get(0);

            //merges the passed in entity to the persistence context
            if (!entityManager.contains(loaded)) {
                loaded = entityManager.merge(loaded);
            }
            return loaded;
        }
        catch (Exception e) {
            LOGGER.info("Abstract DAO exception thrown");
            LOGGER.error("An error occured:", e);
            ;
            //occurs when there's a problem calling the methods
            return toLoad;
        }
    }

    protected static String joinQuery(String joinEntity, Class<?> joinClass) {
        Field[] fields = joinClass.getDeclaredFields();
        StringBuffer toReturn = new StringBuffer();

        for (Field field : fields) {
            String name = field.getName();

            //Doesn't progress down the graph if the field is one to ignore
            if (isEntityField(field)) {
                toReturn.append("LEFT JOIN FETCH " + joinEntity + "." + name + " AS " + joinEntity + "_" + name + " ");
            }
        }
        return toReturn.toString();
    }

    /**
     * Determines if a given Field is a persisted entity or collection of entities.
     * 
     * @param f
     * @return
     */
    private static boolean isEntityField(Field f) {
        for (Annotation annotation : f.getAnnotations()) {
            if (annotation.annotationType().getName().equals("javax.persistence.OneToOne")
                    || annotation.annotationType().getName().equals("javax.persistence.OneToMany")
                    || annotation.annotationType().getName().equals("javax.persistence.ManyToOne")
                    || annotation.annotationType().getName().equals("javax.persistence.ManyToMany")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Used internally to set the number of items to return, using Query.setFirstResult and 
     * Query.setMaxResults.
     * 
     * @param query
     * @param start
     * @param quantity
     */
    @SuppressWarnings("static-method")
    protected void setItemsReturned(Query query, int start, int quantity) {
        query.setFirstResult(start);
        if (quantity != 0) {
            query.setMaxResults(quantity);
        }
    }

    @Override
    public List<T> searchByExactExample(T search, int start, int quantity) {
        return search(search, null, start, quantity, true);
    }

    @Override
    public List<T> searchByExactExample(T search, T ordering, int start, int quantity) {
        return search(search, ordering, start, quantity, true);
    }

    @Override
    public List<T> searchByExample(T search, int start, int quantity) {
        return search(search, null, start, quantity, false);
    }

    @Override
    public List<T> searchByExample(T search, T ordering, int start, int quantity) {
        return search(search, ordering, start, quantity, false);
    }

    @SuppressWarnings("unchecked")
    private List<T> search(T search, T ordering, int start, int quantity, boolean exact) {
        //Creates the HQL query string, using the conditionString method
        List<String> params = new ArrayList<String>();
        String hql = "SELECT S ".concat(conditionString(search, ordering, params, exact));
        //Creates the query
        Query query = entityManager.createQuery(hql, search.getClass());
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        setItemsReturned(query, start, quantity);
        List<T> toReturn = query.getResultList();
        //If there isn't already an existing ordering, sorts the list
        if (ordering == null || hql.equals("SELECT S FROM " + search.getClass().getName() + " S")) {
            try {
                Collections.sort(toReturn, new EntityComparator());
            }
            catch (Exception e) {
                LOGGER.error("Could not sort a collection of entity elements", e);
            }
        }
        return toReturn;
    }

    /**
     * Returns true if the entered value has the format "[0-9]* asc" or "[0-9]* desc", false otherwise. Used to 
     * determine whether the value entered is a command for ordering.
     * 
     * @param value
     * @return
     */
    private static boolean isOrderingCommand(String value) {
        String prefix = "";
        if (value.endsWith(ASC)) {
            prefix = value.substring(0, value.length() - ASC.length());
        }
        else if (value.endsWith(DESC)) {
            prefix = value.substring(0, value.length() - DESC.length());
        }
        else {
            return false;
        }
        try {
            Integer.valueOf(prefix);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * This method is used to generate the string used in the Hibernate query.
     * 
     * @param search - The entity being searched by
     * @return The Hibernate query string that generates the search results.
     */
    private String conditionString(T search, T order, List<String> params, boolean exact) {
        //This hash map will have the path down the object tree as the key, and the field value as its value. 
        //So, if searching for SystemUsers where their first name is "Test", there would be a mapping from 
        //"S.person.firstName" to "%Test%"
        Map<String, String> searchMap = new HashMap<String, String>();
        //This map works the same as searchMap, but for the order entity instead of the search entity.
        Map<String, String> orderMap = new HashMap<String, String>();
        Map<String, String> reversedOrderMap = new HashMap<String, String>();
        //This hash map will have the association as the key, and the name of the associated value as  its 
        //value. So, if we have a SystemUser S with an Account set accounts, and we want to map the Account to 
        //S1, the key would be S.accounts and the value would be S1.
        List<String> joinMap = new ArrayList<String>();

        //Uses the stringSearchPredicates to populate the Map.
        stringSearchPredicates(searchMap, joinMap, "S", search);

        List<String> keysToRemove = new ArrayList<String>();

        //If the order object is not null, generates the map 
        if (order != null) {
            stringSearchPredicates(reversedOrderMap, joinMap, "S", order);
            for (String key : reversedOrderMap.keySet()) {
                //If the value is an ordering command, puts it in the order map; otherwise, it's a search condition.
                if (isOrderingCommand(reversedOrderMap.get(key))) {
                    orderMap.put(reversedOrderMap.get(key), key);
                    keysToRemove.add(key);
                }
                else {
                    searchMap.put(key, reversedOrderMap.get(key));
                }
            }
            //if a value remains in the map, it's a search condition, so ordering commands are removed.
            for (String key : keysToRemove) {
                reversedOrderMap.remove(key);
            }
        }

        StringBuffer toReturn = new StringBuffer("FROM " + search.getClass().getName() + " AS S ");

        //Creates the join relationships in the query
        for (int i = 0; i < joinMap.size(); i++) {
            toReturn.append("JOIN " + joinMap.get(i) + " AS S" + i + " ");
        }

        boolean firstAdded = true;
        for (String k : searchMap.keySet()) {
            //If this isn't the first parameter, adds the AND keyword before the condition
            toReturn.append(firstAdded ? "WHERE" : " AND");
            if (k.endsWith(".id")) {
                //If the condition specifies an ID
                toReturn.append(" " + k + "=" + searchMap.get(k));
            }
            else if (k.endsWith(longIndicator)) {
                toReturn.append(" " + k.substring(0, k.length() - (longIndicator).length()) + "=" + searchMap.get(k));
            }
            else {
                //If the condition is a string to match
                if (exact || reversedOrderMap.containsKey(k)) {
                    //Exact string matching, or it's an exact field specified in the ordering
                    toReturn.append(" " + k + " = ?" + params.size());
                    params.add(searchMap.get(k));
                }
                else {
                    //wildcards at either end, case insensitive
                    toReturn.append(" lower(" + k + ") LIKE ?" + params.size());
                    params.add("%" + searchMap.get(k).toLowerCase() + "%");
                }
            }
            firstAdded = false;
        }
        //adds the order commands
        List<String> orderCommands = new ArrayList<String>(orderMap.keySet());
        try {
            Collections.sort(orderCommands, new EntityComparator());
        }
        catch (Exception e) {
            LOGGER.error("Could not sort a collection of entity elements", e);
        }
        firstAdded = true;
        for (String orderCommand : orderCommands) {
            if (firstAdded) {
                toReturn.append(" ORDER BY ");
            }
            else {
                toReturn.append(", ");
            }
            toReturn.append(orderMap.get(orderCommand));
            if (orderCommand.contains(ASC)) {
                toReturn.append(ASC);
            }
            else {
                toReturn.append(DESC);
            }
            firstAdded = false;
        }
        return toReturn.toString();
    }

    /**
     * Returns the number of items that will be found if a search with the given entity is done.
     * 
     * @param search - the entity to be searched by
     * @return The number of items that will be returned.
     */
    @Override
    public Long resultQuantity(T search) {
        List<String> params = new ArrayList<String>();
        String hql = "select count(*) ".concat(conditionString(search, null, params, false));
        Query query = entityManager.createQuery(hql);
        for (int i = 0; i < params.size(); i++) {
            query.setParameter(i, params.get(i));
        }
        return (Long) query.getSingleResult();
    }

    /**
     * Check if the instance is a managed entity instance belonging to the current persistence context.
     * 
     * @param search - the entity to locate
     * @return Whether or not the entity is managed
     */
    @Override
    public boolean isManaged(T search) {
        return entityManager.contains(search);
    }

    @SuppressWarnings( { "unchecked" })
    public static <T> T getSingleResultOrNull(Query query) {
        List<T> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        else if (results.size() != 1) {
            LOGGER
                    .warn("Search returned multiple results for getSingleResultOrNull, returning the first element found");
        }
        return results.get(0);
    }

    /**
     * This method recursively traverses the object tree, adding parameters to the valueMap and joins objects.
     * 
     * @param valueMap - The Map<String, String> that parameters are being stored on.
     * @param joins - The Map<String, String> that join relationships are being stored on.
     * @param pathSoFar - The path traversed through the tree to get to the current object. 
     * @param current - The current node in the object tree.
     */
    private void stringSearchPredicates(Map<String, String> valueMap, List<String> joins, String pathSoFar,
            Object current) {
    	Set<Object> traversedObjects = new HashSet<>();
    	traversedObjects.add(current);
    	stringSearchPredicates(valueMap, joins, pathSoFar, current, traversedObjects);
    }

    /**
     * This method recursively traverses the object tree, adding parameters to the valueMap and joins objects.
     * 
     * @param valueMap - The Map<String, String> that parameters are being stored on.
     * @param joins - The Map<String, String> that join relationships are being stored on.
     * @param pathSoFar - The path traversed through the tree to get to the current object. 
     * @param current - The current node in the object tree.
     */
    @SuppressWarnings("unchecked")
    private void stringSearchPredicates(Map<String, String> valueMap, List<String> joins, String pathSoFar,
            Object current, Set<Object> traversedObjects) {

        //obtains the fields of the object
        Field[] fields = current.getClass().getDeclaredFields();

        for (Field field : fields) {
            //For each field on the current object, obtains the type, the name of the field, and its value
            Class<?> type = field.getType();
            String name = field.getName();
            Object value;
            try {
                field.setAccessible(true);
                value = field.get(current);
                //Prevent an infinite loop from occurring
                if (traversedObjects.contains(value)) {
                    value = null;
                }
                else if (isEntityField(field)) {
                    traversedObjects.add(value);
                }
            }
            catch (Exception e) {
                value = null;
            }

            if (value != null) {

                //Doesn't progress down the graph if the field is one to ignore
                boolean toBeIgnored = true;

                //ignores the field if it isn't a JPA field
                for (Annotation annotation : field.getAnnotations()) {
                    if (annotation.annotationType().getName().contains(JAVAX_PERSISTENCE)) {
                        toBeIgnored = false;
                        break;
                    }
                }

                //ignores the field if it's in the list of fields to ignore
                for (String f : ignoredFields) {
                    if (name.equals(f)) {
                        toBeIgnored = true;
                        break;
                    }
                }

                String newPath = pathSoFar.concat(".".concat(name));
                //If the return type of the field is an entity, progress down the tree
                if (type.getName().contains("phoenix") && !type.isEnum()) {
                    stringSearchPredicates(valueMap, joins, newPath, value, traversedObjects);
                }//If the return type is a String, we've reached a leaf of the tree, and add it to the Map
                else if (!toBeIgnored && type == String.class && !value.equals("")) {
                    valueMap.put(newPath, value.toString());
                }
                else if (type.getName().contains("util.")
                        && (type.getName().contains("List") || type.getName().contains("Set"))
                        && ((Iterable<Object>) value).iterator().hasNext()) {
                    //If the return type is a List or Set, create a join between the elements in the set, 
                    //then iterate over each entry and progress down the tree
                    String oldPath = newPath;
                    for (Object o : (Iterable<Object>) value) {
                        newPath = "S".concat("" + joins.size());
                        joins.add(oldPath);
                        stringSearchPredicates(valueMap, joins, newPath, o, traversedObjects);
                    }
                }
                else if (name.equals("id")) {
                    //Used to specify a specific entity as a search parameter
                    valueMap.put(newPath, ("" + value));
                }
                else if (!toBeIgnored && type == Long.class) {
                    valueMap.put(newPath + longIndicator, value.toString());
                }
            }
        }
    }

    @Override
    public T findAndLock(Long id, Class<T> type) {
        return this.entityManager.find(type, id, LockModeType.PESSIMISTIC_WRITE);
    }
}
