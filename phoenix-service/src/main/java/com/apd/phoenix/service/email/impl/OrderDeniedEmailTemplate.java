package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.core.utility.EcommercePropertiesLoader;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderDeniedEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final String TEMPLATE = "order.denied";

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		
		String csrPhone = EcommercePropertiesLoader.getInstance().getEcommerceProperties().getString("footer.default.csrphone");

		if (StringUtils.isNotBlank(purchaseOrderDto.getApdPoNumber())) {
			CustomerOrder order = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());
			
			if (order != null) {
				if (order.getAccount() != null && StringUtils.isNotBlank(order.getAccount().getCsrPhone())) {
					csrPhone = order.getAccount().getCsrPhone();
				}
		
				if (order.getCredential() != null && StringUtils.isNotBlank(order.getCredential().getCsrPhone())) {
					csrPhone = order.getCredential().getCsrPhone();
				}
			}
		}
		
		params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
		params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
		params.put("approvalComment", StringUtils.isNotBlank(purchaseOrderDto.getApprovalComment()) ? purchaseOrderDto.getApprovalComment() : "");
		
		params.put("csrPhone", csrPhone);
		
		params.put("merchandiseTotal", purchaseOrderDto.getSubTotal());	
		params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
		params.put("taxTotal", purchaseOrderDto.getMaximumTaxToCharge());	
		params.put("orderTotal", purchaseOrderDto.getOrderTotal());
		params.put("items", purchaseOrderDto.getItems());
		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto purchaseOrderDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
