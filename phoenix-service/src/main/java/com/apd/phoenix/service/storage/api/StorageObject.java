package com.apd.phoenix.service.storage.api;

import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class StorageObject {

    private InputStream content;
    private long contentLength;
    private Map<String, String> attributes;
    private String path;
    private String contentType;
    private Date lastModified;
    private boolean isContentUrl = false;
    private URL presignedUrl;

    public StorageObject() {
		attributes = new HashMap<>();
	}

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public InputStream getContent() {
        return content;
    }

    public void setContent(InputStream content) {
        this.content = content;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public boolean isContentUrl() {
        return isContentUrl;
    }

    public void setContentUrl(boolean isContentUrl) {
        this.isContentUrl = isContentUrl;
    }

    public URL getPresignedUrl() {
        return presignedUrl;
    }

    public void setPresignedUrl(URL presignedUrl) {
        this.presignedUrl = presignedUrl;
    }

}
