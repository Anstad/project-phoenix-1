package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;
import com.apd.phoenix.service.persistence.jpa.LineItemXShipmentXTaxTypeDao;

@Stateless
@LocalBean
public class LineItemXShipmentXTaxTypeBp extends AbstractBp<LineItemXShipmentXTaxType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(LineItemXShipmentXTaxTypeDao dao) {
        this.dao = dao;
    }
}
