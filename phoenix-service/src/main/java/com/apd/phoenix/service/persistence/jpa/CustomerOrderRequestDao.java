package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CustomerOrderRequest;

@Stateless
@LocalBean
public class CustomerOrderRequestDao extends AbstractDao<CustomerOrderRequest> {

    @SuppressWarnings("unchecked")
    public List<CustomerOrderRequest> getCustomerOrderRequestByToken(String token) {
        String hql = "SELECT record FROM CustomerOrderRequest AS record WHERE record.token=:token";
        Query query = entityManager.createQuery(hql);
        query.setParameter("token", token);
        return query.getResultList();
    }
}
