package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;
import com.apd.phoenix.service.model.dto.DepartmentRequestDto;

public class UserModificationRequestDto implements Serializable {

    /**
     * If you add a field to this object which you want to be included in a user modification request email,
     * it must resolve to a string, or be a DTO or collections of DTO. To add a field:
     * 1. Add the field to the UserModificationRequestDTO
     * 2. Set the value in the UserMaintenanceBean
     * 3. Modify the template class (e.g. AddLocationRequestEmailTemplate) to set the value
     * 4. Modify the corresponding .ftl file to include the value
     */
    private static final long serialVersionUID = -8279099531525250252L;
    private String accountId;
    private boolean validAccountId;
    private String userType;
    private String reason;
    private String requestType;
    private String firstName;
    private String lastName;
    private String email;
    private String companyEmail;
    private String phone;
    private String ext;

    private String locationAction;

    private String address1Old;
    private String address2Old;
    private String cityOld;
    private String stateOld;
    private String zipOld;
    private String mailstopOld;
    private String poleOld;
    private String commentsOld;

    private String address1New;
    private String address2New;
    private String cityNew;
    private String stateNew;
    private String zipNew;
    private String mailstopNew;
    private String poleNew;
    private String commentsNew;

    private List<DepartmentRequestDto> departments;
    private String requesterName;
    private String requesterEmail;
    private String requesterPhone;
    private String requesterManagerEmail;

    private String internalEmail;

    /*New User*/
    private String endUser;
    private String userId;
    private String affiliation;
    private String isPSG;
    private String corpBuNum;
    /*First Name, lastName, email,phone, and ext are defined above.*/

    /*Ship-to Address*/
    private String sAddress1;
    private String sAddress2;
    private String sCity;
    private String sState;
    private String sZip;
    private String sDesktop;
    private String sDepartment;
    private String sCostCenter;
    private String sComments;
    private String mailstop;
    private String pole;

    /*Departments*/
    private String dMultipleDepts;
    private String dName;
    private String dDesktop;
    private String dCostCenter;
    private String dSameAsDefault;
    private String dAddress;
    private String dCity;
    private String dState;
    private String dZip;
    private List<DepartmentRequestDto> departmentsList;

    /*Approver*/
    private String aName;
    private String aEmail;
    private String aPhone;

    private String companyName;
    private String billLine1;
    private String billLine2;
    private String billCity;
    private String billState;
    private String billZip;
    private String billContactName;
    private String billContactEmail;
    private String billContactPhone;

    private String locationLocationName;
    private String locationLocationId;
    private String locationLine1;
    private String locationLine2;
    private String locationCity;
    private String locationState;
    private String locationZip;

    private String shipLocationName;
    private String shipLocationId;
    private String shipLine1;
    private String shipLine2;
    private String shipCity;
    private String shipState;
    private String shipZip;

    private String managerName;
    private String managerEmail;
    private String managerPhone;

    private boolean noManagerApproval;

    private List<String> emailList;

    public UserModificationRequestDto() {
        this.setValidAccountId(false);
    }

    public String getLocationAction() {
        return locationAction;
    }

    public void setLocationAction(String locationAction) {
        this.locationAction = locationAction;
    }

    public String getAddress1Old() {
        return address1Old;
    }

    public void setAddress1Old(String address1Old) {
        this.address1Old = address1Old;
    }

    public String getAddress2Old() {
        return address2Old;
    }

    public void setAddress2Old(String address2Old) {
        this.address2Old = address2Old;
    }

    public String getCityOld() {
        return cityOld;
    }

    public void setCityOld(String cityOld) {
        this.cityOld = cityOld;
    }

    public String getStateOld() {
        return stateOld;
    }

    public void setStateOld(String stateOld) {
        this.stateOld = stateOld;
    }

    public String getZipOld() {
        return zipOld;
    }

    public void setZipOld(String zipOld) {
        this.zipOld = zipOld;
    }

    public String getMailstopOld() {
        return mailstopOld;
    }

    public void setMailstopOld(String mailstopOld) {
        this.mailstopOld = mailstopOld;
    }

    public String getPoleOld() {
        return poleOld;
    }

    public void setPoleOld(String poleOld) {
        this.poleOld = poleOld;
    }

    public String getCommentsOld() {
        return commentsOld;
    }

    public void setCommentsOld(String commentsOld) {
        this.commentsOld = commentsOld;
    }

    public String getAddress1New() {
        return address1New;
    }

    public void setAddress1New(String address1New) {
        this.address1New = address1New;
    }

    public String getAddress2New() {
        return address2New;
    }

    public void setAddress2New(String address2New) {
        this.address2New = address2New;
    }

    public String getCityNew() {
        return cityNew;
    }

    public void setCityNew(String cityNew) {
        this.cityNew = cityNew;
    }

    public String getStateNew() {
        return stateNew;
    }

    public void setStateNew(String stateNew) {
        this.stateNew = stateNew;
    }

    public String getZipNew() {
        return zipNew;
    }

    public void setZipNew(String zipNew) {
        this.zipNew = zipNew;
    }

    public String getMailstopNew() {
        return mailstopNew;
    }

    public void setMailstopNew(String mailstopNew) {
        this.mailstopNew = mailstopNew;
    }

    public String getPoleNew() {
        return poleNew;
    }

    public void setPoleNew(String poleNew) {
        this.poleNew = poleNew;
    }

    public String getCommentsNew() {
        return commentsNew;
    }

    public void setCommentsNew(String commentsNew) {
        this.commentsNew = commentsNew;
    }

    public List<DepartmentRequestDto> getDepartments() {
        return departments;
    }

    public void setDepartments(List<DepartmentRequestDto> departments) {
        this.departments = departments;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public boolean isValidAccountId() {
        return validAccountId;
    }

    public void setValidAccountId(boolean validAccountId) {
        this.validAccountId = validAccountId;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String email) {
        this.companyEmail = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getRequesterName() {
        return requesterName;
    }

    public void setRequesterName(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getRequesterEmail() {
        return requesterEmail;
    }

    public void setRequesterEmail(String requesterEmail) {
        this.requesterEmail = requesterEmail;
    }

    public String getRequesterPhone() {
        return requesterPhone;
    }

    public void setRequesterPhone(String requesterPhone) {
        this.requesterPhone = requesterPhone;
    }

    public String getRequesterManagerEmail() {
        return requesterManagerEmail;
    }

    public void setRequesterManagerEmail(String requesterManagerEmail) {
        this.requesterManagerEmail = requesterManagerEmail;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getInternalEmail() {
        return internalEmail;
    }

    public void setInternalEmail(String internalEmail) {
        this.internalEmail = internalEmail;
    }

    /****Customized User Creation Screen*/
    /*New User*/
    public String getEndUser() {
        return endUser;
    }

    public void setEndUser(String endUser) {
        this.endUser = endUser;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getIsPSG() {
        return isPSG;
    }

    public void setIsPSG(String isPSG) {
        this.isPSG = isPSG;
    }

    public String getCorpBuNum() {
        return corpBuNum;
    }

    public void setCorpBuNum(String corpBuNum) {
        this.corpBuNum = corpBuNum;
    }

    /*Ship-to Address*/
    public String getSAddress1() {
        return sAddress1;
    }

    public void setSAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getSAddress2() {
        return sAddress2;
    }

    public void setSAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getSCity() {
        return sCity;
    }

    public void setSCity(String sCity) {
        this.sCity = sCity;
    }

    public String getSState() {
        return sState;
    }

    public void setSState(String sState) {
        this.sState = sState;
    }

    public String getSZip() {
        return sZip;
    }

    public void setSZip(String sZip) {
        this.sZip = sZip;
    }

    public String getSDesktop() {
        return sDesktop;
    }

    public void setSDesktop(String sDesktop) {
        this.sDesktop = sDesktop;
    }

    public String getSDepartment() {
        return sDepartment;
    }

    public void setSDepartment(String sDepartment) {
        this.sDepartment = sDepartment;
    }

    public String getSCostCenter() {
        return sCostCenter;
    }

    public void setSCostCenter(String sCostCenter) {
        this.sCostCenter = sCostCenter;
    }

    public String getSComments() {
        return sComments;
    }

    public void setSComments(String sComments) {
        this.sComments = sComments;
    }

    public String getMailStop() {
        return this.mailstop;
    }

    public void setMailStop(String mailstop) {
        this.mailstop = mailstop;
    }

    public String getPole() {
        return this.pole;
    }

    public void setPole(String pole) {
        this.pole = pole;
    }

    /*Departments*/
    public String getDMultipleDepts() {
        return dMultipleDepts;
    }

    public void setDMultipleDepts(String dMultipleDepts) {
        this.dMultipleDepts = dMultipleDepts;
    }

    public String getDName() {
        return dName;
    }

    public void setDName(String dName) {
        this.dName = dName;
    }

    public String getDDesktop() {
        return dDesktop;
    }

    public void setDDesktop(String dDesktop) {
        this.dDesktop = dDesktop;
    }

    public String getDCostCenter() {
        return dCostCenter;
    }

    public void setDCostCenter(String dCostCenter) {
        this.dCostCenter = dCostCenter;
    }

    public String getDSameAsDefault() {
        return dSameAsDefault;
    }

    public void setDSameAsDefault(String dSameAsDefault) {
        this.dSameAsDefault = dSameAsDefault;
    }

    public String getDAddress() {
        return dAddress;
    }

    public void setDAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public String getDCity() {
        return dCity;
    }

    public void setDCity(String dCity) {
        this.dCity = dCity;
    }

    public String getDState() {
        return dState;
    }

    public void setDState(String dState) {
        this.dState = dState;
    }

    public String getDZip() {
        return dZip;
    }

    public void setDZip(String dZip) {
        this.dZip = dZip;
    }

    public List<DepartmentRequestDto> getDepartmentsList() {
        return this.departmentsList;
    }

    /*Approver*/
    public String getAName() {
        return aName;
    }

    public void setAName(String aName) {
        this.aName = aName;
    }

    public String getAEmail() {
        return aEmail;
    }

    public void setAEmail(String aEmail) {
        this.aEmail = aEmail;
    }

    public String getAPhone() {
        return aPhone;
    }

    public void setAPhone(String aPhone) {
        this.aPhone = aPhone;
    }

    public String getsAddress1() {
        return sAddress1;
    }

    public void setsAddress1(String sAddress1) {
        this.sAddress1 = sAddress1;
    }

    public String getsAddress2() {
        return sAddress2;
    }

    public void setsAddress2(String sAddress2) {
        this.sAddress2 = sAddress2;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String sCity) {
        this.sCity = sCity;
    }

    public String getsState() {
        return sState;
    }

    public void setsState(String sState) {
        this.sState = sState;
    }

    public String getsZip() {
        return sZip;
    }

    public void setsZip(String sZip) {
        this.sZip = sZip;
    }

    public String getsDesktop() {
        return sDesktop;
    }

    public void setsDesktop(String sDesktop) {
        this.sDesktop = sDesktop;
    }

    public String getsDepartment() {
        return sDepartment;
    }

    public void setsDepartment(String sDepartment) {
        this.sDepartment = sDepartment;
    }

    public String getsCostCenter() {
        return sCostCenter;
    }

    public void setsCostCenter(String sCostCenter) {
        this.sCostCenter = sCostCenter;
    }

    public String getsComments() {
        return sComments;
    }

    public void setsComments(String sComments) {
        this.sComments = sComments;
    }

    public String getMailstop() {
        return mailstop;
    }

    public void setMailstop(String mailstop) {
        this.mailstop = mailstop;
    }

    public String getdMultipleDepts() {
        return dMultipleDepts;
    }

    public void setdMultipleDepts(String dMultipleDepts) {
        this.dMultipleDepts = dMultipleDepts;
    }

    public String getdName() {
        return dName;
    }

    public void setdName(String dName) {
        this.dName = dName;
    }

    public String getdDesktop() {
        return dDesktop;
    }

    public void setdDesktop(String dDesktop) {
        this.dDesktop = dDesktop;
    }

    public String getdCostCenter() {
        return dCostCenter;
    }

    public void setdCostCenter(String dCostCenter) {
        this.dCostCenter = dCostCenter;
    }

    public String getdSameAsDefault() {
        return dSameAsDefault;
    }

    public void setdSameAsDefault(String dSameAsDefault) {
        this.dSameAsDefault = dSameAsDefault;
    }

    public String getdAddress() {
        return dAddress;
    }

    public void setdAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public String getdCity() {
        return dCity;
    }

    public void setdCity(String dCity) {
        this.dCity = dCity;
    }

    public String getdState() {
        return dState;
    }

    public void setdState(String dState) {
        this.dState = dState;
    }

    public String getdZip() {
        return dZip;
    }

    public void setdZip(String dZip) {
        this.dZip = dZip;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String getaEmail() {
        return aEmail;
    }

    public void setaEmail(String aEmail) {
        this.aEmail = aEmail;
    }

    public String getaPhone() {
        return aPhone;
    }

    public void setaPhone(String aPhone) {
        this.aPhone = aPhone;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getBillLine1() {
        return billLine1;
    }

    public void setBillLine1(String billLine1) {
        this.billLine1 = billLine1;
    }

    public String getBillLine2() {
        return billLine2;
    }

    public void setBillLine2(String billLine2) {
        this.billLine2 = billLine2;
    }

    public String getBillCity() {
        return billCity;
    }

    public void setBillCity(String billCity) {
        this.billCity = billCity;
    }

    public String getBillState() {
        return billState;
    }

    public void setBillState(String billState) {
        this.billState = billState;
    }

    public String getBillZip() {
        return billZip;
    }

    public void setBillZip(String billZip) {
        this.billZip = billZip;
    }

    public String getBillContactName() {
        return billContactName;
    }

    public void setBillContactName(String billContactName) {
        this.billContactName = billContactName;
    }

    public String getBillContactEmail() {
        return billContactEmail;
    }

    public void setBillContactEmail(String billContactEmail) {
        this.billContactEmail = billContactEmail;
    }

    public String getBillContactPhone() {
        return billContactPhone;
    }

    public void setBillContactPhone(String billContactPhone) {
        this.billContactPhone = billContactPhone;
    }

    public String getLocationLocationName() {
        return locationLocationName;
    }

    public void setLocationLocationName(String locationLocationName) {
        this.locationLocationName = locationLocationName;
    }

    public String getLocationLocationId() {
        return locationLocationId;
    }

    public void setLocationLocationId(String locationLocationId) {
        this.locationLocationId = locationLocationId;
    }

    public String getLocationLine1() {
        return locationLine1;
    }

    public void setLocationLine1(String locationLine1) {
        this.locationLine1 = locationLine1;
    }

    public String getLocationLine2() {
        return locationLine2;
    }

    public void setLocationLine2(String locationLine2) {
        this.locationLine2 = locationLine2;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public void setLocationZip(String locationZip) {
        this.locationZip = locationZip;
    }

    public String getShipLocationName() {
        return shipLocationName;
    }

    public void setShipLocationName(String shipLocationName) {
        this.shipLocationName = shipLocationName;
    }

    public String getShipLocationId() {
        return shipLocationId;
    }

    public void setShipLocationId(String shipLocationId) {
        this.shipLocationId = shipLocationId;
    }

    public String getShipLine1() {
        return shipLine1;
    }

    public void setShipLine1(String shipLine1) {
        this.shipLine1 = shipLine1;
    }

    public String getShipLine2() {
        return shipLine2;
    }

    public void setShipLine2(String shipLine2) {
        this.shipLine2 = shipLine2;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipState() {
        return shipState;
    }

    public void setShipState(String shipState) {
        this.shipState = shipState;
    }

    public String getShipZip() {
        return shipZip;
    }

    public void setShipZip(String shipZip) {
        this.shipZip = shipZip;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerEmail() {
        return managerEmail;
    }

    public void setManagerEmail(String managerEmail) {
        this.managerEmail = managerEmail;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public void setDepartmentsList(List<DepartmentRequestDto> departmentsList) {
        this.departmentsList = departmentsList;
    }

    public boolean isNoManagerApproval() {
        return noManagerApproval;
    }

    public void setNoManagerApproval(boolean noManagerApproval) {
        this.noManagerApproval = noManagerApproval;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

}
