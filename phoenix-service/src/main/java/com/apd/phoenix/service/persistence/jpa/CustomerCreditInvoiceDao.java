package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 * Session Bean implementation class CustomerInvoiceDao
 */
@Stateless
@LocalBean
public class CustomerCreditInvoiceDao extends AbstractDao<CustomerCreditInvoice> {

    public List<CustomerCreditInvoice> getCreditInvoicesForOrder(CustomerOrder order) {
        String hql = "SELECT customerCreditInvoice FROM CustomerCreditInvoice AS customerCreditInvoice "
                + "WHERE customerCreditInvoice.returnOrder.order.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", order.getId());
        List<CustomerCreditInvoice> results = (List<CustomerCreditInvoice>) query.getResultList();
        if (results == null) {
            results = new ArrayList<CustomerCreditInvoice>();
        }
        return results;
    }

}
