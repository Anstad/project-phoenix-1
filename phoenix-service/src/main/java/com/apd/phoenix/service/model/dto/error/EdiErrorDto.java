package com.apd.phoenix.service.model.dto.error;

import java.util.List;

public class EdiErrorDto {

    private List<RelevantNumberDto> relevantNumbers;
    private List<ErrorDataDto> errorDataDtos;
    private String invoiceLine; //null for 832,855
    private String ediType;

    public List<ErrorDataDto> getErrorDataDtos() {
        return errorDataDtos;
    }

    public void setErrorDataDtos(List<ErrorDataDto> errorDataDtos) {
        this.errorDataDtos = errorDataDtos;
    }

    /**
     * @return the invoiceLine
     */
    public String getInvoiceLine() {
        return invoiceLine;
    }

    /**
     * @param invoiceLine the invoiceLine to set
     */
    public void setInvoiceLine(String invoiceLine) {
        this.invoiceLine = invoiceLine;
    }

    /**
     * @return the ediType
     */
    public String getEdiType() {
        return ediType;
    }

    /**
     * @param ediType the ediType to set
     */
    public void setEdiType(String ediType) {
        this.ediType = ediType;
    }

    /**
     * @return the relevantNumbers
     */
    public List<RelevantNumberDto> getRelevantNumbers() {
        return relevantNumbers;
    }

    /**
     * @param relevantNumbers the relevantNumbers to set
     */
    public void setRelevantNumbers(List<RelevantNumberDto> relevantNumbers) {
        this.relevantNumbers = relevantNumbers;
    }

}
