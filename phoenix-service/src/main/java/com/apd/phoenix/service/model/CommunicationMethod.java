/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;

/**
 * The Class CommunicationMethod.
 */
@Entity
@XmlRootElement
@Audited
public class CommunicationMethod implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -5060401005128151106L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    //Duplicate relation
    //   @ManyToMany
    //   private Set<Vendor> vendors = new HashSet<Vendor>();

    @ManyToMany(mappedBy = "communicationMethods", fetch = FetchType.LAZY)
    private Set<Vendor> vendors = new HashSet<Vendor>();

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CommunicationMethod) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /** The name. */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty())
            result += name;
        return result;
    }

    public Set<Vendor> getVendors() {
        return this.vendors;
    }

    public void setVendors(final Set<Vendor> vendors) {
        this.vendors = vendors;
    }

    //   public Set<Vendor> getVendors()
    //   {
    //      return this.vendors;
    //   }
    //
    //   public void setVendors(final Set<Vendor> vendors)
    //   {
    //      this.vendors = vendors;
    //   }
}