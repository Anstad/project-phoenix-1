package com.apd.phoenix.service.persistence.jpa;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.HierarchyNode;

/**
 * HierarchyNode DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class HierarchyNodeDao extends AbstractDao<HierarchyNode> {

    @SuppressWarnings("unchecked")
    public List<HierarchyNode> getChildren(HierarchyNode hierarchyNode) {
        if (hierarchyNode != null) {
            //TODO:Check the skutype?
            String hql = "from HierarchyNode h where h.parent = :node";
            Query query = entityManager.createQuery(hql);
            query.setParameter("node", hierarchyNode);
            List<HierarchyNode> results = query.getResultList();
            return results;
        }
        return new ArrayList<HierarchyNode>();
    }

    @SuppressWarnings("unchecked")
    public List<HierarchyNode> searchByPublicIdAndVersion(Long publicId, int version) {
        String hql = "select h from HierarchyNode h where h.publicId = :publicId and h.version = :version";
        Query query = entityManager.createQuery(hql);
        query.setParameter("publicId", publicId);
        query.setParameter("version", version);
        List<HierarchyNode> results = query.getResultList();
        return results;
    }
}
