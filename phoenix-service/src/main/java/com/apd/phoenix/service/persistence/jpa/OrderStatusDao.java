package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.lang.String;
import java.util.List;
import javax.persistence.Query;
import com.apd.phoenix.service.model.OrderStatus;

/**
 * OrderStatus DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class OrderStatusDao extends AbstractDao<OrderStatus> {

    @SuppressWarnings("unchecked")
    public List<OrderStatus> getStatusList() {
        String hql = "SELECT s FROM OrderStatus AS s";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }

    public OrderStatus getOrderStatus(Long id) {
        String hql = "SELECT s FROM OrderStatus AS s WHERE s.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return (OrderStatus) query.getSingleResult();
    }

    public OrderStatus getOrderStatusByValue(String value) {
        String hql = "SELECT s FROM OrderStatus AS s WHERE s.value=:value";
        Query query = entityManager.createQuery(hql);
        query.setParameter("value", value);
        return (OrderStatus) query.getSingleResult();
    }
}
