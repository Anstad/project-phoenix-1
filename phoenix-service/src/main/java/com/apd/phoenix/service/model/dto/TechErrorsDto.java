package com.apd.phoenix.service.model.dto;

public class TechErrorsDto {

    private String techErrorCode; // TED error code
    private String techErrorDescription; // TED error description

    public String getTechErrorCode() {
        return techErrorCode;
    }

    public void setTechErrorCode(String techErrorCode) {
        this.techErrorCode = techErrorCode;
    }

    public String getTechErrorDescription() {
        return techErrorDescription;
    }

    public void setTechErrorDescription(String techErrorDescription) {
        this.techErrorDescription = techErrorDescription;
    }
}
