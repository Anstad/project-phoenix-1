package com.apd.phoenix.service.invoice;

import java.math.BigDecimal;

public class ShippedLineItem extends MessageLineItem {

    /**
     * 
     */
    private static final long serialVersionUID = 240155861828936043L;
    private String trackingNumber;

    public ShippedLineItem(int quantity, String vendorSku, String apdSku, BigDecimal price, String status,
            String trackingNumber) {
        super(quantity, vendorSku, apdSku, price, status);
        this.trackingNumber = trackingNumber;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

}
