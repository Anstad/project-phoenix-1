package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.TaxRateLog;
import com.apd.phoenix.service.model.ZipPlusFour;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogAction;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogStatus;
import com.apd.phoenix.service.model.TaxRateLog.TaxRateLogType;
import com.apd.phoenix.service.persistence.jpa.ZipPlusFourDao;

/**
 * This class provides business process methods for ZipPlusFour.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ZipPlusFourBp extends AbstractBp<ZipPlusFour> {

    @Inject
    private TaxRateLogBp logBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ZipPlusFourDao dao) {
        this.dao = dao;
    }

    public String countyFipsFromValues(String zip, String plusFour) {
        return ((ZipPlusFourDao) dao).countyFipsFromValues(zip, plusFour);
    }

    public void createZipPlusFours(List<ZipPlusFour> zips) {
        for (ZipPlusFour o : zips) {
            if (o == null) {
                continue;
            }
            try {
                this.create(o);
                logBp.create(new TaxRateLog(TaxRateLogType.ZIP_PLUS_FOUR, TaxRateLogAction.INSERT,
                        TaxRateLogStatus.SUCCESS, o.getZip(), o.getCountyFips()));
            }
            catch (Exception e) {
                LOG.error("Error when adding zip plus four, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.ZIP_PLUS_FOUR, TaxRateLogAction.INSERT,
                        TaxRateLogStatus.FAILURE, o.getZip(), o.getCountyFips()));
            }
        }
    }

    public void deleteZipPlusFour(ZipPlusFour zip) {
        ZipPlusFour searchZip = new ZipPlusFour();
        searchZip.setCountyFips(zip.getCountyFips());
        searchZip.setHi(zip.getHi());
        searchZip.setLow(zip.getLow());
        searchZip.setZip(zip.getZip());
        List<ZipPlusFour> searchList = this.searchByExactExample(searchZip, 0, 0);
        for (ZipPlusFour foundZip : searchList) {
            try {
                this.delete(foundZip.getId(), ZipPlusFour.class);
                logBp.create(new TaxRateLog(TaxRateLogType.ZIP_PLUS_FOUR, TaxRateLogAction.DELETE,
                        TaxRateLogStatus.SUCCESS, zip.getZip(), zip.getCountyFips()));
            }
            catch (Exception e) {
                LOG.error("Error when deleting zip plus four, enable debug logging to view stack trace");
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Stack trace", e);
                }
                logBp.create(new TaxRateLog(TaxRateLogType.ZIP_PLUS_FOUR, TaxRateLogAction.DELETE,
                        TaxRateLogStatus.FAILURE, zip.getZip(), zip.getCountyFips()));
            }
        }

    }
}
