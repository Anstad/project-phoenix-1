package com.apd.phoenix.service.persistence.jpa;

import java.math.BigDecimal;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CustomerCost;

/**
 * CustomerCost DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CustomerCostDao extends AbstractDao<CustomerCost> {

    @SuppressWarnings("unchecked")
    public List<Account> costList(Catalog catalog) {
        String hql = "SELECT DISTINCT account FROM Item AS item JOIN item.customerCosts as cost JOIN cost.customer AS account "
                + "WHERE item.vendorCatalog.id = :id";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", catalog.getId());
        return query.getResultList();
    }

    public BigDecimal getItemCost(Long itemId, Long customerId) {
        String hql = "SELECT cost.value FROM CustomerCost cost WHERE cost.item.id = :itemId AND cost.customer.id = :customerId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("itemId", itemId);
        query.setParameter("customerId", customerId);
        return getSingleResultOrNull(query);
    }
}
