package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.envers.Audited;

/**
 * The Class Permission.
 */
@Entity
@Cacheable
@XmlRootElement
@Audited
public class Permission implements Serializable, com.apd.phoenix.service.model.Entity {

    /**
     * 
     */
    private static final long serialVersionUID = 648902207162405334L;

    /** The allow delete. */
    @Column(nullable = false)
    @Deprecated
    private boolean allowDelete;

    /** The description. */
    @Column
    private String description;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The name. */
    @Column(nullable = false)
    private String name;

    /** The allow read. */
    @Column(nullable = false)
    @Deprecated
    private boolean allowRead;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    /** The allow write. */
    @Column(nullable = false)
    @Deprecated
    private boolean allowWrite;

    @ManyToMany(mappedBy = "permissions")
    private Set<Role> roles = new HashSet<Role>();

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Permission) that).id);
        }
        return super.equals(that);
    }

    /**
     * Gets the allow delete.
     *
     * @deprecated
     * @return the allow delete
     */
    @Deprecated
    public boolean getAllowDelete() {
        return this.allowDelete;
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the allow read.
     * 
     * @deprecated
     * @return the allow read
     */
    @Deprecated
    public boolean getAllowRead() {
        return this.allowRead;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Gets the allow write.
     *
     * @deprecated
     * @return the allow write
     */
    @Deprecated
    public boolean getAllowWrite() {
        return this.allowWrite;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Sets the allow delete.
     *
     * @deprecated
     * @param allowDelete the new allow delete
     */
    @Deprecated
    public void setAllowDelete(final boolean allowDelete) {
        this.allowDelete = allowDelete;
    }

    /**
     * Sets the description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the allow read.
     *
     * @deprecated
     * @param allowRead the new allow read
     */
    @Deprecated
    public void setAllowRead(final boolean allowRead) {
        this.allowRead = allowRead;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /**
     * Sets the allow write.
     *
     * @deprecated
     * @param allowWrite the new allow write
     */
    @Deprecated
    public void setAllowWrite(final boolean allowWrite) {
        this.allowWrite = allowWrite;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty())
            result += name;
        return result;
    }

    public Set<Role> getRoles() {
        return this.roles;
    }

    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }
}
