package com.apd.phoenix.service.model.dto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class PurchaseOrderRequestDto {

    private PurchaseOrderDto customerOrderDto;

    public class APDPurchaseOrder {

        private String apdOrderNumber;//
        private String customerPONumber;//
        private String wrapAndLabelIndicatorRefNumber;
        private String transactionSetPurposeCode;
        private String purchaseOrderTypeCode;
        private Date purchaseOrderDate;
        private String adotIndicator;
        private String carrierDetails;
        private String routing;

        private String shipTo;
        private String billTo;
        public Collection<String> purchaseOrderLineItems;
    }

    public class EDIPurchaseOrder {

        private String transactionSetPurposeCode; // '00' = Original
        private String purchaseOrderTypeCode; // 'NE = New Order, 'OS' = Will Call Order
        private String releaseNumber;
        private String purchaseOrderNumber;
        private Date purchaseOrderDate;
        private String contractNumber;
        private String acknowledgementType;
        private String invoiceTypeCode;
        private String contractTypeCode;
        private String purchaseCategory;
        private String securityLevelCode;
        private String transactionTypeCode;

        private Collection<ReferenceNumberDto> referenceNumbers;

        public class ReferenceNumberDto {

            private String referenceIdentificationQualifier;
            private String referenceIdentification;
            private String referenceDescription;
        }

        private Collection<RoutingCarrierDetails> routingCarrierDetails;

        public class RoutingCarrierDetails {

            private String routingSequenceCode;
            private String identificationCodeQualifier;
            private String identificationCode;
            private String transportationMethod;
            private String routing;
            private String shipmentOrderStatusCode;
        }

        private Collection<ReferenceIdentification> referenceIds;

        public class ReferenceIdentification {

            private String referenceIdentificationQualifier; // 'L1' = Letters and Notes
            private String referenceIdentification;
            //		If N901 is ?L1?, then N902 must be:
            //		?SPH?=Spec Handling Instructions
            //		    (max 2)
            //		?LAB?=Label Instructions(max 6)
            //		?ORI?=Dealer Information(max 6)
            private String freeFormMessage; // Used only if N901 is 'L1' and N902 is 'SPH', 'LAB' or 'ORI'
            private String date;
            private String time;
            private String timeCode;
        }

        private Collection<ShippingBillingDetails> shippingAndBilling;

        public class ShippingBillingDetails {

            //private Name name;
            private String entityIdentifierCode;
            private String name;
            private String identificationCodeQualifier;
            private String identificationCode;
            private String entityRelationshipCode;
            private String entityIdentificationCode;
            private String additionalName;
            //private AddressInformation addressInformation;
            private String addressLine1;
            private String addressLine2;
            //private GeographicLocation geographicLocation
            private String cityName;
            private String stateOrProvinceCode;
            private String postalCode;
            private String countryCode;
            private String locationQualifier;
            private String locationIdentifier;

            private Collection<ContactDto> contacts;

            public class ContactDto {

                private String contactFunctionCode;
                private String name;
                private String communicationNumberQualifier;
                private String communicationNumber;
                private String communicationNumberQualifier2;
                private String communicationNumber2;
                private String communicationNumberQualifier3;
                private String communicationNumber3;
                private String contactInquiryReference;
            }
        }

        private Collection<LineItemDto> lineItems;

        public class LineItemDto {

            //POBaselineItemData
            private String assignedIdentifier; // Line number (USSCO system will truncate after 6 bytes).
            private BigInteger quantity;
            private String unitOfMeasure;
            private BigDecimal unitPrice;
            private String basisUnitPriceCode;
            private String prodServiceIDQualifier1;
            private String prodServID1; // Your Item number
            private String prodServIDQual2; // 'VN'=vendor's item number
            private String prodServID2; // USSCO’s item number
            private String prodServIDQual3;
            private String prodServID3;
            private String prodServIDQual4;
            private String prodServID4;
            private String prodServIDQual5;
            private String prodServID5;
            private String prodServIDQual6;
            private String prodServID6;
            private String prodServIDQual7;
            private String prodServID7;
            private String prodServIDQual8;
            private String prodServID8;
            private String prodServIDQual9;
            private String prodServID9;
            private String prodServIDQual10;
            private String prodServID10;
            // ProductItemDescription
            private String itemDescriptionType;
            private String productCharacteristicCode;
            private String agencyQualifierCode;
            private String productDescriptionCode;
            private String description;
            private String surfaceLayerPositionCode;
            private String sourceSubQualifier;
            private String yesNoConditionOrResponseCode;
            private String languageCode;

            private ReferenceIdentification referenceId;
        }

        //TransactionSetTotals
        private Integer numberofLineItems;
        private String hashTotal; // Number of items ordered (total of PO102 element(s))
        private String weight;
        private String weightUnitOfMeasure;
        private String volume;
        private String volumeUnitOfMeasure;
        private String description;

        //TransactionSetTrailer
        private String numberOfIncludedSegments;
        private String transactionControlNumber;
    }

    public class CXMLOrderRequest {

        private String orderID;
        private Date orderDate;
        private String orderType; // regular, release (a release against a master agreement), or blanket (a blanket purchase order).
        // blanket order
        private String releaseRequired; //If the order is a blanket purchase order, the releaseRequired attribute indicates whether the blanket order requires releases (purchase orders). If “no” is specified, the blanket order does not require purchase orders and can be directly billed against. The default is “yes.”
        private Date effectiveDate; //indicates the date that the blanket purchase order is available for ordering.
        private Date expirationDate; //the date that the blanket purchase order is no longer available. If no value is defined, the end date can be indefinite. 
        private String addressId; //the buying organization’s shipping or billing address defined in the ShipTo and BillTo elements of OrderRequest. This is also the buying organization’s Extrinsic “UniqueName” for ShipTo/BillTo.
        //The Address element contains a PostalAddress and is similar to the Contact element, except that Contact can describe multiple methods for contacting a particular person.
        private String name; //For ship to, this value should be the company or organization of the employee receiving ordered products. For bill to, this is the department or group responsible for payment. Name is not as specific as the location referenced in the second DeliverTo line.
        //<Name xml:lang="en">Workchairs, Inc.</Name>
        private String deliverToLine1;
        /*This element is the name of the person receiving the ordered products. Avoid empty or white space elements and attributes. Missing values might affect EDI and cXML suppliers.
        The suggested implementation name format template is “firstname lastname.”
        Note: Both DeliverTo lines in the BillTo element should be in the same format for consistency. This logic is not in the existing templates; it is recommended that it be added manually.
        <DeliverTo>Joe Smith</DeliverTo> */

        private String deliverToLine2;
        /*This element is the location (building, city, office, or mailstop) of the person receiving the ordered products. Locations should always be complete enough for a mailing label.
        There should be consistency between uses of the same element, particularly ShipTo and BillTo. <DeliverTo>Mailstop M-543</DeliverTo>
         */

        private String street;
        /*If there are multiple lines in the street address, use up to four Street elements instead of inserting new lines.
         * */

        private String city;

        private String State; //two letters

        private String postalCode; //do not use a dash (-) in US extended zipcodes
        private String carrierIdentifier; // carrier name for a shipment
        // for example: <CarrierIdentifier domain="companyName">UPS</CarrierIdentifier>

        private TransportInformation transportInformation;

        public class TransportInformation {

            private String routeMethod; //air, motor, rail, or ship
            private String shippingContractNumber; //The contract number associated to the contract for sale.
            private List<String> shippingInstructions;
        }

        private String country;
        private String telephoneNumber; //areacode, country code, and number elements
        private String fax; // wraps the telephoneNumber element
        private String email;
        // terms of delivery
        private String termsOfDeliveryCode; //Standard delivery terms and Incoterms.
        private String shippingPaymentMethod;
        /*Denotes the mode of shipping payment.
        	Account: When shipping charges are charged to an account. Collect: When the consignee pays the freight charges.
        	Prepaid by Seller: When the seller makes the payment to the carrier for freight charges prior to a shipment. Mixed: When the consignment is partially Collect and partially Prepaid.
        	Other: Any other shipping payment method or the third-party pays the shipment charges. You can enter additional information for the payment method.
         */
        private String transportTerms;
        /*Free-Carrier
         * CostAndFreight 
         * DeliveredAtFrontier	
         * Other: When you specify this option, you can additionally enter a description.
         */
        private String address; // The Deliver To address for the ship notice
        private List<String> comments; //Additional information for the delivery terms
        private String tax;
        /*This element is the tax associated with the purchase order. This element is present if the buying organization computes tax. When appearing within the OrderRequestHeader, Tax describes the total tax for an order. Tax elements at the item level can describe line level tax amounts.
        <Tax> <Money currency="USD">1.34</Money> <Description xml:lang="en">Sales Tax</Description>
        </Tax>*/

        private Collection<ItemOut> items;

        public class ItemOut {

            private String supplierPartId;
            private String supplierPartAuxId;
            private BigDecimal price;
            private String tax;
            private String uom;
        }
    }

}
