package com.apd.phoenix.service.report;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.apache.commons.collections.MultiHashMap;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;

@Stateless
@LocalBean
public class ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);
    private static final String PROPERTIES_FILE = "report.service";
    private static final String PROTOCOL = "http";
    private static final String IP = "localhost";
    private static final String USERNAME = "superuser";
    private static final String PASSWORD = "";
    private static final String DIRECTORY = "/jasperserver-pro/rest_v2/reports/Reports/APD/";
    private static final int PDF_REQUEST_TIMEOUT = 30 * 1000;
    private static final int LARGE_PDF_REQUEST_TIMEOUT = 15 * 60 * 1000;
    private static final int CSV_REQUEST_TIMEOUT = 30 * 60 * 1000;
    private static final String CONTACT_CUSTOMER_SERVICE_PDF_FILENAME = "contact_customer_service_PDF.pdf";

    public InputStream generateBackorderNoticePdf(String apdPo) {
        String reportName = "notifications/backorder-notice-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("apd_po", apdPo);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCardDebitPdf(String transactionId) {
        String reportName = "notifications/card-debit-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("TRANSACTION_ID", transactionId);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCardCreditPdf(String transactionId, String orderId) {
        String reportName = "notifications/card-credit-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("TRANSACTION_ID", transactionId);
        params.put("RETURNORDER_ID", orderId);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateInvoiceDebitPdf(String invoiceId) {
        String reportName = "notifications/invoice-debit-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("INVOICE_ID", invoiceId);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateInvoiceCreditPdf(String invoiceId) {
        String reportName = "notifications/invoice-credit-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("INVOICE_ID", invoiceId);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateOrderAcknowledgmentPdf(String apdPo) {
        String reportName = "notifications/order-ack-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("APD_PO", apdPo);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateUspsOrderAcknowledgmentPdf(String apdPo) {
        String reportName = "notifications/usps-order-ack-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("APD_PO", apdPo);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateApReport() {
        String reportName = "invoice-reports/ap-report";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date endDate = c.getTime();
        c.add(Calendar.DATE, -1);
        Date startDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("START_DATE", sdf.format(startDate));
        params.put("END_DATE", sdf.format(endDate));
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateArReport() {
        String reportName = "invoice-reports/ar-report";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        Date endDate = c.getTime();
        c.add(Calendar.DATE, -1);
        Date startDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("START_DATE", sdf.format(startDate));
        params.put("END_DATE", sdf.format(endDate));
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateShipManifestPdf(String rootAccount) {
        String reportName = "notifications/remote-xml-example";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        if (Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
            c.add(Calendar.DATE, 3);
        }
        else {
            c.add(Calendar.DATE, 1);
        }
        Date deliveryDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.LARGE_PDF;
        MultiMap params = new MultiHashMap();
        params.put("ROOT_ACCOUNT_NAME", rootAccount);
        params.put("DELIVERY_DATE", sdf.format(deliveryDate));
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateShipManifestCsv(String rootAccount) {
        String reportName = "notifications/ShipManifest_detail";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        if (Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
            c.add(Calendar.DATE, 3);
        }
        else {
            c.add(Calendar.DATE, 1);
        }
        Date deliveryDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("ROOT_ACCOUNT_NAME", rootAccount);
        params.put("DELIVERY_DATE", sdf.format(deliveryDate));
        return retrieveReport(reportName, suffix, params);
    }

    @SuppressWarnings("unchecked")
	private InputStream retrieveReport(String reportName, FileType type, MultiMap params) {
        Properties properties = PropertiesLoader.getAsProperties(PROPERTIES_FILE);
        String protocol = properties.getProperty("protocol", PROTOCOL);
        String ip = properties.getProperty("ip", IP);
        String username = properties.getProperty("username", USERNAME);
        String password = properties.getProperty("password", PASSWORD);
        String report_dir = properties.getProperty("report_dir", DIRECTORY);

        params.put("j_username", username);
        params.put("j_password", password);
        StringBuilder restUrl = new StringBuilder(protocol + "://" + ip + report_dir + reportName + type.getValue() + "?");
        for (String key : (Collection<String>)params.keySet()) {
        	for (String value : (Collection<String>)params.get(key)) {
        		restUrl.append(key + "=" + StringEscape.escapeForUrl(value) + "&");
        	}
        }

        //Remove the trailing '&'
        restUrl.deleteCharAt(restUrl.length() - 1);
        
        try {
            return this.getReport(restUrl.toString(), type.getSocketTimeout());
        }
        catch (IOException | RuntimeException e) {
            LOGGER.error("There was an error generating the report", e);
            LOGGER.info("Retrieving " + CONTACT_CUSTOMER_SERVICE_PDF_FILENAME);
            String s3 = PropertiesLoader.getAsProperties("aws.integration.properties").getProperty("content.service.secure.domain");
            try {
                return this.getReport(s3 + "/" + CONTACT_CUSTOMER_SERVICE_PDF_FILENAME, type.getSocketTimeout());
            } catch (IOException | RuntimeException ex) {
                LOGGER.error("There was an error retrieving " + CONTACT_CUSTOMER_SERVICE_PDF_FILENAME, ex);
            }
        }
        //TODO pass exception up the call stack instead of possibly returning null
        LOGGER.warn("There was an error generating the report, returning null.");
        return null;
    }

    private InputStream getReport(String url, int millisecondTimeout) throws ClientProtocolException, IOException {
        LOGGER.info("Getting content from '{}'", url);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet getRequest = new HttpGet(url);
        getRequest.addHeader("accept", "application/pdf");
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(millisecondTimeout).setSocketTimeout(
                millisecondTimeout).build();
        getRequest.setConfig(requestConfig);
        HttpResponse response = null;
        response = httpClient.execute(getRequest);
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
        }
        return response.getEntity().getContent();
    }

    public InputStream generatePurchaseOrderNoticePdf(String apdPo, Long vendorId) {
        String reportName = "notifications/vendor-po-notification-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("apd_po", apdPo);
        params.put("vendor_id", vendorId + "");
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateShipmentNoticePdf(String trackingNo) {
        //TODO: fix the shipment notice PDF report, per PHOEN-3844. Once it's complete, remove 
        //the following lines, and uncomment the lines afterward.
        LOGGER.warn("Skipping generation of the Shipment Notice PDF, until PHOEN-3844 is resolved.");
        try {
            String s3 = PropertiesLoader.getAsProperties("aws.integration.properties").getProperty(
                    "content.service.secure.domain");
            return this.getReport(s3 + "/" + CONTACT_CUSTOMER_SERVICE_PDF_FILENAME, FileType.PDF.getSocketTimeout());
        }
        catch (Exception e) {
            return null;
        }

        //String reportName = "shipment-notification-pdf";
        //String suffix = ".pdf";
        //MultiMap params = new MultiHashMap();
        //params.put("tracking_np", trackingNo);
        //return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCreditCardDeclinedNoticePdf(String apdPo) {
        String reportName = "notifications/p-card-denial-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("apd_po", apdPo);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateLateShipmentNoticePdf(String apdPo) {
        String reportName = "notifications/late-shipment-notification-pdf";
        FileType suffix = FileType.PDF;
        MultiMap params = new MultiHashMap();
        params.put("apd_po", apdPo);
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCustomerServiceArReport(List<String> accounts, Date startDate, Date endDate) {
        String reportName = "invoice-reports/ar-report";
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("START_DATE", sdf.format(startDate));
        params.put("END_DATE", sdf.format(endDate));
        if (accounts != null) {
            for (String account : accounts) {
                if (StringUtils.isNotBlank(account)) {
                    params.put("ROOT_ACCOUNT_NAME", account);
                    //If only one parameter is used, jasper will interpret it as a string, rather than a list
                    params.put("ROOT_ACCOUNT_NAME", " ");
                }
            }
        }
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCustomerServiceShipmasterReport(List<String> accounts, Date date) {
        String reportName = "shipmaster-report-shipments";
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        Date startDate = c.getTime();
        c.add(Calendar.DATE, 1);
        Date endDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("START_DATE", sdf.format(startDate));
        params.put("END_DATE", sdf.format(endDate));
        if (accounts != null) {
            for (String account : accounts) {
                if (StringUtils.isNotBlank(account)) {
                    params.put("ROOT_ACCOUNT_NAME", account);
                    //If only one parameter is used, jasper will interpret it as a string, rather than a list
                    params.put("ROOT_ACCOUNT_NAME", " ");
                }
            }
        }
        params.put("DATE_TYPE", "ship");
        return retrieveReport(reportName, suffix, params);
    }

    public InputStream generateCustomerServiceDailyChargeReport(List<String> accounts, List<String> transactionTypes,
            String poNumber, Date startDate, Date endDate) {
        String reportName = "daily-charge-report";
        Calendar c = Calendar.getInstance();
        c.setTime(endDate);
        c.add(Calendar.DATE, 1);
        endDate = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        FileType suffix = FileType.CSV;
        MultiMap params = new MultiHashMap();
        params.put("START_DATE", sdf.format(startDate));
        params.put("END_DATE", sdf.format(endDate));
        if (accounts != null) {
            for (String account : accounts) {
                if (StringUtils.isNotBlank(account)) {
                    params.put("ROOT_ACCOUNT_NAME", account);
                    //If only one parameter is used, jasper will interpret it as a string, rather than a list
                    params.put("ROOT_ACCOUNT_NAME", " ");
                }
            }
        }
        if (transactionTypes != null) {
            for (String transactionType : transactionTypes) {
                if (StringUtils.isNotBlank(transactionType)) {
                    params.put("TRANSACTION_TYPES", transactionType);
                    //If only one parameter is used, jasper will interpret it as a string, rather than a list
                    params.put("TRANSACTION_TYPES", " ");
                }
            }
        }
        if (StringUtils.isNotBlank(poNumber)) {
            params.put("APD_PO", poNumber);
        }
        return retrieveReport(reportName, suffix, params);
    }

    private enum FileType {
        PDF(".pdf", PDF_REQUEST_TIMEOUT), LARGE_PDF(".pdf", LARGE_PDF_REQUEST_TIMEOUT), CSV(".csv", CSV_REQUEST_TIMEOUT);

        private String value;

        private int socketTimeout;

        private FileType(String value, int socketTimeout) {
            this.value = value;
            this.socketTimeout = socketTimeout;
        }

        public String getValue() {
            return this.value;
        }

        public int getSocketTimeout() {
            return this.socketTimeout;
        }
    }
}
