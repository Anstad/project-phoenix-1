/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class ManifestSuccessDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private List<String> errors;
    private String success;
    private List<ShipmentUpdateDto> shipments;

    /**
     * @return the errors
     */
    public List<String> getErrors() {
        return errors;
    }

    /**
     * @param errors the errors to set
     */
    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    /**
     * @return the success
     */
    public String getSuccess() {
        return success;
    }

    /**
     * @return the shipments
     */
    public List<ShipmentUpdateDto> getShipments() {
        return shipments;
    }

    /**
     * @param shipments the shipments to set
     */
    public void setShipments(List<ShipmentUpdateDto> shipments) {
        this.shipments = shipments;
    }
}
