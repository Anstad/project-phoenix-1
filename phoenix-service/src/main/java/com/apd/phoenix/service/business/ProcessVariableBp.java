package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ProcessVariable;
import com.apd.phoenix.service.persistence.jpa.ProcessVariableDao;

/**
 * This class provides business process methods for ProcessVariable.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ProcessVariableBp extends AbstractBp<ProcessVariable> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ProcessVariableDao dao) {
        this.dao = dao;
    }
}
