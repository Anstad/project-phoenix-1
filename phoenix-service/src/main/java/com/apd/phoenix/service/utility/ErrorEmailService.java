/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.utility;

import java.io.IOException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.swing.Box.Filler;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.impl.EmailMessageSender;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.dto.error.EdiErrorDto;
import com.apd.phoenix.service.model.dto.error.RelevantNumberDto;
import freemarker.template.TemplateException;

/**
 *
 * @author nreidelb
 */

@Stateless
@LocalBean
public class ErrorEmailService {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ErrorEmailService.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    EmailMessageSender emailMessageSender;

    @Inject
    EmailFactoryBp emailFactoryBp;

    public void createErrorEmail(EdiDocumentErrorsDto errorDto) {
        CustomerOrder customerOrder = null;
        for (EdiErrorDto error : errorDto.getEdiErrorDtos()) {
            for (RelevantNumberDto number : error.getRelevantNumbers()) {
                customerOrder = shipmentBp.findCustomerOrderBy824RelevantNumber(customerOrder, number);
            }
        }
        try {
            Message message = emailFactoryBp.createEDIErrorEmail(errorDto, customerOrder);
            emailMessageSender.sendMessage(message, null);
        }
        catch (IOException ex) {
            LOG.error(ex.getMessage());
        }
        catch (TemplateException ex) {
            LOG.error(ex.getMessage());
        }
    }

    public void sendEDIOrderNotFoundEmail(MessageDto messageDto, String token, String messageEventType) {
        Message message = emailFactoryBp.createEDIOrderNotFoundEmail(messageDto, token, messageEventType);
        emailMessageSender.sendMessage(message, null);
    }

    public void sendSectorValidationFailedEmail(PurchaseOrderDto po, String partnerId) {
        Message message = emailFactoryBp.createSectorValidationFailedEmail(po, partnerId);
        emailMessageSender.sendMessage(message, null);
    }

    public void sendDuplicateCustomerPOErrorEmail(CustomerOrder order, String customerPo){
        try {
        	Message message = emailFactoryBp.createDuplicateCustomerPoEmail(order,customerPo);
            emailMessageSender.sendMessage(message, null);
        } catch (TemplateException | IOException ex) {
            LOG.error("Failed to send error email concerning Duplicate Customer Po." + ex.getMessage());
        }
    }

    public void sendWorkflowResubmittedErrorEmail(CustomerOrder order){
        try {
        	Message message = emailFactoryBp.createWorkflowResubmittedEmail(order);
            emailMessageSender.sendMessage(message, null);
        } catch (TemplateException | IOException ex) {
            LOG.error("Failed to send error email concerning Duplicate Customer Po." + ex.getMessage());
        }
    }

    public void create864Email(String body, String partnerId, String fileName) {
        LOG.debug("retrieved String" + body);
        Message message = emailFactoryBp.createEDI864ErrorEmail(body, partnerId, fileName);
        emailMessageSender.sendMessage(message, null);

    }

    public void sendMarquetteErrorEmail(InvoiceDto invoice, String exception) {
        Message message = emailFactoryBp.createMarquetteErrorEmail(invoice, exception);
        emailMessageSender.sendMessage(message, null);
    }
}
