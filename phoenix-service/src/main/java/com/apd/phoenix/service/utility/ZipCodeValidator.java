/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.utility;

import com.apd.phoenix.service.business.TaxRateBp;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dnorris
 */
@FacesValidator("zipCodeValidator")
public class ZipCodeValidator implements Validator {

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (!(value instanceof String)) {
            throw new ValidatorException(new FacesMessage("Please enter a valid zip code."));
        }
        String zipCode = (String) value;
        if (StringUtils.isNotBlank(zipCode)) {
            try {
                InitialContext initContext = new InitialContext();
                TaxRateBp taxRateBp = (TaxRateBp) initContext.lookup("java:module/TaxRateBp");
                if (!taxRateBp.isValidZipCode(zipCode)) {
                    throw new ValidatorException(new FacesMessage("Please enter a valid zip code."));
                }
            }
            catch (NamingException ex) {
                throw new ValidatorException(new FacesMessage("Internal Error while trying to validate zip code."));
            }
        }
    }

}
