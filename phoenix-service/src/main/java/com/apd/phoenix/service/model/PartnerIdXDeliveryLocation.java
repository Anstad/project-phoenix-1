/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Index;
import org.hibernate.envers.Audited;

/**
 * The Class PartnerIdXDeliveryLocation.
 */
@Entity
@Cacheable
@XmlRootElement
@Audited
public class PartnerIdXDeliveryLocation implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The partnerId. */
    @Column(nullable = false, unique = true)
    @Index(name = "PARTNERIDXDELVLOC_PARTNERID")
    private String partnerId;

    /** The name. */
    @Column(nullable = false)
    @Index(name = "PARTNERIDXDELVLOC_LOCATION")
    private String deliveryLocation;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     * 
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     * 
     * @param version
     *            the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PartnerIdXDeliveryLocation) that).id);
        }
        return super.equals(that);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the action type partnerId.
     * 
     * @return the action partnerId
     */
    public String getPartnerId() {
        return this.partnerId;
    }

    /**
     * Sets the partnerId.
     * 
     * @param partnerId
     *            the new partnerId
     */
    public void setPartnerId(final String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * Gets the deliveryLocation.
     * 
     * @return the deliveryLocation
     */
    public String getDeliveryLocation() {
        return this.deliveryLocation;
    }

    /**
     * Sets the deliveryLocation.
     * 
     * @param deliveryLocation
     *            the new deliveryLocation
     */
    public void setDeliveryLocation(final String deliveryLocation) {
        this.deliveryLocation = deliveryLocation;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = "";
        if (deliveryLocation != null && !deliveryLocation.trim().isEmpty()) {
            result += deliveryLocation;
        }
        return result;
    }
}
