package com.apd.phoenix.service.brms.api;

import org.jbpm.task.service.TaskService;

public interface TaskServiceServerFactory {

    public TaskService createTaskServiceServer();
}
