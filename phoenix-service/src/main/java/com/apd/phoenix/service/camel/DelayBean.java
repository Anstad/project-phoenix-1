package com.apd.phoenix.service.camel;

import java.util.Date;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.camel.routepolicy.quartz.SimpleScheduledRoutePolicy;
import org.apache.camel.spi.RoutePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;

/**
 * Used to generate the RoutePolicy for delaying queue startup.
 * 
 * @author RHC
 *
 */
@Startup
@Singleton
public class DelayBean {

    private static final Logger LOG = LoggerFactory.getLogger(DelayBean.class);

    //at deployment, finds the time and the amount after deployment 
    //the messages should be delayed
    private static final long DEPLOY_TIME = System.currentTimeMillis();
    private static final long DEPLOY_DELAY = Long.parseLong(PropertiesLoader.getAsProperties("amq").getProperty(
            "queueConsumeDelay", "0")) * 1000;

    public static RoutePolicy getDelayPolicy() {

        SimpleScheduledRoutePolicy policy = new SimpleScheduledRoutePolicy();
        long startTime = DEPLOY_TIME + DEPLOY_DELAY;
        policy.setRouteStartDate(new Date(startTime));
        policy.setRouteStartRepeatCount(0);
        policy.setRouteStartRepeatInterval(0);

        LOG.info("Starting consumer at " + policy.getRouteStartDate());

        return policy;
    }
}
