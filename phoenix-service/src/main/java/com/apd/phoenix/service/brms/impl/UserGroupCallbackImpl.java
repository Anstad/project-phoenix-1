/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.brms.impl;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jbpm.task.service.UserGroupCallback;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.RoleBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Role;
import com.apd.phoenix.service.model.SystemUser;

@Stateless
@LocalBean
public class UserGroupCallbackImpl implements UserGroupCallback {

    private static final String CUSTOMER_SERVICE = " Customer Service";

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    RoleBp roleBp;

    @Inject
    AccountBp accountBp;

    @Override
    public boolean existsUser(String string) {
        if (string.equals("Administrator")) {
            return true;
        }
        SystemUser search = new SystemUser();
        search.setLogin(string);
        return (!systemUserBp.searchByExample(search, 0, 1).isEmpty());
    }

    @Override
    public boolean existsGroup(String string) {
        Role search = new Role();
        search.setName(string);
        if (!roleBp.searchByExample(search, 0, 1).isEmpty()) {
            return true;
        }
        else {
            String accountName = string.replace(CUSTOMER_SERVICE, "");
            Account searchAccount = new Account();
            searchAccount.setName(accountName);
            return (!accountBp.searchByExactExample(searchAccount, 0, 0).isEmpty());
        }
    }

    @Override
    public List<String> getGroupsForUser(String string, List<String> list, List<String> list1) {
        List<String> toReturn = systemUserBp.getRolesForUser(string);
        for (String accountName : systemUserBp.getAccountsForUser(string)) {
            toReturn.add(accountName + CUSTOMER_SERVICE);
        }
        return toReturn;
    }

}
