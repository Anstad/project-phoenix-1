package com.apd.phoenix.service.email.api;

import java.util.List;
import java.util.Set;
import javax.mail.internet.MimeMessage;

public interface EmailService {

    public MimeMessage prepareRawMessage(String from, Set<String> replyTo, Set<String> bcc, Set<String> to,
            Set<String> cc, String subject, String body, List<Attachment> attachments);

    public boolean sendEmail(String from, Set<String> to, String subject, String body, List<Attachment> attachments);

    public boolean sendEmail(MimeMessage message);
}
