package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class PickDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4665969201132359413L;

    private LineItemDto lineItem;

    private String shipperId;

    private String orderNumber;

    private String customerPoNumber;

    private String apdOrderNumber;

    private String inventoryId;

    private String lineReference;

    private BigInteger qtyToPick;

    private BigInteger qtyBo;

    private boolean isShipped;

    public LineItemDto getLineItem() {
        return lineItem;
    }

    public void setLineItem(LineItemDto lineItem) {
        this.lineItem = lineItem;
    }

    public String getShipperId() {
        return shipperId;
    }

    public void setShipperId(String shipperId) {
        this.shipperId = shipperId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerPoNumber() {
        return customerPoNumber;
    }

    public void setCustomerPoNumber(String customerPoNumber) {
        this.customerPoNumber = customerPoNumber;
    }

    public String getApdOrderNumber() {
        return apdOrderNumber;
    }

    public void setApdOrderNumber(String apdOrderNumber) {
        this.apdOrderNumber = apdOrderNumber;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getLineReference() {
        return lineReference;
    }

    public void setLineReference(String lineReference) {
        this.lineReference = lineReference;
    }

    public BigInteger getQtyToPick() {
        return qtyToPick;
    }

    public void setQtyToPick(BigInteger qtyToPick) {
        this.qtyToPick = qtyToPick;
    }

    public BigInteger getQtyBo() {
        return qtyBo;
    }

    public void setQtyBo(BigInteger qtyBo) {
        this.qtyBo = qtyBo;
    }

    public boolean isShipped() {
        return isShipped;
    }

    public void setShipped(boolean isShipped) {
        this.isShipped = isShipped;
    }

}
