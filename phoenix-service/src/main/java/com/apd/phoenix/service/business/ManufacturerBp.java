package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.persistence.jpa.ManufacturerDao;

/**
 * This class provides business process methods for Manufacturer.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ManufacturerBp extends AbstractBp<Manufacturer> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ManufacturerDao dao) {
        this.dao = dao;
    }

    public Manufacturer getFeeManufacturer() {
        Manufacturer searchManufacturer = new Manufacturer();
        searchManufacturer.setName(LineItem.FEE_ITEM_CATEGORY);
        List<Manufacturer> feeManufacturers = this.searchByExactExample(searchManufacturer, 0, 0);
        if (feeManufacturers != null && !feeManufacturers.isEmpty()) {
            return feeManufacturers.get(0);
        }
        return null;
    }
}
