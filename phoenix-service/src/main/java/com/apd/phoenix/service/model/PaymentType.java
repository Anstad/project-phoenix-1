package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * This class has a one-to-many relationship with PaymentInformation, and is used to indicate what 
 * type of payment the information is used for. Could be an invoice, credit card, and so on.
 * 
 * @author RHC
 *
 */
@Entity
@Audited
public class PaymentType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -2802864258252319707L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String name;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PaymentType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        if (this.getName() != null && !this.getName().trim().isEmpty())
            return this.getName();
        return "";
    }

    public enum PaymentTypeEnum {
        INVOICE("Invoice"), CREDITCARD("Credit Card");

        private final String value;

        private PaymentTypeEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }
}