package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.LineItemXShipment;

/**
 * Address DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class LineItemXShipmentDao extends AbstractDao<LineItemXShipment> {

    public void refresh(LineItemXShipment entity) {
        this.entityManager.refresh(entity);
    }
}
