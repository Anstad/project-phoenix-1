package com.apd.phoenix.service.business;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.exception.OrderException;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CardInformation;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.NotificationLimit;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.NotificationLimit.LimitType;
import com.apd.phoenix.service.model.NotificationProperties.Addressing;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.PunchoutSession;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PaymentInformationDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ValidationErrorDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.persistence.jpa.CredentialXVendorDao;
import com.apd.phoenix.service.persistence.jpa.CustomerCreditInvoiceDao;
import com.apd.phoenix.service.persistence.jpa.CustomerInvoiceDao;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.PaymentTypeDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;
import com.apd.phoenix.service.persistence.jpa.VendorDao;
import com.apd.phoenix.service.utility.CredentialPropertyUtils;

/**
 * @author Red Hat Middleware Consulting. Red Hat, Inc.  Jun 24, 2013
 * 
 */
@Stateless
@LocalBean
public class CustomerOrderBp extends AbstractBp<CustomerOrder> {

    private static final String USSCO_VENDOR_NAME = "USSCO";

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerOrderBp.class);

    public static final String ORDER_REQUESTED = "REQUESTED";
    public static final long HOUR_IN_MS = 1000 * 60 * 60;
    public static final long DAY_IN_MS = HOUR_IN_MS * 24;
    public static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";
    public static final String ORDER_PUNCHED = "PUNCHED";

    private static final long DENIED_ORDER_THRESHOLD = 7;

    private static final String YES = "Y";

    public static final String HANDLING_FEE_DESCRIPTION = "Handling Fee";
    public static final String HANDLING_FEE_SKU = "handling_fee";

    public static final String MINIMUM_ORDER_FEE_DESCRIPTION = "Minimum Order Fee";
    public static final String MINIMUM_ORDER_FEE_SKU = "min_order_fee";

    @Inject
    private AddressBp addressBp;

    @Inject
    private MiscShipToBp miscShipToBp;

    @Inject
    private OrderLogBp orderLogBp;

    @Inject
    private OrderStatusBp orderStatusBp;

    @Inject
    private PoNumberBp poNumberBp;

    @Inject
    private AccountXCredentialXUserBp accountXCredentialXUserBp;

    @Inject
    private CredentialBp credentialBp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private OrderTypeBp orderTypeBp;

    @Inject
    private SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    @Inject
    private LineItemBp lineItemBp;

    @Inject
    private LineItemStatusBp lineItemStatusBp;

    @Inject
    private PoNumberTypeBp poNumberTypeBp;

    @Inject
    VendorDao vendorDao;

    @Inject
    private PaymentTypeDao paymentTypeDao;

    @Inject
    private TaxRateBp taxRateBp;

    @Inject
    private CustomerInvoiceDao customerInvoiceDao;

    @Inject
    private CustomerCreditInvoiceDao customerCreditInvoiceDao;

    @Inject
    private UnitOfMeasureBp unitOfMeasureBp;

    @Inject
    private CredentialXVendorDao credXVendorDao;

    @Inject
    private PersonBp personBp;

    @Inject
    private TaxRateAtPurchaseBp taxRateAtPurchaseBp;

    @Inject
    private PaymentInformationBp paymentInformationBp;

    @Inject
    private AddressTypeBp addressTypeBp;

    @Inject
    private OrderBRMSRecordBp orderBRMSRecordBp;

    public int numberOfOrdersWithCustomerPo(String customerPoNumber, Account account) {
        Collection<CustomerOrder> orders = ((CustomerOrderDao) dao).getOrderByCustomerPo(customerPoNumber, account);
        if (orders == null) {
            return 0;
        }
        else {
            return orders.size();
        }
    }

    public List<CustomerOrder> getOrdersWithCustomerPo(String customerPoNumber, Account account) {
        Collection<CustomerOrder> orders = ((CustomerOrderDao) dao).getOrderByCustomerPo(customerPoNumber, account);
        return new ArrayList<CustomerOrder>(orders);
    }

    private boolean customerPoNotFromOrderToUpdate(PurchaseOrderDto purchaseOrderDto, CustomerOrder customerOrder) {
        return numberOfOrdersWithCustomerPo(purchaseOrderDto.retrieveCustomerPoNumber().getValue(), customerOrder
                .getAccount()) == 1
                && (customerOrder.getCustomerPo() == null || !purchaseOrderDto.retrieveCustomerPoNumber().getValue()
                        .equals(customerOrder.getCustomerPo().getValue()));
    }

    private boolean isCustomerPoNotUniqueWithinAccount(PurchaseOrderDto purchaseOrderDto, CustomerOrder customerOrder) {
        if (numberOfOrdersWithCustomerPo(purchaseOrderDto.retrieveCustomerPoNumber().getValue(), customerOrder
                .getAccount()) > 1
                || customerPoNotFromOrderToUpdate(purchaseOrderDto, customerOrder)) {
            LOG.error("Cannot create order with duplicate customer PO number "
                    + purchaseOrderDto.retrieveCustomerPoNumber().getValue());
            return true;
        }
        return false;
    }

    public static class NonUniqueCustomerPoException extends Exception {

        private CustomerOrder order;
        private String customerPo;

        public NonUniqueCustomerPoException(CustomerOrder orderIn, String invalidPo) {
            this.order = orderIn;
            this.customerPo = invalidPo;
        }

        /**
         * @return the order
         */
        public CustomerOrder getOrder() {
            return order;
        }

        /**
         * @param order the order to set
         */
        public void setOrder(CustomerOrder order) {
            this.order = order;
        }

        /**
         * @return the customerPo
         */
        public String getCustomerPo() {
            return customerPo;
        }

        /**
         * @param customerPo the customerPo to set
         */
        public void setCustomerPo(String customerPo) {
            this.customerPo = customerPo;
        }
    }

    public enum AuthorizationStrategy {
        FULL, ONE, NONE;
    }

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CustomerOrderDao dao) {
        this.dao = dao;
    }

    public CustomerOrder createEmptyOrderRequest(String login) {
        List<AccountXCredentialXUser> results = accountXCredentialXUserBp.getAllAXCXUByLogin(login);
        if (results == null || results.isEmpty()) {
            return null;
        }
        AccountXCredentialXUser accountXCredentialXUser = results.get(0);

        return createEmptyOrder(accountXCredentialXUser, orderTypeBp.getTypeFromValue("URL"));
    }

    public CustomerOrder createEmptyOrderRequestForAccount(String accountId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Creating an empty order request to log against for new inbound order for: " + accountId);
        }
        AccountXCredentialXUser genericAXCXU = accountXCredentialXUserBp.getGenericAXCXUForAccount(accountId);
        if (genericAXCXU == null) {
            LOG.error("Could not find generic user with concurrent access and active set to true for accountId: "
                    + accountId);
            return null;
        }
        return createEmptyOrder(genericAXCXU, orderTypeBp.getTypeFromValue("PUNCHOUT"));
    }

    public CustomerOrder createPunchoutOrder(PunchoutSession punchoutSession) {
        return createEmptyOrder(punchoutSession.getAccntCredUser(), orderTypeBp.getTypeFromValue("PUNCHOUT"));
    }

    public CustomerOrder createEmptyOrder(AccountXCredentialXUser accountXCredentialXUser, OrderType type) {
        return createEmptyOrder(accountXCredentialXUser.getAccount(), accountXCredentialXUser.getCredential(),
                accountXCredentialXUser.getUser(), type);
    }

    public CustomerOrder createEmptyOrder(Account account, Credential credential, SystemUser systemUser, OrderType type) {

        //sets the order's status based on the type of empty order being created
        String orderStatus;
        if (type.getValue().equals("PUNCHOUT")) {
            orderStatus = ORDER_PUNCHED;
        }
        else {
            orderStatus = ORDER_REQUESTED;
        }

        return this.createEmptyOrder(account, credential, systemUser, type, orderStatusBp
                .getOrderStatusByValue(orderStatus), true);
    }

    public CustomerOrder createEmptyOrder(Account account, Credential credential, SystemUser systemUser,
            OrderType type, OrderStatus status, boolean canPersistBeforePlaced) {
        credential = credentialBp.findById(credential.getId(), Credential.class);

        CustomerOrder newOrder = new CustomerOrder();

        if (type.getValue().equals("PUNCHOUT")) {
            newOrder.setOperationAllowed(credential.getPunchoutOperationAllowed());
        }

        //initializes the order
        newOrder.setAccount(account);
        newOrder.setCredential(credential);
        newOrder.setTerms(newOrder.getCredential().getTerms());
        newOrder.setUser(systemUser);

        newOrder.setItems(new HashSet<LineItem>());
        newOrder.setEstimatedShippingAmount(BigDecimal.ZERO);
        newOrder.setMaximumTaxToCharge(BigDecimal.ZERO);
        newOrder.setOrderTotal(BigDecimal.ZERO);
        newOrder.setType(type);
        this.setOrderStatus(newOrder, status);

        //adds notifications, as determined by Credential
        for (NotificationProperties notification : credentialBp.getCredentialNotifications(credential)) {
            this.setNotification(notification, newOrder);
        }
        for (NotificationProperties notification : accountBp.findById(account.getId(), Account.class)
                .getNotificationProperties()) {
            this.setNotification(notification, newOrder);
        }

        //creates the APD PO number
        PoNumber apdPo = poNumberBp.nextApdPoDetached();
        newOrder.getPoNumbers().add(apdPo);
        apdPo.getOrders().add(newOrder);

        //persists the order
        if (canPersistBeforePlaced) {
            newOrder = this.update(newOrder);
        }

        return newOrder;
    }

    public void setNotification(NotificationProperties notification, CustomerOrder order) {
        NotificationProperties newNotification = new NotificationProperties();
        newNotification.setAddressing(notification.getAddressing());
        newNotification.setEventType(notification.getEventType());
        newNotification.setOrder(order);
        newNotification.setRecipients(notification.getRecipients());
        if (!StringUtils.isBlank(newNotification.getRecipients())) {
            order.getNotificationProperties().add(newNotification);
        }
    }

    /**
     * Adds an customerOrder as a child of another customerOrder. Adding a child
     * fails silently if the resulting order graph would be cyclic.
     * 
     * @param parent
     *            - the parent customerOrder in the new relationship
     * @param child
     *            - the child customerOrder in the new relationship
     * @return the persisted, managed parent customerOrder
     */
    public CustomerOrder addChildOrder(CustomerOrder parent, CustomerOrder child) {

        /*
         * iterates through ancestors of the parent customerOrder. If one of the
         * ancestor customerOrders has the same ID as the child customerOrder,
         * then the graph would be cyclical, so the parent would be returned
         * without adding the child.
         */
        CustomerOrder ancestor = parent;
        while (ancestor.getParentOrder() != null) {
            if (ancestor.getId() == child.getId()) {
                return parent;
            }
            ancestor = ancestor.getParentOrder();
        }

        // Removes the child from the original parent, if there is one
        if (child.getParentOrder() != null) {
            child.getParentOrder().getChildren().remove(child);
            this.dao.update(child.getParentOrder());
        }

        // Adds the child to the parent
        parent.getChildren().add(child);
        child.setParentOrder(parent);

        this.dao.update(child);
        return this.dao.update(parent);
    }

    /**
     * Deletes the entity, determined by the id from the database.
     * 
     * @param id
     *            - the id of the entity to be deleted
     */
    @Override
    public void delete(Long id, Class<CustomerOrder> type) {
        CustomerOrder toDelete = this.dao.findById(id, type);

        // Checks that the customerOrder has no children
        if ((toDelete.getChildren() != null && !toDelete.getChildren().isEmpty())) {
            return;
        }

        this.dao.delete(id, type);
    }

    public boolean isManaged(CustomerOrder customerOrder) {
        return this.dao.isManaged(customerOrder);
    }

    public List<CustomerOrder> getCustomerOrders(CustomerOrderSearchCriteria criteria) {
        List<CustomerOrder> coList = ((CustomerOrderDao) dao).getCustomerOrders(criteria);

        if (coList == null) {
            coList = new ArrayList<CustomerOrder>();
        }

        return coList;
    }

    public List<CustomerOrder> getCustomerOrdersPaginated(CustomerOrderSearchCriteria criteria) {
        List<CustomerOrder> coList;

        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criteria.setAccountWhiteList(systemUserDao.getAccountIds(user));
            coList = ((CustomerOrderDao) dao).getCustomerOrdersPaginated(criteria);
        }
        else {
            coList = ((CustomerOrderDao) dao).getCustomerOrdersPaginated(criteria);
        }

        if (coList == null) {
            coList = new ArrayList<CustomerOrder>();
        }

        return coList;
    }

    public int getCustomerOrdersCount(CustomerOrderSearchCriteria criteria) {
        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criteria.setAccountWhiteList(systemUserDao.getAccountIds(user));
        }
        int count = ((CustomerOrderDao) dao).getCustomerOrdersCount(criteria);

        return count;
    }

    public CustomerOrder addItem(CustomerOrder customerOrder, LineItem toAdd) {
        LOGGER.info("adding Item!!");
        customerOrder.getItems().add(toAdd);
        return this.update(customerOrder);
    }

    public CustomerOrder addLog(CustomerOrder customerOrder, OrderStatus status, EventType event) {
        OrderLog log = new OrderLog();
        log.setEventType(event);
        log.setOrderStatus(status);
        log.setUpdateTimestamp(new Date());
        return this.addLog(customerOrder, log);
    }

    public CustomerOrder addLog(CustomerOrder customerOrder, OrderLog orderLog) {
        customerOrder = findById(customerOrder.getId(), CustomerOrder.class);
        customerOrder = this.addLogNoUpdate(customerOrder, orderLog);
        return this.update(customerOrder);
    }

    public CustomerOrder addLogNoUpdate(CustomerOrder customerOrder, OrderLog orderLog) {
        customerOrder.getOrderLogs().add(orderLog);
        return customerOrder;
    }

    public CustomerOrder hydrateForOrderDetails(CustomerOrder toHydrate) {
        return ((CustomerOrderDao) this.dao).hydrateForOrderDetails(toHydrate);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CustomerOrder hydrateForOrderDetailsDifferentTransaction(CustomerOrder toHydrate) {
        return this.hydrateForOrderDetails(toHydrate);
    }

    public List<Long> getAllOrderIdsWithShipToId(String shipToId) {
        List<Long> toReturn = new ArrayList<>();
        List<CustomerOrder> results = ((CustomerOrderDao) dao).getOrdersWithShipToId(shipToId);
        if (results != null) {
            for (CustomerOrder customerOrder : results) {
                toReturn.add(customerOrder.getId());
            }
        }
        return toReturn;
    }

    public Address getAddress(CustomerOrder customerOrder) {
        return ((CustomerOrderDao) dao).findById(customerOrder.getId(), CustomerOrder.class).getAddress();
    }

    public boolean canEditOrder(CustomerOrder customerOrder) {
        return customerOrder.getOperationAllowed().equals(CustomerOrder.PunchoutOrderOperation.edit);
    }

    public CustomerOrder getForWorkflow(long orderId) {
        return ((CustomerOrderDao) dao).getForWorkflow(orderId);
    }

    public String getBillingMethod(CustomerOrder customerOrder) {
        if (customerOrder != null && customerOrder.getPaymentInformation() != null
                && customerOrder.getPaymentInformation().getPaymentType() != null) {
            return customerOrder.getPaymentInformation().getPaymentType().getName();
        }
        else {
            return null;
        }
    }

    public String getApprover(CustomerOrder customerOrder) {
        try {
            AccountXCredentialXUser axcxu = this.getAXCXU(customerOrder);
            if (axcxu != null && axcxu.getApproverUser() != null) {
                return axcxu.getApproverUser().getLogin();
            }
            else if (customerOrder.getCredential().getUser() != null) {
                return customerOrder.getCredential().getUser().getLogin();
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception caught, resorting to default", e);
            return "admin";
        }
    }

    public String getApproverEmail(CustomerOrder customerOrder) {
        try {
            AccountXCredentialXUser axcxu = this.getAXCXU(customerOrder);
            if (axcxu.getApproverUser() == null || axcxu.getApproverUser().getPerson() == null) {
                return null;
            }
            else {
                return axcxu.getApproverUser().getPerson().getEmail();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception caught, resorting to default");
            LOGGER.error(e.getMessage());
            return "admin";
        }
    }

    public AccountXCredentialXUser getAXCXU(CustomerOrder customerOrder) {
        customerOrder = this.findById(customerOrder.getId(), CustomerOrder.class);
        AccountXCredentialXUser search = new AccountXCredentialXUser();
        Account searchAccount = new Account();
        Credential searchCredential = new Credential();
        SystemUser searchUser = new SystemUser();

        searchAccount.setId(customerOrder.getAccount().getId());
        if (customerOrder.getCredential() != null) {
            searchCredential.setId(customerOrder.getCredential().getId());
        }
        searchUser.setId(customerOrder.getUser().getId());
        search.setAccount(searchAccount);
        search.setCredential(searchCredential);
        search.setUser(searchUser);
        List<AccountXCredentialXUser> resultList = accountXCredentialXUserBp.searchByExactExample(search, 0, 1);
        if (resultList != null && !resultList.isEmpty()) {
            return resultList.get(0);
        }
        return null;
    }

    public CustomerOrder getQuotedOrder(CustomerOrder customerOrder) {
        try {
            OrderStatus quoted = orderStatusBp.getOrderStatus(OrderStatusEnum.ORDERED);
            CustomerOrder search = new CustomerOrder();
            search.getApdPo().setId(customerOrder.getApdPo().getId());
            search.getStatus().setId(quoted.getId());
            CustomerOrder toReturn = this.searchByExample(search, 0, 1).get(0);
            return toReturn;
        }
        catch (Exception e) {
            LOGGER.error("Exception caught, resorting to defaul", e);
            return null;
        }
    }

    public List<LineItem> getLineItemsOrdered(CustomerOrder currentOrder) {
        return ((CustomerOrderDao) dao).getLineItemsOrderedBySku(currentOrder.getId());
    }

    public CustomerOrder setOrderStatus(CustomerOrder customerOrder, OrderStatus orderStatus) {
        return this.setOrderStatus(customerOrder, orderStatus, null);
    }

    public CustomerOrder setOrderStatus(CustomerOrder customerOrder, OrderStatus orderStatus,
            EventType statusChangeEvent) {
        if (!customerOrder.hasAllowedStatus()) {
            LOGGER.warn("System tried to overwrite INVALIDATED or MANUALLY_RESOLVED order status with "
                    + orderStatus.getValue());
            return customerOrder;
        }
        if (customerOrder != null
                && orderStatus != null
                && (orderStatus.equals(customerOrder.getStatus()) || orderStatus.equals(customerOrder
                        .getPaymentStatus()))) {
            return customerOrder;
        }
        OrderLog orderLog = new OrderLog();

        orderLog.setOrderStatus(orderStatus);
        orderLog.setUpdateTimestamp(new Date());
        orderLog.setCustomerOrder(customerOrder);
        orderLog.setEventType(statusChangeEvent);

        if (customerOrder.getId() != null) {
            orderLog = orderLogBp.create(orderLog);
        }
        OrderStatus statusToSet = orderStatus;
        if (statusToSet.getOverrideStatus() != null) {
            statusToSet = statusToSet.getOverrideStatus();
            statusToSet.toString();
        }
        if (statusToSet.isPaymentStatus()) {
            customerOrder.setPaymentStatus(statusToSet);
        }
        else {
            customerOrder.setStatus(statusToSet);
        }
        if (statusToSet.isCustomerStatus()) {
            customerOrder.setCustomerStatus(statusToSet);
        }
        if (customerOrder.getId() != null) {
            customerOrder = update(customerOrder);
            return addLog(customerOrder, orderLog);
        }
        else {
            return addLogNoUpdate(customerOrder, orderLog);
        }
    }

    /**
     * This method takes a customer order in a transaction, and reverts its status to the most recent status
     * before the transaction began, that isn't one of the PROCESSING statuses. If no earlier statuses are
     * found, the status is left as is.
     * 
     * @param customerOrder
     * @return
     */
    public CustomerOrder revertOrderStatus(CustomerOrder customerOrder) {
        if (customerOrder == null || customerOrder.getId() == null) {
            return null;
        }
        OrderLog lastCommittedLog = this.getLastCommittedOrderLog(customerOrder);
        if (lastCommittedLog == null || lastCommittedLog.getUpdateTimestamp() == null) {
            return customerOrder;
        }
        OrderStatus previousStatus = null;
        Date statusDate = null;
        for (OrderLog log : customerOrder.getOrderLogs()) {
            if (!log.getUpdateTimestamp().after(lastCommittedLog.getUpdateTimestamp()) && log.getOrderStatus() != null
                    && !log.getOrderStatus().getValue().endsWith("_PROCESSING") && log.getUpdateTimestamp() != null
                    && (statusDate == null || log.getUpdateTimestamp().after(statusDate))) {
                previousStatus = log.getOrderStatus();
                statusDate = log.getUpdateTimestamp();
            }
        }
        if (previousStatus != null) {
            return this.setOrderStatus(customerOrder, previousStatus, EventType.REVERT_STATUS);
        }
        return customerOrder;
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    private OrderLog getLastCommittedOrderLog(CustomerOrder order) {
        order = this.hydrateForOrderDetails(this.findById(order.getId(), CustomerOrder.class));
        List<OrderLog> logs = new ArrayList<OrderLog>(order.getOrderLogs());
        if (logs.size() <= 0) {
            return null;
        }
        Collections.sort(logs, new EntityComparator());
        return logs.get(logs.size() - 1);
    }

    public boolean isCompleted(CustomerOrder customerOrder) {
        OrderStatus orderCompleted = orderStatusBp.getOrderStatus(OrderStatusEnum.COMPLETED);
        return customerOrder.getStatus().equals(orderCompleted);
    }

    public Set<LimitApproval> getApprovalType(CustomerOrder customerOrder) {
        AccountXCredentialXUser axcxu = this.getAXCXU(customerOrder);
        if (axcxu != null) {
            return axcxu.getLimitApprovals();
        }
        else if (customerOrder.getCredential().getLimitApproval() != null) {
            return customerOrder.getCredential().getLimitApproval();
        }
        else {
            return null;
        }
    }

    public CustomerOrder searchByApdPo(String apdPo) {
        return searchByApdPo(apdPo, false);
    }

    public List<CustomerOrder> searchByCustomerPo(String customerPo, String partnerId) {
        CustomerOrderSearchCriteria criteria = new CustomerOrderSearchCriteria(customerPo,
                PoNumberTypeBp.CUSTOMER_PO_TYPE, partnerId);
        return this.getCustomerOrders(criteria);
    }

    public CustomerOrder searchByApdPo(String apdPo, boolean filterByWhitelist) {
        if (StringUtils.isBlank(apdPo)) {
            return null;
        }
        String searchApdPo;
        if (apdPo.indexOf("-") > 0) {
            searchApdPo = apdPo.substring(0, apdPo.indexOf("-"));
        }
        else {
            searchApdPo = apdPo;
        }
        CustomerOrderSearchCriteria criteria = new CustomerOrderSearchCriteria(searchApdPo);
        if (filterByWhitelist && StringUtils.isNotBlank(securityContext.getUserLogin())
                && !securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criteria.setAccountWhiteList(systemUserDao.getAccountIds(securityContext.getUserLogin()));
        }
        List<CustomerOrder> result = this.getCustomerOrders(criteria);
        if (result.isEmpty()) {
            LOG.warn("Could not find order with APD PO " + apdPo + ", whitelisted: " + filterByWhitelist);
            return null;
        }
        else {
            return result.get(0);
        }
    }

    public Set<NotificationProperties> getNotificationPropertiesForEvent(CustomerOrder customerOrder, EventType type) {
        Set<NotificationProperties> toReturn = new HashSet<NotificationProperties>();
        Set<NotificationProperties> notificationProperties = customerOrder.getNotificationProperties();
        for (NotificationProperties nProperties : notificationProperties) {
            if (nProperties.getEventType() != null && nProperties.getEventType().equals(type)) {
                toReturn.add(nProperties);
                //                for (String recipient : nProperties.getRecipients().split(",")) {
                //                    toReturn.add(recipient);
                //                }
            }
        }

        return toReturn;
    }

    public String getNextInvoiceNumber(CustomerOrder customerOrder) {
        customerOrder = this.findById(customerOrder.getId(), CustomerOrder.class);

        CustomerInvoice searchInvoice = new CustomerInvoice();
        searchInvoice.setShipment(new Shipment());
        searchInvoice.getShipment().setOrder(new CustomerOrder());
        searchInvoice.getShipment().getOrder().setId(customerOrder.getId());
        int numberOfInvoices = (int) (long) this.customerInvoiceDao.resultQuantity(searchInvoice);

        CustomerCreditInvoice searchCreditInvoice = new CustomerCreditInvoice();
        searchCreditInvoice.setReturnOrder(new ReturnOrder());
        searchCreditInvoice.getReturnOrder().setOrder(new CustomerOrder());
        searchCreditInvoice.getReturnOrder().getOrder().setId(customerOrder.getId());
        numberOfInvoices += (int) (long) this.customerCreditInvoiceDao.resultQuantity(searchCreditInvoice);

        return customerOrder.getApdPo().getValue().replaceAll("[a-zA-Z]", "") + "-" + (numberOfInvoices + 1);
    }

    public String getType(CustomerOrder customerOrder) {
        if (customerOrder != null && customerOrder.getType() != null) {
            return customerOrder.getType().getValue();
        }
        return null;
    }

    public CustomerOrder createOrUpdateCustomerOrder(PurchaseOrderDto purchaseOrderDto)
            throws NonUniqueCustomerPoException {
        CustomerOrder customerOrder = searchByApdPo(purchaseOrderDto.getApdPoNumber());
        if (purchaseOrderDto.retrieveCustomerPoNumber() != null
                && !StringUtils.isEmpty(purchaseOrderDto.retrieveCustomerPoNumber().getValue())
                && customerOrder != null && customerOrder.getAccount() != null) {
            if (isCustomerPoNotUniqueWithinAccount(purchaseOrderDto, customerOrder)) {
                throw new NonUniqueCustomerPoException(customerOrder, purchaseOrderDto.getCustomerPoNumber());
            }
        }
        if (customerOrder == null) {
            return this.createNewOrder(purchaseOrderDto);
        }
        else {
            if (purchaseOrderDto.isUpdateEmptyOrder()) {
                return this.updateEmptyOrder(customerOrder, purchaseOrderDto);
            }
            return customerOrder;
        }
    }

    private CustomerOrder updateEmptyOrder(CustomerOrder customerOrder, PurchaseOrderDto purchaseOrderDto)
            throws NonUniqueCustomerPoException {
        customerOrder = buildOrder(purchaseOrderDto, customerOrder);
        customerOrder = populateOrder(purchaseOrderDto, customerOrder);
        return customerOrder;
    }

    private CustomerOrder createNewOrder(PurchaseOrderDto purchaseOrderDto) throws NonUniqueCustomerPoException {
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder = buildOrder(purchaseOrderDto, customerOrder);
        customerOrder = this.create(customerOrder);
        customerOrder = populateOrder(purchaseOrderDto, customerOrder);
        return customerOrder;

    }

    /**
     * Creates a new quoted order using a provided PO DTO (status on the DTO must be "QUOTED")
     * 
     * @param punchoutOrder
     * @return quoteId
     * @throws OrderException
     */
    public String createOrUpdatePunchoutOrder(PurchaseOrderDto punchoutOrder) throws OrderException {
        if (punchoutOrder == null) {
            String errorMessage = "Unable to create punchout order.  The provided punchout order DTO is null.";
            LOGGER.error(errorMessage);
            throw new OrderException(errorMessage);
        }
        else if (punchoutOrder.getOrderStatus() == null
                || (!punchoutOrder.getOrderStatus().getValue().equalsIgnoreCase(OrderStatusEnum.QUOTED.getValue()) && !punchoutOrder
                        .getOrderStatus().getValue().equalsIgnoreCase(ORDER_PUNCHED))) {
            String errorMessage = "Unable to create punchout order.  The provided punchout order DTO has an invalid status.";
            LOGGER.error(errorMessage);
            throw new OrderException(errorMessage);
        }

        CustomerOrder quotedOrder = searchByApdPo(punchoutOrder.getApdPoNumber());

        //initializes the order
        if (quotedOrder == null) {
            quotedOrder = new CustomerOrder();
            Account accountSearch = new Account();
            accountSearch.setName(punchoutOrder.getAccount().getName());
            Credential credSearch = new Credential();
            credSearch.setName(punchoutOrder.getCredential().getName());
            quotedOrder.setAccount(accountBp.searchByExactExample(accountSearch, 0, 0).get(0));
            quotedOrder.setCredential(credentialBp.searchByExactExample((credSearch), 0, 0).get(0));
            quotedOrder.setUser(systemUserDao.getSystemUserByLogin(punchoutOrder.getSystemUserDto().getLogin()).get(0));
            quotedOrder.setType(orderTypeBp.getTypeFromValue("PUNCHOUT"));

            PoNumber nextPoNumber = poNumberBp.nextApdPo();
            quotedOrder.getPoNumbers().add(nextPoNumber);
            nextPoNumber.getOrders().add(quotedOrder);

            quotedOrder = create(quotedOrder);
        }

        OrderStatus status = orderStatusBp.getOrderStatusByValue(punchoutOrder.getOrderStatus().getValue());
        this.setOrderStatus(quotedOrder, status);

        if (CustomerOrder.PunchoutOrderOperation.valueOf(punchoutOrder.getOperationAllowed()) != null) {
            quotedOrder.setOperationAllowed(CustomerOrder.PunchoutOrderOperation.valueOf(punchoutOrder
                    .getOperationAllowed()));
        }

        quotedOrder = update(quotedOrder);

        quotedOrder.setEstimatedShippingAmount(punchoutOrder.getEstimatedShippingAmount());
        quotedOrder.setMaximumTaxToCharge(punchoutOrder.getMaximumTaxToCharge());
        quotedOrder.setOrderTotal(punchoutOrder.getOrderTotal());

        //TODO: remove existing line items from quoted order  (for punchout edit)

        //Add the items from the customer's catalog
        //TODO: confirm this logic
        if (!quotedOrder.getType().getValue().equalsIgnoreCase("PUNCHOUT")) {
            for (LineItemDto lineItemDto : punchoutOrder.getItems()) {
                LineItem createdLineItem = lineItemBp.createLineItem(lineItemDto, quotedOrder.getCredential());
                createdLineItem.setOrder(quotedOrder);
                quotedOrder.getItems().add(createdLineItem);
            }
            this.recalculateFeeAmounts(quotedOrder);

        }
        else {
            for (int i = 0; i < punchoutOrder.getItems().size(); i++) {
                LineItemDto lineItemDto = punchoutOrder.getItems().get(i);
                LineItem lineItem = lineItemBp.copyAllDetails(lineItemDto, new LineItem());
                lineItem = lineItemBp.validateLineItemData(lineItem, true);
                lineItemBp.setStatus(lineItem, lineItemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.CREATED));
                // List of line items on order are 1 indexed.
                lineItem.setLineNumber(i + 1);
                quotedOrder.getItems().add(lineItem);
                lineItem.setOrder(quotedOrder);
                if (lineItem.getVendor() == null && StringUtils.isNotBlank(quotedOrder.getCredential().getVendorName())) {
                    lineItem.setVendor(vendorDao.getByName(quotedOrder.getCredential().getVendorName()));
                }
            }
        }

        //Set the shipping address to a clone of one on the account if possible
        quotedOrder.setAddress(addressBp.cloneEntity(addressBp.retrieve(punchoutOrder.getShipToAddressDto(),
                quotedOrder.getAccount())));

        //TODO: This was copied over from CashoutBean createEmptyOrder...uncomment?
        //marks the order as taxable, unless "charge sales tax" is set to "no" on the Credential
        //        String chargeSales = credBean.getPropertyMap().get("charge sales tax");
        //        if (chargeSales != null && (chargeSales.equalsIgnoreCase("no") || chargeSales.equalsIgnoreCase("false"))) {
        //            shouldTax = false;
        //        }

        // Copy default notification properties configured on credential
        if (quotedOrder.getCredential().getNotificationProperties() != null) {
            for (NotificationProperties credNotificationProperties : quotedOrder.getCredential()
                    .getNotificationProperties()) {
                if (StringUtils.isNotBlank(credNotificationProperties.getRecipients())) {
                    quotedOrder.addNotificationProperties(credNotificationProperties.clone());
                }
            }
        }
        // Copy default notification properties configured on account
        if (quotedOrder.getAccount().getNotificationProperties() != null) {
            for (NotificationProperties acctNotificationProperties : quotedOrder.getAccount()
                    .getNotificationProperties()) {
                if (StringUtils.isNotBlank(acctNotificationProperties.getRecipients())) {
                    quotedOrder.addNotificationProperties(acctNotificationProperties.clone());
                }
            }
        }

        //persists the order
        quotedOrder = update(quotedOrder);
        return quotedOrder.getApdPo().getValue();
    }

    /**
     * Iterates through orders with a shipDate equal to today, checking if any orders have items that have
     * not been shipped. Should be called close to and before midnight, e.g. 23:55:00. Because ship dates 
     * for orders are only specified to the day, we can only check whether an order is supposed to ship
     * on a particular day, so the best way to check if an order is late is to check if any orders have not 
     * been shipped that were supposed to for today's date, as late as we can in the day. We don't want to risk 
     * getOrdersWithShipDate(new Date()) being called after midnight so it's best to schedule this function with 
     * a little time to spare.
     * @return orders with items that have not been shipped
     */
    public List<CustomerOrder> getLateOrdersFromToday() {
        List<CustomerOrder> ordersToCheck = getOrdersWithShipDate(new Date());
        List<CustomerOrder> toReturn = new ArrayList<>();
        for (CustomerOrder order : ordersToCheck) {
            if (orderContainsItemsNotShipped(order)) {
                toReturn.add(order);
            }
        }
        return toReturn;
    }

    public List<CustomerOrder> getOrdersWithShipDate(Date date) {
        return ((CustomerOrderDao) dao).getOrdersWithShipDate(date);
    }

    public boolean orderContainsItemsNotShipped(CustomerOrder order) {
        order = this.hydrateLineItems(order);
        for (LineItem item : order.getItems()) {
            if (item.getQuantityRemaining() > 0) {
                return true;
            }
        }
        return false;
    }

    public List<CustomerOrder> getQuotesOlderThan(int days) {
        OrderStatus quoted = orderStatusBp.getOrderStatus(OrderStatusEnum.QUOTED);
        Date startDate = new Date(System.currentTimeMillis() - ((days + 1) * DAY_IN_MS));
        Date endDate = new Date(System.currentTimeMillis() - (days * DAY_IN_MS));
        CustomerOrderSearchCriteria criteria = new CustomerOrderSearchCriteria(quoted, startDate, endDate, quoted,
                null, null, null, null, null, null, null, null, null, null);
        return ((CustomerOrderDao) dao).getCustomerOrders(criteria);
    }

    public CustomerOrder hydrateLineItems(CustomerOrder order) {
        return ((CustomerOrderDao) dao).hydrateLineItems(order);
    }

    public void replaceItem(LineItem rejectedItem, LineItem replacementLineItem, CustomerOrder customerOrder) {
        lineItemBp.cancelLineItem(rejectedItem);
        //lineItemBp.cancelLineItem(rejectedItem);
        addItem(customerOrder, replacementLineItem);
    }

    public LineItem createLineItemForVendorInvoice(LineItemDto lineItemDto, PurchaseOrderDto purchaseOrderDto) {
        LineItem lineItem = new LineItem();
        CustomerOrder order = searchByApdPo(purchaseOrderDto.retrieveApdPoNumber().getValue());
        LineItem expected = lineItemBp.retrieveLineItem(lineItemDto, order);
        if (expected == null) {
            LOG.info("No expected line item found.");
        }
        else {
            lineItem.setExpected(expected);
        }

        lineItem.setApdSku(lineItemDto.getApdSku());
        lineItem.setEstimatedShippingAmount(lineItemDto.getEstimatedShippingAmount());
        lineItem.setMaximumTaxToCharge(lineItemDto.getMaximumTaxToCharge());
        lineItem.setUnitPrice(lineItemDto.getUnitPrice());
        lineItem.setCost(lineItemDto.getCost());
        lineItem.setManufacturerPartId(lineItemDto.getManufacturerPartId());
        lineItem.setSupplierPartId(lineItemDto.getSupplierPartId());
        lineItem.setQuantity(new Integer(lineItemDto.getQuantity().toString()));
        lineItem.setDescription(lineItemDto.getDescription());
        if (lineItemDto.getUnitOfMeasure() != null) {
            lineItem.setUnitOfMeasure(unitOfMeasureBp.getByName(lineItemDto.getUnitOfMeasure().getName()));
        }
        if (lineItemDto.getValidationErrorDtos() != null && !lineItemDto.getValidationErrorDtos().isEmpty()) {
            lineItem.setVendorComments(new ArrayList<String>());
            for (ValidationErrorDto error : lineItemDto.getValidationErrorDtos()) {
                if (error != null && StringUtils.isNotBlank(error.getError())) {
                    lineItem.getVendorComments().add(error.getError());
                }
            }
        }

        //ensure not nullable fields are not null
        if (lineItem.getEstimatedShippingAmount() == null) {
            lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
        }
        if (lineItem.getUnitPrice() == null) {
            lineItem.setUnitPrice(BigDecimal.ZERO);
        }
        if (lineItem.getCost() == null) {
            lineItem.setCost(BigDecimal.ZERO);
        }
        if (lineItem.getUnitOfMeasure() == null) {
            if (expected != null && expected.getUnitOfMeasure() != null) {
                LOG.warn("No unit of measure found for invoiced item. Substituting expected unit of measure: "
                        + expected.getUnitOfMeasure().getName());
                lineItem.setUnitOfMeasure(expected.getUnitOfMeasure());
            }
            else {
                LOG
                        .warn("No unit of measure found for invoiced item or expected item. Substituting default unit of measure.");
                lineItem.setUnitOfMeasure(unitOfMeasureBp.getDefaultLineItemUom());
            }
        }

        lineItem = lineItemBp.create(lineItem);
        return lineItem;
    }

    public boolean canReorder(CustomerOrder customerOrder) {
        Date threshold = new Date(System.currentTimeMillis() - ((DENIED_ORDER_THRESHOLD) * DAY_IN_MS));
        for (OrderLog log : customerOrder.getOrderLogs()) {
            if ((log.getOrderStatus() != null) && log.getOrderStatus().getValue().equals("DENIED")
                    && (log.getUpdateTimestamp().after(threshold))) {
                return true;
            }
        }
        return false;
    }

    private CustomerOrder buildOrder(PurchaseOrderDto purchaseOrderDto, CustomerOrder customerOrder) {
        //Set the APD PO
        PoNumber apdPo = null;
        if (StringUtils.isEmpty(purchaseOrderDto.getApdPoNumber())) {
            apdPo = poNumberBp.nextApdPo();
        }
        else {
            apdPo = poNumberBp.getPoNumber(purchaseOrderDto.getApdPoNumber());
        }
        apdPo.getOrders().add(customerOrder);
        customerOrder.getPoNumbers().add(apdPo);
        //Create the order log for order
        OrderStatus ordered = orderStatusBp.getOrderStatus(OrderStatusEnum.ORDERED);
        OrderLog toAdd = new OrderLog();
        toAdd.setOrderStatus(ordered);
        toAdd.setUpdateTimestamp(new Date());
        toAdd.setCustomerOrder(customerOrder);
        customerOrder.getOrderLogs().add(toAdd);
        this.setOrderStatus(customerOrder, ordered);
        //Calculate the order total
        customerOrder.refreshOrderTotal();
        return customerOrder;
    }

    private CustomerOrder populateOrder(PurchaseOrderDto purchaseOrderDto, CustomerOrder customerOrder)
            throws NonUniqueCustomerPoException {

        //Checks if credential is null, it will always be for newly created orders.
        //Will be done by edi 850 camel routes for new edi orders
        if (customerOrder.getCredential() == null) {
            //Set the Credential
            customerOrder.setCredential(credentialBp.retrieveCredential(purchaseOrderDto.getCredential()));
        }
        else {
            //updates credential on dto to be consistent with credential on order
            if (purchaseOrderDto.getCredential() != null) {
                LOG.warn("Credential from purchaseOrderDto overwritten by credential on order.");
            }
            purchaseOrderDto.setCredential(DtoFactory.createCredentialDto(customerOrder.getCredential()));
        }

        //Checks if account is null, it will always be for newly created orders
        if (customerOrder.getAccount() == null) {
            //Set the Account
            customerOrder.setAccount(accountBp.retrieveAccount(purchaseOrderDto.getAccount()));
        }
        else {
            /**
             * Novant CXML Orders will use an account designated by the locationId              * 
             * which will be a child of the root Novant account
             **/
            if (customerOrder.getAccount().getUseLocationCode() != null
                    && customerOrder.getAccount().getUseLocationCode()
                    && !StringUtils.isEmpty(purchaseOrderDto.getAccountLocationCode())) {
                customerOrder.setAccount(accountBp.findChildByLocationCode(purchaseOrderDto.getAccountLocationCode(),
                        customerOrder.getAccount()));
                if (customerOrder.getAccount() == null) {
                    LOG.error("No account found for location code: " + purchaseOrderDto.getAccountLocationCode());
                }
                //Sets the user to the only user on the sub account                
                Collection<SystemUser> accountUsers = this.accountBp.getUsers(customerOrder.getAccount());
                if (accountUsers != null && !accountUsers.isEmpty()) {
                    customerOrder.setUser(accountUsers.iterator().next());
                    if (accountUsers.size() > 1) {
                        LOG.error("Multiple users found on account: " + customerOrder.getAccount().getName()
                                + " dynamically assigned sub accounts should only have one user.");
                    }
                }
                else {
                    LOG.error("No users specified on account " + customerOrder.getAccount().getName());
                }
            }
        }
        if (StringUtils.isNotEmpty(purchaseOrderDto.getCustomerPoNumber())
                && isCustomerPoNotUniqueWithinAccount(purchaseOrderDto, customerOrder)) {
            throw new NonUniqueCustomerPoException(customerOrder, purchaseOrderDto.getCustomerPoNumber());
        }

        //Checks if systemUser is null, it will always be for newly created orders.
        if (customerOrder.getUser() == null) {
            //Set the SystemUser
            SystemUser search = new SystemUser();
            search.setLogin(purchaseOrderDto.getSystemUserDto().getLogin());
            List<SystemUser> results = systemUserDao.searchByExample(search, 0, 0);
            customerOrder.setUser((results == null || results.isEmpty() ? null : results.get(0)));
        }
        //Set the payment information
        customerOrder = this.setOrderPaymentInformation(customerOrder, purchaseOrderDto);

        //Clear any items that are already on the order from the return cart.
        customerOrder.getItems().clear();

        //Add the items from the customer's catalog
        for (LineItemDto lineItemDto : purchaseOrderDto.getItems()) {
            LineItem lineItem = lineItemBp.createLineItem(lineItemDto, customerOrder.getCredential());
            lineItem.setLineNumber(customerOrder.getMaxLineNumber() + 1);
            lineItem.setSupplierPartAuxiliaryId(lineItemDto.getSupplierPartAuxiliaryId());
            CredentialDto credentialDto = DtoFactory.createCredentialDto(customerOrder.getCredential());
            //String sendCxmlUnitPrice = (String) credentialDto.getProperties().get(CXML_UNIT_PRICE.getValue());
            if (CredentialPropertyUtils.equals(CXML_UNIT_PRICE, ItemBp.APD_COST_PROPERTYTYPE, credentialDto)) {
                if (lineItem.getCost() == null) {
                    if (credentialDto.getProperties().get(CXML_DEFAULT_PRICE_MARGIN.getValue()) == null) {
                        LOG.error("NO cxml default price margin property set on credential.");
                    }
                    else {
                        final BigDecimal cxmlDefaultPriceMargin = new BigDecimal(((String) purchaseOrderDto
                                .getCredential().getProperties().get(CXML_DEFAULT_PRICE_MARGIN.getValue())));
                        lineItem.setCost(lineItem.getUnitPrice().multiply(cxmlDefaultPriceMargin).setScale(2,
                                RoundingMode.CEILING));
                    }
                }
            }
            else {
                if (CredentialPropertyUtils.equals(CXML_UNIT_PRICE, ItemBp.CUSTOMER_PRICE_PROPERTYTYPE, credentialDto)) {
                    if (lineItem.getCost() == null) {
                        lineItem.setCost(lineItem.getUnitPrice());
                    }
                }
            }
            if (lineItem.getCost() == null) {
                lineItem.setCost(lineItem.getUnitPrice());
            }
            if (CredentialPropertyUtils.equals(CONCATENATE_LINE_NUM_AND_DESC, YES, credentialDto)) {
                String lineNumber = lineItem.getCustomerLineNumber();
                if (StringUtils.isBlank(lineNumber)) {
                    lineNumber = lineItem.getLineNumber().toString();
                }
                lineItem.setDescription("[" + lineNumber + "] " + lineItem.getDescription());
                lineItem.setShortName("[" + lineNumber + "] " + lineItem.getShortName());
            }
            if (lineItem.getVendor() == null && StringUtils.isNotBlank(customerOrder.getCredential().getVendorName())) {
                lineItem.setVendor(vendorDao.getByName(customerOrder.getCredential().getVendorName()));
            }

            customerOrder.getItems().add(lineItem);
            lineItem.setOrder(customerOrder);
        }
        this.recalculateFeeAmounts(customerOrder);
        //Set the line item statuses to created
        for (LineItem item : customerOrder.getItems()) {
            //Don't overwrite rejected status
            if (item.getStatus() == null || !item.getStatus().getValue().equals(LineItemStatusEnum.REJECTED.getValue())) {
                lineItemBp.setStatus(item, lineItemStatusBp.getStatus(LineItemStatusEnum.CREATED));
            }
        }
        //Set the shipping address to a clone of one on the account if possible
        boolean requireAuthShipTo = CredentialPropertyUtils.equals(REQUIRE_AUTHORIZED_SHIPTOS, YES, purchaseOrderDto
                .getCredential());
        Address shipTo = null;
        // If using an authorizedship to, overlay everything from the provided shipto except line 1, city, state, and usscoAccontId 
        // zip, and pendingShipToAuthorization on top of a clone of the authorized shipto
        if (requireAuthShipTo) {
            shipTo = addressBp.cloneEntity(addressBp.retrieve(purchaseOrderDto.getShipToAddressDto(), customerOrder
                    .getAccount()));
            if (shipTo != null) {
                MiscShipToDto miscShipToDto = purchaseOrderDto.getShipToAddressDto().getMiscShipToDto();
                if (miscShipToDto != null) {
                    String usscoAccountId = shipTo.getMiscShipTo() == null ? null : shipTo.getMiscShipTo()
                            .getUsAccount();
                    shipTo.setMiscShipTo(miscShipToBp.create(miscShipToDto));
                    shipTo.getMiscShipTo().setDivision(miscShipToDto.getDivision());
                    shipTo.getMiscShipTo().setUsAccount(usscoAccountId);
                }
                shipTo.setName(purchaseOrderDto.getShipToAddressDto().getName());
                shipTo.setCompany(purchaseOrderDto.getShipToAddressDto().getCompanyName());
                shipTo.setLine2(purchaseOrderDto.getShipToAddressDto().getLine2());
                shipTo.setCounty(purchaseOrderDto.getShipToAddressDto().getCounty());
                shipTo.setCountry(purchaseOrderDto.getShipToAddressDto().getCountry());
                shipTo.setGeoCode(purchaseOrderDto.getShipToAddressDto().getGeoCode());
            }
            else {
                purchaseOrderDto.getShipToAddressDto().setPendingShipToAuthorization(Boolean.TRUE);
            }
        }
        // If GLN ID is provided, use only fields from the matching shipto associated with the account
        else if (purchaseOrderDto.getShipToAddressDto().getMiscShipToDto() != null
                && StringUtils.isNotBlank(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getGlnID())) {
            shipTo = addressBp.cloneEntity(addressBp.retrieve(purchaseOrderDto.getShipToAddressDto(), customerOrder
                    .getAccount()));
            if (shipTo != null) {
                shipTo = addressBp.populateInboundData(shipTo, purchaseOrderDto.getShipToAddressDto());
                if (StringUtils.isEmpty(shipTo.getCompany())) {
                    shipTo.setCompany(shipTo.getMiscShipTo().getDeliverToName());
                }
                if (StringUtils.isEmpty(shipTo.getName())) {
                    shipTo.setName(shipTo.getMiscShipTo().getRequesterName());
                }
            }
            else {
                LOG.warn("Could not find address on account " + customerOrder.getAccount().getName()
                        + " with GLN ID of " + purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getGlnID());
            }
        }
        // Otherwise create a shipto from the provided shipto details on the order
        if (shipTo == null) {
            shipTo = addressBp.create(purchaseOrderDto.getShipToAddressDto());
        }
        if (StringUtils.isBlank(shipTo.getName()) && purchaseOrderDto.getShipToAddressDto() != null
                && purchaseOrderDto.getShipToAddressDto().getMiscShipToDto() != null) {
            shipTo.setName(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getRequesterName());
        }
        if (StringUtils.isBlank(shipTo.getName()) && purchaseOrderDto.getShipToAddressDto() != null
                && purchaseOrderDto.getShipToAddressDto().getMiscShipToDto() != null) {
            shipTo.setName(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getContactName());
        }
        // finally use the name data from the system user if no other ship to name is selected
        if (StringUtils.isBlank(shipTo.getName())) {
            shipTo.setName(customerOrder.getUser().getPerson().getFirstName() + " "
                    + customerOrder.getUser().getPerson().getLastName());
        }

        //TODO: per PHOEN-4530, generalize for all MiscShipTo and Address fields, instead of just the carrierrouting and airportcode
        if (customerOrder.getAddress() != null && customerOrder.getAddress().getMiscShipTo() != null) {
            if (shipTo.getMiscShipTo() == null) {
                shipTo.setMiscShipTo(new MiscShipTo());
            }
            if (StringUtils.isBlank(shipTo.getMiscShipTo().getCarrierRouting())) {
                shipTo.getMiscShipTo()
                        .setCarrierRouting(customerOrder.getAddress().getMiscShipTo().getCarrierRouting());
            }
            if (StringUtils.isBlank(shipTo.getMiscShipTo().getAirportCode())) {
                shipTo.getMiscShipTo().setAirportCode(customerOrder.getAddress().getMiscShipTo().getAirportCode());
            }
        }

        if (shipTo.getType() == null) {
            shipTo.setType(addressTypeBp.getByName(AddressTypeBp.SHIPTO_ADDRESS_TYPE));
        }

        customerOrder.setAddress(shipTo);

        this.addEmailAndFilterNotifications(customerOrder);

        //Set the order type
        if (customerOrder.getType() == null) {
            String orderType = purchaseOrderDto.getOrderType() != null ? purchaseOrderDto.getOrderType().getValue()
                    : "PUNCHOUT";
            customerOrder.setType(orderTypeBp.getTypeFromValue(orderType));
        }
        //Set the order date
        customerOrder.setOrderDate(new Date());
        if (purchaseOrderDto.getRequestedDeliveryDate() != null) {
            customerOrder.setRequestedDeliveryDate(purchaseOrderDto.getRequestedDeliveryDate());
        }
        if (purchaseOrderDto.getShipNoLaterDate() != null) {
            customerOrder.setShipNoLaterDate(purchaseOrderDto.getShipNoLaterDate());
        }
        if (purchaseOrderDto.getShipNotBeforeDate() != null) {
            customerOrder.setShipNotBeforeDate(purchaseOrderDto.getShipNotBeforeDate());
        }

        customerOrder.setOrderPayloadId(purchaseOrderDto.getOrderPayloadId());

        PoNumberDto customerPoDto = purchaseOrderDto.retrieveCustomerPoNumber();
        if (customerPoDto != null) {
            if (customerOrder.getCustomerPo() != null) {
                customerOrder.getCustomerPo().setValue(customerPoDto.getValue());
            }
            else {
                PoNumber customerPo = new PoNumber();
                customerPo.setValue(customerPoDto.getValue());
                customerPo.setType(poNumberTypeBp.getCustomerPoType());
                customerOrder.getPoNumbers().add(customerPo);
                customerPo.getOrders().add(customerOrder);
            }
        }

        customerOrder.setWrapAndLabel(customerOrder.getCredential().isWrapAndLabel());
        customerOrder.setAdot(customerOrder.getCredential().isAdot());

        //Compute tax
        computeOrderTax(customerOrder);
        //Calculate the order total
        customerOrder.refreshOrderTotal();
        return this.update(customerOrder);
    }

    /**
     * Calculates and stores the tax amounts for an order
     * 
     * @param order
     * @return
     */
    public CustomerOrder computeOrderTax(CustomerOrder order) {
        //calculates the tax on the order
        //first, gets the tax rate
        String zip = order.getAddress().getZip();
        String county = order.getAddress().getCounty();
        String city = order.getAddress().getCity();
        String state = order.getAddress().getState();
        String geocode = order.getAddress().getGeoCode();
        storeTaxableValues(zip, order);
        BigDecimal taxRate = BigDecimal.ZERO;
        try {
            TaxRate taxRateEntity = this.getTaxRate(zip, county, city, state, geocode);
            taxRate = taxRateEntity.getCombinedSales();
            this.populateOrderAddressValues(order, taxRateEntity);
            order.getTaxRatesAtPurchase().removeAll(order.getTaxRatesAtPurchase());
            order.getTaxRatesAtPurchase().addAll(taxRateAtPurchaseBp.getRateSet(taxRateEntity, order));
        }
        catch (NoTaxException e) {
            order.setTaxAssigned(false);
        }
        order.setMaximumTaxToCharge(BigDecimal.ZERO);
        //then, calculates and sets the tax rate for each item
        for (LineItem item : order.getItems()) {
            BigDecimal itemRate = BigDecimal.ZERO;
            if (item.getTaxable()) {
                itemRate = taxRate;
            }
            //finally, calculates and sets the item's tax
            BigDecimal itemTax = item.getTotalPrice().add(item.getEstimatedShippingAmount()).multiply(itemRate)
                    .setScale(2, RoundingMode.HALF_UP);
            item.setMaximumTaxToCharge(itemTax);
            order.setMaximumTaxToCharge(order.getMaximumTaxToCharge().add(itemTax));
        }
        return order;
    }

    private void storeTaxableValues(String zip, CustomerOrder order) {
        boolean exempt = this.credentialBp.isCredentialTaxExempt(order.getCredential(), zip);

        for (LineItem item : order.getItems()) {
            if (item.getTaxable() == null) {
                item.setTaxable(!exempt);
            }
        }
    }

    /**
     * Called when the order is placed, to set the payment information for the order.
     * @throws Exception 
     */
    private CustomerOrder setOrderPaymentInformation(CustomerOrder order, PurchaseOrderDto purchaseOrderDto) {
        PaymentInformationDto paymentInformationDto = purchaseOrderDto.getPaymentInformationDto();
        if (paymentInformationDto != null && StringUtils.isNotBlank(paymentInformationDto.getPaymentType())
                && paymentInformationDto.getPaymentType().equals(PaymentInformationDto.Type.INVOICE.getValue())) {
            order.setPaymentInformation(paymentInformationBp.createPaymentInformation(paymentInformationDto, order
                    .getAccount()));
            return update(order);
        }

        PaymentType searchType = new PaymentType();
        //defaults to invoice
        searchType.setName(PaymentInformationDto.Type.INVOICE.getValue());

        CredentialDto credentialDto = purchaseOrderDto.getCredential();
        boolean useDefaultCC = CredentialPropertyUtils.equals(CC_USE_DEFAULT_CARD, YES, credentialDto);
        if (purchaseOrderDto.getPaymentInformationDto() != null && !useDefaultCC) {
            //Default payment type to invoice
            if (StringUtils.isEmpty(purchaseOrderDto.getPaymentInformationDto().getPaymentType())) {
                purchaseOrderDto.getPaymentInformationDto().setPaymentType(
                        PaymentInformationDto.Type.INVOICE.getValue());
            }
            order.setPaymentInformation(paymentInformationBp.createPaymentInformation(purchaseOrderDto
                    .getPaymentInformationDto(), order.getAccount()));
        }
        else if (order.getPaymentInformation() == null || order.getPaymentInformation().getCard() == null
                || useDefaultCC) {
            //if credit card isn't set, and it's not forced to use invoice, uses set payment information
            PaymentInformation assignedInfo = getAssignedPaymentInformation(order.getAccount(), order.getCredential(),
                    order.getUser());
            if (assignedInfo != null) {
                order.setPaymentInformation(assignedInfo);
            }
            if (order.getPaymentInformation().getBillingAddress() == null) {
                order.getPaymentInformation().setBillingAddress(
                        addressBp.create(purchaseOrderDto.getPaymentInformationDto().getBillToAddress()));
            }
        }
        if (order.getPaymentInformation() == null) {
            throw new RuntimeException(
                    "Unable to create order.  No payment information provided, nor default payment information configured.");
        }
        if (order.getPaymentInformation() != null && order.getPaymentInformation().getCard() != null) {
            //paid with credit card included with submitted order
            searchType.setName(PaymentInformationDto.Type.CC.getValue());
            PaymentType type = paymentTypeDao.searchByExactExample(searchType, 0, 0).get(0);
            order.getPaymentInformation().setPaymentType(type);
        }
        if (order.getPaymentInformation().getPaymentType() == null) {
            PaymentType type = paymentTypeDao.searchByExactExample(searchType, 0, 0).get(0);
            order.getPaymentInformation().setPaymentType(type);
        }
        order = this.update(order);
        return order;
    }

    /**
     * This method adds the email entered on the cashout page or inbound DTO to each notification, then removes notification properties 
     * based on the notification limits on the order's Credential.
     */
    public void addEmailAndFilterNotifications(CustomerOrder order) {

    	if (order != null && order.getAddress() != null && order.getAddress().getMiscShipTo() != null && StringUtils.isNotBlank(order.getAddress().getMiscShipTo().getOrderEmail())) {
			for (EventType type : EventType.values()) {
				NotificationProperties notification = new NotificationProperties();
				notification.setAddressing(Addressing.TO);
				notification.setEventType(type);
				notification.setOrder(order);
				order.getNotificationProperties().add(notification);
				notification.setRecipients(order.getAddress().getMiscShipTo().getOrderEmail());
			}
    	}
    	
    	order.setCredential(this.credentialBp.findById(order.getCredential().getId(), Credential.class));
    	Map<EventType, LimitType> limitMap = new HashMap<EventType, LimitType>();
    	for (NotificationLimit limit : order.getCredential().getNotificationLimits()) {
    		limitMap.put(limit.getEventType(), limit.getLimitType());
    	}
    	Set<NotificationProperties> toRemove = new HashSet<>();
    	toRemove.addAll(order.getNotificationProperties());
    	for (NotificationProperties notification : order.getNotificationProperties()) {
    		LimitType limit = limitMap.get(notification.getEventType());
            if ((Addressing.CC.equals(notification.getAddressing()) && LimitType.CC_ONLY.equals(limit))
                    || (Addressing.BCC.equals(notification.getAddressing()) && (LimitType.BCC_ONLY.equals(limit) || limit == null)) 
                    || LimitType.ALL.equals(limit)) {
            	toRemove.remove(notification);
            }
    	}
    	order.getNotificationProperties().removeAll(toRemove);
    }

    /**
     * Returns the payment information to be used, based on the Account, Credential, and User. This will be a new, 
     * unmanaged entity.
     * 
     * @return
     */
    public PaymentInformation getAssignedPaymentInformation(Account account, Credential credential,
            SystemUser systemUser) {
        //gets the payment information from the user
        PaymentInformation assignedInformation = null;
        if (assignedInformation == null) {
            assignedInformation = systemUser.getPaymentInformation();
        }
        //if it's null, gets it from the Credential
        if (assignedInformation == null) {
            assignedInformation = credential.getPaymentInformation();
        }
        //if it's null, gets it from the account
        if (assignedInformation == null) {
            assignedInformation = account.getPaymentInformation();
        }
        //if there is no information assigened at all, returns null
        if (assignedInformation == null) {
            return null;
        }

        PaymentInformation toReturn = new PaymentInformation();
        toReturn.setContact(personBp.cloneEntity(assignedInformation.getContact()));
        toReturn.setBillingAddress(addressBp.cloneEntity(assignedInformation.getBillingAddress()));
        toReturn.setPaymentType(assignedInformation.getPaymentType());
        toReturn.setSummary(assignedInformation.isSummary());
        if (assignedInformation.getCard() != null) {
            //stored, assigned information is already compliant
            toReturn.setCard(new CardInformation());
            toReturn.getCard().setDebit(assignedInformation.getCard().getDebit());
            toReturn.getCard().setExpiration(assignedInformation.getCard().getExpiration());
            toReturn.getCard().setNameOnCard(assignedInformation.getCard().getNameOnCard());
            toReturn.getCard().setNumber(assignedInformation.getCard().getNumber());
            toReturn.getCard().setCardType(assignedInformation.getCard().getCardType());
            toReturn.getCard().setCardVaultToken(assignedInformation.getCard().getCardVaultToken());
            toReturn.getCard().setIdentifier(assignedInformation.getCard().getIdentifier());
            toReturn.getCard().setGhost(assignedInformation.getCard().isGhost());
            toReturn.getCard().setCvvIndicator(assignedInformation.getCard().getCvvIndicator());
        }

        return toReturn;
    }

    public CustomerOrder getCustomerOrder(POAcknowledgementDto po) {
        String apdPo = po.getApdPo();
        return this.searchByApdPo(apdPo);
    }

    public CustomerOrder getCustomerOrder(ShipmentDto shipmentDto) {
        if (shipmentDto.getCustomerOrderDto() != null) {
            String apdPo = shipmentDto.getCustomerOrderDto().getApdPoNumber();
            return this.searchByApdPo(apdPo);
        }
        else {
            return null;
        }
    }

    public CustomerOrder getCustomerOrder(CustomerInvoiceDto customerInvoiceDto) {
        String apdPo = customerInvoiceDto.getApdPo();
        return this.searchByApdPo(apdPo);
    }

    public CustomerOrder getCustomerOrder(CustomerCreditInvoiceDto customerCreditInvoiceDto) {
        if (customerCreditInvoiceDto.getReturnOrderDto() != null
                && customerCreditInvoiceDto.getReturnOrderDto().getOrder() != null) {
            String apdPo = customerCreditInvoiceDto.getReturnOrderDto().getOrder().getApdPoNumber();
            return this.searchByApdPo(apdPo);
        }
        else {
            return null;
        }
    }

    public POAcknowledgementDto createFullAcknowledgement(CustomerOrder customerOrder) {
        POAcknowledgementDto toReturn = new POAcknowledgementDto();
        try {
            toReturn.setPurchaseOrderDto(DtoFactory.createPurchaseOrderDto(customerOrder));
        }
        catch (ParsingException e) {
            LOGGER.error("Could not create full acknowledgemnt dto", e);
            return null;
        }
        toReturn.getPurchaseOrderDto().setPartnerDestination(
                customerOrder.getCredential().getPoAcknowledgementDestination());
        toReturn.setApdPo(customerOrder.getApdPo().getValue());
        toReturn.setLineItems(new ArrayList<LineItemDto>());

        for (LineItem lineItem : customerOrder.getItems()) {

            lineItemBp.setStatus(lineItem, lineItemStatusBp.getStatus(LineItemStatusEnum.ORDERED));

            LineItemDto lineItemDto = DtoFactory.createLineItemDto(lineItem);
            toReturn.getLineItems().add(lineItemDto);
        }
        return toReturn;
    }

    public POAcknowledgementDto createFullAcknowledgementForResend(CustomerOrder customerOrder) {
        POAcknowledgementDto toReturn = new POAcknowledgementDto();
        try {
            toReturn.setPurchaseOrderDto(DtoFactory.createPurchaseOrderDto(customerOrder));
        }
        catch (ParsingException e) {
            LOGGER.error("Could not create full acknowledgemnt dto", e);
            return null;
        }
        toReturn.getPurchaseOrderDto().setPartnerDestination(
                customerOrder.getCredential().getPoAcknowledgementDestination());
        toReturn.setApdPo(customerOrder.getApdPo().getValue());
        toReturn.setLineItems(new ArrayList<LineItemDto>());

        for (LineItem lineItem : customerOrder.getItems()) {
            LineItemDto lineItemDto = DtoFactory.createLineItemDto(lineItem);
            toReturn.getLineItems().add(lineItemDto);
        }
        return toReturn;
    }

    public MessageMetadata findOrderCreatedMetadata(String apdPo) throws Exception {
        CustomerOrder order = searchByApdPo(apdPo);
        if (order == null) {
            throw new Exception("Unable to find order with apdPo" + apdPo);
        }
        return ((CustomerOrderDao) dao).getOriginalMetadataForOrder(order);
    }

    public POAcknowledgementDto createAcknowledgment(ShipmentDto shipmentDto, CustomerOrder customerOrder) {
        POAcknowledgementDto toReturn = new POAcknowledgementDto();
        try {
            toReturn.setPurchaseOrderDto(DtoFactory.createPurchaseOrderDto(customerOrder));
        }
        catch (ParsingException e) {
            LOGGER.error("Could not create full acknowledgemnt dto", e);
            return null;
        }
        toReturn.setApdPo(customerOrder.getApdPo().getValue());
        toReturn.setLineItems(new ArrayList<LineItemDto>());
        LineItemStatusDto newStatus = DtoFactory.createLineItemStatusDto(lineItemStatusBp
                .getStatus(LineItemStatusEnum.ACCEPTED));
        for (LineItemXShipmentDto lineItemXShipmentDto : shipmentDto.getLineItemXShipments()) {
            if (lineItemXShipmentDto.getValid() != null && lineItemXShipmentDto.getValid()) {
                LineItemDto lineItemDto = lineItemXShipmentDto.getLineItemDto();
                lineItemDto.setStatus(newStatus);
                lineItemDto.setQuantity(lineItemXShipmentDto.getQuantity());
                toReturn.getLineItems().add(lineItemDto);
                lineItemXShipmentDto.setValid(null);
            }
        }
        return toReturn;
    }

    /**
     * This method returns orders that have been stuck for a certain amount of time. 
     * @param threshold - how long ago the order got stuck (hours)
     * @param window - the frequency of the checks so no order is returned twice (hours)
     * @return orders that have been stuck
     */
    public List<CustomerOrder> checkStuckOrders(int threshold, int window) {
        OrderStatus placedProcessing = orderStatusBp.getOrderStatus(OrderStatusEnum.PLACED_PROCESSING);
        Date startDate = new Date(System.currentTimeMillis() - ((threshold + window) * HOUR_IN_MS));
        Date endDate = new Date(System.currentTimeMillis() - (threshold * HOUR_IN_MS));
        CustomerOrderSearchCriteria criteria = new CustomerOrderSearchCriteria(placedProcessing, startDate, endDate,
                placedProcessing, null, null, null, null, null, null, null, null, null, null);
        return ((CustomerOrderDao) dao).getCustomerOrders(criteria);
    }

    public CustomerOrder internalRejectOrder(CustomerOrder customerOrder) {
        for (LineItem lineItem : customerOrder.getItems()) {
            lineItemBp.internallyReject(lineItem);
        }
        return this.update(customerOrder);
    }

    public CustomerOrder denyOrder(CustomerOrder customerOrder) {
        LineItemStatus denied = lineItemStatusBp.getStatus(LineItemStatusEnum.DENIED);
        for (LineItem lineItem : customerOrder.getItems()) {
            lineItemBp.setStatus(lineItem, denied);
        }
        return this.update(customerOrder);
    }

    public class NoTaxException extends Exception {

        private static final long serialVersionUID = 8513195776616166380L;

    }

    //Functionality to reject items if they have an invalid unit of measure. Currently not needed for any customer.
    private LineItem validateItem(LineItemDto lineItemDto, LineItem lineItem) {
        if ((!StringUtils.isEmpty(lineItemDto.getUnitOfMeasure().getName()))
                && (!lineItemDto.getUnitOfMeasure().getName().equals(lineItem.getUnitOfMeasure().getName()))) {
            final LineItemStatusDto rejected = new LineItemStatusDto();
            rejected.setValue(LineItemStatusEnum.REJECTED.getValue());
            lineItemDto.setStatus(rejected);
            lineItemBp.setStatus(lineItem, lineItemStatusBp.getStatusByValue(LineItemStatusEnum.REJECTED.getValue()));
            lineItem.setVendorComments(new ArrayList<String>());
            final String errorMessage = "Inbound unit of measure: " + lineItemDto.getUnitOfMeasure().getName()
                    + " does not match unit of measure in catalog " + lineItem.getUnitOfMeasure().getName();
            LOGGER.error(errorMessage);
            lineItem.getVendorComments().add(errorMessage);
        }
        return lineItem;
    }

    /**
     * Given an order, gets the USSCO Account number.
     * 
     * @param order
     * @return
     */
    public String getUsAccountNumber(CustomerOrder order) {
        if (order.getAddress() != null && order.getAddress().getMiscShipTo() != null
                && StringUtils.isNotBlank(order.getAddress().getMiscShipTo().getUsAccount())) {
            return order.getAddress().getMiscShipTo().getUsAccount();
        }
        return getDefaultUsscoAccountNumberForOrder(order);
    }

    public String getDefaultUsscoAccountNumberForOrder(CustomerOrder order) {
        AccountXCredentialXUser axcxu = this.getAXCXU(order);
        if (axcxu != null && StringUtils.isNotBlank(axcxu.getUsAccountNumber())) {
            return axcxu.getUsAccountNumber();
        }
        if (order.getCredential() != null
                && StringUtils.isNotBlank(order.getCredential().getPropertyValueByType(
                        CredentialPropertyTypeEnum.US_ACCT_NUM.getValue()))) {
            return order.getCredential().getPropertyValueByType(CredentialPropertyTypeEnum.US_ACCT_NUM.getValue());
        }
        return credXVendorDao.getAccountIdByCredentialAndVendor(vendorDao.getByName(USSCO_VENDOR_NAME), order
                .getCredential());
    }

    public String getAccountName(String apdPo) {
        return ((CustomerOrderDao) dao).getAccountName(apdPo);
    }

    /**
     * Given values of an address, returns the tax rate for that address.
     * 
     * @param zip
     * @return
     */
    public TaxRate getTaxRate(String zip, String county, String city, String state, String geocode)
            throws NoTaxException {
        TaxRate rate = taxRateBp.getTaxRate(zip, county, city, state, geocode);
        if (rate.getCombinedSales() == null) {
            throw new NoTaxException();
        }
        return rate;
    }

    private void populateOrderAddressValues(CustomerOrder order, TaxRate taxRate) {
        if (taxRate != null && order != null && order.getAddress() != null) {
            if (StringUtils.isBlank(order.getAddress().getCity())) {
                order.getAddress().setCity(taxRate.getCity());
            }
            if (StringUtils.isBlank(order.getAddress().getState())) {
                order.getAddress().setState(taxRate.getState());
            }
            if (StringUtils.isBlank(order.getAddress().getZip())) {
                order.getAddress().setZip(taxRate.getZip());
            }
            if (StringUtils.isBlank(order.getAddress().getCounty())) {
                order.getAddress().setCounty(taxRate.getCounty());
            }
            if (StringUtils.isBlank(order.getAddress().getGeoCode())) {
                order.getAddress().setGeoCode(taxRate.getGeocode());
            }
        }
    }

    public List<CustomerOrder> getOrdersPendingApproval() {
        return ((CustomerOrderDao) dao).getOrdersPendingApproval();

    }

    public Date getDateRequestedApproval(CustomerOrder order) {
        for (OrderLog orderLog : order.getOrderLogs()) {
            if (orderLog.getOrderStatus() != null
                    && OrderStatusEnum.PENDING_APPROVAL.getValue().equals(orderLog.getOrderStatus().getValue())) {
                return orderLog.getUpdateTimestamp();
            }
        }
        return null;

    }

    /**
     * Takes an order, and returns a list of fees that are applicable to the order. This disregards
     * existing fees, and may return fees with a price of zero (and therefore shouldn't be added
     * to the order).
     * 
     * @param order
     * @return
     */
    public List<LineItem> getOrderFees(CustomerOrder order) {
    	List<LineItem> toReturn = new ArrayList<>();
    	Credential credential = order.getCredential();
        String minimumOrderFee = credentialBp.getCredetialProperty(MIN_ORDER_FEE.getValue(), credential);
        String minimumOrderAmount = credentialBp.getCredetialProperty(MIN_ORDER_AMT_WITHOUT_ADDITIONAL_FEE.getValue(), credential);
        String handlingFee = credentialBp.getCredetialProperty(HANDLING_FEE.getValue(), credential);
        String minOrderFeeCommodityCode = credentialBp.getCredetialProperty(MIN_ORDER_FEE_COMMODITY_CODE.getValue(), credential);
        String handlingFeeCommodityCode = credentialBp.getCredetialProperty(HANDLING_FEE_COMMODITY_CODE.getValue(), credential);
        
        //calculates minimum order amount fee
		BigDecimal minOrderFeeAmount = BigDecimal.ZERO;
        try {
            if(StringUtils.isNotBlank(minimumOrderAmount) && StringUtils.isNotBlank(minimumOrderFee) && order.getSubtotalNoFees().compareTo(new BigDecimal(minimumOrderAmount)) < 0){
            	minOrderFeeAmount = new BigDecimal(minimumOrderFee);
            }
    	} catch (NumberFormatException e) {
    		LOGGER.error("Error parsing fee amounts", e);
    	}
        toReturn.add(generateFeeItem(minOrderFeeAmount, MINIMUM_ORDER_FEE_DESCRIPTION, MINIMUM_ORDER_FEE_SKU, minOrderFeeCommodityCode));
        
        //calculates handling fee
        BigDecimal handlingFeeAmount = BigDecimal.ZERO;
    	try {
            if (StringUtils.isNotBlank(handlingFee)) {
            	handlingFeeAmount = new BigDecimal(handlingFee);
            }
    	} catch (NumberFormatException e) {
    		LOGGER.error("Error parsing fee amounts", e);
    	}
        toReturn.add(generateFeeItem(handlingFeeAmount, HANDLING_FEE_DESCRIPTION, HANDLING_FEE_SKU, handlingFeeCommodityCode));

        return toReturn;
    }

    private LineItem generateFeeItem(BigDecimal amount, String description, String sku, String commodityCode) {
        LineItem feeItem = lineItemBp.createFee();
        if (StringUtils.isNotBlank(commodityCode)) {
            feeItem.setUnspscClassification(commodityCode);
        }
        feeItem.setDescription(description);
        feeItem.setUnitPrice(amount);
        feeItem.setApdSku(sku);
        feeItem.setSupplierPartId(sku);
        feeItem.setSupplierPartAuxiliaryId(sku);
        return feeItem;
    }

    /**
     * This method recalculates the appropriate fees an a customer order. If those fees exist on the order, 
     * then the dollar amount is updated, otherwise adds the new fee to the order.
     * 
     * @param order
     * @return
     */
    private CustomerOrder recalculateFeeAmounts(CustomerOrder order) {
        List<LineItem> feeItems = this.getOrderFees(order);
        for (LineItem feeItem : feeItems) {
            boolean updatedExistingItem = false;
            for (LineItem item : order.getItems()) {
                //turns item into lineitemdto, to force SKU checking. Also checks if the order item
                //is a fee, so that new fees are created when items are rejected per PHOEN-4871
                //instead of updating an existing, rejected item
                if (item.matchesItemOnOrder(DtoFactory.createLineItemDto(feeItem)) && item.isFee()) {
                    item.setUnitPrice(item.getUnitPrice());
                    updatedExistingItem = true;
                    break;
                }
            }
            if (!updatedExistingItem && feeItem.getUnitPrice().compareTo(BigDecimal.ZERO) > 0) {
                order.getItems().add(feeItem);
                feeItem.setOrder(order);
                feeItem.setLineNumber(order.getMaxLineNumber() + 1);
            }
        }
        return order;
    }

    @Override
    public CustomerOrder eagerLoad(CustomerOrder toLoad) {
        //eagerLoad method is not performant for CustomerOrder, so only returning a managed entity
        LOGGER.warn("Called eagerLoad method on CustomerOrderBp. Returning managed entity instead.");
        if (toLoad != null && toLoad.getId() != null) {
            toLoad = this.findById(toLoad.getId(), CustomerOrder.class);
        }
        return toLoad;
    }

    public POAcknowledgementDto populateOrderAcknowledgement(CustomerOrder customerOrder,
            POAcknowledgementDto poAcknowledgementDto) {
        poAcknowledgementDto.setPurchaseOrderDto(verifyAndPopulateFields(poAcknowledgementDto.getPurchaseOrderDto(),
                customerOrder));
        //populate required fields from customer data for outbound acknowledgements
        for (int i = 0; i < poAcknowledgementDto.getLineItems().size(); i++) {
            LineItemDto item = poAcknowledgementDto.getLineItems().get(i);
            poAcknowledgementDto.getLineItems().set(i, updateCustomerRequiredFields(poAcknowledgementDto, item));
        }

        Credential credential = customerOrder.getCredential();
        poAcknowledgementDto.setPartnerId(credential.getPartnerId());
        return poAcknowledgementDto;
    }

    //Sets fields with data from original order where the customer is expecting such data
    private PurchaseOrderDto verifyAndPopulateFields(PurchaseOrderDto purchaseOrderDto, CustomerOrder customerOrder) {
        if (customerOrder.getAddress().getMiscShipTo() != null) {
            if (purchaseOrderDto.getShipToAddressDto().getMiscShipToDto() == null) {
                purchaseOrderDto.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
            }
            MiscShipTo miscShipEntity = customerOrder.getAddress().getMiscShipTo();
            MiscShipToDto miscShipDto = purchaseOrderDto.getShipToAddressDto().getMiscShipToDto();
            if (useCustomerData(miscShipDto.getAddressId(), miscShipEntity.getAddressId(), "AddressId")) {
                purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().setAddressId(miscShipEntity.getAddressId());
            }
            if (useCustomerData(miscShipDto.getContractNumber(), miscShipEntity.getContractNumber(), "ContractNumber")) {
                purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().setContractNumber(
                        miscShipEntity.getContractNumber());
            }
            if (useCustomerData(miscShipDto.getGlNumber(), miscShipEntity.getGlNumber(), "GlNumber")) {
                purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().setGlNumber(miscShipDto.getGlNumber());
            }
        }
        if (useCustomerData(purchaseOrderDto.getShipToAddressDto().getName(), customerOrder.getAddress().getName(),
                "AddressName")) {
            purchaseOrderDto.getShipToAddressDto().setName(customerOrder.getAddress().getName());
        }
        return purchaseOrderDto;

    }

    private LineItemDto updateCustomerRequiredFields(POAcknowledgementDto poAcknowledgementDto, LineItemDto vendorItem) {
        for (LineItemDto customerItem : poAcknowledgementDto.getPurchaseOrderDto().getItems()) {
            if (customerItem != null && customerItem.matchesItemOnOrder(vendorItem)) {
                String customerUnitPrice = vendorItem.getUnitPrice() != null ? vendorItem.getUnitPrice()
                        .toPlainString() : null;
                String vendorUnitPrice = customerItem.getUnitPrice() != null ? customerItem.getUnitPrice()
                        .toPlainString() : null;
                if (useCustomerData(vendorUnitPrice, customerUnitPrice, "customerLineNumber")) {
                    vendorItem.setUnitPrice(customerItem.getUnitPrice());
                }
                if (useCustomerData(vendorItem.getCustomerLineNumber(), customerItem.getCustomerLineNumber(),
                        "customerLineNumber")) {
                    vendorItem.setCustomerLineNumber(customerItem.getCustomerLineNumber());
                }
                if (customerItem.getCustomerSku() != null
                        && vendorItem.getCustomerSku() != null
                        && useCustomerData(vendorItem.getCustomerSku().getValue(), customerItem.getCustomerSku()
                                .getValue(), "customerSku")) {
                    vendorItem.setCustomerSku(customerItem.getCustomerSku());
                }
                if (useCustomerData(vendorItem.getApdSku(), customerItem.getApdSku(), "apdSku")) {
                    vendorItem.setApdSku(customerItem.getApdSku());
                }

            }
        }
        return vendorItem;
    }

    //Logs errrors where the vendor had different data than the orignal order
    private Boolean useCustomerData(String dtoField, String entityField, String fieldName) {
        if (entityField != null) {
            if (!entityField.equals(dtoField) && dtoField != null) {
                LOG.error("Replacing vendor " + fieldName + ": " + dtoField + " With customer " + fieldName + ": "
                        + entityField + ".");
            }
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public LineItem findLastTimeItemOrdered(LineItem item, SystemUser user) {
        if (item.getItem() == null) {
            return null;
        }
        return ((CustomerOrderDao) dao).findLastTimeItemOrdered(user, item);

    }

    public boolean isInWorkflowLimbo(CustomerOrder order) {
        return order != null && order.getId() != null && this.orderBRMSRecordBp.getProcessId(order.getId()) == null
                && order.hasAllowedStatus();
    }
}
