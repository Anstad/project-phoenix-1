package com.apd.phoenix.service.business;

import java.util.List;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.persistence.jpa.UserRequestDao;

@Stateless
@LocalBean
public class UserRequestBp extends AbstractBp<UserRequest> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(UserRequestDao dao) {
        this.dao = dao;
    }

    public UserRequest createUserRequest() {
        String token = UUID.randomUUID().toString();
        UserRequest u = new UserRequest();
        u.setToken(token);
        create(u);
        return u;
    }

    public Long getIdbyToken(String token) {
        return getFirst(((UserRequestDao) dao).getIdByToken(token));
    }

    private <T> T getFirst(List<T> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }
}
