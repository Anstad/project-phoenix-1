/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client;

import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Set;

/**
 *
 * @author dnorris
 */
public class SplitCharge {

    private BigDecimal totalFreightPrice;
    private BigDecimal totalTaxPrice;
    private BigDecimal subTotalPrice;
    private BigDecimal totalPrice;

    private SplitCharge() {
    }

    public BigDecimal getTotalFreightPrice() {
        return totalFreightPrice;
    }

    public void setTotalFreightPrice(BigDecimal totalFreightPrice) {
        this.totalFreightPrice = totalFreightPrice.setScale(2, RoundingMode.CEILING);
    }

    public BigDecimal getTotalTaxPrice() {
        return totalTaxPrice;
    }

    public void setTotalTaxPrice(BigDecimal totalTaxPrice) {
        this.totalTaxPrice = totalTaxPrice.setScale(2, RoundingMode.CEILING);
    }

    public BigDecimal getSubTotalPrice() {
        return subTotalPrice;
    }

    public void setSubTotalPrice(BigDecimal subTotalPrice) {
        this.subTotalPrice = subTotalPrice.setScale(2, RoundingMode.CEILING);
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice.setScale(2, RoundingMode.CEILING);
    }

    public static SplitCharge createSplitCharge(Set<LineItemXShipment> contents) {
        SplitCharge splitCharge = new SplitCharge();
        initializeChargeAmounts(splitCharge, contents);
        return splitCharge;
    }

    private static void initializeChargeAmounts(SplitCharge splitCharge, Set<LineItemXShipment> contents) {
        BigDecimal totalTaxAmount = new BigDecimal(0);
        BigDecimal totalFreightAmount = new BigDecimal(0);
        BigDecimal subTotalAmount = new BigDecimal(0);
        BigDecimal totalAmount = new BigDecimal(0);
        for (LineItemXShipment lineItemXShipment : contents) {
            totalTaxAmount = totalTaxAmount.add(lineItemXShipment.getTotalTaxAmount());
            totalFreightAmount = totalFreightAmount.add(lineItemXShipment.getShipping());
            subTotalAmount = subTotalAmount.add(lineItemXShipment.getSubTotalAmount());
            totalAmount = totalAmount.add(lineItemXShipment.getTotalAmount());
        }
        splitCharge.setTotalTaxPrice(totalTaxAmount);
        splitCharge.setTotalFreightPrice(totalFreightAmount);
        splitCharge.setSubTotalPrice(subTotalAmount);
        splitCharge.setTotalPrice(totalAmount);
    }

    public static SplitCharge createReturnSplitCharge(Set<LineItemXReturn> contents) {
        SplitCharge splitCharge = new SplitCharge();
        initializeReturnChargeAmounts(splitCharge, contents);
        return splitCharge;
    }

    private static void initializeReturnChargeAmounts(SplitCharge splitCharge, Set<LineItemXReturn> contents) {
        BigDecimal totalTaxAmount = new BigDecimal(0);
        BigDecimal subTotalAmount = new BigDecimal(0);
        BigDecimal totalAmount = new BigDecimal(0);
        for (LineItemXReturn lineItemXReturn : contents) {
            totalTaxAmount = totalTaxAmount.add(lineItemXReturn.getTotalTaxAmount());
            subTotalAmount = subTotalAmount.add(lineItemXReturn.getSubTotalAmount());
            totalAmount = totalAmount.add(lineItemXReturn.getTotalTaxAmount()).add(lineItemXReturn.getSubTotalAmount());
        }
        splitCharge.setTotalTaxPrice(totalTaxAmount);
        splitCharge.setTotalFreightPrice(BigDecimal.ZERO);
        splitCharge.setSubTotalPrice(subTotalAmount);
        splitCharge.setTotalPrice(totalAmount);
    }

}
