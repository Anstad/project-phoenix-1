package com.apd.phoenix.service.business;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.JumptrackStopConfiguration;
import com.apd.phoenix.service.persistence.jpa.JumptrackStopConfigurationDao;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class JumptrackStopConfigurationBp extends AbstractBp<JumptrackStopConfiguration> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JumptrackStopConfigurationBp.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    public void initDao(JumptrackStopConfigurationDao dao) {
        this.dao = dao;
    }

    public Date getNextDeliveryDate(String zip) {
        return getNextDeliveryDate(zip, new Date());
    }

    public Date getNextDeliveryDate(String zip, Date date) {
        //Setting delivery date 
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        JumptrackStopConfiguration config = this.getConfigurationFromZip(zip);
        do {
            c.add(Calendar.DATE, 1);
        }
        while (!deliveryDateAllowed(c, config));
        return c.getTime();
    }

    private JumptrackStopConfiguration getConfigurationFromZip(String zip) {
        //if the zip is blank or null, returns the default configuration
        if (StringUtils.isBlank(zip)) {
            return new JumptrackStopConfiguration();
        }
        JumptrackStopConfiguration searchConfig = new JumptrackStopConfiguration();
        searchConfig.setZip(zip);
        List<JumptrackStopConfiguration> resultList = this.searchByExactExample(searchConfig, 0, 0);
        //if there are no configurations with that zip, returns the default configuration
        if (resultList == null || resultList.isEmpty()) {
            LOGGER.debug("No stop configuration found for zip : " + zip + " , using default configuration.");
            return new JumptrackStopConfiguration();
        }
        //only one result should be found, there is a database constraint that prevents 
        //multiple configs for the same zip code. 
        if (resultList.size() > 1) {
            LOGGER.warn("Multiple configurations found for zip : " + zip + " , one will be used at random.");
        }
        return resultList.get(0);
    }

    private boolean deliveryDateAllowed(Calendar c, JumptrackStopConfiguration config) {
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());
        if (config.isAllowShipSunday() && Calendar.SUNDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipMonday() && Calendar.MONDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipTuesday() && Calendar.TUESDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipWednesday() && Calendar.WEDNESDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipThursday() && Calendar.THURSDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipFriday() && Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        if (config.isAllowShipSaturday() && Calendar.SATURDAY == c.get(Calendar.DAY_OF_WEEK)) {
            return true;
        }
        //if the day being considered is the same as the current day, then all other days have been 
        //rejected. Accepts this day, to prevent an infinite loop, and logs a warning
        if (currentCalendar.get(Calendar.DAY_OF_WEEK) == c.get(Calendar.DAY_OF_WEEK)) {
            LOGGER.warn("No dates were acceptable for JumptrackStopConfiguration id : "
                    + (config.getId() != null ? config.getId() : "null"));
            return true;
        }
        return false;
    }
}