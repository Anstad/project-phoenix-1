/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class LineItemStatus.
 */
@Entity
@XmlRootElement
public class LineItemStatus implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The line item status description. */
    @Column
    private String description;

    /** The value. */
    @Column
    private String value;

    @Column(nullable = false)
    private boolean paymentStatus = false;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((LineItemStatus) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (description != null && !description.trim().isEmpty())
            result += "description: " + description;
        if (value != null && !value.trim().isEmpty())
            result += ", value: " + value;
        return result;
    }

    /**
     * This enum lists the different line item statuses.
     * 
     * @author RHC
     *
     */
    public enum LineItemStatusEnum {
        RETURNED("RETURNED", "The line item was returned"), PARTIAL_CANCEL("PARTIAL_CANCEL",
                "The line item was partially canceled"), PARTIAL_RETURN("PARTIAL_RETURN",
                "The line item was partially returned"), CREATED("CREATED", "The line item was created"), ORDERED(
                "ORDERED", "The line item was ordered"), ACCEPTED("ACCEPTED", "The line item was accepted"), REJECTED(
                "REJECTED", "The line item was rejected"), DENIED("DENIED", "The line item was denied"), BACKORDERED(
                "BACKORDERED", "The line item is back ordered"), CANCELED("CANCELED", "The line item was canceled"), PARTIAL_SHIP(
                "PARTIAL_SHIP", "The line item is partially shipped"), FULLY_SHIPPED("FULLY_SHIPPED",
                "The line item is fully shipped"), PARTIAL_BILL("PARTIAL_BILL", "The line item is partially billed"), FULLY_BILLED(
                "FULLY_BILLED", "The line item was fully billed"), INVOICE_RECEIVED("INVOICE_RECEIVED",
                "The line item was invoiced to APD"), PARTIAL_BACKORDERED("PARTIAL_BACKORDERED",
                "The line item was partially backordered"), ITEM_QUANTITY_CHANGED("ITEM_QUANTITY_CHANGED",
                "The line item quantity was changed"), PARTIAL_CHARGE("PARTIAL_CHARGE",
                "The line item has been partially charged"), FULLY_CHARGED("FULLY_CHARGED",
                "The line item was fully charged"), RESENT("RESENT", "The line item was resent to the vendor"), MANUALLY_RESOLVED(
                "MANUALLY_RESOLVED", "The line item was manually marked as resolved");

        private final String description;
        private final String value;

        private LineItemStatusEnum(String value, String description) {
            this.description = description;
            this.value = value;
        }

        public String getLabel() {
            return this.description;
        }

        public String getValue() {
            return this.value;
        }

    }
}