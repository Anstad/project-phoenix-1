package com.apd.phoenix.service.message.impl;

import javax.annotation.Resource;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderLog.EventType;

public class DenyOrderMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(DenyOrderMessageSender.class);

    @Resource(mappedName = "java:/activemq/deny-order-queue")
    private Destination denyOrderQueue;

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        // TODO Auto-generated method stub
        return false;
    }

    public boolean sendDenyOrder(CustomerOrder order) {

        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(denyOrderQueue);
                messageProducer.send(createDenyOrderMessage(msession, order));
            }
            catch (Exception e) {
                LOGGER.error("Exception when creating jumptrack stop notification request", e);
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error("Exception when closing connection after jumptrack stop notification request", e);
        }
        return true;
    }

    private javax.jms.Message createDenyOrderMessage(Session msession, CustomerOrder order) {
        ObjectMessage objectMessage = null;
        try {
            objectMessage = msession.createObjectMessage(order);
        }
        catch (JMSException e) {
            LOGGER.error(e.toString());
        }
        return objectMessage;
    }

}
