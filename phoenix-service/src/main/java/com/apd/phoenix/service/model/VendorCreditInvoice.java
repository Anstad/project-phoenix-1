/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.model;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
@Entity
@DiscriminatorValue(value = "VENDOR_CREDIT_INVOICE")
public class VendorCreditInvoice extends Invoice {

    private static final long serialVersionUID = 4439600474634073997L;
    @ManyToOne
    private CustomerOrder customerOrder;
    
    //Return Authorization Number
    @Column
    private String raNumber;
    
    @Column
    private BigDecimal restockingFee = BigDecimal.ZERO;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinColumn(name = "INVOICE_ID")
    @Deprecated //use invoice.actualItems instead
    private Set<LineItemXReturn> items = new HashSet<>();
    
    @Enumerated(EnumType.STRING)
    private ReturnOrder.ReturnOrderStatus status = ReturnOrder.ReturnOrderStatus.UNRECONCILED;

    /**
     * @return the customerOrder
     */
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    /**
     * @param customerOrder the customerOrder to set
     */
    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    /**
     * @return the restockingFee
     */
    public BigDecimal getRestockingFee() {
        return restockingFee;
    }

    /**
     * @param restockingFee the restockingFee to set
     */
    public void setRestockingFee(BigDecimal restockingFee) {
        this.restockingFee = restockingFee;
    }

    /**
     * Deprecated - use invoice.actualitems instead
     * 
     * @return the items
     */
    @Deprecated
    public Set<LineItemXReturn> getItems() {
        return items;
    }

    /**
     * Deprecated - use invoice.actualitems instead
     * 
     * @param items the items to set
     */
    @Deprecated 
    public void setItems(Set<LineItemXReturn> items) {
        this.items = items;
    }

    /**
     * @return the status
     */
    public ReturnOrder.ReturnOrderStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(ReturnOrder.ReturnOrderStatus status) {
        this.status = status;
    }

    /**
     * @return the raNumber
     */
    public String getRaNumber() {
        return raNumber;
    }

    /**
     * @param raNumber the raNumber to set
     */
    public void setRaNumber(String raNumber) {
        this.raNumber = raNumber;
    }

}
