package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

/**
 * This entity is used to store information for payments, such as billing address, and has a many-to-one 
 * relationship to PaymentType, as well as a one-to-many relationship with CardInformation.
 * 
 * @author RHC
 *
 */
@Entity
@Audited
public class PaymentInformation implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 4604524377543213510L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    private PaymentType paymentType;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private CardInformation card;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Person contact;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotAudited
    private InvoiceSendMethod invoiceSendMethod;

    @Column(nullable = false)
    private boolean summary = false;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Address billingAddress;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (id != null)
            result += "id: " + id;
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((PaymentInformation) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public PaymentType getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(final PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public CardInformation getCard() {
        return this.card;
    }

    public void setCard(final CardInformation card) {
        this.card = card;
    }

    public Person getContact() {
        return this.contact;
    }

    public void setContact(final Person contact) {
        this.contact = contact;
    }

    @Deprecated
    public InvoiceSendMethod getInvoiceSendMethod() {
        return this.invoiceSendMethod;
    }

    @Deprecated
    public void setInvoiceSendMethod(final InvoiceSendMethod invoiceSendMethod) {
        this.invoiceSendMethod = invoiceSendMethod;
    }

    public boolean isSummary() {
        return summary;
    }

    public void setSummary(boolean summary) {
        this.summary = summary;
    }

    public Address getBillingAddress() {
        return this.billingAddress;
    }

    public void setBillingAddress(Address billAddr) {
        this.billingAddress = billAddr;
    }
}