package com.apd.phoenix.service.persistence.jpa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.IssueLogSearchCriteria;
import com.apd.phoenix.service.model.IssueLog;

/**
 * IssueLog DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class IssueLogDao extends AbstractDao<IssueLog> {

    @SuppressWarnings("unchecked")
    public List<IssueLog> getIssueLogs(IssueLogSearchCriteria criteria) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT issueLog FROM IssueLog AS issueLog"
                + " LEFT OUTER JOIN FETCH issueLog.customerOrder AS customerOrder"
        		+ " LEFT OUTER JOIN FETCH customerOrder.poNumbers AS poNumbers"
        		+ " LEFT OUTER JOIN FETCH customerOrder.account AS account"
        		+ " LEFT OUTER JOIN FETCH account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN FETCH issueLog.comments");

        hql.append(" WHERE 1=1");
        Map<String, Object> paramMap = new HashMap<>();
        if (criteria.getStartDate() != null) {
            hql.append(" AND issueLog.openedDate > :startDate");
            paramMap.put("startDate", criteria.getStartDate());
        }
        if (criteria.getEndDate() != null) {
            hql.append(" AND issueLog.updateTimestamp < :endDate");
            paramMap.put("endDate", criteria.getEndDate());
        }
        if (StringUtils.isNotBlank(criteria.getDepartment())) {
            hql.append(" AND LOWER(issueLog.department) LIKE :department");
            paramMap.put("department", criteria.getDepartment().toLowerCase());
        }
        if (StringUtils.isNotBlank(criteria.getContactName())) {
            hql.append(" AND LOWER(contactName) LIKE :contactName");
            paramMap.put("contactName", "%" + criteria.getContactName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactNumber())) {
            hql.append(" AND LOWER(contactNumber) LIKE :contactNumber");
            paramMap.put("contactNumber", "%" + criteria.getContactNumber().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactEmail())) {
            hql.append(" AND LOWER(contactEmail) LIKE :contactEmail");
            paramMap.put("contactEmail", "%" + criteria.getContactEmail().toLowerCase() + "%");
        }
        if (criteria.getIssueCategory() != null) {
            hql.append(" AND issueLog.issueCategory=:issueCategory");
            paramMap.put("issueCategory", criteria.getIssueCategory());
        }
        if (criteria.getTicketStatus() != null) {
            hql.append(" AND issueLog.status=:ticketStatus");
            paramMap.put("ticketStatus", criteria.getTicketStatus());
        }
        if (StringUtils.isNotBlank(criteria.getTicketNumber())) {
            hql.append(" AND issueLog.ticketNumber=:ticketNumber");
            paramMap.put("ticketNumber", criteria.getTicketNumber());
        }
        if (criteria.getAccountWhiteList() != null) {
        	hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
            paramMap.put("accountWhiteList", criteria.getAccountWhiteList());
        }
        
        hql.append(" ORDER BY issueLog.openedDate desc");
        if (paramMap.keySet().size() == 0) {
        	//no parameters set
        	//return null;
        }

        Query query = entityManager.createQuery(hql.toString());
        
        for (String key : paramMap.keySet()) {
                if(key.equals("startDate")){
                    query.setParameter(key, criteria.getStartDate(), TemporalType.TIMESTAMP);
                }
                else if(key.equals("endDate")){
                    query.setParameter(key, criteria.getEndDate(), TemporalType.TIMESTAMP);
                }
                else if(paramMap.keySet().size() > 0){
                    query.setParameter(key, paramMap.get(key));
                }
        }
        final List<IssueLog> resultList = query.getResultList();
        return resultList;
    }

    @SuppressWarnings("unchecked")
    public List<IssueLog> getIssueLogsPaginated(IssueLogSearchCriteria criteria) {
        StringBuilder hql = new StringBuilder("SELECT DISTINCT issueLog FROM IssueLog AS issueLog"
                + " LEFT OUTER JOIN FETCH issueLog.customerOrder AS customerOrder"
                + " LEFT OUTER JOIN FETCH customerOrder.poNumbers AS poNumbers"
                + " LEFT OUTER JOIN FETCH customerOrder.account AS account"
        		+ " LEFT OUTER JOIN FETCH account.rootAccount AS rootAccount"
                + " LEFT OUTER JOIN FETCH issueLog.comments");

        hql.append(" WHERE 1=1");
        Map<String, Object> paramMap = new HashMap<>();
        if (criteria.getStartDate() != null) {
            hql.append(" AND issueLog.openedDate > :startDate");
            paramMap.put("startDate", criteria.getStartDate());
        }
        if (criteria.getEndDate() != null) {
            hql.append(" AND issueLog.updateTimestamp < :endDate");
            paramMap.put("endDate", criteria.getEndDate());
        }
        if (StringUtils.isNotBlank(criteria.getDepartment())) {
            hql.append(" AND LOWER(issueLog.department) LIKE :department");
            paramMap.put("department", criteria.getDepartment().toLowerCase());
        }
        if (StringUtils.isNotBlank(criteria.getContactName())) {
            hql.append(" AND LOWER(contactName) LIKE :contactName");
            paramMap.put("contactName", "%" + criteria.getContactName().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactNumber())) {
            hql.append(" AND LOWER(contactNumber) LIKE :contactNumber");
            paramMap.put("contactNumber", "%" + criteria.getContactNumber().toLowerCase() + "%");
        }
        if (StringUtils.isNotBlank(criteria.getContactEmail())) {
            hql.append(" AND LOWER(contactEmail) LIKE :contactEmail");
            paramMap.put("contactEmail", "%" + criteria.getContactEmail().toLowerCase() + "%");
        }
        if (criteria.getTicketStatus() != null) {
            hql.append(" AND issueLog.status=:ticketStatus");
            paramMap.put("ticketStatus", criteria.getTicketStatus());
        }
        if (StringUtils.isNotBlank(criteria.getTicketNumber())) {
            hql.append(" AND issueLog.ticketNumber=:ticketNumber");
            paramMap.put("ticketNumber", criteria.getTicketNumber());
        }
        if (criteria.getAccountWhiteList() != null) {
        	hql.append(" AND (account.id in (:accountWhiteList) or rootAccount.id in (:accountWhiteList))");
            paramMap.put("accountWhiteList", criteria.getAccountWhiteList());
        }
        
        hql.append(" ORDER BY issueLog.openedDate desc");
        if (paramMap.keySet().size() == 0) {
        	//no parameters set
        	//return null;
        }

        Query query = entityManager.createQuery(hql.toString());
        
        for (String key : paramMap.keySet()) {
                if(key.equals("startDate")){
                    query.setParameter(key, criteria.getStartDate(), TemporalType.TIMESTAMP);
                }
                else if(key.equals("endDate")){
                    query.setParameter(key, criteria.getEndDate(), TemporalType.TIMESTAMP);
                }
                else if(paramMap.keySet().size() > 0){
                    query.setParameter(key, paramMap.get(key));
                }
        }
        setItemsReturned(query, criteria.getStart(), criteria.getPageSize());
        final List<IssueLog> resultList = query.getResultList();
        return resultList;
    }
}
