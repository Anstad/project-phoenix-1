package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.persistence.jpa.MiscShipToDao;

/**
 *
 * @author RHC
 */
@Stateless
@LocalBean
public class MiscShipToBp extends AbstractBp<MiscShipTo> {

    private MiscShipToDao dao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(MiscShipToDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    public MiscShipTo create(MiscShipToDto miscShipToDto) {
        MiscShipTo toReturn = new MiscShipTo();
        toReturn.setAddressId(miscShipToDto.getAddressId());
        toReturn.setAddressId36(miscShipToDto.getAddressId36());
        toReturn.setAirportCode(miscShipToDto.getAirportCode());
        toReturn.setApdInterchangeId(miscShipToDto.getApdInterchangeId());
        toReturn.setArea(miscShipToDto.getArea());
        toReturn.setCarrierName(miscShipToDto.getCarrierName());
        toReturn.setContractingOfficer(miscShipToDto.getContractingOfficer());
        toReturn.setContractNumber(miscShipToDto.getContractNumber());
        toReturn.setDcNumber(miscShipToDto.getDcNumber());
        toReturn.setDeliverToName(miscShipToDto.getDeliverToName());
        toReturn.setDeliveryDate(miscShipToDto.getDeliveryDate());
        toReturn.setDept(miscShipToDto.getDept());
        toReturn.setDepartment(miscShipToDto.getDepartment());
        toReturn.setDesktop(miscShipToDto.getDesktop());
        toReturn.setDistrict(miscShipToDto.getDistrict());
        toReturn.setDivision(miscShipToDto.getDivision());
        toReturn.setEdiID(miscShipToDto.getEdiID());
        toReturn.setFacility(miscShipToDto.getFacility());
        toReturn.setFinanceNumber(miscShipToDto.getFinanceNumber());
        toReturn.setGlNumber(miscShipToDto.getGlNumber());
        toReturn.setGlnID(miscShipToDto.getGlnID());
        toReturn.setHeaderComments(miscShipToDto.getHeaderComments());
        toReturn.setLob(miscShipToDto.getLob());
        toReturn.setLocationId(miscShipToDto.getLocationId());
        toReturn.setLocations(miscShipToDto.getLocations());
        toReturn.setMailStop(miscShipToDto.getMailStop());
        toReturn.setManager(miscShipToDto.getManager());
        toReturn.setNameOfHospital(miscShipToDto.getNameOfHospital());
        toReturn.setNowl(miscShipToDto.getNowl());
        toReturn.setOldRelease(miscShipToDto.getOldRelease());
        toReturn.setOrderEmail(miscShipToDto.getOrderEmail());
        toReturn.setOrigin(miscShipToDto.getOrigin());
        toReturn.setPol(miscShipToDto.getPol());
        toReturn.setPurchasingGroup(miscShipToDto.getPurchasingGroup());
        toReturn.setRequesterName(miscShipToDto.getRequesterName());
        toReturn.setRequesterPhone(miscShipToDto.getRequesterPhone());
        toReturn.setShippingMethod(miscShipToDto.getShippingMethod());
        toReturn.setSolomonId(miscShipToDto.getSolomonId());
        toReturn.setStoreNumber(miscShipToDto.getStoreNumber());
        toReturn.setStreet2(miscShipToDto.getStreet2());
        toReturn.setStreet3(miscShipToDto.getStreet3());
        toReturn.setUnit(miscShipToDto.getUnit());
        toReturn.setUsAccount(miscShipToDto.getUsAccount());
        toReturn.setUspsUnder13Oz(miscShipToDto.getUspsUnder13Oz());
        toReturn.setVendor(miscShipToDto.getVendor());
        toReturn.setFedstripNumber(miscShipToDto.getFedstripNumber());
        return create(toReturn);
    }
}
