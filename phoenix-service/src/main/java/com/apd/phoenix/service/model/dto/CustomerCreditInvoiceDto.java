/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

public class CustomerCreditInvoiceDto extends InvoiceDto {

    private static final long serialVersionUID = -6118246826896430473L;
    private ReturnOrderDto returnOrderDto;

    public ReturnOrderDto getReturnOrderDto() {
        return returnOrderDto;
    }

    public void setReturnOrderDto(ReturnOrderDto returnOrderDto) {
        this.returnOrderDto = returnOrderDto;
    }

}
