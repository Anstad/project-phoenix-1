package com.apd.phoenix.service.user;

public interface AccountUpdateApprovalService {

    public String generateToken(long processInstanceId);

    public boolean approveTask(String token);

    public boolean rejectTask(String token);

    public Long getProcessIdByToken(String token);
}
