package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;

@Entity
@DiscriminatorValue(value = "RETURNORDER_LOG")
public class ReturnOrderLog extends OrderLog {

    private static final long serialVersionUID = 7146387732022032666L;

    /** The return order. */
    @ManyToOne(fetch = FetchType.LAZY)
    private ReturnOrder returnOrder;

    @Enumerated(EnumType.STRING)
    private ReturnOrderStatus returnOrderStatus;

    /**
     * Gets the return order.
     *
     * @return the return order
     */
    public ReturnOrder getReturnOrder() {
        return this.returnOrder;
    }

    /**
     * Sets the return order.
     *
     * @param returnOrder the new customer order
     */
    public void setReturnOrder(final ReturnOrder returnOrder) {
        this.returnOrder = returnOrder;
    }

    public ReturnOrderStatus getReturnOrderStatus() {
        return returnOrderStatus;
    }

    public void setReturnOrderStatus(ReturnOrderStatus returnOrderStatus) {
        this.returnOrderStatus = returnOrderStatus;
    }
}
