package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * 
 * @author RHC
 * 
 */
@Entity
@Table(name = "ACCXCREDXUSER_CCENTER")
@Audited
public class AssignedCostCenter implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 1L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    //note: it is assumed (in AssignedCostCenterBp) that this field is eagerly fetched
    @ManyToOne(fetch = FetchType.EAGER, optional = false, cascade = { CascadeType.PERSIST, CascadeType.MERGE })
    @JoinColumn(name = "ALLOWEDCOSTCENTERS_ID")
    private CostCenter costCenter;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ACCOUNTCREDENTIALUSERS_ID")
    private AccountXCredentialXUser axcxu;

    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private BigDecimal totalLimit;

    @Column
    private BigDecimal currentCharge;

    @Column
    private BigDecimal pendingCharge;

    @Column(nullable = false)
    private boolean active = true;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    public CostCenter getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(CostCenter costCenter) {
        this.costCenter = costCenter;
    }

    public AccountXCredentialXUser getAxcxu() {
        return axcxu;
    }

    public void setAxcxu(AccountXCredentialXUser axcxu) {
        this.axcxu = axcxu;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public BigDecimal getTotalLimit() {
        return this.totalLimit;
    }

    public void setTotalLimit(final BigDecimal totalLimit) {
        this.totalLimit = totalLimit;
    }

    public String getTotalLimitString() {
        if (this.totalLimit == null) {
            return "";
        }
        return this.totalLimit.toPlainString();
    }

    public void setTotalLimitString(String totalLimit) {
        if (StringUtils.isNotBlank(totalLimit)) {
            this.totalLimit = new BigDecimal(totalLimit);
        }
        else {
            this.totalLimit = null;
        }
    }

    public BigDecimal getCurrentCharge() {
        return this.currentCharge;
    }

    public void setCurrentCharge(final BigDecimal currentCharge) {
        this.currentCharge = currentCharge;
    }

    public String getCurrentChargeString() {
        if (this.currentCharge == null) {
            return "";
        }
        return this.currentCharge.toPlainString();
    }

    public void setCurrentChargeString(String currentCharge) {
        if (StringUtils.isNotBlank(currentCharge)) {
            this.currentCharge = new BigDecimal(currentCharge);
        }
        else {
            this.currentCharge = null;
        }
    }

    public BigDecimal getPendingCharge() {
        return this.pendingCharge;
    }

    public void setPendingCharge(final BigDecimal pendingCharge) {
        this.pendingCharge = pendingCharge;
    }

    public String getPendingChargeString() {
        if (this.pendingCharge == null) {
            return "";
        }
        return this.pendingCharge.toPlainString();
    }

    public void setPendingChargeString(String pendingCharge) {
        if (StringUtils.isNotBlank(pendingCharge)) {
            this.pendingCharge = new BigDecimal(pendingCharge);
        }
        else {
            this.pendingCharge = null;
        }
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((AssignedCostCenter) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName();
        return result;
    }

    public static class AssignedCostCenterComparator implements Comparator<AssignedCostCenter> {

        @Override
        public int compare(AssignedCostCenter arg0, AssignedCostCenter arg1) {
            if (StringUtils.isNotBlank(arg0.getCostCenter().getName())
                    && StringUtils.isNotBlank(arg1.getCostCenter().getName())) {
                return arg0.getCostCenter().getName().compareToIgnoreCase(arg1.getCostCenter().getName());
            }
            return (new GenericComparator()).compare(arg0, arg1);
        }
    }
}