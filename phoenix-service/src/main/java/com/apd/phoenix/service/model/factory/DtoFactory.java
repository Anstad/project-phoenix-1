package com.apd.phoenix.service.model.factory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CanceledQuantity;
import com.apd.phoenix.service.model.CashoutPage;
import com.apd.phoenix.service.model.CashoutPageXField;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemClassification;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.Pick;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.PoNumberType;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.ShippingPartner;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.VendorInvoice;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CXMLConfigurationDto;
import com.apd.phoenix.service.model.dto.CXMLCredentialDto;
import com.apd.phoenix.service.model.dto.CashoutPageDto;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.CatalogDto;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.CredentialTermsDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.DtoUtils;
import com.apd.phoenix.service.model.dto.ItemClassificationDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.ItemImageDto;
import com.apd.phoenix.service.model.dto.LimitApprovalDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentXTaxTypeDto;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.OrderStatusDto;
import com.apd.phoenix.service.model.dto.OrderTypeDto;
import com.apd.phoenix.service.model.dto.PaymentInformationDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PoNumberTypeDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.SystemUserDto;
import com.apd.phoenix.service.model.dto.TermsOfSaleDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.dto.VendorDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;

/**
 * Factory class for creating Dto objects from entities
 * @author RHC
 *
 */
public class DtoFactory {

    private static final String ORDER_TYPE_MANUAL = "MANUAL";

    private static final Logger logger = LoggerFactory.getLogger(DtoFactory.class);

    public static PickDto createPickDto(Pick pick) {
        PickDto pickDto = new PickDto();
        pickDto.setApdOrderNumber(pick.getApdOrderNumber());
        pickDto.setCustomerPoNumber(pick.getCustomerPoNumber());
        pickDto.setInventoryId(pick.getInventoryId());
        pickDto.setLineItem(DtoFactory.createLineItemDto(pick.getLineItem()));
        pickDto.setLineReference(pick.getLineReference());
        pickDto.setOrderNumber(pick.getOrderNumber());
        pickDto.setQtyBo(pick.getQtyBo());
        pickDto.setQtyToPick(pick.getQtyToPick());
        pickDto.setShipped(pick.isShipped());
        pickDto.setShipperId(pick.getShipperId());
        return pickDto;
    }

    public static AccountDto createMarquetteStreamlinedAccountDto(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setSolomonCustomerID(account.getSolomonCustomerId());
        accountDto.setApdAssignedAccountId(account.getApdAssignedAccountId());
        accountDto.setCreationDate(account.getCreationDate());
        accountDto.setName(account.getName());
        return accountDto;
    }

    public static AccountDto createAccountDto(Account account) {
        AccountDto accountDto = new AccountDto();
        ArrayList<AddressDto> addresses = new ArrayList<>();
        for(Address address:account.getAddresses()){
            addresses.add(createAddressDto(address));
        }
        for (AccountXAccountPropertyType axapt : account.getProperties()) {
            if (axapt.getType() != null && axapt.getType().getName() != null) {
                accountDto.getProperties().put(axapt.getType().getName(), axapt.getValue());
            }
        }
        accountDto.setAddresses(addresses);
        accountDto.setActive(account.getIsActive());
        accountDto.setApdAssignedAccountId(account.getApdAssignedAccountId());
        accountDto.setCreationDate(account.getCreationDate());
        accountDto.setDatabaseId(account.getId().toString());
        accountDto.setSolomonCustomerID(account.getSolomonCustomerId());
        accountDto.setName(account.getName());
      //TODO:  accountDto.setTerms(account.get);
        if(account.getCxmlConfiguration()!=null){
            accountDto.setcXMLConfigurationDto(createCXMLConfigurationDto(account.getCxmlConfiguration()));
        }

        return accountDto;
    }

    public static AddressDto createAddressDto(Address address) {
        if (address == null) {
            return null;
        }
        AddressDto addressDto = new AddressDto();
        addressDto.setCountry(address.getCountry());
        addressDto.setCompanyName(address.getCompany());
        addressDto.setName(address.getName());
        addressDto.setCounty(address.getCounty());
        addressDto.setCity(address.getCity());
        addressDto.setGeoCode(address.getGeoCode());
        addressDto.setLine1(address.getLine1());
        addressDto.setLine2(address.getLine2());
        addressDto.setPendingShipToAuthorization(address.getPendingShipToAuthorization());

        addressDto.setState(address.getState());
        addressDto.setZip(address.getZip());
        try {
            addressDto.setMiscShipToDto(createMiscShipToDto(address.getMiscShipTo()));
        }
        catch (Exception e) {
            logger.error("Caught exception while trying to create the miscShipTo dto:" + e.toString());
        }
        String shipToId = address.getShipToId();
        if (StringUtils.isBlank(shipToId) && address.getMiscShipTo() != null) {
            shipToId = address.getMiscShipTo().getAddressId();
            if (StringUtils.isBlank(shipToId)) {
                shipToId = address.getMiscShipTo().getAddressId36();
            }
        }
        addressDto.setShipToId(shipToId);
        if (addressDto.getMiscShipToDto() == null) {
            addressDto.setMiscShipToDto(new MiscShipToDto());
        }
        addressDto.getMiscShipToDto().setAddressId(shipToId);

        return addressDto;
    }

    public static CredentialDto createCredentialDto(Credential credential) {
        CredentialDto credentialDto = new CredentialDto();
        if (credential != null) {
            credentialDto.setProperties(new HashMap<String, Serializable>());
            for (CredentialXCredentialPropertyType cxcpt : credential.getProperties()) {
                if (cxcpt.getType() != null && cxcpt.getType().getName() != null) {
                    credentialDto.getProperties().put(cxcpt.getType().getName(), cxcpt.getValue());
                }
            }
            credentialDto.setCashoutPageDto(createCashoutPageDto(credential.getCashoutPage()));
            credentialDto.setName(credential.getName());
            credentialDto.setTerms(createCredentialTermsDto(credential.getTerms()));
            if (credential.getOutgoingToCredentials() != null) {
                credentialDto.setOutgoingToCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential toCredential : credential.getOutgoingToCredentials()) {
                    credentialDto.getOutgoingToCredentials().add(createCXMLCredentialDto(toCredential));
                }
            }
            if (credential.getOutgoingFromCredentials() != null) {
                credentialDto.setOutgoingFromCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential fromCredential : credential.getOutgoingFromCredentials()) {
                    credentialDto.getOutgoingFromCredentials().add(createCXMLCredentialDto(fromCredential));
                }
            }
            if (credential.getOutgoingSenderCredentials() != null) {
                credentialDto.setOutgoingSenderCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential senderCredential : credential.getOutgoingSenderCredentials()) {
                    credentialDto.getOutgoingSenderCredentials().add(createCXMLCredentialDto(senderCredential));
                }
            }
            credentialDto.setOutgoingSharedSecret(credential.getOutgoingSenderSharedSecret());
            if (credential.getCustomerOutoingToCred() != null) {
                credentialDto.setCustomerOutgoingToCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential toCredential : credential.getCustomerOutoingToCred()) {
                    credentialDto.getCustomerOutgoingToCredentials().add(createCXMLCredentialDto(toCredential));
                }
            }
            if (credential.getCustomerOutoingFromCred() != null) {
                credentialDto.setCustomerOutgoingFromCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential fromCredential : credential.getCustomerOutoingFromCred()) {
                    credentialDto.getCustomerOutgoingFromCredentials().add(createCXMLCredentialDto(fromCredential));
                }
            }
            if (credential.getCustomerOutoingSenderCred() != null) {
                credentialDto.setCustomerOutgoingSenderCredentials(new HashSet<CXMLCredentialDto>());
                for (CxmlCredential senderCredential : credential.getCustomerOutoingSenderCred()) {
                    credentialDto.getCustomerOutgoingSenderCredentials().add(createCXMLCredentialDto(senderCredential));
                }
            }
            credentialDto.setCustomerOutgoingSharedSecret(credential.getCustomerOutgoingSharedSecret());
            credentialDto.setVendorPunchoutUrl(credential.getVendorPunchoutUrl());
            credentialDto.setVendorOrderUrl(credential.getVendorOrderUrl());
            credentialDto.setVendorName(credential.getName());
            if (credential.getDeploymentMode() != null) {
                credentialDto.setDeploymentMode(credential.getDeploymentMode().getLabel());
            }
            if (credential.getDefaultShipFromAddress() != null) {
                credentialDto.setDefaultShipFromAddress(createAddressDto(credential.getDefaultShipFromAddress()));
            }
        }
        return credentialDto;
    }

    public static CatalogXItemDto createCatalogXItemDto(CatalogXItem catalogXItem) {
        CatalogXItemDto dto = new CatalogXItemDto();
        dto.setAdded(catalogXItem.getAdded());
        dto.setDeleted(catalogXItem.getDeleted());
        dto.setModified(catalogXItem.getModified());
        if (catalogXItem.getCatalog() != null) {
            dto.setCatalog(createCatalogDto(catalogXItem.getCatalog()));
        }
        if (catalogXItem.getItem() != null) {
            dto.setItem(createFullItemDto(catalogXItem.getItem()));
        }
        dto.setPrice(catalogXItem.getPrice());
        return dto;
    }

    public static CredentialTermsDto createCredentialTermsDto(Credential.Terms terms) {
        CredentialTermsDto credentialTermsDto = new CredentialTermsDto();
        if (terms != null) {
            credentialTermsDto.setDaysUntilDue(terms.getDaysUntilDue());
            credentialTermsDto.setDaysWhileDiscounted(terms.getDaysWhileDiscounted());
            credentialTermsDto.setDiscountPercent(terms.getDiscountPercent());
            credentialTermsDto.setLabel(terms.getLabel());
        }
        return credentialTermsDto;
    }

    public static PurchaseOrderDto createMarquetteStreamlinedPurchaseOrderDto(CustomerOrder customerOrder){
    	PurchaseOrderDto customerOrderDto = new PurchaseOrderDto();
        customerOrderDto.setAccount(createMarquetteStreamlinedAccountDto(customerOrder.getAccount()));
        if(customerOrder.getShipDate() != null) {
        	customerOrderDto.setShipDate(customerOrder.getShipDate());
        }
        customerOrderDto.setApdPoNumber(customerOrder.getApdPo().getValue());
        customerOrderDto.setOrderDate(customerOrder.getOrderDate());
        customerOrderDto.setTerms(createCredentialTermsDto(customerOrder.getTerms()));
        List<PoNumberDto> poNumberList = new ArrayList<>();
        for(PoNumber poNumber:customerOrder.getPoNumbers()){
            poNumberList.add(createPoNumberDto(poNumber));
        }
        customerOrderDto.setPoNumbers(poNumberList);
        if(customerOrder.getTerms()!=null){
            customerOrderDto.setTermsOfSaleDto(createTermsOfSaleDto(customerOrder.getTerms()));
        }
        return customerOrderDto;
    }

    public static PurchaseOrderDto createPurchaseOrderDto(CustomerOrder customerOrder) throws DtoFactory.ParsingException {
        PurchaseOrderDto customerOrderDto = new PurchaseOrderDto();
        customerOrderDto.setAccount(createAccountDto(customerOrder.getAccount()));
        customerOrderDto.setShipToAddressDto(createAddressDto(customerOrder.getAddress()));
        customerOrderDto.setOrderType(createOrderTypeDto(customerOrder.getType()));
        if(customerOrder.getShipDate() != null) {
        	customerOrderDto.setShipDate(customerOrder.getShipDate());
        }
        
        if (customerOrder.getPaymentInformation() != null) {//A quote doesn't need to have a billing address
        	customerOrderDto.setPaymentInformationDto(createPaymentInformationDto(customerOrder.getPaymentInformation()));
        	Address billingAddress = customerOrder.getPaymentInformation().getBillingAddress();
        
        	if (billingAddress != null && 
        		!StringUtils.isEmpty(billingAddress.getLine1()) && 
        		!StringUtils.isEmpty(billingAddress.getZip()) &&
        		!StringUtils.isEmpty(billingAddress.getState()) &&
        		!StringUtils.isEmpty(billingAddress.getCity())) {
        		customerOrderDto.changeBillToAddressDto(createAddressDto(customerOrder.getPaymentInformation().getBillingAddress()));
        	} else {
        		customerOrderDto.changeBillToAddressDto(new AddressDto());
        	}
        } else {
        	customerOrderDto.changeBillToAddressDto(new AddressDto());
        }
        customerOrderDto.setApdPoNumber(customerOrder.getApdPo().getValue());
        customerOrderDto.setEstimatedShippingAmount(customerOrder.getEstimatedShippingAmount());
        customerOrderDto.setApprovalComment(customerOrder.getApprovalComment());
        customerOrderDto.setMaximumTaxToCharge(customerOrder.getMaximumTaxToCharge());
        customerOrderDto.setCredential(createCredentialDto(customerOrder.getCredential()));
        customerOrderDto.setWillCallEdiOverride(customerOrder.isWillCallEdiOverride());
        customerOrderDto.setWrapAndLabel(customerOrder.isWrapAndLabel());
        customerOrderDto.setAdot(customerOrder.isAdot());
        List<LineItemDto> lineItemList = new ArrayList<>();
        for(LineItem i:customerOrder.getItems()){
            lineItemList.add(createLineItemDto(i));
        }
        customerOrderDto.setItems(lineItemList);
        customerOrderDto.setOrderDate(customerOrder.getOrderDate());
        customerOrderDto.setCustomerRevisionNumber(String.valueOf(customerOrder.getCustomerRevisionNumber()));
        customerOrderDto.setCustomerRevisionDate(customerOrder.getCustomerRevisionDate());
        customerOrderDto.setOrderStatus(createOrderStatusDto(customerOrder.getStatus()));
        customerOrderDto.setOrderTotal(customerOrder.getOrderTotal());
        List<PoNumberDto> poNumberList = new ArrayList<>();
        for(PoNumber poNumber:customerOrder.getPoNumbers()){
            poNumberList.add(createPoNumberDto(poNumber));
        }
        customerOrderDto.setPoNumbers(poNumberList);
        
        customerOrderDto.setCancelDate(customerOrder.getCancelDate());
        customerOrderDto.setDeliveryDate(customerOrder.getDeliveryDate());
        customerOrderDto.setSystemUserDto(createSystemUserDto(customerOrder.getUser()));
        customerOrderDto.setPartnerId(customerOrder.getCredential().getPartnerId());
        customerOrderDto.setStatus(customerOrder.getStatus().getValue());
        customerOrderDto.setTerms(createCredentialTermsDto(customerOrder.getTerms()));
        if (customerOrder.getOperationAllowed() != null) {
        	customerOrderDto.setOperationAllowed(customerOrder.getOperationAllowed().name());
        }
        customerOrderDto.setLastOrderConfirmationId(customerOrder.findLastOrderConfirmationId());
        customerOrderDto.setOrderPayloadId(customerOrder.getOrderPayloadId());
        
        if(customerOrder.getTerms()!=null){
            customerOrderDto.setTermsOfSaleDto(createTermsOfSaleDto(customerOrder.getTerms()));
        }
        customerOrderDto.setOrderWeight(DtoUtils.calculateOrderWeight(customerOrderDto));

        return customerOrderDto;
    }

    private static OrderTypeDto createOrderTypeDto(OrderType type) {
        if (type != null) {
            OrderTypeDto orderTypeDto = new OrderTypeDto();
            orderTypeDto.setDescription(type.getDescription());
            orderTypeDto.setValue(type.getValue());
            return orderTypeDto;
        }
        return null;
    }

    public static VendorInvoiceDto createVendorInvoiceDto(VendorInvoice vendorInvoice)
            throws DtoFactory.ParsingException {
        VendorInvoiceDto invoiceDto = new VendorInvoiceDto();
        invoiceDto.setCreatedDate(vendorInvoice.getDate());
        invoiceDto.setInvoiceNumber(vendorInvoice.getInvoiceNumber());
        invoiceDto.setAmount(vendorInvoice.getAmount());
        if (vendorInvoice.getSpecialInstructions() != null) {
            invoiceDto.setSpecalInstructions(new ArrayList<String>());
            invoiceDto.getSpecalInstructions().addAll(vendorInvoice.getSpecialInstructions());
        }
        invoiceDto.setBillingTypeCode(vendorInvoice.getBillingTypeCode());
        invoiceDto.setActualItems(new ArrayList<LineItemDto>());
        for (LineItem item : vendorInvoice.getActualItems()) {
            invoiceDto.getActualItems().add(createLineItemDto(item));
        }
        invoiceDto.setCustomerOrderDto(createPurchaseOrderDto(vendorInvoice.getCustomerOrder()));

        return invoiceDto;
    }

    public static CustomerInvoiceDto createCustomerInvoiceDto(CustomerInvoice customerInvoice)
            throws DtoFactory.ParsingException {
        CustomerInvoiceDto customerInvoiceDto = new CustomerInvoiceDto();
        customerInvoiceDto.setCreatedDate(customerInvoice.getDate());
        customerInvoiceDto.setInvoiceNumber(customerInvoice.getInvoiceNumber());
        customerInvoiceDto.setAmount(customerInvoice.getAmount());
        if (customerInvoice.getSpecialInstructions() != null) {
            customerInvoiceDto.setSpecalInstructions(new ArrayList<String>());
            customerInvoiceDto.getSpecalInstructions().addAll(customerInvoice.getSpecialInstructions());
        }
        customerInvoiceDto.setBillingTypeCode(customerInvoice.getBillingTypeCode());
        ShipmentDto shipmentDto = DtoFactory.createShipmentDto(customerInvoice.getShipment());
        customerInvoiceDto.setShipment(shipmentDto);
        customerInvoiceDto.setApdPo(customerInvoice.getShipment().getOrder().getApdPo().getValue());
        return customerInvoiceDto;
    }

    public static CustomerInvoiceDto createMarquetteStreamlinedCustInvoiceDto(CustomerInvoice customerInvoice) {
        CustomerInvoiceDto customerInvoiceDto = new CustomerInvoiceDto();
        customerInvoiceDto.setDatabaseId(customerInvoice.getId());
        customerInvoiceDto.setCreatedDate(customerInvoice.getDate());
        customerInvoiceDto.setInvoiceNumber(customerInvoice.getInvoiceNumber());
        customerInvoiceDto.setAmount(customerInvoice.getAmount());
        ShipmentDto shipment = new ShipmentDto();
        shipment.setCustomerOrderDto(createMarquetteStreamlinedPurchaseOrderDto(customerInvoice.getShipment()
                .getOrder()));
        customerInvoiceDto.setShipment(shipment);
        customerInvoiceDto.setApdPo(customerInvoice.getShipment().getOrder().getApdPo().getValue());
        return customerInvoiceDto;
    }

    public static ItemDto createFullItemDto(Item item) {
        ItemDto itemDto = new ItemDto();
        itemDto.setProperties(new HashMap<String, String>());
        for (ItemXItemPropertyType property : item.getPropertiesReadOnly()) {
            itemDto.getProperties().put(property.getType().getName(), property.getValue());
        }
        itemDto.setClassifications(item.getClassificationStringMap());
        itemDto.setCommodityCode(item.getCommodityCode());
        itemDto.setDescription(item.getDescription());
        itemDto.setItemImages(new HashSet<ItemImageDto>());
        for (ItemImage image : item.getItemImages()) {
            itemDto.getItemImages().add(createItemImage(image));
        }
        if (item.getUnitOfMeasure() != null) {
            itemDto.setUnitOfMeasure(createUnitOfMeasureDto(item.getUnitOfMeasure()));
        }
        itemDto.setSkus(new ArrayList<SkuDto>());
        if (item.getSkus() != null) {
            for (Sku sku : item.getSkus()) {
                SkuDto skuDto = createSkuDto(sku);
                itemDto.getSkus().add(skuDto);
            }
        }
        itemDto.setName(item.getName());
        itemDto.setItemWeight(item.getItemWeight());
        return itemDto;
    }

    public static ItemDto createItemDto(Item item) {
        ItemDto itemDto = new ItemDto();
        itemDto.setItemClassifications(new HashSet<ItemClassificationDto>());
        for (ItemClassification itemClassification : item.getItemClassifications()) {
            itemDto.getItemClassifications().add(createItemClassificationDto(itemClassification));
        }
        for (Sku sku : item.getSkus()) {
            itemDto.getSkus().add(createSkuDto(sku));
        }
        itemDto.setSpecifications(new HashMap<String, String>());
        for (String specificationKey : item.getSpecifications().keySet()) {
            itemDto.getSpecifications().put(specificationKey, item.getSpecifications().get(specificationKey));
        }
        itemDto.setItemWeight(item.getItemWeight());
        return itemDto;
    }

    public static ItemClassificationDto createItemClassificationDto(ItemClassification itemClassification) {
        ItemClassificationDto itemClassificationDto = new ItemClassificationDto();
        itemClassificationDto.setType(itemClassification.getItemClassificationType().getName());
        return itemClassificationDto;
    }

    public static LineItemDto createLineItemDto(LineItem lineItem, Item item) {
        LineItemDto lineItemDto = new LineItemDto();
        if (lineItem.getApdSku() != null) {
            lineItemDto.setApdSku(lineItem.getApdSku());
        }
        lineItemDto.setCore(lineItem.isCore());
        lineItemDto.setCost(lineItem.getCost());

        lineItemDto.setCustomerSku(createSkuDto(lineItem.getCustomerSku()));
        if (lineItem.getCustomerSku() != null) {
            lineItemDto.setBuyerPartNumber(lineItem.getCustomerSku().getValue());
        }
        lineItemDto.setBackorderedQuantity(lineItem.getBackorderedQuantity());
        lineItemDto.setDescription(lineItem.getDescription());
        lineItemDto.setEstimatedShippingAmount(lineItem.getEstimatedShippingAmount());
        lineItemDto.setMaximumTaxToCharge(lineItem.getMaximumTaxToCharge());
        //Due to Lazy Init problems in static factory methods, we have to pass in a separate item
        if (item == null) {
            //Case where no item specified
            if (lineItem.getItem() != null) {
                lineItemDto.setItem(createItemDto(lineItem.getItem()));
            }
        }
        else {
            lineItemDto.setItem(createItemDto(item));
        }
        lineItemDto.setLeadTime(lineItem.getLeadTime());
        lineItemDto.setLineNumber(lineItem.getLineNumber());
        lineItemDto.setManufacturerName(lineItem.getManufacturerName());
        lineItemDto.setManufacturerPartId(lineItem.getManufacturerPartId());
        lineItemDto.setParentLineNumber(lineItem.getParentLineNumber());
        lineItemDto.setPurchaseComment(lineItem.getPurchaseComment());
        lineItemDto.setQuantity(new BigInteger(String.valueOf(lineItem.getQuantity())));
        lineItemDto.setOriginalQuantity(lineItem.getOriginalQuantity());

        BigInteger invoiced = BigInteger.ZERO;
        BigInteger shipped = BigInteger.ZERO;
        BigInteger cancelled = BigInteger.ZERO;
        for (LineItemXShipment lineItemXShipment : lineItem.getItemXShipments()) {
            shipped = shipped.add(lineItemXShipment.getQuantity());
            if (lineItemXShipment.getPaid()) {
                invoiced = invoiced.add(lineItemXShipment.getQuantity());
            }
        }
        for (CanceledQuantity cancelledQuantity : lineItem.getCanceledQuantities()) {
            if (cancelledQuantity.getQuantity() != null) {
                cancelled = cancelled.add(cancelledQuantity.getQuantity());
            }
        }

        lineItemDto.setQuantityInvoicedToCustomer(invoiced);
        lineItemDto.setQuantityInvoicedByVendor(lineItem.getQuantityInvoicedByVendor());
        lineItemDto.setQuantityShipped(shipped);
        lineItemDto.setQuantityCancelled(cancelled);
        lineItemDto.setStatus(createLineItemStatusDto(lineItem.getStatus()));
        lineItemDto.setShortName(lineItem.getShortName());
        lineItemDto.setSupplierPartAuxiliaryId(lineItem.getSupplierPartAuxiliaryId());
        lineItemDto.setSupplierPartId(lineItem.getSupplierPartId());
        lineItemDto.setTaxable(lineItem.isTaxable());
        if (lineItem.getCustomerExpectedUnitOfMeasure() != null) {
            lineItemDto.setCustomerExpectedUoM(lineItem.getCustomerExpectedUnitOfMeasure().getName());
        }
        lineItemDto.setUnitOfMeasure(createUnitOfMeasureDto(lineItem.getUnitOfMeasure()));
        lineItemDto.setUnitPrice(lineItem.getUnitPrice());
        lineItemDto.setUnspscClassification(lineItem.getUnspscClassification());
        if (lineItem.getVendorComments() != null) {
            lineItemDto.setVendorComments(new ArrayList<String>());
            lineItemDto.getVendorComments().addAll(lineItem.getVendorComments());
        }
        lineItemDto.setCustomerLineNumber(lineItem.getCustomerLineNumber());
        lineItemDto.setFee(lineItem.isFee());
        lineItemDto.setExtraneousCustomerSku(lineItem.getExtraneousCustomerSku());
        lineItemDto.setUniversalProductCode(lineItem.getUniversalProductCode());
        lineItemDto.setGlobalTradeItemNumber(lineItem.getGlobalTradeItemNumber());
        lineItemDto.setGlobalTradeIdentificatonNumber(lineItem.getGlobalTradeIdentificatonNumber());
        if (lineItem.getVendor() != null) {
            lineItemDto.setVendorName(lineItem.getVendor().getName());
        }
        return lineItemDto;
    }

    public static LineItemDto createLineItemDto(LineItem lineItem) {
        return createLineItemDto(lineItem, null);
    }

    public static LineItemStatusDto createLineItemStatusDto(LineItemStatus lineItemStatus) {
        LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
        if (lineItemStatus != null) {
            lineItemStatusDto.setValue(lineItemStatus.getValue());
            lineItemStatusDto.setDescription(lineItemStatus.getDescription());
        }
        return lineItemStatusDto;
    }

    public static LineItemXShipmentDto createLineItemXShipmentDto(LineItemXShipment lineItemXShipment) {
        LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
        lineItemXShipmentDto.setQuantity(lineItemXShipment.getQuantity());
        lineItemXShipmentDto.setShipping(lineItemXShipment.getShipping());
        List<LineItemXShipmentXTaxTypeDto> taxTypes = lineItemXShipmentDto.getLineItemXShipmentXTaxTypeDtos();
        for (LineItemXShipmentXTaxType taxType : lineItemXShipment.getTaxes()) {
            taxTypes.add(createLineItemXShipmentXTaxTypeDto(taxType));
        }
        lineItemXShipmentDto.setLineItemXShipmentXTaxTypeDtos(taxTypes);
        if (lineItemXShipment.getLineItem() != null) {
            lineItemXShipmentDto.setLineItemDto(createLineItemDto(lineItemXShipment.getLineItem()));
        }

        return lineItemXShipmentDto;
    }

    public static ShipmentDto createShipmentDto(Shipment shipment) throws DtoFactory.ParsingException {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setCustomerOrderDto(createPurchaseOrderDto(shipment.getOrder()));
        shipmentDto.setTrackingNumber(shipment.getTrackingNumber());
        shipmentDto.setShipmentPrice(shipment.getTotalShippingPrice());
        shipmentDto.setTotalTaxPrice(shipment.getTotalTaxPrice());
        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
        if (shipment.getShipFromAddress() != null) {
            shipmentDto.setFromAddress(createAddressDto(shipment.getShipFromAddress()));
        }
        if (shipment.getCustomerServiceAddress() != null) {
            shipmentDto.setCustomerServiceAddress(createAddressDto(shipment.getCustomerServiceAddress()));
        }
        if (shipment.getItemXShipments() != null) {
            for (LineItemXShipment lixs : shipment.getItemXShipments()) {
                shipmentDto.getLineItemXShipments().add(createLineItemXShipmentDto(lixs));
            }
        }
        if (shipment.getShippingPartner() != null) {
            shipmentDto.setShippingPartnerDto(createShippingPartner(shipment.getShippingPartner()));
        }
        shipmentDto.setShipmentDatabaseKey(shipment.getId());
        return shipmentDto;
    }

    public static LineItemXShipmentXTaxTypeDto createLineItemXShipmentXTaxTypeDto(
            LineItemXShipmentXTaxType lineItemXShipmentXTaxType) {
        LineItemXShipmentXTaxTypeDto lineItemXShipmentXTaxTypeDto = new LineItemXShipmentXTaxTypeDto();
        lineItemXShipmentXTaxTypeDto.setValue(lineItemXShipmentXTaxType.getValue());

        return lineItemXShipmentXTaxTypeDto;
    }

    public static MessageDto createMessageDto(Message message) {
        MessageDto messageDto = new MessageDto();

        return messageDto;
    }

    public static MessageMetadataDto createMessageMetadataDto(MessageMetadata messageMetadata) {
        MessageMetadataDto messageMetadataDto = new MessageMetadataDto();

        return messageMetadataDto;
    }

    public static OrderStatusDto createOrderStatusDto(OrderStatus orderStatus) {
        OrderStatusDto orderStatusDto = new OrderStatusDto();
        orderStatusDto.setDescription(orderStatus.getDescription());
        orderStatusDto.setValue(orderStatus.getValue());
        return orderStatusDto;
    }

    public static PoNumberDto createPoNumberDto(PoNumber poNumber) {
        PoNumberDto poNumberDto = new PoNumberDto();
        poNumberDto.setType(poNumber.getType().getName());
        poNumberDto.setValue(poNumber.getValue());
        return poNumberDto;
    }

    @Deprecated
    public static PoNumberTypeDto createPoNumberTypeDto(PoNumberType poNumberType) {
        PoNumberTypeDto poNumberTypeDto = new PoNumberTypeDto();
        poNumberTypeDto.setName(poNumberType.getName());
        return poNumberTypeDto;
    }

    public static SkuDto createSkuDto(Sku sku) {
        SkuDto skuDto = null;
        if (sku != null) {
            skuDto = new SkuDto();
            skuDto.setType(createSkuTypeDto(sku.getType()));
            skuDto.setValue(sku.getValue());
        }
        return skuDto;
    }

    public static SkuTypeDto createSkuTypeDto(SkuType skuType) {
        SkuTypeDto skuTypeDto = new SkuTypeDto();
        skuTypeDto.setName(skuType.getName());
        return skuTypeDto;
    }

    public static UnitOfMeasureDto createUnitOfMeasureDto(UnitOfMeasure unitOfMeasure) {
        UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
        unitOfMeasureDto.setName(unitOfMeasure.getName());
        return unitOfMeasureDto;
    }

    private static MiscShipToDto createMiscShipToDto(MiscShipTo miscShipTo) throws IllegalAccessException,
            InvocationTargetException, NoSuchMethodException {
        MiscShipToDto miscShipToDto = new MiscShipToDto();

        Map<String, Object> properties = BeanUtils.describe(miscShipTo);
        for (Entry<String, Object> entry : properties.entrySet()) {
            Object value = entry.getValue();
            String key = entry.getKey();
            if (value != null && key != null && !key.equals("class")) {
                BeanUtils.setProperty(miscShipToDto, key, value);
            }
        }

        return miscShipToDto;
    }

    private static PaymentInformationDto createPaymentInformationDto(PaymentInformation paymentInformation) {
        PaymentInformationDto paymentInformationDto = new PaymentInformationDto();
        paymentInformationDto.setPaymentType(paymentInformation.getPaymentType().getName());
        if (paymentInformation.getCard() != null) {
            paymentInformationDto.setCardNumber(paymentInformation.getCard().getNumber());
            paymentInformationDto.setExpiration(paymentInformation.getCard().getExpiration());
            paymentInformationDto.setIdentity(paymentInformation.getCard().getIdentifier());
            paymentInformationDto.setNameOnCard(paymentInformation.getCard().getNameOnCard());
        }
        return paymentInformationDto;
    }

    public static ReturnOrderDto createReturnDto(ReturnOrder returnOrder) throws DtoFactory.ParsingException {
        ReturnOrderDto returnOrderDto = new ReturnOrderDto();
        returnOrderDto.setOrder(createPurchaseOrderDto(returnOrder.getOrder()));
        if (returnOrder.getRestockingFee() != null) {
            returnOrderDto.setRestockingFee(returnOrder.getRestockingFee().getUnitPrice());
        }
        returnOrderDto.setItems(new HashSet<LineItemXReturnDto>());
        try {
            for (LineItemXReturn lixr : returnOrder.getItems()) {
                returnOrderDto.getItems().add(createLineItemXReturnDto(lixr));
            }
        }
        catch (NullPointerException e) {
            throw new ParsingException("return order contains no items");
        }
        returnOrderDto.setRaNumber(returnOrder.getRaNumber());
        returnOrderDto.setStatus(returnOrder.getStatus().name());
        returnOrderDto.setCreatedDate(returnOrder.getCreatedDate());
        return returnOrderDto;
    }

    public static LineItemXReturnDto createLineItemXReturnDto(LineItemXReturn lixr) {
        LineItemXReturnDto lineItemXReturnDto = new LineItemXReturnDto();
        lineItemXReturnDto.setQuantity(lixr.getQuantity());
        lineItemXReturnDto.setLineItemDto(createLineItemDto(lixr.getLineItem()));
        return lineItemXReturnDto;
    }

    private static ItemImageDto createItemImage(ItemImage image) {
        ItemImageDto imageDto = new ItemImageDto();
        imageDto.setImageName(image.getImageName());
        imageDto.setImageUrl(image.getImageUrl());
        imageDto.setPrimary(image.getPrimary());
        return imageDto;
    }

    private static CashoutPageDto createCashoutPageDto(CashoutPage cashoutPage) {
        CashoutPageDto cashoutPageDto = new CashoutPageDto();
        cashoutPageDto.setCashoutPageXFields(new HashSet<CashoutPageXFieldDto>());
        for (CashoutPageXField xField : cashoutPage.getPageXFields()) {
            cashoutPageDto.getCashoutPageXFields().add(createCashoutPageXField(xField));
        }
        return cashoutPageDto;
    }

    private static CashoutPageXFieldDto createCashoutPageXField(CashoutPageXField cashoutPageXField) {
        final CashoutPageXFieldDto cashoutPageXFieldDto = new CashoutPageXFieldDto();
        if (cashoutPageXField.getField() != null) {
            cashoutPageXFieldDto.setField(cashoutPageXField.getField().getName());
        }
        if (cashoutPageXField.getMapping() != null) {
            cashoutPageXFieldDto.setMessageMapping(cashoutPageXField.getMapping().getName());
        }
        cashoutPageXFieldDto.setDefaultValue(cashoutPageXField.getDefaultValue());
        cashoutPageXFieldDto.setLabel(cashoutPageXField.getLabel());
        cashoutPageXFieldDto.setIncludeLabelInMapping(cashoutPageXField.getIncludeLabelInMapping());
        return cashoutPageXFieldDto;
    }

    public static CXMLConfigurationDto createCXMLConfigurationDto(CxmlConfiguration cXMLConfiguration) {
        CXMLConfigurationDto cXMLConfigurationDto = new CXMLConfigurationDto();
        for (CxmlCredential cxmlCredential : cXMLConfiguration.getFromCredentials()) {
            cXMLConfigurationDto.getFromCredentials().add(createCXMLCredentialDto(cxmlCredential));
        }
        cXMLConfigurationDto.setName(cXMLConfiguration.getName());
        for (CxmlCredential cxmlCredential : cXMLConfiguration.getToCredentials()) {
            cXMLConfigurationDto.getToCredentials().add(createCXMLCredentialDto(cxmlCredential));
        }
        cXMLConfigurationDto.setSenderCredentials(cXMLConfigurationDto.getSenderCredentials());
        cXMLConfigurationDto.setSenderSharedSecret(cXMLConfiguration.getSenderSharedSecret());
        return cXMLConfigurationDto;
    }

    public static CXMLCredentialDto createCXMLCredentialDto(CxmlCredential cxmlCredential) {
        CXMLCredentialDto cxmlCredentialDto = new CXMLCredentialDto();
        cxmlCredentialDto.setDomain(cxmlCredential.getDomain());
        cxmlCredentialDto.setIdentifier(cxmlCredential.getIdentifier());
        return cxmlCredentialDto;
    }

    private static ShippingPartnerDto createShippingPartner(ShippingPartner shippingPartner) {
        ShippingPartnerDto partner = new ShippingPartnerDto();
        partner.setDuns(shippingPartner.getDuns());
        partner.setName(shippingPartner.getName());
        partner.setScacCode(shippingPartner.getScacCode());
        return partner;
    }

    private static CatalogDto createCatalogDto(Catalog catalog) {
        CatalogDto dto = new CatalogDto();
        dto.setExpirationDate(catalog.getExpirationDate());
        dto.setStartDate(catalog.getStartDate());
        return dto;
    }

    private static TermsOfSaleDto createTermsOfSaleDto(Credential.Terms terms) {
        TermsOfSaleDto termsOfSaleDto = new TermsOfSaleDto();
        if (terms.getDiscountPercent() != null) {
            termsOfSaleDto.setTermsDiscountPercent(terms.getDiscountPercent().toString());
        }
        if (StringUtils.isNotBlank(terms.getLabel())) {
            termsOfSaleDto.setDescription(terms.getLabel());
        }
        termsOfSaleDto.setTermsDiscountDaysDue(terms.getDaysUntilDue());
        if (terms.getDaysWhileDiscounted() != null) {
            termsOfSaleDto.setTermsNetDays(terms.getDaysWhileDiscounted().toString());
        }
        return termsOfSaleDto;
    }

    public static class ParsingException extends Exception {

        private String reason;

        public ParsingException(String string) {
            this.reason = string;
        }

        public String toString() {
            return reason;
        }
    }

    public static VendorDto createVendorDto(Vendor vendor) {
        VendorDto dto = new VendorDto();
        dto.setName(vendor.getName());
        if (vendor.getAddresses() != null && !vendor.getAddresses().isEmpty()) {
            if (vendor.getAddresses().iterator().hasNext()) {
                dto.setAddress(createAddressDto(vendor.getAddresses().iterator().next()));
            }
        }
        return dto;
    }

    public static LimitApprovalDto createLimitApprovalDto(LimitApproval limitApproval) {
        LimitApprovalDto limitApprovalDto = new LimitApprovalDto();
        limitApprovalDto.setType(limitApproval.getType().getType());
        limitApprovalDto.setLimit(limitApproval.getLimit());
        if (limitApproval.getValue() != null) {
            limitApprovalDto.setValue(limitApproval.getValue().trim());
        }
        return limitApprovalDto;
    }

    public static void clone(Object source, Object target, String[] fieldsToIgnore) {
        if (source == null || target == null) {
            return;
        }
        for (Field sourceField : source.getClass().getDeclaredFields()) {
            boolean skip = false;
            for (String toIgnore : fieldsToIgnore) {
                if (toIgnore.equals(sourceField.getName())) {
                    skip = true;
                }
            }
            if (!skip) {
                for (Field targetField : target.getClass().getDeclaredFields()) {
                    if (targetField.getName().equals(sourceField.getName())) {
                        try {
                            sourceField.setAccessible(true);
                            targetField.setAccessible(true);
                            targetField.set(target, sourceField.get(source));
                        }
                        catch (Exception e) {
                            //do nothing
                        }
                    }
                }
            }
        }
    }

    public static CustomerCreditInvoiceDto createCustomerCreditInvoiceDto(CustomerCreditInvoice customerCreditInvoice)
            throws ParsingException {
        CustomerCreditInvoiceDto customerCreditInvoiceDto = new CustomerCreditInvoiceDto();
        customerCreditInvoiceDto.setActualItems(new ArrayList<LineItemDto>());
        for (LineItem lineItem : customerCreditInvoice.getActualItems()) {
            customerCreditInvoiceDto.getActualItems().add(DtoFactory.createLineItemDto(lineItem));
        }
        customerCreditInvoiceDto.setAmount(customerCreditInvoice.getAmount());
        customerCreditInvoiceDto.setBillingTypeCode(customerCreditInvoice.getBillingTypeCode());
        customerCreditInvoiceDto.setCreatedDate(customerCreditInvoice.getDate());
        customerCreditInvoiceDto.setInvoiceNumber(customerCreditInvoice.getInvoiceNumber());
        customerCreditInvoiceDto.setReturnOrderDto(DtoFactory.createReturnDto(customerCreditInvoice.getReturnOrder()));
        customerCreditInvoiceDto.setSpecalInstructions(new ArrayList<String>());
        if (customerCreditInvoice.getSpecialInstructions() != null) {
            customerCreditInvoiceDto.setSpecalInstructions(new ArrayList<String>());
            customerCreditInvoiceDto.getSpecalInstructions().addAll(customerCreditInvoice.getSpecialInstructions());
        }
        return customerCreditInvoiceDto;
    }

    public static CustomerCreditInvoiceDto createMaquetteStreamlinedCustomerCreditInvoiceDto(
            CustomerCreditInvoice customerCreditInvoice) throws ParsingException {
        CustomerCreditInvoiceDto customerCreditInvoiceDto = new CustomerCreditInvoiceDto();
        customerCreditInvoiceDto.setDatabaseId(customerCreditInvoice.getId());
        customerCreditInvoiceDto.setAmount(customerCreditInvoice.getAmount());
        customerCreditInvoiceDto.setBillingTypeCode(customerCreditInvoice.getBillingTypeCode());
        customerCreditInvoiceDto.setCreatedDate(customerCreditInvoice.getDate());
        customerCreditInvoiceDto.setInvoiceNumber(customerCreditInvoice.getInvoiceNumber());
        ReturnOrderDto returnOrderDto = new ReturnOrderDto();
        returnOrderDto.setOrder(createMarquetteStreamlinedPurchaseOrderDto(customerCreditInvoice.getReturnOrder()
                .getOrder()));
        customerCreditInvoiceDto.setReturnOrderDto(returnOrderDto);
        return customerCreditInvoiceDto;
    }

    public static SystemUserDto createSystemUserDto(SystemUser systemUser) {
        SystemUserDto systemUserDto = new SystemUserDto();
        systemUserDto.setLogin(systemUser.getLogin());
        return systemUserDto;
    }

    public static CustomerInvoiceDto createCustomerInvoiceDto(CustomerCreditInvoiceDto customerCreditInvoiceDto) {
        CustomerInvoiceDto customerInvoiceDto = new CustomerInvoiceDto();
        customerInvoiceDto.setShipment(createShipmentDto(customerCreditInvoiceDto.getReturnOrderDto()));
        customerInvoiceDto.setInvoiceNumber(customerCreditInvoiceDto.getReturnOrderDto().getRaNumber());
        customerInvoiceDto.setCreatedDate(customerCreditInvoiceDto.getReturnOrderDto().getCreatedDate());
        if (customerCreditInvoiceDto.getReturnOrderDto() != null
                && customerCreditInvoiceDto.getReturnOrderDto().getOrder() != null) {
            customerInvoiceDto.setPartnerId(customerCreditInvoiceDto.getReturnOrderDto().getOrder().getPartnerId());
        }
        //the amount of a credit invoice should be negative
        if (customerCreditInvoiceDto.getAmount() != null) {
            customerInvoiceDto.setAmount(customerCreditInvoiceDto.getAmount().abs().negate());
        }
        customerInvoiceDto.setBillingTypeCode(customerCreditInvoiceDto.getBillingTypeCode());
        customerInvoiceDto.setSpecalInstructions(customerCreditInvoiceDto.getSpecalInstructions());
        return customerInvoiceDto;
    }

    public static ShipmentDto createShipmentDto(ReturnOrderDto returnOrderDto) {
    	ShipmentDto shipmentDto = new ShipmentDto();
    	shipmentDto.setCustomerOrderDto(returnOrderDto.getOrder());
    	List<LineItemXShipmentDto> lineItemXShipmentDtos = new ArrayList<>();
    	for(LineItemXReturnDto lineItemXReturnDto : returnOrderDto.getItems()) {
    		LineItemXShipmentDto lineItemXShipmentDto = createLineItemXShipmentDto(lineItemXReturnDto);
    		lineItemXShipmentDtos.add(lineItemXShipmentDto);
    	}
    	
    	shipmentDto.setLineItemXShipments(lineItemXShipmentDtos);
    	return shipmentDto;
    }

    public static LineItemXShipmentDto createLineItemXShipmentDto(LineItemXReturnDto lineItemXReturnDto) {
        LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
        lineItemXShipmentDto.setLineItemDto(lineItemXReturnDto.getLineItemDto());
        lineItemXShipmentDto.setQuantity(lineItemXReturnDto.getQuantity());
        lineItemXShipmentDto.setShipping(lineItemXReturnDto.getLineItemDto().getEstimatedShippingAmount());
        if (!lineItemXReturnDto.getLineItemDto().getFee()) {
            lineItemXShipmentDto.getLineItemDto().setEstimatedShippingAmount(
                    lineItemXShipmentDto.getLineItemDto().getEstimatedShippingAmount().negate());
            lineItemXShipmentDto.getLineItemDto().setUnitPrice(
                    lineItemXShipmentDto.getLineItemDto().getUnitPrice().negate());
        }
        //TODO: Tax calculation
        return lineItemXShipmentDto;
    }

    public static ShipmentDto createBasicShipmentDto(CustomerOrder co, String shipNumber) {
        ShipmentDto shipmentDto = new ShipmentDto();
        try {
            shipmentDto.setCustomerOrderDto(DtoFactory.createPurchaseOrderDto(co));
        }
        catch (ParsingException e) {
            logger.error(e.getLocalizedMessage());
        }
        shipmentDto.setTrackingNumber(shipNumber);
        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
        return shipmentDto;
    }

}
