/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;

@Entity
@Audited
public class LimitApproval implements Serializable, com.apd.phoenix.service.model.Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    public enum Type {
        Order("order"), LineItem("lineItem"), CostCenter("costCenter"), SKU("sku");

        private final String type;

        private Type(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }

    }

    private BigDecimal limit;
    private String value;

    @Column(nullable = false)
    private Type type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AXCXU_ID")
    private AccountXCredentialXUser axcxu;

    public BigDecimal getLimit() {
        return limit;
    }

    public Type[] getTypes() {
        return Type.values();
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public AccountXCredentialXUser getAxcxu() {
        return axcxu;
    }

    public void setAxcxu(AccountXCredentialXUser axcxu) {
        this.axcxu = axcxu;
    }

}
