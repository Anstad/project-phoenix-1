/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

/**
 *
 * @author nreidelb
 */
public class ItemRelationshipDto {

    private static final long serialVersionUID = -7660250832469611853L;
    //this SKU can be a vendor or APD SKU
    private String sku;
    private String vendorName;
    private int position;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
