package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.AccountBRMSRecord;

/**
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class AccountBRMSRecordDao extends AbstractDao<AccountBRMSRecord> {

    @SuppressWarnings("unchecked")
    public List<Boolean> isActive(Long accountId) {
        String hql = "SELECT record.active FROM AccountBRMSRecord AS record JOIN record.account AS a WHERE a.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", accountId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getSessionId(Long accountId) {
        String hql = "SELECT record.sessionId FROM AccountBRMSRecord AS record JOIN record.account AS a WHERE a.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", accountId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getProcessId(Long accountId) {
        String hql = "SELECT record.processId FROM AccountBRMSRecord AS record JOIN record.account AS a WHERE a.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", accountId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<AccountBRMSRecord> getRecordByAccountId(Long accountId) {
        String hql = "SELECT record FROM AccountBRMSRecord AS record JOIN record.account AS a WHERE a.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", accountId);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<AccountBRMSRecord> getRecordBySessionId(Long sessionId) {
        String hql = "SELECT record FROM AccountBRMSRecord AS record WHERE record.sessionId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", sessionId);
        return query.getResultList();
    }

    public List<String> getSnapshot(long accountId) {
        String hql = "SELECT record.snapshot FROM AccountBRMSRecord AS record JOIN record.account AS a WHERE a.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", accountId);
        return query.getResultList();
    }

}
