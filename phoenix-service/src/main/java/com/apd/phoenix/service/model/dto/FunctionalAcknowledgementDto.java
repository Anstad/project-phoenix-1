package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class FunctionalAcknowledgementDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private EventTypeDto eventType;
    private AcknowledgementCode acknowledgementCode;
    private String transactionId;
    private String groupId;
    private String senderId;
    private String interchangeId;
    private String messageTransactionId;
    private String messageGroupId;
    private String messageSenderId;
    private List<NoteDto> noteDtos;

    public enum AcknowledgementCode {
        A(FuncationalAcknowledgementDtoConstants.ACCEPTED), E(
                FuncationalAcknowledgementDtoConstants.ACCEPTED_WITH_ERRORS), R(
                FuncationalAcknowledgementDtoConstants.REJECTED);

        private final String label;

        private AcknowledgementCode(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public EventTypeDto getEventType() {
        return eventType;
    }

    public void setEventType(EventTypeDto eventType) {
        this.eventType = eventType;
    }

    public AcknowledgementCode getAcknowledgementCode() {
        return acknowledgementCode;
    }

    public void setAcknowledgementCode(AcknowledgementCode acknowledgementCode) {
        this.acknowledgementCode = acknowledgementCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public List<NoteDto> getNoteDtos() {
        return noteDtos;
    }

    public void setNoteDtos(List<NoteDto> noteDtos) {
        this.noteDtos = noteDtos;
    }

    public String getMessageTransactionId() {
        return messageTransactionId;
    }

    public void setMessageTransactionId(String messageTransactionId) {
        this.messageTransactionId = messageTransactionId;
    }

    public String getMessageGroupId() {
        return messageGroupId;
    }

    public void setMessageGroupId(String messageGroupId) {
        this.messageGroupId = messageGroupId;
    }

    public String getMessageSenderId() {
        return messageSenderId;
    }

    public void setMessageSenderId(String messageSenderId) {
        this.messageSenderId = messageSenderId;
    }

    public String getInterchangeId() {
        return interchangeId;
    }

    public void setInterchangeId(String interchangeId) {
        this.interchangeId = interchangeId;
    }

}
