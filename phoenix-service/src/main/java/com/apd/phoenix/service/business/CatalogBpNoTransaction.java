package com.apd.phoenix.service.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import au.com.bytecode.opencsv.CSVReader;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.catalog.CatalogChange;
import com.apd.phoenix.service.catalog.CatalogCsvPurge;
import com.apd.phoenix.service.catalog.CatalogMessageEntryPoint;
import com.apd.phoenix.service.catalog.CatalogUploadPurge;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;

@Stateless
@LocalBean
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CatalogBpNoTransaction extends AbstractBp<Catalog> {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    @Inject
    private CredentialBp credentialBp;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private ItemBp itemBp;

    @Inject
    private CatalogBp catalogBp;

    @Inject
    CatalogMessageEntryPoint catalogMessageEntryPoint;

    @Inject
    CatalogUploadPurge catalogUploadPurge;

    @Inject
    CatalogCsvPurge catalogCsvPurge;

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogBpNoTransaction.class);

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CatalogDao dao) {
        this.dao = dao;
    }

    /**
     * Method called to parse the CSV of a catalog
     * 
     * @param catalog - the catalog whose CSV will be parsed
     * @param action - the action that will be taken
     * @param itemsToLinkList - a List of items that have replacements 
     * @param catXItemsToLinkList - a List of CatalogXItems that have substitutions
     * @param resultList - the result list
     * @param summary - an object that the results are stored in
     * @param persistChanges - a boolean indicating whether the changes should be persisted
     * @return
     * @throws IOException
     */
    private void parseCsv(Catalog catalog, SyncAction action,
            List<SyncItemResult> resultList, SyncItemResultSummary summary, boolean persistChanges)
            throws IOException {
    	String correlationId = UUID.randomUUID().toString();
    	LOGGER.info("Starting catalog upload {}", correlationId.toString());
        Catalog oldCatalog = catalog;
        ArrayList<CatalogChange> queuedChanges = new ArrayList<>();
        Properties catalogUploadProperties = PropertiesLoader.getAsProperties("catalog.upload.integration");
        int THRESHOLD = Integer.parseInt(catalogUploadProperties.getProperty("catalogUploadBatchSize"));
        //If we're replacing or creating a diff, a new catalog is created based off a previous one, so we 
        //copy the values of the previous catalog.
        if (action == SyncAction.REPLACE || action == SyncAction.CREATE_DIFF) {
            oldCatalog = this.eagerLoad(catalog.getParent());
            if (action == SyncAction.REPLACE && persistChanges) {
                oldCatalog.setExpirationDate(new Date());
                oldCatalog.setReplacement(catalog);
                catalog.setParent(null);
                oldCatalog.getChildren().remove(catalog);
            }
        }
        
        LOGGER.info("purging records for catalogid={}", catalog.getId());
        catalogUploadPurge.purgeOldRecords(catalog.getId());
        catalogCsvPurge.purgeOldRecords(catalog.getId());

        Long totalItems = 0L;
        try ( 
		        InputStream fileStream = catalogBp.getDiffCsv(catalog);
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
        ) {
        	while (csvReader.readNext() != null) {
        		totalItems++;
        	}
        	totalItems = totalItems-2;
        } catch (Exception e) {
        	LOGGER.error("Caught exception, aborting catalog upload!", e);
        	return;
        }
        
        LOGGER.info("Processing {} items", totalItems);
        
        try (
		        InputStream fileStream = catalogBp.getDiffCsv(catalog);
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
        	) {
        	csvReader.readNext(); //reads off the first line, which is just the action
	        String[] header = csvReader.readNext();
	        if (header != null) {
	            //Iterates through each line in the CSV, except the first (which is the header)
	            String[] line;
	            while ((line = csvReader.readNext()) != null) {

	            	// Construct a property map for easier access to item data using the 
	            	// CSV header row for keys and the current CSV row for values.
	            	Map<String, String> itemData = new HashMap<>();
	                for (int i = 0; i < header.length; i++) {
	                    if (i < line.length && !line[i].equals("")) {
	                        itemData.put(header[i], line[i]);
	                    }
	                }
	                //For a vendor catalog
	                if (catalog.getVendor() != null) {
	                	if (!itemData.containsKey(ItemBp.VENDOR_HEADER)) {
		                    itemData.put(ItemBp.VENDOR_HEADER, catalog.getVendor().getName());
		                }
	                	queuedChanges.add(new CatalogChange( itemData,  action,catalog.getId(), persistChanges, correlationId,totalItems, false));
		            	if (queuedChanges.size() > THRESHOLD) {
	                		catalogMessageEntryPoint.scheduleRequest(queuedChanges);
	                		queuedChanges = new ArrayList<>();
	                	}
	                }
	                else { //For a customer catalog
		                //Creates the new CatalogXItem
	                	queuedChanges.add(new CatalogChange( itemData,  action,catalog.getId(), oldCatalog.getId(), persistChanges, correlationId,totalItems, true));
		            	if (queuedChanges.size() > THRESHOLD) {
	                		catalogMessageEntryPoint.scheduleRequest(queuedChanges);
	                		queuedChanges = new ArrayList<>();
	                	}
	                }
	            }
	            catalogMessageEntryPoint.scheduleRequest(queuedChanges);
	        }
        }
    }

    /**
     * Executes any scheduled catalog upload actions. Called by the method in CatalogBpScheduler.
     */
    public void executeScheduledActions() {
    	Date today = new Date();
    	LOGGER.info("Starting Catalog Processing: " + today);
    	if (!catalogBp.isUploadScheduleExists()) {
        	catalogBp.setUploadSchedule(new ArrayList<String>());
        }
        List<String> newSchedule = new ArrayList<>();
        List<String> toProcessId = new ArrayList<>();
        try (
		        InputStream fileStream = catalogBp.getUploadSchedule();
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
        	) {
            String[] line;
            while ((line = csvReader.readNext()) != null) {
            	Date parseDate = new SimpleDateFormat(DATE_FORMAT).parse(line[1]);
            	if (DateUtils.isSameDay(parseDate, today)) {
            		toProcessId.add(line[0]);
            	}
            	else if (parseDate.after(today)) {
            		newSchedule.add(StringEscapeUtils.escapeCsv(line[0]) + "," + StringEscapeUtils.escapeCsv(line[1]));
            	}
            }
            catalogBp.setUploadSchedule(newSchedule);
        }
        catch (Exception e) {
        	LOGGER.error("An error occured:", e);
        }
        //TODO: parallelize
        for (String catalogId : toProcessId) {
        	try {
        		this.changeCatalog(this.findById(Long.parseLong(catalogId), Catalog.class), true, true);
        	}
        	catch (Exception e) {
        		LOGGER.error("An error occured:", e);;
        	}
        }
    	today = new Date();
    	LOGGER.info("Completed Catalog Processing: " + today);
    }

    /**
     * Sets a catalog to be updated at a certain date.
     * 
     * @param catalog
     * @param scheduledDate
     */
    public void setScheduledAction(Catalog catalog, Date scheduledDate) {
    	if (!catalogBp.isUploadScheduleExists()) {
        	catalogBp.setUploadSchedule(new ArrayList<String>());
        }
        List<String> rowsToWrite = new ArrayList<>();
        try (
		        InputStream fileStream = catalogBp.getUploadSchedule();
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
        	) {
            String[] line;
            while ((line = csvReader.readNext()) != null) {
            	if (Long.parseLong(line[0]) != catalog.getId()) {
            		rowsToWrite.add(StringEscapeUtils.escapeCsv(line[0]) + "," + StringEscapeUtils.escapeCsv(line[1]));
            	}
            }
        }
        catch (Exception e) {
        	LOGGER.error("An error occured:", e);
        }
        rowsToWrite.add(catalog.getId().toString() + "," + DateFormatUtils.format(scheduledDate, DATE_FORMAT));
        this.catalogBp.setUploadSchedule(rowsToWrite);
    }

    /**
     * Called by executeScheduledActions or dryRunReport. Passes in the Catalog to perform the action on. If deleteDiff 
     * is set the true, the diff CSV is deleted at the end of the process; if not (ie for dry run) the csv is kept.
     * 
     * @param catalog
     * @param persistChanges - boolean, indicating whether the changes should be persisted
     * @return the catalog
     */
    public void changeCatalog(Catalog catalog, boolean persistChanges, boolean calculateAllPricing) {
    	catalog.setLastProcessStart(new Date());
    	catalog = dao.update(catalog);
        catalog = this.eagerLoad(catalog);
		try (
		        InputStream fileStream = catalogBp.getDiffCsv(catalog);
		        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileStream));
		        CSVReader csvReader = new CSVReader(bufferedReader);
        	) {
			//gets the first line of the CSV, to get the action
	        String actionName = csvReader.readNext()[0];
	        for (SyncAction action : SyncAction.values()) {
	        	if (action.getHeader().equals(actionName)) {
			        //executes the action
			        executeActionOnCatalog(catalog, action, persistChanges);
			        break;
	        	}
	        }
    	} catch (IOException e) {
			//do nothing
		}	
    }

    /**
     * Takes an action and a catalog, and performs the action on the catalog. Called by changeCatalog, calls parseCsv.
     * 
     * @param catalog
     * @param action
     * @param persistChanges - boolean, indicating whether the changes should be persisted
     */
    private void executeActionOnCatalog(Catalog catalog, SyncAction action, boolean persistChanges) {
        try {
            List<SyncItemResult> resultList = new ArrayList<SyncItemResult>();
            SyncItemResultSummary summary = new SyncItemResultSummary();
            //parses the CSV file of the catalog, adding to the result lists and the summary
            catalog = this.eagerLoad(catalog);
            LOGGER.debug("***********STARTING PARSE OF CSV");
            this.parseCsv(catalog, action, resultList, summary, persistChanges);
            LOGGER.debug("***********ENDING PARSE OF CSV");
        }
        catch (IOException e) {
            LOGGER.error("An error occured:", e);
        }
    }

    /**
     * Method called after the CSV of a catalog was parsed
     * 
     * @param catalog - the catalog whose CSV was parsed
     * @param action - the action that was taken
     * @param itemsToLinkList - a List of items that have replacements 
     * @param catXItemsToLinkList - a List of CatalogXItems that have substitutions
     * @param resultList - the result list
     * @param summary - an object that the results are stored in
     * @param parent - the parent of the catalog
     * @param shouldWrite - a boolean indicating whether the changes should be persisted
     * @return
     */
    public Catalog postParseCsv(Catalog catalog, SyncAction action, String correlationId,
            SyncItemResultSummary summary, Catalog parent, boolean shouldWrite) {
        //generates the report
        LOGGER.info("*************SETTING REPORT CSV");
        catalogBp.setReportCsv(catalog, correlationId, summary);
        LOGGER.info("*************ENDING REPORT CSV");
        catalog = this.findById(catalog.getId(), Catalog.class);
        if (shouldWrite) {
            if (action.equals(SyncAction.REPLACE)) {
                LOGGER.info("*************REPLACING CATALOG");
                catalog = this.replaceCatalog(parent, catalog);
                LOGGER.info("*************REPLACED CATALOG");
            }
            catalog = this.eagerLoad(catalog);
            if (catalog.getVendor() != null) {
                LOGGER.info("*************SKU QUERY LIST");
                List<Object[]> relationList = catalogXItemBp.skuQueryResult(catalog.getVendor().getName());
                LOGGER.info("*************SKU QUERY LIST DONE");
                for (Object[] array : relationList) {
                    CatalogXItem toChange = (CatalogXItem) array[0];
                    Item newItem = (Item) array[1];
                    toChange.setItem(newItem);
                    catalogXItemBp.update(toChange);
                }
            }
        }
        catalog.setLastProcessEnd(new Date());
        this.update(catalog);
        return catalog;
    }

    /**
     * Helper method for changeCatalog. This method makes the changes necessary to replace the catalog 
     * "oldCatalog" with the "replacement" catalog.
     * 
     * @param oldCatalog - the catalog to be replaced
     * @param replacement - the replacement catalog
     */
    private Catalog replaceCatalog(Catalog oldCatalog, Catalog replacement) {
        oldCatalog = this.eagerLoad(oldCatalog);
        replacement = this.eagerLoad(replacement);
        replacement.getChildren().addAll(oldCatalog.getChildren());
        replacement.getChildren().remove(replacement);
        replacement = dao.update(replacement);
        oldCatalog.getChildren().removeAll(oldCatalog.getChildren());
        dao.update(oldCatalog);
        Credential searchCredential = new Credential();
        searchCredential.setCatalog(new Catalog());
        searchCredential.getCatalog().setId(oldCatalog.getId());
        List<Credential> results = credentialBp.searchByExactExample(searchCredential, 0, 0);
        for (Credential c : results) {
            c.setCatalog(replacement);
            credentialBp.update(c);
        }
        if (replacement.getVendor() != null) {
            int batch = 0;
            List<Item> vendorBatchList = itemBp.batchForStreetPrice(oldCatalog, batch);
            while (vendorBatchList.size() > 0) {
                //TODO: parallelize
                for (Item item : vendorBatchList) {
                    itemBp.discontinue(item);
                    item.setVendorCatalog(replacement);
                    itemBp.update(item);
                }
                batch++;
                vendorBatchList = itemBp.batchForStreetPrice(oldCatalog, batch);
            }
        }
        return replacement;
    }

    public void reindexScheduledCatalogs() {
        List<Catalog> catalogs = ((CatalogDao) this.dao).getCatalogsToReindex();
        for (Catalog catalog : catalogs) {
            this.catalogBp.reindexCatalog(catalog);
        }
    }

    public void regenerateScheduledCatalogCsvs() {
        List<Catalog> catalogs = ((CatalogDao) this.dao).getCatalogsToRegenerateCsv();
        for (Catalog catalog : catalogs) {
            this.catalogBp.setCatalogCsv(catalog);
        }
    }

    public void regenerateScheduledSmartOci() {
        LOGGER.info("Timer Checking for SmartOCI generation");
        List<Catalog> catalogs = ((CatalogDao) this.dao).getSmartOciToRegenerate();
        for (Catalog catalog : catalogs) {
            this.catalogBp.setSmartOci(catalog);
        }
    }
}
