package com.apd.phoenix.service.business;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;

import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.dto.VendorDto;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;

@Stateless
@LocalBean
public class UnitOfMeasureBp extends AbstractBp<UnitOfMeasure> {

    public enum UnitOfMeasureCode {

        EACH("EA"), BOX("BX"), PACK("PK"), DOZEN("DZN"), ROLL("RL"), COUNT("CT"), RM("RM");

        String value;

        private UnitOfMeasureCode(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }
    
    @Inject
    UnitOfMeasureDao unitOfMeasureDao;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(UnitOfMeasureDao dao) {
        this.dao = dao;
    }

    public UnitOfMeasure getByName(String name) {
        return unitOfMeasureDao.getByName(name);
    }

    public UnitOfMeasure getByCode(UnitOfMeasureCode uomCode) {
        return getByName(uomCode.getValue());
    }

    public UnitOfMeasure getDefaultLineItemUom() {
        return getByCode(UnitOfMeasureCode.EACH);
    }
    
    //FIXME Temporary solution for NGC/HII UOM conversion.  Long term, this should be implemented via decision tables.
    public UnitOfMeasureDto convertToCustomerUom(AccountDto account, UnitOfMeasureDto vendorUom) {
    	UnitOfMeasureDto customerUom = null;
		UnitOfMeasureConversion conversion = null;
		
    	if (vendorUom != null && account != null && account.getApdAssignedAccountId() != null) {
    		String accountId = StringUtils.upperCase(account.getApdAssignedAccountId());
    	
    		// Determine the appropriate conversion map to use
    		if (doesAccountRequireUomConversion(accountId)) {
    			conversion = UnitOfMeasureConversion.STAPLES_TO_NGCHII;
    		}
    	}
		customerUom = convertUom(vendorUom, conversion);
    	return customerUom;
    }

	public static boolean doesAccountRequireUomConversion(String accountId) {
		return StringUtils.isNotEmpty(accountId) && accountId.matches("^(NORTHROP|NG|ES|HUNTINGTON|HII)[\\s[A-Z]]*");
	}
    
    //FIXME Temporary solution for NGC/HII UOM conversion.  Long term, this should be implemented via decision tables.
    public UnitOfMeasureDto convertToVendorUom(VendorDto vendor, UnitOfMeasureDto customerUom) {
    	UnitOfMeasureDto vendorUom = null;
    	UnitOfMeasureConversion conversion = null;
    	
    	// Determine the appropriate conversion map to use
    	if (customerUom != null && vendor != null && StringUtils.equalsIgnoreCase(vendor.getName(), "STAPLES")) {
    		conversion = UnitOfMeasureConversion.NGCHII_TO_STAPLES;
    	}
    	
    	vendorUom = convertUom(customerUom, conversion);
    	return vendorUom;
    }
    
    /**
     * Applies a UnitOfMeasureConversion to the provided UnitOfMeasureDto and returns the resulting target UnitOfMeasureDto.
     * If the source UnitOfMeasureDto is null the source UnitOfMeasureDto.getName() is blank, the UnitOfMeasureConversion is null,
     * or the converted unit of measure code is blank, this method will return a UnitOfMeasure with the default uom code ("EA").  
     * 
     * @param fromUom
     * @param uomConversion
     * @return
     */
    public UnitOfMeasureDto convertUom(UnitOfMeasureDto fromUom, UnitOfMeasureConversion uomConversion) {
    	UnitOfMeasureDto toUom = new UnitOfMeasureDto();
    	String toUomCode = "";
    	if (uomConversion != null && fromUom != null && StringUtils.isNotBlank(fromUom.getName()) && uomConversionMap.containsKey(uomConversion)) {
    		toUomCode = uomConversionMap.get(uomConversion).get(fromUom.getName());
    	} 
    	if (StringUtils.isBlank(toUomCode) && fromUom != null) {
    		toUomCode = fromUom.getName();
    	}
    	if (StringUtils.isBlank(toUomCode)) {
    		toUomCode = UnitOfMeasureCode.EACH.getValue();
    	}
    	toUom.setName(toUomCode);
    	return toUom;
    }
    
    public enum UnitOfMeasureConversion {
    	ANSI_TO_EDIFACT, EDIFACT_TO_ANSI, STAPLES_TO_NGCHII, NGCHII_TO_STAPLES;
    }
    
    public static final Map<String,String> ansiToEdifactUomCodeMap = new HashMap<>();
    static {
    	ansiToEdifactUomCodeMap.put("DZ", "DZN");
    	ansiToEdifactUomCodeMap.put("GR", "GRO");
    	ansiToEdifactUomCodeMap.put("ST", "SET");
    }
    
    public static final Map<String,String> edifactToAnsiUomCodeMap = new HashMap<>();
    static {
    	edifactToAnsiUomCodeMap.put("DZN", "DZ");
    	edifactToAnsiUomCodeMap.put("GRO", "GR");
    	edifactToAnsiUomCodeMap.put("SET", "ST");
    }
    
    // Includes UOM code mappings from Staples codes to internal NGC codes
    public static final Map<String,String> toNgcHiiUomCodeMap = new HashMap<>();
    static {
    	toNgcHiiUomCodeMap.put("DZN", "DZ");
    	toNgcHiiUomCodeMap.put("GR", "GRO");
    	toNgcHiiUomCodeMap.put("ST", "SET");
    	toNgcHiiUomCodeMap.put("HU", "HC");
    }
    
    // Includes UOM code mappings from NGC/HII internal codes to Staples codes 
//    public static final Map<String,String> toStaplesUomCodeMap = new HashMap<>();
//    static {
//    	toStaplesUomCodeMap.put("DZ", "DZN");
//    	toStaplesUomCodeMap.put("GRO", "GR");
//    	toStaplesUomCodeMap.put("SET", "ST");
//    }
    
    public static final Map<UnitOfMeasureConversion,Map<String,String>> uomConversionMap = new HashMap<>();
    static {
    	uomConversionMap.put(UnitOfMeasureConversion.ANSI_TO_EDIFACT,ansiToEdifactUomCodeMap);
    	uomConversionMap.put(UnitOfMeasureConversion.EDIFACT_TO_ANSI, edifactToAnsiUomCodeMap);
    	uomConversionMap.put(UnitOfMeasureConversion.STAPLES_TO_NGCHII, toNgcHiiUomCodeMap);
    	//uomConversionMap.put(UnitOfMeasureConversion.NGCHII_TO_STAPLES, toStaplesUomCodeMap);
    }
}
