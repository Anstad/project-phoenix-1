package com.apd.phoenix.service.message.impl;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;
import javax.annotation.PostConstruct;

@Stateless
@LocalBean
public class XMLMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLMessageSender.class);

    @Resource(mappedName = "java:/activemq/jumptrack-manifest")
    private Queue jumptrackJumptrackManifestQueue;

    @Resource(mappedName = "java:/activemq/jumptrack-stop-notification")
    private Queue jumptrackJumptrackStopNotificationQueue;

    @PostConstruct
    public void setupQueues() {
        this.jumptrackStopNotificationQueue = jumptrackJumptrackStopNotificationQueue;
        this.jumptrackManifestQueue = jumptrackJumptrackManifestQueue;
    }

    @Override
    public javax.jms.Message createJumptrackStopNotificationRequestMessage(Session session,
            StopNotificationRequestDto stopNotificationRequestDto) {
        return createObjectMessage(session, stopNotificationRequestDto);
    }

    @Override
    public javax.jms.Message createJumptrackManifestMessage(Session session, ManifestRequestDto manifestRequestDto) {
        return createObjectMessage(session, manifestRequestDto);
    }

    private static javax.jms.Message createObjectMessage(Session session, Serializable object) {
        javax.jms.Message toReturn = null;
        try {
            toReturn = session.createObjectMessage(object);
        }
        catch (JMSException e) {
            LOGGER.error("Error creating javax.jms.Message from object", e);
        }
        return toReturn;
    }

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        // TODO Auto-generated method stub
        return false;
    }
}
