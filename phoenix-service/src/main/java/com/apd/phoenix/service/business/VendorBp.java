package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.VendorPropertyType;
import com.apd.phoenix.service.persistence.jpa.VendorDao;

@Stateless
@LocalBean
public class VendorBp extends AbstractBp<Vendor> {

    private static final Logger LOGGER = LoggerFactory.getLogger(VendorBp.class);

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(VendorDao dao) {
        this.dao = dao;
    }

    public List<VendorPropertyType> retrieveAllPropertyTypes() {
        LOGGER.info("\n\tVendorBp:retrieveAllPropertyTypes()\n");
        return ((VendorDao) dao).getAllPropertyTypes();
    }

    @Inject
    private ItemBp itemBp;

    /**
     * This method takes a vendor, and deactivates it. 
     * 
     * TODO: this method should be called by a chron job, when the expiration date of a vendor passes.
     * 
     * @param vendor
     */
    public void deactivate(Vendor vendor) {
        List<Item> catalogList = itemBp.getCurrentCatalogItems(vendor.getId());
        for (Item item : catalogList) {
            itemBp.discontinue(item);
        }
    }

    /**
     * This method takes a vendor, and reactivates it. 
     * 
     * TODO: this method should be called by a chron job, when the activation date of a vendor passes.
     * 
     * @param vendor
     */
    public void reactivate(Vendor vendor) {
        List<Item> catalogList = itemBp.getCurrentCatalogItems(vendor.getId());
        for (Item item : catalogList) {
            itemBp.unDiscontinue(item);
        }
    }

    public Vendor findByName(String vendorName) {
        if (StringUtils.isBlank(vendorName)) {
            return null;
        }
        Vendor searchVendor = new Vendor();
        searchVendor.setName(vendorName);
        List<Vendor> searchList = this.searchByExactExample(searchVendor, 0, 0);
        if (!searchList.isEmpty()) {
            return searchList.get(0);
        }
        return null;
    }
}
