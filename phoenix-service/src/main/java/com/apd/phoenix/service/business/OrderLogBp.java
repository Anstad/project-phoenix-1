package com.apd.phoenix.service.business;

import com.apd.phoenix.service.model.ChangeOrderLog;
import com.apd.phoenix.service.model.CustomerOrder;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.persistence.jpa.ChangeOrderLogDao;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.OrderLogDao;

@Stateless
@LocalBean
public class OrderLogBp extends AbstractBp<OrderLog> {

    @Inject
    OrderLogDao orderLogDao;

    @Inject
    ChangeOrderLogDao changeOrderLogDao;

    @Inject
    CustomerOrderDao customerOrderDao;

    @Inject
    public void initDao(OrderLogDao dao) {
        this.dao = dao;
    }

    public OrderLog logChangeOrder(CustomerOrder customerOrder, String description, String username) {
        ChangeOrderLog orderLog = changeOrderLogDao.create(new ChangeOrderLog());
        orderLog.setCustomerOrder(customerOrder);
        orderLog.setChangingUser(username);
        orderLog.setDescription(description);
        orderLog = changeOrderLogDao.update(orderLog);
        return orderLog;
    }

    public EventType getFromDto(String eventName) {
        if (StringUtils.isBlank(eventName)) {
            return null;
        }
        for (EventType type : EventType.values()) {
            if (type.getLabel() != null && type.getLabel().equals(eventName)) {
                return type;
            }
        }
        return null;
    }

    public EventTypeDto getDto(EventType type) {
        if (type == null) {
            return null;
        }
        for (EventTypeDto dto : EventTypeDto.values()) {
            if (dto.getLabel() != null && dto.getLabel().equals(type.getLabel())) {
                return dto;
            }
        }
        return null;
    }
}
