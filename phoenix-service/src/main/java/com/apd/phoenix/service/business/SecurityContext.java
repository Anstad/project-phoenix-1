package com.apd.phoenix.service.business;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.security.Principal;
import javax.annotation.Resource;
import javax.ejb.AccessTimeout;
import javax.ejb.LocalBean;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.Role;

/**
 * This EJB is used to determine whether the currently logged-in user has a permission.
 * 
 * TODO: support adding a credential, which is used to specify session-specific permissions. Currently only 
 * User-based permissions are used.
 * 
 * @author RHC
 *
 */
@Stateful
@LocalBean
@SessionScoped
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 30)
public class SecurityContext implements Serializable {

    private static final long serialVersionUID = 595763824129320981L;

    @Resource
    private SessionContext sessionContext;

    private Set<String> credentialPermissions;

    public void setCredential(Credential credentialUsed) {
        this.credentialPermissions = new HashSet<>();
        for (Permission permission : credentialUsed.getPermissions()) {
        	this.credentialPermissions.add(permission.getName());
        }
        for (Role role : credentialUsed.getRoles()) {
        	for (Permission permission : role.getPermissions()) {
        		this.credentialPermissions.add(permission.getName());
        	}
        }
    }

    public boolean hasPermission(String permission) {
        try {
            return sessionContext.isCallerInRole(permission) || this.credentialPermissions.contains(permission);
        }
        catch (NullPointerException e) {
            //SessionContext throws an NPE when isCallerInRole is 
            //called when not logged in, rather than returning false
            return false;
        }
    }

    public String getUserLogin() {
        return sessionContext.getCallerPrincipal().getName();
    }

    public Principal getCallerPrincipal() {
        return sessionContext.getCallerPrincipal();
    }
}
