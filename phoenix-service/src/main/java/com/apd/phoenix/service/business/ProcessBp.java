package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Process;
import com.apd.phoenix.service.persistence.jpa.ProcessDao;

/**
 * This class provides business process methods for Process.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ProcessBp extends AbstractBp<Process> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessBp.class);
    protected ProcessDao dao;

    /**
     * This method is injected with the correct Dao and 
     * calls the initAbstract method in the superclass.
     * 
     * @param dao
     */
    @Inject
    public void init(ProcessDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ProcessDao dao) {
        this.dao = dao;
    }

    public void retriveOrderInformation() {
        LOGGER.info("\t\n****************** Calling into the dao");
        Process process = new Process();
        process.setName("Process from rule");
        if (null == dao) {
            dao = new ProcessDao();
        }
        dao.create(process);
    }

    /**
     * @param customerOrder
     */
    public List<Process> findProcessByOrderByCredential(CustomerOrder customerOrder) {
        LOGGER.info("\t\n****************** findProcessByOrderByCredential");
        if (customerOrder != null) {
            return dao.findProcessByOrderByCredential(customerOrder);
        }
        else {
            return null;
        }
    }

    /**
     * @param customerOrder
     */
    public List<Process> findProcessByOrderByAccount(CustomerOrder customerOrder) {
        LOGGER.info("\t\n****************** findProcessByOrderByAccount");
        if (customerOrder != null) {
            return dao.findProcessByOrderByAccount(customerOrder);
        }
        else {
            return null;
        }
    }
}
