package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.ScrollableResults;
import org.hibernate.StatelessSession;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBp.SyncError;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationException;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationResult;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.CustomerCost;
import com.apd.phoenix.service.model.HierarchyNode;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.Item.ItemStatus;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.ItemHistory;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemRelationship;
import com.apd.phoenix.service.model.ItemSellingPoint;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.ItemRelationshipDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao;
import com.apd.phoenix.service.persistence.jpa.HierarchyNodeDao;
import com.apd.phoenix.service.persistence.jpa.ItemDao;
import com.apd.phoenix.service.persistence.jpa.SkuDao;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;

@Stateless
@LocalBean
public class ItemBp extends AbstractBp<Item> {

    private static final String ITEM_WEIGHT_SPECIFICATION_NAME = "Item Weight";
    private static final String LAST_APD_COST = "Last APD Cost";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String CATEGORY = "category";
    public static final String CLASSIFICATIONS = "classifications";
    public static final String HIERARCHY_HEADER = "hierarchy";
    public static final String MANUFACTURER = "manufacturer";
    public static final String SELLING_POINT = "selling points";
    public static final String MATCHBOOK = "matchbook";
    public static final String SEARCH_TERMS = "search terms";
    public static final String SKU_SUFFIX = " sku";
    public static final String SPECIFICATIONS = "specifications";
    public static final String UNIT_OF_MEASURE = "unit of measure";
    public static final String VENDOR_HEADER = "vendor";
    public static final String UNSPSC_CLASSIFICATIONTYPE = "unspsc";

    public static final String APD_SKUTYPE = "APD";
    public static final String VENDOR_SKUTYPE = "vendor";
    public static final String CUSTOMER_SKUTYPE = "customer";
    public static final String APD_COST_PROPERTYTYPE = "APDcost";
    public static final String STREET_PRICE_PROPERTYTYPE = "street";
    public static final String LIST_PRICE_PROPERTYTYPE = "list price";
    public static final String CUSTOMER_PRICE_PROPERTYTYPE = "unitPrice";
    public static final String LAST_PRICE_PROPERTYTYPE = "last calculated price";
    public static final String LAST_PROFIT_PROPERTYTYPE = "last calculated profit";
    public static final String CURRENT_PROFIT_PROPERTYTYPE = "current profit";
    public static final String PRICE_CHANGE_DATE_PROPERTYTYPE = "last price change date";
    public static final String CHANGED_SKU_PROPERTYTYPE = "changed sku number";
    public static final String CUSTOMER_REPLACEMENT_VENDOR_PROPERTYTYPE = "customerReplacementVendor";
    public static final String CUSTOMER_REPLACEMENT_SKU_PROPERTYTYPE = "customerReplacementSku";
    public static final String PREVENT_REPLACEMENT_PROPERTYTYPE = "prevent replacement";
    public static final String SPECIAL_ORDER_PROPERTYTYPE = "specialOrder";
    public static final String CUSTOM_ORDER_PROPERTYPYPE = "customOrder";
    public static final String FIXED_PRICE_PRICINGTYPE = "Fixed Price";

    public static final String IN_ROW_DELIMETER_SPLITTER = "\\|";
    public static final String IN_ROW_DELIMETER = "|";
    public static final String SUB_DELIMITER = "=";
    public static final String DELIMETER = ",";

    private static final String NO = "N";
    private static final String YES = "Y";
    private static final String BLANK = "";
    private static final String NULL = "null";
    private static final String TRUE = "true";
    private static final String FALSE = "false";
    private static final String ZERO_DOLLARS_AND_CENTS = "0.00";

    private static final String CHANGED_SKU_HISTORY_MESSAGE = "Changed APD sku to: ";
    private static final String REPLACED_HISTORY_MESSAGE = "Replaced by item ";
    private static final String REPLACEMENT_VENDOR = "replacementVendor";
    private static final String REPLACEMENT_SKU = "replacementSku";
    private static final String NEW_APD_SKU = "newApdSku";
    public static final String APD_SKU_HEADER = "APD sku";
    private static final String ETS_SKU = "ets_sku";
    private static final String MINIMUM = "minimum";
    private static final String MULTIPLES = "multiples";
    public static final String CATALOG = "catalog";
    private static final String IMAGE_URLS = "image URLs";
    private static final String DISCONTINUED_TYPE = "discontinued";
    private static final String HIERARCHY_SUFFIX = " hierarchy";
    private static final String COST_SUFFIX = " cost";
    public static final String ITEM_WEIGHT = "item weight";
    private static final String DISCONTINUED_DATE_TYPE = "discontinuedDate";

    private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

    public Item searchById(Long id) {
        return (Item) ((ItemDao) dao).findById(id, Item.class);
    }

    public enum ClassificationTypeEnum {
        ASSEMBLY_CODE("Assembly_Code"), EPACPGCOMPLIANT_CODE("EPACPGCompliant_Code"), GREEN_INDICATOR("Green_Indicator"), GREEN_INFORMATION(
                "Green_Information"), MSDS_Indicator("MSDS_Indicator"), NON_RETURNABLE_CODE("Non_Returnable_Code"), OVERWEIGHT_OVERSIZE_INDICATOR(
                "OverweightOversize_Indicator"), RECYCLE_INDICATOR("Recycle_Indicator"), UNSPSC("UNSPSC"), WBE_INDICATOR(
                "WBE_Indicator");

        private final String description;

        private ClassificationTypeEnum(String description) {
            this.description = description;
        }

        public String getDescription() {
            return this.description;
        }
    }

    @Inject
    private UnitOfMeasureDao uomDao;

    @Inject
    private SkuDao skuDao;

    @Inject
    private CustomerCostBp customerCostBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemDao dao) {
        this.dao = dao;
    }

    public void eagerLoadCustomerCosts(Set<CustomerCost> toLoad) {
        for (CustomerCost cost : toLoad) {
            customerCostBp.eagerLoad(cost);
        }
    }

    public void eagerLoadSkus(Set<Sku> toLoad) {
        for (Sku sku : toLoad) {
            skuDao.eagerLoad(sku);
        }
    }

    /**
     * Takes an item and an ordered list of property types to be stored, and returns the item as a row in a CSV file.
     * 
     * @param entry - the item to be stored in a CSV
     * @param propertyTypes - an ordered list of properties on the item.
     * @param skuTypes - an ordered list of sku types
     * @param customerList - an ordered list of customers
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String csvRow(Item entry, List<ItemPropertyType> propertyTypes, List<SkuType> skuTypes,
            List<String> customerList) {
        if (entry == null) {
            return BLANK;
        }
        Map<String, String> propertyValues = new HashMap<>();
        Map<String, Sku> skuValues = new HashMap<>();
        Map<String, String> customerCosts = new HashMap<>();
        for (ItemXItemPropertyType property : entry.getPropertiesReadOnly()) {
            propertyValues.put(property.getType().getName(), property.getValue());
        }
        for (Sku sku : entry.getSkus()) {
            skuValues.put(sku.getType().getName(), sku);
        }
        for (CustomerCost cost : entry.getCustomerCosts()) {
            customerCosts.put(cost.getCustomer().getName(), String.valueOf(cost.getValue()));
        }
        StringBuilder outputBuffer = new StringBuilder();
        //Stores the name of the Vendor
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getVendorCatalog().getVendor().getName()));
        outputBuffer.append(DELIMETER);
        //Stores the APD sku
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(APD_SKUTYPE).getValue()));
        outputBuffer.append(DELIMETER);
        //Stores hierarchy node of the item
        if (entry.getHierarchyNode() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getHierarchyNode().getPublicId().toString()));
        }
        outputBuffer.append(DELIMETER);
        //Stores item information
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getName()));
        outputBuffer.append(DELIMETER);
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getDescription()));
        outputBuffer.append(DELIMETER);
        if (entry.getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItemCategory().getName()));
        }
        outputBuffer.append(DELIMETER);
        if (entry.getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getUnitOfMeasure().getName()));
        }
        outputBuffer.append(DELIMETER);
        StringBuffer imageString = new StringBuffer();
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() != null && image.getPrimary()) {
        		imageString.append(image.getImageUrl());
        	}
        }
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() == null || !image.getPrimary()) {
        		if (imageString.length() != 0) {
        			imageString.append(IN_ROW_DELIMETER);
        		}
        		imageString.append(image.getImageUrl());
        	}
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(imageString.toString()));
        outputBuffer.append(DELIMETER);
        if (ItemStatus.DISCONTINUED.equals(entry.getStatus())) {
        	outputBuffer.append(StringEscapeUtils.escapeCsv(TRUE));
        }
        outputBuffer.append(DELIMETER);
        //Stores the APD sku of the replacement item
        if (entry.getReplacement() != null) {
        	for (Sku sku : entry.getReplacement().getSkus()) {
        		if (sku.getType().getName().equals(APD_SKUTYPE)) {
                    outputBuffer.append(StringEscapeUtils.escapeCsv(sku.getValue()));
        		}
        	}
        }
        outputBuffer.append(DELIMETER);
        if (entry.getManufacturer() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getManufacturer().getName()));
        }
        
        outputBuffer.append(DELIMETER);
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getSearchTerms()));
        
        outputBuffer.append(DELIMETER);
        if (entry.getMinimum() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getMinimum().toString()));
        }
        
        outputBuffer.append(DELIMETER);
        if (entry.getMultiple() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getMultiple().toString()));
        }
        
        outputBuffer.append(DELIMETER);
        if (entry.getLastCostChangeDate() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv("  "+entry.getLastCostChangeDate().toString()+ " "));
        }
        
        //Stores the values for different property types
        for (ItemPropertyType type : propertyTypes) {
            outputBuffer.append(DELIMETER);
            if (propertyValues.containsKey(type.getName())) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(propertyValues.get(type.getName())));
            }
        }
        outputBuffer.append(DELIMETER);
        StringBuilder classificationBuffer = new StringBuilder();
        Map<ItemClassificationType, String> classMap = entry.getClassifications();
        for (ItemClassificationType classType : classMap.keySet()) {
            if (classificationBuffer.length() != 0) {
                classificationBuffer.append(IN_ROW_DELIMETER);
            }
            classificationBuffer.append(classType.getName());
            classificationBuffer.append(SUB_DELIMITER);
            classificationBuffer.append(classMap.get(classType));
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(classificationBuffer.toString()));
        outputBuffer.append(DELIMETER);
        StringBuilder specificationBuffer = new StringBuilder();
        Map<String, String> specMap = entry.getSpecifications();
        for (String spec : specMap.keySet()) {
            if (specificationBuffer.length() != 0) {
                specificationBuffer.append(IN_ROW_DELIMETER);
            }
            specificationBuffer.append(spec);
            specificationBuffer.append(SUB_DELIMITER);
            specificationBuffer.append(specMap.get(spec));
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(specificationBuffer.toString()));
        //Stores the values for different SKU types
        for (SkuType type : skuTypes) {
            outputBuffer.append(DELIMETER);
            if (skuValues.containsKey(type.getName())) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(type.getName()).getValue()));
            }
        }
        //stores the different matchbooks
        outputBuffer.append(DELIMETER);
        StringBuilder matchbookBuffer = new StringBuilder();
        for (Matchbook matchbook : entry.getMatchbook()) {
        	if (matchbookBuffer.length() != 0) {
        		matchbookBuffer.append(IN_ROW_DELIMETER);
        	}
        	matchbookBuffer.append(matchbook.getModel());
        	matchbookBuffer.append(SUB_DELIMITER);
        	matchbookBuffer.append(matchbook.getManufacturer().getName());
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(matchbookBuffer.toString()));
        //Stores the costs for different customers
        for (String account : customerList) {
            outputBuffer.append(DELIMETER);
            if (customerCosts.containsKey(account)) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(customerCosts.get(account)));
            }
        }

        return outputBuffer.toString();
    }

    @Inject
    private ItemCategoryBp categoryBp;

    @Inject
    private ManufacturerBp manufacturerBp;

    @Inject
    private SkuTypeBp skuTypeBp;

    @Inject
    private HierarchyNodeDao hierarchyNodeDao;

    @Inject
    private ItemPropertyTypeBp propertyTypeBp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private PricingTypeBp pricingBp;

    @Inject
    private MatchbookBp matchbookBp;

    /**
     * Executes a synchronization operation on a catalog item.
     * @param itemData 
     * @param catalog 
     * @param action
     * @param summary
     * @param persistChanges - a boolean indicating whether the changes should be persisted
     * @return SyncItemResult wrapper object
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public SyncItemResult syncItem(Map<String,String> itemData, Catalog catalog, SyncAction action, SyncItemResultSummary summary, boolean persistChanges) {
    	//This class is a wrapper, storing the result as well as any warnings and errors that occur
        SyncItemResult toReturn = new SyncItemResult();
    	
    	Item result = this.getItemToSync(itemData, catalog, action, summary,
				persistChanges, toReturn);

        Map<String, Sku> skuMap = new HashMap<>();
        Map<String, CustomerCost> costMap = new HashMap<>();
        Map<String, ItemXItemPropertyType> propertyMap = new HashMap<>();
        //creates a map from the property type name, sku type name, or customer name to a property, sku, or customer
        for (Sku sku : result.getSkus()) {
            skuMap.put(sku.getType().getName(), sku);
        }
        for (CustomerCost cost : result.getCustomerCosts()) {
            costMap.put(cost.getCustomer().getName(), cost);
        }
        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
            propertyMap.put(property.getType().getName(), property);
        }
        //Iterates through each key
        for (String key : itemData.keySet()) {
            if (StringUtils.isBlank(itemData.get(key)) || key.equals(APD_SKU_HEADER) || key.equals(VENDOR_HEADER)) {
                //do nothing
            }
            else if (key.equals(NAME)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setName(null);
            		continue;
            	}
                result.setName(itemData.get(key));
            }
            else if (key.equals(DESCRIPTION)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setDescription(null);
            		continue;
            	}
                result.setDescription(itemData.get(key));
            }
            else if (key.equals(CATEGORY) && !SyncAction.VENDOR_UPDATE.equals(action)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setItemCategory(null);
            		continue;
            	}
                //Stores the category of the item
                ItemCategory searchCategory = new ItemCategory();
                searchCategory.setName(itemData.get(key));
                List<ItemCategory> searchList = categoryBp.searchByExactExample(searchCategory, 0, 1);
                if (!searchList.isEmpty()) {
                    result.setItemCategory(searchList.get(0));
                }
                else {
                    toReturn.getWarnings().add(CatalogBp.SyncError.CATEGORY_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
                }
            }
            else if (key.equals(CATALOG)) {
            	if (result.getVendorCatalog() == null) {
            		result.setVendorCatalog(catalog);
            	}
            	if (!itemData.get(key).equals(catalog.getName()) || !itemData.get(key).equals(result.getVendorCatalog().getName())) {
                    toReturn.getErrors().add(CatalogBp.SyncError.CATALOG_INCORRECTLY_SPECIFIED.getLabel());
                    summary.incrementErrors();
            	}
            }
            else if (key.equals(MANUFACTURER)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setManufacturer(null);
            		continue;
            	}
                //Stores the manufacturer of the item
                Manufacturer searchManufacturer = new Manufacturer();
                searchManufacturer.setName(itemData.get(key));
                List<Manufacturer> searchList = manufacturerBp.searchByExactExample(searchManufacturer, 0, 0);
                if (!searchList.isEmpty()) {
                    result.setManufacturer(searchList.get(0));
                }
                else {
                	Manufacturer newManufacturer = new Manufacturer();
                	newManufacturer.setName(itemData.get(key));
                	result.setManufacturer(manufacturerBp.create(newManufacturer));
                }
            }
            else if (key.equals(REPLACEMENT_VENDOR)) {
                //do nothing; handled in the "REPLACEMENT_SKU" case
            }
            else if (key.equals(REPLACEMENT_SKU)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setReplacement(null);
            		continue;
            	}
            	//setting replacement SKU of an item
                if (itemData.get(key) != null) {
                    try {
                    	String replacementSku = itemData.get(key);
                    	String replacementVendor = catalog.getVendor().getName();
                    	if (itemData.containsKey(REPLACEMENT_VENDOR) && StringUtils.isNotBlank(itemData.get(REPLACEMENT_VENDOR))) {
                    		replacementVendor = itemData.get(REPLACEMENT_VENDOR);
                    	}
                    	result.setReplacement(this.searchByExactExample(searchItem(replacementSku, replacementVendor), 0, 0).get(0));
                    }
                    catch (Exception e) {
                        toReturn.getWarnings().add(SyncError.ITEM_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                }
            }
            else if (key.equals(ETS_SKU)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setAbilityOneSubstitute(null);
            		continue;
            	}
            	//setting the ETS sku of an item (might be replaced by item relationship)
                if (itemData.get(key) != null) {
                    try {
                        this.setEtsItem(result.getSku(ItemBp.APD_SKUTYPE), itemData.get(key), catalog.getVendor().getName());
                    }
                    catch (ItemNotFoundException e) {
                        toReturn.getWarnings().add(SyncError.ITEM_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                }
            }
            else if (key.endsWith(COST_SUFFIX)) {
                //finds the customer specified, then sets the customer cost for that customer
                String accountName = key.substring(0, key.length() - COST_SUFFIX.length());
                //If the cost exists, sets it to the new value
                if (costMap.containsKey(accountName)) {
                    try {
                    	if (itemData.get(key).equals(NULL)) {
                    		result.getCustomerCosts().remove(costMap.get(accountName));
                    		continue;
                    	}
                    	costMap.get(accountName).setValue(new BigDecimal(itemData.get(key)));
                    }
                    catch (Exception e) {
                        toReturn.getWarnings().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
                        summary.incrementWarnings();
                    }
                }
                else if (!itemData.get(key).equals(NULL)) { //otherwise, creates a new CustomerCost object
                    Account searchAccount = new Account();
                    searchAccount.setName(key.substring(0, key.length() - COST_SUFFIX.length()));
                    List<Account> accountResults = accountBp.searchByExactExample(searchAccount, 0, 0);
                    if (accountResults.isEmpty()) {
                        toReturn.getWarnings().add(CatalogBp.SyncError.ACCOUNT_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                    else {
                        CustomerCost newCost = new CustomerCost();
                        newCost.setCustomer(accountResults.get(0));
                        newCost.setItem(result);
                        try {
                            newCost.setValue(new BigDecimal(itemData.get(key)));
                            result.getCustomerCosts().add(newCost);
                        }
                        catch (Exception e) {
                            toReturn.getWarnings().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel());
                            summary.incrementWarnings();
                        }
                    }
                }
            }
            else if (key.endsWith(SKU_SUFFIX)) {
                //Changes the value of the specified SKU
                String skuTypeName = key.substring(0, key.length() - SKU_SUFFIX.length());
                Sku currentSku;
                //if the sku exists already, changes the value
                if (skuMap.containsKey(skuTypeName)) {
                	if (itemData.get(key).equals(NULL)) {
                		result.getSkus().remove(skuMap.get(skuTypeName));
                		continue;
                	}
                    skuMap.get(skuTypeName).setValue(itemData.get(key));
                    currentSku = skuMap.get(skuTypeName);
                }
                else if (!itemData.get(key).equals(NULL)) { //otherwise, creates a new sku of that type 
                    SkuType skuTypeSearch = new SkuType();
                    skuTypeSearch.setName(skuTypeName);
                    List<SkuType> skuTypeList = skuTypeBp.searchByExactExample(skuTypeSearch, 0, 1);
                    if (skuTypeList.size() == 0) {
                        toReturn.getWarnings().add(CatalogBp.SyncError.SKU_TYPE_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                        continue;
                    }
                    currentSku = new Sku();
                    currentSku.setValue(itemData.get(key));
                    currentSku.setType(skuTypeList.get(0));
                    currentSku.setItem(result);
                    result.getSkus().add(currentSku);
                }
            }
            else if (key.endsWith(HIERARCHY_SUFFIX)) {
                //do nothing; handled in the "sku" case
            }
            else if (key.equals(SEARCH_TERMS)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setSearchTerms(null);
            		continue;
            	}
                //sets the item's search terms
        		//first adds all existing search terms
        		Set<String> terms = new HashSet<String>();
        		if (!StringUtils.isBlank(result.getSearchTerms())) {
	        		for (String s : result.getSearchTerms().split(IN_ROW_DELIMETER_SPLITTER)) {
	        			terms.add(s);
	        		}
        		}
        		//then adds the new search terms
        		for (String s : itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER)) {
        			terms.add(s);
        		}
        		//the builds the string, and stores it
        		StringBuilder termString = new StringBuilder();
        		for (String s : terms) {
        			if (termString.length() > 0) {
        				termString.append(IN_ROW_DELIMETER);
        			}
        			termString.append(s);
        		}
        		result.setSearchTerms(termString.toString());
            }
            else if (key.equals(SPECIFICATIONS)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setSpecifications(new HashMap<String, String>());
            		continue;
            	}
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	Map<String, String> specificationMap = result.getSpecifications();
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER);
            		String attributeKey = BLANK;
            		String attributeValue = BLANK;
            		if (equalsLocation > 0) {            			
            			attributeKey = attribute.substring(0, equalsLocation);
            			attributeValue = attribute.substring(equalsLocation + 1, attribute.length());
            			specificationMap.put(attributeKey, attributeValue);
            		}
        			
            	}
            	result.setSpecifications(specificationMap);
            }
            else if (key.equals(CLASSIFICATIONS)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setClassifications(new HashMap<ItemClassificationType, String>());
            		continue;
            	}
            	String[] attributes = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	Map<ItemClassificationType, String> classificationMap = result.getClassifications();
            	List<ItemClassificationType> itemClassTypes = classTypeBp.findAll(ItemClassificationType.class, 0, 0);
            	for (String attribute : attributes) {
            		int equalsLocation = attribute.indexOf(SUB_DELIMITER);
            		String attributeKey = BLANK;
            		String attributeValue = YES;
            		if (equalsLocation > 0) {            			
            			attributeKey = attribute.substring(0, equalsLocation);
            			attributeValue = attribute.substring(equalsLocation + 1, attribute.length());
            		} else {
            			continue;
            		}
            		boolean placedInMap = false;
        			for (ItemClassificationType classType : itemClassTypes) {
        				if (classType.getName().equals(attributeKey)) {
        					classificationMap.put(classType, attributeValue);
        					placedInMap = true;
        					if (classType.getName().toLowerCase().equals(UNSPSC_CLASSIFICATIONTYPE)) {
        						result.setCommodityCode(attributeValue);
        					}
        					break;
        				}
        			}
        			if (!placedInMap) {
        				toReturn.getWarnings().add(CatalogBp.SyncError.CLASSIFICATION_NOT_FOUND.getLabel());
        				summary.incrementWarnings();
        			}
            	}
            	result.setClassifications(classificationMap);
            }
            else if (key.equals(UNIT_OF_MEASURE)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setUnitOfMeasure(null);
            		continue;
            	}
                //sets the item's unit of measure
                UnitOfMeasure searchUom = new UnitOfMeasure();
                searchUom.setName(itemData.get(key));
                List<UnitOfMeasure> uomList = uomDao.searchByExactExample(searchUom, 0, 0);
                if (!uomList.isEmpty()) {
                    result.setUnitOfMeasure(uomList.get(0));
                }
                else {
                    toReturn.getWarnings().add(CatalogBp.SyncError.UOM_NOT_FOUND.getLabel());
                    summary.incrementErrors();
                }
            }
            else if (key.equals(HIERARCHY_HEADER)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.setHierarchyNode(null);
            		continue;
            	}
            	//sets the item's hierarchy node 
                try {                	
	                	List<HierarchyNode> nodeList = 
	                			hierarchyNodeDao.searchByPublicIdAndVersion(Long.valueOf(itemData.get(HIERARCHY_HEADER)), 2015);
	                if (!nodeList.isEmpty()) {
	                    result.setHierarchyNode(nodeList.get(0));
	                } else {
	                	 //otherwise, warning is displayed
	                    toReturn.getWarnings().add(CatalogBp.SyncError.HIERARCHY_NOT_FOUND.getLabel());
	                    summary.incrementWarnings();
	                }
                } catch (NumberFormatException e) {
                	//the value entered wasn't a number, do nothing
                	 toReturn.getWarnings().add(CatalogBp.SyncError.HIERARCHY_NOT_FOUND.getLabel());
	                    summary.incrementWarnings();
                }  
            }
            else if (key.equals(DISCONTINUED_TYPE)) {
                if (itemData.get(key).equalsIgnoreCase(TRUE) && !ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                    //If the item is being set to discontinued and either the propery map doesn't have the 
                    //"discontinued" property or the value of the property isn't true, discontinues the item.
                    summary.incrementDiscontinued();
                    if (persistChanges) {
                    	this.discontinue(result);
                    }
                }
                else if (itemData.get(key).equalsIgnoreCase(FALSE) && ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                    //If a discontinued item has the discontinued property set to false, undiscontinues
                	if (persistChanges) {
                		this.unDiscontinue(result);
                	}
                } else if (itemData.get(key).equalsIgnoreCase(TRUE) && ItemStatus.DISCONTINUED.equals(result.getStatus())) {
                	 //do nothing; If the item is being set to discontinued and item property have discontinued"
                }
                else {
                    toReturn.getWarnings().add(CatalogBp.SyncError.PROPERTY_VALUE_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
                }
            }
            else if (key.equals(IMAGE_URLS)) {
            	if (itemData.get(key).equals(NULL)) {
            		itemData.put(key, BLANK);
            	}
            	String[] imageUrls = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	result.getItemImages().removeAll(result.getItemImages());
            	for (String imageUrl : imageUrls) {
            		ItemImage image = new ItemImage();
            		image.setImageUrl(imageUrl);
            		image.setItem(result);
            		//the first URL in the list is set as "primary"
            		image.setPrimary(result.getItemImages().isEmpty());
            		result.getItemImages().add(image);
            	}
            }
            else if (key.equals(MATCHBOOK)) {
            	if (itemData.get(key).equals(NULL)) {
            		result.getMatchbook().removeAll(result.getMatchbook());
            		continue;
            	}
            	//creates link to matchbook items, creating the items if necessary
            	String[] matchbookProducts = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	boolean foundAllMatchbooks = true;
            	for (String product : matchbookProducts) {
            		Matchbook matchbook = matchbookBp.findFromArray(product.split(SUB_DELIMITER));
            		if (matchbook != null) {
            			result.getMatchbook().add(matchbook);
            		} else {
            			foundAllMatchbooks = false;
            		}
            	}
            	if (!foundAllMatchbooks) {
                    toReturn.getWarnings().add(CatalogBp.SyncError.MATCHBOOK_MANUFACTURER_NOT_FOUND.getLabel());
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(SELLING_POINT)) {
            	if (itemData.get(key).equals(NULL)) {
            		itemData.put(key, BLANK);
            	}
            	//creates selling points of the items
            	String[] sellingPoints = itemData.get(key).split(IN_ROW_DELIMETER_SPLITTER);
            	result.getSellingPoints().removeAll(result.getSellingPoints());
            	for (int i = 0; i < sellingPoints.length; i ++) {
            		ItemSellingPoint newSellingPoint = new ItemSellingPoint();
            		newSellingPoint.setValue(sellingPoints[i]);
            		newSellingPoint.setPosition(i + 1);
            		result.getSellingPoints().add(newSellingPoint);
            	}
            }
            else if (key.equals(MULTIPLES)) {
            	//sets the multiples that the item must be ordered in
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		result.setMultiple(null);
                		continue;
                	}
            		result.setMultiple(Integer.parseInt(itemData.get(key)));
            	}
            	catch (NumberFormatException e) {
                    toReturn.getWarnings().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(MINIMUM)) {
            	//sets the minimum of the item that must be ordered
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		result.setMinimum(null);
                		continue;
                	}
            		result.setMinimum(Integer.parseInt(itemData.get(key)));
            	}
            	catch (NumberFormatException e) {
                    toReturn.getWarnings().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementWarnings();
            	}
            }
            else if (key.equals(ITEM_WEIGHT)) {
            	try {
                	if (itemData.get(key).equals(NULL)) {
                		toReturn.getErrors().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel() + ": " + key);
                        summary.incrementErrors();                		
                	}                	
            		result.setItemWeight(new BigDecimal(itemData.get(key)));
            		Map<String, String> specificationMap = result.getSpecifications(); 
            		specificationMap.put(ITEM_WEIGHT_SPECIFICATION_NAME, itemData.get(key));
            		result.setSpecifications(specificationMap);
            	}
            	catch (NumberFormatException e) {
                    toReturn.getErrors().add(CatalogBp.SyncError.NUMBER_FORMAT.getLabel() + ": " + key);
                    summary.incrementErrors();
                    result.setItemWeight(new BigDecimal(0));
            	}
            }
            else {
                //This is reached for any header that doesn't match previous values, ie it's an item property.
                if (propertyMap.containsKey(key)) {
                	ItemXItemPropertyType toModify = propertyMap.get(key);
					if (itemData.get(key).equals(NULL)) {
                		removeProperty(result,toModify);
                		continue;
                	}
                    //If the property already exists, changes its value
					ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
                    newProperty.setValue(itemData.get(key));
                    newProperty.setType(toModify.getType());
                    result = addOrModifyItemProperty(result, newProperty);
                }
                else if (!itemData.get(key).equals(NULL)) {
                    //otherwise, creates a new itemxitempropertytype.
                    ItemPropertyType newType = propertyTypeBp.getPropertyType(key);
                    if (newType == null) {
                        toReturn.getWarnings().add(CatalogBp.SyncError.PROPERTY_TYPE_NOT_FOUND.getLabel() + ": " + key);
                        summary.incrementWarnings();
                    }
                    else {
                        ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
                        newProperty.setValue(itemData.get(key));
                        newProperty.setType(newType);
                        result = addOrModifyItemProperty(result, newProperty);
                    }
                }
            }
        }
        if (!itemData.keySet().contains(CATALOG)) {
            toReturn.getErrors().add(CatalogBp.SyncError.CATALOG_INCORRECTLY_SPECIFIED.getLabel());
            summary.incrementErrors();
        }
        
        this.setItemStatus(result);
        
        if (action != SyncAction.VENDOR_UPDATE) {
	        try {
	        	if(result.getVendorCatalog() == null){
	        		throw new NoVendorCatalogSpecifiedException();
	        	}
	        	this.calculateStreetPrice(result);
	        }
	        catch (PricingCalculationException e) {
	        	toReturn.getErrors().add(CatalogBp.SyncError.CANT_CALCULATE_STREET.getLabel());
	            summary.incrementErrors();
	        }
        }
        //checks to see if a required field has been left blank on the item
        if (result.getItemCategory() == null && action != SyncAction.VENDOR_UPDATE) {
            toReturn.getErrors().add(CatalogBp.SyncError.CATEGORY_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        } else if (result.getItemCategory() == null && action.equals(SyncAction.VENDOR_UPDATE)) {
        	//set category to "new item"
        	ItemCategory newCategory = new ItemCategory();
        	newCategory.setName("New Item");        	
            List<ItemCategory> searchList = categoryBp.searchByExactExample(newCategory, 0, 1);
            if (!searchList.isEmpty()) {
                result.setItemCategory(searchList.get(0));
            } else {
            	toReturn.getErrors().add(CatalogBp.SyncError.CATEGORY_NOT_ASSIGNED.getLabel());
                summary.incrementErrors();
            }
        }
        if (result.getUnitOfMeasure() == null) {
            toReturn.getErrors().add(CatalogBp.SyncError.UOM_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getName() == null || result.getName().isEmpty()) {
            toReturn.getErrors().add(CatalogBp.SyncError.NAME_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getDescription() == null || result.getDescription().isEmpty()) {
            toReturn.getErrors().add(CatalogBp.SyncError.DESCRIPTION_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        if (result.getItemWeight() == null) {
        	String weightSpecification = result.getSpecifications().get(ITEM_WEIGHT_SPECIFICATION_NAME);
			if(!StringUtils.isEmpty(weightSpecification)){
				LOG.debug("Setting item weight to specification property vale " + weightSpecification);
				try {
					result.setItemWeight(new BigDecimal(weightSpecification));
				} catch (NumberFormatException e){
					LOG.error("Could not convert " + weightSpecification + " to big decimal.");
		            toReturn.getErrors().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel());
		            summary.incrementErrors();
				}
			} else {
				toReturn.getErrors().add(CatalogBp.SyncError.ITEM_WEIGHT_NOT_FOUND.getLabel());
	            summary.incrementErrors();
			}
        }
        boolean hasVendorSku = false;
        for (Sku s : result.getSkus()) {
        	if (s.getType().getName().equals(VENDOR_SKUTYPE)) {
        		hasVendorSku = true;
        		break;
        	}
        }
        if (!hasVendorSku) {
            toReturn.getErrors().add(CatalogBp.SyncError.VENDOR_SKU_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        BigDecimal apdCost = BigDecimal.ZERO;
        boolean foundApdCost = false;
        if (action != SyncAction.VENDOR_UPDATE) {	        
	        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
	        	if (property.getType().getName().equals(APD_COST_PROPERTYTYPE) && property.getValue() != null) {
	        		foundApdCost = true;
	        		apdCost = new BigDecimal(property.getValue());
	        		break;
	        	}
	        }
	        if (!foundApdCost) {
	            toReturn.getErrors().add(CatalogBp.SyncError.NO_APD_COST.getLabel());
	            summary.incrementErrors();
	        }
        }
        
        boolean foundListPrice = false;
        BigDecimal listPrice = BigDecimal.ZERO;
        for (ItemXItemPropertyType property : result.getPropertiesReadOnly()) {
        	if (property.getType().getName().equals(LIST_PRICE_PROPERTYTYPE) && property.getValue() != null) {
        		foundListPrice = true;
        		listPrice = new BigDecimal(property.getValue());
        		break;
        	}
        }
        if (!foundListPrice) {
            toReturn.getErrors().add(CatalogBp.SyncError.NO_LIST_PRICE.getLabel());
            summary.incrementErrors();
        }
        
        //check to see apdcost should be less than list price
       if(foundApdCost && foundListPrice && apdCost.compareTo(listPrice) == 1) {    	   
		   toReturn.getErrors().add(CatalogBp.SyncError.APD_COST_GT_LIST_PRICE.getLabel());
           summary.incrementErrors();
           summary.incrementFailures();
       }
        
        //if changes should be made
        if (toReturn.getErrors().isEmpty() && persistChanges) {
            if (result.getId() != null) {
            	//if the entity will be updated, makes the update
                summary.incrementUpdated();
                this.update(result);
            }
            else {
            	//if it will be created, creates the entity
                summary.incrementCreated();
                this.createNoRefresh(result);
            }
        }
        else {
        	if (persistChanges) {
	        	//if can't make changes, but changes were intended, marks as failure
	        	summary.incrementFailures();
        	}
        	//rolls back changes
        	ItemRollbackException exception = new ItemRollbackException();
        	exception.setResult(toReturn);
        	throw exception;
        }
        return toReturn;
    }

    /**
     * Given several parameters, this method returns the item to update. It will return a non-null object. If a new item
     * should be created, the item will be empty; otherwise, an existing item will be returned. It is possible that the 
     * existing item will fail validation (including if the catalog it's on doesn't match the catalog on the CSV or 
     * the one being updated), but that is handled later in the syncItem method.
     * 
     * @param itemData
     * @param catalog
     * @param action
     * @param summary
     * @param persistChanges
     * @param toReturn
     * @return
     */
    private Item getItemToSync(Map<String, String> itemData, Catalog catalog,
			SyncAction action, SyncItemResultSummary summary,
			boolean persistChanges, SyncItemResult toReturn) {
		//finds the APD sku and vendor, and finds the item
        String apdSku = itemData.get(APD_SKU_HEADER);
        String vendorName = itemData.get(VENDOR_HEADER);
        String vendorSku = itemData.get(ItemBp.VENDOR_SKUTYPE + ItemBp.SKU_SUFFIX);
        
        //occurs when updating based on USSCO XML. Then, only vendor SKU is specified.
        if (action.equals(SyncAction.VENDOR_UPDATE) && StringUtils.isEmpty(apdSku) && StringUtils.isNotBlank(vendorSku)) {
        	Item searchItem = this.searchItem(vendorSku, VENDOR_SKUTYPE, vendorName);
        	//searching by vendor catalog name as well as SKU, in case there are items with 
        	//the same vendor SKU on different catalogs of the same vendor.
        	searchItem.getVendorCatalog().setName(itemData.get(ItemBp.CATALOG));
        	List<Item> resultList = this.dao.searchByExactExample(searchItem, 0, 0);
        	//if there's already an item with that vendor SKU, uses its APD SKU; otherwise, creates a new item, 
        	//with APD SKU = vendor SKU
        	if (!resultList.isEmpty()) {
        		apdSku = resultList.get(0).getSku(ItemBp.APD_SKUTYPE);
        	}
        	else {
        		apdSku = vendorSku;
        	}
        }
        
        if (!catalog.getVendor().getName().equals(vendorName)) {
        	toReturn.getErrors().add(CatalogBp.SyncError.INCORRECT_VENDOR.getLabel());
        	summary.incrementErrors();
        }
        
        if (StringUtils.isBlank(apdSku)) {
        	toReturn.getErrors().add(CatalogBp.SyncError.NO_APD_SKU.getLabel());
        	summary.incrementErrors();
        }
        
        toReturn.setResultApdSku(apdSku);
        toReturn.setResultVendor(vendorName);
        
        List<Item> existingItems = new ArrayList<>();
        // If APD Sku is provided, retrieve the item with that sku
        if (StringUtils.isNotBlank(apdSku) && StringUtils.isNotBlank(vendorName)) {
	        Item searchItem = this.searchItem(apdSku, vendorName);
	        existingItems = this.dao.searchByExactExample(searchItem, 0, 0);
        }

        Item result;
        //If the item doesn't exist, creates a new one
        if (existingItems.isEmpty() || !persistChanges) {
        	result = new Item();
            result.setName(BLANK);
            result.setDescription(BLANK);
            Sku newSku = new Sku();
            newSku.setItem(result);
            newSku.setType(skuTypeBp.getApdSkuType());
            newSku.setValue(apdSku);
            result.getSkus().add(newSku);
        }
        else {
            //otherwise, sets the item to the first item returned in the list
            //(Sku and vendor is a unique key, so this should have only one element)
            result = this.findById(existingItems.get(0).getId(), Item.class);
            if (itemData.containsKey(NEW_APD_SKU)) {
                result = this.changeSku(result, itemData.get(NEW_APD_SKU));
                summary.incrementDiscontinued();
            }
            else if (action.equals(SyncAction.REPLACE)) {
                //If the action is a replacement, the item's prop.erties are reset.
                result = this.writeItem(new Item(), result);
            }
        }
		return result;
	}

    @Inject
    private CatalogXItemDao catalogXItemDao;

    @Inject
    private ItemClassificationTypeBp classTypeBp;

    /**
     * This method discontinues an Item
     * 
     * @param item
     */
    public void discontinue(Item item) {
        if (ItemStatus.DISCONTINUED.equals(item.getStatus())) {
            //already discontinued
            return;
        }
        item = setDiscontinuedDate((new Date()), item);
        item.setStatus(ItemStatus.DISCONTINUED);
        //if the item exists in the datamodel already, updates all customer items
        if (item.getId() != null) {
            //gets the list of customer items referring to this vendor item
            CatalogXItem searchCxi = new CatalogXItem();
            searchCxi.setItem(new Item());
            searchCxi.getItem().setId(item.getId());
            List<CatalogXItem> searchCxiList = catalogXItemDao.searchByExactExample(searchCxi, 0, 0);
            Item replacementItem = new Item();
            if (item.getReplacement() != null) {
                replacementItem = this.findById(item.getReplacement().getId(), Item.class);
            }
            //for each customer item
            for (CatalogXItem catalogXItem : searchCxiList) {
                CatalogXItem fetchedCatalogXItem = catalogXItemDao.findById(catalogXItem.getId(), CatalogXItem.class);
                //if the customer item can be replaced, and if a replacement exists, and if the pricing type isn't 
                //"Fixed Price", it is pointed to the replacement
                if (replacementItem.getId() != null
                        && replaceAllowedFromCatalogXItem(fetchedCatalogXItem)
                        && (fetchedCatalogXItem.getPricingType() == null || !fetchedCatalogXItem.getPricingType()
                                .getName().equals(FIXED_PRICE_PRICINGTYPE))) {
                    //if the replacement isn't in the customer catalog, creates it
                    if (replaceAllowedWithConstraints(replacementItem, fetchedCatalogXItem)) {
                        CatalogXItem replacement = new CatalogXItem();
                        replacement.setCatalog(fetchedCatalogXItem.getCatalog());
                        replacement.setItem(replacementItem);
                        replacement.setPricingType(fetchedCatalogXItem.getPricingType());
                        replacement.setPricingParameter(fetchedCatalogXItem.getPricingParameter());
                        try {
                            this.calculatePrice(replacement);
                            catalogXItemDao.create(replacement);
                        }
                        catch (PricingCalculationException e) {
                            LOG.debug("Price calculation exception when creating replacement customer item: "
                                    + replacementItem.getSku(APD_SKUTYPE));
                        }
                    }
                    //updates properties on original
                    this.setCatalogXItemProperty(fetchedCatalogXItem, CUSTOMER_REPLACEMENT_SKU_PROPERTYTYPE,
                            replacementItem.getSku(APD_SKUTYPE));
                    this.setCatalogXItemProperty(fetchedCatalogXItem, CUSTOMER_REPLACEMENT_VENDOR_PROPERTYTYPE,
                            replacementItem.getVendorCatalog().getVendor().getName());
                }
                catalogXItemDao.update(fetchedCatalogXItem);
            }
        }
    }

    /**
     * Takes a CatalogXItem, and returns true if the CatalogXItem allows replacement, based on its properties
     * 
     * @param catalogXItem
     * @return
     */
    private boolean replaceAllowedFromCatalogXItem(CatalogXItem catalogXItem) {
        for (CatalogXItemXItemPropertyType property : catalogXItem.getProperties()) {
            if (property.getType().getName().equals(PREVENT_REPLACEMENT_PROPERTYTYPE)
                    && property.getValue().equalsIgnoreCase(TRUE)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Takes a CatalogXItem and a potential Item replacement, and determines whether making the replacement would violate 
     * database constraints. Specifically, the unique constraint on CATALOG_ID and ITEM_ID in the CATALOGXITEM table. That 
     * is, a vendor item can only occur once in a customer catalog.
     * 
     * @param replacement
     * @param catalogXItem
     * @return
     */
    private boolean replaceAllowedWithConstraints(Item replacement, CatalogXItem catalogXItem) {
        CatalogXItem constraintSearchItem = new CatalogXItem();
        constraintSearchItem.setCatalog(new Catalog());
        constraintSearchItem.setItem(new Item());
        constraintSearchItem.getCatalog().setId(catalogXItem.getCatalog().getId());
        constraintSearchItem.getItem().setId(replacement.getId());
        return catalogXItemDao.resultQuantity(constraintSearchItem) == 0;
    }

    private void setCatalogXItemProperty(CatalogXItem item, String propertyType, String value) {
        boolean foundProperty = false;
        for (CatalogXItemXItemPropertyType property : item.getProperties()) {
            if (property.getType().getName().equals(propertyType)) {
                property.setValue(value);
                foundProperty = true;
            }
        }
        if (!foundProperty) {
            ItemPropertyType availabilityType = propertyTypeBp.getPropertyType(propertyType);
            CatalogXItemXItemPropertyType newProperty = new CatalogXItemXItemPropertyType();
            newProperty.setType(availabilityType);
            newProperty.setValue(value);
            item.getProperties().add(newProperty);
        }
    }

    /**
     * This method un-discontinues an Item.
     * 
     * @param item
     */
    public void unDiscontinue(Item item) {
        //blanks out the current status
        item.setStatus(null);
        //creates a new status
        this.setItemStatus(item);
    }

    public void calculateStreetPrice(Item item) throws PricingCalculationException {
        BigDecimal price = pricingBp.calculatePrice(item, item.getVendorCatalog(), null, null, null).getPrice();
        for (ItemXItemPropertyType property : item.getPropertiesReadOnly()) {
            if (property.getType().getName().equals(STREET_PRICE_PROPERTYTYPE)) {
                property.setValue(price.toPlainString());
                item = addOrModifyItemProperty(item, property);
                return;
            }
        }
        ItemPropertyType searchType = new ItemPropertyType();
        searchType.setName(STREET_PRICE_PROPERTYTYPE);
        List<ItemPropertyType> searchList = propertyTypeBp.searchByExactExample(searchType, 0, 0);
        if (searchList.size() != 0) {
            ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
            newProperty.setType(searchList.get(0));
            newProperty.setValue(price.toPlainString());
            item = addOrModifyItemProperty(item, newProperty);
            return;
        }
    }

    public Item searchItem(String sku, String skuType, String vendorName) {
        Item search = new Item();
        search.setSkus(new HashSet<Sku>());
        Sku searchSku = new Sku();
        searchSku.setType(new SkuType());
        searchSku.getType().setName(skuType);
        searchSku.setValue(sku);
        search.getSkus().add(searchSku);
        search.setVendorCatalog(new Catalog());
        search.getVendorCatalog().setVendor(new Vendor());
        search.getVendorCatalog().getVendor().setName(vendorName);
        return search;
    }

    /**
     * This is a utility method, it generates a transient entity with the APD sku and vendor name set. This 
     * uniquely identifies one item.
     * 
     * @param apdSku
     * @param vendorName
     * @return
     */
    public Item searchItem(String apdSku, String vendorName) {
        return searchItem(apdSku, APD_SKUTYPE, vendorName);
    }

    /**
     * Sets the AbilityOneSubsitute of the item.
     * 
     * @param skuOfOriginal
     * @param skuOfNew
     * @param vendor
     * @param result
     */
    public void setEtsItem(String skuOfOriginal, String skuOfNew, String vendor) throws ItemNotFoundException {
        List<Item> originalSearchList = this.searchByExactExample(searchItem(skuOfOriginal, vendor), 0, 0);
        List<Item> etsSearchList = this.searchByExactExample(searchItem(skuOfNew, vendor), 0, 0);
        if (originalSearchList.isEmpty() || etsSearchList.isEmpty()) {
            throw new ItemNotFoundException();
        }
        Item original = originalSearchList.get(0);
        Item etsItem = etsSearchList.get(0);
        original.setAbilityOneSubstitute(etsItem);
    }

    /**
     * Given an Item and a new value for the APD sku, this method returns a new, unmanaged Item entity with the same 
     * values but a different APD sku.
     * 
     * @param original
     * @param newApdSku
     * @return
     */
    private Item changeSku(Item original, String newApdSku) {
        //search for an Item that already has this SKU
        Item searchItem = this.searchItem(newApdSku, original.getVendorCatalog().getVendor().getName());
        List<Item> resultList = this.searchByExactExample(searchItem, 0, 0);
        Item toReturn;
        if (!resultList.isEmpty()) {
            //If there is already an Item with that SKU, replaces the original with that item
            this.replace(original, resultList.get(0));
            return original;
        }
        else {
            //creates a clone
            toReturn = this.cloneEntity(original);
        }
        this.discontinue(original);
        //Adds the "changed SKU" property to the original item
        ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
        newProperty.setType(propertyTypeBp.getPropertyType(CHANGED_SKU_PROPERTYTYPE));
        newProperty.setValue(newApdSku);
        addOrModifyItemProperty(original, newProperty);
        //Adds a history to the original item
        ItemHistory newHistory = new ItemHistory();
        newHistory.setTime(new Date());
        newHistory.setDescription(CHANGED_SKU_HISTORY_MESSAGE + newApdSku);
        toReturn.getHistory().add(newHistory);
        //creates a new SKU for the new Item
        Sku newSku = new Sku();
        newSku.setType(skuTypeBp.getApdSkuType());
        newSku.setValue(newApdSku);
        newSku.setItem(toReturn);
        toReturn.getSkus().add(newSku);
        //Adds the original APD sku to the search terms of the new item
        for (Sku sku : original.getSkus()) {
            if (sku.getType().getName().equals(APD_SKUTYPE)) {
                if (toReturn.getSearchTerms() == null) {
                    toReturn.setSearchTerms(BLANK);
                }
                toReturn.setSearchTerms(toReturn.getSearchTerms().concat(IN_ROW_DELIMETER + sku.getValue()));
                break;
            }
        }
        return toReturn;
    }

    @Override
    public Item cloneEntity(Item t) {
        return this.writeItem(t, new Item());
    }

    /**
     * This method takes two Item objects, and writes the contents of the first one onto the second one, except for 
     * the APD sku, primary key, and history, which are maintained.
     * <br /><br />
     * writeItem(new Item(), existingItem) clears all values from existingItem, except for the ID, APD sku, and 
     * history. This is good for catalog replacements.
     * <br /><br />
     * writeItem(existingItem, new Item()) returns an item with all of the values of existingItem, except for the ID, 
     * APD sku, and history. Used by the cloneEntity method.
     * 
     * @param original
     * @param toWrite
     * @return
     */
    public Item writeItem(Item original, Item toWrite) {
        toWrite.setHierarchyNode(original.getHierarchyNode());
        toWrite.setDescription(original.getDescription());
        toWrite.setItemCategory(original.getItemCategory());
        toWrite.setManufacturer(original.getManufacturer());
        toWrite.setName(original.getName());
        toWrite.setReplacement(original.getReplacement());
        toWrite.setVendorCatalog(original.getVendorCatalog());
        toWrite.setSearchTerms(original.getSearchTerms());
        toWrite.getCustomerCosts().removeAll(toWrite.getCustomerCosts());
        toWrite.setCustomerCosts(new HashSet<CustomerCost>());
        for (CustomerCost cost : original.getCustomerCosts()) {
            CustomerCost newCost = new CustomerCost();
            newCost.setCustomer(cost.getCustomer());
            newCost.setValue(cost.getValue());
            newCost.setItem(toWrite);
            toWrite.getCustomerCosts().add(newCost);
        }
        Iterator<Sku> skuIterator = toWrite.getSkus().iterator();
        while (skuIterator.hasNext()) {
            Sku sku = skuIterator.next();
            if (!sku.getType().getName().equals(APD_SKUTYPE)) {
                skuIterator.remove();
            }
        }
        for (Sku sku : original.getSkus()) {
            if (!sku.getType().getName().equals(APD_SKUTYPE)) {
                Sku newSku = new Sku();
                newSku.setType(sku.getType());
                newSku.setValue(sku.getValue());
                newSku.setItem(toWrite);
                toWrite.getSkus().add(newSku);
            }
        }
        toWrite.getProperties().removeAll(toWrite.getPropertiesReadOnly());
        for (ItemXItemPropertyType property : original.getPropertiesReadOnly()) {
            ItemXItemPropertyType newProperty = new ItemXItemPropertyType();
            newProperty.setType(property.getType());
            newProperty.setValue(property.getValue());
            toWrite = addOrModifyItemProperty(toWrite, newProperty);
        }
        return toWrite;
    }

    //This is where item property list modification should occur
    @SuppressWarnings("deprecation")
    public Item addOrModifyItemProperty(Item toModify, ItemXItemPropertyType property) {
        if (ItemBp.APD_COST_PROPERTYTYPE.equals(property.getType().getName())) {
            toModify = setCurrentApdCostToOldApdCost(toModify);
        }
        ItemXItemPropertyType currentProperty = toModify.getPropertyReadOnly(property.getType().getName());
        if (currentProperty != null) {
            //If the property exists on the item already, remove it to avoid a duplicate
            removeProperty(toModify, currentProperty);
        }
        toModify.getProperties().add(property);
        return toModify;
    }

    /**
     * This method takes two Item entities, and goes through the process of marking one item as the replacement of 
     * the other.
     * 
     * @param original
     * @param replacement
     */
    private void replace(Item original, Item replacement) {
        if (original.getReplacement() != null && original.getReplacement().getId().equals(replacement.getId())) {
            return;
        }
        original.setReplacement(replacement);
        String apdSku = BLANK;
        for (Sku sku : replacement.getSkus()) {
            if (sku.getType().getName().equals(APD_SKUTYPE)) {
                apdSku = sku.getValue();
                break;
            }
        }
        ItemHistory newHistory = new ItemHistory();
        newHistory.setTime(new Date());
        newHistory.setDescription(REPLACED_HISTORY_MESSAGE + apdSku);
        original.getHistory().add(newHistory);
    }

    public List<Item> getCurrentCatalogItems(Long vendorId) {
        return ((ItemDao) this.dao).getCurrentCatalogItems(vendorId);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ScrollableResults batchForCsv(Catalog catalog, StatelessSession session) {
        return ((ItemDao) this.dao).batchForCsv(catalog, session);
    }

    public ArrayList<Long> batchForJMS(Catalog catalog, int batch) {
        return ((ItemDao) this.dao).batchForJMS(catalog, batch);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<Item> batchForStreetPrice(Catalog catalog, int batch) {
        return ((ItemDao) this.dao).batchForStreetPrice(catalog, batch);
    }

    public class ItemNotFoundException extends Exception {

        private static final long serialVersionUID = 2211212555758045254L;

    }

    public void refresh(Item item) {
        ((ItemDao) this.dao).refresh(item);
    }

    public void setRelationships(String vendorSku, String vendorName,
            MultivaluedMap<String, ItemRelationshipDto> relationshipData) {
        Item searchItem = this.searchItem(vendorSku, VENDOR_SKUTYPE, vendorName);
        List<Item> searchList = this.searchByExactExample(searchItem, 0, 0);
        if (searchList.isEmpty()) {
            return;
        }
        Item originalItem = this.findById(searchList.get(0).getId(), Item.class);
        for (String relationshipType : relationshipData.keySet()) {
            for (ItemRelationshipDto relationship : relationshipData.get(relationshipType)) {
                String relationshipSku = relationship.getSku();
                String relationshipVendorName = relationship.getVendorName();
                int relationshipPosition = relationship.getPosition();
                searchItem = this.searchItem(relationshipSku, relationshipVendorName);
                searchList = this.searchByExactExample(searchItem, 0, 0);
                if (!searchList.isEmpty()) {
                    addItemRelationship(originalItem, searchList.get(0), relationshipType, relationshipPosition);
                }
                else {
                    searchItem = this.searchItem(relationshipSku, VENDOR_SKUTYPE, relationshipVendorName);
                    searchList = this.searchByExactExample(searchItem, 0, 0);
                    if (!searchList.isEmpty()) {
                        addItemRelationship(originalItem, searchList.get(0), relationshipType, relationshipPosition);
                    }
                }
            }
        }
        this.update(originalItem);
    }

    private void addItemRelationship(Item original, Item relative, String relationship, int position) {
        ItemRelationship newRelationship = new ItemRelationship();
        for (ItemRelationship testRelationship : original.getSimilarItems()) {
            if (testRelationship.getRelative().getId().equals(relative.getId())
                    && testRelationship.getRelationship().equals(relationship)) {
                newRelationship = testRelationship;
                break;
            }
        }
        newRelationship.setOriginal(original);
        newRelationship.setRelative(relative);
        newRelationship.setRelationship(relationship);
        newRelationship.setPosition(position);
        original.getSimilarItems().add(newRelationship);
    }

    public void setItemStatus(Item toSet) {
        if (ItemStatus.DISCONTINUED.equals(toSet.getStatus())) {
            return;
        }
        for (ItemXItemPropertyType property : toSet.getPropertiesReadOnly()) {
            if ((property.getType().getName().equals(SPECIAL_ORDER_PROPERTYTYPE) || property.getType().getName()
                    .equals(CUSTOM_ORDER_PROPERTYPYPE))
                    && property.getValue().equalsIgnoreCase(TRUE) && !toSet.getStatus().equals(ItemStatus.DISCONTINUED)) {
                toSet.setStatus(ItemStatus.CONTACT_REQUIRED);
                return;
            }
        }
        toSet.setStatus(ItemStatus.AVAILABLE);
    }

    public Item retrieveItem(ItemDto item, Catalog catalog) {
        String apdSku = BLANK;
        List<SkuDto> skuDtos = item.getSkus();
        for (SkuDto skuDto : skuDtos) {
            if (skuDto.getType().getName().equals(APD_SKUTYPE)) {
                apdSku = skuDto.getValue();
            }
        }
        List<Item> results = null;
        if (!StringUtils.isEmpty(apdSku)) {
            results = ((ItemDao) dao).searchByApdSku(apdSku, catalog);
        }
        if (results != null && !results.isEmpty()) {
            if (results.size() > 1) {
                LOG.error("More than one item in customer catalog: " + catalog.getName() + " with apd sku: " + apdSku);
            }
            return results.get(0);
        }
        else {
            return null;
        }
    }

    @Inject
    private CatalogDao catalogDao;

    /**
     * Calculates the price of a CatalogXItem
     * 
     * @param toCalculate
     * @throws PricingCalculationException 
     */
    public CatalogXItem calculatePrice(CatalogXItem toCalculate) throws PricingCalculationException {
        if (!this.catalogXItemDao.isManaged(toCalculate) && toCalculate.getId() != null) {
            toCalculate = catalogXItemDao.findById(toCalculate.getId(), CatalogXItem.class);
        }
        if (!this.dao.isManaged(toCalculate.getItem()) && toCalculate.getItem().getId() != null) {
            toCalculate.setItem(dao.findById(toCalculate.getItem().getId(), Item.class));
        }
        if (!this.catalogDao.isManaged(toCalculate.getCatalog()) && toCalculate.getCatalog().getId() != null) {
            toCalculate.setCatalog(catalogDao.findById(toCalculate.getCatalog().getId(), Catalog.class));
        }

        //gets the name of the catalog, to check if a customer-specific cost is applicable
        Long customerId = toCalculate.getCatalog().getCustomer().getId();
        if (toCalculate.getCatalog().getCustomer().getRootAccount() != null) {
            customerId = toCalculate.getCatalog().getCustomer().getRootAccount().getId();
        }
        //first, calculates the new price and profit
        PricingCalculationResult result = pricingBp.calculatePrice(toCalculate.getItem(), toCalculate.getCatalog(),
                customerId, toCalculate.getPricingType(), toCalculate.getPricingParameter());
        BigDecimal newPrice = result.getPrice();
        BigDecimal newProfit = result.getProfit();

        //then, compares the existing profit and price to the calculated profits and prices
        CatalogXItemXItemPropertyType currentProfitProperty = getCatalogXItemProperty(toCalculate,
                CURRENT_PROFIT_PROPERTYTYPE);

        if (toCalculate.getPrice() == null || StringUtils.isBlank(currentProfitProperty.getValue())
                || newPrice.compareTo(toCalculate.getPrice()) != 0
                || !newProfit.toString().equals(currentProfitProperty.getValue())) {

            //if there's a change in either, updates the price, profit, and last price change date
            CatalogXItemXItemPropertyType priceChangeProperty = getCatalogXItemProperty(toCalculate,
                    PRICE_CHANGE_DATE_PROPERTYTYPE);

            priceChangeProperty.setValue((new Date()).toString());

            CatalogXItemXItemPropertyType lastPriceProperty = getCatalogXItemProperty(toCalculate,
                    LAST_PRICE_PROPERTYTYPE);

            lastPriceProperty.setValue(toCalculate.getPrice() != null ? toCalculate.getPrice().toString()
                    : ZERO_DOLLARS_AND_CENTS);

            CatalogXItemXItemPropertyType lastProfitProperty = getCatalogXItemProperty(toCalculate,
                    LAST_PROFIT_PROPERTYTYPE);

            lastProfitProperty
                    .setValue(StringUtils.isNotBlank(currentProfitProperty.getValue()) ? currentProfitProperty
                            .getValue() : ZERO_DOLLARS_AND_CENTS);

            currentProfitProperty.setValue(newProfit.toString());

            toCalculate.setPrice(newPrice);
        }
        return toCalculate;
    }

    private CatalogXItemXItemPropertyType getCatalogXItemProperty(CatalogXItem item, String propertyType) {
        CatalogXItemXItemPropertyType toReturn = item.getProperty(propertyType);
        if (toReturn == null) {
            toReturn = new CatalogXItemXItemPropertyType();
            toReturn.setType(propertyTypeBp.getPropertyType(propertyType));
            item.getProperties().add(toReturn);
        }
        return toReturn;
    }

    public Item setDiscontinuedDate(Date discontinuedDate, Item item) {
        if (item.getPropertiesReadOnly() == null) {
            item.setProperties(new HashSet<ItemXItemPropertyType>());
        }
        ItemXItemPropertyType discontintuedDateProperty = item.getPropertyReadOnly(DISCONTINUED_DATE_TYPE);
        if (discontintuedDateProperty == null) {
            discontintuedDateProperty = new ItemXItemPropertyType();
            discontintuedDateProperty.setType(propertyTypeBp.getPropertyType(DISCONTINUED_DATE_TYPE));
        }
        discontintuedDateProperty.setValue(sdf.format(discontinuedDate));
        item = addOrModifyItemProperty(item, discontintuedDateProperty);
        return item;
    }

    //private method part of add property logic
    @SuppressWarnings("deprecation")
    private Item setCurrentApdCostToOldApdCost(Item item) {
        ItemXItemPropertyType apdCost = item.getPropertyReadOnly(APD_COST_PROPERTYTYPE);
        if (item.getId() == null || apdCost == null || StringUtils.isEmpty(apdCost.getValue())) {
            //Don't set last apd cost if there isn't one, or if the item hasn't been persisted yet
            return item;
        }
        else {
            ItemXItemPropertyType lastApdCost = item.getPropertyReadOnly(LAST_APD_COST);
            if (lastApdCost != null && !StringUtils.isEmpty(lastApdCost.getValue())) {
                item = removeProperty(item, lastApdCost);
            }
            lastApdCost = new ItemXItemPropertyType();
            ItemPropertyType propertyType = propertyTypeBp.getPropertyType(LAST_APD_COST);
            lastApdCost.setType(propertyType);
            //Grabs the current apdCost from the database in case the local object has already changed it
            lastApdCost.setValue(((ItemDao) this.dao).getCurrentApdCost(item));
            item.getProperties().add(lastApdCost);
            return item;
        }
    }

    //Propert way to remove an item property from an item
    @SuppressWarnings("deprecation")
    public Item removeProperty(Item item, ItemXItemPropertyType property) {
        item.getProperties().remove(property);
        return item;

    }

    public class NoVendorCatalogSpecifiedException extends RuntimeException {

    }

}
