/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.brms.impl;

import com.apd.phoenix.service.brms.api.TaskServiceClientFactory;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jbpm.task.TaskService;
import org.jbpm.task.service.local.LocalTaskService;

@Stateless
@LocalBean
public class TaskServiceClientFactoryImpl implements TaskServiceClientFactory {

    @Inject
    TaskServiceServerFactoryImpl taskServiceServerFactory;

    public TaskService getTaskService() {
        return new LocalTaskService(taskServiceServerFactory.createTaskServiceServer());
    }
}