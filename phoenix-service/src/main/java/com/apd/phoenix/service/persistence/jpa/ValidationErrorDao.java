package com.apd.phoenix.service.persistence.jpa;

import com.apd.phoenix.service.model.ValidationError;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Validation Error DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ValidationErrorDao extends AbstractDao<ValidationError> {
}
