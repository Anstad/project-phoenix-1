package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.persistence.jpa.SyncItemResultDao;

/**
 * This class provides business process methods for Addresses.
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class SyncItemResultBp extends AbstractBp<SyncItemResult> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SyncItemResultDao dao) {
        this.dao = dao;
    }

    public List<SyncItemResult> getResults(String correlationId) {
        return ((SyncItemResultDao) dao).getResults(correlationId);
    }

    public void removeRecords(String correlationId) {
        Long id = ((SyncItemResultDao) dao).getId(correlationId);
        ((SyncItemResultDao) dao).removeRecords(id);
    }

    public List<Long> getResults(String correlationId, int batch) {
        return ((SyncItemResultDao) dao).getResults(correlationId, batch);
    }

    public List<SyncItemResult> getByIds(List<Long> ids) {
        return ((SyncItemResultDao) dao).getByIds(ids);
    }

    public void detach(SyncItemResult syncItemResult) {
        ((SyncItemResultDao) dao).detach(syncItemResult);
    }

    public long getCount(String correlationId) {
        return ((SyncItemResultDao) dao).getCount(correlationId);
    }

    public String csvRow(SyncItemResult syncItemResult) {
        StringBuilder sb = new StringBuilder();

        if (StringUtils.isNotBlank(syncItemResult.getResultVendor())) {
            sb.append(syncItemResult.getResultVendor());
        }
        sb.append(",");
        if (StringUtils.isNotBlank(syncItemResult.getResultApdSku())) {
            sb.append(syncItemResult.getResultApdSku());
        }
        sb.append(",");
        StringBuffer errorBuffer = new StringBuffer();
        for (String error : syncItemResult.getErrors()) {
            if (errorBuffer.length() != 0) {
                errorBuffer.append("; ");
            }
            errorBuffer.append(error);
        }
        sb.append(StringEscapeUtils.escapeCsv(errorBuffer.toString()));
        sb.append(",");
        StringBuffer warningBuffer = new StringBuffer();
        for (String warning : syncItemResult.getWarnings()) {
            if (warningBuffer.length() != 0) {
                warningBuffer.append("; ");
            }
            warningBuffer.append(warning);
        }
        sb.append(StringEscapeUtils.escapeCsv(warningBuffer.toString()));
        return sb.toString();
    }
}
