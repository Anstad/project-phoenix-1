/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.email.impl;

import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author nreidelb
 */
public class DuplicateCustomerPoErrorEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final String TEMPLATE = "duplicate.customer.po.error";

    @Override
    public String createBody(PurchaseOrderDto content) throws IOException, TemplateException {
         Map<String, Object> params = new HashMap<>();
         params.put("customerOrder", content);
         return this.create(params);
    }

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto content) throws NoAttachmentContentException {
        return null;
    }
}
