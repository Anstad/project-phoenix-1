package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Process;

/**
 * Process DAO stub
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ProcessDao extends AbstractDao<Process> {

    private static Logger LOG = LoggerFactory.getLogger(ProcessDao.class);

    /**
     * @param customerOrder
     * @return <List>Process
     */
    @SuppressWarnings("unchecked")
    public List<Process> findProcessByOrderByAccount(CustomerOrder customerOrder) {
        Query query = entityManager.createNamedQuery("findProcessByAccountByOrder");
        query.setParameter("id", customerOrder.getId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("\n\t*****There are " + query.getResultList().size());
        }

        return query.getResultList();
    }

    /**
     * @param customerOrder
     * @return <List>Process
     */
    @SuppressWarnings("unchecked")
    public List<Process> findProcessByOrderByCredential(CustomerOrder customerOrder) {
        Query query = entityManager.createNamedQuery("findProcessByCredentialByOrder");
        query.setParameter("id", customerOrder.getId());

        if (LOG.isDebugEnabled()) {
            LOG.debug("\n\t*****There are " + query.getResultList().size());
        }

        return query.getResultList();
    }

    @SuppressWarnings("static-method")
    public List<String> findAvailableBRMSProcesses() {
        return null;
    }
}
