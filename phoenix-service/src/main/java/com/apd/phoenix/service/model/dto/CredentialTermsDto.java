package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import org.drools.core.util.StringUtils;

public class CredentialTermsDto implements Serializable {

    private static final long serialVersionUID = 7612903098954401082L;
    private Integer daysUntilDue;
    private Integer discountPercent;
    private Integer daysWhileDiscounted;
    private String label;

    public Integer getDaysUntilDue() {
        return daysUntilDue;
    }

    public void setDaysUntilDue(Integer daysUntilDue) {
        this.daysUntilDue = daysUntilDue;
    }

    public Integer getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(Integer discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Integer getDaysWhileDiscounted() {
        return daysWhileDiscounted;
    }

    public void setDaysWhileDiscounted(Integer daysWhileDiscounted) {
        this.daysWhileDiscounted = daysWhileDiscounted;
    }

    @Override
    public String toString() {
        return "Terms: " + (discountPercent == null ? "" : discountPercent.toString() + "% ")
                + (daysUntilDue == null ? "" : ("net " + daysUntilDue.toString()));

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
