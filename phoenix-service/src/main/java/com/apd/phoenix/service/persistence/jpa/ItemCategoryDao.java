package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.ItemCategory;
import javax.persistence.Query;

/**
 * ItemCateogry DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class ItemCategoryDao extends AbstractDao<ItemCategory> {

    public ItemCategory getCategoryByName(String name) {
        String hql = "SELECT category FROM ItemCategory as category WHERE category.name=:name";
        Query query = entityManager.createQuery(hql);
        query.setParameter("name", name);
        return (ItemCategory) query.getSingleResult();
    }
}
