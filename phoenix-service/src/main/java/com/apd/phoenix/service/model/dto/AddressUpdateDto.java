package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddressUpdateDto implements Serializable {

    private static final long serialVersionUID = -5156424826714232358L;
    private List<AddressDto> addresses;
    private String companyName;

    public List<AddressDto> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressDto> addresses) {
        this.addresses = addresses;
    }

    /**
     * @return the companyName
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * @param companyName the companyName to set
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

}
