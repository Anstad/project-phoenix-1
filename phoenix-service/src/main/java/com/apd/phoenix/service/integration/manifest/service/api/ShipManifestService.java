package com.apd.phoenix.service.integration.manifest.service.api;

public interface ShipManifestService {

    public boolean sendManifestToJumptrack();

    public boolean verifyUsscoManifests();

    public boolean isUsscoFileNameAllowed(String filename);

    public boolean sendStopNotificationRequest();
}
