package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;
import com.apd.phoenix.service.message.api.MessageConstants;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class MessageMetadataDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5653880638222192418L;

    private Date messageDate;

    private String filePath;

    private long contentLength;

    private String contentType;

    private MessageType messageType;

    private CommunicationType communicationType;

    private String destination;

    private String ccRecipients;

    private String bccRecipients;

    private String transactionId;

    private String senderId;

    private String receiverId;

    private String interchangeId;

    private String groupId;

    public enum MessageType {
        EMAIL(MessageConstants.EMAIL), EDI(MessageConstants.EDI), CXML(MessageConstants.CXML), XCBL(
                MessageConstants.XCBL), OCI(MessageConstants.OCI), CSV(MessageConstants.CSV), XML(MessageConstants.XML), IMAGE(
                MessageConstants.IMAGE), TEXT(MessageConstants.TEXT);

        private final String label;

        private MessageType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum CommunicationType {
        INBOUND(MessageConstants.INBOUND), OUTBOUND(MessageConstants.OUTBOUND);

        private final String label;

        private CommunicationType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public Date getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(Date messageDate) {
        this.messageDate = messageDate;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(CommunicationType communicationType) {
        this.communicationType = communicationType;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getCcRecipients() {
        return ccRecipients;
    }

    public void setCcRecipients(String ccRecipients) {
        this.ccRecipients = ccRecipients;
    }

    public String getBccRecipients() {
        return bccRecipients;
    }

    public void setBccRecipients(String bccRecipients) {
        this.bccRecipients = bccRecipients;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getInterchangeId() {
        return interchangeId;
    }

    public void setInterchangeId(String interchangeId) {
        this.interchangeId = interchangeId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    /**
     * This object is serialized and put on MessageMetadata, for notification regeneration.
     * 
     * Note: if any modifications are made do this class, deserialization will probably break
     * for earlier versions (ie all versions in the upper environments). To prevent problems, 
     * you MUST run the following SQL:
     * 
     * UPDATE MESSAGEMETADATA SET objectsWrapper = null;
     * 
     * This will clear out the objects for regeneration, and those orders will only resend old,
     * potentially stale messages.
     * 
     * @author RHC
     *
     */
    public static class MessageObjectsWrapperDto implements Serializable {

        private static final long serialVersionUID = 1L;

        private long orderId;
        private Object content;
        @Deprecated
        private EventTypeDto eventType;
        private String eventName;

        public long getOrderId() {
            return orderId;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public Object getContent() {
            return content;
        }

        public void setContent(Object content) {
            this.content = content;
        }

        @Deprecated
        public EventTypeDto getEventType() {
            return eventType;
        }

        @Deprecated
        public void setEventType(EventTypeDto eventType) {
            this.eventType = eventType;
        }

        public String getEventName() {
            return this.eventName;
        }

        public void setEventName(String eventName) {
            this.eventName = eventName;
        }
    }

}
