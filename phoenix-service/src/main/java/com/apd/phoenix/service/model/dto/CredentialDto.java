package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CredentialDto implements Serializable {

    private static final long serialVersionUID = -6517519924081898124L;
    public Map<String, Serializable> properties = new HashMap<String, Serializable>();

    private CashoutPageDto cashoutPageDto;
    private String name;
    private CredentialTermsDto terms;
    private Set<CXMLCredentialDto> outgoingToCredentials;
    private Set<CXMLCredentialDto> outgoingFromCredentials;
    private Set<CXMLCredentialDto> outgoingSenderCredentials;
    private String outgoingSharedSecret;
    private Set<CXMLCredentialDto> customerOutgoingToCredentials;
    private Set<CXMLCredentialDto> customerOutgoingFromCredentials;
    private Set<CXMLCredentialDto> customerOutgoingSenderCredentials;
    private String customerOutgoingSharedSecret;
    private String vendorPunchoutUrl;
    private String vendorOrderUrl;
    private String vendorName;
    private String deploymentMode;
    private AddressDto defaultShipFromAddress;

    public CredentialDto() {
    }

    public CredentialDto(Map<String, Serializable> properties) {
        this.properties.putAll(properties);
    }

    public Map<String, Serializable> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, Serializable> properties) {
        this.properties = properties;
    }

    /**
     * @return the cashoutPageDto
     */
    public CashoutPageDto getCashoutPageDto() {
        return cashoutPageDto;
    }

    /**
     * @param cashoutPageDto the cashoutPageDto to set
     */
    public void setCashoutPageDto(CashoutPageDto cashoutPageDto) {
        this.cashoutPageDto = cashoutPageDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CredentialTermsDto getTerms() {
        return terms;
    }

    public void setTerms(CredentialTermsDto terms) {
        this.terms = terms;
    }

    /**
     * @return the outgoingToCredentials
     */
    public Set<CXMLCredentialDto> getOutgoingToCredentials() {
        return outgoingToCredentials;
    }

    /**
     * @param outgoingToCredentials the outgoingToCredentials to set
     */
    public void setOutgoingToCredentials(Set<CXMLCredentialDto> outgoingToCredentials) {
        this.outgoingToCredentials = outgoingToCredentials;
    }

    /**
     * @return the outgoingFromCredentials
     */
    public Set<CXMLCredentialDto> getOutgoingFromCredentials() {
        return outgoingFromCredentials;
    }

    /**
     * @param outgoingFromCredentials the outgoingFromCredentials to set
     */
    public void setOutgoingFromCredentials(Set<CXMLCredentialDto> outgoingFromCredentials) {
        this.outgoingFromCredentials = outgoingFromCredentials;
    }

    /**
     * @return the outgoingSenderCredentials
     */
    public Set<CXMLCredentialDto> getOutgoingSenderCredentials() {
        return outgoingSenderCredentials;
    }

    /**
     * @param outgoingSenderCredentials the outgoingSenderCredentials to set
     */
    public void setOutgoingSenderCredentials(Set<CXMLCredentialDto> outgoingSenderCredentials) {
        this.outgoingSenderCredentials = outgoingSenderCredentials;
    }

    /**
     * @return the outgoingSharedSecret
     */
    public String getOutgoingSharedSecret() {
        return outgoingSharedSecret;
    }

    /**
     * @param outgoingSharedSecret the outgoingSharedSecret to set
     */
    public void setOutgoingSharedSecret(String outgoingSharedSecret) {
        this.outgoingSharedSecret = outgoingSharedSecret;
    }

    /**
     * @return the customerOutgoingToCredentials
     */
    public Set<CXMLCredentialDto> getCustomerOutgoingToCredentials() {
        return customerOutgoingToCredentials;
    }

    /**
     * @param customerOutgoingToCredentials the customerOutgoingToCredentials to set
     */
    public void setCustomerOutgoingToCredentials(Set<CXMLCredentialDto> customerOutgoingToCredentials) {
        this.customerOutgoingToCredentials = customerOutgoingToCredentials;
    }

    /**
     * @return the customerOutgoingFromCredentials
     */
    public Set<CXMLCredentialDto> getCustomerOutgoingFromCredentials() {
        return customerOutgoingFromCredentials;
    }

    /**
     * @param customerOutgoingFromCredentials the customerOutgoingFromCredentials to set
     */
    public void setCustomerOutgoingFromCredentials(Set<CXMLCredentialDto> customerOutgoingFromCredentials) {
        this.customerOutgoingFromCredentials = customerOutgoingFromCredentials;
    }

    /**
     * @return the customerOutgoingSenderCredentials
     */
    public Set<CXMLCredentialDto> getCustomerOutgoingSenderCredentials() {
        return customerOutgoingSenderCredentials;
    }

    /**
     * @param customerOutgoingSenderCredentials the customerOutgoingSenderCredentials to set
     */
    public void setCustomerOutgoingSenderCredentials(Set<CXMLCredentialDto> customerOutgoingSenderCredentials) {
        this.customerOutgoingSenderCredentials = customerOutgoingSenderCredentials;
    }

    /**
     * @return the customerOutgoingSharedSecret
     */
    public String getCustomerOutgoingSharedSecret() {
        return customerOutgoingSharedSecret;
    }

    /**
     * @param customerOutgoingSharedSecret the customerOutgoingSharedSecret to set
     */
    public void setCustomerOutgoingSharedSecret(String customerOutgoingSharedSecret) {
        this.customerOutgoingSharedSecret = customerOutgoingSharedSecret;
    }

    /**
     * @return the vendorPunchoutUrl
     */
    public String getVendorPunchoutUrl() {
        return vendorPunchoutUrl;
    }

    /**
     * @param vendorPunchoutUrl the vendorPunchoutUrl to set
     */
    public void setVendorPunchoutUrl(String vendorPunchoutUrl) {
        this.vendorPunchoutUrl = vendorPunchoutUrl;
    }

    public String getVendorOrderUrl() {
        return vendorOrderUrl;
    }

    public void setVendorOrderUrl(String vendorOrderUrl) {
        this.vendorOrderUrl = vendorOrderUrl;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getDeploymentMode() {
        return deploymentMode;
    }

    public void setDeploymentMode(String deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

    /**
     * @return the defaultShipFromAddress
     */
    public AddressDto getDefaultShipFromAddress() {
        return defaultShipFromAddress;
    }

    /**
     * @param defaultShipFromAddress the defaultShipFromAddress to set
     */
    public void setDefaultShipFromAddress(AddressDto defaultShipFromAddress) {
        this.defaultShipFromAddress = defaultShipFromAddress;
    }
}
