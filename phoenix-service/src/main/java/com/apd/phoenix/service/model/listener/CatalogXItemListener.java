package com.apd.phoenix.service.model.listener;

import java.util.Date;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import com.apd.phoenix.service.model.CatalogXItem;

/**
 *
 * @author RHC
 */
public class CatalogXItemListener extends SolrEntityListener {

    @PrePersist
    @PreUpdate
    public void preUpdateEventListener(CatalogXItem entity) {
        entity.setLastModified(new Date());
    }

    @PostPersist
    public void addInsertSolrDocument(final CatalogXItem entity) throws NamingException, JMSException {
        doSolrDocumentAction(entity.getId(), SolrDocumentAction.CATXI_INSERT);
    }

    @PostUpdate
    public void addUpdateSolrDocument(final CatalogXItem entity) throws NamingException, JMSException {
        addUpdateSolrDocument(entity.getId());
        doSolrDocumentAction(entity.getCatalog().getId(), SolrDocumentAction.COMP_FAV_LIST_DELETE);
    }

    public static void addUpdateSolrDocument(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_UPDATE);
    }

    @PostRemove
    public void deleteSolrDocument(final CatalogXItem entity) throws JMSException, NamingException {
        doSolrDocumentAction(new CatalogXItem.DeleteItemMessage(entity), SolrDocumentAction.CATXI_DELETE);
        doSolrDocumentAction(entity.getCatalog().getId(), SolrDocumentAction.COMP_FAV_LIST_DELETE);
    }

    public static void addRepriceSolrDocument(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_REPRICE);
    }

    public static void addCalculateOverrides(final Long id) throws NamingException, JMSException {
        doSolrDocumentAction(id, SolrDocumentAction.CATXI_OVERRIDES_RECALC);
    }
}
