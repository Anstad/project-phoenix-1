package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.Date;

public class TermsOfSaleDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -1816087136806435968L;

    private String termsTypeCode;

    private String termsBasisDateCode;

    private String termsDiscountPercent;

    private Date termsDiscountDueDate;

    private Integer termsDiscountDaysDue;

    private Date termsNetDueDate;

    private String termsNetDays;

    private String description;

    public String getTermsTypeCode() {
        return termsTypeCode;
    }

    public void setTermsTypeCode(String termsTypeCode) {
        this.termsTypeCode = termsTypeCode;
    }

    public String getTermsBasisDateCode() {
        return termsBasisDateCode;
    }

    public void setTermsBasisDateCode(String termsBasisDateCode) {
        this.termsBasisDateCode = termsBasisDateCode;
    }

    public String getTermsDiscountPercent() {
        return termsDiscountPercent;
    }

    public void setTermsDiscountPercent(String termsDiscountPercent) {
        this.termsDiscountPercent = termsDiscountPercent;
    }

    public Date getTermsDiscountDueDate() {
        return termsDiscountDueDate;
    }

    public void setTermsDiscountDueDate(Date termsDiscountDueDate) {
        this.termsDiscountDueDate = termsDiscountDueDate;
    }

    public Integer getTermsDiscountDaysDue() {
        return termsDiscountDaysDue;
    }

    public void setTermsDiscountDaysDue(Integer termsDiscountDaysDue) {
        this.termsDiscountDaysDue = termsDiscountDaysDue;
    }

    public Date getTermsNetDueDate() {
        return termsNetDueDate;
    }

    public void setTermsNetDueDate(Date termsNetDueDate) {
        this.termsNetDueDate = termsNetDueDate;
    }

    public String getTermsNetDays() {
        return termsNetDays;
    }

    public void setTermsNetDays(String termsNetDays) {
        this.termsNetDays = termsNetDays;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
