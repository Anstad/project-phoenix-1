package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.StringEscape;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class OrderApprovalNeededEmailTemplate extends EmailTemplate<PurchaseOrderDto> {

    @Inject
    CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderApprovalNeededEmailTemplate.class);

    private static final String TEMPLATE = "order.approval.needed";

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		
		params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
		params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
		
		LOGGER.info("Getting token");
		CustomerOrder customerOrder = customerOrderBp.searchByApdPo(purchaseOrderDto.getApdPoNumber());
		String token="";
		if (customerOrder != null){
			token = customerOrderRequestBp.createRequestToken(customerOrder);
		}
		String approver = customerOrderBp.getApprover(customerOrder);
		Properties workflow = PropertiesLoader.getAsProperties("integration");
		String authority = workflow.getProperty("orderApprovalRestIp", "http://localhost:8080");
		LOGGER.info("Token {}",token);
		
		params.put("approveUrl",authority + "/ws/workflow/order/approve/" + StringEscape.escapeForUrl(approver) + "/" + token);
		params.put("denyUrl",authority + "/ws/workflow/order/deny/" + StringEscape.escapeForUrl(approver) + "/" + token);
		
		params.put("merchandiseTotal", purchaseOrderDto.getSubTotal());	
		params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
		params.put("taxTotal", purchaseOrderDto.getMaximumTaxToCharge());	
		params.put("orderTotal", purchaseOrderDto.getOrderTotal());
		params.put("items", purchaseOrderDto.getItems());
		
		if (customerOrder != null && customerOrder.getUser() != null && customerOrder.getUser().getPerson() != null) {
			params.put("userName", customerOrder.getUser().getPerson().getFormalName());
			params.put("userEmail", customerOrder.getUser().getPerson().getEmail());
		}
		
		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto purchaseOrderDto) {
        // TODO Auto-generated method stub
        return null;
    }
}
