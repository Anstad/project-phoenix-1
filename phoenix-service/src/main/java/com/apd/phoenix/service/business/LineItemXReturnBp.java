package com.apd.phoenix.service.business;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.persistence.jpa.LineItemXReturnDao;

@Stateless
@LocalBean
public class LineItemXReturnBp extends AbstractBp<LineItemXReturn> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LineItemXReturnBp.class);

    @Inject
    public void initDao(LineItemXReturnDao dao) {
        this.dao = dao;
    }

    @Deprecated
    public LineItemXReturn checkForNullReturnShipping(LineItemXReturn lixr) {
        if (lixr.getReturnShippingAmount() == null) {
            //TODO: change to error after April 2016
            LOGGER.warn("Return shipping should never be null for orders after april 2015. "
                    + "Calculating return shipping now for backwards compatability.");
            lixr.calculateReturnShippingAmount();
            return this.dao.update(lixr);
        }
        return lixr;

    }
}
