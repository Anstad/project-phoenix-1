package com.apd.phoenix.service.message.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderBp.NonUniqueCustomerPoException;
import com.apd.phoenix.service.business.MessageMetadataBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.CommunicationType;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.utility.ErrorEmailService;

@Named
@Stateless
public class MessageServiceImpl implements MessageService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private MessageMetadataDao messageMetadataDao;

    @Inject
    MessageMetadataBp messageMetadataBp;

    @Inject
    MessageUtils messageUtils;

    @Inject
    CXMLMessageSender cxmlMessageSender;

    @Inject
    DefaultMessageSender defaultMessageSender;

    @Inject
    EDIMessageSender ediMessageSender;

    @Inject
    EmailMessageSender emailMessageSender;

    @Inject
    OCIMessageSender ociMessageSender;

    @Inject
    XCBLMessageSender xcblMessageSender;

    @Inject
    CSVMessageSender csvMessageSender;

    @Inject
    XMLMessageSender xmlMessageSender;

    @Inject
    ErrorEmailService errorEmailService;

    @Override
    public boolean sendOrderMessage(CustomerOrder order, Message message, EventType eventType) {
        sendMessage(message, eventType);
        CustomerOrder result = logMessage(order, message, eventType);
        if (result == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        if (message.getMetadata() != null
                && CommunicationType.INBOUND.equals(message.getMetadata().getCommunicationType())) {
            LOG.info("Cannot send inbound messages");
            return false;
        }
        MessageType messageType = message.getMetadata().getMessageType();
        MessageSender messageSender = getMessageSenderForType(messageType);
        return messageSender.sendMessage(message, eventType);
    }

    private CustomerOrder logMessage(CustomerOrder customerOrder, Message message, EventType eventType) {
        if (customerOrder == null) {
            return null;
        }
        OrderLog orderLog = new OrderLog();
        orderLog.setVersion(0);
        orderLog.setMessageMetadata(message.getMetadata());
        orderLog.setCustomerOrder(customerOrder);
        // TODO:EventType selection
        orderLog.setEventType(eventType);
        return customerOrderBp.addLog(customerOrder, orderLog);
    }

    @Override
    public boolean receiveOrderMessage(MessageDto messageDto, String token, String messageEventType) {
        MessageMetadataDto messageMetadataDto = messageDto.getMessageMetadataDto();

        LOG.info("token: {}", token);
        LOG.info("message: {}", messageDto);
        LOG.info("event type: {}", messageEventType);
        CustomerOrder customerOrder;

        customerOrder = customerOrderBp.searchByApdPo(token);
        if (customerOrder == null) {
            LOG.error("customer order not found");
            //Some order requests will not have a pre existing order at this stage.
            if (!EventType.ORDER_REQUEST.name().equals(messageEventType)) {
                errorEmailService.sendEDIOrderNotFoundEmail(messageDto, token, messageEventType);
            }
            return false;
        }
        LOG.info("co id: {}", customerOrder.getId());

        OrderLog orderLog = new OrderLog();
        orderLog.setCustomerOrder(customerOrder);
        orderLog.setEventType(OrderLog.EventType.valueOf(messageEventType));

        MessageMetadata messageMetadata = new MessageMetadata();
        messageMetadata.setCommunicationType(MessageMetadata.CommunicationType.valueOf(messageMetadataDto
                .getCommunicationType().name()));
        messageMetadata.setMessageType(MessageMetadata.MessageType.valueOf(messageMetadataDto.getMessageType().name()));
        messageMetadata.setMessageDate(messageMetadataDto.getMessageDate());
        messageMetadata.setContentLength(messageMetadataDto.getContentLength());
        messageMetadata.setContentType(messageMetadataDto.getContentType());
        messageMetadata.setDestination(messageMetadataDto.getDestination());
        messageMetadata.setBccRecipients(messageMetadataDto.getBccRecipients());
        messageMetadata.setCcRecipients(messageMetadataDto.getCcRecipients());
        messageMetadata.setFilePath(messageMetadataDto.getFilePath());
        messageMetadata.setInterchangeId(messageMetadataDto.getInterchangeId());
        messageMetadata.setGroupId(messageMetadataDto.getGroupId());
        messageMetadata.setSenderId(messageMetadataDto.getSenderId());
        messageMetadata.setReceiverId(messageMetadataDto.getReceiverId());
        messageMetadata.setTransactionId(messageMetadataDto.getTransactionId());
        messageMetadata = messageMetadataDao.create(messageMetadata);

        //Set the CXML Order payload id for future reference
        //        if (EventType.PURCHASE_ORDER_SENT.equals(orderLog.getEventType())
        //                && com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType.CXML.equals(messageMetadataDto
        //                        .getMessageType())) {
        //            customerOrder.setOrderPayloadId(messageMetadataDto.getInterchangeId());
        //            customerOrder = customerOrderBp.update(customerOrder);
        //        }

        orderLog.setMessageMetadata(messageMetadata);
        //orderLog = orderLogDao.create(orderLog);

        customerOrderBp.addLog(customerOrder, orderLog);

        try {
            if (!createMessage(messageDto)) {
                return false;
            }
        }
        catch (Exception e) {
            LOG.error("Unable to create message", e);
            return false;
        }
        return true;
    }

    private boolean createMessage(MessageDto messageDTO) {

        MessageMetadataDto messageMetadataDTO = messageDTO.getMessageMetadataDto();
        if (messageMetadataDTO == null) {
            return false;
        }

        MessageMetadata metadata = new MessageMetadata();

        MessageType messageType = MessageType.valueOf(messageMetadataDTO.getMessageType().getLabel());
        if (messageType == null) {
            return false;
        }
        metadata.setMessageType(messageType);
        String path = messageMetadataDTO.getFilePath();
        if (path == null || path.isEmpty()) {
            return false;
        }
        metadata.setFilePath(path);
        CommunicationType comType = CommunicationType.valueOf(messageMetadataDTO.getCommunicationType().getLabel());
        if (comType == null) {
            return false;
        }
        metadata.setCommunicationType(comType);
        String destination = messageMetadataDTO.getDestination();
        if (!messageType.equals(MessageType.EMAIL) && (destination == null || destination.isEmpty())) {
            return false;
        }
        metadata.setDestination(destination);
        metadata.setCcRecipients(messageMetadataDTO.getCcRecipients());
        metadata.setBccRecipients(messageMetadataDTO.getBccRecipients());
        metadata.setMessageDate(new Date());
        
        byte[] content = new byte[0];
        if (StringUtils.isNotBlank(messageDTO.getContent())) {
	        if(messageDTO.isBase64Encoded()) {
	        	content = org.apache.commons.codec.binary.Base64.decodeBase64(messageDTO.getContent());
	        } else {
	        	content = messageDTO.getContent().getBytes();
	        }
        }
        else {
        	messageDTO.setContent("");
        	LOG.warn("Storing an empty message.");
        }
        
        metadata.setContentLength(content.length);
        metadata.setContentType(messageDTO.getMessageMetadataDto().getContentType());

        try (InputStream stream = new ByteArrayInputStream(content)) {

            Message message = new Message();
            message.setMetadata(metadata);
            message.setContent(stream);

            messageUtils.createMessage(message);
        } catch (IOException ex) {
            LOG.error("Error reading content to InputStream.", ex);
        }
        return true;
    }

    @Override
    public boolean sendPurchaseOrder(PurchaseOrderDto po, MessageType messageType) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        Message message = messageSender.sendPurchaseOrder(po);
        try {
            CustomerOrder customerOrder = customerOrderBp.createOrUpdateCustomerOrder(po);
            EventType type = EventType.PURCHASE_ORDER_SENT;
            if (message != null && customerOrder != null) {
                this.logMessage(customerOrder, message, type);
                return true;
            }
            else {
                return false;
            }
        }
        catch (NonUniqueCustomerPoException e) {
            errorEmailService.sendDuplicateCustomerPOErrorEmail(e.getOrder(), e.getCustomerPo());
        }
        return false;
    }

    @Override
    public boolean sendPurchaseOrderAcknowledgment(POAcknowledgementDto po, MessageType messageType) {
        return sendPurchaseOrderAcknowledgment(po, messageType, Boolean.FALSE);
    }

    @Override
    public boolean sendPurchaseOrderAcknowledgment(POAcknowledgementDto po, MessageType messageType, Boolean resend) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        Message message = messageSender.sendPOAcknowledgement(po, resend);
        CustomerOrder customerOrder = customerOrderBp.getCustomerOrder(po);
        EventType type;
        if (resend) {
            type = EventType.ORDER_ACKNOWLEDGEMENT_RESEND;
        }
        else {
            type = EventType.ORDER_ACKNOWLEDGEMENT;
        }
        if (message != null && customerOrder != null) {
            this.logMessage(customerOrder, message, type);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean sendShipmentNotice(ShipmentDto shipmentDto, MessageType messageType) {
        return sendShipmentNotice(shipmentDto, messageType, Boolean.FALSE);
    }

    @Override
    public boolean sendShipmentNotice(ShipmentDto shipmentDto, MessageType messageType, Boolean resend) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        Message message = messageSender.sendShipmentNotice(shipmentDto, resend);
        CustomerOrder customerOrder = customerOrderBp.getCustomerOrder(shipmentDto);
        EventType type = EventType.SHIPMENT_NOTICE;
        if (message != null && customerOrder != null) {
            this.logMessage(customerOrder, message, type);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean sendInvoice(CustomerInvoiceDto customerInvoiceDto, MessageType messageType) {
        return sendInvoice(customerInvoiceDto, messageType, Boolean.FALSE);
    }

    @Override
    public boolean sendInvoice(CustomerInvoiceDto customerInvoiceDto, MessageType messageType, Boolean resend) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        CustomerOrder customerOrder = customerOrderBp.getCustomerOrder(customerInvoiceDto);
        EventType type = EventType.CUSTOMER_INVOICED;
        Message message = messageSender.sendInvoice(customerInvoiceDto, resend);
        if (message != null && customerOrder != null) {
            this.logMessage(customerOrder, message, type);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, MessageType messageType) {
        return sendCreditInvoice(customerCreditInvoiceDto, messageType, Boolean.FALSE);
    }

    @Override
    public boolean sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, MessageType messageType,
            Boolean resend) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        Message message = messageSender.sendCreditInvoice(customerCreditInvoiceDto, resend);
        EventType type = EventType.CUSTOMER_CREDIT_INVOICED;
        CustomerOrder customerOrder = customerOrderBp.getCustomerOrder(customerCreditInvoiceDto);
        if (message != null && customerOrder != null) {
            this.logMessage(customerOrder, message, type);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean sendCardNotification(CardNotificationDto cardNotificationDto, MessageType messageType) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        Message message = messageSender.sendCardNotification(cardNotificationDto);
        EventType type = EventType.CREDIT_CARD_TRANSACTION;

        if (cardNotificationDto.getEventType() != null) {
            for (EventType entityType : EventType.values()) {
                if (entityType.getLabel().equals(cardNotificationDto.getEventType().getLabel())) {
                    type = entityType;
                    break;
                }
            }
        }

        CustomerOrder customerOrder = customerOrderBp.findById(cardNotificationDto.getOrderId(), CustomerOrder.class);
        if (message != null && customerOrder != null) {
            this.logMessage(customerOrder, message, type);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean sendShipConfirmation(List<PickDto> pickList, MessageType messageType) {
        if (messageType == null) {
            return false;
        }
        MessageSender messageSender = getMessageSenderForType(messageType);
        return messageSender.sendShipConfirmation(pickList) != null;
    }

    private MessageSender getMessageSenderForType(MessageType messageType) {
        switch (messageType) {
            case CXML:
                return cxmlMessageSender;
            case EDI:
                return ediMessageSender;
            case EMAIL:
                return emailMessageSender;
            case XCBL:
                return xcblMessageSender;
            case OCI:
                return ociMessageSender;
            case CSV:
                return csvMessageSender;
            case XML:
                return xmlMessageSender;
            default:
                return defaultMessageSender;
        }
    }

    @Override
    public boolean sendJumptrackManifest(ManifestRequestDto manifestRequestDto, MessageType messageType) {
        LOG.info("Sending JumptrackManifest");

        MessageSender messageSender = getMessageSenderForType(messageType);
        messageSender.sendJumptrackManifest(manifestRequestDto);
        return true;
    }

    @Override
    public boolean sendJumptrackStopNotificationRequest(StopNotificationRequestDto stopNotificationRequestDto,
            MessageType messageType) {
        LOG.info("Sending Jumptrack stop notification");
        MessageSender messageSender = getMessageSenderForType(messageType);
        messageSender.sendJumptrackStopNotificationRequest(stopNotificationRequestDto);
        return true;
    }

    @Override
    public boolean sendMarquetteInvoice(List<InvoiceDto> invoice, Boolean credit) {
        LOG.info("Sending Marquette Invoice");
        MessageSender messageSender = getMessageSenderForType(MessageType.CSV);
        messageSender.sendMarquetteInvoice(invoice, credit);
        return false;
    }
}
