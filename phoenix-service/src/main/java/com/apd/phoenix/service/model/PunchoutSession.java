package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.listener.PunchoutSessionEntityListener;

/**
 *
 * @author RH
 */
@EntityListeners(PunchoutSessionEntityListener.class)
@Entity
public class PunchoutSession implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 6056259594315954985L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column
    private String sessionToken;

    @Temporal(TemporalType.TIMESTAMP)
    private Date initTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;

    @Column
    private String browserFormPostUrl;

    @Column
    private String browserFormPostTargetFrame = "_top";

    public String getBrowserFormPostTargetFrame() {
        return browserFormPostTargetFrame;
    }

    public void setBrowserFormPostTargetFrame(String browserFormPostTargetFrame) {
        this.browserFormPostTargetFrame = browserFormPostTargetFrame;
    }

    @Column
    private String buyerCookie;

    @OneToOne
    private AccountXCredentialXUser accntCredUser;

    @OneToOne
    private CustomerOrder customerOrder;

    @Column
    private String systemUserLoginName;

    @Column(nullable = false, updatable = false, columnDefinition = "varchar2(10) default 'create'")
    @Enumerated(EnumType.STRING)
    private PunchoutOrderOperation operation = PunchoutOrderOperation.create;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public Date getInitTime() {
        return initTime;
    }

    public void setInitTime(Date startTime) {
        this.initTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getBuyerCookie() {
        return buyerCookie;
    }

    public void setBuyerCookie(String buyerCookie) {
        this.buyerCookie = buyerCookie;
    }

    public String getSystemUserLoginName() {
        return systemUserLoginName;
    }

    public void setSystemUserLoginName(String systemUserLoginName) {
        this.systemUserLoginName = systemUserLoginName;
    }

    public String getBrowserFormPostUrl() {
        return browserFormPostUrl;
    }

    public void setBrowserFormPostUrl(String browserFormPostUrl) {
        this.browserFormPostUrl = browserFormPostUrl;
    }

    public AccountXCredentialXUser getAccntCredUser() {
        return accntCredUser;
    }

    public void setAccntCredUser(AccountXCredentialXUser accntCredUser) {
        this.accntCredUser = accntCredUser;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public Credential getCredential() {
        Credential credential = null;
        if (this.accntCredUser != null) {
            credential = getAccntCredUser().getCredential();
        }
        return credential;
    }

    public PunchoutOrderOperation getOperation() {
        return operation;
    }

    public void setOperation(PunchoutOrderOperation operation) {
        this.operation = operation;
    }
}
