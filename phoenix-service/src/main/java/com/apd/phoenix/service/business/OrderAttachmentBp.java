package com.apd.phoenix.service.business;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderAttachment;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.OrderAttachmentDao;
import com.apd.phoenix.service.storage.api.StorageObject;
import com.apd.phoenix.service.storage.api.StorageObjectRequest;
import com.apd.phoenix.service.storage.api.StorageService;

/**
 * This class provides business process methods for OrderUploadedFile.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class OrderAttachmentBp extends AbstractBp<OrderAttachment> {

    private Properties awsProperties;

    @Inject
    private StorageService storageService;

    @Inject
    private CustomerOrderDao customerOrderDao;

    @PostConstruct
    public void init() {
        awsProperties = PropertiesLoader.getAsProperties("aws.integration");
    }

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(OrderAttachmentDao dao) {
        this.dao = dao;
    }

    public InputStream getFile(OrderAttachment uploadedFile) {
        String path = this.getPath(uploadedFile);
        StorageObjectRequest request = new StorageObjectRequest();
        request.setPath(path);
        request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
        StorageObject object = storageService.retrieveObject(request);
        if (object == null) {
            return null;
        }
        return object.getContent();
    }

    public void setFile(OrderAttachment uploadedFile, InputStream content, long length) throws IOException {
        String path = this.getPath(uploadedFile);
        if (this.isFileExists(path)) {
            StorageObject storageObject = new StorageObject();
            storageObject.setPath(path);
            storageObject.setContent(content);
            storageObject.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
            storageObject.setContentLength(length);
            storageObject.setContentType("text/plain");
            storageService.updateObject(storageObject);
        }
        else {
            StorageObject storageObject = new StorageObject();
            storageObject.setPath(path);
            storageObject.setContent(content);
            storageObject.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
            storageObject.setContentLength(length);
            storageObject.setContentType("text/plain");
            storageService.createObject(storageObject);
        }
    }

    private boolean isFileExists(String path) {
        try {
            StorageObjectRequest request = new StorageObjectRequest();
            request.setPath(path);
            request.getAttributes().put("bucketName", awsProperties.getProperty("message.service.bucketName"));
            return storageService.retrieveObject(request) != null;
        }
        catch (Exception e) {
            return false;
        }
    }

    private String getPath(OrderAttachment uploadedFile) {
        StringBuilder path = new StringBuilder();
        CustomerOrder searchOrder = new CustomerOrder();
        OrderAttachment searchFile = new OrderAttachment();
        searchFile.setId(uploadedFile.getId());
        searchOrder.getAttachments().add(searchFile);
        path.append("uploaded-files/");
        path.append(uploadedFile.getId().toString());
        return path.toString();
    }
}
