/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class ShipmentDto extends TransactionDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private PurchaseOrderDto customerOrderDto;
    private ShippingPartnerDto shippingPartnerDto;
    private BigDecimal totalTaxPrice;
    private List<LineItemXShipmentDto> lineItemXShipments;
    private String trackingNumber;
    private String routing;
    private String trackingNumberQualifier;

    private BigDecimal shipmentPrice;
    private List<String> comments;
    //whether the shipment was fully delivered
    private String status;
    //whether the delivery was a shipment or a return
    private String type;
    private Date shipTime;
    private Date deliveredTime;
    private AddressDto fromAddress;
    private AddressDto customerServiceAddress;
    private String shippingMethodOfPayment;
    private BigInteger packs;
    private BigInteger innerPacks;
    //Do not expose publicly
    private Long shipmentDatabaseKey;

    /**
     * @return the customerOrderDto
     */
    public PurchaseOrderDto getCustomerOrderDto() {
        return customerOrderDto;
    }

    /**
     * @param customerOrderDto the customerOrderDto to set
     */
    public void setCustomerOrderDto(PurchaseOrderDto customerOrderDto) {
        this.customerOrderDto = customerOrderDto;
    }

    /**
     * @return the lineItemXShipments
     */
    public List<LineItemXShipmentDto> getLineItemXShipments() {
        return lineItemXShipments;
    }

    /**
     * @param lineItemXShipments the lineItemXShipments to set
     */
    public void setLineItemXShipments(List<LineItemXShipmentDto> lineItemXShipments) {
        this.lineItemXShipments = lineItemXShipments;
    }

    /**
     * @return the trackingNumber
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * @param trackingNumber the trackingNumber to set
     */
    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    /**
     * @return the shipmentPrice
     */
    public BigDecimal getShipmentPrice() {
        return shipmentPrice;
    }

    /**
     * @param shipmentPrice the shipmentPrice to set
     */
    public void setShipmentPrice(BigDecimal shipmentPrice) {
        this.shipmentPrice = shipmentPrice;
    }

    /**
     * @return the comments
     */
    public List<String> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the shipTime
     */
    public Date getShipTime() {
        return shipTime;
    }

    /**
     * @param shipTime the shipTime to set
     */
    public void setShipTime(Date shipTime) {
        this.shipTime = shipTime;
    }

    public List<LineItemXShipmentDto> getValidItems(){
    	List<LineItemXShipmentDto> toReturn = new ArrayList<>();
    	for (LineItemXShipmentDto lixs : lineItemXShipments){
    		if (lixs.getValid() != null && lixs.getValid()){
    			toReturn.add(lixs);
    		}
    	}
    	return toReturn;
    }

    public BigDecimal calculateLIXSTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentDto lixs : lineItemXShipments) {
            toReturn = toReturn.add(lixs.calculateSubTotal());
        }
        return toReturn;
    }

    public BigDecimal calculateLIXSTotalNoFees() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentDto lixs : lineItemXShipments) {
            if (!lixs.getLineItemDto().getFee()) {
                toReturn = toReturn.add(lixs.calculateSubTotal());
            }
        }
        return toReturn;
    }

    public BigDecimal calculateLIXSTotalFeesOnly() {
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentDto lixs : lineItemXShipments) {
            if (lixs.getLineItemDto().getFee()) {
                toReturn = toReturn.add(lixs.calculateSubTotal());
            }
        }
        return toReturn;
    }

    public BigDecimal calculateShippingTotal() {
        List<LineItemXShipmentDto> lixsd = lineItemXShipments;
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentDto lixs : lixsd) {
            toReturn = toReturn.add(lixs.getShipping());
        }
        return toReturn;
    }

    public BigDecimal calculateTaxableAmountTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        toReturn = toReturn.add(this.calculateLIXSTotal());
        toReturn = toReturn.add(this.calculateShippingTotal());
        return toReturn;
    }

    public BigDecimal calculateTaxTotal() {
        List<LineItemXShipmentDto> lixsd = lineItemXShipments;
        BigDecimal toReturn = BigDecimal.ZERO;
        for (LineItemXShipmentDto lixs : lixsd) {
            if (lixs.calculateTotalTaxAmount() != null) {
                toReturn = toReturn.add(lixs.calculateTotalTaxAmount());
            }
        }
        return toReturn;
    }

    public BigDecimal calculateShipmentTotal() {
        BigDecimal toReturn = BigDecimal.ZERO;
        toReturn = toReturn.add(this.calculateTaxableAmountTotal());
        toReturn = toReturn.add(this.calculateTaxTotal());
        return toReturn;
    }

    public Date getDeliveredTime() {
        return deliveredTime;
    }

    public void setDeliveredTime(Date deliveredTime) {
        this.deliveredTime = deliveredTime;
    }

    public ShippingPartnerDto getShippingPartnerDto() {
        return shippingPartnerDto;
    }

    public void setShippingPartnerDto(ShippingPartnerDto shippingPartnerDto) {
        this.shippingPartnerDto = shippingPartnerDto;
    }

    public AddressDto getCustomerServiceAddress() {
        return customerServiceAddress;
    }

    public void setCustomerServiceAddress(AddressDto customerServiceAddress) {
        this.customerServiceAddress = customerServiceAddress;
    }

    /**
     * @return the fromAddress
     */
    public AddressDto getFromAddress() {
        if (fromAddress == null && getCustomerOrderDto() != null && getCustomerOrderDto().getCredential() != null) {
            return getCustomerOrderDto().getCredential().getDefaultShipFromAddress();
        }
        return fromAddress;
    }

    /**
     * @param fromAddress the fromAddress to set
     */
    public void setFromAddress(AddressDto fromAddress) {
        this.fromAddress = fromAddress;
    }

    /**
     * @return the totalTaxPrice
     */
    public BigDecimal getTotalTaxPrice() {
        return totalTaxPrice;
    }

    /**
     * @param totalTaxPrice the totalTaxPrice to set
     */
    public void setTotalTaxPrice(BigDecimal totalTaxPrice) {
        this.totalTaxPrice = totalTaxPrice;
    }

    public String getRouting() {
        return routing;
    }

    public void setRouting(String routing) {
        this.routing = routing;
    }

    public String getTrackingNumberQualifier() {
        return trackingNumberQualifier;
    }

    public void setTrackingNumberQualifier(String trackingNumberQualifier) {
        this.trackingNumberQualifier = trackingNumberQualifier;
    }

    public String getShippingMethodOfPayment() {
        return shippingMethodOfPayment;
    }

    public void setShippingMethodOfPayment(String shippingMethodOfPayment) {
        this.shippingMethodOfPayment = shippingMethodOfPayment;
    }

    public BigInteger getInnerPacks() {
        return innerPacks;
    }

    public void setInnerPacks(BigInteger innerPacks) {
        this.innerPacks = innerPacks;
    }

    public BigInteger getPacks() {
        return packs;
    }

    public void setPacks(BigInteger packs) {
        this.packs = packs;
    }

    /**
     * DO NOT EXPOSE PUBLICLY
     * Used to lookup a shipment uniquely within workflow only.
     * @return the shipmentDatabaseKey
     */
    @Deprecated
    public Long getShipmentDatabaseKey() {
        return shipmentDatabaseKey;
    }

    /**
     * @param shipmentDatabaseKey the shipmentDatabaseKey to set
     */
    public void setShipmentDatabaseKey(Long shipmentDatabaseKey) {
        this.shipmentDatabaseKey = shipmentDatabaseKey;
    }

}
