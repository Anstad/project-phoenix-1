package com.apd.phoenix.service.utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;
import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class allows the results in a search bean or a Query to be downloaded as a CSV.
 * 
 * @author RHC
 *
 */
@Named
@LocalBean
@Stateless
public class CsvExportService implements DataExportService {

    private static final Logger LOG = LoggerFactory.getLogger(CsvExportService.class);

    @Override
    public File getFile(String[] columns, List<Object[]> results) {
        //TODO: filename
        File file = new File("/var/tmp/results.csv");
        if (file.exists()) {
            file.delete();
        }
        try (
        		BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
        	) {
            //creates the file
            file.createNewFile();
            //writes the CSV header
            for (String s : columns) {
                bw.write(StringEscapeUtils.escapeCsv(s));
                bw.write(",");
            }
            bw.write("\n");

            //writes each row
            this.writeResults(columns, results, bw);

        }
        catch (IOException e) {
        	LOG.error(e.getMessage());
        }
        return file;
    }

    public File getFile(List<?> results, Class<?> dto) throws IllegalArgumentException, IllegalAccessException {
        //TODO: filename
    	Field[] fields = dto.getDeclaredFields();
    	List<String> columns = new ArrayList<>();
    	for (Field field : fields) {
    		columns.add(field.getName());
    	}
    	
        File file = new File("/var/tmp/results.csv");
        if (file.exists()) {
            file.delete();
        }
        try (
        		BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
        	) {
            //creates the file
            file.createNewFile();
            //writes the CSV header
            for (String s : columns) {
                bw.write(StringEscapeUtils.escapeCsv(s));
                bw.write(",");
            }
            bw.write("\n");

            //writes each row
            for(Object obj : results) {
            	for(Field field : fields) {
            		field.setAccessible(true);
            		Object o = field.get(obj);
            		bw.write(StringEscapeUtils.escapeCsv(o == null ? "" : o.toString()));
                    bw.write(",");
            	}
            	bw.write("\n");
            }

        }
        catch (IOException e) {
        	LOG.error(e.getMessage());
        }
        return file;
    }

    /**
     * This method takes an array of columns, rows, and the bufferedwriter, and writes the rows of the results 
     * to the bufferedwriter.
     * 
     * @param columns
     * @param results
     * @param bw
     * @throws Exception
     */
    private void writeResults(String[] columns, List<Object[]> results, BufferedWriter bw) throws IOException {
        for (Object[] oArray : results) {
            for (Object o : oArray) {
                bw.write(StringEscapeUtils.escapeCsv(o == null ? "" : o.toString()));
                bw.write(",");
            }
            bw.write("\n");
        }
    }
}
