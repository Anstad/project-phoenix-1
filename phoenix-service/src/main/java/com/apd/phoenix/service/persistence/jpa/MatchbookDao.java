package com.apd.phoenix.service.persistence.jpa;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.Manufacturer;
import com.apd.phoenix.service.model.Matchbook;

/**
 * Matchbook DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class MatchbookDao extends AbstractDao<Matchbook> {

    @Inject
    private CatalogDao catalogDao;

    @SuppressWarnings("unchecked")
	public Set<Manufacturer> matchbookManufacturersFromCatalog(Catalog catalog) {
		Set<Manufacturer> setToReturn = new HashSet<>();
		catalog = catalogDao.findById(catalog.getId(), Catalog.class);
		
		while (catalog != null) {

	        String hql = "SELECT DISTINCT matchbook.manufacturer FROM CatalogXItem cxi JOIN cxi.item.matchbook AS "
	        		+ "matchbook WHERE cxi.catalog.id = :catalogId AND cxi.id NOT IN (SELECT subselectItem.id FROM CatalogXItem subselectItem "
	        		+ "JOIN subselectItem.properties AS property WHERE subselectItem.catalog.id = :catalogId AND property.type.name "
	        		+ "= 'availability' AND property.value = 'no')";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalog.getId());
	        catalog = catalog.getParent();

	        setToReturn.addAll((List<Manufacturer>) query.getResultList());
		}
		return setToReturn;
	}

    @SuppressWarnings("unchecked")
	public Set<Matchbook> searchByCatalogAndManufacturer(Catalog catalog, Manufacturer manufacturer) {
		Set<Matchbook> setToReturn = new HashSet<>();
		catalog = catalogDao.findById(catalog.getId(), Catalog.class);
		
		while (catalog != null) {

	        String hql = "SELECT DISTINCT matchbook FROM CatalogXItem cxi JOIN cxi.item.matchbook AS "
	        		+ "matchbook WHERE cxi.catalog.id = :catalogId AND matchbook.manufacturer.id = :manufacturerId AND cxi.id NOT IN "
	        		+ "(SELECT subselectItem.id FROM CatalogXItem subselectItem JOIN subselectItem.properties AS property WHERE subselectItem.catalog.id "
	        		+ "= :catalogId AND property.type.name = 'availability' AND property.value = 'no')";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("catalogId", catalog.getId());
	        query.setParameter("manufacturerId", manufacturer.getId());
	        catalog = catalog.getParent();

	        setToReturn.addAll((List<Matchbook>) query.getResultList());
		}
		return setToReturn;
	}
}
