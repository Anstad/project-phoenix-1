package com.apd.phoenix.service.business;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.hibernate.envers.RevisionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.audit.EntityChange;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.model.CustomRevision;
import com.apd.phoenix.service.model.Entity;
import com.apd.phoenix.service.persistence.jpa.AuditLogDao;
import com.apd.phoenix.service.persistence.jpa.CustomRevisionDao;
import com.google.common.base.Charsets;

@Stateless
public class AuditLogBp {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditLogBp.class);
    private static final String FROM = "no-reply@apdmarketplace.com";
    private static final String DATE_FORMAT = "MM/dd/YYYY";

    @Inject
    AuditLogDao auditLogDao;

    @Inject
    CustomRevisionDao customRevisionDao;

    @Inject
    EmailService emailService;

    @Inject
    EmailFactoryBp emailFactoryBp;

    public List<EntityChange> getChangesByDateRange(Class<? extends Entity> clazz, Date startDate, Date endDate) {
        List<EntityChange> toReturn = new ArrayList<>();
        Map<Long, Object> previousRevision = new HashMap<>();
    	List<Object[]> entitiesChanged = auditLogDao.getEntityByRevision(clazz,startDate,endDate);
    	for (Object[] revisionArray : entitiesChanged){
    		Object object = revisionArray[0];
    		CustomRevision customRevision = (CustomRevision) revisionArray[1];
    		RevisionType revisionType = (RevisionType) revisionArray[2];
    		EntityChange entityChange = new EntityChange();
    		entityChange.setUsername(customRevision.getUsername());
    		entityChange.setTimestamp(customRevision.getRevisionDate());
    		entityChange.setEntityDifferences(new HashMap<String, String[]>());
    		try {
    			Method getter = clazz.getMethod("getId");
				entityChange.setPrimaryKey((long)getter.invoke(object));
			} catch (Exception e) {
				LOGGER.error("Could not determine primary key of object");
				break;
			}
    		for (Field field : clazz.getDeclaredFields()) {
    			field.setAccessible(true);
    			for (Annotation annotation : field.getAnnotations()) {
                    if (annotation.annotationType().getName().equals("javax.persistence.Column")) {
                    	String[] results = getDifferences(object, previousRevision.get(entityChange.getPrimaryKey()), field, revisionType);
                    	if (!results[0].equals(results[1])) {
                    		entityChange.getEntityDifferences().put(field.getName(),results);
                    	}
                    }
    			}
    		}
    		toReturn.add(entityChange);
    		previousRevision.put(entityChange.getPrimaryKey(), object);
    	}
        return toReturn;
    }

    private String[] getDifferences(Object newObject, Object oldObject, Field field, RevisionType revisionType) {
        String[] toReturn = new String[2];
        toReturn[0] = "";
        toReturn[1] = "";
        try {
            Object newValue = field.get(newObject);
            if (newValue != null && (oldObject != null || RevisionType.ADD.equals(revisionType))) {
                toReturn[1] = field.get(newObject).toString();
            }
            if (oldObject != null) {
                Object oldValue = field.get(oldObject);
                if (oldValue != null) {
                    toReturn[0] = oldValue.toString();
                }
            }
        }
        catch (Exception e) {
            LOGGER.error("Could not calculate differences for entity:", e);
        }
        return toReturn;
    }

    @Asynchronous
	public void sendEmail(Class<? extends Entity> clazz, Date startDate,
			Date endDate, String email) {
    	List<EntityChange> result = this.getChangesByDateRange(clazz, startDate, endDate);
    	String subject = clazz.getSimpleName() + " changes from " + getDateFormat().format(startDate) + " to " + getDateFormat().format(endDate);
    	Set<String> emails = new HashSet<>();
    	emails.add(email);
    	Set<String> recipients = emailFactoryBp.parseRecipients(emails);
    	String body = "Attached is the requested changelog for " + clazz.getName();
    	List<Attachment> attachments = new ArrayList<>();
    	String filename = clazz.getSimpleName() + "-" + getDateFormat().format(startDate) + "-" + getDateFormat().format(endDate) + ".csv";
    	attachments.add(this.createCSV(result, filename));
    	emailService.sendEmail(FROM, recipients, subject, body, attachments);
    }

    private Attachment createCSV(List<EntityChange> results, String filename) {
        Attachment attachment = new Attachment();
        attachment.setFileName(filename);
        attachment.setMimeType(MimeType.csv);
        StringBuilder sb = new StringBuilder("primary key, user, timestamp, field, old value, new value");
        for (EntityChange entityChange : results) {
            String row = entityChange.getPrimaryKey() + "," + entityChange.getUsername() + ","
                    + entityChange.getTimestamp();
            for (Entry<String, String[]> entry : entityChange.getEntityDifferences().entrySet()) {
                sb.append("\n");
                sb.append(row + "," + entry.getKey() + "," + entry.getValue()[0] + "," + entry.getValue()[1]);
            }
        }
        InputStream is = new ByteArrayInputStream(sb.toString().getBytes(Charsets.UTF_8));
        attachment.setContent(is);
        return attachment;
    }

    private DateFormat getDateFormat() {
        return new SimpleDateFormat(DATE_FORMAT);
    }
}
