package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.CashoutPageXField;

/**
 * CashoutPageXFieldType DAO stub
 * 
 * @author RHC
 *
 */
/**
 * Session Bean implementation class CashoutPageXFieldTypeDao
 */
@Stateless
@LocalBean
public class CashoutPageXFieldDao extends AbstractDao<CashoutPageXField> {
}
