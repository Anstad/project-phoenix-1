package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.PaymentInformation;

/**
 * Session Bean implementation class PaymentInformationDao
 */
@Stateless
@LocalBean
public class PaymentInformationDao extends AbstractDao<PaymentInformation> {

}
