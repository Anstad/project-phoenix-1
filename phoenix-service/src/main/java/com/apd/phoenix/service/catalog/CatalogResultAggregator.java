package com.apd.phoenix.service.catalog;

import java.util.concurrent.TimeUnit;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBpNoTransaction;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.business.SyncItemResultBp;
import com.apd.phoenix.service.business.SyncItemResultSummaryBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogUpload;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.persistence.jpa.SyncItemResultDao;

@Stateless
public class CatalogResultAggregator {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogResultAggregator.class);

    @Inject
    CatalogUploadBp catalogUploadBp;

    @Inject
    CatalogBp catalogBp;

    @Inject
    SyncItemResultBp syncItemResultBp;

    @Inject
    SyncItemResultDao syncItemResultDao;

    @Inject
    SyncItemResultSummaryBp syncItemResultSummaryBp;

    @Inject
    CatalogBpNoTransaction catalogBpNoTransaction;

    public void process(CatalogChangeResult catalogChangeResult) {
        SyncItemResult syncItemResult = catalogChangeResult.getResult();
        SyncItemResultSummary syncItemResultSummary = catalogChangeResult.getSummary();
        String correlationId = catalogChangeResult.getCorrelationId();
        SyncAction action = catalogChangeResult.getAction();

        Boolean newUpload = !catalogUploadBp.existsByCorrelationId(correlationId);

        syncItemResult.setCorrelationId(correlationId);
        syncItemResultSummary.setCorrelationId(correlationId);

        syncItemResult = syncItemResultDao.create(syncItemResult);
        syncItemResultSummary = syncItemResultSummaryBp.create(syncItemResultSummary);

        if (newUpload) {
            CatalogUpload catalogUpload = new CatalogUpload();
            catalogUpload.setCatalogId(catalogChangeResult.getCatalogId());
            catalogUpload.setOldCatalogId(catalogChangeResult.getOldCatalogId());
            catalogUpload.setCorrelationId(correlationId);
            catalogUpload.setProcessedItems(1L);
            catalogUpload.setAction(action);
            catalogUpload.setTotalItems(catalogChangeResult.getTotalItems());
            catalogUpload.setPersistChanges(catalogChangeResult.getPersistChanges());
            catalogUploadBp.create(catalogUpload);
        }
    }

    @Asynchronous
    @TransactionTimeout(unit = TimeUnit.HOURS, value = 2)
    public void finished(String correlationId) {
        LOGGER.info("Aggregating for {}....", correlationId);
        CatalogUpload catalogUpload = catalogUploadBp.getByCorrelationId(correlationId);
        Catalog catalog = catalogBp.findById(catalogUpload.getCatalogId(), Catalog.class);
        if (catalog != null) {
            Catalog parent = catalog.getParent();
            SyncItemResultSummary summary = syncItemResultSummaryBp.getSummary(correlationId);
            catalogBpNoTransaction.postParseCsv(catalog, catalogUpload.getAction(), correlationId, summary, parent,
                    catalogUpload.getPersistChanges());
            if (catalogUpload.getPersistChanges()) {
                catalogBp.deleteDiffCsv(catalog);
            }
            catalog = catalogBp.update(catalog);
        }
        catalogUploadBp.markAsFinished(correlationId);
        LOGGER.info("Finished aggreagating for {}", correlationId);
    }
}
