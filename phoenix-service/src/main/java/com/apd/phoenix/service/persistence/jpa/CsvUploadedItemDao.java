package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.CsvUploadedItem;

/**
 * Address DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CsvUploadedItemDao extends AbstractDao<CsvUploadedItem> {

    private static final int BATCH_SIZE = 100;

    /**
     * Takes an AccountXCredentialXUser and an address type, and returns a List of Addresses with that type. If the 
     * User has at least one address of that type, returns the addresses that the user has with that type; otherwise, 
     * returns the addresses that the account has with that type.
     * 
     * @param axcxu
     * @param addressType
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<CsvUploadedItem> uploadedItems() {
        String hql = "SELECT uploadedItem FROM CsvUploadedItem uploadedItem";

        //Creates the query
        Query query = entityManager.createQuery(hql);

        this.setItemsReturned(query, 0, BATCH_SIZE);

        return (List<CsvUploadedItem>) query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<String> uploadedItemFields() {
        String hql = "SELECT DISTINCT field.key FROM CsvUploadedItemField field";

        //Creates the query
        Query query = entityManager.createQuery(hql);
        return (List<String>) query.getResultList();
    }
}
