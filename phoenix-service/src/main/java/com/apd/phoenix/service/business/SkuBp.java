package com.apd.phoenix.service.business;

import java.io.File;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.persistence.jpa.SkuDao;

/**
 * This class provides business process methods for Sku.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class SkuBp extends AbstractBp<Sku> {

    @Inject
    SkuTypeBp skuTypeBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(SkuDao dao) {
        this.dao = dao;
    }

    public List<Sku> skuList(String catalogName, String skuType, String skuValue, String keyword, int start,
            int quantity) {
        return ((SkuDao) this.dao).skuList(catalogName, skuType, skuValue, keyword, start, quantity);
    }

    public File skuCsv(String[] columns, String catalogName, String skuType, String skuValue, String keyword) {
        return ((SkuDao) this.dao).skuCsv(columns, catalogName, skuType, skuValue, keyword);
    }

    public int searchCount(String catalogName, String skuType, String skuValue, String keyword) {
        return ((SkuDao) this.dao).searchCount(catalogName, skuType, skuValue, keyword);
    }

    public List<Object[]> customerSkuList(String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String keyword, int start, int quantity) {
        return ((SkuDao) this.dao).customerSkuList(catalogName, customer, customerCatalogName, customerCatalogIds,
                skuType, skuValue, keyword, start, quantity);
    }

    public File customerSkuCsv(String[] columns, String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String keyword) {
        return ((SkuDao) this.dao).customerSkuCsv(columns, catalogName, customer, customerCatalogName,
                customerCatalogIds, skuType, skuValue, keyword);
    }

    public int customerSearchCount(String catalogName, String customer, String customerCatalogName,
            List<Long> customerCatalogIds, String skuType, String skuValue, String keyword) {
        return ((SkuDao) this.dao).customerSearchCount(catalogName, customer, customerCatalogName, customerCatalogIds,
                skuType, skuValue, keyword);
    }

    public Sku getApdSkuByValue(String value) {
        Sku search = new Sku();
        search.setType(skuTypeBp.getApdSkuType());
        search.setValue(value);
        List<Sku> results = searchByExample(search, 0, 0);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public Sku copyToNewCustomerSku(Sku sku) {
        if (sku == null) {
            return null;
        }
        Sku returnSku = new Sku();
        returnSku.setType(skuTypeBp.getCustomerSkuType());
        returnSku.setValue(sku.getValue());
        return returnSku;
    }
}
