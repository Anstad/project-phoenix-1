package com.apd.phoenix.service.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This utility class is used to store the methods used to allow a user to download a file.
 * 
 * @author RHC
 *
 */
public class FileDownloader {

    private static final Logger logger = LoggerFactory.getLogger(FileDownloader.class);

    /**
     * This method takes a File, and starts the download dialog.
     * 
     * @param file - the file to be downloaded
     * @return
     */
    public static String downloadFile(File file) {
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();

            writeOutContent(response, file, file.getName());

            FacesContext.getCurrentInstance().responseComplete();
            return null;
        }
        catch (Exception e) {
            logger.error("Error downloading file", e);
            return null;
        }
    }

    /**
     * Helper method for downloadFile.
     * 
     * @param res
     * @param content
     * @param fileName
     */
    private static void writeOutContent(final HttpServletResponse res, final File content, final String fileName) {
        if (content == null) {
            return;
        }
        res.setDateHeader("Expires", 0);
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        try (
	        	FileInputStream fis = new FileInputStream(content);
	            ServletOutputStream os = res.getOutputStream();
	        ) {
            int bt = fis.read();
            while (bt != -1) {
                os.write(bt);
                bt = fis.read();
            }
            os.flush();
        }
        catch (final IOException e) {
        	logger.error("Error writing file to servlet response", e);
        }
    }

    public static String downloadFile(InputStream file) {
        return downloadFile(file, "file.csv");
    }

    /**
     * This method takes a File, and starts the download dialog.
     * 
     * @param file - the file to be downloaded
     * @return
     */
    public static String downloadFile(InputStream file, String fileName) {
        try {
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();

            writeOutContent(response, file, fileName);

            FacesContext.getCurrentInstance().responseComplete();
            return null;
        }
        catch (Exception e) {
            logger.error("Error downloading file", e);
            return null;
        }
    }

    /**
     * Helper method for downloadFile.
     * 
     * @param res
     * @param content
     * @param fileName
     */
    private static void writeOutContent(final HttpServletResponse res, InputStream fis, final String fileName) {
        res.setDateHeader("Expires", 0);
        res.setHeader("Content-disposition", "attachment; filename=" + fileName);
        try (
        		ServletOutputStream os = res.getOutputStream();
        		InputStream fisToRead = fis;
	        ) {
        	if (fisToRead != null) {
	            int bt = fisToRead.read();
	            while (bt != -1) {
	                os.write(bt);
	                bt = fisToRead.read();
	            }
        	}
            os.flush();
        }
        catch (final IOException e) {
        	logger.error("Error writing file to servlet response", e);
        }
    }
}
