package com.apd.phoenix.service.brms.api;

import javax.ejb.Stateless;
import org.jbpm.task.TaskService;

@Stateless
public interface TaskServiceClientFactory {

    public TaskService getTaskService();

}
