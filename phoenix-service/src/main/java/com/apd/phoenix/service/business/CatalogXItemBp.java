package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBp.SyncError;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationException;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CatalogXItemXItemPropertyType;
import com.apd.phoenix.service.model.CustomerCost;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.FavoritesList;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.ItemImage;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemXItemPropertyType;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.PricingType;
import com.apd.phoenix.service.model.Sku;
import com.apd.phoenix.service.model.SkuType;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;
import com.apd.phoenix.service.model.Item.ItemStatus;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;
import com.apd.phoenix.service.persistence.jpa.CatalogDao;
import com.apd.phoenix.service.persistence.jpa.CatalogXItemDao;

@Stateless
@LocalBean
public class CatalogXItemBp extends AbstractBp<CatalogXItem> {

    private static final String YES_VALUE = "yes";
    private static final String TRUE = "true";
    private static final String FAVORITES_LISTS = "favorites lists";
    private static final String CATALOG = "catalog";
    private static final String BLANK = "";
    private static final String DELIMITER = ",";
    private static final String CELL_DELIMETER_SPLITTER = "\\|";
    private static final String CELL_DELIMETER = "|";
    private static final String SUBSTITUTE_SKU = "substituteSku";
    private static final String CORE_ITEM_EXPIRATION_DATE = "coreItemExpirationDate";
    private static final String CORE_ITEM_START_DATE = "coreItemStartDate";
    private static final String PRICING_PARAMETER = "pricingParameter";
    private static final String PRICING_TYPE = "pricingType";
    private static final String CUSTOMER_SKUTYPE = "customer";
    private static final String CUSTOMER_SKU = "customerSku";
    private static final String CUSTOMER_HIERARCHY = "customerSkuHierarchy";
    private static final String SUBSTITUTE_VENDOR = "substituteVendor";
    private static final String APD_SKUTYPE = "APD";
    private static final String DELETE_HEADER = "delete";
    private static final String APD_SKUTYPE_HEADER = "APD sku";
    private static final String VENDOR_NAME_HEADER = "vendor";
    private static final String VENDOR_SKUTYPE = "vendor";
    private static final String MANUFACTURER_SKUTYPE = "manufacturer";
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String SKU_SUFFIX = " sku";
    private static final String UNSPSC_CLASSTYPE = "unspsc";
    private static final String NULL_VALUE = "null";
    private static final String STREET = "street";
    private static final String APD_COST = "APDcost";
    private static final String LIST_PRICE = "list price";
    private static final String MANUFACTURER = "manufacturer";
    private static final String UNSPSC_CLASSIFICATIONTYPE = "unspsc";

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogXItemBp.class);

    @Inject
    private CatalogDao catalogDao;

    @Inject
    private SkuBp skuBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CatalogXItemDao dao) {
        this.dao = dao;
    }

    /**
     * This method returns a String storing the row of an element.
     * 
     * @param entry
     * @param propertyTypes
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String csvRow(CatalogXItem entry, List<ItemPropertyType> propertyTypes) {
        if (entry == null) {
            return BLANK;
        }
        Map<String, String> propertyValues = new HashMap<String, String>();
        Map<String, Sku> skuValues = new HashMap<String, Sku>();
        //Stores the properties and Skus in a map that can be retrieved
        for (CatalogXItemXItemPropertyType property : entry.getProperties()) {
            propertyValues.put(property.getType().getName(), property.getValue());
        }
        for (Sku sku : entry.getItem().getSkus()) {
            skuValues.put(sku.getType().getName(), sku);
        }
        //Stores the item information
        StringBuffer outputBuffer = new StringBuffer();
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItem().getVendorCatalog().getVendor().getName()));
        outputBuffer.append(DELIMITER);
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(APD_SKUTYPE).getValue()));
        outputBuffer.append(DELIMITER);
        if (entry.getItem() != null && entry.getItem().getStatus() != null
                && ItemStatus.DISCONTINUED.equals(entry.getItem().getStatus())) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(TRUE));
        }
        outputBuffer.append(DELIMITER);
        if (entry.getCoreItemStartDate() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv((new SimpleDateFormat(DATE_FORMAT)).format(entry
                    .getCoreItemStartDate())));
        }
        outputBuffer.append(DELIMITER);
        if (entry.getCoreItemExpirationDate() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv((new SimpleDateFormat(DATE_FORMAT)).format(entry
                    .getCoreItemExpirationDate())));
        }
        outputBuffer.append(DELIMITER);
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getCatalog().getName()));
        outputBuffer.append(DELIMITER);
        if (entry.getCustomerSku() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getCustomerSku().getValue()));
        }
        outputBuffer.append(DELIMITER);
        //Stores the current price
        if (entry.getPrice() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getPrice().toString()));
        }
        outputBuffer.append(DELIMITER);
        //Stores the pricing type and pricing parameter
        if (entry.getPricingType() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getPricingType().getName()));
        }
        outputBuffer.append(DELIMITER);
        if (entry.getPricingParameter() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getPricingParameter()));
        }
        outputBuffer.append(DELIMITER);
        //Stores the favorites lists
        StringBuffer favoritesBuffer = new StringBuffer();
        for (FavoritesList list : entry.getFavorites()) {
            if (list.getCatalog() != null) {
                if (favoritesBuffer.length() != 0) {
                    favoritesBuffer.append(CELL_DELIMETER);
                }
                favoritesBuffer.append(list.getName());
            }
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(favoritesBuffer.toString()));

        outputBuffer.append(DELIMITER);
        if (entry.getItem() != null && entry.getItem().getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItem().getItemCategory().getName()));
        }

        if (entry.getItem() != null && entry.getItem().getPropertiesReadOnly() != null) {

            if (entry.getItem().getPropertyReadOnly(APD_COST) != null) {
                outputBuffer.append(DELIMITER);
                outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItem().getPropertyReadOnly(APD_COST)
                        .getValue()));
            }

            if (entry.getItem().getPropertyReadOnly(LIST_PRICE) != null) {
                outputBuffer.append(DELIMITER);
                outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getItem().getPropertyReadOnly(LIST_PRICE)
                        .getValue()));
            }

            if (entry.getItem().getPropertyReadOnly(STREET) != null) {
                outputBuffer.append(DELIMITER);
                outputBuffer
                        .append(StringEscapeUtils.escapeCsv(entry.getItem().getPropertyReadOnly(STREET).getValue()));
            }
        }

        //Stores the properties
        for (ItemPropertyType type : propertyTypes) {
            outputBuffer.append(DELIMITER);
            if (propertyValues.containsKey(type.getName())) {
                outputBuffer.append(StringEscapeUtils.escapeCsv(propertyValues.get(type.getName())));
            }
        }
        return outputBuffer.toString();
    }

    @Inject
    private ItemBp itemBp;

    @Inject
    private PricingTypeBp pricingTypeBp;

    @Inject
    private ItemPropertyTypeBp propertyTypeBp;

    @Inject
    private SkuTypeBp skuTypeBp;

    @Inject
    private FavoritesListBp listBp;

    /**
     * Executes a synchronization operation on a catalog item.
     * @param itemData
     * @param catalogOfOriginal
     * @param action
     * @param summary
     * @param persistChanges - a boolean indicating whether the changes should be persisted
     * 
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public SyncItemResult syncItem(Map<String, String> itemData, Catalog catalog, Catalog catalogOfOriginal,
            SyncAction action, SyncItemResultSummary summary, boolean persistChanges) {
        //Finds the item that matches the one specified by the APD sku and the vendor
        String apdSku = itemData.get(APD_SKUTYPE_HEADER);
        String vendorName = itemData.get(VENDOR_NAME_HEADER);
        SyncItemResult toReturn = new SyncItemResult();
        toReturn.setResultApdSku(apdSku);
        toReturn.setResultVendor(vendorName);
        List<Item> searchResults = new ArrayList<>();
        if (StringUtils.isNotBlank(apdSku) && StringUtils.isNotBlank(vendorName)) {
        	searchResults = itemBp.searchByExactExample(itemBp.searchItem(apdSku, vendorName), 0, 0);
        }
        if (searchResults.size() == 0) {
            //If no such item exists, returns an error
            toReturn.getErrors().add(CatalogBp.SyncError.ITEM_NOT_FOUND.getLabel());
            CatalogXItem result = new CatalogXItem();
            result.setItem(itemBp.searchItem(apdSku, vendorName));
            toReturn.setResultApdSku(apdSku);
            toReturn.setResultVendor(vendorName);
            summary.incrementFailures();
            summary.incrementErrors();
            return toReturn;
        }
        Item toLink = searchResults.get(0);
        //Finds the previous version of the object, in the case of an update or new child
        CatalogXItem previous = new CatalogXItem();
        CatalogXItem result = new CatalogXItem();

        if (action != CatalogBp.SyncAction.NEW && action != CatalogBp.SyncAction.REPLACE) {
            CatalogXItem searchItem = new CatalogXItem();
            searchItem.setItem(new Item());
            searchItem.getItem().setId(toLink.getId());
            searchItem.setCatalog(new Catalog());
            searchItem.getCatalog().setId(catalogOfOriginal.getId());
            List<CatalogXItem> existingRelationships = this.searchByExactExample(searchItem, 0, 0);
            if (existingRelationships.size() != 0) {
                previous = this.findById(existingRelationships.get(0).getId(), CatalogXItem.class);
            }
        }
        //In the case of an update, the previous version is also the version being updated.
        if (action == CatalogBp.SyncAction.UPDATE) {
            result = previous;
        }
        result.setItem(toLink);
        Map<String, Sku> skuMap = new HashMap<String, Sku>();
        Map<String, CustomerCost> costMap = new HashMap<String, CustomerCost>();
        Map<String, ItemXItemPropertyType> itemPropertyMap = new HashMap<String, ItemXItemPropertyType>();
        Map<String, CatalogXItemXItemPropertyType> previousPropertyMap = new HashMap<String, CatalogXItemXItemPropertyType>();
        //places the SKUs, customer costs, item properties, and catalogXItem properties into a map
        for (Sku sku : toLink.getSkus()) {
            skuMap.put(sku.getType().getName() + SKU_SUFFIX, sku);
        }
        toLink.getCustomerCosts().size();
        for (CustomerCost cost : toLink.getCustomerCosts()) {
            costMap.put(cost.getCustomer().getName(), cost);
        }
        toLink.getPropertiesReadOnly().size();
        for (ItemXItemPropertyType property : toLink.getPropertiesReadOnly()) {
            itemPropertyMap.put(property.getType().getName(), property);
        }
        previous.getProperties().size();
        for (CatalogXItemXItemPropertyType property : previous.getProperties()) {
            previousPropertyMap.put(property.getType().getName(), property);
        }

        //Sets the different parameters to the values in the previous version
        result.setCoreItemExpirationDate(previous.getCoreItemExpirationDate());
        result.setCoreItemStartDate(previous.getCoreItemStartDate());
        result.setPricingParameter(previous.getPricingParameter());
        result.setPricingType(previous.getPricingType());
        
        //flag indicating whether the item should be deleted
        boolean delete = false;

        for (String key : itemData.keySet()) {
            if (StringUtils.isBlank(itemData.get(key)) || key.equals(CATALOG) || key.equals(APD_SKUTYPE_HEADER) || key.equals(VENDOR_NAME_HEADER)) {
                //do nothing
            }
            else if (key.equals(CORE_ITEM_START_DATE) || key.equals(CORE_ITEM_EXPIRATION_DATE)) {
                try {
                	Date date;
                	if (itemData.get(key).equals(NULL_VALUE)) {
                		date = null;
                	}
                	else {
                		date = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(itemData.get(key));
                	}
                    if (key.equals(CORE_ITEM_START_DATE)) {
                        result.setCoreItemStartDate(date);
                    }
                    else {
                        result.setCoreItemExpirationDate(date);
                    }
                }
                catch (ParseException e) {
                    toReturn.getWarnings().add(CatalogBp.SyncError.DATE_FORMAT.getLabel());
                    summary.incrementWarnings();
                    continue;
                }
            }
            else if (key.equals(PRICING_PARAMETER)) {
                //set when pricing type is set
            }
            else if (key.equals(PRICING_TYPE)) {
            	if (itemData.get(key).equals(NULL_VALUE)) {
            		result.setPricingType(null);
            		result.setPricingParameter(null);
            		continue;
            	}
                PricingType type = pricingTypeBp.getTypeByName(itemData.get(key));
                if (type == null) {
                    toReturn.getErrors().add(CatalogBp.SyncError.PRICING_TYPE_NOT_FOUND.getLabel());
                    summary.incrementErrors();
                    continue;
                }
                String pricingParameter = result.getPricingParameter();
                if (itemData.containsKey(PRICING_PARAMETER)) {
                    pricingParameter = itemData.get(PRICING_PARAMETER);
                }
                if (pricingParameter != null && pricingParameter.matches(type.getParameterRegEx())) {
                    result.setPricingType(type);
                    result.setPricingParameter(pricingParameter);
                }
                else {
                    toReturn.getErrors().add(SyncError.BAD_PRICING_PARAMETER.getLabel());
                    summary.incrementErrors();
                }
            }
            else if (key.equals(CUSTOMER_SKU)) {
            	if (itemData.get(key).equals(NULL_VALUE)) {
            		result.setCustomerSku(null);
            		continue;
            	}
                //If the previous version doesn't have a customer sku, or it's set to a different value
                if (previous.getCustomerSku() == null
                        || !previous.getCustomerSku().getValue().equals(itemData.get(key))) {
                	//searches for items with this customer SKU in this catalog and parent catalogs
                	List<Long> idList = catalogDao.getParentCatalogIdList(catalog.getId());
                	idList.add(catalog.getId());
                	for (Long catalogId : idList) {
                		CatalogXItem searchItem = new CatalogXItem();
                		searchItem.setCatalog(new Catalog());
                		searchItem.getCatalog().setId(catalogId);
                		searchItem.setCustomerSku(new Sku());
                		searchItem.getCustomerSku().setValue(itemData.get(key));
                		List<CatalogXItem> searchList = this.searchByExactExample(searchItem, 0, 0);
                		for (CatalogXItem item : searchList) {
                			//if one of the items in one of the catalogs has the same customer SKU and 
                			//different item ID, it's a collision that will not be overridden, so fails
                			if (item != null && item.getId() != null && item.getId() != result.getId() 
                					&& item.getItem() != null && item.getItem().getId() != null && item.getItem().getId() != result.getItem().getId()) {
                                toReturn.getErrors().add(SyncError.DUPLICATE_CUSTOMER_SKU.getLabel());
                                summary.incrementErrors();
                                continue;
                			}
                		}
                	}
                    //creates a new SKU with this value
                    SkuType searchType = new SkuType();
                    searchType.setName(CUSTOMER_SKUTYPE);
                    List<SkuType> typeList = skuTypeBp.searchByExactExample(searchType, 0, 0);
                    if (typeList.size() == 0) {
                        toReturn.getWarnings().add(SyncError.SKU_TYPE_NOT_FOUND.getLabel());
                        summary.incrementWarnings();
                    }
                    else {
                        Sku newSku = new Sku();
                        newSku.setType(typeList.get(0));
                        newSku.setValue(itemData.get(key));
                        result.setCustomerSku(newSku);
                    }
                }
            }
            else if (key.equals(SUBSTITUTE_VENDOR)) {
                //do nothing; handled in the "substituteSku" case
            }
            else if (key.equals(SUBSTITUTE_SKU)) {
            	if (itemData.get(key).equals(NULL_VALUE)) {
            		this.clearSubstitute(result);
            		continue;
            	}
                //stores the substitute's sku and vendor in the wrapper
                this.linkItems(result, itemData.get(key), itemData.get(SUBSTITUTE_VENDOR),
                        catalog.getId(), toReturn, summary);
            }
            else if (key.equals(CUSTOMER_HIERARCHY)) {
                //do nothing; handled in the "customer sku" case
            }
            else if (key.equals(FAVORITES_LISTS)) {
            	if (itemData.get(key).equals(NULL_VALUE)) {
            		itemData.put(key, "");
            	}
                String listString = itemData.get(key);
                //splits the cell to get the favorites lists
                String[] nameArray = listString.split(CELL_DELIMETER_SPLITTER);
                Set<String> newListSet = new HashSet<>();
                for (String name : nameArray) {
                	if (StringUtils.isNotBlank(name)) {
                		newListSet.add(name);
                	}
                }
                //removes any favoritesList that shouldn't be in the new set,
                //and removes any name from the new set that the item is already in the list of
                List<FavoritesList> listsToRemove = new ArrayList<>();
                for (FavoritesList list : result.getFavorites()) {
                	if (!newListSet.contains(list.getName()) && list.getCatalog() != null) {
                		listsToRemove.add(list);
                	} else {
                		newListSet.remove(list.getName());
                	}
                }
                for (FavoritesList list : listsToRemove) {
                	result.getFavorites().remove(list);
                }
                //then for each quote-separated string, finds (or creates) the list 
                for (String name : newListSet) {
                    FavoritesList searchList = new FavoritesList();
                    searchList.setName(name);
                    searchList.setCatalog(new Catalog());
                    searchList.getCatalog().setId(catalog.getId());
                    List<FavoritesList> list = listBp.searchByExactExample(searchList, 0, 0);
                    if (!list.isEmpty()) {
                        result.getFavorites().add(list.get(0));
                    }
                    else {
                        FavoritesList newList = new FavoritesList();
                        newList.setCatalog(catalog);
                        newList.setName(name);
                        result.getFavorites().add(newList);
                    }
                }
            }
            else if (key.equals(DELETE_HEADER)) {
            	if (StringUtils.isNotBlank(itemData.get(key)) && itemData.get(key).equals(YES_VALUE)) {
            		delete = true;
            	}
            }
            else {
                //processes Item properties
                ItemPropertyType thisPropertyType = propertyTypeBp.getPropertyType(key);
                if (thisPropertyType == null) {
                    toReturn.getWarnings().add(CatalogBp.SyncError.PROPERTY_TYPE_NOT_FOUND.getLabel() + ": " + key);
                    summary.incrementWarnings();
                    continue;
                }
                //if the previous version contains the property and we're updating, makes changes to the property
                if (previousPropertyMap.containsKey(key) && action.equals(CatalogBp.SyncAction.UPDATE)) {
                    previousPropertyMap.get(key).setValue(itemData.get(key));
                    if (itemData.get(key).equals(NULL_VALUE)) {
                    	previousPropertyMap.remove(key);
                    }
                }
                else if ((previousPropertyMap.containsKey(key) && itemData.get(key).equals(
                        previousPropertyMap.get(key).getValue()))
                        || (!previousPropertyMap.containsKey(key) && itemPropertyMap.containsKey(key) && itemData.get(
                                key).equals(itemPropertyMap.get(key).getValue()))) {
                    //do nothing if:
                    // -the previous version has the property, and the values are equal
                    //OR
                    // -the previous version doesn't have the property, the item does have the property, and the values are equal
                }
                else if (!itemData.get(key).equals(NULL_VALUE)) {
                    //Create a new property
                    CatalogXItemXItemPropertyType newProperty = new CatalogXItemXItemPropertyType();
                    newProperty.setType(thisPropertyType);
                    newProperty.setValue(itemData.get(key));
                    result.getProperties().add(newProperty);
                }
            }
        }
        //sets the catalog of the item, and calculates the price
        result.setCatalog(catalog);
        try {
            this.calculatePrice(result);
        }
        catch (PricingCalculationException e) {
            toReturn.getErrors().add(CatalogBp.SyncError.CANT_CALCULATE_PRICE.getLabel());
            summary.incrementErrors();
        }
        if (!itemData.keySet().contains(CATALOG) || !itemData.get(CATALOG).equals(result.getCatalog().getName())) {
            toReturn.getErrors().add(CatalogBp.SyncError.CATALOG_INCORRECTLY_SPECIFIED.getLabel());
            summary.incrementErrors();
        }
        if (result.getCustomerSku() == null) {
            toReturn.getErrors().add(CatalogBp.SyncError.CUSTOMER_SKU_NOT_ASSIGNED.getLabel());
            summary.incrementErrors();
        }
        //if changes should be made
        if (toReturn.getErrors().isEmpty() && persistChanges) {
            if (result.getId() != null) {
                //if the entity will be updated, makes the update
                summary.incrementUpdated();
                this.update(result);
                if (delete) {
                	//if the "delete" column is set to "yes", deletes the item
                    toReturn.getErrors().add(CatalogBp.SyncError.DELETED.getLabel());
                    summary.incrementWarnings();
                    this.delete(result.getId(), CatalogXItem.class);
                }
            }
            else if (!delete) {
                //if it will be created, and wasn't marked for deletion, creates the entity
                summary.incrementCreated();
                this.createNoRefresh(result);
            }
            if (result.getPrice().equals(BigDecimal.ZERO)) {
            	summary.incrementZeroPrice();
            }
        }
        else {
        	if (persistChanges) {
	        	//if can't make changes, but changes were intended, marks as failure
	        	summary.incrementFailures();
        	}
        	//rolls back changes
        	ItemRollbackException exception = new ItemRollbackException();
        	exception.setResult(toReturn);
        	throw exception;
        }
        return toReturn;
    }

    /**
     * Calculates the price of a CatalogXItem
     * 
     * @param toCalculate
     * @throws PricingCalculationException 
     */
    public CatalogXItem calculatePrice(CatalogXItem toCalculate) throws PricingCalculationException {
        return itemBp.calculatePrice(toCalculate);
    }

    public void setSubstitute(CatalogXItem original, CatalogXItem substitute) {
        if (substitute == null || original == null) {
            return;
        }
        String substituteSku = "";
        for (Sku sku : substitute.getItem().getSkus()) {
            if (sku.getType().getName().equals(APD_SKUTYPE)) {
                substituteSku = sku.getValue();
            }
        }
        this.linkItems(original, substituteSku, substitute.getItem().getVendorCatalog().getVendor().getName(), original
                .getCatalog().getId(), new SyncItemResult(), new SyncItemResultSummary());
        this.setProperty(original, SUBSTITUTE_VENDOR, substitute.getItem().getVendorCatalog().getVendor().getName());
    }

    public void clearSubstitute(CatalogXItem toClear) {
        toClear.setSubstituteItem(null);
        CatalogXItemXItemPropertyType substituteVendorProperty = null;
        CatalogXItemXItemPropertyType substituteSkuProperty = null;
        for (CatalogXItemXItemPropertyType property : toClear.getProperties()) {
            if (property.getType().getName().equals(SUBSTITUTE_VENDOR)) {
                substituteVendorProperty = property;
            }
            if (property.getType().getName().equals(SUBSTITUTE_SKU)) {
                substituteSkuProperty = property;
            }
        }
        if (substituteVendorProperty != null) {
            toClear.getProperties().remove(substituteVendorProperty);
        }
        if (substituteSkuProperty != null) {
            toClear.getProperties().remove(substituteSkuProperty);
        }
    }

    /**
     * Sets the substitute of an item, storing the result in the CsvLineResult.
     * 
     * @param skuOfOriginal
     * @param vendorOfOriginal
     * @param skuOfNew
     * @param vendorOfNew
     * @param result
     * @param catalogId
     */
    private void linkItems(CatalogXItem original, String skuOfNew, String vendorOfNew, Long catalogId,
            SyncItemResult result, SyncItemResultSummary summary) {
        CatalogXItem newSearch = new CatalogXItem();
        newSearch.setItem(itemBp.searchItem(skuOfNew, vendorOfNew));
        newSearch.setCatalog(new Catalog());
        newSearch.getCatalog().setId(catalogId);
        List<CatalogXItem> newSearchList = this.searchByExactExample(newSearch, 0, 0);
        if (newSearchList.size() == 0) {
            result.getWarnings().add(CatalogBp.SyncError.ITEM_NOT_FOUND.getLabel());
            summary.incrementWarnings();
            return;
        }
        CatalogXItem newItem = newSearchList.get(0);
        original.setSubstituteItem(newItem);
        this.setProperty(original, SUBSTITUTE_VENDOR, vendorOfNew);
        this.setProperty(original, SUBSTITUTE_SKU, skuOfNew);
        return;
    }

    /**
     * Takes a vendor name, and returns all CatalogXItem objects whose items have been replaced by a SKU change. 
     * Specifically, it returns a List of Object[] arrays, where each element in the List is an Array of 2 objects: 
     * a CatalogXItem whose Item has been replaced by a SKU change, and the Item that is the replacement.
     * 
     * @param vendorName - the name of the vendor whose products had a SKU change. 
     * @return
     */
    public List<Object[]> skuQueryResult(String vendorName) {
        return ((CatalogXItemDao) this.dao).skuQueryResult(vendorName);
    }

    public CatalogXItem hydrateForItemBrowse(CatalogXItem toHydrate) {
        return ((CatalogXItemDao) this.dao).hydrateForItemBrowse(toHydrate);
    }

    public CatalogXItem hydrateForProductDetails(CatalogXItem toHydrate) {
        return ((CatalogXItemDao) this.dao).hydrateForProductDetails(toHydrate);
    }

    public void refresh(CatalogXItem toRefresh) {
        ((CatalogXItemDao) this.dao).refresh(toRefresh);
    }

    /**
     * This method takes a catalogXItem, and returns a LineItem of that item.
     * 
     * @param toRead
     * @return
     */
    public LineItem getFromCatalogXItem(CatalogXItem toRead) {
        LineItem lineItem = new LineItem();
        toRead = this.hydrateForLineItem(toRead);
        Sku apdSku = null;
        for (Sku sku : toRead.getItem().getSkus()) {
            if (sku.getType().getName().equals(APD_SKUTYPE)) {
                lineItem.setApdSku(sku.getValue());
                apdSku = sku;
            }
            if (sku.getType().getName().equals(VENDOR_SKUTYPE)) {
                lineItem.setSupplierPartId(sku.getValue());
            }
            if (sku.getType().getName().equals(MANUFACTURER_SKUTYPE)) {
                lineItem.setManufacturerPartId(sku.getValue());
            }
            if (sku.getType().getName().equals(CUSTOMER_SKUTYPE)) {
                if (toRead.getCustomerSku() == null) {
                    logMisplacedCustomerSku(toRead, apdSku, sku);
                }
                else {
                    /*
                     * Do nothing. The customer sku shouldn't be stored on the vendor item 
                     * and the customer sku on the catalogXItem is used as the customer sku below.
                     */
                }
            }
        }
        lineItem.setCustomerSku(skuBp.copyToNewCustomerSku(toRead.getCustomerSku()));
        if ((lineItem.getCustomerSku() == null || lineItem.getCustomerSku().getValue().isEmpty()) && apdSku != null) {
            lineItem.setCustomerSku(skuBp.copyToNewCustomerSku(apdSku));
        }
        lineItem.setCategory(toRead.getItem().getItemCategory());
        Date today = new Date();
        lineItem.setCore(toRead.getCoreItemStartDate() != null && toRead.getCoreItemStartDate().before(today)
                && toRead.getCoreItemExpirationDate() != null && toRead.getCoreItemExpirationDate().after(today));
        lineItem.setCost(toRead.getApdCost());
        lineItem.setDescription(toRead.getItem().getDescription());
        lineItem.setItem(toRead.getItem());
        if (toRead.getItem().getManufacturer() != null) {
            lineItem.setManufacturerName(toRead.getItem().getManufacturer().getName());
        }
        lineItem.setShortName(toRead.getItem().getName());
        lineItem.setUnitOfMeasure(toRead.getItem().getUnitOfMeasure());
        lineItem.setUnitPrice(toRead.getPrice());
        lineItem.setVendor(toRead.getItem().getVendorCatalog().getVendor());

        if (toRead.getProperties() != null) {
            for (CatalogXItemXItemPropertyType property : toRead.getProperties()) {
                if (property.getType().getName().toLowerCase().equals(UNSPSC_CLASSTYPE)) {
                    lineItem.setUnspscClassification(property.getValue());
                }
            }
        }
        if (StringUtils.isEmpty(lineItem.getUnspscClassification())) {
            lineItem.setUnspscClassification(toRead.getItem().getCommodityCode());
        }
        if (StringUtils.isEmpty(lineItem.getUnspscClassification())) {
            for (ItemClassificationType type : toRead.getItem().getClassifications().keySet()) {
                if (type.getName().toLowerCase().equals(UNSPSC_CLASSTYPE)) {
                    lineItem.setUnspscClassification(toRead.getItem().getClassifications().get(type));
                }
            }
        }
        return lineItem;
    }

    private void logMisplacedCustomerSku(CatalogXItem toRead, Sku apdSku, Sku customerSkuOnVendorItem) {
        String errorMsg = "Customer Item with customer sku " + customerSkuOnVendorItem.getValue()
                + ", on customer catalog: " + (toRead.getCatalog() == null ? "unknown" : toRead.getCatalog().getName())
                + ", erroniously stores customer sku on vendor item instead of customer item. ";
        if (apdSku == null) {
            LOG.error(errorMsg + "WILL NOT SET CUSTOMER SKU.");
        }
        else {
            if (apdSku.getValue().equals(customerSkuOnVendorItem.getValue())) {
                LOG.warn(errorMsg + "Will set customer sku to apd sku: " + apdSku.getValue() + ".");
            }
            else {
                LOG.error(errorMsg + "Will set customer sku to apd sku: " + apdSku.getValue() + ".");
            }
        }
    }

    public CatalogXItem hydrateForLineItem(CatalogXItem toHydrate) {
        return ((CatalogXItemDao) this.dao).hydrateForLineItem(toHydrate);
    }

    public List<Integer> getQuantiesForOrder(CustomerOrder order) {
        return ((CatalogXItemDao) this.dao).getQuantitiesForOrder(order);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CatalogXItem> batchForCsv(Catalog catalog, int batch) {
        return ((CatalogXItemDao) this.dao).batchForCsv(catalog, batch);
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<CatalogXItem> batchForPrice(Catalog catalog, int batch) {
        return ((CatalogXItemDao) this.dao).batchForPrice(catalog, batch);
    }

    public List<CatalogXItem> getCatalogXItemsSimilarItems(CatalogXItem thisCXItem) {
        return ((CatalogXItemDao) this.dao).getCatalogXItemsSimilarItems(thisCXItem, 0);
    }

    public List<CatalogXItem> getCatalogXItemsSimilarItems(CatalogXItem thisCXItem, int maxResults) {
        return ((CatalogXItemDao) this.dao).getCatalogXItemsSimilarItems(thisCXItem, maxResults);
    }

    public CatalogXItem getCatalogXItem(String vendorName, String apdSku, long catalogId) {
        Item resultItem = itemBp.searchItem(apdSku, vendorName);
        CatalogXItem toReturn = ((CatalogXItemDao) this.dao).getCatalogXItem(resultItem, catalogId);
        if (toReturn == null) {
            toReturn = this.checkParentCatalogs(vendorName, apdSku, catalogId);
        }
        return toReturn;
    }

    private CatalogXItem checkParentCatalogs(String vendorName, String apdSku, long catalogId) {
        List<Long> catalogIdList = catalogDao.getParentCatalogIdList(catalogId);
        List<CatalogXItem> catxItems = new ArrayList<>();
        for (int i = 0; i < catalogIdList.size(); i++) {
            Long id = catalogIdList.get(i);
            Item resultItem = itemBp.searchItem(apdSku, vendorName);
            CatalogXItem catalogXItem = new CatalogXItem();
            catalogXItem.setItem(resultItem);
            catalogXItem.setCatalog(new Catalog());
            catalogXItem.getCatalog().setId(id);
            catxItems = this.searchByExactExample(catalogXItem, 0, 0);
            if (!catxItems.isEmpty()) {
                break;
            }
        }
        if (catxItems.isEmpty()) {
            return null;
        } else {
            return catxItems.get(0);
        }  
    }

    private void setProperty(CatalogXItem item, String propertyType, String value) {
        for (CatalogXItemXItemPropertyType property : item.getProperties()) {
            if (property.getType().getName().equals(propertyType)) {
                property.setValue(value);
                return;
            }
        }
        ItemPropertyType availabilityType = propertyTypeBp.getPropertyType(propertyType);
        CatalogXItemXItemPropertyType newProperty = new CatalogXItemXItemPropertyType();
        newProperty.setType(availabilityType);
        newProperty.setValue(value);
        item.getProperties().add(newProperty);
    }

    public List<Long> batchForJMS(Catalog input, int batch) {
        return ((CatalogXItemDao) this.dao).batchForJMS(input, batch);
    }

    public List<Long> batchAllForJMS(int batch) {
        return ((CatalogXItemDao) this.dao).batchAllForJMS(batch);
    }

    private ArrayList<CatalogXItemDto> getUspsChangedItemsBatch(List<Long> ids) {
    	ArrayList<CatalogXItemDto> toReturn = new ArrayList<>();
        for(Long id : ids){
        	CatalogXItem managedEntity = this.findById(id, CatalogXItem.class);
            //Entities who are in this list because child elements have changed should be marked as modified
            if((!Boolean.TRUE.equals(managedEntity.getAdded()))&&(!Boolean.TRUE.equals(managedEntity.getDeleted()))&&(!Boolean.TRUE.equals(managedEntity.getModified()))){
            	managedEntity.setModified(Boolean.TRUE);
            }
            toReturn.add(DtoFactory.createCatalogXItemDto(managedEntity));
            this.clearModifedAndPersist(managedEntity);            
        }
        return toReturn;
    }

    private void clearModifedAndPersist(CatalogXItem itemEntity) {
        if (Boolean.TRUE.equals(itemEntity.getDeleted())) {
            ((CatalogXItemDao) dao).deleteQuietly(itemEntity.getId(), CatalogXItem.class);
        }
        else {
            itemEntity.setAdded(Boolean.FALSE);
            itemEntity.setModified(Boolean.FALSE);
            itemEntity.getItem().setExternalCatalogChange(Boolean.FALSE);
            itemEntity.getItem().setExternalCatalogRelevant(Boolean.TRUE);
            itemEntity.getCatalog().setChangedItemRelevantFields(Boolean.FALSE);
            ((CatalogXItemDao) dao).updateQuietly(itemEntity);
        }
    }

    public CatalogXItem getCatalogXItem(Catalog catalog, Item item) {
        if (catalog == null || item == null) {
            return null;
        }
        CatalogXItem search = new CatalogXItem();
        Catalog searchCatalog = new Catalog();
        searchCatalog.setId(catalog.getId());
        Item searchItem = new Item();
        searchItem.setId(item.getId());

        List<CatalogXItem> results = ((CatalogXItemDao) dao).searchByExample(search, 0, 0);
        if (results != null && !results.isEmpty()) {
            return results.get(0);
        }
        else {
            return null;
        }
    }

    public ArrayList<CatalogXItemDto> getUSPSChangedItems() {
    	ArrayList<CatalogXItemDto> toReturn = new ArrayList<>();
    	ArrayList<CatalogXItemDto> toAdd = new ArrayList<>();
		try {
			LOGGER.info("Getting USPS changed items!");
        	List<Long> ids = ((CatalogXItemDao) dao).getUspsChangedItemsIds();
			LOGGER.info("Done getting USPS changed items!");
    		toAdd = this.getUspsChangedItemsBatch(ids);
    		toReturn.addAll(toAdd);
		}
		catch (Exception e) {
			LOGGER.error("Error while getting USPS changed items", e);
			toAdd = new ArrayList<CatalogXItemDto>();
		}
    	return toReturn;
    }

    public CatalogXItem searchByCustomerSku(SkuDto customerSku, Catalog catalog) {
        return ((CatalogXItemDao) dao).searchByCustomerSku(customerSku.getValue(), catalog);
    }

    public void calculateOverrideCatalogs(CatalogXItem item) {
        item.setOverrideCatalogs(((CatalogXItemDao) dao).getOverrideCatalogs(item.getId()));
    }

    private List<CatalogXItem> getItemsOverridden(Catalog catalog, Item item) {
    	//starts with this item's catalog, then in the loop immediately proceeds to parent
    	Set<Catalog> checkedCatalogs = new HashSet<>();
    	List<CatalogXItem> overriddenItems = new ArrayList<>();
    	while (catalog.getParent() != null && !checkedCatalogs.contains(catalog.getParent())) {
    		catalog = catalog.getParent();
    		checkedCatalogs.add(catalog);
    		CatalogXItem searchItem = new CatalogXItem();
    		searchItem.setItem(new Item());
    		searchItem.setCatalog(new Catalog());
    		searchItem.getItem().setId(item.getId());
    		searchItem.getCatalog().setId(catalog.getId());
    		List<CatalogXItem> searchList = this.searchByExactExample(searchItem, 0, 0);
    		if (searchList != null) {
    			overriddenItems.addAll(searchList);
    		}
    	}
    	return overriddenItems;
    }

    public void postPersist(Long catalogXItemId) {
        if (catalogXItemId == null) {
            return;
        }
        CatalogXItem entity = this.findById(catalogXItemId, CatalogXItem.class);
        this.calculateOverrideCatalogs(entity);
        this.update(entity);
        for (CatalogXItem updateItem : this.getItemsOverridden(entity.getCatalog(), entity.getItem())) {
            this.calculateOverrideCatalogs(updateItem);
            this.update(updateItem);
        }
    }

    public void postDelete(Long catalogId, Long itemId) {
        if (catalogId == null || itemId == null) {
            return;
        }
        Catalog catalog = catalogDao.findById(catalogId, Catalog.class);
        Item item = itemBp.findById(itemId, Item.class);
        for (CatalogXItem updateItem : this.getItemsOverridden(catalog, item)) {
            this.calculateOverrideCatalogs(updateItem);
            this.update(updateItem);
        }
    }

    /**
     * This method calculates the catalog overrides for all catalogxitems. This will update
     * every single customer item, triggering a Solr reindex. This will take a long time
     * and put a lot of load on the batch servers.
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Asynchronous
    public void calculateAllOverrides() {
        int batch = 0;
        List<Long> batchList = new ArrayList<Long>();
        batchList = this.batchAllForJMS(batch);
        while (batchList.size() > 0) {
    		try {
    			for (Long message : batchList) {
    				CatalogXItemListener.addCalculateOverrides(message);
    			}
			} catch (NamingException | JMSException e) {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Error sending item update", e);
				}
			}
        	batch++;
        	batchList = this.batchAllForJMS(batch);
        }
    }

    /**
     * Takes an item and an ordered list of property types to be stored, and returns the item as a row in a CSV file.
     * 
     * @param entry - the item to be stored in a CSV
     * @param propertyTypes - an ordered list of properties on the item.
     * @param skuTypes - an ordered list of sku types
     * @param customerList - an ordered list of customers
     * @return
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String smartOciCsvRow(CatalogXItem catalgXItem, List<ItemPropertyType> propertyTypes) {
    	 
    	if (catalgXItem == null || catalgXItem.getItem() == null) {
            return BLANK;
        }
    	Item entry = catalgXItem.getItem();
        
        StringBuilder outputBuffer = new StringBuilder();
        Map<String, String> propertyValues = new HashMap<>();
        Map<String, Sku> skuValues = new HashMap<>();
        Map<String, String> customerCosts = new HashMap<>();
        
        for (ItemXItemPropertyType property : entry.getPropertiesReadOnly()) {
            propertyValues.put(property.getType().getName(), property.getValue());
        }
        for (Sku sku : entry.getSkus()) {
            skuValues.put(sku.getType().getName(), sku);
        }
        for (CustomerCost cost : entry.getCustomerCosts()) {
            customerCosts.put(cost.getCustomer().getName(), String.valueOf(cost.getValue()));
        }       
        
        outputBuffer.append(DELIMITER);
        
        //Stores the Vendor SKU
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(VENDOR_SKUTYPE) != null ? skuValues.get(VENDOR_SKUTYPE).getValue() : ""));
        outputBuffer.append(DELIMITER);
        
        //Stores item name
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getName()));
        outputBuffer.append(DELIMITER);
        
        
        Map<ItemClassificationType, String> classMap = entry.getClassifications();
        for (ItemClassificationType classType : classMap.keySet()) {
        	if (classType.getName().toLowerCase().equals(UNSPSC_CLASSIFICATIONTYPE)) {
        		outputBuffer.append(classMap.get(classType));
                
        	}
        }        
        outputBuffer.append(DELIMITER);
        //unit of measure
        if (entry.getItemCategory() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getUnitOfMeasure().getName()));
            
        }
        outputBuffer.append(DELIMITER);
                
        //current price
        outputBuffer.append(catalgXItem.getPrice());
        outputBuffer.append(DELIMITER);
        
        //currency
        outputBuffer.append("USD");
        outputBuffer.append(DELIMITER);
        
        //manufacturer sku
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(MANUFACTURER) != null ? skuValues.get(MANUFACTURER).getValue() : ""));
        outputBuffer.append(DELIMITER);
        
        //manufacturer name
        if (entry.getManufacturer() != null) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getManufacturer().getName()));            
        }
        outputBuffer.append(DELIMITER);
        //description
        outputBuffer.append(StringEscapeUtils.escapeCsv(entry.getDescription()));
        outputBuffer.append(DELIMITER);
        
        StringBuffer imageString = new StringBuffer();
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() != null && image.getPrimary()) {
        		imageString.append(image.getImageUrl());
        	}
        }
        for (ItemImage image : entry.getItemImages()) {
        	if (image.getPrimary() == null || !image.getPrimary()) {
        		if (imageString.length() != 0) {
        			imageString.append(CELL_DELIMETER);
        		}
        		imageString.append(image.getImageUrl());
        	}
        }
        outputBuffer.append(StringEscapeUtils.escapeCsv(imageString.toString()));
        outputBuffer.append(DELIMITER);
        
        //Stores the APD SKU
        outputBuffer.append(StringEscapeUtils.escapeCsv(skuValues.get(APD_SKUTYPE) != null ? skuValues.get(APD_SKUTYPE).getValue() : ""));
        outputBuffer.append(DELIMITER);
        
        
        //Stores the values for different property types           
        if (propertyValues.containsKey("STAPLES_REF_NUM")) {
            outputBuffer.append(StringEscapeUtils.escapeCsv(propertyValues.get("STAPLES_REF_NUM")));           
        }       
       
        return outputBuffer.toString();
    }
}
