package com.apd.phoenix.service.model.listener;

import java.io.Serializable;
import java.util.Properties;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.solr.IndexingException;

public class SolrEntityListener {

    static final Logger logger = LoggerFactory.getLogger(SolrEntityListener.class);

    private static final Properties catalogProperties = PropertiesLoader.getAsProperties("catalog.upload.integration");
    private static final String connectionFactoryJndi = catalogProperties
            .getProperty("solrItemUpdateQueueConnectionJndi");
    private static final String queueJndi = catalogProperties.getProperty("solrItemUpdateQueueJndi");

    protected static void doSolrDocumentAction(Serializable content, SolrDocumentAction action) throws NamingException,
            JMSException {
        //Send to solr queue
        InitialContext context = new InitialContext();
        ConnectionFactory connectionFactory = (ConnectionFactory) context.lookup(connectionFactoryJndi);
        Destination itemQueue = (Destination) context.lookup(queueJndi);
        Connection connection = connectionFactory.createConnection();
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Sending " + action.name() + " message");
            }
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = session.createProducer(itemQueue);
            SolrDocumentMessage message = new SolrDocumentMessage();
            message.setContent(content);
            message.setAction(action);
            ObjectMessage objectMessage = session.createObjectMessage(message);
            messageProducer.send(objectMessage);
        }
        catch (JMSException ex) {
            throw new IndexingException("Exception during solr message creation: " + content, ex);
        }
        finally {
            try {
                connection.close();
            }
            catch (Exception e) {
                logger.error("Could not close connection", e);
            }
        }

        if (logger.isTraceEnabled()) {
            logger.trace("Object " + action.name() + "D. " + content);
        }
    }

    public enum SolrDocumentAction {
        CATXI_INSERT, CATXI_UPDATE, CATXI_DELETE, CATXI_OVERRIDES_RECALC, COMP_FAV_LIST_DELETE, ITEM_UPDATE, ITEM_DELETE, CATXI_REPRICE;
    }

    public SolrEntityListener() {
        super();
    }

    public static class SolrDocumentMessage implements Serializable {

        private static final long serialVersionUID = 1L;
        private Serializable content;
        private SolrDocumentAction action;

        public SolrDocumentAction getAction() {
            return action;
        }

        public void setAction(SolrDocumentAction action) {
            this.action = action;
        }

        public Serializable getContent() {
            return content;
        }

        public void setContent(Serializable content) {
            this.content = content;
        }
    }
}