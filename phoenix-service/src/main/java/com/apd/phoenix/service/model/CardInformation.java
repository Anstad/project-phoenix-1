package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import org.apache.commons.lang.StringUtils;
import org.hibernate.envers.Audited;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;

/**
 * This entity is used to store credit card information. This will NOT be used to store CVV numbers.
 * 
 * @author RHC
 *
 */
@Entity
@Audited
public class CardInformation implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -3075493837723927969L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @Column(name = "name_on_card")
    private String nameOnCard;

    //The card number will never be persisted only set temporarily before the call to the card vault service for secure storage
    @Transient
    private String number;

    //Identifier should be set to the truncated last 4 digits of the original card
    @Column
    private String identifier;

    @Column(nullable = false)
    private boolean debit;

    @Column(nullable = false)
    private boolean ghost;

    @Column
    private String cardVaultToken;

    @Temporal(TemporalType.DATE)
    private Date expiration;

    @Enumerated(EnumType.STRING)
    private CardTypeEnum cardType;

    @Enumerated(EnumType.STRING)
    private CvvIndicator cvvIndicator;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((CardInformation) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(final String number) {
        this.number = number;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean getDebit() {
        return this.debit;
    }

    public void setDebit(final boolean debit) {
        this.debit = debit;
    }

    public boolean isGhost() {
        return ghost;
    }

    public void setGhost(boolean ghost) {
        this.ghost = ghost;
    }

    public Date getExpiration() {
        return this.expiration;
    }

    public void setExpiration(final Date expiration) {
        this.expiration = expiration;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getCardVaultToken() {
        return cardVaultToken;
    }

    public void setCardVaultToken(String cardVaultToken) {
        this.cardVaultToken = cardVaultToken;
    }

    /**
     * @return the cardType
     */
    public CardTypeEnum getCardType() {
        return cardType;
    }

    /**
     * @param cardType the cardType to set
     */
    public void setCardType(CardTypeEnum cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cvv indicator
     */
    public CvvIndicator getCvvIndicator() {
        return cvvIndicator;
    }

    /**
     * @param cvvIndicator the cvv indicator to set
     */
    public void setCvvIndicator(CvvIndicator cvvIndicator) {
        this.cvvIndicator = cvvIndicator;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        result += "serialVersionUID: " + serialVersionUID;
        if (StringUtils.isNotBlank(identifier)) {
            result += ", identifier: " + identifier;
        }
        return result;
    }

    public enum CardTypeEnum {
        VISA("Visa"), AMEX("American Express"), MC("Mastercard");

        private String label;

        private CardTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    public enum CvvIndicator {

        NONE(CardSecurityCodeIndicator.NONE), PROVIDED(CardSecurityCodeIndicator.PROVIDED), NOT_PROVIDED(
                CardSecurityCodeIndicator.NOT_PROVIDED), ILLEGIBLE(CardSecurityCodeIndicator.ILLEGIBLE), NOT_ON_CARD(
                CardSecurityCodeIndicator.NOT_ON_CARD);

        private CardSecurityCodeIndicator xmlValue;

        private CvvIndicator(CardSecurityCodeIndicator xmlValue) {
            this.xmlValue = xmlValue;
        }

        public CardSecurityCodeIndicator getXmlValue() {
            return this.xmlValue;
        }

    }

    public static Date addExpirationMargin(Date original) {
        if (original == null) {
            return original;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(original);
        while (cal.get(Calendar.DAY_OF_MONTH) < 10 || cal.get(Calendar.DAY_OF_MONTH) > 20) {
            cal.add(Calendar.DATE, 1);
        }
        return cal.getTime();
    }

}