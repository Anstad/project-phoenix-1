package com.apd.phoenix.service.business;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.persistence.jpa.ItemPropertyTypeDao;

/**
 * This class provides business process methods for ItemPropertyType.
 * 
 * @author RHC
 * 
 */
@Stateless
@LocalBean
public class ItemPropertyTypeBp extends AbstractBp<ItemPropertyType> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(ItemPropertyTypeDao dao) {
        this.dao = dao;
    }

    /**
     * Returns the ItemPropertyType with the specified name.
     * 
     * @return
     */
    public ItemPropertyType getPropertyType(String type) {
        ItemPropertyType searchType = new ItemPropertyType();
        searchType.setName(type);
        List<ItemPropertyType> searchList = this.searchByExactExample(searchType, 0, 1);
        if (searchList.size() == 0) {
            return null;
        }
        return searchList.get(0);
    }
}
