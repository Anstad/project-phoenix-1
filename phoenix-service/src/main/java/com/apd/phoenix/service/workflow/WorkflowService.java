/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.workflow;

import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.CONCATENATE_LINE_NUM_AND_DESC;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.CXML_DEFAULT_PRICE_MARGIN;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.CXML_UNIT_PRICE;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.REQUIRE_AUTHORIZED_SHIPTOS;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum.USPS_USSCO_ADDITIONAL_PROCESSING;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderBp.NonUniqueCustomerPoException;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.MiscShipToBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.Command;
import com.apd.phoenix.service.executor.command.api.SignalCommand;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;
import com.apd.phoenix.service.itemrequest.ItemRequest;
import com.apd.phoenix.service.message.impl.InternalMessageSender;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LimitApproval;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.PaymentType.PaymentTypeEnum;
import com.apd.phoenix.service.model.Permission;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.persistence.jpa.VendorDao;
import com.apd.phoenix.service.utility.CredentialPropertyUtils;
import com.apd.phoenix.service.utility.ErrorEmailService;

@Stateless
@LocalBean
public class WorkflowService {

    private static final String DISALLOWED_STATUS_MESSAGE = "An attempt was made to place an order with the INVALIDATED or MANUALLY_RESOLVED status";

    private static final String MISSING_PERMISSION_MESSAGE = "An attempt was made to place an order without the \"shopping live credential\" permission";

    private static final Logger logger = LoggerFactory.getLogger(WorkflowService.class);

    private static final String YES = "Y";

    private static final String APD_CUSTOMER_SERVICE = "APD Customer Service";

    private static final String CUSTOMER_SERVICE = " Customer Service";

    private static final String TECH_SUPPORT = " Tech Support";

    private static final String APD_TECH_SUPPORT = "Tech Support";

    @Inject
    private BRMSCommandCreator commandCreator;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private AddressBp addressBp;

    @Inject
    private MiscShipToBp miscShipToBp;

    @Inject
    private OrderStatusBp orderStatusBp;

    @Inject
    private VendorDao vendorDao;

    @Inject
    private PaymentGatewayService paymentGatewayService;

    @Inject
    private ErrorEmailService errorEmailService;

    @Inject
    private OrderProcessLookupBp orderProcessBp;

    @Inject
    private CommentBp commentBp;

    @Inject
    private InternalMessageSender internalMessageSender;

    private void signalOrder(SignalCommand.Name name, String apdPoNumber, Object message) {
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(apdPoNumber);
        if (customerOrder != null) {
        	if (customerOrder.hasAllowedStatus()) {
	            //TODO: per PHOEN-4493, use the generated ID for the order process instead 
	            //of assuming the process ID is the same as the order ID
	            long processId = customerOrder.getId();
	            List<Object> facts = new ArrayList<>();
	            facts.add(message);
	            commandCreator.sendSignalEvent(name, message, SignalCommand.Type.ORDER, processId, facts, null);
        	}
        	else {
        		logger.warn("Attempting to signal an INVALIDATED or MANUALLY_RESOLVED order, " + apdPoNumber);
        	}
        }
        else {
            logger.error("Unable to find order to signal with APD PO " + apdPoNumber);
        }
    }

    public void processOrder(String apdPoNumber) {
        long orderId = customerOrderBp.searchByApdPo(apdPoNumber).getId();
        this.processOrder(orderId, null);
    }

    public void processOrder(CustomerOrder customerOrder) {
        PurchaseOrderDto po = null;
        try {
            po = DtoFactory.createPurchaseOrderDto(customerOrder);
        }
        catch (ParsingException e) {
            logger.error(e.toString());
            throw new RuntimeException(e);
        }

        // Set the shipping address to a clone of one on the account if possible
        boolean requireAuthShipTo = CredentialPropertyUtils.equals(REQUIRE_AUTHORIZED_SHIPTOS, YES, po.getCredential());
        Address shipTo = null;
        // If using an authorizedship to, overlay everything from the provided shipto except line 1, city, state, 
        // zip, and pendingShipToAuthorization on top of a clone of the authorized shipto
        if (requireAuthShipTo) {
            shipTo = addressBp.cloneEntity(addressBp.retrieve(po.getShipToAddressDto(), customerOrder.getAccount()));
            if (shipTo != null) {
                MiscShipToDto miscShipToDto = po.getShipToAddressDto().getMiscShipToDto();
                if (miscShipToDto != null) {
                    shipTo.setMiscShipTo(miscShipToBp.create(miscShipToDto));
                }
                shipTo.setName(po.getShipToAddressDto().getName());
                shipTo.setCompany(po.getShipToAddressDto().getCompanyName());
                shipTo.setLine2(po.getShipToAddressDto().getLine2());
                shipTo.setCounty(po.getShipToAddressDto().getCounty());
                shipTo.setCountry(po.getShipToAddressDto().getCountry());
                shipTo.setGeoCode(po.getShipToAddressDto().getGeoCode());
            }
            else {
                po.getShipToAddressDto().setPendingShipToAuthorization(Boolean.TRUE);
            }
        }
        // If GLN ID is provided, use only fields from the matching shipto associated with the account
        else if (po.getShipToAddressDto().getMiscShipToDto() != null
                && StringUtils.isNotBlank(po.getShipToAddressDto().getMiscShipToDto().getGlnID())) {
            shipTo = addressBp.cloneEntity(addressBp.retrieve(po.getShipToAddressDto(), customerOrder.getAccount()));
            if (shipTo != null) {
                shipTo = addressBp.populateInboundData(shipTo, po.getShipToAddressDto());
            }
            else {
                logger.error("Could not find address on account " + customerOrder.getAccount().getName()
                        + " with GLN ID of " + po.getShipToAddressDto().getMiscShipToDto().getGlnID());
            }
        }
        // Otherwise create a shipto from the provided shipto details on the order
        if (shipTo == null) {
            shipTo = addressBp.create(po.getShipToAddressDto());
        }
        if (StringUtils.isBlank(shipTo.getName()) && po.getShipToAddressDto() != null
                && po.getShipToAddressDto().getMiscShipToDto() != null) {
            shipTo.setName(po.getShipToAddressDto().getMiscShipToDto().getRequesterName());
        }
        if (StringUtils.isBlank(shipTo.getName()) && po.getShipToAddressDto() != null
                && po.getShipToAddressDto().getMiscShipToDto() != null) {
            shipTo.setName(po.getShipToAddressDto().getMiscShipToDto().getContactName());
        }
        customerOrder.setAddress(shipTo);

        this.customerOrderBp.addEmailAndFilterNotifications(customerOrder);

        for (LineItem lineItem : customerOrder.getItems()) {
            if (CredentialPropertyUtils.equals(CXML_UNIT_PRICE, ItemBp.APD_COST_PROPERTYTYPE, po.getCredential())) {
                if (lineItem.getCost() == null) {
                    if (po.getCredential().getProperties().get(CXML_DEFAULT_PRICE_MARGIN.getValue()) == null) {
                        // LOG.error("NO cxml default price margin property set on credential.");
                    }
                    else {
                        final BigDecimal cxmlDefaultPriceMargin = new BigDecimal(((String) po.getCredential()
                                .getProperties().get(CXML_DEFAULT_PRICE_MARGIN.getValue())));
                        lineItem.setCost(lineItem.getUnitPrice().multiply(cxmlDefaultPriceMargin).setScale(2,
                                RoundingMode.CEILING));
                    }
                }
            }
            else {
                if (CredentialPropertyUtils.equals(CXML_UNIT_PRICE, ItemBp.CUSTOMER_PRICE_PROPERTYTYPE, po
                        .getCredential())) {
                    if (lineItem.getCost() == null) {
                        lineItem.setCost(lineItem.getUnitPrice());
                    }
                }
            }
            if (lineItem.getCost() == null) {
                lineItem.setCost(lineItem.getUnitPrice());
            }
            if (CredentialPropertyUtils.equals(CONCATENATE_LINE_NUM_AND_DESC, YES, po.getCredential())) {
                String lineNumber = lineItem.getCustomerLineNumber();
                if (StringUtils.isBlank(lineNumber)) {
                    lineNumber = lineItem.getLineNumber().toString();
                }
                lineItem.setDescription("[" + lineNumber + "] " + lineItem.getDescription());
            }
            if (lineItem.getVendor() == null && StringUtils.isNotBlank(customerOrder.getCredential().getVendorName())) {
                lineItem.setVendor(vendorDao.getByName(customerOrder.getCredential().getVendorName()));
            }
        }
        customerOrderBp.update(customerOrder);
        this.processOrder(customerOrder.getId(), null);
    }

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    CredentialBp credentialBp;

    public void processOrderAfterValidation(Long orderId, PurchaseOrderDto orderRequestDto) {
    	List<Object> facts = new ArrayList<Object>();
        CustomerOrder customerOrder = customerOrderBp.getForWorkflow(orderId);
        boolean canPlace = false;
        for (Permission permission : customerOrder.getCredential().getPermissions()) {
        	if (permission != null && "shopping live credential".equals(permission.getName())) {
        		canPlace = true;
        	}
        }
        if (!canPlace) {
        	logger.warn(MISSING_PERMISSION_MESSAGE);
        	if (customerOrder.getApdPo() != null && StringUtils.isNotBlank(customerOrder.getApdPo().getValue())) {
        		logger.warn("Disallowed PO: " + customerOrder.getApdPo().getValue());
        	}
        	commentBp.addSystemComment(customerOrder, MISSING_PERMISSION_MESSAGE);
        	return;
        }
        
        if (!customerOrder.hasAllowedStatus()) {
        	logger.warn(DISALLOWED_STATUS_MESSAGE);
        	if (customerOrder.getApdPo() != null && StringUtils.isNotBlank(customerOrder.getApdPo().getValue())) {
        		logger.warn("Disallowed PO: " + customerOrder.getApdPo().getValue());
        	}
        	commentBp.addSystemComment(customerOrder, DISALLOWED_STATUS_MESSAGE);
        	return;
        }

        customerOrderBp.setOrderStatus(customerOrder, orderStatusBp.getOrderStatus(OrderStatusEnum.PLACED_PROCESSING));
        String orderType = customerOrderBp.getType(customerOrder);
        String approver = customerOrderBp.getApprover(customerOrder);
        String apdPo = customerOrder.getApdPo().getValue();
        String invoiceMethod = customerOrderBp.getBillingMethod(customerOrder);
        Set<LimitApproval> approvals = customerOrderBp.getApprovalType(customerOrder);
        String customerService = systemUserBp.getWorkflowGroups(customerOrder.getUser(), APD_CUSTOMER_SERVICE, CUSTOMER_SERVICE);
        String techSupport = systemUserBp.getWorkflowGroups(customerOrder.getUser(), APD_TECH_SUPPORT, TECH_SUPPORT);
        Boolean shouldRetryOrders = !StringUtils.isEmpty(credentialBp.getCredetialProperty(USPS_USSCO_ADDITIONAL_PROCESSING.getValue(), customerOrder.getCredential()));
        Integer pass = 1;
        
        if (invoiceMethod == null){
        	logger.error("No billing method, defaulting to 'Invoice'");
            invoiceMethod = PaymentTypeEnum.INVOICE.getValue();
        }
        
        if (approvals != null && !approvals.isEmpty()){
        	for (LimitApproval approval : approvals) {
        		facts.add(DtoFactory.createLimitApprovalDto(approval));
        	}
        }
        
        if (orderRequestDto != null) {
        	orderRequestDto.setRequest(true);
        	facts.add(orderRequestDto);
        }
        
        Map<String, Object> processParams = new HashMap<>();
        processParams.put("orderId", new Long(orderId));
        processParams.put("manager", approver);
        processParams.put("billingMethod", invoiceMethod);
        processParams.put("apdPo", apdPo);
        processParams.put("orderType", orderType);
        processParams.put("customerServiceGroups", customerService);
        processParams.put("techSupportGroups", techSupport);
        processParams.put("orderRequestDto", orderRequestDto);
        processParams.put("shouldRetryOrders", shouldRetryOrders);
        processParams.put("pass", pass);
        processParams.put("canceled", false);
        
        
        logger.info("-----------Starting process with the following params:\n" + processParams.toString());
        
        //TODO: per PHOEN-4493, use a generated ID instead of the orderId
        commandCreator.sendStartProcess(StartProcessCommand.Name.STANDARD_ORDER, StartProcessCommand.Type.ORDER,
                orderId, processParams, facts, null);
    }

    public void processOrder(Long orderId, PurchaseOrderDto orderRequestDto) {
        internalMessageSender.sendPurchaseOrderForValidation(orderRequestDto, orderId);
    }

    public void processOrder(PurchaseOrderDto purchaseOrderDto) {
        try {
            CustomerOrder customerOrder = customerOrderBp.createOrUpdateCustomerOrder(purchaseOrderDto);
            try {
                purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
            }
            catch (ParsingException e) {
                logger.error(e.getMessage());
            }
            if (customerOrder == null) {
                logger.error("Failed to create purchase order.");
                return;
            }
            //If the order has a credit card on it still, then that card needs to be put in the card vault
            if (customerOrder.getPaymentInformation() != null
                    && customerOrder.getPaymentInformation().getCard() != null
                    && customerOrder.getPaymentInformation().getCard().getNumber() != null) {
                try {
                    customerOrder.getPaymentInformation().setCard(
                            paymentGatewayService.storeCreditCard(customerOrder.getPaymentInformation()));
                }
                catch (PaymentGatewayService.CardVaultStorageException ex) {
                    logger.error(ex.getMessage());
                }
                customerOrderBp.update(customerOrder);
            }
            this.processOrder(customerOrder.getId(), purchaseOrderDto);
        }
        catch (NonUniqueCustomerPoException e) {
            errorEmailService.sendDuplicateCustomerPOErrorEmail(e.getOrder(), e.getCustomerPo());
        }
    }

    public void cancelOrder(String apdPoNumber, Object message) {
        this.signalOrder(SignalCommand.Name.CANCEL_ORDER, apdPoNumber, message);
    }

    public void requestItem(long requestId, ItemRequest request){
        Map<String, Object> processParams = new HashMap<>();
        processParams.put("itemRequest", request);
        processParams.put("requestId", requestId);
        commandCreator.sendStartProcess(StartProcessCommand.Name.ITEM_REQUEST, Command.Type.USER_REQUEST, requestId, processParams, null, null);
    }

    public void startUserRequest(long requestId, Map<String, Object> params) {
        commandCreator.sendStartProcess(StartProcessCommand.Name.USER_MODIFICATION,
                StartProcessCommand.Type.USER_REQUEST, requestId, params, null, null);
    }

    public void processOrderAcknowledgement(POAcknowledgementDto poAcknowledgementDto) {
        this.setOrderStatus(customerOrderBp.getCustomerOrder(poAcknowledgementDto),
                OrderStatusEnum.WAITING_TO_FILL_PROCESSING);
        String apdPo = poAcknowledgementDto.getApdPo();
        logger.info("PO Ack for order: {}", apdPo);
        this.signalOrder(SignalCommand.Name.ORDER_ACK, apdPo, poAcknowledgementDto);
    }

    public void processMarfieldShipment(ShipmentDto shipmentDto) {
        String apdPoNumber = shipmentDto.getCustomerOrderDto().getApdPoNumber();
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(apdPoNumber);
        if (customerOrder == null) {
            logger.error("No customer Order found for apd PO " + apdPoNumber);
        }
        //Marfield ships using lines and order data that don't match APD's, so we ship the entire entry with the designated line number.
        for (LineItemXShipmentDto lineItemX : shipmentDto.getLineItemXShipments()) {
            Integer lineNumber = lineItemX.getLineItemDto().getLineNumber();
            for (LineItem lineItem : customerOrder.getItems()) {
                if (lineItem.getLineNumber().equals(lineNumber)) {
                    String marfieldProductCode = lineItemX.getLineItemDto().getVendorSku().getValue();
                    if (!marfieldProductCode.equals(lineItem.getSupplierPartId())) {
                        logger.warn("Marfield ProdCode " + marfieldProductCode + " does not match supplier part id "
                                + lineItem.getSupplierPartId() + " for line number " + lineNumber + " on order "
                                + apdPoNumber);
                    }
                    lineItemX.setLineItemDto(DtoFactory.createLineItemDto(lineItem));
                    lineItemX.setQuantity(new BigInteger(lineItem.getQuantity().toString()));
                    break;
                }
            }
        }
        processOrderShipment(shipmentDto);
    }

    public void processOrderShipment(ShipmentDto shipmentDto) {
        this.setOrderStatus(customerOrderBp.getCustomerOrder(shipmentDto), OrderStatusEnum.SHIPPED_PROCESSING);
        String apdPo = shipmentDto.getCustomerOrderDto().retrieveApdPoNumber().getValue();
        this.processOrderShipment(shipmentDto, apdPo);
    }

    public void processAdvancedShipmentNotice(AdvanceShipmentNoticeDto advanceShipmentNoticeDto) {
        String apdPo = advanceShipmentNoticeDto.getApdPo();
        CustomerOrder order = customerOrderBp.searchByApdPo(apdPo);
        if (StringUtils.isNotBlank(advanceShipmentNoticeDto.getCustomerPo())
                && order.getCustomerPo() != null
                && StringUtils.isNotBlank(order.getCustomerPo().getValue())
                && advanceShipmentNoticeDto.getCustomerPo().startsWith(order.getCustomerPo().getValue())
                && advanceShipmentNoticeDto.getCustomerPo().substring(order.getCustomerPo().getValue().length())
                        .matches("-[0-9]*")) {
            logger.info("Received an advance shipment notice for a resent Staples order " + apdPo
                    + ". Will not ship in workflow.");
            return;
        }
        this.setOrderStatus(order, OrderStatusEnum.SHIPPED_PROCESSING);
        for (ShipmentDto shipmentDto : advanceShipmentNoticeDto.getShipmentDtos()) {
            this.processOrderShipment(shipmentDto, apdPo);
        }
    }

    private void processOrderShipment(ShipmentDto shipmentDto, String apdPo) {
        try {
            shipmentDto.setCustomerOrderDto(DtoFactory.createPurchaseOrderDto(customerOrderBp.searchByApdPo(apdPo)));
        }
        catch (ParsingException e) {
            logger.error("Parsing exception!", e);
        }
        this.signalOrder(SignalCommand.Name.SHIPPED, apdPo, shipmentDto);
    }

    public void processInvoice(VendorInvoiceDto invoiceDto) {
        String apdPo = invoiceDto.getCustomerOrderDto().retrieveApdPoNumber().getValue();
        this.signalOrder(SignalCommand.Name.VOUCHED, apdPo, invoiceDto);
    }

    public void processVendorCredit(VendorCreditInvoiceDto vendorCreditInvoiceDto){
    	String apdPo = vendorCreditInvoiceDto.getCustomerOrderDto().retrieveApdPoNumber().getValue();
    	CustomerOrder customerOrder = customerOrderBp.searchByApdPo(apdPo);
    	long processId = orderProcessBp.getUniqueProcessId(customerOrder);
    	
    	Map<String, Object> processParams = new HashMap<>();
    	processParams.put("vendorCreditInvoiceDto", vendorCreditInvoiceDto);
    	processParams.put("orderId", new Long(processId));
    	
    	List<Object> facts = new ArrayList<>();
    	facts.add(vendorCreditInvoiceDto);
	    commandCreator.sendStartProcess(StartProcessCommand.Name.CREDIT_INVOICE, StartProcessCommand.Type.ORDER, processId, processParams, null /*facts*/, null);    	
    }

    public void processReturn(ReturnOrder returnOrder){
    	CustomerOrder customerOrder = customerOrderBp.hydrateForOrderDetails(returnOrder.getOrder());
    	String techSupport = systemUserBp.getWorkflowGroups(customerOrder.getUser(), APD_TECH_SUPPORT, TECH_SUPPORT);
        String billingMethod = customerOrderBp.getBillingMethod(customerOrder);
        if (billingMethod == null){
        	logger.info("[Processing order return] : Setting billing method = 'Invoice' (Default)");
        	billingMethod = "Invoice";
        }
    	long processId = orderProcessBp.getUniqueProcessId(customerOrder);
    	Map<String, Object> processParams = new HashMap<>();
    	String apdPo = customerOrder.getApdPo().getValue();
    	 processParams.put("techSupportGroups", techSupport);
    	processParams.put("apdPo", apdPo);
    	processParams.put("orderId", new Long(processId));
    	try {
			processParams.put("returnOrderDto", DtoFactory.createReturnDto(returnOrder));
			processParams.put("billingMethod", billingMethod);
	    	commandCreator.sendStartProcess(StartProcessCommand.Name.RETURN_ORDER, StartProcessCommand.Type.ORDER, processId, processParams, null /*facts*/, null);
		} catch (DtoFactory.ParsingException e) {
			logger.error("There was a parseing exception" + e.toString());
		}
    }

    public void processManualOrder(CustomerOrder customerOrder) {
        this.processOrder(customerOrder.getId(), null);
    }

    public void processOldCart(CustomerOrder customerOrder){
    	Map<String, Object> processParams = new HashMap<>();
    	customerOrder = customerOrderBp.findById(customerOrder.getId(), CustomerOrder.class);
    	long processId = orderProcessBp.getUniqueProcessId(customerOrder);
    	PoNumber apdPo = customerOrder.getApdPo();
    	if (apdPo != null) {
    		processParams.put("apdPo", apdPo.getValue());
    	}
    	processParams.put("orderId", new Long(processId));
	    commandCreator.sendStartProcess(StartProcessCommand.Name.OLD_CART, StartProcessCommand.Type.ORDER, processId, processParams, null /*facts*/, null);
    }

    private void setOrderStatus(CustomerOrder customerOrder, OrderStatusEnum status) {
        this.setOrderStatus(customerOrder.getId(), status);
    }

    private void setOrderStatus(Long orderId, OrderStatusEnum status) {
        CustomerOrder order = customerOrderBp.findById(orderId, CustomerOrder.class);
        customerOrderBp.setOrderStatus(order, orderStatusBp.getOrderStatus(status));
        customerOrderBp.update(order);
    }

    public void processOrderStuck(CustomerOrder customerOrder) {
    	Map<String, Object> processParams = new HashMap<>();
    	customerOrder = customerOrderBp.findById(customerOrder.getId(), CustomerOrder.class);
    	PoNumber apdPo = customerOrder.getApdPo();
    	long processId = orderProcessBp.getUniqueProcessId(customerOrder);
    	if (apdPo != null) {
    		processParams.put("apdPo", apdPo.getValue());
    	}
    	processParams.put("orderId", new Long(processId));
	    commandCreator.sendStartProcess(StartProcessCommand.Name.ORDER_STUCK, StartProcessCommand.Type.ORDER, processId, processParams, null /*facts*/, null);
	}

    public void processOrderUpdate(long orderId) {
        CustomerOrder fetchedOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        try {
            this.signalOrder(SignalCommand.Name.ORDER_UPDATE, fetchedOrder.getApdPo().getValue(), DtoFactory
                    .createPurchaseOrderDto(fetchedOrder));
        }
        catch (ParsingException e) {
            logger.error("Parsing exception!", e);
        }
    }
}
