package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.business.CustomerCreditInvoiceBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import freemarker.template.TemplateException;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.CashoutPageXFieldDto;
import com.apd.phoenix.service.model.dto.DtoUtils;

@Stateless
@LocalBean
public class CustomerCreditInvoiceEmailTemplate extends EmailTemplate<CustomerCreditInvoiceDto> {

    private static final String TEMPLATE = "customer.credit.invoice";
    private static final String ATTACHMENT_PREFIX = "customer-credit-invoice";

    private static final String CONTENT_URL = PropertiesLoader.getAsProperties("aws.integration").getProperty(
            "content.service.secure.domain");
    private static final String APD_LOGO = CONTENT_URL + "/subdomain_images/logos/APD-logo.png";
    private static final String FACEBOOK = CONTENT_URL + "/subdomain_images/socialMediaIcons/Facebook.jpg";
    private static final String GOOGLE_PLUS = CONTENT_URL + "/subdomain_images/socialMediaIcons/GooglePlus.jpg";
    private static final String LINKEDIN = CONTENT_URL + "/subdomain_images/socialMediaIcons/LinkedIn.jpg";
    private static final String WORDPRESS = CONTENT_URL + "/subdomain_images/socialMediaIcons/WordPress.jpg";
    private static final String YOUTUBE = CONTENT_URL + "/subdomain_images/socialMediaIcons/YouTube.jpg";

    @Inject
    CustomerCreditInvoiceBp customerCreditInvoiceBp;

    @Override
	public String createBody(CustomerCreditInvoiceDto customerCreditInvoiceDto) throws IOException, TemplateException {
		Map<String, Object> params = new HashMap<>();
		ReturnOrderDto returnOrderDto = customerCreditInvoiceDto.getReturnOrderDto();
		PurchaseOrderDto purchaseOrderDto = returnOrderDto.getOrder();
		AddressDto addressDto = purchaseOrderDto.getShipToAddressDto();
		MiscShipToDto miscShipToDto = addressDto.getMiscShipToDto();
		
		if (purchaseOrderDto.retrieveCustomerPoNumber() != null) {
			params.put("customerOrderPo", purchaseOrderDto.retrieveCustomerPoNumber().getValue());
		}
		else{
			params.put("customerOrderPo", "None");
		}
		
		/*Logic For Remit Address Account. If it is Marquette Commercial Finance, then we want to show that account. If not then
		 * we should APD.*/		 
		Boolean isMarquette = false; //Initialize the Boolean Flag to 'false'.		 
		/*TODO: CredentialXCredentialPropertyType credXPropertiesList = customerOrder.getCredential().getProperties();
		for (CredentialXCredentialPropertyType credXProp : credXPropertiesList) {
			
			if(credXProp.getType() != null && credXProp.getType().getName() != null && 
					credXProp.getValue().toLowerCase() == "yes" && 
					credXProp.getType().getName() == "orders placed under marquette")
			{
				isMarquette=true;
			}
			
		}*/		
		
		String remitName = (isMarquette? "Marquette Commercial Finance": "American Product Distributors");
		String remitStreet = (isMarquette? "NW 6333; P.O. Box 1450": "8350 Arrowridge Blvd");
		String remitCityState = (isMarquette? "Minneapolis, MN 55485-6333": "Charlotte, NC 28273");
		
		params.put("remitName", remitName);
		params.put("remitStreet", remitStreet);
		params.put("remitCityState", remitCityState);
		
		params.put("apdPo", purchaseOrderDto.retrieveApdPoNumber().getValue());
		
		params.put("billingAddress", this.getSafeAddress(purchaseOrderDto.lookupBillToAddressDto()));
		params.put("shippingAddress", this.getSafeAddress(purchaseOrderDto.getShipToAddressDto()));
		
		Set<LineItemXReturnDto> validItems = returnOrderDto.getItems();
		params.put("items", validItems);
		
		params.put("merchandiseTotal", purchaseOrderDto.getSubTotal());
		params.put("shippingTotal", purchaseOrderDto.getEstimatedShippingAmount());	
		params.put("taxTotal", purchaseOrderDto.getTaxTotal());	
		params.put("orderTotal", purchaseOrderDto.getOrderTotal());
		
		params.put("paymentTerms", "Net 10"); //TODO: replace with credential property
		params.put("orderDate", purchaseOrderDto.getOrderDate());
		params.put("invoiceDate", returnOrderDto.getCreatedDate());
		params.put("invoiceNumber", customerCreditInvoiceDto.getInvoiceNumber());
		params.put("raNumber", returnOrderDto.getRaNumber());
		params.put("desktop", (miscShipToDto != null ? miscShipToDto.getDesktop(): ""));
                String cashoutPageFieldLabel = "building";
                if(purchaseOrderDto.getCredential() != null && purchaseOrderDto.getCredential().getCashoutPageDto() != null 
                         && purchaseOrderDto.getCredential().getCashoutPageDto().getCashoutPageXFields()!=null){
                    cashoutPageFieldLabel = DtoUtils.findCashoutPageFieldLabel(purchaseOrderDto.getCredential().getCashoutPageDto().getCashoutPageXFields(),"department");
                }
                params.put("departmentLabel", cashoutPageFieldLabel);
                params.put("department", (miscShipToDto != null ? miscShipToDto.getDepartment(): ""));
		
		//Logos
		params.put("apdLogo", APD_LOGO);
		params.put("facebookLogo", FACEBOOK);
		params.put("googlePlusLogo", GOOGLE_PLUS);
		params.put("linkedInLogo", LINKEDIN);
		params.put("wordpressLogo", WORDPRESS);
		params.put("youTubeLogo", YOUTUBE);

		return this.create(params);
	}

    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(CustomerCreditInvoiceDto customerCreditInvoiceDto)
            throws NoAttachmentContentException {
        Attachment attachment = new Attachment();
        CustomerCreditInvoice customerCreditInvoice = customerCreditInvoiceBp
                .retrieveCustomerCreditInvoice(customerCreditInvoiceDto);
        if (customerCreditInvoiceDto != null && customerCreditInvoice != null) {
            attachment.setFileName(ATTACHMENT_PREFIX + customerCreditInvoiceDto.getInvoiceNumber() + PDF_EXT);
            attachment.setMimeType(MimeType.pdf);
            InputStream is = reportService.generateInvoiceCreditPdf(customerCreditInvoice.getId().toString());
            if (is != null) {
                attachment.setContent(is);
            }
            else {
                throw new NoAttachmentContentException();
            }
        }
        return attachment;
    }
}
