/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Class OrderStatus.
 */
@Entity
@XmlRootElement
public class OrderStatus implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -4236539192338443352L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The order status description. */
    @Column
    private String description;

    /** The value. */
    @Column(unique = true)
    private String value;

    @Column(nullable = false)
    private boolean paymentStatus = false;

    //indicates whether the status should be displayed to the customer
    @Column(nullable = false)
    private boolean customerStatus = false;

    //if this field is not null, it will be set on the order instead of the original status,
    //to prevent incorrect statuses being set on the order
    @ManyToOne(fetch = FetchType.LAZY)
    private OrderStatus overrideStatus;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((OrderStatus) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the description.
     *
     * @return the description
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets description.
     *
     * @param description the new description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(final String value) {
        this.value = value;
    }

    public boolean isPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public OrderStatus getOverrideStatus() {
        return overrideStatus;
    }

    public void setOverrideStatus(OrderStatus overrideStatus) {
        this.overrideStatus = overrideStatus;
    }

    public boolean isCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(boolean customerStatus) {
        this.customerStatus = customerStatus;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (description != null && !description.trim().isEmpty())
            result += "description: " + description;
        if (value != null && !value.trim().isEmpty())
            result += ", value: " + value;
        return result;
    }

    public enum OrderStatusEnum {
        PARTIAL_BILL("PARTIAL_BILL", "The order has been partially billed"), BILLED("BILLED",
                "The order has been billed"), CREATED("CREATED", "The order was created."), VOUCHED("VOUCHED",
                "The order has been vouched"), PLACED_PROCESSING("PLACED_PROCESSING", "The order is being placed"), SENT_TO_VENDOR_PROCESSING(
                "SENT_TO_VENDOR_PROCESSING", "The order is being sent to the vendor"), WAITING_TO_FILL_PROCESSING(
                "WAITING_TO_FILL_PROCESSING", "The order is being acknowledged"), WAITING_TO_FILL("WAITING_TO_FILL",
                "The order is waiting to be filled"), VOUCHING("VOUCHING", "The order is being vouched"), SHIPPED_PROCESSING(
                "SHIPPED_PROCESSING", "The order is being shipped"), CANCELED_PROCESSING("CANCELED_PROCESSING",
                "The order is being cancelled"), VALIDATING("VALIDATING", "The order is validating"), QUOTED("QUOTED",
                "The order was quoted"), ORDERED("ORDERED", "The order was ordered"), PENDING_APPROVAL(
                "PENDING_APPROVAL", "The order is pending approval"), PENDING_TASK("PENDING_TASK",
                "The order pending a task being completed"), SENT_TO_VENDOR("SENT_TO_VENDOR",
                "The order has been sent to the vendor"), REFUSED("REFUSED", "The order was refused"), ACCEPTED(
                "ACCEPTED", "The order has been accepted by the vendor"), PARTIAL_SHIP("PARTIAL_SHIP",
                "The order is being shipped"), COMPLETED("COMPLETED", "The order was completed"), APPROVED("APPROVED",
                "The order was approved"), DENIED("DENIED", "The order was denied"), PLACED("PLACED",
                "The order was placed"), CANCELED("CANCELED", "The order was canceled"), SHIPPED("SHIPPED",
                "The order has shipped"), REJECTED("REJECTED", "All the items in the requested order were rejected."), PUNCHED(
                "PUNCHED", "The order was quoted"), PARTIAL_CHARGE("PARTIAL_CHARGE",
                "The order has been partially charged"), CHARGED("CHARGED", "The order was charged"), CAPS("CAPS",
                "The order is imported from the previous system"), INVALIDATED("INVALIDATED",
                "A user Invalidated the order"), MANUALLY_RESOLVED("MANUALLY_RESOLVED",
                "The order was manually marked as resolved");

        private final String description;
        private final String value;

        private OrderStatusEnum(String value, String description) {
            this.description = description;
            this.value = value;
        }

        public String getLabel() {
            return this.description;
        }

        public String getValue() {
            return this.value;
        }
    }
}