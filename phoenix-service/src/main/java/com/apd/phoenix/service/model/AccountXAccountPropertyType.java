package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.Audited;
import org.hibernate.envers.AuditTable;

/**
 * This entity is used to store the value of different properties associated
 * with an Account.
 * 
 * @author RHC
 * 
 */
@Entity
@Audited
@AuditTable(value = "acxacpt_AUD")
public class AccountXAccountPropertyType implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = 3715541824614131317L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @Fetch(FetchMode.JOIN)
    private AccountPropertyType type;

    @Column
    private String value;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((AccountXAccountPropertyType) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    public AccountPropertyType getType() {
        return this.type;
    }

    public void setType(final AccountPropertyType type) {
        this.type = type;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        String result = getClass().getSimpleName() + " ";
        if (value != null && !value.trim().isEmpty())
            result += "value: " + value;
        return result;
    }
}