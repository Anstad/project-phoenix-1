package com.apd.phoenix.service.utility.exception;

/**
 * This exception is thrown by processors started with MessageBeanRouteBuilder, indicating that the 
 * message should be placed back on the source queue rather than discarded.
 * 
 * @author RHC
 *
 */
public class RetryMessageException extends Exception {

}
