package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class LimitApprovalDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 75670219651610760L;

    private BigDecimal limit;

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String type;

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}