package com.apd.phoenix.service.business;

import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.CxmlConfiguration;
import com.apd.phoenix.service.persistence.jpa.CxmlConfigurationDao;

/**
 *
 * @author dnorris
 */
@Stateless
@LocalBean
public class CxmlConfigurationBp extends AbstractBp<CxmlConfiguration> {

    private CxmlConfigurationDao dao;

    /**
     * This method is injected with the correct Dao for initialization
     *
     * @param dao
     */
    @Inject
    public void initDao(CxmlConfigurationDao dao) {
        super.initAbstract(dao);
        this.dao = dao;
    }

    public Account getConfigurationFromCxmlHeaderData(Map<String, String> fromCredentialMap,
            Map<String, String> toCredentialMap, Map<String, String> senderCredentialMap, String senderSharedSecret) {
        return ((CxmlConfigurationDao) dao).getConfigurationFromCxmlHeaderData(fromCredentialMap, toCredentialMap,
                senderCredentialMap, senderSharedSecret);
    }

    public CxmlConfiguration getCxmlConfigurationFromAccount(Account account) {
        List<CxmlConfiguration> results = ((CxmlConfigurationDao) dao).getCxmlConfigurationForAccount(account);
        if (results != null && results.size() > 0) {
            return results.get(0);
        }
        return null;
    }
}
