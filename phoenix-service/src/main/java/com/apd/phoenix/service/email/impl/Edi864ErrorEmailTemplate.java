package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import freemarker.template.TemplateException;

public class Edi864ErrorEmailTemplate extends EmailTemplate<String> {

    private static final String TEMPLATE = "edi.error.864";

    public String createBody(String content,  String partnerId, String fileName) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
		String[] lines = content.split("~");
		List<String> rawLines = Arrays.asList(lines);
		for(String line:rawLines){
			line = line + "~";
		}
		params.put("rawLines", rawLines);
		params.put("partnerId", partnerId);
		params.put("fileName", fileName);
		return this.create(params);
	}

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(String content) throws NoAttachmentContentException {
        return null;
    }

    @Override
    public String createBody(String content) throws IOException, TemplateException {
        return null;
    }

}
