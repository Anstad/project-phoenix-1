/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * The Class ClassificationNote.
 */
@Entity
@XmlRootElement
public class ClassificationNote implements Serializable, com.apd.phoenix.service.model.Entity {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7521282573940116813L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ClassificationNote) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ClassificationNote [version=" + version + ", itemClassification=" + itemClassification + "]";
    }

    /** The item classification. */
    //TODO Need to confirm whether join fetch is appropriate
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ItemClassification_id")
    @Fetch(FetchMode.JOIN)
    private ItemClassification itemClassification;

    /**
     * Gets the item classification.
     *
     * @return the item classification
     */
    public ItemClassification getItemClassification() {
        return this.itemClassification;
    }

    /**
     * Sets the item classification.
     *
     * @param itemClassification the new item classification
     */
    public void setItemClassification(final ItemClassification itemClassification) {
        this.itemClassification = itemClassification;
    }

    /** The note type. */
    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private NoteType noteType;

    /**
     * Gets the note type.
     *
     * @return the note type
     */
    public NoteType getNoteType() {
        return this.noteType;
    }

    /**
     * Sets the note type.
     *
     * @param noteType the new note type
     */
    public void setNoteType(final NoteType noteType) {
        this.noteType = noteType;
    }

}