package com.apd.phoenix.service.message.api;

import java.util.List;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;

/**
 * This interface is the api for handling messages
 * 
 * @author RHC
 * 
 */
public interface MessageService {

    public boolean sendMessage(Message message, EventType eventType);

    public boolean sendOrderMessage(CustomerOrder order, Message message, EventType eventType);

    public boolean sendPurchaseOrder(PurchaseOrderDto po, MessageType messageType);

    public boolean sendPurchaseOrderAcknowledgment(POAcknowledgementDto po, MessageType messageType);

    public boolean sendShipmentNotice(ShipmentDto shipmentDto, MessageType messageType);

    public boolean sendShipmentNotice(ShipmentDto shipmentDto, MessageType messageType, Boolean resend);

    public boolean sendInvoice(CustomerInvoiceDto customerInvoiceDto, MessageType messageType);

    public boolean sendInvoice(CustomerInvoiceDto customerInvoiceDto, MessageType messageType, Boolean resend);

    public boolean sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, MessageType messageType);

    public boolean sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, MessageType messageType,
            Boolean true1);

    public boolean receiveOrderMessage(MessageDto messageDto, String token, String messageEventType);

    public boolean sendJumptrackManifest(ManifestRequestDto manifestRequestDto, MessageType messageType);

    public boolean sendShipConfirmation(List<PickDto> pickList, MessageType messageType);

    public boolean sendJumptrackStopNotificationRequest(StopNotificationRequestDto stopNotificationRequestDto,
            MessageType messageType);

    public boolean sendCardNotification(CardNotificationDto cardNotification, MessageType messageType);

    public boolean sendMarquetteInvoice(List<InvoiceDto> invoice, Boolean credit);

    public boolean sendPurchaseOrderAcknowledgment(POAcknowledgementDto poAcknowledgementDto, MessageType messageType,
            Boolean resend);

}
