package com.apd.phoenix.service.catalog;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogUploadBp;
import com.apd.phoenix.service.business.SyncItemResultBp;
import com.apd.phoenix.service.business.SyncItemResultSummaryBp;

@Stateless
public class CatalogUploadPurge {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogUploadPurge.class);

    @Inject
    CatalogUploadBp catalogUploadBp;

    @Inject
    SyncItemResultBp syncItemResultBp;

    @Inject
    SyncItemResultSummaryBp syncItemResultSummaryBp;

    public void purgeOldRecords(long catalogId) {
        List<String> correlationIds = catalogUploadBp.getCorrelationIds(catalogId);
        for (String correlationId : correlationIds) {
            LOGGER.info("Purging records for {}", correlationId);
            syncItemResultBp.removeRecords(correlationId);
            syncItemResultSummaryBp.removeRecords(correlationId);
            catalogUploadBp.removeRecords(correlationId);
        }
    }
}
