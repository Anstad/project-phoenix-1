package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Invoice;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.persistence.jpa.CustomerInvoiceDao;

/**
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CustomerInvoiceBp extends AbstractBp<CustomerInvoice> {

    @Inject
    InvoiceBp invoiceBp;

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(CustomerInvoiceDao dao) {
        this.dao = dao;
    }

    public CustomerInvoice invoice(Shipment shipment) {
        CustomerOrder customerOrder = customerOrderBp.findById(shipment.getOrder().getId(), CustomerOrder.class);

        CustomerInvoice customerInvoice = new CustomerInvoice();

        customerInvoice.setDate(new Date());
        customerInvoice.setInvoiceNumber("" + customerOrderBp.getNextInvoiceNumber(customerOrder));
        customerInvoice.setShipment(shipment);
        BigDecimal amount = BigDecimal.ZERO;
        amount = amount.add(shipment.getSubTotalPrice());
        amount = amount.add(shipment.getTotalFreightPrice());
        amount = amount.add(shipment.getTotalTaxPrice());
        customerInvoice.setAmount(amount);
        //customerInvoice.setBillingTypeCode(billingTypeCode); //TODO: Determine what this is

        customerInvoice = this.create(customerInvoice);
        return customerInvoice;
    }

    public CustomerInvoice retrieveCustomerInvoice(CustomerInvoiceDto customerInvoiceDto) {
        Invoice search = new Invoice();
        search.setInvoiceNumber(customerInvoiceDto.getInvoiceNumber());
        List<Invoice> results = invoiceBp.searchByExactExample(search, 0, 5);
        if (results != null && !results.isEmpty()) {
            return (CustomerInvoice) results.get(0);
        }
        return null;
    }

    public List<CustomerInvoice> findAllByApdPo(String apdPo) {
        return ((CustomerInvoiceDao) this.dao).findAllByApdPo(apdPo);
    }
}
