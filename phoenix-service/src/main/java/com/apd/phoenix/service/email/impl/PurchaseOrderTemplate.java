package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import freemarker.template.TemplateException;

@Stateless
@LocalBean
public class PurchaseOrderTemplate extends EmailTemplate<PurchaseOrderDto> {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderTemplate.class);

    @Inject
    private VendorBp vendorBp;

    private static final String TEMPLATE = "vendor.purchase.order";
    private static final String PO_FILE_PREFIX = "purchase-order-notice-";

    @Override
	public String createBody(PurchaseOrderDto purchaseOrderDto) throws IOException,
			TemplateException {
		Map<String, Object> params = new HashMap<>();
		
		params.put("order", purchaseOrderDto);
		params.put("currentTime", new Date());
		params.put("replyTo", this.getProperty(EmailFactoryBp.EMAIL_PROPERTIES_FROM));
		
		return this.create(params);
	}

    @Override
    public String getTemplate() {
        return TEMPLATE;
    }

    @Override
    public Attachment createAttachment(PurchaseOrderDto purchaseOrderDto) throws NoAttachmentContentException {
        if (purchaseOrderDto != null && purchaseOrderDto.getVendor() != null) {
            Vendor vendor = vendorBp.findByName(purchaseOrderDto.getVendor().getName());
            if (vendor == null) {
                LOG.error("Could not find vendor with name : " + purchaseOrderDto.getVendor().getName());
                throw new NoAttachmentContentException();
            }
            String apdPo = purchaseOrderDto.getApdPoNumber();
            Attachment poNotice = new Attachment();
            poNotice.setFileName(PO_FILE_PREFIX + apdPo + PDF_EXT);
            poNotice.setMimeType(Attachment.MimeType.pdf);
            InputStream is = reportService.generatePurchaseOrderNoticePdf(apdPo, vendor.getId());
            if (is == null) {
                throw new NoAttachmentContentException();
            }
            poNotice.setContent(is);
            return poNotice;
        }
        LOG.error("Null vendor on purchaseOrderDto");
        throw new NoAttachmentContentException();
    }
}
