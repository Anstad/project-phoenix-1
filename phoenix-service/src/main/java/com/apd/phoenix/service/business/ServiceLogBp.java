package com.apd.phoenix.service.business;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.ServiceLog;
import com.apd.phoenix.service.persistence.jpa.ServiceLogDao;
import com.apd.phoenix.service.persistence.jpa.SystemUserDao;

@Stateless
@LocalBean
public class ServiceLogBp extends AbstractBp<ServiceLog> {

    private static final String VIEW_DATA_FOR_ALL_ACCOUNTS = "view data for all accounts";

    @Inject
    private SystemUserDao systemUserDao;

    @Inject
    private SecurityContext securityContext;

    @Inject
    public void initDao(ServiceLogDao dao) {
        this.dao = dao;
    }

    public List<ServiceLog> getServiceLogs(ServiceLogSearchCriteria criteria) {
        List<ServiceLog> logList;

        String user = securityContext.getUserLogin();
        if (!securityContext.hasPermission(VIEW_DATA_FOR_ALL_ACCOUNTS)) {
            criteria.setAccountWhiteList(systemUserDao.getAccountIds(user));
            logList = ((ServiceLogDao) dao).getServiceLogs(criteria);
        }
        else {
            logList = ((ServiceLogDao) dao).getServiceLogs(criteria);
        }

        if (logList == null) {
            logList = new ArrayList<ServiceLog>();
        }

        return logList;
    }
}
