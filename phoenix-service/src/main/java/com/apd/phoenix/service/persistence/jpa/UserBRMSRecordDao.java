package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.UserBRMSRecord;

/**
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class UserBRMSRecordDao extends AbstractDao<UserBRMSRecord> {

    private static final Logger LOG = LoggerFactory.getLogger(UserBRMSRecordDao.class);

    @SuppressWarnings("unchecked")
    public List<Boolean> isActive(Long id) {
        String hql = "SELECT record.active FROM UserBRMSRecord AS record JOIN record.userRequest AS ur WHERE ur.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getSessionId(Long id) {
        String hql = "SELECT record.sessionId FROM UserBRMSRecord AS record JOIN record.userRequest AS ur WHERE ur.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getProcessId(Long id) {
        String hql = "SELECT record.processId FROM UserBRMSRecord AS record JOIN record.userRequest AS ur WHERE ur.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<UserBRMSRecord> getRecordByUserRequestId(Long id) {
        String hql = "SELECT record FROM UserBRMSRecord AS record JOIN record.userRequest AS ur WHERE ur.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<UserBRMSRecord> getRecordBySessionId(Integer sessionId) {
        String hql = "SELECT record FROM UserBRMSRecord AS record WHERE record.sessionId=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", sessionId);
        return (List<UserBRMSRecord>) query.getSingleResult();
    }

    public List<String> getSnapshot(long id) {
        String hql = "SELECT record.snapshot FROM UserBRMSRecord AS record JOIN record.userRequest AS ur WHERE ur.id=:id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return query.getResultList();
    }
}
