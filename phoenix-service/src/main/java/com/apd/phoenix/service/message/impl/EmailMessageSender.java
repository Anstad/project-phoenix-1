package com.apd.phoenix.service.message.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageSender;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.CardNotificationDto;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;

@Stateless
public class EmailMessageSender extends MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailMessageSender.class);

    @Inject
    private EmailService emailService;

    @Resource(mappedName = "java:/activemq/email-order-outbound")
    private Queue emailOrderQueue;

    @Resource(mappedName = "java:/activemq/email-shipment-notice-outbound")
    private Queue shipmentNoticeQueue;

    @Resource(mappedName = "java:/activemq/email-invoice-outbound")
    private Queue emailInvoiceQueue;

    @Resource(mappedName = "java:/activemq/email-credit-invoice-outbound")
    private Queue emailCreditInvoiceQueue;

    @Resource(mappedName = "java:/activemq/email-purchase-order-outbound")
    private Queue emailPurchaseOrderQueue;

    @Resource(mappedName = "java:/activemq/email-card-notification-outbound")
    private Queue emailCardNotificationQueue;

    @Override
    public Message sendPurchaseOrder(PurchaseOrderDto purchaseOrderDto) {
        this.putObjectMessage(purchaseOrderDto, emailPurchaseOrderQueue);
        return null;
    }

    @Override
    public Message sendPOAcknowledgement(POAcknowledgementDto poAcknowledgementDto, Boolean resend) {
        this.putObjectMessage(poAcknowledgementDto, emailOrderQueue);
        return null;
    }

    @Override
    public Message sendShipmentNotice(ShipmentDto shipmentDto, Boolean resend) {
        this.putObjectMessage(shipmentDto, shipmentNoticeQueue);
        return null;
    }

    @Override
    public boolean sendMessage(Message message, EventType eventType) {
        if (message != null && message.getContent() != null && message.getContent().markSupported()) {
            try {
                LOGGER.info("Reseting input stream since mark supported");
                message.getContent().reset();
            }
            catch (IOException e) {
                LOGGER.error("Could not retrieve value 'markSupported' from Message", e);
                return false;
            }
        }
        else if (message != null) {
            LOGGER.info("retrieving message from storage since mark is unsupported");
            message = messageUtils.retrieveMessage(message.getMetadata());
        }

        if (message == null || message.getMetadata() == null || message.getContent() == null) {
            LOGGER.error("Message data was null! " + eventType);
            return false;
        }

        try {
            MimeMessage emailMessage = new MimeMessage(null, message.getContent());
            this.applyOverrides(emailMessage);
            emailService.sendEmail(emailMessage);
        }
        catch (Exception e) {
            LOGGER.error("Could not send email", e);
        }
        return true;
    }

    private void applyOverrides(MimeMessage message) {

        Properties emailOverride = PropertiesLoader.getAsProperties("email.override");

        if (emailOverride.getProperty("override", "true").equalsIgnoreCase("true")) {
            String[] destinationList = emailOverride.getProperty("email", "").split(",");
            InternetAddress[] addresses = new InternetAddress[destinationList.length];
            for (int i = 0; i < destinationList.length; i++) {
                try {
                    InternetAddress address = new InternetAddress(destinationList[i]);
                    addresses[i] = address;
                }
                catch (AddressException e) {
                    LOGGER.error("Could not parse email address", e);
                }
            }
            try {
                message.setRecipients(javax.mail.Message.RecipientType.TO, addresses);
                message.setRecipients(javax.mail.Message.RecipientType.CC, new InternetAddress[0]);
                message.setRecipients(javax.mail.Message.RecipientType.BCC, new InternetAddress[0]);
            }
            catch (MessagingException e) {
                LOGGER.error("Error setting override email", e);
            }
        }

        return;
    }

    private void putObjectMessage(Serializable object, Queue queue) {
        putObjectMessage(object, queue, false);
    }

    private void putObjectMessage(Serializable object, Queue queue, Boolean resend) {
        try {
            Connection connection = connectionFactory.createConnection();
            try {
                Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                MessageProducer messageProducer = msession.createProducer(queue);
                ObjectMessage message = msession.createObjectMessage(object);
                message.setBooleanProperty("resend", resend);
                messageProducer.send(message);
            }
            catch (Exception e) {
                LOGGER.error(e.toString());
            }
            if (connection != null) {
                connection.close();
            }
        }
        catch (Exception e) {
            LOGGER.error(e.toString());
        }
    }

    @Override
    public Message sendInvoice(CustomerInvoiceDto customerInvoiceDto, Boolean resend) {
        this.putObjectMessage(customerInvoiceDto, emailInvoiceQueue, resend);
        return null;
    }

    @Override
    public Message sendCreditInvoice(CustomerCreditInvoiceDto customerCreditInvoiceDto, Boolean resend) {
        this.putObjectMessage(customerCreditInvoiceDto, emailCreditInvoiceQueue, resend);
        return null;
    }

    @Override
    public Message sendCardNotification(CardNotificationDto cardNotificationDto) {
        this.putObjectMessage(cardNotificationDto, emailCardNotificationQueue);
        return null;
    }

    public boolean sendSimpleMessage(String from, String recipient, String subject, String body,
            List<Attachment> attachments) {
        Set<String> recipientsList = new HashSet<String>();
        recipientsList.add(recipient);
        MimeMessage message = emailService.prepareRawMessage(from, null, null, recipientsList, null, subject, body,
                attachments);
        this.applyOverrides(message);
        emailService.sendEmail(message);
        return false;
    }
}
