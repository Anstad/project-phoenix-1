package com.apd.phoenix.service.persistence.jpa;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import com.apd.phoenix.service.model.Comment;

@Stateless
@LocalBean
public class CommentDao extends AbstractDao<Comment> {
}