/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.FetchProfile;
import org.hibernate.annotations.IndexColumn;
import org.hibernate.envers.AuditJoinTable;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import com.apd.phoenix.service.model.CustomerOrder.PunchoutOrderOperation;
import com.apd.phoenix.service.model.CxmlConfiguration.DeploymentMode;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.MessageMetadata.PunchoutType;

import javax.persistence.OneToOne;
/**
 * The Class Credential.
 */
@Entity
@XmlRootElement
@Audited
public class Credential implements Serializable, com.apd.phoenix.service.model.Entity{

    private static final long serialVersionUID = 2390995672816114188L;

    /** The id. */
    @Id
    private @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    Long id = null;

    /** The version. */
    @Version
    private @Column(name = "version", nullable = false)
    int version = 0;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "cred_id")
    @NotAudited
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<NotificationProperties> notificationProperties = new HashSet<NotificationProperties>();

    @Temporal(TemporalType.DATE)
    private Date activationDate;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=3)
    private Set<Bulletin> bulletins = new HashSet<Bulletin>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<CostCenter> costCenters = new HashSet<CostCenter>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<Department> departments = new HashSet<Department>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "cred_id")
    @AuditJoinTable(name = "cxcpt_AUD")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=20)
    private Set<CredentialXCredentialPropertyType> properties = new HashSet<CredentialXCredentialPropertyType>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "credential_id")
    @NotAudited
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=40)
    private Set<NotificationLimit> notificationLimits = new HashSet<NotificationLimit>();

    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=4)
    private Set<Role> roles = new HashSet<Role>();
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=3)
    private Set<LimitApproval> limitApproval = new HashSet<LimitApproval>();
    
    @ManyToMany(fetch = FetchType.LAZY)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=6)
    private Set<Permission> permissions = new HashSet<Permission>();

    @Temporal(TemporalType.DATE)
    private Date expirationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private CommunicationMethod communicationMethod;

    @OneToMany(mappedBy = "credential", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @NotAudited
    private Set<ProcessConfiguration> processConfigurations;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @NotAudited
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=2)
    private Set<PoNumber> blanketPos = new HashSet<PoNumber>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cred_id")
    @NotAudited
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<IpAddress> ips = new HashSet<IpAddress>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PaymentInformation paymentInformation;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "outToCredentialTable")
    @IndexColumn(base = 1, name = "out-tc")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<CxmlCredential> outgoingToCredentials = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "outFromCredentialTable")
    @IndexColumn(base = 1, name = "out-fc")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<CxmlCredential> outgoingFromCredentials = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "outSenderCredentialTable")
    @IndexColumn(base = 1, name = "out-sc")
    private Set<CxmlCredential> outgoingSenderCredentials = new HashSet<>();
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "customerOutToCred")
    @IndexColumn(base = 1, name = "customer-out-tc")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<CxmlCredential> customerOutoingToCred = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "customerOutFromCred")
    @IndexColumn(base = 1, name = "customer-out-fc")
    @Fetch(FetchMode.SELECT)
    @BatchSize(size=5)
    private Set<CxmlCredential> customerOutoingFromCred = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "customerOutSenderCred")
    @IndexColumn(base = 1, name = "customer-out-sc")
    private Set<CxmlCredential> customerOutoingSenderCred = new HashSet<>();
    
    @Column(name="RETURNCARTDOM")
    private String returnCartCredentialDomain;
    
    @Column(name="RETURNCARTID")
    private String returnCartCredentialIdentity;
    
    @Column
    private String customerOutgoingSharedSecret;
    
    @Column(nullable = false)
    private boolean requireZipCodeInput = false;
    
    @Column
    private String defaultPunchoutZipCode;
    
    @Column
    private String outgoingSenderSharedSecret;
    
    @Column
    private String vendorPunchoutUrl;
    
    @Column
    private String vendorOrderUrl;
    
    @Column
    private String vendorName;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "deploymentmode")
    private DeploymentMode deploymentMode;

    @Enumerated(EnumType.STRING)
    @Column(name = "poackmtype")
    private MessageType poAcknowledgementMessageType;
    
    @Column(name = "poackdest")
    private String poAcknowledgementDestination;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "snmtype")
    private MessageType shipmentNotificationMessageType;
    
    @Column(name = "sndest")
    private String shipmentNotificationDestination;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "imtype")
    private MessageType invoiceMessageType;
    
    @Column(name = "idest")
    private String invoiceDestination;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "punchouttype")
    private PunchoutType punchoutType;
    
    @Column(name = "ediRecCode")
    private String ediReceiverCode;
    
    @Column
    private String partnerId;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "cimtype")
    private MessageType creditInvoiceMessageType;
    
    @Column(name= "cidest")
    private String creditInvoiceDestination;
    
    @Column
    private String ediVersion;

    //determines whether the shipping method for EDI should be overridden with "will call"
    @Column(nullable = false)
    private boolean willCallEdiOverride = false;
    
    @Column(nullable = false)
    private boolean wrapAndLabel = false;
    
    @Column(nullable = false)
    private boolean adot = false;
    
    @Enumerated(EnumType.STRING)
    private Terms terms;
    
    @Column
    private String csrPhone;
    
    @Column
    private String csrEmail;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "punchopallow")
    private PunchoutOrderOperation punchoutOperationAllowed;
    
    @OneToOne
    private Address defaultShipFromAddress;
    
    /** The root account. */
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Account rootAccount;
    
    /** The catalog. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Catalog catalog;
    
    /** The cashout page. */
    @ManyToOne(fetch = FetchType.LAZY)
    @NotAudited
    private CashoutPage cashoutPage;
    
    @Column(name = "usrGuideFleName")
    private String userGuideFileName;
    
    @Column(name = "customFleName")
    private String customOrderFileName;
    
    @Column(name = "specialFleName")
    private String specialOrderFileName;
    
    /** The user. */
    @ManyToOne(fetch = FetchType.EAGER)
    private SystemUser user;    

	public PunchoutOrderOperation getPunchoutOperationAllowed() {
		return punchoutOperationAllowed;
	}

	public void setPunchoutOperationAllowed(
			PunchoutOrderOperation punchoutOperationAllowed) {
		this.punchoutOperationAllowed = punchoutOperationAllowed;
	}

	/**
     * @return the terms
     */
    public Terms getTerms() {
        return terms;
    }

    /**
     * @param terms the terms to set
     */
    public void setTerms(Terms terms) {
        this.terms = terms;
    }

    public DeploymentMode getDeploymentMode() {
		return deploymentMode;
	}

	public void setDeploymentMode(DeploymentMode deploymentMode) {
		this.deploymentMode = deploymentMode;
	}

	/**
     * @return the defaultShipToAddress
     */
    public Address getDefaultShipFromAddress() {
        return defaultShipFromAddress;
    }

    /**
     * @param defaultShipToAddress the defaultShipToAddress to set
     */
    public void setDefaultShipFromAddress(Address defaultShipFromAddress) {
        this.defaultShipFromAddress = defaultShipFromAddress;
    }

    /**
     * @return the userGuideFileName
     */
    public String getUserGuideFileName() {
        return userGuideFileName;
    }

    /**
     * @param userGuideFileName the userGuideFileName to set
     */
    public void setUserGuideFileName(String userGuideFileName) {
        this.userGuideFileName = userGuideFileName;
    }

    /**
     * @return the customOrderFileName
     */
    public String getCustomOrderFileName() {
        return customOrderFileName;
    }

    /**
     * @param customOrderFileName the customOrderFileName to set
     */
    public void setCustomOrderFileName(String customOrderFileName) {
        this.customOrderFileName = customOrderFileName;
    }

    /**
     * @return the specialOrderFileName
     */
    public String getSpecialOrderFileName() {
        return specialOrderFileName;
    }

    /**
     * @param specialOrderFileName the specialOrderFileName to set
     */
    public void setSpecialOrderFileName(String specialOrderFileName) {
        this.specialOrderFileName = specialOrderFileName;
    }

    /**
     * These are the different types of terms that exist on a credential.
     * 
     * @author RHC
     *
     */
    public enum Terms {
		//note: if a new term is added, it needs to be added to the INVOICE_DEBIT.jrxml report
        ONE_PERCENT_THIRTY_NET_FOURTY_FIVE(30,1,45, "1% 30, NET 45"), TWO_PERCENT_THIRTY_NET_THIRTY_ONE(30,2,31,"2% 30, NET 31"), 
        TEN(10,null,null,"Net 10"), FIFTEEN(10,null,null,"Net 15"), TWENTY(10,null,null,"Net 20"), THIRTY(10,null,null,"Net 30"),
        FOURTY_FIVE(10,null,null,"NET 45"), TIME_OF_PURCHASE(0,null,null,"Time of Purchase");
        private final Integer daysUntilDue;
        private final Integer discountPercent;
        private final Integer daysWhileDiscounted;
        private final String label;
        
        Terms(Integer daysDue, Integer discountPercent, Integer discountDays, String label){
               daysUntilDue = daysDue;
               this.discountPercent = discountPercent;
               this.daysWhileDiscounted = discountDays;  
               this.label = label;
        }
        
        public Integer getDaysUntilDue() {
            return daysUntilDue;
        }

        /**
         * @return the discountPercent
         */
        public Integer getDiscountPercent() {
            return discountPercent;
        }

        /**
         * @return the daysWhileDiscounted
         */
        public Integer getDaysWhileDiscounted() {
            return daysWhileDiscounted;
        }
        
        public String getLabel() {
        	return this.label;
        }
        
    }
    
    /**
     * This enum lists all of the permissions associated with a Credential.
     * 
     * @author RHC
     *
     */
    public enum CredentialPermissions {
        showRebateCenter, showAboutApd, showTonerRecycle, showReturnPolicy, showCustomOrders, showSpecialOrders, allowBackordered, orderFreeItem, editOrder, allowRejected
    }

    public enum ValueToCheck {
        ORDER_TOTAL, ANY_ITEM_TOTAL, ANY_UNIT_PRICE, COST_CENTER
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((Credential) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the root account.
     *
     * @return the root account
     */
    public Account getRootAccount() {
        return this.rootAccount;
    }

    /**
     * Sets the root account.
     *
     * @param rootAccount the new root account
     */
    public void setRootAccount(final Account rootAccount) {
        this.rootAccount = rootAccount;
    }

    /** The name. */
    @Column(nullable = false, unique = true)
    private String name;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /** The authorize address. */
    @Column(nullable = false)
    private boolean authorizeAddress;

    /**
     * Gets the authorize address.
     *
     * @return the authorize address
     */
    public boolean getAuthorizeAddress() {
        return this.authorizeAddress;
    }

    /**
     * Sets the authorize address.
     *
     * @param authorizeAddress the new authorize address
     */
    public void setAuthorizeAddress(final boolean authorizeAddress) {
        this.authorizeAddress = authorizeAddress;
    }

    /** The sku type. */
    @ManyToOne(fetch = FetchType.EAGER)
    private SkuType skuType;

    /**
     * Gets the sku type.
     *
     * @return the sku type
     */
    public SkuType getSkuType() {
        return this.skuType;
    }

    /**
     * Sets the sku type.
     *
     * @param skuType the new sku type
     */
    public void setSkuType(final SkuType skuType) {
        this.skuType = skuType;
    }

    /** The store credit. */
    @Column
    private BigDecimal storeCredit;

    /**
     * Gets the store credit.
     *
     * @return the store credit
     */
    public BigDecimal getStoreCredit() {
        return this.storeCredit;
    }

    /**
     * Sets the store credit.
     *
     * @param storeCredit the new store credit
     */
    public void setStoreCredit(final BigDecimal storeCredit) {
        this.storeCredit = storeCredit;
    }

    /**
     * Gets the catalog.
     *
     * @return the catalog
     */
    public Catalog getCatalog() {
        return this.catalog;
    }

    /**
     * Sets the catalog.
     *
     * @param catalog the new catalog
     */
    public void setCatalog(final Catalog catalog) {
        this.catalog = catalog;
    }

    /**
     * Gets the cashout page.
     *
     * @return the cashout page
     */
    public CashoutPage getCashoutPage() {
        return this.cashoutPage;
    }

    /**
     * Sets the cashout page.
     *
     * @param cashoutPage the new cashout page
     */
    public void setCashoutPage(final CashoutPage cashoutPage) {
        this.cashoutPage = cashoutPage;
    }

    public Set<NotificationProperties> getNotificationProperties() {
        return this.notificationProperties;
    }

    public void setNotificationProperties(final Set<NotificationProperties> notificationProperties) {
        this.notificationProperties = notificationProperties;
    }

    public Date getActivationDate() {
        return this.activationDate;
    }

    public void setActivationDate(final Date activationDate) {
        this.activationDate = activationDate;
    }

    public Set<Bulletin> getBulletins() {
        return this.bulletins;
    }

    public void setBulletins(final Set<Bulletin> bulletins) {
        this.bulletins = bulletins;
    }

    public Set<CredentialXCredentialPropertyType> getProperties() {
        return this.properties;
    }

    public void setProperties(final Set<CredentialXCredentialPropertyType> properties) {
        this.properties = properties;
    }
    
    public String getPropertyValueByType(String type){
    	for(CredentialXCredentialPropertyType property:getProperties()){
    		if(property.getType() != null && property.getType().getName().equals(type)){
    			return property.getValue();
    		}
    	}
    	return null;
    }

    public Set<NotificationLimit> getNotificationLimits() {
		return notificationLimits;
	}

	public void setNotificationLimits(Set<NotificationLimit> notificationLimits) {
		this.notificationLimits = notificationLimits;
	}

	public Set<Role> getRoles() {
        return this.roles;
    }

    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }

    public Set<Permission> getPermissions() {
        return this.permissions;
    }

    public void setPermissions(final Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Date getExpirationDate() {
        return this.expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public CommunicationMethod getCommunicationMethod() {
        return this.communicationMethod;
    }

    public void setCommunicationMethod(final CommunicationMethod communicationMethod) {
        this.communicationMethod = communicationMethod;
    }

    public Set<ProcessConfiguration> getProcessConfigurations() {
        return this.processConfigurations;
    }

    public void setProcessConfigurations(final Set<ProcessConfiguration> processConfiguration) {
        this.processConfigurations = processConfiguration;
    }

    public Set<PoNumber> getBlanketPos() {
        return this.blanketPos;
    }

    public void setBlanketPos(final Set<PoNumber> blanketPos) {
        this.blanketPos = blanketPos;
    }

    public Set<IpAddress> getIps() {
        return this.ips;
    }

    public void setIps(final Set<IpAddress> ips) {
        this.ips = ips;
    }

    public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public PaymentInformation getPaymentInformation() {
        return this.paymentInformation;
    }

    public void setPaymentInformation(final PaymentInformation paymentInformation) {
        this.paymentInformation = paymentInformation;
    }

    public Set<CostCenter> getCostCenters() {
        return costCenters;
    }

    public void setCostCenters(Set<CostCenter> costCenters) {
        this.costCenters = costCenters;
    }

    public Set<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(Set<Department> departments) {
        this.departments = departments;
    }

    public Set<CxmlCredential> getOutgoingToCredentials() {
        return outgoingToCredentials;
    }

    public void setOutgoingToCredentials(Set<CxmlCredential> outgoingToCredentials) {
        this.outgoingToCredentials = outgoingToCredentials;
    }

    public Set<CxmlCredential> getOutgoingFromCredentials() {
        return outgoingFromCredentials;
    }

    public void setOutgoingFromCredentials(Set<CxmlCredential> outgoingFromCredentials) {
        this.outgoingFromCredentials = outgoingFromCredentials;
    }

    public Set<CxmlCredential> getOutgoingSenderCredentials() {
        return outgoingSenderCredentials;
    }

    public void setOutgoingSenderCredentials(Set<CxmlCredential> outgoingSenderCredentials) {
        this.outgoingSenderCredentials = outgoingSenderCredentials;
    }

    public String getOutgoingSenderSharedSecret() {
        return outgoingSenderSharedSecret;
    }

    public void setOutgoingSenderSharedSecret(String outgoingSenderSharedSecret) {
        this.outgoingSenderSharedSecret = outgoingSenderSharedSecret;
    }

    /**
     * @return the punchoutType
     */
    public PunchoutType getPunchoutType() {
        return punchoutType;
    }

    /**
     * @param punchoutType the punchoutType to set
     */
    public void setPunchoutType(PunchoutType punchoutType) {
        this.punchoutType = punchoutType;
    }

    public String getVendorPunchoutUrl() {
        return vendorPunchoutUrl;
    }

    public void setVendorPunchoutUrl(String vendorPunchoutUrl) {
        this.vendorPunchoutUrl = vendorPunchoutUrl;
    }

	public String getVendorOrderUrl() {
		return vendorOrderUrl;
	}

	public void setVendorOrderUrl(String vendorOrderUrl) {
		this.vendorOrderUrl = vendorOrderUrl;
	}

	public MessageType getPoAcknowledgementMessageType() {
		return poAcknowledgementMessageType;
	}

	public void setPoAcknowledgementMessageType(
			MessageType poAcknowledgementMessageType) {
		this.poAcknowledgementMessageType = poAcknowledgementMessageType;
	}

	public String getPoAcknowledgementDestination() {
		return poAcknowledgementDestination;
	}

	public void setPoAcknowledgementDestination(String poAcknowledgementDestination) {
		this.poAcknowledgementDestination = poAcknowledgementDestination;
	}

	public MessageType getShipmentNotificationMessageType() {
		return shipmentNotificationMessageType;
	}

	public void setShipmentNotificationMessageType(
			MessageType shipmentNotificationMessageType) {
		this.shipmentNotificationMessageType = shipmentNotificationMessageType;
	}

	public String getShipmentNotificationDestination() {
		return shipmentNotificationDestination;
	}

	public void setShipmentNotificationDestination(
			String shipmentNotificationDestination) {
		this.shipmentNotificationDestination = shipmentNotificationDestination;
	}
	
	public MessageType getInvoiceMessageType() {
		return invoiceMessageType;
	}

	public void setInvoiceMessageType(MessageType invoiceMessageType) {
		this.invoiceMessageType = invoiceMessageType;
	}

	public String getInvoiceDestination() {
		return invoiceDestination;
	}

	public void setInvoiceDestination(String invoiceDestination) {
		this.invoiceDestination = invoiceDestination;
	}

	public MessageType getCreditInvoiceMessageType() {
		return creditInvoiceMessageType;
	}

	public void setCreditInvoiceMessageType(MessageType creditInvoiceMessageType) {
		this.creditInvoiceMessageType = creditInvoiceMessageType;
	}

	public String getCreditInvoiceDestination() {
		return creditInvoiceDestination;
	}

	public void setCreditInvoiceDestination(String creditInvoiceDestination) {
		this.creditInvoiceDestination = creditInvoiceDestination;
	}

	public String getEdiReceiverCode() {
		return ediReceiverCode;
	}

	public void setEdiReceiverCode(String ediReceiverCode) {
		this.ediReceiverCode = ediReceiverCode;
	}

	public String getEdiVersion() {
		return ediVersion;
	}

	public void setEdiVersion(String ediVersion) {
		this.ediVersion = ediVersion;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = StringUtils.lowerCase(partnerId);
	}

	public boolean isWillCallEdiOverride() {
		return willCallEdiOverride;
	}

	public void setWillCallEdiOverride(boolean willCallEdiOverride) {
		this.willCallEdiOverride = willCallEdiOverride;
	}

	public String getCsrPhone() {
		return csrPhone;
	}

	public void setCsrPhone(String csrPhone) {
		this.csrPhone = csrPhone;
	}

	public String getCsrEmail() {
		return csrEmail;
	}

	public void setCsrEmail(String csrEmail) {
		this.csrEmail = csrEmail;
	}

    public boolean isRequireZipCodeInput() {
        return requireZipCodeInput;
    }

    public void setRequireZipCodeInput(boolean requireZipCodeInput) {
        this.requireZipCodeInput = requireZipCodeInput;
    }

    public String getDefaultPunchoutZipCode() {
        return defaultPunchoutZipCode;
    }

    public void setDefaultPunchoutZipCode(String defaultPunchoutZipCode) {
        this.defaultPunchoutZipCode = defaultPunchoutZipCode;
    }

    public Set<CxmlCredential> getCustomerOutoingToCred() {
        return customerOutoingToCred;
    }

    public void setCustomerOutoingToCred(Set<CxmlCredential> customerOutoingToCred) {
        this.customerOutoingToCred = customerOutoingToCred;
    }

    public Set<CxmlCredential> getCustomerOutoingFromCred() {
        return customerOutoingFromCred;
    }

    public void setCustomerOutoingFromCred(Set<CxmlCredential> customerOutoingFromCred) {
        this.customerOutoingFromCred = customerOutoingFromCred;
    }

    public Set<CxmlCredential> getCustomerOutoingSenderCred() {
        return customerOutoingSenderCred;
    }

    public void setCustomerOutoingSenderCred(Set<CxmlCredential> customerOutoingSenderCred) {
        this.customerOutoingSenderCred = customerOutoingSenderCred;
    }

    public String getCustomerOutgoingSharedSecret() {
        return customerOutgoingSharedSecret;
    }

    public void setCustomerOutgoingSharedSecret(String customerOutgoingSharedSecret) {
        this.customerOutgoingSharedSecret = customerOutgoingSharedSecret;
    }
    
    public boolean isHostedCatalog() {
    	return StringUtils.isBlank(this.getVendorPunchoutUrl());
    }

    public boolean isWrapAndLabel() {
		return wrapAndLabel;
	}

	public void setWrapAndLabel(boolean wrapAndLabel) {
		this.wrapAndLabel = wrapAndLabel;
	}

	public boolean isAdot() {
		return adot;
	}

	public void setAdot(boolean adot) {
		this.adot = adot;
	}

	public String getReturnCartCredentialDomain() {
		return returnCartCredentialDomain;
	}

	public void setReturnCartCredentialDomain(String returnCartCredentialDomain) {
		this.returnCartCredentialDomain = returnCartCredentialDomain;
	}

	public String getReturnCartCredentialIdentity() {
		return returnCartCredentialIdentity;
	}

	public void setReturnCartCredentialIdentity(String returnCartCredentialIdentity) {
		this.returnCartCredentialIdentity = returnCartCredentialIdentity;
	}

	@Override
    public String toString() {
        String result = "";
        if (name != null && !name.trim().isEmpty()) {
            result += name;
        }
        return result;
    }

	public Set<LimitApproval> getLimitApproval() {
		return this.limitApproval;
	}

	public void setLimitApproval(final Set<LimitApproval> limitApproval) {
		this.limitApproval = limitApproval;
	}
	
	public SystemUser getUser() {
		return user;
	}

	public void setUser(SystemUser user) {
		this.user = user;
	}

}
