/*
 * 
 */
package com.apd.phoenix.service.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.envers.Audited;

/**
 * The Class ServiceLog.
 */
@MappedSuperclass
@Audited
public abstract class ContactLog implements Serializable, com.apd.phoenix.service.model.Entity {

    private static final long serialVersionUID = -8572669203622878898L;

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id = null;

    /** The version. */
    @Version
    @Column(name = "version", nullable = false)
    private int version = 0;

    /** The customer order. */
    @ManyToOne(fetch = FetchType.LAZY)
    private CustomerOrder customerOrder;

    /** The Department. */
    @Column
    private String department;

    @Column
    private String contactName;

    @Column
    private String contactNumber;

    @Column
    private String contactEmail;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updateTimestamp;

    @Temporal(TemporalType.TIMESTAMP)
    private Date resolvedDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date openedDate;

    @Column
    private String resolution;

    @Column
    private Boolean followUpComplete;

    @Column
    private String ticketNumber;

    @Enumerated(EnumType.STRING)
    private TicketStatus status;

    public enum TicketStatus {
        OPEN, CLOSED;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the version.
     *
     * @return the version
     */
    public int getVersion() {
        return this.version;
    }

    /**
     * Sets the version.
     *
     * @param version the new version
     */
    public void setVersion(final int version) {
        this.version = version;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((ContactLog) that).id);
        }
        return super.equals(that);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }

    /**
     * Gets the customer order.
     *
     * @return the customer order
     */
    public CustomerOrder getCustomerOrder() {
        return this.customerOrder;
    }

    /**
     * Sets the customer order.
     *
     * @param customerOrder the new customer order
     */
    public void setCustomerOrder(final CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    public Date getUpdateTimestamp() {
        return this.updateTimestamp;
    }

    public void setUpdateTimestamp(final Date updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public Date getResolvedDate() {
        return resolvedDate;
    }

    public void setResolvedDate(Date resolvedDate) {
        this.resolvedDate = resolvedDate;
    }

    public Date getOpenedDate() {
        return openedDate;
    }

    public void setOpenedDate(Date openedDate) {
        this.openedDate = openedDate;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Boolean getFollowUpComplete() {
        return followUpComplete;
    }

    public void setFollowUpComplete(Boolean followUpComplete) {
        this.followUpComplete = followUpComplete;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    public abstract Set<ContactLogComment> getComments();

    public abstract void setComments(Set<ContactLogComment> comments);
}
