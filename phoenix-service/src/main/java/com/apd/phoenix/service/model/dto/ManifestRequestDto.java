/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author nreidelb
 */
public class ManifestRequestDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8813294263823214432L;
    private List<ManifestDto> manifests;

    /**
     * @return the manifests
     */
    public List<ManifestDto> getManifests() {
        return manifests;
    }

    /**
     * @param manifests the manifests to set
     */
    public void setManifests(List<ManifestDto> manifests) {
        this.manifests = manifests;
    }
}
