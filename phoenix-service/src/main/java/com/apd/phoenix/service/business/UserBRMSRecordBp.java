package com.apd.phoenix.service.business;

import java.util.List;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.UserBRMSRecord;
import com.apd.phoenix.service.model.UserRequest;
import com.apd.phoenix.service.persistence.jpa.UserBRMSRecordDao;

@Stateless
@LocalBean
public class UserBRMSRecordBp extends AbstractBp<UserBRMSRecord> {

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(UserBRMSRecordDao dao) {
        this.dao = dao;
    }

    public Boolean isActive(Long id) {
        return getFirst(((UserBRMSRecordDao) dao).isActive(id));
    }

    public Integer getSessionId(Long id) {
        return getFirst(((UserBRMSRecordDao) dao).getSessionId(id));
    }

    public UserBRMSRecord getRecordById(Long id) {
        return getFirst(((UserBRMSRecordDao) dao).getRecordByUserRequestId(id));
    }

    public UserBRMSRecord getRecordBySessionId(Integer sessionId) {
        return getFirst(((UserBRMSRecordDao) dao).getRecordBySessionId(sessionId));
    }

    private <T> T getFirst(List<T> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        return list.get(0);
    }

    public void createRecord(Long urid, Integer sessionId, Boolean active) {
        Properties versionProperties = PropertiesLoader.getAsProperties("guvnor");
        UserBRMSRecord newRecord = new UserBRMSRecord();
        newRecord.setActive(active);
        newRecord.setSessionId(sessionId);
        newRecord.setSnapshot(versionProperties.getProperty("snapshot"));
        UserRequest ur = new UserRequest();
        ur.setId(urid);
        newRecord.setUserRequest(ur);

        create(newRecord);
    }

    public void setActive(Long id, Boolean active) {
        UserBRMSRecord oldRecord = getRecordById(id);
        oldRecord.setActive(active);
        update(oldRecord);
    }

    public void setProcessId(Long id, long processId) {
        UserBRMSRecord oldRecord = getRecordById(id);
        oldRecord.setProcessId(processId);
        update(oldRecord);
    }

    public boolean isStale(Long id) {
        //TODO: Figure out how to determine if an account's processId is stale and implement
        return false;
    }

    public long getProcessId(Long id) {
        return getFirst(((UserBRMSRecordDao) dao).getProcessId(id));
    }

    public String getSnapshot(long id) {
        return getFirst(((UserBRMSRecordDao) dao).getSnapshot(id));
    }
}
