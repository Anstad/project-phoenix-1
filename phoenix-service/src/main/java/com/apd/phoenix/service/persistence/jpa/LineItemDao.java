/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.persistence.jpa;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import com.apd.phoenix.service.model.LineItem;

/**
 *
 * @author selrahal
 */
@Stateless
public class LineItemDao extends AbstractDao<LineItem> {

    @SuppressWarnings("unchecked")
    public LineItem getLineItemByLineNumberAndApdPo(String lineNumberValue, String apdPoNumber) {
        Integer lineNumber = new Integer(lineNumberValue);
        String hql = "SELECT lineItem FROM CustomerOrder AS customerOrder "
                + "LEFT JOIN customerOrder.poNumbers AS poNumber LEFT JOIN customerOrder.items as lineItem "
                + "WHERE poNumber.value=:apdPoNumber AND lineItem.lineNumber=:lineNumber";
        Query query = entityManager.createQuery(hql);
        query.setParameter("apdPoNumber", apdPoNumber);
        query.setParameter("lineNumber", lineNumber);
        List<LineItem> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    //TODO: refactor. Line items aren't guaranteed to have an APD SKU, they might just have a vendor SKU
    @SuppressWarnings("unchecked")
    public LineItem getLineItemByVendorSkuAndApdPo(String apdSku, String apdPoNumber) {
        String hql = "SELECT lineItem FROM CustomerOrder AS customerOrder "
                + "LEFT JOIN customerOrder.poNumbers AS poNumber LEFT JOIN customerOrder.items as lineItem "
                + "WHERE poNumber.value=:apdPoNumber AND lineItem.supplierPartId=:apdSku";
        Query query = entityManager.createQuery(hql);
        query.setParameter("apdPoNumber", apdPoNumber);
        query.setParameter("apdSku", apdSku);
        List<LineItem> results = query.getResultList();
        if (results.isEmpty()) {
            return null;
        }
        return results.get(0);
    }

    public LineItem hydrateLineItem(Long id) {
        String hql = "SELECT lineItem from LineItem AS lineItem LEFT JOIN FETCH lineItem.item as item"
                + " LEFT JOIN FETCH item.skus as sku" + " WHERE lineItem.id = :id";
        Query query = entityManager.createQuery(hql);
        query.setParameter("id", id);
        return (LineItem) query.getSingleResult();
    }

}
