package com.apd.phoenix.service.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.hibernate.envers.Audited;

@Entity
@DiscriminatorValue(value = "INVOICE_LOG")
public class InvoiceLog extends OrderLog {

    private static final long serialVersionUID = -4625520754314883328L;

    /** The customer order. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Invoice invoice;

    /**
     * @return the invoice
     */
    public Invoice getInvoice() {
        return invoice;
    }

    /**
     * @param invoice the invoice to set
     */
    public void setInvoice(final Invoice invoice) {
        this.invoice = invoice;
    }
}
