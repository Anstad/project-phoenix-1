package com.apd.phoenix.service.persistence.jpa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;

/**
 * CatalogXItem DAO stub
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class CatalogXItemDao extends AbstractDao<CatalogXItem> {

    private static final String APD = "APD";
    private static final String CHANGED_SKU = "changed sku number";
    private static final String JMS_BATCH_SIZE = "100";
    private static final String JMS_PROPERTIES = "catalog.upload.integration";
    private static final String CSV_BATCH_SIZE = "100";
    private static final int PRICE_BATCH_SIZE = 100;
    public static final String USPS_APD_DESIGNATED_VENDOR_ID = "usps";

    @Override
    public CatalogXItem update(CatalogXItem catalogXItem) {
        if (catalogXItem.getCatalog().getCustomer() != null) {
            if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
                CatalogXItem oldCatalogXItem = findById(catalogXItem.getId(), CatalogXItem.class);
                if (changedRelevantFields(catalogXItem, oldCatalogXItem)) {
                    catalogXItem.setModified(Boolean.TRUE);
                }
            }
        }
        return super.update(catalogXItem);
    }

    @Override
    public CatalogXItem create(CatalogXItem catalogXItem) {
        if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
            catalogXItem.setAdded(Boolean.TRUE);
        }
        return super.create(catalogXItem);
    }

    @Override
    public void createNoRefresh(CatalogXItem catalogXItem) {
        if (USPS_APD_DESIGNATED_VENDOR_ID.equals(catalogXItem.getCatalog().getCustomer().getApdAssignedAccountId())) {
            catalogXItem.setAdded(Boolean.TRUE);
        }
        super.createNoRefresh(catalogXItem);
    }

    public void deleteQuietly(Long id, Class<CatalogXItem> aClass) {
        super.delete(id, aClass);
    }

    /**
     * Takes a vendor name, and returns all CatalogXItem objects whose items have been replaced by a SKU change. 
     * Specifically, it returns a List of Object[] arrays, where each element in the List is an Array of 2 objects: 
     * a CatalogXItem whose Item has been replaced by a SKU change, and the Item that is the replacement.
     * 
     * @param vendorName - the name of the vendor whose products had a SKU change. 
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Object[]> skuQueryResult(String vendorName) {
        String hql = "SELECT catalogLink, newItem FROM CatalogXItem AS catalogLink, Item AS newItem JOIN "
                + "catalogLink.item.properties AS property JOIN newItem.skus AS sku WHERE "
                + "catalogLink.item.vendorCatalog.vendor.name LIKE :vendorName AND property.type.name LIKE " + "'"
                + CHANGED_SKU + "' AND sku.type.name LIKE '" + APD + "' AND sku.value LIKE property.value AND "
                + "newItem.vendorCatalog.vendor.name LIKE :vendorName AND catalogLink.catalog.vendor is null";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("vendorName", vendorName);
        return query.getResultList();
    }

    public CatalogXItem hydrateForItemBrowse(CatalogXItem toHydrate) {
        String hql = "SELECT catXItem FROM CatalogXItem AS catXItem INNER JOIN FETCH catXItem.item AS chosenItem "
                + "LEFT JOIN FETCH catXItem.substituteItem LEFT JOIN FETCH chosenItem.manufacturer AS "
                + "itemManufacturer WHERE catXItem.id = :catXItemId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catXItemId", toHydrate.getId());

        return getSingleResultOrNull(query);
    }

    public CatalogXItem hydrateForProductDetails(CatalogXItem toHydrate) {
        String hql = "SELECT catXItem FROM CatalogXItem AS catXItem INNER JOIN FETCH catXItem.item AS chosenItem "
                + "LEFT JOIN FETCH catXItem.substituteItem LEFT JOIN FETCH chosenItem.manufacturer AS "
                + "itemManufacturer WHERE catXItem.id = " + toHydrate.getId().toString();
        //Creates the query
        Query query = entityManager.createQuery(hql);
        return getSingleResultOrNull(query);
    }

    @SuppressWarnings("unchecked")
    public List<CatalogXItem> getCatalogXItemsSimilarItems(CatalogXItem thisCXItem, int maxResults) {

    	
    	Query test = entityManager.createQuery("SELECT relative.id from Item i JOIN i.similarItems as irl JOIN irl.relative AS relative"
    			                               +" WHERE i.id = :itemId");
        test.setParameter("itemId", thisCXItem.getItem().getId());
    	
    	List<Long> results = test.getResultList();
    	if (results.isEmpty()) {
    		return new ArrayList<>();
    	}
        Query query = entityManager.createQuery("SELECT cxi FROM CatalogXItem cxi WHERE cxi.catalog.id = :catalogId AND cxi.item.id IN (:resultList)");
        query.setParameter("catalogId", thisCXItem.getCatalog().getId());
        query.setParameter("resultList", results);
        setItemsReturned(query, 0, maxResults);
        return query.getResultList();
    }

    public CatalogXItem hydrateForLineItem(CatalogXItem toHydrate) {
        String hql = "SELECT catXItem FROM CatalogXItem AS catXItem LEFT JOIN FETCH catXItem.item AS chosenItem "
                + "LEFT JOIN FETCH chosenItem.manufacturer AS itemManufacturer LEFT JOIN FETCH chosenItem.skus LEFT JOIN FETCH "
                + "chosenItem.itemCategory LEFT JOIN FETCH chosenItem.unitOfMeasure LEFT JOIN FETCH chosenItem.vendorCatalog "
                + "AS catalog LEFT JOIN FETCH catalog.vendor AS vendor LEFT JOIN FETCH chosenItem.itemClassifications "
                + "AS itemClass LEFT JOIN FETCH itemClass.itemClassificationType LEFT JOIN FETCH itemClass.classificationCode "
                + "AS code LEFT JOIN FETCH code.codeType LEFT JOIN FETCH catXItem.properties LEFT JOIN FETCH "
                + "catXItem.substituteItem LEFT JOIN FETCH chosenItem.properties LEFT JOIN FETCH catXItem.favorites "
                + "WHERE catXItem.id = :catXItemId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("catXItemId", toHydrate.getId());

        setItemsReturned(query, 0, 0);
        return getSingleResultOrNull(query);
    }

    @SuppressWarnings("unchecked")
    public List<Integer> getQuantitiesForOrder(CustomerOrder order) {
        StringBuilder hql = new StringBuilder(
                "SELECT lineItem.quantity FROM "
                        + "LineItem as lineItem INNER JOIN lineItem.order as order WHERE order.id=:orderId ORDER BY lineItem.customerSku");
        Query query = entityManager.createQuery(hql.toString());
        query.setParameter("orderId", order.getId());
        return query.getResultList();
    }

    public void refresh(CatalogXItem toRefresh) {
        this.entityManager.refresh(toRefresh);
    }

    @SuppressWarnings("unchecked")
    public List<CatalogXItem> batchForCsv(Catalog catalog, int batch) {
    	Properties csvProperties = PropertiesLoader.getAsProperties(JMS_PROPERTIES);
    	int csvBatchSize = Integer.parseInt(csvProperties.getProperty("csvBatchSize", CSV_BATCH_SIZE));
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi WHERE cxi.catalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * csvBatchSize, csvBatchSize);
        List<Double> idList = preQuery.getResultList();
        if (idList.size() > 0) {
	        String hql = "SELECT DISTINCT cxi FROM CatalogXItem AS cxi " + CatalogXItemDao.joinQuery("cxi", CatalogXItem.class) 
	        		+ " LEFT JOIN FETCH cxi_item.skus LEFT JOIN FETCH "
	                + "cxi_item.vendorCatalog AS catalog LEFT JOIN FETCH catalog.vendor LEFT JOIN FETCH "
	                + "cxi_substituteItem.item AS subItem LEFT JOIN FETCH subItem.skus LEFT JOIN FETCH subItem.vendorCatalog "
	                + "AS subCatalog LEFT JOIN FETCH subCatalog.vendor WHERE cxi.id IN (:idList)";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("idList", idList);
	        return query.getResultList();
        }
        return new ArrayList<>();
    }

    public void detach(CatalogXItem item) {
        this.entityManager.detach(item);
    }

    @SuppressWarnings("unchecked")
    public List<CatalogXItem> batchForPrice(Catalog catalog, int batch) {
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi WHERE cxi.catalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", catalog.getId());
        setItemsReturned(preQuery, batch * PRICE_BATCH_SIZE, PRICE_BATCH_SIZE);
        List<Double> idList = preQuery.getResultList();
        if (idList.size() > 0) {
	        String hql = "SELECT DISTINCT cxi FROM CatalogXItem AS cxi LEFT JOIN FETCH cxi.item AS item LEFT JOIN FETCH "
	        	+ "item.vendorCatalog LEFT JOIN FETCH item.properties LEFT JOIN FETCH cxi.catalog WHERE cxi.id IN (:idList)";
	        //Creates the query
	        Query query = entityManager.createQuery(hql);
	        query.setParameter("idList", idList);
	        return query.getResultList();
        }
        return new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    public List<Long> batchForJMS(Catalog input, int batch) {
        Properties jmsProperties = PropertiesLoader.getAsProperties(JMS_PROPERTIES);
        int jmsBatchSize = Integer.parseInt(jmsProperties.getProperty("jmsBatchSize", JMS_BATCH_SIZE));
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi WHERE cxi.catalog.id = :id";
        Query preQuery = entityManager.createQuery(preHql);
        preQuery.setParameter("id", input.getId());
        setItemsReturned(preQuery, batch * jmsBatchSize, jmsBatchSize);
        return preQuery.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Long> batchAllForJMS(int batch) {
        Properties jmsProperties = PropertiesLoader.getAsProperties(JMS_PROPERTIES);
        int jmsBatchSize = Integer.parseInt(jmsProperties.getProperty("allJmsBatchSize", JMS_BATCH_SIZE));
        String preHql = "SELECT cxi.id FROM CatalogXItem AS cxi";
        Query preQuery = entityManager.createQuery(preHql);
        setItemsReturned(preQuery, batch * jmsBatchSize, jmsBatchSize);
        return preQuery.getResultList();
    }

    //Updates the entity without showing that the item was modified, used to clear the modified field
    public void updateQuietly(CatalogXItem itemEntity) {
        super.update(itemEntity);
        entityManager.flush();
    }

    //price is only relevant field
    private boolean changedRelevantFields(CatalogXItem item, CatalogXItem oldCatalogXItem) {
        if (item.getPrice() == null) {
            return oldCatalogXItem.getPrice() == null;
        }
        else {
            return item.getPrice().equals(oldCatalogXItem.getPrice());
        }
    }

    @SuppressWarnings("unchecked")
    public List<Long> getUspsChangedItemsIds() {
        String hql = "SELECT cxi.id FROM CatalogXItem AS cxi JOIN cxi.item AS item "
                + "JOIN cxi.catalog AS cat WHERE cat.customer.apdAssignedAccountId = 'usps' "
                + "AND (coalesce(cxi.modified, false) = true OR coalesce(cxi.added, false) = true "
                + "OR coalesce(cxi.deleted, false) = true OR coalesce(item.externalCatalogChange,false) = true "
                + "OR coalesce(cat.changedItemRelevantFields,false) = true) "
                //Do not include items which were added and then removed before the customer updated their catalog
                + "AND NOT (coalesce(cxi.added, false) = true AND coalesce(cxi.deleted, false) = true)";
        Query query = entityManager.createQuery(hql);
        return query.getResultList();
    }

    public CatalogXItem getCatalogXItem(Item resultItem, long catalogId) {
        CatalogXItem catalogXItem = new CatalogXItem();
        catalogXItem.setItem(resultItem);
        catalogXItem.setCatalog(new Catalog());
        catalogXItem.getCatalog().setId(catalogId);
        List<CatalogXItem> catxItems = searchByExactExample(catalogXItem, 0, 0);
        if (catxItems.isEmpty()) {
            return null;
        }
        else {
            catalogXItem = catxItems.get(0);
            catalogXItem = this.findById(catalogXItem.getId(), CatalogXItem.class);
        }
        return catalogXItem;
    }

    public CatalogXItem searchByCustomerSku(String sku, Catalog catalog) {
        String hql = "SELECT catX FROM CatalogXItem AS catX "
                + "WHERE catX.customerSku.value = :sku AND catX.catalog.id = :catId";
        //Creates the query
        Query query = entityManager.createQuery(hql);
        query.setParameter("sku", sku);
        query.setParameter("catId", catalog.getId());
        return (CatalogXItem) getSingleResultOrNull(query);
    }

    public String getOverrideCatalogs(Long id) {
        // get the hibernate session from the injected entity manager.
        Session session = (Session) entityManager.getDelegate();

        String overridesQuery = "select "
                + "        overrideCxi.catalog_id AS OVERRIDES_ID "
                + "        from CatalogXItem overrideCxi "
                + "       	JOIN "
                + "       		("
                + "        			select "
                + "        				catalog.id AS id, CONNECT_BY_ROOT catalog.id AS root_id "
                + "       			from Catalog start with catalog.customer_id is not null "
                + "       				connect by catalog.parent_id = PRIOR catalog.id "
                + "       		) overrideCatalog "
                + "        		ON overrideCatalog.id = overrideCxi.catalog_id AND overrideCatalog.root_id != overrideCatalog.id "
                + "      	WHERE overrideCatalog.root_id = (SELECT CATALOG_ID FROM CATALOGXITEM WHERE ID = :cxiId) AND overrideCxi.item_id = (SELECT ITEM_ID FROM CATALOGXITEM WHERE ID = :cxiId) ";
        final org.hibernate.Query itemQuery = session.createSQLQuery(overridesQuery);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter("cxiId", id);

        List<Map<String, Object>> results = (List<Map<String, Object>>) itemQuery.list();

        if (results == null) {
            return null;
        }

        String toReturn = "";

        for (Map<String, Object> result : results) {
            String overridesIds = result.get("OVERRIDES_ID") == null ? "" : ((BigDecimal) result.get("OVERRIDES_ID"))
                    .toString();
            if (StringUtils.isNotBlank(overridesIds)) {
                if (StringUtils.isNotBlank(toReturn)) {
                    toReturn = toReturn.concat(",");
                }
                toReturn = toReturn.concat(overridesIds);
            }
        }

        return toReturn;
    }

}
