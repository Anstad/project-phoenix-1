package com.apd.phoenix.service.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Invoice;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.VendorCreditInvoice;
import com.apd.phoenix.service.model.VendorInvoice;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.VendorInvoiceDao;
import java.math.BigInteger;
import java.util.List;

/**
 * 
 * @author RHC
 *
 */
@Stateless
@LocalBean
public class VendorInvoiceBp extends AbstractBp<VendorInvoice> {

    @Inject
    InvoiceBp invoiceBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    private VendorCreditInvoiceBp vendorCreditInvoiceBp;

    /**
     * This method is injected with the correct Dao for initialization
     * 
     * @param dao
     */
    @Inject
    public void initDao(VendorInvoiceDao dao) {
        this.dao = dao;
    }

    public VendorInvoice persistVendorInvoice(VendorInvoiceDto invoiceDto) {
        VendorInvoice vendorInvoice = new VendorInvoice();
        PurchaseOrderDto purchaseOrderDto = invoiceDto.getCustomerOrderDto();

        //sets the invoice to invalid of there is an item that is not valid
        vendorInvoice.setActualItems(new ArrayList<LineItem>());
        for (LineItemDto lineItemDto : invoiceDto.getActualItems()) {
            if (lineItemDto != null) {
                vendorInvoice.getActualItems().add(
                        customerOrderBp.createLineItemForVendorInvoice(lineItemDto, purchaseOrderDto));
                if (Boolean.FALSE.equals(lineItemDto.getValid())) {
                    vendorInvoice.setValid(false);
                }
            }
        }

        if (vendorInvoice.getValid() == null || vendorInvoice.getValid()) {
            vendorInvoice.setApprovalDate(new Date());
        }

        //Calculate the total cost of the actual valid line items on the invoice
        BigDecimal amount = BigDecimal.ZERO;
        for (LineItem lineItem : vendorInvoice.getActualItems()) {
            amount = amount.add(lineItem.getCost().multiply(new BigDecimal(lineItem.getQuantity())));
        }

        vendorInvoice.setAmount(amount);

        vendorInvoice.setBillingTypeCode(invoiceDto.getBillingTypeCode());
        vendorInvoice.setDate(new Date());
        vendorInvoice.setInvoiceDate(invoiceDto.getInvoiceDate());
        vendorInvoice.setDueDate(invoiceDto.getDueDate());
        vendorInvoice.setInvoiceNumber(invoiceDto.getInvoiceNumber());
        vendorInvoice.setSpecialInstructions(invoiceDto.getSpecalInstructions());

        String apdPo = invoiceDto.getCustomerOrderDto().retrieveApdPoNumber().getValue();
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(apdPo);

        //update line items on customerOrder with quanity invoiced by vendor
        for (LineItem invoiceItem : vendorInvoice.getActualItems()) {
            boolean found = false;
            for (LineItem orderItem : customerOrder.getItems()) {
                if (orderItem != null && DtoFactory.createLineItemDto(orderItem).matchesItemOnOrder(invoiceItem)) {
                    final BigInteger newlyInvoiced = new BigInteger(invoiceItem.getQuantity().toString());
                    orderItem.setQuantityInvoicedByVendor(orderItem.getQuantityInvoicedByVendor().add(newlyInvoiced));
                    lineItemBp.update(orderItem);
                    found = true;
                    break;
                }
            }
            if (!found) {
                LOG.error("NO ORDER LINE ITEM FOUND FOR ITEM WITH APD SKU=" + invoiceItem.getApdSku() + " VENDOR SKU="
                        + invoiceItem.getManufacturerPartId());
            }
        }

        customerOrder = customerOrderBp.update(customerOrder);
        vendorInvoice.setCustomerOrder(customerOrder);

        return this.create(vendorInvoice);
    }

    public List<Invoice> findInvoicesByOrder(CustomerOrder order) {
        List<Invoice> toReturn = new ArrayList<Invoice>();
        if (order == null || order.getId() == null) {
            return toReturn;
        }
        VendorInvoice searchInvoice = new VendorInvoice();
        searchInvoice.setCustomerOrder(new CustomerOrder());
        searchInvoice.getCustomerOrder().setId(order.getId());
        toReturn.addAll(this.searchByExactExample(searchInvoice, 0, 0));

        VendorCreditInvoice searchCreditInvoice = new VendorCreditInvoice();
        searchCreditInvoice.setCustomerOrder(new CustomerOrder());
        searchCreditInvoice.getCustomerOrder().setId(order.getId());
        toReturn.addAll(vendorCreditInvoiceBp.searchByExactExample(searchCreditInvoice, 0, 0));

        return toReturn;
    }
}
