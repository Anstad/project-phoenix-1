/*
 * 
 */
package com.apd.phoenix.service.model;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.apd.phoenix.service.business.EntityComparator.GenericComparator;

/**
 * The Class ServiceLog.
 */
@Entity
@Table(name = "SERVICE_LOG")
public class ServiceLog extends ContactLog {

    private static final long serialVersionUID = -1830113389129708195L;

    /** The Account. */
    @ManyToOne(fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    private SystemUser apdCsrRep;

    @ManyToOne(fetch = FetchType.EAGER)
    private ContactMethod contactMethod;

    @ManyToOne(fetch = FetchType.EAGER)
    private ServiceReason serviceReason;

    @Column(length = 3000)
    private String serviceDetails;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "servicelog_id")
    private Set<ContactLogComment> comments = new HashSet<ContactLogComment>();

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "servicelog_id")
    private Set<OrderAttachment> attachments = new HashSet<OrderAttachment>();

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public SystemUser getApdCsrRep() {
        return apdCsrRep;
    }

    public void setApdCsrRep(SystemUser apdCsrRep) {
        this.apdCsrRep = apdCsrRep;
    }

    public ContactMethod getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(ContactMethod contactMethod) {
        this.contactMethod = contactMethod;
    }

    public ServiceReason getServiceReason() {
        return serviceReason;
    }

    public void setServiceReason(ServiceReason serviceReason) {
        this.serviceReason = serviceReason;
    }

    public String getServiceDetails() {
        return serviceDetails;
    }

    public void setServiceDetails(String serviceDetails) {
        this.serviceDetails = serviceDetails;
    }

    public Set<ContactLogComment> getComments() {
        return comments;
    }

    public void setComments(Set<ContactLogComment> comments) {
        this.comments = comments;
    }

    public Set<OrderAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(Set<OrderAttachment> attachments) {
        this.attachments = attachments;
    }

    public static class ServiceLogComparator implements Comparator<ServiceLog> {

        @Override
        public int compare(ServiceLog log0, ServiceLog log1) {

            if (log0.getOpenedDate() != null && log1.getOpenedDate() != null) {
                return log0.getOpenedDate().compareTo(log1.getOpenedDate());
            }
            return (new GenericComparator()).compare(log0, log1);
        }
    }
}
