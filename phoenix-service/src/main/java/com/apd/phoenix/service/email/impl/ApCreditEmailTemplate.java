/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.email.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailTemplate;
import com.apd.phoenix.service.email.api.NoAttachmentContentException;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import freemarker.template.TemplateException;

/**
 *
 * @author RHC
 */
public class ApCreditEmailTemplate extends EmailTemplate<VendorCreditInvoiceDto> {

    private static final String TEMPLATE = "ap.credit";

    @Override
    public String createBody(VendorCreditInvoiceDto vendorCreditInvoiceDto) throws IOException, TemplateException {
        Map<String, Object> params = new HashMap<>();
        if (vendorCreditInvoiceDto != null) {
	        if (StringUtils.isNotBlank(vendorCreditInvoiceDto.getInvoiceNumber())) {
	        	params.put("invoiceNumber", vendorCreditInvoiceDto.getInvoiceNumber());
	        }
	        if (StringUtils.isNotBlank(vendorCreditInvoiceDto.getRaNumber())) {
	        	params.put("raNumber", vendorCreditInvoiceDto.getRaNumber());
	        }
	        if (vendorCreditInvoiceDto.getCustomerOrderDto() != null && StringUtils.isNotBlank(vendorCreditInvoiceDto.getCustomerOrderDto().getApdPoNumber())) {
	        	params.put("apdPo", vendorCreditInvoiceDto.getCustomerOrderDto().getApdPoNumber());
	        }
	        BigDecimal amount = BigDecimal.ZERO;
	        if (vendorCreditInvoiceDto.getItems() != null) {
	        	for (LineItemXReturnDto item : vendorCreditInvoiceDto.getItems()) {
	        		if (item != null) {
	        			BigDecimal quantity = null;
	        			BigDecimal price = null;
	        			if (item.getQuantity() != null) {
	        				quantity = new BigDecimal(item.getQuantity().intValue());
	        			} else if (item.getLineItemDto() != null && item.getLineItemDto().getQuantity() != null) {
	        				quantity = new BigDecimal(item.getLineItemDto().getQuantity().intValue());
	        			}
	        			if (item.getLineItemDto() != null && item.getLineItemDto().getCost() != null) {
	        				price = item.getLineItemDto().getCost();
	        			} else if (item.getLineItemDto() != null && item.getLineItemDto().getUnitPrice() != null) {
	        				price = item.getLineItemDto().getUnitPrice();
	        			}
	        			if (quantity != null && price != null) {
	        				amount = amount.add(quantity.multiply(price));
	        			}
	        		}
	        	}
	        }
	        if (vendorCreditInvoiceDto.getRestockingFee() != null) {
	        	amount = amount.subtract(vendorCreditInvoiceDto.getRestockingFee());
	        }
	        params.put("amount", amount.negate());
        }
        return this.create(params);
    }

    @Override
    public String getTemplate() {
        return ApCreditEmailTemplate.TEMPLATE;
    }

    @Override
    public Attachment createAttachment(VendorCreditInvoiceDto vendorCreditInvoiceDto)
            throws NoAttachmentContentException {
        return null;
    }

}
