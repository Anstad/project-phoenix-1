[condition][]There is a workflow process instance=$pi:WorkflowProcessInstanceImpl()
[condition][]There is an {type} in the workflow variable {var}=${type}:{type}() from $pi.getVariable({var})
[condition][]There is a {type} in a VendorManifestsResponse manifest list=${type}:{type}() from $VendorManifestsResponse.manifests
[consequence][]Set process variable {key} to {value}=$pi.setVariable({key},{value});
[condition][]- with {field} greater than {value}={field} > {value}
[*][]- with {field} less than or equal to {value}={field} <= {value}
[condition][]- with {field} not equal to {value}={field} != {value}
[condition][]- with {field} equal to {value}={field} == {value}
