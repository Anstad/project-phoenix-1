[condition][]There is an order with status {status}=WorkflowProcessInstanceImpl($corder: variables['customerOrder'] ) CustomerOrder(status.value == {status}) from $corder
[consequence][]Set {key} to {value}=processInstance.setVariable({key},{value});
[condition][]There is a workflow with id {processId}=processInstance:WorkflowProcessInstanceImpl(processId=={processId})
[condition][]There is an Invoiced line item=lineItem: LineItem()
[condition][]There is a Billed line item=itemBilled: BilledLineItem()
[condition][]- with same {field}=this.{field} == lineItem.{field}
[condition][]- the gross margin is less than {margin}=((lineItem.unitPrice-this.price)/lineItem.unitPrice) < {margin}
[consequence][]Flag item as invalid=itemBilled.setInvalid(true);
[consequence][]Add error message to item:{message}=itemBilled.getErrorMessages().add({message});
[condition][]- with unequal {field}=this.{field} != lineItem.{field}
[condition][]- with {field} equal to {value}=this.{field} == {value}
[condition][]- {field} is not {value}=this.{field} != {value}
[condition][]- with line item status {value}=this.status.value == {value}
[condition][]- where line item status not {value}=this.status.value != {value}
