[condition][]There is an order line item=lineItem:LineItem()
[condition][]There is a shipment notification from vendor=itemShipped:ShippedLineItem()
[condition][]There is an? {type} line item=item{type}:{type}LineItem()
[condition][]- with same {field}=this.{field} == lineItem.{field}
[condition][]- the {field} is not equal=this.{field} != lineItem.{field}
[consequence][]Flag {type} item as invalid=item{type}.setInvalid(true);
[consequence][]Add error message to {type} item:{message}=item{type}.getErrorMessages().add({message});
