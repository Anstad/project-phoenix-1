<p>A charge has been declined for the following amount:
${result.totalAmount?string.currency}, for order number: ${result.apdPo}. This is a result of shipment ${result.trackingNum}
</p>
<p>
On the following card:<br />
${result.nameOnCard} <br />
${result.cardIdentifier} <br />
${result.cardExpriation} <br />
