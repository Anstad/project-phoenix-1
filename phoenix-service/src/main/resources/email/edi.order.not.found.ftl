<html>
<body>
<h2>
An inbound message could not be linked to an existing order.
</h2>
<p>
<#if dto.apdPo??>The document expected an order with APD PO of ${dto.apdPo}, but no such order was found in the system.
<#else>No apd PO was given for the inbound document.</#if> This issue will have to be remediated manually.
</p>
<p>The raw message for the inbound document is attached.</p>
</body>
</html>