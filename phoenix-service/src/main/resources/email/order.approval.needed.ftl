<html>
<body>
<p>The following order has been placed by <#if userName??>${userName}</#if> <#if userEmail??>(${userEmail})</#if> and needs your approval. Please login and check your tasks or select an option below to approve/deny.

<p>Placed on ${orderDate}

<p>Billing Information<br />
${billingAddress.line1}<br />
${billingAddress.city}, ${billingAddress.state} ${billingAddress.zip}<br />

<p>Shipping Information<br />
${shippingAddress.line1}<br />
${shippingAddress.city}, ${shippingAddress.state} ${shippingAddress.zip}<br />

<table>
  <tr>
    <th>Quantity</th>
    <th>SKU</th>
    <th>Description</th>
    <th>Status</th>
    <th>Unit Price</th>
    <th>Unit of Measure</th>
    <th>Total</th>
  </tr>
<#list items as item>
  <tr>
    <td>${item.quantity}</td>
    <td>${item.apdSku}</td>
    <td><#if item.shortName??>${item.shortName}<#elseif item.description??>${item.description}</#if></td>
    <td>${item.status.value}</td>
    <td>${item.unitPrice?string.currency}</td>
    <td>${item.unitOfMeasure.name}</td>
    <td>${item.subTotal?string.currency}</td>
  </tr>
</#list>
</table>
<br />
<a href="${approveUrl}">Approve this order</a><br />
<a href="${denyUrl}">Deny this order</a><br />

<p>Total Merchandise:........${merchandiseTotal?string.currency}
<p>Est. Shipping/Handling:...${shippingTotal?string.currency}
<p>Tax:......................${taxTotal?string.currency}
<p>Total:....................${orderTotal?string.currency}

</body>
</html>
