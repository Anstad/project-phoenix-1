<html>
    <body>
    	<p>
    	<#if noManagerApproval>
    	Welcome to APD! Your request has been received and is being reviewed for processing. We will email you directly with an initial password and log-in instructions. 
    	<#else>
       Welcome to APD! Your request has been received and is being reviewed for processing. Once your manager(s) have approved this request, we will email you directly
       with an initial password and log-in instructions.    
       </#if>
       </p>
       <p>
       <#if userId??>Your user id is: ${userId}</#if>
       </p>
       <p>
       Thank you for choosing APD for all of your office supply needs!
       </p>
       <p>
       DO NOT REPLY TO THIS MESSAGE
       </br>
       THIS IS AN UNMONITORED MAILBOX. FOR ASSISTANCE CALL OUR NATIONAL CLIENT SUPPORT DESKT AT 1-877-381-1664
       </p>
    </body>
</html>