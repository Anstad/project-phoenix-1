<html>
<body>
<h1>
Order Attempted to Process Twice
</h1>
<h2>
An order, APD PO: <#if apdPo??>${apdPo}</#if> which was already in workflow, received a "Start Process" command. The command was discarded.
</h2>
</body>
</html>