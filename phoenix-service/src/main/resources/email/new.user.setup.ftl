<html>
    <body>
       	<p>Hello ${fullName},</p>

		<p>Thank you for registering for access to APD Marketplace. Your new username is: ${login}.  You may login at https://apdmarketplace.com.</p>

		<p>If your company is tax-exempt, then please contact us at 1-800-849-5842 or Sales@AmericanProduct.com.</p>

<p>Forgot your password? You may reset your password at any time using the instructions below:</p>

<p>1 Reset your password using your new username. Please go to 
https://apdmarketplace.com/shopping/ecommerce/user/resetPassword.xhtml, enter your new username (see above), and click Submit.</p>

<p>2 Check your email for a link to submit your new password. After confirming your username, you will receive an email with a time-sensitive link. Click that link to go to the page where you may create your new password.</p> 

<p>3 Bookmark the new page, log in, and begin shopping. After submitting your new password, you will be redirected to the login page (https://apdmarketplace.com). Please bookmark it in your browser and log in with your new username and password.</p> 

<p>If you have questions about your orders, then please contact us at CustomerService@AmericanProduct.com or call us toll-free at 1-800-849-5842.</p>

<p>Thank you for your business!</p>
</body>
</html>