<html>
<body>
<p>Shipment with tracking number ${trackingNumber}, on Order ${apdPo} has the following errors:

<table>
  <tr>
    <th>SKU</th>
    <th>QTY Shipped</th>
    <th>Error</th>
  </tr>
<#list items as item>
  <#list item.lineItemDto.validationErrorDtos as error>
  <tr>
    <td><#if item.lineItemDto.apdSku??>${item.lineItemDto.apdSku}<#elseif item.lineItemDto.supplierPartId??>${item.lineItemDto.supplierPartId}</#if></td>
    <td><span style="text-align:center">${item.quantity}</span></td>
    <td>${error.error}</td>
  </tr>
  </#list>
</#list>
</table>

</body>
</html>
