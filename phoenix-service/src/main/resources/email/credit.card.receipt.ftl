<p>A charge has been applied to your card for
${result.totalAmount?string.currency}, for order number: ${result.apdPo}. This is a result of shipment ${result.trackingNum}.
</p>
