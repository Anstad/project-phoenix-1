<html>
<body>
<h1>
Duplicate Customer PO detected
</h1>
<h2>
An order, APD PO: <#if customerOrder.apdPoNumber??>${customerOrder.apdPoNumber}</#if> was placed with duplicate customer PO: <#if customerOrder.customerPoNumber??>${customerOrder.customerPoNumber}</#if>. The order was not allowed to proceed to workflow. 
</h2>
</body>
</html>