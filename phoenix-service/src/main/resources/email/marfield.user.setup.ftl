<html>
    <body>
       	<p>Hello ${fullName},</p>

		<p>Your new username is: ${login}</p>
		
		<p>Please follow these steps to log in:</p> 
		
		<ol>
			<li> Add apdmarketplace.com to trusted sites. To ensure the APD Punchout loads correctly in Internet Explorer, Firefox or Chrome, follow the instructions in the User Guide (www.marfield.net/production/storefront/PepsiCo/PepsiCo-MarfieldShopperGuide.pdf).</li> 
			
			<li> Reset Your Password using new username. Please go to 
			https://marfield.apdmarketplace.com/shopping/ecommerce/user/resetPassword.xhtml to enter your new username (see above) and click Submit.</li>			
			
			<li> Check your email for a link to submit your new password. After confirming your username, you will receive an email with a time-sensitive link. Click that link to the page where you may submit your new password. </li> 
			
			<li> Bookmark the new page, Log In, and Begin Shopping. After submitting your new password, you will be redirected to the login page (https://marfield.apdmarketplace.com/). Please bookmark it in your browser and log in with your new username and password.</li> 
		</ol>
		<p>You will then be able to enter your orders for business cards, stationery, and related personalized products from Marfield as usual!
		If you have questions about your orders, please contact service@marfield.com or call toll-free 877.245.9122.
		Thank you for your business! </p>
        
    </body>
</html>