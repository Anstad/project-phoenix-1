<table style="width:100%">
	<col width="50%">
	<tr>
		<td><b>APD Order #${apdPo}</b></td>
	</tr>
</table>

<div style="width:100%; text-align:center; padding-top:15px; position: relative;">
<h2>LATE SHIPMENT NOTICE</h2><br />
</div>


<table style="width:100%; padding-top: 5px">
	<col width="100%">	
	<tr>
		<td style="padding-top: 15px; text-align: left;">
		<p>The items shown below have been placed in backorder status and will be shipped at a later date. An American Product Distributors Customer Service Representative will call to advise you of the delivery date. (Estimated Arrival time is 3 to 10 business days).
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 15px">
	<col width="25%">	
	<col width="50%">
	<col width="25%">
	<tr>
		<td><p><b>Billing Information</b><br />
				${billingAddress.name}<br />
				<#if billingAddress.companyName??>${billingAddress.companyName}</#if><br />
				${billingAddress.line1}<br />
				${billingAddress.city}, ${billingAddress.state} ${billingAddress.zip}<br /></td>		
		<td></td>
		<td><p><b>Shipping Information</b><br />
				${shippingAddress.name}<br />
				<#if shippingAddress.companyName??>${shippingAddress.companyName}</#if><br />
				${shippingAddress.line1}<br />
				${shippingAddress.city}, ${shippingAddress.state} ${shippingAddress.zip}<br />
				<#if desktop??>Desktop: ${desktop}<br /></#if>
                <#if building??>Building: ${building}<br /></#if>
                <#if costcenter??>Cost Center: ${costcenter}<br /></#if>
                <#if phone??>Phone: ${phone}<br /></#if>
		</td>
	</tr>
</table>

<table style="width:100%; padding-top: 5px">
	<col width="25%">	
	<col width="5%">	
	<tr>
		<td><b>Customer PO#: </b> ${customerPo}</td>
	</tr>
	<tr>
		<td><b>APD Order #: </b> ${apdPo}</td>
	</tr>
	<tr>
		<td><b>Order Date: </b>${orderDate}</td>
	</tr>	
</table>


<table style="tableLayout:auto; width:100%; margin-top: 20px; padding-top: 20px; position: relative; border:1px solid black;">
  <tr>
  	<th style="border:1px solid black; background-color: #C0C0C0">Total Qty Ordered</th>
  	<th style="border:1px solid black; background-color: #C0C0C0">Total Qty Shipped</th>
    <th style="border:1px solid black; background-color: #C0C0C0">SKU #</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Description</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Status</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Unit Price</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Unit of Measure</th>
    <th style="border:1px solid black; background-color: #C0C0C0">Total</th>
  </tr>
<#list items as item>
  <tr>
  	<td style="border:1px solid black;">${item.quantity}</td>
  	<td style="border:1px solid black;">${item.quantityShipped}</td>
    <td style="border:1px solid black;">${item.apdSku}</td>
    <td style="border:1px solid black;"><#if item.shortName??>${item.shortName}<#elseif item.description??>${item.description}</#if></td>
    <td style="border:1px solid black;">${item.status.value}</td>
    <td style="border:1px solid black;">${item.unitPrice?string.currency}</td>
    <td style="border:1px solid black;">${item.unitOfMeasure.name}</td>
    <td style="border:1px solid black;">${item.totalPrice?string.currency}</td>
  </tr>
</#list>
</table>

<div style="padding-top:30px">
<p>Thank you for ordering from American Product Distributors, Inc. If you have any questions, call your dedicated Customer Service team @ <b><#if csPhone??>${csPhone}<#else>1-800-849-5842</#if></b></p>
</div>

</body>
</html>