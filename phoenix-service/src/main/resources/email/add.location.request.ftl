<html>
    <body>
        <h1>Add Location Request</h1>
        <br />
        Please ADD A LOCATION for the user using the information provided below
        <br />
        
         <p>
       <b> USER INFORMATION </b>
        </p>
   
   		     <p>
        <#if requestType?has_content>Request Type: ${requestType}<br /></#if>
        <#if userId?has_content>ID: ${userId}<br /></#if>
        <#if firstName?has_content>First Name: ${firstName}<br /></#if>
        <#if lastName?has_content>Last Name: ${lastName}<br /></#if>
        <#if email?has_content>E-mail: ${email}<br /></#if>
        <#if phone?has_content>Phone: ${phone} <#if ext??>EXT: ${ext}</#if><br /></#if>
        </p>
          <p>
       <b> LOCATION INFORMATION</b>
        </p>
   
        <p>
        <#if sAddress1?has_content>Address: ${sAddress1}<br /></#if>
        <#if sAddress2?has_content>${sAddress2}<br /></#if>
        <#if sCity?has_content>City: ${sCity}<br /></#if>
        <#if sState?has_content>State: ${sState}<br /></#if>
        <#if sZip?has_content>Zip: ${sZip}<br /></#if>
        <#if sDesktop?has_content>Desktop: ${sDesktop}<br /></#if>
        <#if sDepartment?has_content>Department: ${sDepartment}<br /></#if>
        <#if sCostCenter?has_content>Cost Center: ${sCostCenter}<br /></#if>
        <#if sComments?has_content>Comments: ${sComments}<br /></#if>
        </p>
        
    </body>
</html>