[condition][]There is an(?) customer {type}=customer{type}: {type}()
[consequence][]Create instance : "{value}"=insert(new Instance("{value}"));
[condition][]There is no current Instance with field : "{value}"=not Instance(field == "{value}")
[consequence][]Retract the fact : '{variable}'=retract({variable}); //this would retract bound variable {variable}
[consequence][]Set {key} to {value}=processInstance.setVariable({key},{value});
[condition][]There is a workflow with id {processId}=processInstance:WorkflowProcessInstanceImpl(processId=={processId})
[condition][]- with {field} equal to {value}=this.{field} == {value}
[condition][]- with {field} is not {value}=this.{field} != {value}
[condition][]- with order {field} less than threshold=this.{field}<processInstance.getVariable('order_weight_threshold')
[consequence][]Update order account to NSN product account=customerOrder.setAccountNumber(orderNSNLineItem.getAccountNumber());
[condition][]There is an(?) {type} item in order=order{type}:{type}()
[consequence][]Update order account to Hazmat product account=customerOrder.setAccountNumber(orderHazmatLineItem.getAccountNumber());
[consequence][]Update order account to OverWeightSize product account=customerOrder.setAccountNumber(orderOverWeightSizeLineItem.getAccountNumber());
[condition][]- with order {field} equal greater than threshold=this.{field}>=processInstance.getVariable('order_weight_threshold')
[consequence][]Update order account to default account=customerOrder.setAccountNumber((String)processInstance.getVariable('account_number'));
[consequence][]Update order account to {value}=customerOrder.setAccountNumber({value});
[consequence][]Update order facility to {value}=customerOrder.setFacility({value});
