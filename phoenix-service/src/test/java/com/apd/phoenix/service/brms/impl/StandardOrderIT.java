package com.apd.phoenix.service.brms.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.SignalCommand;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;
import com.apd.phoenix.service.invoice.ShippedLineItem;
import com.apd.phoenix.service.message.AckLineItem;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.PoNumberType;

@RunWith(Arquillian.class)
public class StandardOrderIT {

    private static final Logger LOG = LoggerFactory.getLogger(StandardOrderIT.class);

    @Inject
    BRMSCommandCreator commandCreator;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.executor", "com.apd.phoenix.service.brms.impl",
                "com.apd.phoenix.service.model", "com.apd.phoenix.service.invoice").addClasses().addAsWebInfResource(
                EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                resolver.artifacts("org.jbpm:jbpm-human-task:5.3.1.BRMS").resolveAsFiles());

        //LOG.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void edi855() throws InterruptedException {
    	Long orderId = new Long(1870);
    	List<Object> facts = new ArrayList<>(); 
    	Map<String, Object> processParams = new HashMap<String, Object>();
    	
    	LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();

        item1.setUnitPrice(new BigDecimal("7.99"));
        lis.setValue("Submitted");
        item1.setStatus(lis);
        item1.setQuantity(1);
        item1.setApdSku("1234");
        
        
        List<AckLineItem> lineItems = new ArrayList<>();
        AckLineItem sli1 = new AckLineItem(1, "1234", "1234", BigDecimal.ZERO, "IB");
        AckLineItem sli2 = new AckLineItem(1, "1234", "1234", BigDecimal.ZERO, "IR");
        AckLineItem sli3 = new AckLineItem(1, "1234", "1234", BigDecimal.ZERO, "IA");
        lineItems.add(sli1);
        lineItems.add(sli2);
        lineItems.add(sli3);
        
        
        processParams.put("isManual", false);
    	processParams.put("isOrderComplete", false);
    	processParams.put("edi855OrderStatus", "Accepted by vendor");
    	processParams.put("orderId", orderId);
        processParams.put("lineItems", lineItems);
        
        commandCreator.sendStartProcess(StartProcessCommand.Name.STANDARD_ORDER, StartProcessCommand.Type.ORDER, orderId, processParams, null, StartProcessCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
        
        facts.add(item1);
        facts.add(sli1);
        facts.add(sli2);
        facts.add(sli3);
        
        commandCreator.sendSignalEvent(SignalCommand.Name.ORDER_ACK, lineItems, SignalCommand.Type.ORDER, orderId, facts, SignalCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
    }

    @Ignore
    public void edi856() throws InterruptedException {
    	List<Object> facts = new ArrayList<>(); 
    	Map<String, Object> processParams = new HashMap<String, Object>();
    	
    	LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();

        item1.setUnitPrice(new BigDecimal("7.99"));
        lis.setValue("Submitted");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        //
        CustomerOrder co = new CustomerOrder();
        OrderStatus os = new OrderStatus();
        os.setValue("Submitted");
        co.setStatus(os);
        PoNumber pon = new PoNumber();
        pon.setValue("12345");
        pon.setType(new PoNumberType());
        pon.getType().setName("APD");
        co.getPoNumbers().add(pon);
        Credential credential = new Credential();
        PaymentInformation payInfo = new PaymentInformation();
        PaymentType payType = new PaymentType();
        payType.setName("cc");
        payInfo.setPaymentType(payType);
        credential.setPaymentInformation(payInfo);
        item1.setOrder(co);

        List<ShippedLineItem> lineItems = new ArrayList<>();
        ShippedLineItem sli1 = new ShippedLineItem(1, "1234", "1234", BigDecimal.ZERO, "Shipped", "Z1234");
        lineItems.add(sli1);
        

    	processParams.put("isManual", false);
    	processParams.put("isOrderComplete", false);
        processParams.put("isvalid", true);
        processParams.put("invoiceMethod", credential.getPaymentInformation().getPaymentType().getName());
        processParams.put("lineItems", lineItems);
        
        commandCreator.sendStartProcess(StartProcessCommand.Name.STANDARD_ORDER, StartProcessCommand.Type.ORDER, new Long(1870), processParams, null, StartProcessCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
        
        facts.add(item1);
        facts.add(sli1);
        
        commandCreator.sendSignalEvent(SignalCommand.Name.SHIPPED, lineItems, SignalCommand.Type.ORDER, new Long(1870), facts, SignalCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
    }
}
