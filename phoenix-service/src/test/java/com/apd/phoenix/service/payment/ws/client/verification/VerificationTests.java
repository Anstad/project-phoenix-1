/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.verification;

import com.apd.phoenix.service.payment.ws.Address;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;
import com.apd.phoenix.service.payment.ws.CardVerificationParams;
import com.apd.phoenix.service.payment.ws.Contact;
import com.apd.phoenix.service.payment.ws.CountryCode;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionVerificationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTypes;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionCardVerificationApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.*;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class VerificationTests extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(VerificationTests.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void americanExpress() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validAmericanExpressNumber,
                CreditCardTypes.AMERICAN_EXPRESS);
        assertNotNull(validCreditCardTest(creditCard));
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void jcb() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validJCBNumber, CreditCardTypes.JCB);
        assertTrue(validCreditCardTest(creditCard).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void discover() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validDiscoverCardNumber,
                CreditCardTypes.DISCOVER);
        assertTrue(validCreditCardTest(creditCard).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void diners() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validDinersCardNumber, CreditCardTypes.DINERS);
        assertTrue(validCreditCardTest(creditCard).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void masterCard() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validMasterCardNumber,
                CreditCardTypes.MASTER_CARD);
        assertTrue(validCreditCardTest(creditCard).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void visa() {
        CreditCardTransactionCreditCard creditCard = createTestCreditCard(validVisaCardNumber, CreditCardTypes.VISA);
        assertTrue(validCreditCardTest(creditCard).isSucceeded());
    }

    public CreditCardTransactionResult validCreditCardTest(CreditCardTransactionCreditCard creditCard) {
        try {
            CardVerificationParams params = factory.createCardVerificationParams();
            params.setCreditCard(factory.createCardVerificationParamsCreditCard(creditCard));
            params.setTerminalIdentifier(factory.createCardVerificationParamsTerminalIdentifier(terminalIdentifier));
            params.setTransactionKey(factory.createCardVerificationParamsTransactionKey("123234" + Math.random()));
            CreditCardTransactionVerificationResult result = port.cardVerification(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionCardVerificationApplicationFaultFaultFaultMessage ex) {
            logger.error("ICreditCardTransactionCardVerificationApplicationFaultFaultFaultMessage", ex);
        }
        return null;
    }

    private CreditCardTransactionCreditCard createTestCreditCard(String cardNumber, CreditCardTypes type) {
        CreditCardTransactionCreditCard toReturn = factory.createCreditCardTransactionCreditCard();
        //Set card number
        String validAccountNumber = cardNumber;
        toReturn.setCardAccountNumber(factory.createCreditCardCardAccountNumber(validAccountNumber));
        //Set billing address
        Address billingAddress = getVerificationAddress();
        toReturn.setBillingAddress(factory.createCreditCardBillingAddress(billingAddress));

        //Set Card Type
        toReturn.setCardType(type);
        //Set Contact
        Contact contact = getVerificationContact();
        toReturn.setCardholder(factory.createCreditCardCardholder(contact));
        //Set Expiration Info
        toReturn.setExpirationMonth(4);
        toReturn.setExpirationYear(2014);

        toReturn.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
        if (type.equals(CreditCardTypes.AMERICAN_EXPRESS)) {
            toReturn.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("1234"));
        }
        else {
            toReturn.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("123"));
        }

        String nameOnCard = "John Smith";
        toReturn.setNameOnCard(factory.createCreditCardNameOnCard(nameOnCard));

        return toReturn;
    }

    private Contact getVerificationContact() {
        String firstname = "John";
        String lastName = "Smith";
        Contact contact = factory.createContact();
        contact.setFirstName(factory.createContactFirstName(firstname));
        contact.setLastName(factory.createContactLastName(lastName));
        return contact;
    }

    private Address getVerificationAddress() {
        Address address = factory.createAddress();
        address.setAddressLine1(factory.createAddressAddressLine1("5123 Test St."));
        address.setCountryCode(CountryCode.NONE);
        address.setPostalCode(factory.createAddressPostalCode("32456"));
        return address;
    }

}
