package com.apd.phoenix.service.executor.client;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.SignalCommand;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;
import com.apd.phoenix.service.invoice.BilledLineItem;
import com.apd.phoenix.service.invoice.MessageLineItem;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;

@RunWith(Arquillian.class)
public class ExecutorClientIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorClientIT.class);

    @Inject
    BRMSCommandCreator commandCreator;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.executor").addClasses(LineItem.class, LineItemStatus.class,
                BilledLineItem.class, MessageLineItem.class).addPackages(true, "com.apd.phoenix.service.model")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-human-task:5.3.1.BRMS").resolveAsFiles());

        // LOGGER.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Ignore
    public void signalTest() throws InterruptedException {
        commandCreator.sendStartProcess(StartProcessCommand.Name.SIGNAL_TEST, StartProcessCommand.Type.ORDER, new Long(
                1731), null, null, StartProcessCommand.Callback.HELLOWORLD);
        Thread.sleep(30000);
        commandCreator.sendSignalEvent(SignalCommand.Name.SIGNAL_TEST, null, SignalCommand.Type.ORDER, new Long(1731),
                null, SignalCommand.Callback.HELLOWORLD);
        Thread.sleep(30000);
    }

    @Ignore
    public void standardOrderEdi855() throws InterruptedException {
    	Map<String, Object> processParams = new HashMap<>();
    	List<Object> facts = new ArrayList<>(); 
    	
    	processParams.put("isManual", false);
    	processParams.put("isOrderComplete", false);
    	
    	

        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");
        lineItems.add(bli1);

        processParams.put("lineItems", lineItems);
        
        commandCreator.sendStartProcess(StartProcessCommand.Name.STANDARD_ORDER, StartProcessCommand.Type.ORDER, new Long(1915), processParams, null, StartProcessCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
        
        
        
        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("9.00"));
        lis.setValue("Shipped");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");
        
        facts.add(item1);
        facts.add(bli1);
        commandCreator.sendSignalEvent(SignalCommand.Name.VOUCHED, lineItems, SignalCommand.Type.ORDER, new Long(1915), facts, SignalCommand.Callback.HELLOWORLD);
        Thread.sleep(10000);
    }

    @Ignore
    public void exampleInlineEvent() {
        commandCreator.sendStartProcess(StartProcessCommand.Name.INLINE_SIGNAL_TEST, StartProcessCommand.Type.ORDER,
                new Long(1731), null, null, StartProcessCommand.Callback.HELLOWORLD);

        commandCreator.sendSignalEvent(SignalCommand.Name.INLINE_SIGNAL_TEST, "Hello world", SignalCommand.Type.ORDER,
                new Long(1731), null, SignalCommand.Callback.HELLOWORLD);

    }
}
