package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.jbpm.task.TaskService;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.task.service.responsehandlers.BlockingTaskSummaryResponseHandler;

@RunWith(Arquillian.class)
public class UpdateOrderStatusIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateOrderStatusIT.class);

    @Inject
    BRMSCommandCreator commandCreator;

    @Inject
    TaskServiceClientFactoryImpl taskClientFactory;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.executor", "com.apd.phoenix.service.brms.impl").addClasses()
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-human-task:5.3.1.BRMS").resolveAsFiles());

        // LOGGER.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void clientTest() throws InterruptedException {
        Long orderId = new Long(1915);

        LOGGER.info("Starting Client");
        Map<String, Object> processParams = new HashMap<String, Object>();
        processParams.put("orderId", orderId);
        processParams.put("status", "DENIED");
        processParams.put("usePending", true);
        processParams.put("useExisting", false);

        commandCreator.sendStartProcess(StartProcessCommand.Name.ORDER_REJECTED_SHIPTO, StartProcessCommand.Type.ORDER,
                orderId, processParams, null, StartProcessCommand.Callback.HELLOWORLD);

        Thread.sleep(5000);

        TaskService client = taskClientFactory.getTaskService();

        List<TaskSummary> tasks = client.getTasksAssignedAsPotentialOwner("admin", "en-UK");

        LOGGER.info("---------# of tasks available to admin=" + tasks.size());
        //        for (TaskSummary taskSummary : tasks) {
        //            if (taskSummary.getStatus().equals(Status.Ready)) {
        //                BlockingTaskOperationResponseHandler opHandler = new BlockingTaskOperationResponseHandler();
        //                LOGGER.info("-----------claiming task=" + taskSummary.getId());
        //                client.claim(taskSummary.getId(), "admin", opHandler);
        //                opHandler.waitTillDone(1000);
        //
        //                LOGGER.info("-----------starting task=" + taskSummary.getId());
        //                opHandler = new BlockingTaskOperationResponseHandler();
        //                client.start(taskSummary.getId(), "admin", opHandler);
        //                opHandler.waitTillDone(1000);
        //
        //                LOGGER.info("-----------completing task=" + taskSummary.getId());
        //                commandCreator.sendCompleteTask(new Long(taskSummary.getId()), "admin", null,
        //                        CompleteTaskCommand.Type.ORDER, orderId, null, CompleteTaskCommand.Callback.HELLOWORLD);
        //                Thread.sleep(2000);
        //            }
        //        }
        //
        //        Object message = orderId;
        //
        //        commandCreator.sendSignalEvent(SignalCommand.Name.SHIPTO_AUTHORIZED, message, SignalCommand.Type.ORDER,
        //                orderId, null, SignalCommand.Callback.HELLOWORLD);

        try {
            client.disconnect();
        }
        catch (Exception ex) {
            java.util.logging.Logger.getLogger(UpdateOrderStatusIT.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
