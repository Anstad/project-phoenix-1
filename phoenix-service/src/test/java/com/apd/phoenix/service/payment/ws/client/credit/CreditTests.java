/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.credit;

import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.CreditParams;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionCreditApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validAmericanExpressNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDinersCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validDiscoverCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validJCBNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validMasterCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class CreditTests extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(CreditTests.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void americanExpress() {
        StoredCardIdentifier card = getStoredCardIdentifier(validAmericanExpressNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void jcb() {
        StoredCardIdentifier card = getStoredCardIdentifier(validJCBNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void discover() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDiscoverCardNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void diners() {
        StoredCardIdentifier card = getStoredCardIdentifier(validDinersCardNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void masterCard() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void visa() {
        StoredCardIdentifier card = getStoredCardIdentifier(validVisaCardNumber);
        assertTrue(validCreditTest(card).isSucceeded());
    }

    public CreditCardTransactionResult validCreditTest(StoredCardIdentifier creditCard) {
        try {
            CreditParams params = new CreditParams();

            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionResult result = port.credit(clientCredentials, params);
            return result;
        }
        catch (ICreditCardTransactionCreditApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }
}
