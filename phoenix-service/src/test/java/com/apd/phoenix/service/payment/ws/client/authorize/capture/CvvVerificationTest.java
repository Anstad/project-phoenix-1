/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.authorize.capture;

import com.apd.phoenix.service.payment.ws.AuthorizeAndCaptureParams;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeIndicator;
import com.apd.phoenix.service.payment.ws.CardSecurityCodeResponse;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionCreditCard;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validMasterCardNumber;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class CvvVerificationTest extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(CvvVerificationTest.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void cvvAccept() {
        StoredCardIdentifier card = getStoredCardIdentifier(validMasterCardNumber);
        CreditCardTransactionAuthorizationResult result = validCardAuthorizeAndCaptureTest(card);
        assertEquals(result.getCardSecurityCodeResponse(), CardSecurityCodeResponse.MATCHED);
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void cvvDenial() {
        StoredCardIdentifier invalidCvvCard = getStoredCardIdentifier(validVisaCardNumber);
        CreditCardTransactionAuthorizationResult cvvResult = cvvDenialAuthorizationRequestTest(invalidCvvCard);
        assertEquals(cvvResult.getCardSecurityCodeResponse(), CardSecurityCodeResponse.NOT_MATCHED);
    }

    public CreditCardTransactionAuthorizationResult validCardAuthorizeAndCaptureTest(StoredCardIdentifier creditCard) {
        try {
            AuthorizeAndCaptureParams params = new AuthorizeAndCaptureParams();
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(creditCard);
            CreditCardTransactionCreditCard cardForCvv = factory.createCreditCardTransactionCreditCard();
            cardForCvv.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
            cardForCvv.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("124"));
            creditCardTransaction.setCreditCard(factory.createCreditCardTransactionCreditCard1(cardForCvv));
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorizeAndCapture(clientCredentials, params);
            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            return result;
        }
        catch (ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }

    public CreditCardTransactionAuthorizationResult cvvDenialAuthorizationRequestTest(StoredCardIdentifier storedCard) {
        try {
            AuthorizeAndCaptureParams params = new AuthorizeAndCaptureParams();
            CreditCardTransactionCreditCard cardForCvv = factory.createCreditCardTransactionCreditCard();
            cardForCvv.setCardSecurityCodeIndicator(CardSecurityCodeIndicator.PROVIDED);
            cardForCvv.setCardSecurityCode(factory.createCreditCardTransactionCreditCardCardSecurityCode("123"));
            CreditCardTransaction creditCardTransaction = createValidCreditCardTransaction(storedCard);
            creditCardTransaction.setCreditCard(factory.createCreditCardTransactionCreditCard1(cardForCvv));
            params.setCreditCardTransaction(factory
                    .createAuthorizeAndCaptureParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeAndCaptureParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorizeAndCapture(clientCredentials, params);
            return result;
        }
        catch (ICreditCardTransactionAuthorizeAndCaptureApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return null;
    }
}
