package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.command.api.BRMSCommandCreator;
import com.apd.phoenix.service.executor.command.api.Command.Callback;
import com.apd.phoenix.service.executor.command.api.StartProcessCommand.Name;
import com.apd.phoenix.service.executor.command.api.Command.Type;

@RunWith(Arquillian.class)
public class UserRequestsIT {

    private static final Logger LOG = LoggerFactory.getLogger(UserRequestsIT.class);

    @Inject
    BRMSCommandCreator commandCreator;

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.executor", "com.apd.phoenix.service.brms.impl",
                "com.apd.phoenix.service.model", "com.apd.phoenix.service.invoice", "com.apd.phoenix.service.message")
                .addClasses().addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-human-task:5.3.1.BRMS").resolveAsFiles());

        LOG.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void userCreation() throws InterruptedException {
        Long id = new Long(1);
        commandCreator.sendStartProcess(Name.USER_CREATION, Type.USER_REQUEST, id, null, null, Callback.HELLOWORLD);
        Thread.sleep(10000);
    }
}
