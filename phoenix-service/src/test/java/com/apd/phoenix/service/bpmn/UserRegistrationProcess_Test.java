package com.apd.phoenix.service.bpmn;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.ProcessInstance;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkflowProcessInstance;
import org.jbpm.test.JbpmJUnitTestCase;
import org.junit.Test;

import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.SystemUser;

public class UserRegistrationProcess_Test extends JbpmJUnitTestCase {

	private TestWorkItemHandler workItemHandler = null;
	private ProcessInstance processInstance = null;
	private StatefulKnowledgeSession ksession = null;

	HashMap<String, Object> params = new HashMap<>();
	SystemUser newUser = null;

	@Test
	public void startProcessWithSystemUser() {
		ksession = createKnowledgeSession("ECommerce_UserRegistration.bpmn");

		workItemHandler = new TestWorkItemHandler();

		ksession.getWorkItemManager().registerWorkItemHandler("Human Task", workItemHandler);

		newUser = createUser();

		params.put("newUser", newUser);

		processInstance = ksession.startProcess("com.apd.phoenix.knowledge.UserRegistration", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		assertNodeTriggered(processInstance.getId(), "User Registration");

		assertEquals(newUser, (SystemUser) getVariableValue("newUser", processInstance.getId(), ksession));

		WorkItem workItem = workItemHandler.getWorkItem();
		assertNotNull(workItem);

		ksession.getWorkItemManager().abortWorkItem(workItem.getId());

		assertProcessInstanceAborted(processInstance.getId(), ksession);
	}

	@Test
	public void completeTaskWithLoginName() {
		ksession = createKnowledgeSession("ECommerce_UserRegistration.bpmn");

		workItemHandler = new TestWorkItemHandler();

		ksession.getWorkItemManager().registerWorkItemHandler("Human Task", workItemHandler);

		newUser = createUser();

		params.put("newUser", newUser);

		processInstance = ksession.startProcess("com.apd.phoenix.knowledge.UserRegistration", params);

		assertProcessInstanceActive(processInstance.getId(), ksession);

		assertNodeTriggered(processInstance.getId(), "User Registration");

		String userLogin = "userLogin";

		HashMap<String, Object> completeParams = new HashMap<>();
		completeParams.put("enteredLogin", userLogin);

		WorkItem workItem = workItemHandler.getWorkItem();
		assertNotNull(workItem);

		ksession.getWorkItemManager().completeWorkItem(workItem.getId(), completeParams);

		assertEquals(userLogin, ((WorkflowProcessInstance) processInstance).getVariable("assignedLogin"));

		assertProcessInstanceCompleted(processInstance.getId(), ksession);
	}

	private SystemUser createUser() {
		SystemUser user = new SystemUser();

		Person person = new Person();
		person.setFirstName("Mike");
		person.setMiddleInitial("E.");
		person.setLastName("Mason");
		person.setPrefix("Mr.");

		Address address = new Address();
		address.setCity("Charlotte");
		address.setState("NC");
		address.setLine1("555 Arrow Dr.");
		address.setZip("55555");

		Set<Address> addresses = new HashSet<Address>();
		addresses.add(address);

		PhoneNumber phoneNumber = new PhoneNumber();
		phoneNumber.setCountryCode("1");
		phoneNumber.setAreaCode("222");
		phoneNumber.setExchange("333");
		phoneNumber.setLineNumber("4444");

		Set<PhoneNumber> phoneNumbers = new HashSet<PhoneNumber>();
		phoneNumbers.add(phoneNumber);

		person.setPhoneNumbers(phoneNumbers);
		person.setAddresses(addresses);
		user.setPerson(person);

		return user;
	}
}