/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client;

import com.apd.phoenix.service.payment.ws.AuthorizeParams;
import com.apd.phoenix.service.payment.ws.CreditCardTransaction;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionAuthorizationResult;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.ICreditCardTransactionVoidApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.payment.ws.StoredCardIdentifier;
import com.apd.phoenix.service.payment.ws.VoidParams;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validVisaCardNumber;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 */
public class VoidTest extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayBaseTest.class);
    private CreditCardTransaction creditCardTransaction;

    @BeforeClass
    public void init() {
        try {
            AuthorizeParams params = factory.createAuthorizeParams();
            StoredCardIdentifier creditCard = getStoredCardIdentifier(validVisaCardNumber);
            creditCardTransaction = createValidCreditCardTransaction(creditCard);
            params.setCreditCardTransaction(factory.createAuthorizeParamsCreditCardTransaction(creditCardTransaction));
            params.setTerminalIdentifier(factory.createAuthorizeParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionAuthorizationResult result = port.authorize(clientCredentials, params);

            if (!result.isSucceeded()) {
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error(failureDetail);
                    }
                }
            }
            assertTrue(result.isSucceeded());
        }
        catch (ICreditCardTransactionAuthorizeApplicationFaultFaultFaultMessage ex) {
            logger.error("ICreditCardTransactionVoidApplicationFaultFaultFaultMessage caught", ex);
        }
    }

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void validVoidTest() {
        try {
            VoidParams params = factory.createVoidParams();
            params.setOriginalTransactionKey(factory
                    .createOriginalTransactionParamsOriginalTransactionKey(creditCardTransaction.getTransactionKey()
                            .getValue()));
            params.setTerminalIdentifier(factory.createOriginalTransactionParamsTerminalIdentifier(terminalIdentifier));
            CreditCardTransactionResult result = port._void(clientCredentials, params);
            assertTrue(result.isSucceeded());
        }
        catch (ICreditCardTransactionVoidApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
    }
}
