package com.apd.phoenix.service.card.management;

import java.util.List;
import static org.testng.Assert.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

/**
 *
 * @author dnorris
 */
public class AddUpdateDeleteStoredCreditCardNG extends CustomerManagementBase {

    private static final Logger logger = LoggerFactory.getLogger(AddUpdateDeleteStoredCreditCardNG.class);
    private String storedCardToken;
    private UpdateStoredCardParams updateStoredCardParams;

    @BeforeClass
    public void setUpClass() throws Exception {
        initFirstTestCustomer();
        initFirstCustomerParams();
        initSecondTestCustomer();
        initSecondCustomerParams();
        initContact();
        initStoredCreditCard();

        //Ensure test customers do not already exist
        cleanUpBeforeTest();
    }

    private CustomerIdentifier createCustomeridentifier(String customerCode) {
        CustomerIdentifier customerIdentifier = factory.createCustomerIdentifier();
        customerIdentifier = factory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(factory.createCustomerIdentifierCustomerCode(customerCode));
        customerIdentifier.setLocationCode(factory.createCustomerIdentifierLocationCode(locationCode));
        customerIdentifier.setMerchantCode(factory.createCustomerIdentifierMerchantCode(merchantCode));
        return customerIdentifier;
    }

    @Test
    public void addFirstCustomerToVault() throws ICreditCardManagementCreateCustomerApplicationFaultFaultFaultMessage {
        CreditCardManagementResult result = port.createCustomer(clientCredentials, firstCustomerParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());

    }

    @Test
    public void addSecondCustomerToVault() throws ICreditCardManagementCreateCustomerApplicationFaultFaultFaultMessage {
        CreditCardManagementResult result = port.createCustomer(clientCredentials, secondCustomerParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "addFirstCustomerToVault" })
    public void updateFirstCustomer() throws Exception {
        UpdateCustomerParams updateParams = factory.createUpdateCustomerParams();
        firstCustomer.setName(factory.createCustomerName("Bob's Updated Hardware"));
        updateParams.setCustomer(factory.createUpdateCustomerParamsCustomer(firstCustomer));
        updateParams.setLocationIdentifier(factory.createUpdateCustomerParamsLocationIdentifier(locationIdentifier));

        CreditCardManagementResult result = port.updateCustomer(clientCredentials, updateParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "addSecondCustomerToVault" })
    public void updateSecondCustomer() throws Exception {
        UpdateCustomerParams updateParams = factory.createUpdateCustomerParams();
        secondCustomer.setName(factory.createCustomerName("John Doe Updated"));
        updateParams.setCustomer(factory.createUpdateCustomerParamsCustomer(secondCustomer));
        updateParams.setLocationIdentifier(factory.createUpdateCustomerParamsLocationIdentifier(locationIdentifier));

        CreditCardManagementResult result = port.updateCustomer(clientCredentials, updateParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "updateSecondCustomer" })
    public void deleteSecondCustomerFromVault()
            throws ICreditCardManagementDeleteCustomerApplicationFaultFaultFaultMessage {
        DeleteCustomerParams deleteCustomerParams = factory.createDeleteCustomerParams();
        deleteCustomerParams.setCustomerIdentifier(factory
                .createDeleteCustomerParamsCustomerIdentifier(createCustomeridentifier(CUSTOMER_CODE_2)));
        CreditCardManagementResult result = port.deleteCustomer(clientCredentials, deleteCustomerParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "addFirstCustomerToVault" })
    public void addStoredCreditCard() throws ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage {
        AddStoredCardParams addStoredCardParams;
        addStoredCardParams = factory.createAddStoredCardParams();
        addStoredCardParams.setCustomerIdentifier(factory
                .createAddStoredCardParamsCustomerIdentifier(createCustomeridentifier(CUSTOMER_CODE_1)));
        addStoredCardParams.setCreditCard(factory.createAddStoredCardParamsCreditCard(storedCreditCard));
        TokenCreditCardManagementResult result = port.addStoredCreditCard(clientCredentials, addStoredCardParams);
        storedCardToken = result.getToken().getValue();
        if (!result.isSucceeded()) {
            logger.error("AddStoredCreditCard failed.");
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        else {
            logger.info("AddStoredCreditCard Succeeded.");
        }
        assertNotNull(storedCardToken);
        assertTrue(result.isSucceeded());
    }

    private void createUpdateStoredCardParams() {
        StoredCreditCard updatedCard;
        updatedCard = factory.createStoredCreditCard();
        updatedCard.setBillingAddress(factory.createCreditCardBillingAddress(createValidAddress()));
        updatedCard.setCardAccountNumber(factory.createCreditCardCardAccountNumber(validVisaCardNumber));
        updatedCard.setCardType(CreditCardTypes.VISA);
        updatedCard.setCardholder(factory.createCreditCardCardholder(contact));
        updatedCard.setExpirationMonth(5);
        updatedCard.setExpirationYear(2015);
        updatedCard.setNameOnCard(factory.createCreditCardNameOnCard("John Doe"));
        updatedCard.setToken(factory.createStoredCreditCardToken(storedCardToken));
        updateStoredCardParams = factory.createUpdateStoredCardParams();
        updateStoredCardParams.setCreditCard(factory.createUpdateStoredCardParamsCreditCard(updatedCard));
        updateStoredCardParams.setCustomerIdentifier(factory
                .createUpdateStoredCardParamsCustomerIdentifier(createCustomeridentifier(CUSTOMER_CODE_1)));
    }

    @Test(dependsOnMethods = { "addStoredCreditCard" })
    public void updateStoredCreditCard()
            throws ICreditCardManagementUpdateStoredCreditCardApplicationFaultFaultFaultMessage {
        createUpdateStoredCardParams();
        CreditCardManagementResult result = port.updateStoredCreditCard(clientCredentials, updateStoredCardParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "updateStoredCreditCard" })
    private void removeStoredCreditCard()
            throws ICreditCardManagementDeleteStoredCreditCardApplicationFaultFaultFaultMessage {
        DeleteStoredCardParams deleteStoredCardParams;
        deleteStoredCardParams = factory.createDeleteStoredCardParams();
        deleteStoredCardParams.setCustomerIdentifier(factory
                .createDeleteStoredCardParamsCustomerIdentifier(createCustomeridentifier(CUSTOMER_CODE_1)));
        deleteStoredCardParams.setToken(factory.createDeleteStoredCardParamsToken(storedCardToken));
        CreditCardManagementResult result = port.deleteStoredCreditCard(clientCredentials, deleteStoredCardParams);
        if (!result.isSucceeded()) {
            logger.error("AddStoredCreditCard Failed.");
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
        else {
            logger.info("AddStoredCreditCard Succeeded.");
        }
        assertTrue(result.isSucceeded());
    }

    @Test(dependsOnMethods = { "removeStoredCreditCard" })
    public void deleteFirstCustomerFromVault()
            throws ICreditCardManagementDeleteCustomerApplicationFaultFaultFaultMessage {
        DeleteCustomerParams deleteCustomerParams = factory.createDeleteCustomerParams();
        deleteCustomerParams.setCustomerIdentifier(factory
                .createDeleteCustomerParamsCustomerIdentifier(createCustomeridentifier(CUSTOMER_CODE_1)));
        CreditCardManagementResult result = port.deleteCustomer(clientCredentials, deleteCustomerParams);
        if (!result.isSucceeded()) {
            if (result.getValidationFailures().getValue() != null) {
                List<String> failureReasons = result.getValidationFailures().getValue().getString();
                for (String failureDetail : failureReasons) {
                    logger.error(failureDetail);
                }
            }
        }
    }
}
