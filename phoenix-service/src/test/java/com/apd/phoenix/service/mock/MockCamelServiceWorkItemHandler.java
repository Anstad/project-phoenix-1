/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.mock;

import java.util.Map;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemHandler;
import org.drools.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author selrahal
 */
public class MockCamelServiceWorkItemHandler implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockCamelServiceWorkItemHandler.class);

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
        try {
            for (Map.Entry<String, Object> entry : workItem.getParameters().entrySet()) {
                LOGGER.info("key: " + entry.getKey() + ", value: " + entry.getValue());
            }
            manager.completeWorkItem(workItem.getId(), null);
        }
        catch (Exception e) {
            LOGGER.error("Exception caught while trying to start: ", e);
            this.abortWorkItem(workItem, manager);
        }
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
        LOGGER.info("**Aborting Work Item Handler**");
        manager.abortWorkItem(workItem.getId());
    }

}
