/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.ws.client.bug;

import com.apd.phoenix.service.card.management.AddStoredCardParams;
import com.apd.phoenix.service.card.management.Address;
import com.apd.phoenix.service.card.management.Contact;
import com.apd.phoenix.service.card.management.CreditCardManagementFailureReason;
import com.apd.phoenix.service.card.management.CustomerIdentifier;
import com.apd.phoenix.service.card.management.GetTokenForCardNumberParams;
import com.apd.phoenix.service.card.management.ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage;
import com.apd.phoenix.service.card.management.StoredCreditCard;
import com.apd.phoenix.service.card.management.TokenCreditCardManagementResult;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayBaseTest;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.CUSTOMER_CODE;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.locationCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.merchantCode;
import static com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestConstants.validAmericanExpressNumber;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

/**
 *
 * @author dnorris
 * Description of bug: 3DSI has inconsistent handling of extra whitespace space appended to the end of a card number
 * When storing a card in the vault the first time with an extra space everything works as expected, 
 * but attempting to retrieve the card with the same String value fails
 */
public class PaymentGatewayBugProof extends PaymentGatewayBaseTest {

    private static final Logger logger = LoggerFactory.getLogger(PaymentGatewayBugProof.class);

    @Test(dependsOnMethods = { "addCustomerToVault" })
    public void storeCardWithExtraWhiteSpace() {
        StoredCreditCard storedCreditCard = createStoredCreditCard(validAmericanExpressNumber + " ");
        TokenCreditCardManagementResult result = storeOrRetrieveCard(storedCreditCard);
        assertTrue(result.isSucceeded());

    }

    @Test(dependsOnMethods = { "storeCardWithExtraWhiteSpace" })
    public void retrieveCardWithExtraWhiteSpace() {
        //retrieve same card info
        StoredCreditCard storedCreditCard = createStoredCreditCard(validAmericanExpressNumber + " ");
        TokenCreditCardManagementResult result = storeOrRetrieveCard(storedCreditCard);
        logger.info(result.getFailureReason().toString());
        assertTrue(result.isSucceeded());
    }

    private StoredCreditCard createStoredCreditCard(String cardNumber) {
        StoredCreditCard storedCreditCard;
        storedCreditCard = cardManagerFactory.createStoredCreditCard();
        String validAccountNumber = cardNumber;
        storedCreditCard.setCardAccountNumber(cardManagerFactory.createCreditCardCardAccountNumber(validAccountNumber));
        Address billingAddress = getValidAddress();
        storedCreditCard.setBillingAddress(cardManagerFactory.createCreditCardBillingAddress(billingAddress));
        Contact contact = getValidContact();
        storedCreditCard.setCardholder(cardManagerFactory.createCreditCardCardholder(contact));
        storedCreditCard.setExpirationMonth(4);
        storedCreditCard.setExpirationYear(2014);
        String nameOnCard = "John Smith";
        storedCreditCard.setNameOnCard(cardManagerFactory.createCreditCardNameOnCard(nameOnCard));
        return storedCreditCard;
    }

    private TokenCreditCardManagementResult storeOrRetrieveCard(StoredCreditCard storedCreditCard) {
        String vaultToken = null;
        TokenCreditCardManagementResult result = null;
        try {
            logger.info("Attempting to Store Card in Vault");
            AddStoredCardParams addStoredCardParams;
            addStoredCardParams = cardManagerFactory.createAddStoredCardParams();
            CustomerIdentifier customerIdentifier = cardManagerFactory.createCustomerIdentifier();
            customerIdentifier.setCustomerCode(cardManagerFactory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE));
            customerIdentifier.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
            customerIdentifier.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));

            addStoredCardParams.setCustomerIdentifier(cardManagerFactory
                    .createAddStoredCardParamsCustomerIdentifier(customerIdentifier));
            addStoredCardParams.setCreditCard(cardManagerFactory.createAddStoredCardParamsCreditCard(storedCreditCard));
            result = cardManagementPort.addStoredCreditCard(cardManagementClientCredentials, addStoredCardParams);
            if (!result.isSucceeded()) {
                if (result.getFailureReason().equals(CreditCardManagementFailureReason.CARD_NUMBER_IN_USE)) {
                    logger.info("Card is already stored in vault.");
                    result = getTokenForExistingCard(storedCreditCard.getCardAccountNumber().getValue());
                }
                else {
                    if (result.getValidationFailures().getValue() != null) {
                        List<String> failureReasons = result.getValidationFailures().getValue().getString();
                        for (String failureDetail : failureReasons) {
                            logger
                                    .error("Error in soap exchange with payment gateway. FAILURE REASON:"
                                            + failureDetail);
                        }
                    }
                }
            }
            else {
                logger.info("Card successfully stored in vault.");
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementAddStoredCreditCardApplicationFaultFaultFaultMessage ex) {
            logger.info(null, ex);
        }
        return result;
    }

    private TokenCreditCardManagementResult getTokenForExistingCard(String cardNumber) {
        String vaultToken = null;
        TokenCreditCardManagementResult result = null;
        try {
            logger.debug("Retrieving token for existing card from the vault.");
            GetTokenForCardNumberParams getParams = createGetTokenForCardNumberParams(cardNumber);
            result = cardManagementPort.getTokenForCardNumber(cardManagementClientCredentials, getParams);
            if (!result.isSucceeded()) {
                logger.error("Unable to retrieve token from the vault.");
                if (result.getValidationFailures().getValue() != null) {
                    List<String> failureReasons = result.getValidationFailures().getValue().getString();
                    for (String failureDetail : failureReasons) {
                        logger.error("Error in soap exchange with payment gateway. FAILURE REASON:" + failureDetail);
                    }
                }
            }
            else {
                logger.debug("Token retrieved successfully from the vault.");
                vaultToken = result.getToken().getValue();
            }
        }
        catch (ICreditCardManagementGetTokenForCardNumberApplicationFaultFaultFaultMessage ex) {
            logger.error(null, ex);
        }
        return result;
    }

    private GetTokenForCardNumberParams createGetTokenForCardNumberParams(String cardNumber) {
        GetTokenForCardNumberParams getParams = cardManagerFactory.createGetTokenForCardNumberParams();
        getParams.setAccountNumber(cardManagerFactory.createGetTokenForCardNumberParamsAccountNumber(cardNumber));
        CustomerIdentifier customerIdentifier = cardManagerFactory.createCustomerIdentifier();
        customerIdentifier.setCustomerCode(cardManagerFactory.createCustomerIdentifierCustomerCode(CUSTOMER_CODE));
        customerIdentifier.setMerchantCode(cardManagerFactory.createCustomerIdentifierMerchantCode(merchantCode));
        customerIdentifier.setLocationCode(cardManagerFactory.createCustomerIdentifierLocationCode(locationCode));
        getParams.setCustomerIdentifier(cardManagerFactory
                .createGetTokenForCardNumberParamsCustomerIdentifier(customerIdentifier));
        return getParams;
    }
}
