/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.card.management;

import com.apd.phoenix.service.payment.ws.client.PaymentGatewayTestPropertiesLoader;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;

/**
 *
 * @author dnorris
 */
public class CardManagerBaseTestNG {

    private static final Logger logger = LoggerFactory.getLogger(CardManagerBaseTestNG.class);
    private static final PropertiesConfiguration props = PaymentGatewayTestPropertiesLoader.getInstance()
            .getProperties();
    protected static final String clientCode = props.getString("testClientCode");
    protected static final String username = props.getString("testUserName");
    protected static final String password = props.getString("testPassword");
    protected static final String locationCode = props.getString("testLocationCode");
    protected static final String merchantCode = props.getString("testMerchantCode");
    protected static final String validVisaCardNumber = "4111111111111111";
    protected static final String validMasterCardNumber = "5584312967483092";
    protected static final String validAmericanExpressNumber = "370824782591981";
    protected static final String validJCBNumber = "3530111333300000";
    protected static final String validDiscoverCardNumber = "6011375146854740";
    protected static final String validDinersCardNumber = "365948383996583";
    protected CreditCardManagementService cardManagementService;
    protected ICreditCardManagement port;
    protected ClientCredentials clientCredentials;
    protected ObjectFactory factory;
    protected LocationIdentifier locationIdentifier;
    protected Contact contact;
    protected StoredCreditCard storedCreditCard;
    protected static final String CUSTOMER_CODE_1 = "BobsHardware";
    protected static final String CUSTOMER_CODE_2 = "APD-TEST";

    @BeforeClass
    public void setup() {
        factory = new ObjectFactory();
        cardManagementService = new CreditCardManagementService();
        port = cardManagementService.getCreditCardManagementService();
        initClientCredentials();
        initLocationIdentifier();
    }

    private void initClientCredentials() {
        clientCredentials = factory.createClientCredentials();
        clientCredentials.setClientCode(factory.createClientCredentialsClientCode(clientCode));
        clientCredentials.setUserName(factory.createClientCredentialsUserName(username));
        clientCredentials.setPassword(factory.createClientCredentialsPassword(password));
    }

    private void initLocationIdentifier() {
        locationIdentifier = factory.createLocationIdentifier();
        locationIdentifier.setLocationCode(factory.createLocationIdentifierLocationCode(locationCode));
        locationIdentifier.setMerchantCode(factory.createLocationIdentifierMerchantCode(merchantCode));
    }

    protected Address createValidAddress() {
        Address address = factory.createAddress();
        address.setAddressLine1(factory.createAddressAddressLine1("5123 Test St."));
        address.setCountryCode(CountryCode.NONE);
        address.setPostalCode(factory.createAddressPostalCode("32456"));
        return address;
    }

    protected void initContact() {
        contact = factory.createContact();
        contact.setFirstName(factory.createContactFirstName("John"));
        contact.setLastName(factory.createContactLastName("Doe"));
    }

    protected void initStoredCreditCard() {
        storedCreditCard = factory.createStoredCreditCard();
        storedCreditCard.setBillingAddress(factory.createCreditCardBillingAddress(createValidAddress()));
        storedCreditCard.setCardAccountNumber(factory.createCreditCardCardAccountNumber(validVisaCardNumber));
        storedCreditCard.setCardType(CreditCardTypes.VISA);
        storedCreditCard.setCardholder(factory.createCreditCardCardholder(contact));
        storedCreditCard.setExpirationMonth(4);
        storedCreditCard.setExpirationYear(2014);
        storedCreditCard.setNameOnCard(factory.createCreditCardNameOnCard("John Doe"));
    }
}
