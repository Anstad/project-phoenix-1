package com.apd.phoenix.service.model;

import javax.inject.Inject;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class CatalogXItemTest extends EntityTest {

    @Inject
    private CatalogXItem catalogxitem;

    @Test
    public void testIsDeployed() {
        Assert.assertNotNull(catalogxitem);
    }
}
