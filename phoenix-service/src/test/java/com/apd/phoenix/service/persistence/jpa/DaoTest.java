package com.apd.phoenix.service.persistence.jpa;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import org.apache.commons.lang.StringUtils;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.formatter.Formatters;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.apd.phoenix.service.model.MockDataFactory.*;

@SuppressWarnings("unused")
public abstract class DaoTest {

    private static final Logger LOG = LoggerFactory.getLogger(DaoTest.class);

    private static final String DEFAULT_DEPLOYMENT_FILENAME = "DaoTest.jar";

    protected static String[] cleanDBQueries;

    @PersistenceContext
    EntityManager em;

    @Inject
    UserTransaction utx;

    @Deployment
    public static JavaArchive createDeployment() {

        String deploymentFilename = StringUtils.endsWith(getDeploymentFilename(), ".jar") ? getDeploymentFilename()
                : DEFAULT_DEPLOYMENT_FILENAME;

        JavaArchive ja = ShrinkWrap.create(JavaArchive.class, deploymentFilename)
                // setting this True will also add sub packages
                .addPackages(false, "com.apd.phoenix.service.model", "com.apd.phoenix.service.persistence.jpa")
                .addAsManifestResource("test-persistence.xml", "persistence.xml")
                // .addAsManifestResource("jbossas-ds.xml")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");

        LOG.info(ja.toString(Formatters.VERBOSE));
        return ja;
    }

    public static String getDeploymentFilename() {
        return DEFAULT_DEPLOYMENT_FILENAME;
    }

    @Before
    public void preparePersistenceTest() throws Exception {
        clearData();
        startTransaction();
    }

    private void clearData() throws Exception {
        startTransaction();
        LOG.info("Removing records from previous test...");
        for (String query : cleanDBQueries) {
            em.createQuery(query).executeUpdate();
        }
        commitTransaction();
        em.clear();
    }

    private void startTransaction() throws Exception {
        utx.begin();
        em.joinTransaction();
    }

    @After
    public void commitTransaction() throws Exception {
        utx.commit();
    }

    public enum EventTypeEnum {
        ORDER_SENT, ORDER_RECEIVED, ORDER_ACK_SENT, ORDER_ACK_RECEIVED, QUICK_ORDER_ACK_SENT, QUICK_ORDER_ACK_RECEIVED, SHIPMENT_NOTIFICATION_SENT, SHIPMENT_NOTIFICATION_RECEIVED, INVOICE_SENT, INVOICE_RECEIVED, ERROR_RESPONSE, LINEITEM_ORDERED, LINEITEM_REJECTED, LINEITEM_REMOVED, LINEITEM_UPDATED, LINEITEM_ADDED, LINEITEM_BACKORDERED;
    }

    public enum OrderStatusEnum {
        ORDERED, SENT_TO_VENDOR, ACCEPTED_BY_VENDOR;
    }

    public enum LineItemStatusEnum {
        ORDERED, BACKORDERED
    }

}
