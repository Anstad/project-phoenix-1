package com.apd.phoenix.bpmn.order;

import javax.inject.Inject;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;

public class MockReplaceItemOnOrderServiceWorkItemHandler implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MockReplaceItemOnOrderServiceWorkItemHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Override
    public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    public void executeWorkItem(WorkItem wi, WorkItemManager wih) {
        LOGGER.info("customerOrderBp.replaceItem(customerOrder, rejectedItem, replacementItem)");
        wih.completeWorkItem(wi.getId(), null);
    }

}
