package com.apd.phoenix.bpmn.manifest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.manifest.Manifest;
import com.apd.phoenix.service.manifest.VendorManifestsResponse;
import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;

public class ShippingManifest2Test extends BrmsBaseTest {

    @Override
    protected Map<Resource, ResourceType> getResources() {
        Map<Resource, ResourceType> resources = new HashMap<Resource, ResourceType>();
        resources.put(ResourceFactory.newClassPathResource("brms/shipment-manifest/change-set.xml"),
                ResourceType.CHANGE_SET);

        return resources;
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        session.getWorkItemManager().registerWorkItemHandler("Camel Service", new MockCamelServiceWorkItemHandler());
    }

    @Test
    public void multipleVendors() throws InterruptedException {
    	
    	VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(true);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTPService", new MockFTPWorkItemHandler(vmr));
        
    	session.getWorkItemManager().registerWorkItemHandler("S3Service", new MockS3StorageWorkItemHandler(5));
    	
    	session.getWorkItemManager().registerWorkItemHandler("Human Task", new MockCamelServiceWorkItemHandler());
    	
    	session.getWorkItemManager().registerWorkItemHandler("CamelService", new MockCamelServiceWorkItemHandler());
    	
    	Map<String, Object> inputVariables = new HashMap<String, Object>();
    	
    	List<String> vendors = new ArrayList<>();
    	List<String> vendorsInError = new ArrayList<>();
    	vendors.add("APD");
    	vendors.add("USSCO");
        inputVariables.put("vendors", vendors);
        inputVariables.put("retry", false);
        inputVariables.put("vendorsInError", vendorsInError);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendor-manifest-retrieval", inputVariables);
        
        
    }
}
