package com.apd.phoenix.bpmn.invoice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import junit.framework.Assert;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.invoice.BilledLineItem;
import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;
import com.apd.phoenix.service.mock.MockEmailWorkItemHandler;
import com.apd.phoenix.service.mock.MockUpdateLineItemWID;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;

public class VendorInvoiceTest extends BrmsBaseTest {

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/vendor-invoice/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Before
    public void setupWorkItemHandlers() {

        this.session.getWorkItemManager().registerWorkItemHandler("Camel Service",
                new MockCamelServiceWorkItemHandler());
        this.session.getWorkItemManager().registerWorkItemHandler("Update Order", new MockUpdateOrderWIH());
        this.session.getWorkItemManager().registerWorkItemHandler("Email", new MockEmailWorkItemHandler());
        this.session.getWorkItemManager().registerWorkItemHandler("Updatelineitemstatus", new MockUpdateLineItemWID());

    }

    @Test
    public void alreadyInvoiced() {

        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("7.99"));
        lis.setValue("Invoiced");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");

        lineItems.add(bli1);

        session.insert(item1);
        session.insert(bli1);
        inputVariables.put("lineItems", lineItems);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendorInvoices", inputVariables);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
        Assert.assertTrue(bli1.isInvalid());
        assertNodeTriggered(processInstance.getId(), "Email");
        assertNodeNotTriggered(processInstance.getId(), "Update line item status");
    }

    @Test
    public void invalidMargin() {

        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("7.60"));
        lis.setValue("Invoiced");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");
        lineItems.add(bli1);

        session.insert(item1);
        session.insert(bli1);
        inputVariables.put("lineItems", lineItems);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendorInvoices", inputVariables);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
        Assert.assertTrue(bli1.isInvalid());
        assertNodeTriggered(processInstance.getId(), "Email");
        String message = bli1.getErrorMessages().get(0);
        Assert.assertEquals("The gross margin is invalid", message);
        assertNodeNotTriggered(processInstance.getId(), "Update line item status");
    }

    @Test
    public void validMargin() {

        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("9.00"));
        lis.setValue("Shipped");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");
        lineItems.add(bli1);

        session.insert(item1);
        session.insert(bli1);
        inputVariables.put("lineItems", lineItems);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendorInvoices", inputVariables);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
        Assert.assertFalse(bli1.isInvalid());
        assertNodeNotTriggered(processInstance.getId(), "Email");
        assertNodeTriggered(processInstance.getId(), "Update line item status");
    }

    @Test
    public void mismatchedQuantity() {
        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("9.00"));
        lis.setValue("Shipped");
        item1.setStatus(lis);
        item1.setQuantity(1);
        item1.setApdSku("1234");

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");
        lineItems.add(bli1);

        session.insert(item1);
        session.insert(bli1);
        inputVariables.put("lineItems", lineItems);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendorInvoices", inputVariables);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
        Assert.assertTrue(bli1.isInvalid());
        assertNodeTriggered(processInstance.getId(), "Email");
        String message = bli1.getErrorMessages().get(0);
        Assert.assertEquals("Line item quantity mismatch", message);
        assertNodeNotTriggered(processInstance.getId(), "Update line item status");
    }

    @Test
    public void validQuantity() {
        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();
        item1.setUnitPrice(new BigDecimal("9.00"));
        lis.setValue("Shipped");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        ArrayList<BilledLineItem> lineItems = new ArrayList<BilledLineItem>();
        BilledLineItem bli1 = new BilledLineItem(2, "1234", "1234", new BigDecimal("7.60"), "Shipped");
        lineItems.add(bli1);

        session.insert(item1);
        session.insert(bli1);
        inputVariables.put("lineItems", lineItems);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "phoenix.vendorInvoices", inputVariables);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
        Assert.assertFalse(bli1.isInvalid());
        assertNodeNotTriggered(processInstance.getId(), "Email");
        assertNodeTriggered(processInstance.getId(), "Update line item status");
    }
}
