package com.apd.phoenix.bpmn.account;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.jbpm.task.AccessType;
import org.jbpm.task.Status;
import org.jbpm.task.TaskService;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.task.service.ContentData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.user.AccountUpdateApprovalService;

public class MockAccountUpdateApprovalService implements AccountUpdateApprovalService {

    private TaskService taskService;
    private static final Logger LOGGER = LoggerFactory.getLogger(MockAccountUpdateApprovalService.class);
    private String taskName;
    private String actorId;
    private Map<String, Long> tokens = new HashMap<>();

    public MockAccountUpdateApprovalService(TaskService taskService, String taskName, String actorId) {
        this.taskService = taskService;
        this.taskName = taskName;
        this.actorId = actorId;
    }

    public String generateToken(long processInstanceId) {
        String token = UUID.randomUUID().toString();
        tokens.put(token, processInstanceId);
        return token;
    }

    public boolean rejectTask(String token) {
        return completeTask(token, false);
    }

    public boolean approveTask(String token) {
        return completeTask(token, true);
    }

    public boolean completeTask(String token, boolean approve) {
		LOGGER.info("** Approving Task for Token: " + token + " **");
		List<Status> statusList = new ArrayList<>();
        statusList.add(Status.Ready);
        Long processId = getProcessIdByToken(token);
        if(processId == null) {
        	return false;
        }
		List<TaskSummary> approverTasks = taskService.getTasksByStatusByProcessId(processId, statusList, "en-UK");
		for(TaskSummary task : approverTasks) {
			
			if(task.getName().equals(taskName)) {
				LOGGER.info("Starting: " + task.getName());
				taskService.claim(task.getId(), actorId);
		        taskService.start(task.getId(), actorId);
		        
		        Map<String, Object> completeParams = new HashMap<>();
		        completeParams.put("approved", approve);
		        ContentData content = new ContentData();
		        content.setAccessType(AccessType.Inline);
		        content.setContent(getByteArrayFromObject(completeParams));
		
		        taskService.complete(task.getId(), actorId, content);
		        return true;
			}
		}
		return false;
		
	}

    public Long getProcessIdByToken(String token) {
        return tokens.get(token);
    }

    public static byte[] getByteArrayFromObject(Object obj) {
        byte[] result = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            baos.close();
            result = baos.toByteArray();
        }
        catch (IOException ioEx) {
        }
        return result;
    }
}
