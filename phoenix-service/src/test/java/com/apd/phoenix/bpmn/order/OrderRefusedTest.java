package com.apd.phoenix.bpmn.order;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.task.AccessType;
import org.jbpm.task.Task;
import org.jbpm.task.TaskService;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.task.service.ContentData;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.model.CustomerOrder;

public class OrderRefusedTest extends BrmsBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderRefusedTest.class);
    private TaskService taskService;
    private String actorId = "admin";

    public OrderRefusedTest() {
        super(true);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        taskService = this.getTaskService(this.session);
    }

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/order-refused/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Test
    public void cancelOrder() {
    	Map<String, Object> processParams = new HashMap<String, Object>();
    	processParams.put("order", new CustomerOrder());
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) this.session.startProcess(
                "order-refused", processParams);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(1, taskList.size());
        
        Task task = taskService.getTask(taskList.get(0).getId());
        taskService.claim(task.getId(), actorId);
        taskService.start(task.getId(), actorId);
        
        boolean cancelOrder = true;
        
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("cancelOrder", cancelOrder);
        ContentData content = new ContentData();
        content.setAccessType(AccessType.Inline);
        content.setContent(getByteArrayFromObject(completeParams));
        
        taskService.complete(task.getId(), actorId, content);
        
        this.assertProcessInstanceCompleted(processInstance.getId(), this.session);
    }

    @Test
    public void retryOrderTimeout() {
        Map<String, Object> processParams = new HashMap<String, Object>();
        processParams.put("order", new CustomerOrder());
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) this.session.startProcess("order-refused",
                processParams);

        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(1, taskList.size());

        try {
            Thread.sleep(2000);
        }
        catch (InterruptedException e) {
            LOGGER.error("An error occured:", e);
        }

        taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(0, taskList.size());

        this.assertNodeTriggered(processInstance.getId(), "Retry Order");
        this.assertProcessInstanceCompleted(processInstance.getId(), this.session);
    }

    @Test
    public void retryOrderManual(){
    	Map<String, Object> processParams = new HashMap<String, Object>();
    	processParams.put("order", new CustomerOrder());
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) this.session.startProcess(
                "order-refused", processParams);
        
        List<TaskSummary> taskList = taskService.getTasksAssignedAsPotentialOwner(actorId, "en-UK");
        Assert.assertEquals(1, taskList.size());
        
        Task task = taskService.getTask(taskList.get(0).getId());
        taskService.claim(task.getId(), actorId);
        taskService.start(task.getId(), actorId);
        
        boolean cancelOrder = false;
        
        Map<String, Object> completeParams = new HashMap<>();
        completeParams.put("cancelOrder", cancelOrder);
        ContentData content = new ContentData();
        content.setAccessType(AccessType.Inline);
        content.setContent(getByteArrayFromObject(completeParams));
        
        taskService.complete(task.getId(), actorId, content);
        
        this.assertNodeTriggered(processInstance.getId(), "Retry Order");
        this.assertProcessInstanceCompleted(processInstance.getId(), this.session);
    }

    public static byte[] getByteArrayFromObject(Object obj) {
        byte[] result = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(obj);
            oos.flush();
            oos.close();
            baos.close();
            result = baos.toByteArray();
        }
        catch (IOException ioEx) {
        }
        return result;
    }

}
