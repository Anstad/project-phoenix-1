package com.apd.phoenix.bpmn.manifest;

import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;
import java.util.HashMap;
import java.util.Map;
import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.event.process.DefaultProcessEventListener;
import org.drools.event.process.ProcessStartedEvent;
import org.drools.event.rule.DefaultAgendaEventListener;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManifestMain {

    public static StatefulKnowledgeSession session;
    private static final Logger logger = LoggerFactory.getLogger(ManifestMain.class);

    /**
     * @param args
     * @throws InterruptedException 
     */
    public static void main(String[] args) throws InterruptedException {

        KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();

        for (Map.Entry<Resource, ResourceType> entry : getResources().entrySet()) {
            kbuilder.add(entry.getKey(), entry.getValue());
        }

        if (kbuilder.hasErrors()) {
            KnowledgeBuilderErrors errors = kbuilder.getErrors();

            for (KnowledgeBuilderError error : errors) {
                logger.info(">>> Error:" + error.getMessage());

            }
            throw new IllegalStateException(">>> Knowledge couldn't be parsed! ");
        }

        KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();

        kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

        session = kbase.newStatefulKnowledgeSession();

        KnowledgeRuntimeLoggerFactory.newConsoleLogger(session);

        session.addEventListener(new DefaultAgendaEventListener() {

            @Override
            public void afterRuleFlowGroupActivated(org.drools.event.rule.RuleFlowGroupActivatedEvent event) {
                session.fireAllRules();
            }

        });

        session.addEventListener(new DefaultProcessEventListener() {

            @Override
            public void beforeProcessStarted(ProcessStartedEvent event) {
                session.insert(event.getProcessInstance());
            }

        });

        session.getWorkItemManager().registerWorkItemHandler("Camel Service", new MockCamelServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("Notification Service",
                new MockCamelServiceWorkItemHandler());

        //        session.getWorkItemManager().registerWorkItemHandler("Timer Service", new TimerServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(6));

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session
                .startProcess("phoenix.signal-event-test");
        //assertTrue(processInstance.getState() == ProcessInstance.STATE_ACTIVE);
        //session = restoreSession(session, true);
        // now signal process instance
        Thread.sleep(2000);
        session.signalEvent("MyEvent", "SomeValue", processInstance.getId());
        session.signalEvent("MyEvent", "SomeValue", processInstance.getId());
        Thread.sleep(2000);

    }

    public static Map<Resource, ResourceType> getResources() {
        Map<Resource, ResourceType> resources = new HashMap<Resource, ResourceType>();
        resources.put(ResourceFactory.newClassPathResource("brms/order/change-set.xml"), ResourceType.CHANGE_SET);

        return resources;
    }

}
