package com.apd.phoenix.bpmn.invoice;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.invoice.CustomerOrderUpdate;
import com.apd.phoenix.service.invoice.ShippedLineItem;
import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.PaymentInformation;
import com.apd.phoenix.service.model.PaymentType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.PoNumberType;

public class CustomerInvoiceTest extends BrmsBaseTest {

    public CustomerInvoiceTest() {
        super(false);
    }

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		resources.put( ResourceFactory.newClassPathResource("brms/customer-invoice/change-set.xml"), ResourceType.CHANGE_SET);
		return resources;
	}

    @Before
    public void setupWorkItemHandlers() {

        //mockWorkItemHandler = new MockWorkItemHandler();
        MockCamelServiceWorkItemHandler cs = new MockCamelServiceWorkItemHandler();

        this.session.getWorkItemManager().registerWorkItemHandler("Camel Service", cs);
        this.session.getWorkItemManager().registerWorkItemHandler("Update Order", new MockUpdateOrderWIH());

    }

    @Test
    public void incorrectQuantity() {
        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();

        item1.setUnitPrice(new BigDecimal("7.99"));
        lis.setValue("Submitted");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");
        //
        CustomerOrder co = new CustomerOrder();
        OrderStatus os = new OrderStatus();
        os.setValue("Submitted");
        co.setStatus(os);
        PoNumber pon = new PoNumber();
        pon.setValue("12345");
        pon.setType(new PoNumberType());
        pon.getType().setName("APD");
        co.getPoNumbers().add(pon);
        Credential credential = new Credential();
        PaymentInformation payInfo = new PaymentInformation();
        PaymentType payType = new PaymentType();
        payType.setName("cc");
        payInfo.setPaymentType(payType);
        credential.setPaymentInformation(payInfo);
        item1.setOrder(co);

        CustomerOrderUpdate cou = new CustomerOrderUpdate();
        cou.setOrderId("12345");
        cou.setStatus("Shipped");
        cou.setTrackingNumber("Z1234");
        cou.setTrackingProvider("UPS");

        ShippedLineItem sli1 = new ShippedLineItem(1, "1234", "1234", BigDecimal.ZERO, "Shipped", "Z1234");
        cou.getItems().add(sli1);

        session.insert(item1);
        session.insert(sli1);

        Map<String, Object> inputVariables = new HashMap<String, Object>();

        inputVariables.put("customerOrder", co);
        inputVariables.put("customerOrderUpdate", cou);
        inputVariables.put("isvalid", true);
        inputVariables.put("invoiceMethod", credential.getPaymentInformation().getPaymentType().getName());

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("customer-invoice",
                inputVariables);

        //        Assert.assertTrue(sli1.isInvalid());
        //        Assert.assertEquals("Line item quantity mismatch", sli1.getErrorMessages().get(0));
        //        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }

    @Test
    public void correctQuantity() {
        LineItem item1 = new LineItem();
        LineItemStatus lis = new LineItemStatus();

        item1.setUnitPrice(new BigDecimal("7.99"));
        lis.setValue("Submitted");
        item1.setStatus(lis);
        item1.setQuantity(2);
        item1.setApdSku("1234");

        //
        CustomerOrder co = new CustomerOrder();
        OrderStatus os = new OrderStatus();
        os.setValue("Submitted");
        co.setStatus(os);
        PoNumber pon = new PoNumber();
        pon.setValue("12345");
        pon.setType(new PoNumberType());
        pon.getType().setName("APD");
        co.getPoNumbers().add(pon);
        Credential credential = new Credential();
        PaymentInformation payInfo = new PaymentInformation();
        PaymentType payType = new PaymentType();
        payType.setName("cc");
        payInfo.setPaymentType(payType);
        credential.setPaymentInformation(payInfo);
        item1.setOrder(co);

        CustomerOrderUpdate cou = new CustomerOrderUpdate();
        cou.setOrderId("12345");
        cou.setStatus("Shipped");
        cou.setTrackingNumber("Z1234");
        cou.setTrackingProvider("UPS");

        ShippedLineItem sli1 = new ShippedLineItem(2, "1234", "1234", BigDecimal.ZERO, "Shipped", "Z1234");
        cou.getItems().add(sli1);
        co.getItems().add(item1);

        session.insert(item1);
        session.insert(sli1);

        Map<String, Object> inputVariables = new HashMap<String, Object>();

        inputVariables.put("customerOrder", co);
        inputVariables.put("customerOrderUpdate", cou);
        inputVariables.put("isvalid", true);
        inputVariables.put("invoiceMethod", credential.getPaymentInformation().getPaymentType().getName());

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess("customer-invoice",
                inputVariables);

        //        Assert.assertFalse(sli1.isInvalid());
        //        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }

}
