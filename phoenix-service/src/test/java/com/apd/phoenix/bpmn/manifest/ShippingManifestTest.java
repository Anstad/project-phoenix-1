package com.apd.phoenix.bpmn.manifest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import junit.framework.Assert;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.bpmn.BrmsBaseTest;
import com.apd.phoenix.service.manifest.Manifest;
import com.apd.phoenix.service.manifest.VendorManifestsResponse;
import com.apd.phoenix.service.mock.MockCamelServiceWorkItemHandler;

public class ShippingManifestTest extends BrmsBaseTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingManifestTest.class);

    @Override
    protected Map<Resource, ResourceType> getResources() {
        Map<Resource, ResourceType> resources = new HashMap<Resource, ResourceType>();
        resources.put(ResourceFactory.newClassPathResource("brms/shipment-manifest/change-set.xml"),
                ResourceType.CHANGE_SET);

        return resources;
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
        session.getWorkItemManager().registerWorkItemHandler("Camel Service", new MockCamelServiceWorkItemHandler());

        session.getWorkItemManager().registerWorkItemHandler("Notification Service",
                new MockCamelServiceWorkItemHandler());

        //        session.getWorkItemManager().registerWorkItemHandler("Timer Service", new TimerServiceWorkItemHandler());

    }

    @Test
    public void allWorkflowsStarted() {
    	
    	VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(true);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTP Service", new MockFTPWorkItemHandler(vmr));
        
    	session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(5));
        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("count", 0);
        inputVariables.put("error", false);
        inputVariables.put("maxRetryReached", false);
        inputVariables.put("vendor1", "APD");
        inputVariables.put("vendor2", "USSCO");
        //Default settings
        inputVariables.put("delay", 1000);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "manifest-aggregation", inputVariables);
        Assert.assertEquals(3, session.getProcessInstances().size());
        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);;
        }

        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }

    @Test
    public void correctManifestSize() {
    	
    	VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(true);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTP Service", new MockFTPWorkItemHandler(vmr));
        
        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(6));
        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("count", 0);
        inputVariables.put("error", false);
        inputVariables.put("maxRetryReached", false);
        inputVariables.put("vendorName", "APD");
        //Default settings
        inputVariables.put("delay", 1000);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "vendor-shipment-manifest", inputVariables);
        
        try {
            Thread.sleep(5000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);;
        }
        Assert.assertEquals(0, processInstance.getVariable("count"));
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }

    @Test
    public void incorrectManifestSize() {
    	
    	VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(true);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTP Service", new MockFTPWorkItemHandler(vmr));
        
        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(5));
        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("count", 0);
        inputVariables.put("error", false);
        inputVariables.put("maxRetryReached", false);
        inputVariables.put("vendorName", "APD");
        //Default settings
        inputVariables.put("delay", 1000);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "vendor-shipment-manifest", inputVariables);
        
        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);;
        }
        Assert.assertTrue((int)processInstance.getVariable("count") > 0);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }

    @Test
    public void invalidManifest() {
    	VendorManifestsResponse vmr = new VendorManifestsResponse();
        Manifest manifest1 = new Manifest();
        manifest1.setValid(false);
        manifest1.setContent("Hello world");
        List<Manifest> manifests = new ArrayList<>();
        manifests.add(manifest1);
        vmr.setManifests(manifests);
        session.getWorkItemManager().registerWorkItemHandler("FTP Service", new MockFTPWorkItemHandler(vmr));
        
        session.getWorkItemManager().registerWorkItemHandler("S3 Service", new MockS3StorageWorkItemHandler(6));
        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("count", 0);
        inputVariables.put("error", false);
        inputVariables.put("maxRetryReached", false);
        inputVariables.put("vendorName", "APD");
        //Default settings
        inputVariables.put("delay", 1000);
        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) session.startProcess(
                "vendor-shipment-manifest", inputVariables);
        
        try {
            Thread.sleep(10000);
        }
        catch (InterruptedException e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);;
        }
        Assert.assertTrue((int)processInstance.getVariable("count") > 0);
        Assert.assertEquals(WorkflowProcessInstance.STATE_COMPLETED, processInstance.getState());
    }
}
