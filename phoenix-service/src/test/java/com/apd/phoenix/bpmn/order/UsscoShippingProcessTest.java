package com.apd.phoenix.bpmn.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import junit.framework.Assert;
import org.drools.KnowledgeBase;
import org.drools.builder.ResourceType;
import org.drools.definition.type.FactType;
import org.drools.logger.KnowledgeRuntimeLogger;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.test.JbpmJUnitTestCase;
import org.jbpm.workflow.instance.WorkflowProcessInstance;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.ClassificationCode;
import com.apd.phoenix.service.model.ItemClassification;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CodeType;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemClassificationType;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.ItemPropertyType;
import com.apd.phoenix.service.model.ItemXItemPropertyType;

/**
 * This is a file to test a process.
 */
public class UsscoShippingProcessTest extends JbpmJUnitTestCase {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsscoShippingProcessTest.class);

    @Test
    public void testUsscoShippingProcess_LookupZipCode() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("ShippingValidation.dsl", ResourceType.DSL);
        resources.put("ShippingValidation.dslr", ResourceType.DSLR);
        KnowledgeBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        StatefulKnowledgeSession ksession = createKnowledgeSession(kbase); //knowledge session

        KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(ksession);

        FactType orderFactType = intitializeFactType(kbase, "Order");
        Object orderFact = buildFacts(kbase, "Order");
        ksession.insert(orderFact);

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("account_number", "000000");

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.startProcess("ussco-shipping",
                inputVariables);
        assertProcessInstanceActive(processInstance.getId(), ksession);

        //NOTE call fireAllRules or fireAllRules with number of rules to be fired
        ksession.fireAllRules(1);
        assertNodeTriggered(processInstance.getId(), "Lookup Order Zip Code");
        Assert.assertEquals("555555", orderFactType.get(orderFact, "accountNumber"));
        logger.close();
        ksession.dispose();
    }

    @Test
    public void testApdShippingProcess_LookupOrderDetail() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("ShippingValidation.dsl", ResourceType.DSL);
        resources.put("ShippingValidation.dslr", ResourceType.DSLR);
        KnowledgeBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
            ;
        }
        StatefulKnowledgeSession ksession = createKnowledgeSession(kbase); //knowledge session
        KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(ksession);

        FactType orderFactType = intitializeFactType(kbase, "Order");
        Object orderFact = buildFacts(kbase, "Order");
        ksession.insert(orderFact);
        Object NSNItemFact = buildFacts(kbase, "NSNLineItem");
        ksession.insert(NSNItemFact);
        Object HazmatItemFact = buildFacts(kbase, "HazmatLineItem");
        ksession.insert(HazmatItemFact);
        Object OverWeightSizeItemFact = buildFacts(kbase, "OverWeightSizeLineItem");
        ksession.insert(OverWeightSizeItemFact);

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("account_number", "000000");

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.startProcess("ussco-shipping",
                inputVariables);
        assertProcessInstanceActive(processInstance.getId(), ksession);

        ksession.fireAllRules(4);
        assertNodeTriggered(processInstance.getId(), "Lookup Order Detail");
        Assert.assertEquals("333333", orderFactType.get(orderFact, "accountNumber"));
    }

    @Test
    public void testApdShippingProcess_LookupFacilityCode() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("ShippingValidation.dsl", ResourceType.DSL);
        resources.put("ShippingValidation.dslr", ResourceType.DSLR);
        KnowledgeBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
            ;
        }
        StatefulKnowledgeSession ksession = createKnowledgeSession(kbase); //knowledge session
        KnowledgeRuntimeLogger logger = KnowledgeRuntimeLoggerFactory.newConsoleLogger(ksession);

        FactType orderFactType = intitializeFactType(kbase, "Order");
        Object orderFact = buildFacts(kbase, "Order");
        ksession.insert(orderFact);
        Object NSNItemFact = buildFacts(kbase, "NSNLineItem");
        ksession.insert(NSNItemFact);
        Object HazmatItemFact = buildFacts(kbase, "HazmatLineItem");
        ksession.insert(HazmatItemFact);
        Object OverWeightSizeItemFact = buildFacts(kbase, "OverWeightSizeLineItem");
        ksession.insert(OverWeightSizeItemFact);

        Map<String, Object> inputVariables = new HashMap<String, Object>();
        inputVariables.put("account_number", "000000");
        inputVariables.put("order_weight_threshold", 13.0);
        inputVariables.put("facility_code", "fac123");

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.startProcess("ussco-shipping",
                inputVariables);
        assertProcessInstanceActive(processInstance.getId(), ksession);

        ksession.fireAllRules(5);
        assertNodeTriggered(processInstance.getId(), "Lookup Order Facility Code");
        Assert.assertEquals(null, orderFactType.get(orderFact, "facility"));
        Assert.assertEquals("000000", orderFactType.get(orderFact, "accountNumber"));
    }

    @Test
    public void testApdShippingProcess_ReceivedEDI() {
        Map<String, ResourceType> resources = new HashMap<String, ResourceType>();
        resources.put("apdShipping.bpmn", ResourceType.BPMN2);
        resources.put("ShippingValidation.dsl", ResourceType.DSL);
        resources.put("ShippingValidation.dslr", ResourceType.DSLR);
        KnowledgeBase kbase = null;
        try {
            kbase = createKnowledgeBase(resources);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            LOGGER.error("An error occured:", e);
            ;
        }
        StatefulKnowledgeSession ksession = createKnowledgeSession(kbase); //knowledge session

        FactType orderFactType = intitializeFactType(kbase, "Order");
        Object orderFact = buildFacts(kbase, "Order");
        ksession.insert(orderFact);

        WorkflowProcessInstance processInstance = (WorkflowProcessInstance) ksession.startProcess("ussco-shipping",
                null);

        ksession.fireAllRules();
        Assert.assertTrue(processInstance.getNodeInstances().isEmpty());
        ksession.signalEvent("EDI Item Rejects Event", null);
        //processInstance.signalEvent("EDI Item Rejects Event", null);

        LOGGER.info("Item rejects event signaled ");
        LOGGER.info(Integer.toString(processInstance.getNodeInstances().size()));
        assertNodeTriggered(processInstance.getId(), "Validate IR Items");
        //Assert.assertEquals("Check In Patient", processInstance.getNodeInstances().iterator().next().getNodeName());

        //System.out.println(processInstance.getNodeInstances().iterator().next().getNodeName());

        //System.out.println(orderFactType.get(orderFact, "accountNumber"));

    }

    private FactType intitializeFactType(KnowledgeBase kbase, String factType) {
        return kbase.getFactType("Phoenix", factType);
    }

    private Object buildFacts(KnowledgeBase kbase, String factType) {
        FactType type = kbase.getFactType("Phoenix", factType);
        Object apdFact = null;
        try {
            apdFact = type.newInstance();
        }
        catch (InstantiationException e) {
            LOGGER.error("Cannot initiate the class");
        }
        catch (IllegalAccessException e) {
            LOGGER.error("Cannot access the class on the package");
        }
        if (factType.equals("Order"))
            createOrderFacts(apdFact, type);
        else if (factType.equals("NSNLineItem"))
            createNSNLineItemFacts(apdFact, type);
        else if (factType.equals("HazmatLineItem"))
            createHazmatLineItemFacts(apdFact, type);
        else if (factType.equals("OverWeightSizeLineItem"))
            createOverWeightSizeLineItemFacts(apdFact, type);
        return apdFact;
    }

    private void createOrderFacts(Object apdOrder, FactType factType) {
    	Item item_1 = new Item();
    	item_1.setName("Order Item 1");
    	Item item_2 = new Item();
    	item_1.setName("Order Item 2");
    	ItemPropertyType ipt_1 = new ItemPropertyType();
    	ipt_1.setName("IR");
    	ItemXItemPropertyType ixipt_1 = new ItemXItemPropertyType();
    	ixipt_1.setType(ipt_1); ixipt_1.setValue("true");
    	ItemXItemPropertyType ixipt_2 = new ItemXItemPropertyType();
    	ixipt_2.setType(ipt_1); ixipt_2.setValue("false");    	
    	Set<ItemXItemPropertyType> ixipts_1 = new HashSet<ItemXItemPropertyType>();
    	ixipts_1.add(ixipt_1);
    	Set<ItemXItemPropertyType> ixipts_2 = new HashSet<ItemXItemPropertyType>();
    	ixipts_2.add(ixipt_2);
    	item_1.setProperties(ixipts_1);
    	item_2.setProperties(ixipts_2);    	
    	List<Item> items = new ArrayList<>();
    	items.add(item_1); items.add(item_2);
    	CodeType codeType_1 = new CodeType(); codeType_1.setName("Y");
    	CodeType codeType_2 = new CodeType(); codeType_2.setName("Y");
    	CodeType codeType_3 = new CodeType(); codeType_3.setName("Y");
    	ItemClassificationType ict_1 = new ItemClassificationType(); ict_1.setName("NSN");
    	ItemClassificationType ict_2 = new ItemClassificationType(); ict_2.setName("Hazmat");
    	ItemClassificationType ict_3 = new ItemClassificationType(); ict_3.setName("OverWeightSize");
    	ClassificationCode cc_1 = new ClassificationCode(); cc_1.setCodeType(codeType_1);
    	ClassificationCode cc_2 = new ClassificationCode(); cc_2.setCodeType(codeType_2);
    	ClassificationCode cc_3 = new ClassificationCode(); cc_3.setCodeType(codeType_3);
    	ItemClassification ic_1 = new ItemClassification(); 
    	ic_1.setItemClassificationType(ict_1); ic_1.setClassificationCode(cc_1);
    	ItemClassification ic_2 = new ItemClassification(); 
    	ic_2.setItemClassificationType(ict_2); ic_2.setClassificationCode(cc_2);
    	ItemClassification ic_3 = new ItemClassification(); 
    	ic_3.setItemClassificationType(ict_3); ic_3.setClassificationCode(cc_3);
    	List<ItemClassification> ics = new ArrayList<>();
    	ics.add(ic_1); ics.add(ic_2); ics.add(ic_3);
    	Address address = new Address();
    	address.setZip("01001");
    	Vendor vendor = new Vendor();
        vendor.setName("USSCO");
        Account rootAccount = new Account();
        rootAccount.setName("USPS");
        Account account = new Account();
        account.setRootAccount(rootAccount);
        factType.set(apdOrder, "name", "apd_mockup_order");
        factType.set(apdOrder, "accountNumber", "201333");
        factType.set(apdOrder, "items", items);
        factType.set(apdOrder, "ics", ics);
        factType.set(apdOrder, "address", address);
        factType.set(apdOrder, "vendor", vendor);
        factType.set(apdOrder, "customer", account);
        factType.set(apdOrder, "weight", 12.0);
        factType.set(apdOrder, "amount", 24.00);
        factType.set(apdOrder, "facility", "");
    }

    private void createNSNLineItemFacts(Object NSNLineItem, FactType factType) {
        factType.set(NSNLineItem, "accountNumber", "111111");
    }

    private void createHazmatLineItemFacts(Object HazmatLineItem, FactType factType) {
        factType.set(HazmatLineItem, "accountNumber", "222222");
    }

    private void createOverWeightSizeLineItemFacts(Object OverWeightSizeLineItem, FactType factType) {
        factType.set(OverWeightSizeLineItem, "accountNumber", "333333");
    }

}