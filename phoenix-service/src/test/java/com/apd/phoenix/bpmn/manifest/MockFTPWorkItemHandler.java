package com.apd.phoenix.bpmn.manifest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemHandler;
import org.drools.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.manifest.VendorManifestsResponse;

public class MockFTPWorkItemHandler implements WorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(MockFTPWorkItemHandler.class);

    VendorManifestsResponse vmr;

    public MockFTPWorkItemHandler(VendorManifestsResponse vmr) {
        this.vmr = vmr;
    }

    @Override
    public void abortWorkItem(WorkItem workItem, WorkItemManager workItemManager) {

    }

    @Override
    public void executeWorkItem(WorkItem workItem, WorkItemManager workItemManager) {
        logger.info("** Starting TEST FTP Listener **");
        Map<String, Object> outputVariables = new HashMap<String, Object>();

        outputVariables.put("manifestsResponse", vmr);

        workItemManager.completeWorkItem(workItem.getId(), outputVariables);
    }
}
