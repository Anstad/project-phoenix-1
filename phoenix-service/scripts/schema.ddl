
    create table ACCXCREDXUSER_CCENTER (
        id number(19,0) not null,
        active number(1,0) not null,
        currentCharge number(19,2),
        pendingCharge number(19,2),
        totalLimit number(19,2),
        version number(10,0) not null,
        ACCOUNTCREDENTIALUSERS_ID number(19,0) not null,
        ALLOWEDCOSTCENTERS_ID number(19,0) not null,
        primary key (id)
    );

    create table ACCXCREDXUSER_CCENTER_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        active number(1,0),
        currentCharge number(19,2),
        pendingCharge number(19,2),
        totalLimit number(19,2),
        ACCOUNTCREDENTIALUSERS_ID number(19,0),
        ALLOWEDCOSTCENTERS_ID number(19,0),
        primary key (id, REV)
    );

    create table AccXCredXUser_Dept (
        id number(19,0) not null,
        active number(1,0) not null,
        recipientsEmails varchar2(255 char),
        recipientsNames varchar2(255 char),
        version number(10,0) not null,
        AccountXCredentialXUser_id number(19,0) not null,
        departments_id number(19,0) not null,
        primary key (id)
    );

    create table AccXCredXUser_Dept_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        active number(1,0),
        recipientsEmails varchar2(255 char),
        recipientsNames varchar2(255 char),
        AccountXCredentialXUser_id number(19,0),
        departments_id number(19,0),
        primary key (id, REV)
    );

    create table AccXCredXUser_Desktops (
        AccountXCredentialXUser_id number(19,0) not null,
        desktops_id number(19,0) not null,
        primary key (AccountXCredentialXUser_id, desktops_id)
    );

    create table AccXCredXUser_Desktops_AUD (
        REV number(10,0) not null,
        AccountXCredentialXUser_id number(19,0) not null,
        desktops_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, AccountXCredentialXUser_id, desktops_id)
    );

    create table Account (
        id number(19,0) not null,
        apdAssignedAccountId varchar2(255 char),
        creationDate date,
        csrEmail varchar2(255 char),
        csrPhone varchar2(255 char),
        isActive number(1,0) not null,
        locationCode varchar2(255 char),
        name varchar2(255 char) not null unique,
        solomonCustomerId varchar2(255 char) not null,
        useLocationCode number(1,0),
        version number(10,0) not null,
        cxmlConfiguration_id number(19,0),
        parentAccount_id number(19,0),
        paymentInformation_id number(19,0),
        primaryContact_id number(19,0),
        rootAccount_id number(19,0),
        primary key (id)
    );

    create table AccountBRMSRecord (
        id number(19,0) not null,
        active number(1,0),
        processId number(19,0),
        sessionId number(10,0),
        snapshot varchar2(255 char),
        version number(10,0) not null,
        account_id number(19,0),
        primary key (id)
    );

    create table AccountGroup (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table AccountGroup_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table AccountPropertyType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table AccountPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table AccountXAccountPropertyType (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        account_id number(19,0),
        primary key (id)
    );

    create table AccountXCredentialXUser (
        id number(19,0) not null,
        active number(1,0) not null,
        punchLevel varchar2(255 char),
        usAccountNumber varchar2(255 char),
        version number(10,0) not null,
        account_id number(19,0),
        address_id number(19,0),
        approverUser_id number(19,0),
        cred_id number(19,0),
        paymentInformation_id number(19,0),
        user_id number(19,0),
        primary key (id)
    );

    create table Account_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        apdAssignedAccountId varchar2(255 char),
        creationDate date,
        csrEmail varchar2(255 char),
        csrPhone varchar2(255 char),
        isActive number(1,0),
        locationCode varchar2(255 char),
        name varchar2(255 char),
        solomonCustomerId varchar2(255 char),
        useLocationCode number(1,0),
        cxmlConfiguration_id number(19,0),
        parentAccount_id number(19,0),
        paymentInformation_id number(19,0),
        primaryContact_id number(19,0),
        rootAccount_id number(19,0),
        primary key (id, REV)
    );

    create table Account_AccountGroup (
        accounts_id number(19,0) not null,
        groups_id number(19,0) not null,
        primary key (accounts_id, groups_id)
    );

    create table Account_AccountGroup_AUD (
        REV number(10,0) not null,
        accounts_id number(19,0) not null,
        groups_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, accounts_id, groups_id)
    );

    create table Account_Bulletin (
        Account_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        primary key (Account_id, bulletins_id)
    );

    create table Account_Bulletin_AUD (
        REV number(10,0) not null,
        Account_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Account_id, bulletins_id)
    );

    create table Account_IpAddress_AUD (
        REV number(10,0) not null,
        account_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, account_id, id)
    );

    create table Account_PoNumber (
        Account_id number(19,0) not null,
        blanketPos_id number(19,0) not null,
        primary key (Account_id, blanketPos_id)
    );

    create table ActionType (
        id number(19,0) not null,
        description varchar2(255 char),
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table ActionType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        description varchar2(255 char),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Address (
        id number(19,0) not null,
        city varchar2(255 char),
        company varchar2(255 char),
        country varchar2(255 char),
        county varchar2(255 char),
        geoCode varchar2(255 char),
        line1 varchar2(255 char),
        line2 varchar2(255 char),
        name varchar2(255 char),
        pendingShipToAuthorization number(1,0),
        primary number(1,0),
        shipToId varchar2(255 char),
        state varchar2(255 char),
        version number(10,0) not null,
        zip varchar2(255 char),
        account_id number(19,0),
        miscShipTo_id number(19,0),
        person_id number(19,0),
        type_id number(19,0),
        vendor_id number(19,0),
        primary key (id)
    );

    create table AddressPropertyType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table AddressPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table AddressType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table AddressType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table AddressXAddressPropertyType (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        address_id number(19,0),
        type_id number(19,0),
        primary key (id)
    );

    create table Address_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        city varchar2(255 char),
        company varchar2(255 char),
        country varchar2(255 char),
        county varchar2(255 char),
        geoCode varchar2(255 char),
        line1 varchar2(255 char),
        line2 varchar2(255 char),
        name varchar2(255 char),
        pendingShipToAuthorization number(1,0),
        primary number(1,0),
        shipToId varchar2(255 char),
        state varchar2(255 char),
        zip varchar2(255 char),
        account_id number(19,0),
        person_id number(19,0),
        type_id number(19,0),
        vendor_id number(19,0),
        primary key (id, REV)
    );

    create table ApdPoLog (
        id number(19,0) not null,
        creationDate timestamp,
        version number(10,0) not null,
        apdPo_id number(19,0),
        primary key (id)
    );

    create table AttachmentType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table AttachmentType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Bulletin (
        id number(19,0) not null,
        csr number(1,0) not null,
        expirationDate date,
        global number(1,0) not null,
        MESSAGE clob,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table Bulletin_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        csr number(1,0),
        expirationDate date,
        global number(1,0),
        MESSAGE clob,
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table CanceledQuantity (
        id number(19,0) not null,
        cancelDate timestamp,
        quantity number(19,2) not null,
        reason varchar2(255 char),
        version number(10,0),
        lineItem_id number(19,0),
        primary key (id)
    );

    create table CardInformation (
        id number(19,0) not null,
        cardType varchar2(255 char),
        cardVaultToken varchar2(255 char),
        cvvIndicator varchar2(255 char),
        debit number(1,0) not null,
        expiration date,
        ghost number(1,0) not null,
        identifier varchar2(255 char),
        name_on_card varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table CardInformation_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        cardType varchar2(255 char),
        cardVaultToken varchar2(255 char),
        cvvIndicator varchar2(255 char),
        debit number(1,0),
        expiration date,
        ghost number(1,0),
        identifier varchar2(255 char),
        name_on_card varchar2(255 char),
        primary key (id, REV)
    );

    create table Cart (
        id number(19,0) not null,
        estimatedShippingAmount number(19,2),
        expirationDate timestamp,
        maximumTaxToCharge number(19,2),
        version number(10,0) not null,
        userCredential_id number(19,0),
        primary key (id)
    );

    create table CartItem (
        id number(19,0) not null,
        quantity number(10,0) not null,
        version number(10,0) not null,
        catalogXItem_id number(19,0) not null,
        cart_id number(19,0),
        primary key (id)
    );

    create table CashoutPage (
        id number(19,0) not null,
        billtoAddressDisplayFormat varchar2(255 char),
        cancelDate number(1,0),
        deliveryDate number(1,0),
        expectedDate number(1,0),
        hideCCNumber number(1,0),
        higherPriceNotice number(1,0),
        multipleShipTo number(1,0),
        name varchar2(255 char),
        noCardAction varchar2(255 char),
        serviceDate number(1,0),
        shiptoAddressDisplayFormat varchar2(255 char),
        updateUserInformation number(1,0),
        version number(10,0) not null,
        primary key (id)
    );

    create table CashoutPageXField (
        id number(19,0) not null,
        defaultValue varchar2(255 char),
        inboundMapping varchar2(255 char),
        includeLabelInMapping number(1,0),
        label varchar2(255 char),
        required number(1,0) not null,
        updatable number(1,0) not null,
        version number(10,0) not null,
        field_id number(19,0) not null,
        mapping_id number(19,0),
        page_id number(19,0),
        primary key (id)
    );

    create table CashoutPageXField_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        defaultValue varchar2(255 char),
        inboundMapping varchar2(255 char),
        includeLabelInMapping number(1,0),
        label varchar2(255 char),
        required number(1,0),
        updatable number(1,0),
        primary key (id, REV)
    );

    create table Catalog (
        id number(19,0) not null,
        changeDate date,
        changedItemRelevantFields number(1,0),
        expirationDate date,
        lastProcessEnd timestamp,
        lastProcessStart timestamp,
        name varchar2(255 char) not null unique,
        nonUpsableShippingMaximum number(19,2),
        nonUpsableShippingMinimum number(19,2),
        nonUpsableShippingRate number(19,2),
        nonUpsableShippingRatePercent number(1,0),
        regenerateCsvDate date,
        regenerateCsvNightly number(1,0),
        regenerateSmartOciDate date,
        reindexDate date,
        reindexNightly number(1,0),
        startDate date,
        upsableShippingMaximum number(19,2),
        upsableShippingMinimum number(19,2),
        upsableShippingRate number(19,2),
        upsableShippingRatePercent number(1,0),
        version number(10,0) not null,
        customer_id number(19,0),
        parent_id number(19,0),
        replacement_id number(19,0),
        vendor_id number(19,0),
        primary key (id)
    );

    create table CatalogCsv (
        id number(19,0) not null,
        catalogId number(19,0) not null,
        correlationId varchar2(255 char),
        processedItems number(19,0),
        totalItems number(19,0),
        version number(10,0) not null,
        primary key (id)
    );

    create table CatalogUpload (
        id number(19,0) not null,
        action number(10,0),
        catalogId number(19,0),
        completed number(1,0) not null,
        correlationId varchar2(255 char),
        oldCatalogId number(19,0),
        persistChanges number(1,0),
        processedItems number(19,0),
        totalItems number(19,0),
        version number(10,0) not null,
        primary key (id)
    );

    create table CatalogXCategoryXPricingType (
        id number(19,0) not null,
        parameter varchar2(255 char),
        version number(10,0) not null,
        catalog_id number(19,0),
        category_id number(19,0),
        type_id number(19,0),
        primary key (id)
    );

    create table CatalogXItem (
        id number(19,0) not null,
        added number(1,0),
        coreItemExpirationDate date,
        coreItemStartDate date,
        deleted number(1,0),
        lastModified timestamp not null,
        modified number(1,0),
        overrideCatalogs varchar2(255 char),
        price number(19,2) not null,
        pricingParameter varchar2(255 char),
        version number(10,0) not null,
        catalog_id number(19,0) not null,
        customerSku_id number(19,0),
        item_id number(19,0) not null,
        pricingType_id number(19,0),
        substituteItem_id number(19,0),
        primary key (id),
        unique (catalog_id, item_id)
    );

    create table CatalogXItemXItemPropertyType (
        id number(19,0) not null,
        value varchar2(4000 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        catalogxitem_id number(19,0),
        primary key (id)
    );

    create table Catalog_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        lastProcessEnd timestamp,
        lastProcessStart timestamp,
        nonUpsableShippingMaximum number(19,2),
        nonUpsableShippingMinimum number(19,2),
        nonUpsableShippingRate number(19,2),
        nonUpsableShippingRatePercent number(1,0),
        upsableShippingRate number(19,2),
        upsableShippingRatePercent number(1,0),
        primary key (id, REV)
    );

    create table CategoryMarkup (
        id number(19,0) not null,
        markupPercentage number(19,2),
        version number(10,0) not null,
        catalog_id number(19,0),
        category_id number(19,0),
        primary key (id)
    );

    create table CategoryMarkup_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        markupPercentage number(19,2),
        catalog_id number(19,0),
        primary key (id, REV)
    );

    create table ClassificationCode (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        codeType_id number(19,0),
        primary key (id)
    );

    create table ClassificationNote (
        id number(19,0) not null,
        version number(10,0) not null,
        ItemClassification_id number(19,0),
        noteType_id number(19,0),
        primary key (id)
    );

    create table CodeType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table CommunicationMethod (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table CommunicationMethod_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table ContactLogComment (
        id number(19,0) not null,
        commentTimestamp timestamp,
        content varchar2(2000 char) not null,
        version number(10,0) not null,
        apdCsrRep_id number(19,0) not null,
        servicelog_id number(19,0),
        issuelog_id number(19,0),
        primary key (id)
    );

    create table ContactMethod (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ContactMethod_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table CostCenter (
        id number(19,0) not null,
        description varchar2(255 char),
        name varchar2(255 char),
        ccNumber varchar2(255 char),
        spendingLimit number(19,2),
        totalSpent number(19,2),
        version number(10,0) not null,
        period_id number(19,0),
        primary key (id)
    );

    create table CostCenter_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        description varchar2(255 char),
        name varchar2(255 char),
        ccNumber varchar2(255 char),
        spendingLimit number(19,2),
        totalSpent number(19,2),
        period_id number(19,0),
        primary key (id, REV)
    );

    create table Credential (
        id number(19,0) not null,
        activationDate date,
        adot number(1,0) not null,
        authorizeAddress number(1,0) not null,
        cidest varchar2(255 char),
        cimtype varchar2(255 char),
        csrEmail varchar2(255 char),
        csrPhone varchar2(255 char),
        customFleName varchar2(255 char),
        customerOutgoingSharedSecret varchar2(255 char),
        defaultPunchoutZipCode varchar2(255 char),
        deploymentmode varchar2(255 char),
        ediRecCode varchar2(255 char),
        ediVersion varchar2(255 char),
        expirationDate date,
        idest varchar2(255 char),
        imtype varchar2(255 char),
        name varchar2(255 char) not null unique,
        outgoingSenderSharedSecret varchar2(255 char),
        partnerId varchar2(255 char),
        poackdest varchar2(255 char),
        poackmtype varchar2(255 char),
        punchopallow varchar2(255 char),
        punchouttype varchar2(255 char),
        requireZipCodeInput number(1,0) not null,
        RETURNCARTDOM varchar2(255 char),
        RETURNCARTID varchar2(255 char),
        sndest varchar2(255 char),
        snmtype varchar2(255 char),
        specialFleName varchar2(255 char),
        storeCredit number(19,2),
        terms varchar2(255 char),
        usrGuideFleName varchar2(255 char),
        vendorName varchar2(255 char),
        vendorOrderUrl varchar2(255 char),
        vendorPunchoutUrl varchar2(255 char),
        version number(10,0) not null,
        willCallEdiOverride number(1,0) not null,
        wrapAndLabel number(1,0) not null,
        cashoutPage_id number(19,0),
        catalog_id number(19,0),
        communicationMethod_id number(19,0),
        defaultShipFromAddress_id number(19,0),
        paymentInformation_id number(19,0),
        rootAccount_id number(19,0) not null,
        skuType_id number(19,0),
        user_id number(19,0),
        primary key (id)
    );

    create table CredentialPropertyType (
        id number(19,0) not null,
        defaultValue varchar2(255 char),
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table CredentialPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        defaultValue varchar2(255 char),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table CredentialXVendor (
        id number(19,0) not null,
        accountId varchar2(255 char) not null,
        version number(10,0) not null,
        credential_id number(19,0),
        vendor_id number(19,0),
        primary key (id)
    );

    create table CredentialXVendor_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        accountId varchar2(255 char),
        credential_id number(19,0),
        vendor_id number(19,0),
        primary key (id, REV)
    );

    create table Credential_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        activationDate date,
        adot number(1,0),
        authorizeAddress number(1,0),
        cidest varchar2(255 char),
        cimtype varchar2(255 char),
        csrEmail varchar2(255 char),
        csrPhone varchar2(255 char),
        customFleName varchar2(255 char),
        customerOutgoingSharedSecret varchar2(255 char),
        defaultPunchoutZipCode varchar2(255 char),
        deploymentmode varchar2(255 char),
        ediRecCode varchar2(255 char),
        ediVersion varchar2(255 char),
        expirationDate date,
        idest varchar2(255 char),
        imtype varchar2(255 char),
        name varchar2(255 char),
        outgoingSenderSharedSecret varchar2(255 char),
        partnerId varchar2(255 char),
        poackdest varchar2(255 char),
        poackmtype varchar2(255 char),
        punchopallow varchar2(255 char),
        punchouttype varchar2(255 char),
        requireZipCodeInput number(1,0),
        RETURNCARTDOM varchar2(255 char),
        RETURNCARTID varchar2(255 char),
        sndest varchar2(255 char),
        snmtype varchar2(255 char),
        specialFleName varchar2(255 char),
        storeCredit number(19,2),
        terms varchar2(255 char),
        usrGuideFleName varchar2(255 char),
        vendorName varchar2(255 char),
        vendorOrderUrl varchar2(255 char),
        vendorPunchoutUrl varchar2(255 char),
        willCallEdiOverride number(1,0),
        wrapAndLabel number(1,0),
        catalog_id number(19,0),
        communicationMethod_id number(19,0),
        defaultShipFromAddress_id number(19,0),
        paymentInformation_id number(19,0),
        rootAccount_id number(19,0),
        skuType_id number(19,0),
        user_id number(19,0),
        primary key (id, REV)
    );

    create table Credential_Bulletin (
        Credential_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        primary key (Credential_id, bulletins_id)
    );

    create table Credential_Bulletin_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, bulletins_id)
    );

    create table Credential_CostCenter (
        Credential_id number(19,0) not null,
        costCenters_id number(19,0) not null,
        primary key (Credential_id, costCenters_id)
    );

    create table Credential_CostCenter_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        costCenters_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, costCenters_id)
    );

    create table Credential_Department (
        Credential_id number(19,0) not null,
        departments_id number(19,0) not null,
        primary key (Credential_id, departments_id)
    );

    create table Credential_Department_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        departments_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, departments_id)
    );

    create table Credential_LimitApproval (
        Credential_id number(19,0) not null,
        limitApproval_id number(19,0) not null,
        primary key (Credential_id, limitApproval_id)
    );

    create table Credential_LimitApproval_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        limitApproval_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, limitApproval_id)
    );

    create table Credential_Permission (
        Credential_id number(19,0) not null,
        permissions_id number(19,0) not null,
        primary key (Credential_id, permissions_id)
    );

    create table Credential_Permission_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        permissions_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, permissions_id)
    );

    create table Credential_PoNumber (
        Credential_id number(19,0) not null,
        blanketPos_id number(19,0) not null,
        primary key (Credential_id, blanketPos_id)
    );

    create table Credential_PropertyType (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        cred_id number(19,0),
        primary key (id)
    );

    create table Credential_PropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        value varchar2(255 char),
        type_id number(19,0),
        primary key (id, REV)
    );

    create table Credential_Role (
        Credential_id number(19,0) not null,
        roles_id number(19,0) not null,
        primary key (Credential_id, roles_id)
    );

    create table Credential_Role_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        roles_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, roles_id)
    );

    create table CsvUploadedItem (
        id number(19,0) not null,
        version number(10,0) not null,
        primary key (id)
    );

    create table CsvUploadedItemField (
        id number(19,0) not null,
        key varchar2(255 char) not null,
        value varchar2(4000 char) not null,
        version number(10,0) not null,
        uploadeditem_id number(19,0),
        primary key (id)
    );

    create table CustomerCost (
        id number(19,0) not null,
        value number(19,2),
        version number(10,0) not null,
        customer_id number(19,0),
        item_id number(19,0),
        primary key (id)
    );

    create table CustomerOrder (
        id number(19,0) not null,
        adot number(1,0) not null,
        apdSalesPerson varchar2(255 char),
        approvalComment varchar2(4000 char),
        assignedCostCenterLabel varchar2(255 char),
        buyerCookie varchar2(255 char),
        cancelDate date,
        currencyCode varchar2(255 char),
        customerRevisionDate timestamp,
        customerRevisionNumber number(10,0),
        deliveryDate date,
        estimatedShipping varchar2(255 char),
        estimatedShippingAmount number(19,2),
        maximumTaxToCharge number(19,2),
        merchandiseTypeCode varchar2(255 char),
        operationAllowed varchar2(255 char),
        orderDate timestamp,
        orderExtrasArtifact varchar2(4000 char),
        orderOrigin varchar2(255 char),
        orderPayloadId varchar2(255 char),
        orderTotal number(19,2) not null,
        passedInitialValidation number(1,0) not null,
        paymentTransactionKey varchar2(255 char),
        requestedDeliveryDate date,
        requisitionName varchar2(255 char),
        resendAttempts number(10,0),
        shipDate date,
        shipNoLaterDate date,
        shipNotBeforeDate date,
        shipToExtrasArtifact varchar2(4000 char),
        taxAssigned number(1,0),
        terms varchar2(255 char),
        version number(10,0) not null,
        willCallEdiOverride number(1,0) not null,
        wrapAndLabel number(1,0) not null,
        account_id number(19,0),
        address_id number(19,0),
        assignedCostCenter_id number(19,0),
        cardInformation_id number(19,0),
        credential_id number(19,0),
        customerStatus_id number(19,0),
        parentOrder_id number(19,0),
        paymentInformation_id number(19,0),
        paymentStatus_id number(19,0),
        status_id number(19,0),
        type_id number(19,0),
        user_id number(19,0),
        primary key (id)
    );

    create table CustomerOrderRequest (
        id number(19,0) not null,
        token varchar2(255 char) not null unique,
        version number(10,0) not null,
        customerOrder_id number(19,0),
        primary key (id)
    );

    create table CustomerOrder_TaxType (
        id number(19,0) not null,
        value number(19,2) not null,
        version number(10,0) not null,
        type_id number(19,0) not null,
        order_id number(19,0),
        primary key (id)
    );

    create table CustomerSpecificIconUrl (
        id number(19,0) not null,
        overrideIconUrl varchar2(255 char),
        version number(10,0) not null,
        classificationType_id number(19,0),
        propertyType_id number(19,0),
        account_id number(19,0),
        primary key (id),
        check ((PROPERTYTYPE_ID IS NULL AND CLASSIFICATIONTYPE_ID IS NOT NULL) OR (PROPERTYTYPE_ID IS NOT NULL AND CLASSIFICATIONTYPE_ID IS NULL))
    );

    create table CxmlConfiguration (
        id number(19,0) not null,
        credentialNamePrefix varchar2(255 char),
        credentialNameXpathExpression varchar2(255 char),
        deploymentMode varchar2(255 char),
        name varchar2(255 char) unique,
        senderSharedSecret varchar2(255 char),
        systemUserPrefix varchar2(255 char),
        systemUserXpathExpression varchar2(255 char),
        SYSTEMUSERXPATHEXPDEFRES varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table CxmlConfiguration_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        credentialNamePrefix varchar2(255 char),
        credentialNameXpathExpression varchar2(255 char),
        deploymentMode varchar2(255 char),
        name varchar2(255 char),
        senderSharedSecret varchar2(255 char),
        systemUserPrefix varchar2(255 char),
        systemUserXpathExpression varchar2(255 char),
        SYSTEMUSERXPATHEXPDEFRES varchar2(255 char),
        primary key (id, REV)
    );

    create table CxmlCredential (
        id number(19,0) not null,
        domain varchar2(255 char),
        identifier varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table CxmlCredential_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        domain varchar2(255 char),
        identifier varchar2(255 char),
        primary key (id, REV)
    );

    create table Department (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table Department_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table DescriptionType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table Desktop (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table Desktop_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table DimensionType (
        id number(19,0) not null,
        dimensionTypeName varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table FavoritesList (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        catalog_id number(19,0),
        axcxu_id number(19,0),
        primary key (id),
        unique (axcxu_id, name, catalog_id)
    );

    create table Field (
        id number(19,0) not null,
        name varchar2(255 char) unique,
        version number(10,0) not null,
        fieldType_id number(19,0),
        primary key (id)
    );

    create table FieldOptions (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        field_id number(19,0),
        primary key (id)
    );

    create table FieldType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table GlobalSetting (
        id number(19,0) not null,
        name varchar2(255 char),
        value varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table HierarchyNode (
        id number(19,0) not null,
        description varchar2(255 char),
        indexedPaths varchar2(4000 char),
        publicid number(19,0) not null,
        version number(10,0) not null,
        parent_id number(19,0),
        primary key (id)
    );

    create table INVOICE (
        FROM_CLASS varchar2(31 char) not null,
        id number(19,0) not null,
        amount number(19,2),
        approvalDate timestamp,
        billingTypeCode varchar2(255 char),
        createdDate timestamp,
        dueDate date,
        invoiceDate date,
        invoiceNumber varchar2(255 char),
        version number(10,0) not null,
        raNumber varchar2(255 char),
        restockingFee number(19,2),
        status varchar2(255 char),
        valid number(1,0),
        returnOrder_id number(19,0),
        shipment_id number(19,0),
        customerOrder_id number(19,0),
        primary key (id)
    );

    create table INVOICE_LineItem (
        INVOICE_id number(19,0) not null,
        actualItems_id number(19,0) not null
    );

    create table INVOICE_SPECIAL (
        INVOICE_ID number(19,0) not null,
        INSTRUCTION varchar2(255 char)
    );

    create table ISSUE_LOG (
        id number(19,0) not null,
        contactEmail varchar2(255 char),
        contactName varchar2(255 char),
        contactNumber varchar2(255 char),
        department varchar2(255 char),
        followUpComplete number(1,0),
        openedDate timestamp not null,
        resolution varchar2(255 char),
        resolvedDate timestamp,
        status varchar2(255 char),
        ticketNumber varchar2(255 char),
        updateTimestamp timestamp,
        version number(10,0) not null,
        expectedResolution timestamp,
        issueCategory varchar2(255 char),
        issueDescription varchar2(1000 char),
        scheduledFollowUp timestamp,
        customerOrder_id number(19,0),
        reporter_id number(19,0),
        primary key (id)
    );

    create table ITEMCLASSIFICATION_ITEM (
        items_id number(19,0) not null,
        itemclassifications_id number(19,0) not null,
        primary key (items_id, itemclassifications_id)
    );

    create table ITEM_ITEM (
        id number(19,0) not null,
        position number(10,0) not null,
        relationship varchar2(255 char) not null,
        version number(10,0) not null,
        ITEM_ID number(19,0) not null,
        SIMILARITEMS_ID number(19,0) not null,
        primary key (id)
    );

    create table ImageType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table InvoiceSendMethod (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table IpAddress (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        cred_id number(19,0),
        account_id number(19,0),
        primary key (id)
    );

    create table IpAddress_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        value varchar2(255 char),
        primary key (id, REV)
    );

    create table Item (
        id number(19,0) not null,
        commodityCode varchar2(255 char),
        description varchar2(4000 char) not null,
        externalCatalogChange number(1,0),
        externalCatalogRelevant number(1,0),
        itemWeight number(19,2),
        lastCostChangeDate timestamp,
        lastModified timestamp not null,
        minimum number(10,0),
        multiple number(10,0),
        name varchar2(255 char) not null,
        searchTerms varchar2(4000 char),
        status varchar2(255 char),
        version number(10,0) not null,
        abilityOneSubstitute_id number(19,0),
        hierarchyNode_id number(19,0),
        itemCategory_id number(19,0),
        manufacturer_id number(19,0),
        replacement_id number(19,0),
        unitOfMeasure_id number(19,0) not null,
        vendorCatalog_id number(19,0) not null,
        primary key (id)
    );

    create table ItemCategory (
        id number(19,0) not null,
        name varchar2(255 char) unique,
        version number(10,0) not null,
        parent_id number(19,0),
        primary key (id)
    );

    create table ItemClassification (
        id number(19,0) not null,
        version number(10,0) not null,
        classificationCode_id number(19,0),
        itemClassificationImage_id number(19,0),
        ItemClassType_id number(19,0),
        primary key (id)
    );

    create table ItemClassificationImage (
        id number(19,0) not null,
        imageUrl varchar2(255 char),
        version number(10,0) not null,
        imageType_id number(19,0),
        primary key (id)
    );

    create table ItemClassificationType (
        id number(19,0) not null,
        iconUrl varchar2(255 char),
        name varchar2(255 char) not null unique,
        toolTipLabel varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ItemDimension (
        id number(19,0) not null,
        value double precision,
        version number(10,0) not null,
        dimensionType_id number(19,0),
        item_id number(19,0),
        unitOfMeasure_id number(19,0),
        primary key (id)
    );

    create table ItemHistory (
        id number(19,0) not null,
        description varchar2(255 char),
        time timestamp,
        version number(10,0) not null,
        item_id number(19,0),
        primary key (id)
    );

    create table ItemIdentifier (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        item_id number(19,0),
        itemIdentifierType_id number(19,0),
        primary key (id)
    );

    create table ItemIdentifierType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table ItemImage (
        id number(19,0) not null,
        imageName varchar2(255 char),
        imageUrl varchar2(255 char),
        primary number(1,0),
        version number(10,0) not null,
        imageType_id number(19,0),
        item_id number(19,0),
        primary key (id)
    );

    create table ItemPackaging (
        id number(19,0) not null,
        version number(10,0) not null,
        item_id number(19,0),
        packagingDimension_id number(19,0),
        packagingType_id number(19,0),
        primary key (id)
    );

    create table ItemPropertyType (
        id number(19,0) not null,
        iconUrl varchar2(255 char),
        name varchar2(255 char) not null unique,
        supplementary number(1,0) not null,
        toolTipLabel varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ItemPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        iconUrl varchar2(255 char),
        name varchar2(255 char),
        supplementary number(1,0),
        toolTipLabel varchar2(255 char),
        primary key (id, REV)
    );

    create table ItemSellingPoint (
        id number(19,0) not null,
        position number(10,0) not null,
        value varchar2(4000 char) not null,
        version number(10,0) not null,
        item_id number(19,0),
        primary key (id)
    );

    create table ItemSpecification (
        id number(19,0) not null,
        itemSpecificationId number(10,0),
        version number(10,0) not null,
        item_id number(19,0),
        primary key (id)
    );

    create table ItemXItemPropertyType (
        id number(19,0) not null,
        value varchar2(4000 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        item_id number(19,0),
        primary key (id)
    );

    create table Item_Matchbook (
        Item_id number(19,0) not null,
        matchbook_id number(19,0) not null,
        primary key (Item_id, matchbook_id)
    );

    create table JumptrackStopConfiguration (
        id number(19,0) not null,
        allowShipFriday number(1,0) not null,
        allowShipMonday number(1,0) not null,
        allowShipSaturday number(1,0) not null,
        allowShipSunday number(1,0) not null,
        allowShipThursday number(1,0) not null,
        allowShipTuesday number(1,0) not null,
        allowShipWednesday number(1,0) not null,
        version number(10,0) not null,
        zip varchar2(255 char) not null unique,
        primary key (id)
    );

    create table LimitApproval (
        id number(19,0) not null,
        limit number(19,2),
        type number(10,0) not null,
        value varchar2(255 char),
        AXCXU_ID number(19,0),
        primary key (id)
    );

    create table LimitApproval_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        limit number(19,2),
        type number(10,0),
        value varchar2(255 char),
        AXCXU_ID number(19,0),
        primary key (id, REV)
    );

    create table LineItem (
        id number(19,0) not null,
        apdSku varchar2(255 char),
        backorderedQuantity number(19,2),
        core number(1,0),
        cost number(19,2),
        customerExpectedUnitPrice number(19,2),
        customerLineNumber varchar2(255 char),
        description varchar2(4000 char) not null,
        estimatedShippingAmount number(19,2) not null,
        extraneousCustomerSku varchar2(255 char),
        globalTradeIdentificatonNumber varchar2(255 char),
        globalTradeItemNumber varchar2(255 char),
        hasBeenRejected number(1,0),
        itemType varchar2(255 char),
        leadTime number(10,0),
        lineNumber number(10,0),
        manufacturerName varchar2(255 char),
        manufacturerPartId varchar2(255 char),
        maximumTaxToCharge number(19,2),
        originalQuantity number(10,0),
        originallyAddedName varchar2(255 char),
        originallyAddedSku varchar2(255 char),
        parentLineNumber number(10,0),
        purchaseComment varchar2(255 char),
        quantity number(10,0) not null,
        quantityInvoicedByVendor number(19,2),
        returnShipping number(1,0),
        shipToExtrasArtifact varchar2(4000 char),
        shortName varchar2(255 char),
        supplierPartAuxiliaryId varchar2(255 char),
        supplierPartId varchar2(255 char),
        taxable number(1,0),
        unitPrice number(19,2) not null,
        universalProductCode varchar2(255 char),
        unspscClassification varchar2(255 char),
        url varchar2(255 char),
        version number(10,0) not null,
        category_id number(19,0),
        customerExpectedUoM_id number(19,0),
        customerSku_id number(19,0),
        expected_id number(19,0),
        item_id number(19,0),
        order_id number(19,0),
        paymentStatus_id number(19,0),
        status_id number(19,0),
        unitOfMeasure_id number(19,0) not null,
        vendor_id number(19,0),
        primary key (id)
    );

    create table LineItemHistory (
        id number(19,0) not null,
        description varchar2(255 char),
        updateTimestamp date,
        version number(10,0) not null,
        actionType_id number(19,0),
        lineItem_id number(19,0),
        lineItemStatus_id number(19,0),
        primary key (id)
    );

    create table LineItemStatus (
        id number(19,0) not null,
        description varchar2(255 char),
        paymentStatus number(1,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table LineItemXReturn (
        id number(19,0) not null,
        quantity number(19,2) not null,
        reason varchar2(255 char),
        restockingFee number(19,2),
        returnShipping number(1,0),
        returnShippingAmount number(19,2),
        version number(10,0) not null,
        lineItem_id number(19,0),
        INVOICE_ID number(19,0),
        RETURNORDER_ID number(19,0),
        primary key (id)
    );

    create table LineItemXShipment (
        id number(19,0) not null,
        paid number(1,0),
        quantity number(19,2) not null,
        shipping number(19,2),
        version number(10,0) not null,
        lineItem_id number(19,0),
        shipments_id number(19,0),
        primary key (id)
    );

    create table LineItemXShipment_TaxType (
        id number(19,0) not null,
        value number(19,2) not null,
        version number(10,0) not null,
        lineItemXReturn_id number(19,0),
        lineItemXShipment_id number(19,0),
        type_id number(19,0),
        primary key (id),
        check (LINEITEMXSHIPMENT_ID IS NOT NULL OR LINEITEMXRETURN_ID IS NOT NULL AND NOT (LINEITEMXSHIPMENT_ID IS NOT NULL AND LINEITEMXRETURN_ID IS NOT NULL))
    );

    create table Manufacturer (
        id number(19,0) not null,
        name varchar2(255 char) unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table Manufacturer_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Matchbook (
        id number(19,0) not null,
        device varchar2(255 char),
        family varchar2(255 char),
        model varchar2(255 char) not null,
        version number(10,0) not null,
        manufacturer_id number(19,0) not null,
        primary key (id),
        unique (manufacturer_id, model)
    );

    create table Matchbook_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        device varchar2(255 char),
        family varchar2(255 char),
        model varchar2(255 char),
        manufacturer_id number(19,0),
        primary key (id, REV)
    );

    create table MessageMapping (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table MessageMetadata (
        id number(19,0) not null,
        attachmentName varchar2(255 char),
        bccRecipients varchar2(2000 char),
        ccRecipients varchar2(2000 char),
        communicationType varchar2(255 char),
        contentLength number(19,0) not null,
        contentType varchar2(255 char),
        destination varchar2(2000 char),
        filePath varchar2(255 char),
        groupId varchar2(255 char),
        interchangeId varchar2(255 char),
        messageDate date,
        messageType varchar2(255 char),
        receiverId varchar2(255 char),
        senderId varchar2(255 char),
        transactionId varchar2(255 char),
        version number(10,0) not null,
        objectsWrapper_id number(19,0),
        primary key (id)
    );

    create table MessageObjectsWrapper (
        id number(19,0) not null,
        data blob,
        version number(10,0) not null,
        primary key (id)
    );

    create table MilitaryZip (
        id number(19,0) not null,
        city varchar2(255 char),
        county varchar2(255 char),
        state varchar2(255 char),
        uspsCity varchar2(255 char),
        version number(10,0) not null,
        zip varchar2(255 char),
        primary key (id)
    );

    create table MiscShipTo (
        id number(19,0) not null,
        Area varchar2(255 char),
        addressId varchar2(255 char),
        addressId36 varchar2(255 char),
        airportCode varchar2(255 char),
        apdInterchangeId varchar2(255 char),
        carrierName varchar2(255 char),
        carrierRouting varchar2(255 char),
        contactName varchar2(255 char),
        contractNumber varchar2(255 char),
        contractingOfficer varchar2(255 char),
        customerShipToID varchar2(255 char),
        dcNumber varchar2(255 char),
        deliverToName varchar2(255 char),
        deliveryDate varchar2(255 char),
        department varchar2(255 char),
        dept varchar2(255 char),
        desktop varchar2(255 char),
        district varchar2(255 char),
        division varchar2(255 char),
        ediID varchar2(255 char),
        facility varchar2(255 char),
        fedexNumber varchar2(255 char),
        fedstripNumber varchar2(255 char),
        financeNumber varchar2(255 char),
        glNumber varchar2(255 char),
        glnID varchar2(255 char),
        headerComments varchar2(255 char),
        lob varchar2(255 char),
        locationId varchar2(255 char),
        locations varchar2(255 char),
        mailStop varchar2(255 char),
        manager varchar2(255 char),
        nameOfHospital varchar2(255 char),
        nowl varchar2(255 char),
        oldRelease varchar2(255 char),
        orderEmail varchar2(255 char),
        origin varchar2(255 char),
        pol varchar2(255 char),
        purchasingGroup varchar2(255 char),
        requesterName varchar2(255 char),
        requesterPhone varchar2(255 char),
        shippingMethod varchar2(255 char),
        solomonId varchar2(255 char),
        storeNumber varchar2(255 char),
        street2 varchar2(255 char),
        street3 varchar2(255 char),
        unit varchar2(255 char),
        usAccount varchar2(255 char),
        uspsUnder13Oz varchar2(255 char),
        vendor varchar2(255 char),
        primary key (id)
    );

    create table NoteType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table NotificationLimit (
        id number(19,0) not null,
        eventType varchar2(255 char),
        limitType varchar2(255 char),
        version number(10,0) not null,
        credential_id number(19,0),
        primary key (id)
    );

    create table NotificationProperties (
        id number(19,0) not null,
        addressing varchar2(255 char),
        eventType varchar2(255 char),
        recipients varchar2(2000 char) not null,
        version number(10,0) not null,
        order_id number(19,0),
        cred_id number(19,0),
        acc_id number(19,0),
        primary key (id)
    );

    create table ORDER_LOG (
        FROM_CLASS varchar2(31 char) not null,
        id number(19,0) not null,
        eventType varchar2(255 char),
        updateTimestamp timestamp,
        version number(10,0) not null,
        changingUser varchar2(255 char),
        description varchar2(3000 char),
        apdPo varchar2(255 char),
        authCode varchar2(255 char),
        cardExpriation date,
        card_identifier varchar2(255 char),
        custPo varchar2(255 char),
        failureReason varchar2(255 char),
        freightAmount number(19,2),
        itemCount number(10,0),
        name_on_card varchar2(255 char),
        salesTax number(19,2),
        subtotal number(19,2),
        timestamp timestamp,
        totalAmount number(19,2),
        trackingNum varchar2(255 char),
        transactionAmount number(19,2),
        transactionKey varchar2(255 char),
        transactionSuccess number(1,0),
        transactionType varchar2(255 char),
        note varchar2(255 char),
        returnOrderStatus varchar2(255 char),
        customerOrder_id number(19,0),
        messageMetadata_id number(19,0),
        orderStatus_id number(19,0),
        creditcard_returnorder_id number(19,0),
        invoice_id number(19,0),
        lineItemStatus_id number(19,0),
        returnOrder_id number(19,0),
        shipment_id number(19,0),
        lineItem_id number(19,0),
        primary key (id),
        check ((FROM_CLASS!='CREDITCARDTRANSACTION_LOG') OR ((ITEMCOUNT IS NOT NULL) AND (TRANSACTIONSUCCESS IS NOT NULL)))
    );

    create table ORDER_LOG_LineItemXShipment (
        ORDER_LOG_id number(19,0) not null,
        lineItemXShipments_id number(19,0) not null
    );

    create table OrderAttachment (
        id number(19,0) not null,
        attachmentTimestamp timestamp not null,
        fileName varchar2(255 char) not null,
        version number(10,0) not null,
        user_id number(19,0) not null,
        servicelog_id number(19,0),
        order_id number(19,0),
        primary key (id)
    );

    create table OrderBRMSRecord (
        id number(19,0) not null,
        active number(1,0),
        entryId number(19,0) not null,
        processId number(19,0),
        sessionId number(10,0),
        snapshot varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table OrderProcessLookup (
        id number(19,0) not null,
        version number(10,0) not null,
        customerOrder_id number(19,0),
        primary key (id)
    );

    create table OrderStatus (
        id number(19,0) not null,
        customerStatus number(1,0) not null,
        description varchar2(255 char),
        paymentStatus number(1,0) not null,
        value varchar2(255 char) unique,
        version number(10,0) not null,
        overrideStatus_id number(19,0),
        primary key (id)
    );

    create table OrderType (
        id number(19,0) not null,
        description varchar2(255 char),
        value varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table PackagingDimension (
        id number(19,0) not null,
        value double precision,
        version number(10,0) not null,
        dimensionType_id number(19,0),
        unitOfMeasure_id number(19,0),
        primary key (id)
    );

    create table PackagingType (
        id number(19,0) not null,
        description varchar2(255 char),
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table PartnerIdXDeliveryLocation (
        id number(19,0) not null,
        deliveryLocation varchar2(255 char) not null,
        partnerId varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table PartnerIdXDeliveryLocation_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        deliveryLocation varchar2(255 char),
        partnerId varchar2(255 char),
        primary key (id, REV)
    );

    create table PasswordResetEntry (
        id number(19,0) not null,
        expirationDate timestamp,
        generatedKey varchar2(255 char),
        hash varchar2(255 char),
        salt varchar2(255 char),
        version number(10,0) not null,
        user_id number(19,0),
        primary key (id)
    );

    create table PasswordResetEntry_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        expirationDate timestamp,
        generatedKey varchar2(255 char),
        hash varchar2(255 char),
        salt varchar2(255 char),
        user_id number(19,0),
        primary key (id, REV)
    );

    create table PaymentInformation (
        id number(19,0) not null,
        summary number(1,0) not null,
        version number(10,0) not null,
        billingAddress_id number(19,0),
        card_id number(19,0),
        contact_id number(19,0),
        invoiceSendMethod_id number(19,0),
        paymentType_id number(19,0) not null,
        primary key (id)
    );

    create table PaymentInformation_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        summary number(1,0),
        billingAddress_id number(19,0),
        card_id number(19,0),
        contact_id number(19,0),
        paymentType_id number(19,0),
        primary key (id, REV)
    );

    create table PaymentType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table PaymentType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table PendingWorkflowCommand (
        id number(19,0) not null,
        commandName varchar2(255 char),
        ctx long raw,
        executed number(1,0) not null,
        expires timestamp,
        version number(10,0) not null,
        primary key (id)
    );

    create table PeriodType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table PeriodType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Permission (
        id number(19,0) not null,
        allowDelete number(1,0) not null,
        allowRead number(1,0) not null,
        allowWrite number(1,0) not null,
        description varchar2(255 char),
        name varchar2(255 char) not null,
        version number(10,0) not null,
        primary key (id)
    );

    create table PermissionXUser (
        id number(19,0) not null,
        expirationDate date,
        startDate date,
        version number(10,0) not null,
        permission_id number(19,0),
        user_id number(19,0),
        primary key (id)
    );

    create table PermissionXUser_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        expirationDate date,
        startDate date,
        permission_id number(19,0),
        primary key (id, REV)
    );

    create table Permission_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        allowDelete number(1,0),
        allowRead number(1,0),
        allowWrite number(1,0),
        description varchar2(255 char),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Person (
        id number(19,0) not null,
        email varchar2(255 char),
        firstName varchar2(255 char),
        lastName varchar2(255 char),
        middleInitial varchar2(255 char),
        prefix varchar2(255 char),
        suffix varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table Person_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        email varchar2(255 char),
        firstName varchar2(255 char),
        lastName varchar2(255 char),
        middleInitial varchar2(255 char),
        prefix varchar2(255 char),
        suffix varchar2(255 char),
        primary key (id, REV)
    );

    create table PhoneNumber (
        id number(19,0) not null,
        areaCode varchar2(255 char),
        countryCode varchar2(255 char),
        exchange varchar2(255 char),
        extension varchar2(255 char),
        lineNumber varchar2(255 char),
        value varchar2(255 char),
        version number(10,0) not null,
        account_id number(19,0),
        person_id number(19,0),
        type_id number(19,0),
        vendor_id number(19,0),
        primary key (id)
    );

    create table PhoneNumberType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table PhoneNumberType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table PhoneNumber_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        areaCode varchar2(255 char),
        countryCode varchar2(255 char),
        exchange varchar2(255 char),
        extension varchar2(255 char),
        lineNumber varchar2(255 char),
        value varchar2(255 char),
        account_id number(19,0),
        person_id number(19,0),
        type_id number(19,0),
        vendor_id number(19,0),
        primary key (id, REV)
    );

    create table Pick (
        id number(19,0) not null,
        apdOrderNumber varchar2(255 char),
        customerPoNumber varchar2(255 char),
        inventoryId varchar2(255 char),
        isShipped number(1,0),
        lineReference varchar2(255 char),
        orderNumber varchar2(255 char),
        qtyBo number(19,2),
        qtyToPick number(19,2),
        shipperId varchar2(255 char) not null,
        version number(10,0) not null,
        lineItem_id number(19,0),
        primary key (id)
    );

    create table PoNumber (
        id number(19,0) not null,
        expirationDate date,
        startDate date,
        value varchar2(255 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        primary key (id)
    );

    create table PoNumberType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table PoNumberType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table PoNumber_CustomerOrder (
        poNumbers_id number(19,0) not null,
        orders_id number(19,0) not null,
        primary key (poNumbers_id, orders_id)
    );

    create table PricingType (
        id number(19,0) not null,
        name varchar2(255 char),
        parameterRegEx varchar2(255 char) not null,
        useCeiling number(1,0) not null,
        version number(10,0) not null,
        primary key (id)
    );

    create table PricingType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        parameterRegEx varchar2(255 char),
        useCeiling number(1,0),
        primary key (id, REV)
    );

    create table Process (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ProcessConfiguration (
        id number(19,0) not null,
        version number(10,0) not null,
        account_id number(19,0),
        credential_id number(19,0),
        process_id number(19,0) not null,
        primary key (id),
        unique (id, credential_id),
        unique (id, account_id)
    );

    create table ProcessVariable (
        id number(19,0) not null,
        name varchar2(255 char) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        processConfiguration_id number(19,0),
        primary key (id),
        unique (processConfiguration_id, name)
    );

    create table PunchoutSession (
        id number(19,0) not null,
        browserFormPostTargetFrame varchar2(255 char),
        browserFormPostUrl varchar2(255 char),
        buyerCookie varchar2(255 char),
        endTime timestamp,
        initTime timestamp,
        operation varchar2(10) default 'create' not null,
        sessionToken varchar2(255 char),
        systemUserLoginName varchar2(255 char),
        version number(10,0) not null,
        accntCredUser_id number(19,0),
        customerOrder_id number(19,0),
        primary key (id)
    );

    create table REVINFO (
        REV number(10,0) not null,
        revisionDate timestamp,
        REVTSTMP number(19,0),
        username varchar2(255 char),
        primary key (REV)
    );

    create table ReturnOrder (
        id number(19,0) not null,
        closeDate timestamp,
        createdDate timestamp,
        raNumber varchar2(255 char),
        reconciledDate timestamp,
        status varchar2(255 char),
        version number(10,0) not null,
        order_id number(19,0),
        restockingFee_id number(19,0),
        primary key (id)
    );

    create table Role (
        id number(19,0) not null,
        isActive number(1,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table RoleXUser (
        id number(19,0) not null,
        expirationDate date,
        startDate date,
        version number(10,0) not null,
        role_id number(19,0),
        user_id number(19,0),
        primary key (id)
    );

    create table RoleXUser_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        expirationDate date,
        startDate date,
        role_id number(19,0),
        primary key (id, REV)
    );

    create table Role_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        isActive number(1,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table Role_Permission (
        roles_id number(19,0) not null,
        permissions_id number(19,0) not null,
        primary key (roles_id, permissions_id)
    );

    create table Role_Permission_AUD (
        REV number(10,0) not null,
        roles_id number(19,0) not null,
        permissions_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, roles_id, permissions_id)
    );

    create table SERVICE_LOG (
        id number(19,0) not null,
        contactEmail varchar2(255 char),
        contactName varchar2(255 char),
        contactNumber varchar2(255 char),
        department varchar2(255 char),
        followUpComplete number(1,0),
        openedDate timestamp not null,
        resolution varchar2(255 char),
        resolvedDate timestamp,
        status varchar2(255 char),
        ticketNumber varchar2(255 char),
        updateTimestamp timestamp,
        version number(10,0) not null,
        serviceDetails varchar2(3000 char),
        customerOrder_id number(19,0),
        account_id number(19,0),
        apdCsrRep_id number(19,0),
        contactMethod_id number(19,0),
        serviceReason_id number(19,0),
        primary key (id)
    );

    create table ServiceReason (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ShipManifest (
        id number(19,0) not null,
        bulkCartonIndicator varchar2(255 char),
        carrierCode varchar2(255 char),
        cartonBarcode varchar2(255 char),
        cartonID varchar2(255 char) unique,
        cartonWeight varchar2(255 char),
        customerAddress1 varchar2(255 char),
        customerAddress2 varchar2(255 char),
        customerBarCode varchar2(255 char),
        customerCity varchar2(255 char),
        customerName varchar2(255 char),
        customerNumber varchar2(255 char),
        customerPostalCode varchar2(255 char),
        customerPurchaseOrderNumber varchar2(255 char),
        customerReferenceData varchar2(255 char),
        customerRouteData varchar2(255 char),
        customerRouteDescription varchar2(255 char),
        customerState varchar2(255 char),
        dateProcessed date,
        dateTimeSent date,
        dealerInformation1 varchar2(255 char),
        dealerInformation2 varchar2(255 char),
        dealerInformation3 varchar2(255 char),
        dealerInformation4 varchar2(255 char),
        dealerInformation5 varchar2(255 char),
        dealerInformation6 varchar2(255 char),
        deliveryDate date,
        deliveryMethodCode varchar2(255 char),
        deliveryTypeCode varchar2(255 char),
        documentID varchar2(255 char),
        endConsumerAddress1 varchar2(255 char),
        endConsumerAddress2 varchar2(255 char),
        endConsumerAddress3 varchar2(255 char),
        endConsumerCity varchar2(255 char),
        endConsumerName varchar2(255 char),
        endConsumerPostalCode varchar2(255 char),
        endConsumerPurchaseOrderData varchar2(255 char),
        endConsumerState varchar2(255 char),
        facilityAbbreviation varchar2(255 char),
        facilityAddress1 varchar2(255 char),
        facilityAddress2 varchar2(255 char),
        facilityCity varchar2(255 char),
        facilityName varchar2(255 char),
        facilityNumber varchar2(255 char),
        facilityPostalCode varchar2(255 char),
        facilityState varchar2(255 char),
        fillFacilityNumber varchar2(255 char),
        hazardousMaterialIndicator varchar2(255 char),
        isManifested number(1,0),
        itemPrefix varchar2(255 char),
        itemStock varchar2(255 char),
        manifestDate date,
        numBoxes number(19,2),
        orderNumber varchar2(255 char),
        primaryOrderNumber varchar2(255 char),
        scanDateTime date,
        selectionTypeCode varchar2(255 char),
        shipTruckCode varchar2(255 char),
        shipTruckDockId varchar2(255 char),
        shippingInformation1 varchar2(255 char),
        shippingInformation2 varchar2(255 char),
        shippingInformation3 varchar2(255 char),
        shippingInformation4 varchar2(255 char),
        shippingInformation5 varchar2(255 char),
        shippingInformation6 varchar2(255 char),
        specialInstructions1 varchar2(255 char),
        specialInstructions2 varchar2(255 char),
        subOrderNumber varchar2(255 char),
        tradingPartnerID varchar2(255 char),
        truckCodeDescription varchar2(255 char),
        type varchar2(255 char),
        version number(10,0) not null,
        customerOrder_id number(19,0),
        primary key (id)
    );

    create table ShipManifest_Pick (
        ShipManifest_id number(19,0) not null,
        picks_id number(19,0) not null,
        primary key (ShipManifest_id, picks_id),
        unique (picks_id)
    );

    create table Shipment (
        id number(19,0) not null,
        deliveredTime timestamp,
        shipTime timestamp,
        subTotalCost number(19,2) not null,
        subTotalPrice number(19,2) not null,
        toCharge number(1,0),
        totalFreightCost number(19,2) not null,
        totalFreightPrice number(19,2) not null,
        totalShippingCost number(19,2) not null,
        totalShippingPrice number(19,2) not null,
        totalTaxPrice number(19,2) not null,
        trackingNumber varchar2(255 char),
        version number(10,0) not null,
        apdInvoice_id number(19,0),
        customerServiceAddress_id number(19,0),
        order_id number(19,0),
        parentShipment_id number(19,0),
        address_id number(19,0),
        shippingPartner_id number(19,0),
        primary key (id)
    );

    create table ShippingPartner (
        id number(19,0) not null,
        duns varchar2(255 char) not null unique,
        name varchar2(255 char) not null unique,
        scacCode varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table ShippingPartner_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        duns varchar2(255 char),
        name varchar2(255 char),
        scacCode varchar2(255 char),
        primary key (id, REV)
    );

    create table Sku (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        item_id number(19,0),
        type_id number(19,0),
        primary key (id)
    );

    create table SkuType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table SkuType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table SkuType_HierarchyNode (
        skuTypes_id number(19,0) not null,
        hierarchyNodes_id number(19,0) not null,
        primary key (skuTypes_id, hierarchyNodes_id)
    );

    create table SpecificationDescription (
        id number(19,0) not null,
        description varchar2(255 char),
        version number(10,0) not null,
        descriptionType_id number(19,0),
        specificationproperty_id number(19,0),
        primary key (id)
    );

    create table SpecificationProperty (
        id number(19,0) not null,
        name varchar2(255 char),
        sequence number(10,0),
        value varchar2(400 char),
        version number(10,0) not null,
        itemspecification_id number(19,0),
        primary key (id)
    );

    create table SyncItemResult (
        id number(19,0) not null,
        correlationId varchar2(255 char),
        resultApdSku varchar2(255 char),
        resultVendor varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table SyncItemResultSummary (
        id number(19,0) not null,
        correlationId varchar2(255 char),
        created number(10,0) not null,
        discontinued number(10,0) not null,
        errors number(10,0) not null,
        failures number(10,0) not null,
        updated number(10,0) not null,
        version number(10,0) not null,
        warnings number(10,0) not null,
        zeroPrice number(10,0) not null,
        primary key (id)
    );

    create table SystemComment (
        id number(19,0) not null,
        commentDate timestamp,
        content varchar2(4000 char),
        fileUrl varchar2(255 char),
        madeBySystem number(1,0),
        version number(10,0) not null,
        shipment_id number(19,0),
        user_id number(19,0),
        order_id number(19,0),
        primary key (id),
        check ((MADEBYSYSTEM = 1) OR (USER_ID IS NOT NULL))
    );

    create table SystemUser (
        id number(19,0) not null,
        changePassword number(1,0) not null,
        concurrentAccess number(1,0) not null,
        creationDate date,
        creditCardAttempts number(10,0),
        isActive number(1,0) not null,
        lastCreditCardAttemptDate timestamp,
        lastLoginAttemptDate date,
        lastLoginDate date,
        login varchar2(255 char) not null unique,
        loginAttempts number(10,0),
        password varchar2(255 char),
        passwordCreationDate date,
        previousCreditCardAttemptDate timestamp,
        salt varchar2(255 char),
        status varchar2(255 char),
        version number(10,0) not null,
        paymentInformation_id number(19,0),
        person_id number(19,0) not null,
        primary key (id),
        unique (person_id)
    );

    create table SystemUser_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        changePassword number(1,0),
        concurrentAccess number(1,0),
        creationDate date,
        creditCardAttempts number(10,0),
        isActive number(1,0),
        lastCreditCardAttemptDate timestamp,
        lastLoginAttemptDate date,
        lastLoginDate date,
        login varchar2(255 char),
        loginAttempts number(10,0),
        password varchar2(255 char),
        passwordCreationDate date,
        previousCreditCardAttemptDate timestamp,
        salt varchar2(255 char),
        status varchar2(255 char),
        paymentInformation_id number(19,0),
        person_id number(19,0),
        primary key (id, REV)
    );

    create table SystemUser_Account (
        users_id number(19,0) not null,
        accounts_id number(19,0) not null,
        primary key (users_id, accounts_id)
    );

    create table SystemUser_Account_AUD (
        REV number(10,0) not null,
        users_id number(19,0) not null,
        accounts_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, users_id, accounts_id)
    );

    create table SystemUser_Bulletin (
        SystemUser_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        primary key (SystemUser_id, bulletins_id)
    );

    create table SystemUser_Bulletin_AUD (
        REV number(10,0) not null,
        SystemUser_id number(19,0) not null,
        bulletins_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, SystemUser_id, bulletins_id)
    );

    create table SystemUser_RoleXUser_AUD (
        REV number(10,0) not null,
        user_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, user_id, id)
    );

    create table TaxRate (
        id number(19,0) not null,
        city varchar2(255 char),
        cityLocalSales number(19,2),
        cityLocalUse number(19,2),
        citySales number(19,2),
        cityUse number(19,2),
        combinedSales number(19,2),
        combinedUse number(19,2),
        county varchar2(255 char),
        countyDefault varchar2(255 char),
        countyFips varchar2(255 char),
        countyLocalSales number(19,2),
        countyLocalUse number(19,2),
        countySales number(19,2),
        countyUse number(19,2),
        effectiveDate date,
        expireDate date,
        generalDefault varchar2(255 char),
        geocode varchar2(255 char),
        locationInCity varchar2(255 char),
        state varchar2(255 char),
        stateSales number(19,2),
        stateUse number(19,2),
        version number(10,0) not null,
        zip varchar2(255 char),
        primary key (id)
    );

    create table TaxRateLog (
        id number(19,0) not null,
        action varchar2(255 char),
        identifier varchar2(255 char),
        status varchar2(255 char),
        timestamp timestamp,
        type varchar2(255 char),
        version number(10,0) not null,
        zip varchar2(255 char),
        primary key (id)
    );

    create table TaxType (
        id number(19,0) not null,
        name varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table UnitOfMeasure (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table UnitOfMeasure_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table UserBRMSRecord (
        id number(19,0) not null,
        active number(1,0),
        processId number(19,0),
        sessionId number(10,0),
        snapshot varchar2(255 char),
        version number(10,0) not null,
        userRequest_id number(19,0),
        primary key (id)
    );

    create table UserRequest (
        id number(19,0) not null,
        token varchar2(255 char),
        version number(10,0) not null,
        primary key (id)
    );

    create table ValidationError (
        id number(19,0) not null,
        description varchar2(255 char),
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table Vendor (
        id number(19,0) not null,
        activationDate date,
        communicationDestination varchar2(255 char) not null,
        communicationMessageType varchar2(255 char) not null,
        controlAccountNumber varchar2(255 char),
        expirationDate date,
        name varchar2(255 char) not null unique,
        remitTo varchar2(255 char),
        solomonVendorId varchar2(255 char),
        version number(10,0) not null,
        contact_id number(19,0),
        primary key (id)
    );

    create table VendorPropertyType (
        id number(19,0) not null,
        name varchar2(255 char) not null unique,
        version number(10,0) not null,
        primary key (id)
    );

    create table VendorPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        name varchar2(255 char),
        primary key (id, REV)
    );

    create table VendorXVendorPropertyType (
        id number(19,0) not null,
        value varchar2(255 char),
        version number(10,0) not null,
        type_id number(19,0) not null,
        vendor_id number(19,0),
        primary key (id)
    );

    create table VendorXVendorPropertyType_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        value varchar2(255 char),
        type_id number(19,0),
        primary key (id, REV)
    );

    create table Vendor_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        activationDate date,
        communicationDestination varchar2(255 char),
        communicationMessageType varchar2(255 char),
        controlAccountNumber varchar2(255 char),
        expirationDate date,
        name varchar2(255 char),
        remitTo varchar2(255 char),
        solomonVendorId varchar2(255 char),
        contact_id number(19,0),
        primary key (id, REV)
    );

    create table Vendor_Address_AUD (
        REV number(10,0) not null,
        vendor_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, vendor_id, id)
    );

    create table Vendor_CommunicationMethod (
        vendors_id number(19,0) not null,
        communicationMethods_id number(19,0) not null,
        primary key (vendors_id, communicationMethods_id)
    );

    create table Vendor_CommunicationMethod_AUD (
        REV number(10,0) not null,
        vendors_id number(19,0) not null,
        communicationMethods_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, vendors_id, communicationMethods_id)
    );

    create table Vendor_PhoneNumber_AUD (
        REV number(10,0) not null,
        vendor_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, vendor_id, id)
    );

    create table WorkflowLog (
        id number(19,0) not null,
        changeDate timestamp not null,
        csrUser varchar2(255 char) not null,
        target varchar2(255 char),
        taskEvent varchar2(255 char) not null,
        taskId number(19,0) not null,
        version number(10,0) not null,
        primary key (id)
    );

    create table ZipPlusFour (
        id number(19,0) not null,
        countyFips varchar2(255 char),
        hi varchar2(255 char),
        low varchar2(255 char),
        version number(10,0) not null,
        zip varchar2(255 char),
        primary key (id)
    );

    create table acxacpt_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        value varchar2(255 char),
        type_id number(19,0),
        primary key (id, REV)
    );

    create table axapt_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        value varchar2(255 char),
        address_id number(19,0),
        type_id number(19,0),
        primary key (id, REV)
    );

    create table axcxu_aud (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        active number(1,0),
        punchLevel varchar2(255 char),
        usAccountNumber varchar2(255 char),
        account_id number(19,0),
        address_id number(19,0),
        approverUser_id number(19,0),
        cred_id number(19,0),
        paymentInformation_id number(19,0),
        user_id number(19,0),
        primary key (id, REV)
    );

    create table cctranlog_faildetails (
        cctranlog_id number(19,0) not null,
        failuredetails varchar2(255 char)
    );

    create table cctranlog_vendorList (
        cctranlog_id number(19,0) not null,
        vendorList varchar2(255 char)
    );

    create table comp_favlist_catxitem (
        items_id number(19,0) not null,
        favoriteslist_id number(19,0) not null,
        primary key (items_id, favoriteslist_id)
    );

    create table customerOutFromCred (
        Credential_id number(19,0) not null,
        customerOutoingFromCred_id number(19,0) not null,
        primary key (Credential_id, customerOutoingFromCred_id),
        unique (customerOutoingFromCred_id)
    );

    create table customerOutFromCred_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        customerOutoingFromCred_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, customerOutoingFromCred_id)
    );

    create table customerOutSenderCred (
        Credential_id number(19,0) not null,
        customerOutoingSenderCred_id number(19,0) not null,
        primary key (Credential_id, customerOutoingSenderCred_id),
        unique (customerOutoingSenderCred_id)
    );

    create table customerOutSenderCred_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        customerOutoingSenderCred_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, customerOutoingSenderCred_id)
    );

    create table customerOutToCred (
        Credential_id number(19,0) not null,
        customerOutoingToCred_id number(19,0) not null,
        primary key (Credential_id, customerOutoingToCred_id),
        unique (customerOutoingToCred_id)
    );

    create table customerOutToCred_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        customerOutoingToCred_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, customerOutoingToCred_id)
    );

    create table cxcpt_AUD (
        REV number(10,0) not null,
        cred_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, cred_id, id)
    );

    create table cxcxpt_AUD (
        id number(19,0) not null,
        REV number(10,0) not null,
        REVTYPE number(3,0),
        parameter varchar2(255 char),
        catalog_id number(19,0),
        type_id number(19,0),
        primary key (id, REV)
    );

    create table favoriteslist_catalogxitem (
        favoriteslist_id number(19,0) not null,
        items_id number(19,0) not null,
        primary key (favoriteslist_id, items_id)
    );

    create table from_credential_table (
        CxmlConfiguration_id number(19,0) not null,
        fromCredentials_id number(19,0) not null,
        primary key (CxmlConfiguration_id, fromCredentials_id),
        unique (fromCredentials_id)
    );

    create table from_credential_table_AUD (
        REV number(10,0) not null,
        CxmlConfiguration_id number(19,0) not null,
        fromCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, CxmlConfiguration_id, fromCredentials_id)
    );

    create table liXShipment_vendorComments (
        liXShipment_id number(19,0) not null,
        vendorComment varchar2(255 char)
    );

    create table lineItem_vendorComments (
        lineItem_id number(19,0) not null,
        vendorComment varchar2(255 char)
    );

    create table outFromCredentialTable (
        Credential_id number(19,0) not null,
        outgoingFromCredentials_id number(19,0) not null,
        primary key (Credential_id, outgoingFromCredentials_id),
        unique (outgoingFromCredentials_id)
    );

    create table outFromCredentialTable_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        outgoingFromCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, outgoingFromCredentials_id)
    );

    create table outSenderCredentialTable (
        Credential_id number(19,0) not null,
        outgoingSenderCredentials_id number(19,0) not null,
        primary key (Credential_id, outgoingSenderCredentials_id),
        unique (outgoingSenderCredentials_id)
    );

    create table outSenderCredentialTable_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        outgoingSenderCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, outgoingSenderCredentials_id)
    );

    create table outToCredentialTable (
        Credential_id number(19,0) not null,
        outgoingToCredentials_id number(19,0) not null,
        primary key (Credential_id, outgoingToCredentials_id),
        unique (outgoingToCredentials_id)
    );

    create table outToCredentialTable_AUD (
        REV number(10,0) not null,
        Credential_id number(19,0) not null,
        outgoingToCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, Credential_id, outgoingToCredentials_id)
    );

    create table sender_credential_table (
        CxmlConfiguration_id number(19,0) not null,
        senderCredentials_id number(19,0) not null,
        primary key (CxmlConfiguration_id, senderCredentials_id),
        unique (senderCredentials_id)
    );

    create table sender_credential_table_AUD (
        REV number(10,0) not null,
        CxmlConfiguration_id number(19,0) not null,
        senderCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, CxmlConfiguration_id, senderCredentials_id)
    );

    create table sir_errors (
        sir_id number(19,0) not null,
        errors varchar2(4000 char)
    );

    create table sir_warnings (
        sir_id number(19,0) not null,
        warnings varchar2(4000 char)
    );

    create table to_credential_table (
        CxmlConfiguration_id number(19,0) not null,
        toCredentials_id number(19,0) not null,
        primary key (CxmlConfiguration_id, toCredentials_id),
        unique (toCredentials_id)
    );

    create table to_credential_table_AUD (
        REV number(10,0) not null,
        CxmlConfiguration_id number(19,0) not null,
        toCredentials_id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, CxmlConfiguration_id, toCredentials_id)
    );

    create table vxvxvpt_AUD (
        REV number(10,0) not null,
        vendor_id number(19,0) not null,
        id number(19,0) not null,
        REVTYPE number(3,0),
        primary key (REV, vendor_id, id)
    );

    alter table ACCXCREDXUSER_CCENTER 
        add constraint FKCE242C77FFE1BA34 
        foreign key (ACCOUNTCREDENTIALUSERS_ID) 
        references AccountXCredentialXUser;

    alter table ACCXCREDXUSER_CCENTER 
        add constraint FKCE242C77179FBA96 
        foreign key (ALLOWEDCOSTCENTERS_ID) 
        references CostCenter;

    alter table ACCXCREDXUSER_CCENTER_AUD 
        add constraint FK8EE746C875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table AccXCredXUser_Dept 
        add constraint FK44393CA6540E04AE 
        foreign key (departments_id) 
        references Department;

    alter table AccXCredXUser_Dept 
        add constraint FK44393CA661F26857 
        foreign key (AccountXCredentialXUser_id) 
        references AccountXCredentialXUser;

    alter table AccXCredXUser_Dept_AUD 
        add constraint FKD68AA77775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table AccXCredXUser_Desktops 
        add constraint FKDB3499B861F26857 
        foreign key (AccountXCredentialXUser_id) 
        references AccountXCredentialXUser;

    alter table AccXCredXUser_Desktops 
        add constraint FKDB3499B8377FE6DC 
        foreign key (desktops_id) 
        references Desktop;

    alter table AccXCredXUser_Desktops_AUD 
        add constraint FK978D2B8975D1432 
        foreign key (REV) 
        references REVINFO;

    create index ACCOUNT_NAME_IDX on Account (name);

    alter table Account 
        add constraint FK1D0C220D48B9DF01 
        foreign key (parentAccount_id) 
        references Account;

    alter table Account 
        add constraint FK1D0C220D7ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation;

    alter table Account 
        add constraint FK1D0C220D46F7094 
        foreign key (primaryContact_id) 
        references Person;

    alter table Account 
        add constraint FK1D0C220DD0B0DA57 
        foreign key (cxmlConfiguration_id) 
        references CxmlConfiguration;

    alter table Account 
        add constraint FK1D0C220DE5957139 
        foreign key (rootAccount_id) 
        references Account;

    alter table AccountBRMSRecord 
        add constraint FK9D4C7BB4185A22D7 
        foreign key (account_id) 
        references Account;

    alter table AccountGroup_AUD 
        add constraint FK1846C94375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table AccountPropertyType_AUD 
        add constraint FKEF0CD92D75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table AccountXAccountPropertyType 
        add constraint FKF7E20391185A22D7 
        foreign key (account_id) 
        references Account;

    alter table AccountXAccountPropertyType 
        add constraint FKF7E20391460EDA79 
        foreign key (type_id) 
        references AccountPropertyType;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE1DC8230E7 
        foreign key (approverUser_id) 
        references SystemUser;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE17ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE199EEF977 
        foreign key (address_id) 
        references Address;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE14E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE145A534A6 
        foreign key (cred_id) 
        references Credential;

    alter table AccountXCredentialXUser 
        add constraint FK34428EE1185A22D7 
        foreign key (account_id) 
        references Account;

    alter table Account_AUD 
        add constraint FK970E815E75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Account_AccountGroup 
        add constraint FK2FA3E964236EB562 
        foreign key (groups_id) 
        references AccountGroup;

    alter table Account_AccountGroup 
        add constraint FK2FA3E9647BEA6A7E 
        foreign key (accounts_id) 
        references Account;

    alter table Account_AccountGroup_AUD 
        add constraint FK7F34C53575D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Account_Bulletin 
        add constraint FKB24DABB9367C0B98 
        foreign key (bulletins_id) 
        references Bulletin;

    alter table Account_Bulletin 
        add constraint FKB24DABB9185A22D7 
        foreign key (Account_id) 
        references Account;

    alter table Account_Bulletin_AUD 
        add constraint FK37DB550A75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Account_IpAddress_AUD 
        add constraint FK5006654C75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Account_PoNumber 
        add constraint FKF0898C3A185A22D7 
        foreign key (Account_id) 
        references Account;

    alter table Account_PoNumber 
        add constraint FKF0898C3ACE415F34 
        foreign key (blanketPos_id) 
        references PoNumber;

    alter table ActionType_AUD 
        add constraint FKA116A88175D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Address 
        add constraint FK1ED033D4D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table Address 
        add constraint FK1ED033D4E4B11BD 
        foreign key (miscShipTo_id) 
        references MiscShipTo;

    alter table Address 
        add constraint FK1ED033D43E66BDBD 
        foreign key (person_id) 
        references Person;

    alter table Address 
        add constraint FK1ED033D4185A22D7 
        foreign key (account_id) 
        references Account;

    alter table Address 
        add constraint FK1ED033D46C357B4B 
        foreign key (type_id) 
        references AddressType;

    alter table AddressPropertyType_AUD 
        add constraint FKE49A7CF475D1432 
        foreign key (REV) 
        references REVINFO;

    create index ADDRESS_TYPE_IDX on AddressType (name);

    alter table AddressType_AUD 
        add constraint FK952813FF75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table AddressXAddressPropertyType 
        add constraint FKF9E70B1F99EEF977 
        foreign key (address_id) 
        references Address;

    alter table AddressXAddressPropertyType 
        add constraint FKF9E70B1FDD1B79C0 
        foreign key (type_id) 
        references AddressPropertyType;

    alter table Address_AUD 
        add constraint FK115657A575D1432 
        foreign key (REV) 
        references REVINFO;

    create index po_apdpolog_idx on ApdPoLog (apdPo_id);

    create index createdate_apdpolog_idx on ApdPoLog (creationDate);

    alter table ApdPoLog 
        add constraint FK3425E7D05ECADFF1 
        foreign key (apdPo_id) 
        references PoNumber;

    alter table AttachmentType_AUD 
        add constraint FKFD3C366E75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Bulletin_AUD 
        add constraint FK51BD901875D1432 
        foreign key (REV) 
        references REVINFO;

    create index CANCEL_CANCELDATE_IDX on CanceledQuantity (cancelDate);

    create index CANCELQUANTITY_LINEITEM_IDX on CanceledQuantity (lineItem_id);

    alter table CanceledQuantity 
        add constraint FK3BD1E864B7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    alter table CardInformation_AUD 
        add constraint FK69BCAFAD75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Cart 
        add constraint FK1FEF409ADA0556 
        foreign key (userCredential_id) 
        references AccountXCredentialXUser;

    alter table CartItem 
        add constraint FK4393E73779C529D 
        foreign key (cart_id) 
        references Cart;

    alter table CartItem 
        add constraint FK4393E73D4DB665D 
        foreign key (catalogXItem_id) 
        references CatalogXItem;

    alter table CashoutPageXField 
        add constraint FKE48C344CA5D99E16 
        foreign key (mapping_id) 
        references MessageMapping;

    alter table CashoutPageXField 
        add constraint FKE48C344C71C12AF2 
        foreign key (page_id) 
        references CashoutPage;

    alter table CashoutPageXField 
        add constraint FKE48C344C44537EF7 
        foreign key (field_id) 
        references Field;

    alter table CashoutPageXField_AUD 
        add constraint FK9FAD5C1D75D1432 
        foreign key (REV) 
        references REVINFO;

    create index CATALOG_NAME_IDX on Catalog (name);

    alter table Catalog 
        add constraint FK8457F7F9A6F76EBE 
        foreign key (replacement_id) 
        references Catalog;

    alter table Catalog 
        add constraint FK8457F7F9D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table Catalog 
        add constraint FK8457F7F9DE9C8386 
        foreign key (customer_id) 
        references Account;

    alter table Catalog 
        add constraint FK8457F7F92AEE8D26 
        foreign key (parent_id) 
        references Catalog;

    create index cs_corrId on CatalogCsv (correlationId);

    create index cu_corrId on CatalogUpload (correlationId);

    alter table CatalogXCategoryXPricingType 
        add constraint FK7BA786C5B7BD2710 
        foreign key (category_id) 
        references ItemCategory;

    alter table CatalogXCategoryXPricingType 
        add constraint FK7BA786C51B2C461D 
        foreign key (type_id) 
        references PricingType;

    alter table CatalogXCategoryXPricingType 
        add constraint FK7BA786C531B94B57 
        foreign key (catalog_id) 
        references Catalog;

    alter table CatalogXItem 
        add constraint FKC4EC1F3291FB7B97 
        foreign key (pricingType_id) 
        references PricingType;

    alter table CatalogXItem 
        add constraint FKC4EC1F32D4B669BD 
        foreign key (item_id) 
        references Item;

    alter table CatalogXItem 
        add constraint FKC4EC1F32EA944B5 
        foreign key (customerSku_id) 
        references Sku;

    alter table CatalogXItem 
        add constraint FKC4EC1F3220C000E2 
        foreign key (substituteItem_id) 
        references CatalogXItem;

    alter table CatalogXItem 
        add constraint FKC4EC1F3231B94B57 
        foreign key (catalog_id) 
        references Catalog;

    create index CATALOGXITEM_ID_IX on CatalogXItemXItemPropertyType (catalogxitem_id);

    alter table CatalogXItemXItemPropertyType 
        add constraint FK846B38E8D4DB665D 
        foreign key (catalogxitem_id) 
        references CatalogXItem;

    alter table CatalogXItemXItemPropertyType 
        add constraint FK846B38E878D89205 
        foreign key (type_id) 
        references ItemPropertyType;

    alter table Catalog_AUD 
        add constraint FK2765814A75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table CategoryMarkup 
        add constraint FK16ACD4E6B7BD2710 
        foreign key (category_id) 
        references ItemCategory;

    alter table CategoryMarkup 
        add constraint FK16ACD4E631B94B57 
        foreign key (catalog_id) 
        references Catalog;

    alter table CategoryMarkup_AUD 
        add constraint FK98F81FB775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table ClassificationCode 
        add constraint FKEB05AF133EED1BDD 
        foreign key (codeType_id) 
        references CodeType;

    alter table ClassificationNote 
        add constraint FKEB0AB1187B0231BD 
        foreign key (noteType_id) 
        references NoteType;

    alter table ClassificationNote 
        add constraint FKEB0AB11815D522DD 
        foreign key (ItemClassification_id) 
        references ItemClassification;

    alter table CommunicationMethod_AUD 
        add constraint FK570CD68875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table ContactLogComment 
        add constraint FKE766069B29E27F47 
        foreign key (apdCsrRep_id) 
        references SystemUser;

    alter table ContactLogComment 
        add constraint FKE766069B797AADDD 
        foreign key (servicelog_id) 
        references SERVICE_LOG;

    alter table ContactLogComment 
        add constraint FKE766069BE92BED1D 
        foreign key (issuelog_id) 
        references ISSUE_LOG;

    alter table ContactMethod_AUD 
        add constraint FKFC333E5275D1432 
        foreign key (REV) 
        references REVINFO;

    alter table CostCenter 
        add constraint FK537676A269C00797 
        foreign key (period_id) 
        references PeriodType;

    alter table CostCenter_AUD 
        add constraint FK3062837375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential 
        add constraint FK4E15C477B4B76DDE 
        foreign key (defaultShipFromAddress_id) 
        references Address;

    alter table Credential 
        add constraint FK4E15C477BACFEB7 
        foreign key (skuType_id) 
        references SkuType;

    alter table Credential 
        add constraint FK4E15C4777ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation;

    alter table Credential 
        add constraint FK4E15C4774E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table Credential 
        add constraint FK4E15C477E5957139 
        foreign key (rootAccount_id) 
        references Account;

    alter table Credential 
        add constraint FK4E15C47762FD6737 
        foreign key (communicationMethod_id) 
        references CommunicationMethod;

    alter table Credential 
        add constraint FK4E15C47731B94B57 
        foreign key (catalog_id) 
        references Catalog;

    alter table Credential 
        add constraint FK4E15C477DD089F57 
        foreign key (cashoutPage_id) 
        references CashoutPage;

    alter table CredentialPropertyType_AUD 
        add constraint FKD2E8679775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table CredentialXVendor 
        add constraint FK17F98769D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table CredentialXVendor 
        add constraint FK17F98769AFD1BCFD 
        foreign key (credential_id) 
        references Credential;

    alter table CredentialXVendor_AUD 
        add constraint FKE7D1D8BA75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_AUD 
        add constraint FKC4CDEC875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_Bulletin 
        add constraint FK3B63208F367C0B98 
        foreign key (bulletins_id) 
        references Bulletin;

    alter table Credential_Bulletin 
        add constraint FK3B63208FAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_Bulletin_AUD 
        add constraint FK9CDE6EE075D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_CostCenter 
        add constraint FKAD50676AAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_CostCenter 
        add constraint FKAD50676A309BEEE 
        foreign key (costCenters_id) 
        references CostCenter;

    alter table Credential_CostCenter_AUD 
        add constraint FK36C6D03B75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_Department 
        add constraint FK33A103A540E04AE 
        foreign key (departments_id) 
        references Department;

    alter table Credential_Department 
        add constraint FK33A103AAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_Department_AUD 
        add constraint FKBB0F910B75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_LimitApproval 
        add constraint FK20BF5536AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_LimitApproval 
        add constraint FK20BF5536C2092797 
        foreign key (limitApproval_id) 
        references LimitApproval;

    alter table Credential_LimitApproval_AUD 
        add constraint FK5A31F80775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_Permission 
        add constraint FKB1D192B7AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_Permission 
        add constraint FKB1D192B7BC8D8328 
        foreign key (permissions_id) 
        references Permission;

    alter table Credential_Permission_AUD 
        add constraint FK74F78D0875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_PoNumber 
        add constraint FK799F0110AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_PoNumber 
        add constraint FK799F0110CE415F34 
        foreign key (blanketPos_id) 
        references PoNumber;

    alter table Credential_PropertyType 
        add constraint FK2165219745A534A6 
        foreign key (cred_id) 
        references Credential;

    alter table Credential_PropertyType 
        add constraint FK21652197B66C2309 
        foreign key (type_id) 
        references CredentialPropertyType;

    alter table Credential_PropertyType_AUD 
        add constraint FKC068ABE875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Credential_Role 
        add constraint FK7D571B5E46C1C336 
        foreign key (roles_id) 
        references Role;

    alter table Credential_Role 
        add constraint FK7D571B5EAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table Credential_Role_AUD 
        add constraint FK7BAA6A2F75D1432 
        foreign key (REV) 
        references REVINFO;

    create index uploadeditem_field_idx on CsvUploadedItemField (uploadeditem_id);

    alter table CsvUploadedItemField 
        add constraint FK707855E14A59795D 
        foreign key (uploadeditem_id) 
        references CsvUploadedItem;

    create index CC_ITEM_IX on CustomerCost (item_id);

    alter table CustomerCost 
        add constraint FK3F6DD8EBD4B669BD 
        foreign key (item_id) 
        references Item;

    alter table CustomerCost 
        add constraint FK3F6DD8EBDE9C8386 
        foreign key (customer_id) 
        references Account;

    create index CUSTORDER_ORDERDATE_IDX on CustomerOrder (orderDate);

    alter table CustomerOrder 
        add constraint FKAEF781F05F7CC357 
        foreign key (cardInformation_id) 
        references CardInformation;

    alter table CustomerOrder 
        add constraint FKAEF781F08B76AAE3 
        foreign key (parentOrder_id) 
        references CustomerOrder;

    alter table CustomerOrder 
        add constraint FKAEF781F07ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation;

    alter table CustomerOrder 
        add constraint FKAEF781F07175679D 
        foreign key (assignedCostCenter_id) 
        references ACCXCREDXUSER_CCENTER;

    alter table CustomerOrder 
        add constraint FKAEF781F099EEF977 
        foreign key (address_id) 
        references Address;

    alter table CustomerOrder 
        add constraint FKAEF781F04E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table CustomerOrder 
        add constraint FKAEF781F0D6EA6B25 
        foreign key (status_id) 
        references OrderStatus;

    alter table CustomerOrder 
        add constraint FKAEF781F0AFD1BCFD 
        foreign key (credential_id) 
        references Credential;

    alter table CustomerOrder 
        add constraint FKAEF781F0185A22D7 
        foreign key (account_id) 
        references Account;

    alter table CustomerOrder 
        add constraint FKAEF781F04C0A74E5 
        foreign key (type_id) 
        references OrderType;

    alter table CustomerOrder 
        add constraint FKAEF781F0EB7E507 
        foreign key (customerStatus_id) 
        references OrderStatus;

    alter table CustomerOrder 
        add constraint FKAEF781F0971F1FDF 
        foreign key (paymentStatus_id) 
        references OrderStatus;

    alter table CustomerOrderRequest 
        add constraint FK5C66FA7F26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table CustomerOrder_TaxType 
        add constraint FKD872C6168E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    alter table CustomerOrder_TaxType 
        add constraint FKD872C6162B5202 
        foreign key (type_id) 
        references TaxType;

    alter table CustomerSpecificIconUrl 
        add constraint FKD7C87CC6F5826A90 
        foreign key (propertyType_id) 
        references ItemPropertyType;

    alter table CustomerSpecificIconUrl 
        add constraint FKD7C87CC68CC90C70 
        foreign key (classificationType_id) 
        references ItemClassificationType;

    alter table CustomerSpecificIconUrl 
        add constraint FKD7C87CC6185A22D7 
        foreign key (account_id) 
        references Account;

    alter table CxmlConfiguration_AUD 
        add constraint FK5BD0393375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table CxmlCredential_AUD 
        add constraint FK41C768BC75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Department_AUD 
        add constraint FKB4AB444375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Desktop_AUD 
        add constraint FKAFA7F7CD75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table FavoritesList 
        add constraint FKFC5FBB7590DF4B4F 
        foreign key (axcxu_id) 
        references AccountXCredentialXUser;

    alter table FavoritesList 
        add constraint FKFC5FBB7531B94B57 
        foreign key (catalog_id) 
        references Catalog;

    alter table Field 
        add constraint FK40BB0DA532E0A57 
        foreign key (fieldType_id) 
        references FieldType;

    alter table FieldOptions 
        add constraint FKC6EDA46444537EF7 
        foreign key (field_id) 
        references Field;

    alter table HierarchyNode 
        add constraint FK6128C1B7ECE7DD24 
        foreign key (parent_id) 
        references HierarchyNode;

    create index INVOICE_RETURNORDER_IDX on INVOICE (returnOrder_id);

    create index INVOICE_CREATEDATE_IDX on INVOICE (createdDate);

    create index INVOICE_SHIPMENT_IDX on INVOICE (shipment_id);

    alter table INVOICE 
        add constraint FK9FA1CF0DBBACDEDD 
        foreign key (shipment_id) 
        references Shipment;

    alter table INVOICE 
        add constraint FK9FA1CF0D96D53A97 
        foreign key (returnOrder_id) 
        references ReturnOrder;

    alter table INVOICE 
        add constraint FK9FA1CF0D26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table INVOICE_LineItem 
        add constraint FK7FC5EA39753382D7 
        foreign key (INVOICE_id) 
        references INVOICE;

    alter table INVOICE_LineItem 
        add constraint FK7FC5EA3943253E12 
        foreign key (actualItems_id) 
        references LineItem;

    alter table INVOICE_SPECIAL 
        add constraint FK8B80E567753382D7 
        foreign key (INVOICE_ID) 
        references INVOICE;

    alter table ISSUE_LOG 
        add constraint FKB12F029E49053676 
        foreign key (reporter_id) 
        references SystemUser;

    alter table ISSUE_LOG 
        add constraint FKB12F029E26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table ITEMCLASSIFICATION_ITEM 
        add constraint FK3ECCA9D95EC40F1C 
        foreign key (itemclassifications_id) 
        references ItemClassification;

    alter table ITEMCLASSIFICATION_ITEM 
        add constraint FK3ECCA9D99CD121F0 
        foreign key (items_id) 
        references Item;

    alter table ITEM_ITEM 
        add constraint FK2836ACBFD4B669BD 
        foreign key (ITEM_ID) 
        references Item;

    alter table ITEM_ITEM 
        add constraint FK2836ACBFBAC8DCDB 
        foreign key (SIMILARITEMS_ID) 
        references Item;

    alter table IpAddress 
        add constraint FKD8D77CAD185A22D7 
        foreign key (account_id) 
        references Account;

    alter table IpAddress 
        add constraint FKD8D77CAD45A534A6 
        foreign key (cred_id) 
        references Credential;

    alter table IpAddress_AUD 
        add constraint FK726B8BFE75D1432 
        foreign key (REV) 
        references REVINFO;

    create index ITEM_ITEM_IX on Item (replacement_id);

    alter table Item 
        add constraint FK22EF3397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure;

    alter table Item 
        add constraint FK22EF334E03425E 
        foreign key (replacement_id) 
        references Item;

    alter table Item 
        add constraint FK22EF33C66C90F7 
        foreign key (hierarchyNode_id) 
        references HierarchyNode;

    alter table Item 
        add constraint FK22EF33D72635FD 
        foreign key (manufacturer_id) 
        references Manufacturer;

    alter table Item 
        add constraint FK22EF334B90121D 
        foreign key (itemCategory_id) 
        references ItemCategory;

    alter table Item 
        add constraint FK22EF337ADFF69F 
        foreign key (vendorCatalog_id) 
        references Catalog;

    alter table Item 
        add constraint FK22EF33E061875A 
        foreign key (abilityOneSubstitute_id) 
        references Item;

    alter table ItemCategory 
        add constraint FK32432D51D77B2BE4 
        foreign key (parent_id) 
        references ItemCategory;

    alter table ItemClassification 
        add constraint FKB69BD9F950060531 
        foreign key (ItemClassType_id) 
        references ItemClassificationType;

    alter table ItemClassification 
        add constraint FKB69BD9F9AC87CC77 
        foreign key (itemClassificationImage_id) 
        references ItemClassificationImage;

    alter table ItemClassification 
        add constraint FKB69BD9F939A07C9D 
        foreign key (classificationCode_id) 
        references ClassificationCode;

    alter table ItemClassificationImage 
        add constraint FK2B0232E2EF79F2B7 
        foreign key (imageType_id) 
        references ImageType;

    create index ID_ITEM_IX on ItemDimension (item_id);

    alter table ItemDimension 
        add constraint FK7789077397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure;

    alter table ItemDimension 
        add constraint FK7789077388D89C57 
        foreign key (dimensionType_id) 
        references DimensionType;

    alter table ItemDimension 
        add constraint FK77890773D4B669BD 
        foreign key (item_id) 
        references Item;

    create index IH_ITEM_IX on ItemHistory (item_id);

    alter table ItemHistory 
        add constraint FK7AD69DE1D4B669BD 
        foreign key (item_id) 
        references Item;

    create index II_ITEM_IX on ItemIdentifier (item_id);

    alter table ItemIdentifier 
        add constraint FK6E543CD4B669BD 
        foreign key (item_id) 
        references Item;

    alter table ItemIdentifier 
        add constraint FK6E543C9AF89DD 
        foreign key (itemIdentifierType_id) 
        references ItemIdentifierType;

    create index ITEMIMAGEID_IX on ItemImage (item_id);

    alter table ItemImage 
        add constraint FKF69951E8D4B669BD 
        foreign key (item_id) 
        references Item;

    alter table ItemImage 
        add constraint FKF69951E8EF79F2B7 
        foreign key (imageType_id) 
        references ImageType;

    create index IP_ITEM_IX on ItemPackaging (item_id);

    alter table ItemPackaging 
        add constraint FK2436F190B8531B37 
        foreign key (packagingType_id) 
        references PackagingType;

    alter table ItemPackaging 
        add constraint FK2436F190293DD25D 
        foreign key (packagingDimension_id) 
        references PackagingDimension;

    alter table ItemPackaging 
        add constraint FK2436F190D4B669BD 
        foreign key (item_id) 
        references Item;

    alter table ItemPropertyType_AUD 
        add constraint FKE6209A5375D1432 
        foreign key (REV) 
        references REVINFO;

    create index ITEM_SELLPT_ID_IX on ItemSellingPoint (item_id);

    alter table ItemSellingPoint 
        add constraint FK97A3D6F3D4B669BD 
        foreign key (item_id) 
        references Item;

    create index is_item_id on ItemSpecification (item_id);

    alter table ItemSpecification 
        add constraint FK53120C90D4B669BD 
        foreign key (item_id) 
        references Item;

    create index ITEM_ID_IX on ItemXItemPropertyType (item_id);

    create index TYPE_ID_IX on ItemXItemPropertyType (type_id);

    alter table ItemXItemPropertyType 
        add constraint FKA8404B07D4B669BD 
        foreign key (item_id) 
        references Item;

    alter table ItemXItemPropertyType 
        add constraint FKA8404B0778D89205 
        foreign key (type_id) 
        references ItemPropertyType;

    alter table Item_Matchbook 
        add constraint FK2D669002D4B669BD 
        foreign key (Item_id) 
        references Item;

    alter table Item_Matchbook 
        add constraint FK2D669002AB387BF7 
        foreign key (matchbook_id) 
        references Matchbook;

    alter table LimitApproval 
        add constraint FK7C45AFE90DF4B4F 
        foreign key (AXCXU_ID) 
        references AccountXCredentialXUser;

    alter table LimitApproval_AUD 
        add constraint FK882ED9CF75D1432 
        foreign key (REV) 
        references REVINFO;

    create index LINEITEM_LINENUMBER_IDX on LineItem (lineNumber);

    create index LI_ITEM_IX on LineItem (item_id);

    create index LINEITEM_CUSTORDER_IDX on LineItem (order_id);

    alter table LineItem 
        add constraint FK4AAEE94797CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure;

    alter table LineItem 
        add constraint FK4AAEE947D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table LineItem 
        add constraint FK4AAEE947D0A084BD 
        foreign key (customerExpectedUoM_id) 
        references UnitOfMeasure;

    alter table LineItem 
        add constraint FK4AAEE947D4B669BD 
        foreign key (item_id) 
        references Item;

    alter table LineItem 
        add constraint FK4AAEE947EA944B5 
        foreign key (customerSku_id) 
        references Sku;

    alter table LineItem 
        add constraint FK4AAEE947B7BD2710 
        foreign key (category_id) 
        references ItemCategory;

    alter table LineItem 
        add constraint FK4AAEE9475B7981A4 
        foreign key (status_id) 
        references LineItemStatus;

    alter table LineItem 
        add constraint FK4AAEE9478E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    alter table LineItem 
        add constraint FK4AAEE947E0A3A08C 
        foreign key (expected_id) 
        references LineItem;

    alter table LineItem 
        add constraint FK4AAEE9471BAE365E 
        foreign key (paymentStatus_id) 
        references LineItemStatus;

    alter table LineItemHistory 
        add constraint FK5154E54D5C204E7D 
        foreign key (actionType_id) 
        references ActionType;

    alter table LineItemHistory 
        add constraint FK5154E54DB7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    alter table LineItemHistory 
        add constraint FK5154E54DB27BA87D 
        foreign key (lineItemStatus_id) 
        references LineItemStatus;

    create index RETURNORDER_LIXR_IDX on LineItemXReturn (RETURNORDER_ID);

    create index LIXR_LINEITEM_IDX on LineItemXReturn (lineItem_id);

    alter table LineItemXReturn 
        add constraint FK77B3F0815A2BC1B6 
        foreign key (INVOICE_ID) 
        references INVOICE;

    alter table LineItemXReturn 
        add constraint FK77B3F08196D53A97 
        foreign key (RETURNORDER_ID) 
        references ReturnOrder;

    alter table LineItemXReturn 
        add constraint FK77B3F081B7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    create index LIXS_LINEITEM_IDX on LineItemXShipment (lineItem_id);

    create index LIXS_SHIPMENT_IDX on LineItemXShipment (shipments_id);

    alter table LineItemXShipment 
        add constraint FK4DFF94ABB7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    alter table LineItemXShipment 
        add constraint FK4DFF94ABFDA949FE 
        foreign key (shipments_id) 
        references Shipment;

    create index LIXSXTT_LINEITEMXSHIPMENT_IDX on LineItemXShipment_TaxType (lineItemXShipment_id);

    create index LIXSXTT_LINEITEMXRETURN_IDX on LineItemXShipment_TaxType (lineItemXReturn_id);

    alter table LineItemXShipment_TaxType 
        add constraint FKD45EDD1A80FC937 
        foreign key (lineItemXReturn_id) 
        references LineItemXReturn;

    alter table LineItemXShipment_TaxType 
        add constraint FKD45EDD1CB300DB7 
        foreign key (lineItemXShipment_id) 
        references LineItemXShipment;

    alter table LineItemXShipment_TaxType 
        add constraint FKD45EDD12B5202 
        foreign key (type_id) 
        references TaxType;

    alter table Manufacturer_AUD 
        add constraint FKD246454275D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Matchbook 
        add constraint FK9C11CA0ED72635FD 
        foreign key (manufacturer_id) 
        references Manufacturer;

    alter table Matchbook_AUD 
        add constraint FK4B0E40DF75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table MessageMetadata 
        add constraint FKD964A0363E63769E 
        foreign key (objectsWrapper_id) 
        references MessageObjectsWrapper;

    alter table NotificationLimit 
        add constraint FK8C01ED70AFD1BCFD 
        foreign key (credential_id) 
        references Credential;

    alter table NotificationProperties 
        add constraint FK2EC8B49E8E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    alter table NotificationProperties 
        add constraint FK2EC8B49E45A534A6 
        foreign key (cred_id) 
        references Credential;

    alter table NotificationProperties 
        add constraint FK2EC8B49EF363A403 
        foreign key (acc_id) 
        references Account;

    create index CCTRANLOG_RETURN_IDX on ORDER_LOG (creditcard_returnorder_id);

    create index CCTRANLOG_SUCCESS_IDX on ORDER_LOG (transactionSuccess);

    create index ORDERLOG_UPDATETIME_IDX on ORDER_LOG (updateTimestamp);

    create index ORDERLOG_CUSTORDER_IDX on ORDER_LOG (customerOrder_id);

    alter table ORDER_LOG 
        add constraint FK8ED49E93BBACDEDD 
        foreign key (shipment_id) 
        references Shipment;

    alter table ORDER_LOG 
        add constraint FK8ED49E93753382D7 
        foreign key (invoice_id) 
        references INVOICE;

    alter table ORDER_LOG 
        add constraint FK8ED49E93454B440D 
        foreign key (creditcard_returnorder_id) 
        references ReturnOrder;

    alter table ORDER_LOG 
        add constraint FK8ED49E9396D53A97 
        foreign key (returnOrder_id) 
        references ReturnOrder;

    alter table ORDER_LOG 
        add constraint FK8ED49E93B7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    alter table ORDER_LOG 
        add constraint FK8ED49E9326CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table ORDER_LOG 
        add constraint FK8ED49E93A910D157 
        foreign key (messageMetadata_id) 
        references MessageMetadata;

    alter table ORDER_LOG 
        add constraint FK8ED49E93B27BA87D 
        foreign key (lineItemStatus_id) 
        references LineItemStatus;

    alter table ORDER_LOG 
        add constraint FK8ED49E9384C13B97 
        foreign key (orderStatus_id) 
        references OrderStatus;

    alter table ORDER_LOG_LineItemXShipment 
        add constraint FK8321BDFF255C549A 
        foreign key (lineItemXShipments_id) 
        references LineItemXShipment;

    alter table ORDER_LOG_LineItemXShipment 
        add constraint FK8321BDFF3EA8F9 
        foreign key (ORDER_LOG_id) 
        references ORDER_LOG;

    alter table OrderAttachment 
        add constraint FKD35F2AF1797AADDD 
        foreign key (servicelog_id) 
        references SERVICE_LOG;

    alter table OrderAttachment 
        add constraint FKD35F2AF14E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table OrderAttachment 
        add constraint FKD35F2AF18E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    create index orderbrmsrecord_entryid on OrderBRMSRecord (entryId);

    create index ORDERLOG_CUSTORDER_IDX on OrderProcessLookup (customerOrder_id);

    alter table OrderProcessLookup 
        add constraint FK2C2B183B26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table OrderStatus 
        add constraint FKC492B8C0BAA7D4D9 
        foreign key (overrideStatus_id) 
        references OrderStatus;

    alter table PackagingDimension 
        add constraint FKF5AEB54397CE9D17 
        foreign key (unitOfMeasure_id) 
        references UnitOfMeasure;

    alter table PackagingDimension 
        add constraint FKF5AEB54388D89C57 
        foreign key (dimensionType_id) 
        references DimensionType;

    create index PARTNERIDXDELVLOC_LOCATION on PartnerIdXDeliveryLocation (deliveryLocation);

    create index PARTNERIDXDELVLOC_PARTNERID on PartnerIdXDeliveryLocation (partnerId);

    alter table PartnerIdXDeliveryLocation_AUD 
        add constraint FKB9660D8F75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PasswordResetEntry 
        add constraint FKC0DA017E4E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table PasswordResetEntry_AUD 
        add constraint FKD90D404F75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PaymentInformation 
        add constraint FKCC5E4766C3EB5397 
        foreign key (paymentType_id) 
        references PaymentType;

    alter table PaymentInformation 
        add constraint FKCC5E476613E27D12 
        foreign key (contact_id) 
        references Person;

    alter table PaymentInformation 
        add constraint FKCC5E47663C027E32 
        foreign key (billingAddress_id) 
        references Address;

    alter table PaymentInformation 
        add constraint FKCC5E4766171AC637 
        foreign key (invoiceSendMethod_id) 
        references InvoiceSendMethod;

    alter table PaymentInformation 
        add constraint FKCC5E4766A6E063E3 
        foreign key (card_id) 
        references CardInformation;

    alter table PaymentInformation_AUD 
        add constraint FK5BAC523775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PaymentType_AUD 
        add constraint FK6C86781175D1432 
        foreign key (REV) 
        references REVINFO;

    create index PWCOMMAND_EXECUTED_IDX on PendingWorkflowCommand (executed);

    create index PWCOMMAND_EXPIRES_IDX on PendingWorkflowCommand (expires);

    alter table PeriodType_AUD 
        add constraint FKE00BE50C75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PermissionXUser 
        add constraint FK637BAFB4BCDF29FD 
        foreign key (permission_id) 
        references Permission;

    alter table PermissionXUser 
        add constraint FK637BAFB44E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table PermissionXUser_AUD 
        add constraint FKDA20E38575D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Permission_AUD 
        add constraint FK6E93404075D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Person_AUD 
        add constraint FK9F49F2C675D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PhoneNumber 
        add constraint FK1C4E6237D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table PhoneNumber 
        add constraint FK1C4E62373E66BDBD 
        foreign key (person_id) 
        references Person;

    alter table PhoneNumber 
        add constraint FK1C4E6237185A22D7 
        foreign key (account_id) 
        references Account;

    alter table PhoneNumber 
        add constraint FK1C4E6237D68B89AE 
        foreign key (type_id) 
        references PhoneNumberType;

    alter table PhoneNumberType_AUD 
        add constraint FKD70F2F6275D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PhoneNumber_AUD 
        add constraint FKAD809C8875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Pick 
        add constraint FK25F441B7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    create index PO_NUM_IDX on PoNumber (value);

    alter table PoNumber 
        add constraint FKF1EF7E484D174BA5 
        foreign key (type_id) 
        references PoNumberType;

    alter table PoNumberType_AUD 
        add constraint FKD05FAA7375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table PoNumber_CustomerOrder 
        add constraint FK27F6343924BC7FA 
        foreign key (poNumbers_id) 
        references PoNumber;

    alter table PoNumber_CustomerOrder 
        add constraint FK27F63439A806D542 
        foreign key (orders_id) 
        references CustomerOrder;

    alter table PricingType_AUD 
        add constraint FK33925D175D1432 
        foreign key (REV) 
        references REVINFO;

    alter table ProcessConfiguration 
        add constraint FKDAF5E6E7AFD1BCFD 
        foreign key (credential_id) 
        references Credential;

    alter table ProcessConfiguration 
        add constraint FKDAF5E6E7185A22D7 
        foreign key (account_id) 
        references Account;

    alter table ProcessConfiguration 
        add constraint FKDAF5E6E7951FA397 
        foreign key (process_id) 
        references Process;

    alter table ProcessVariable 
        add constraint FKDD2D2DEB4F9C731D 
        foreign key (processConfiguration_id) 
        references ProcessConfiguration;

    alter table PunchoutSession 
        add constraint FKDC86EEF626CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table PunchoutSession 
        add constraint FKDC86EEF6A9E5B5D8 
        foreign key (accntCredUser_id) 
        references AccountXCredentialXUser;

    create index RETURN_CREATEDATE_IDX on ReturnOrder (createdDate);

    create index RETURNORDER_CUSTORDER_IDX on ReturnOrder (order_id);

    create index RETURN_RECONCILEDATE_IDX on ReturnOrder (reconciledDate);

    create index RETURN_CLOSEDATE_IDX on ReturnOrder (closeDate);

    alter table ReturnOrder 
        add constraint FK9F0BFDE8E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    alter table ReturnOrder 
        add constraint FK9F0BFDE6A6D81FD 
        foreign key (restockingFee_id) 
        references LineItem;

    alter table RoleXUser 
        add constraint FK8B08ED6DA8A94ADD 
        foreign key (role_id) 
        references Role;

    alter table RoleXUser 
        add constraint FK8B08ED6D4E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table RoleXUser_AUD 
        add constraint FKD8F69CBE75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Role_AUD 
        add constraint FKF3FAE76775D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Role_Permission 
        add constraint FKF8A5693846C1C336 
        foreign key (roles_id) 
        references Role;

    alter table Role_Permission 
        add constraint FKF8A56938BC8D8328 
        foreign key (permissions_id) 
        references Permission;

    alter table Role_Permission_AUD 
        add constraint FKE90A3B0975D1432 
        foreign key (REV) 
        references REVINFO;

    alter table SERVICE_LOG 
        add constraint FKD76567A29E27F47 
        foreign key (apdCsrRep_id) 
        references SystemUser;

    alter table SERVICE_LOG 
        add constraint FKD76567AA9054CB7 
        foreign key (contactMethod_id) 
        references ContactMethod;

    alter table SERVICE_LOG 
        add constraint FKD76567A26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table SERVICE_LOG 
        add constraint FKD76567A185A22D7 
        foreign key (account_id) 
        references Account;

    alter table SERVICE_LOG 
        add constraint FKD76567A4E2C1B7 
        foreign key (serviceReason_id) 
        references ServiceReason;

    alter table ShipManifest 
        add constraint FK1734C4AB26CAE17 
        foreign key (customerOrder_id) 
        references CustomerOrder;

    alter table ShipManifest_Pick 
        add constraint FKE8531B559EBD9BDD 
        foreign key (ShipManifest_id) 
        references ShipManifest;

    alter table ShipManifest_Pick 
        add constraint FKE8531B552A91F50C 
        foreign key (picks_id) 
        references Pick;

    create index SHIPMENT_CUSTORDER_IDX on Shipment (order_id);

    create index SHIPMENT_SHIPTIME_IDX on Shipment (shipTime);

    alter table Shipment 
        add constraint FKE513D5BA9744A7F3 
        foreign key (parentShipment_id) 
        references Shipment;

    alter table Shipment 
        add constraint FKE513D5BA99EEF977 
        foreign key (address_id) 
        references Address;

    alter table Shipment 
        add constraint FKE513D5BA5BE726AC 
        foreign key (apdInvoice_id) 
        references INVOICE;

    alter table Shipment 
        add constraint FKE513D5BA8E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    alter table Shipment 
        add constraint FKE513D5BA495D8297 
        foreign key (shippingPartner_id) 
        references ShippingPartner;

    alter table Shipment 
        add constraint FKE513D5BAD81419CE 
        foreign key (customerServiceAddress_id) 
        references Address;

    alter table ShippingPartner_AUD 
        add constraint FK75423FEB75D1432 
        foreign key (REV) 
        references REVINFO;

    create index SKU_ITEM_ID_IX on Sku (item_id);

    create index SKU_TYPE_ID_IX on Sku (type_id);

    alter table Sku 
        add constraint FK144FDD4B669BD 
        foreign key (item_id) 
        references Item;

    alter table Sku 
        add constraint FK144FDDC2B4674 
        foreign key (type_id) 
        references SkuType;

    alter table SkuType_AUD 
        add constraint FKFFADEC2875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table SkuType_HierarchyNode 
        add constraint FKDB1B914FBA0B4EF2 
        foreign key (hierarchyNodes_id) 
        references HierarchyNode;

    alter table SkuType_HierarchyNode 
        add constraint FKDB1B914FA777D0F2 
        foreign key (skuTypes_id) 
        references SkuType;

    create index sd_spid_ix on SpecificationDescription (specificationproperty_id);

    alter table SpecificationDescription 
        add constraint FK6A555F19D189F457 
        foreign key (specificationproperty_id) 
        references SpecificationProperty;

    alter table SpecificationDescription 
        add constraint FK6A555F19352E4157 
        foreign key (descriptionType_id) 
        references DescriptionType;

    create index sp_is_id on SpecificationProperty (itemspecification_id);

    alter table SpecificationProperty 
        add constraint FK16701FD87DA5C397 
        foreign key (itemspecification_id) 
        references ItemSpecification;

    create index sir_corrId on SyncItemResult (correlationId);

    create index sirs_corrId on SyncItemResultSummary (correlationId);

    alter table SystemComment 
        add constraint FKE1344110BBACDEDD 
        foreign key (shipment_id) 
        references Shipment;

    alter table SystemComment 
        add constraint FKE13441104E9ECF4C 
        foreign key (user_id) 
        references SystemUser;

    alter table SystemComment 
        add constraint FKE13441108E1EC39 
        foreign key (order_id) 
        references CustomerOrder;

    create index LOGIN_INDEX on SystemUser (login);

    alter table SystemUser 
        add constraint FK9D23FEBA3E66BDBD 
        foreign key (person_id) 
        references Person;

    alter table SystemUser 
        add constraint FK9D23FEBA7ED1037D 
        foreign key (paymentInformation_id) 
        references PaymentInformation;

    alter table SystemUser_AUD 
        add constraint FK595E3F8B75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table SystemUser_Account 
        add constraint FK139A6D88478FA7EF 
        foreign key (users_id) 
        references SystemUser;

    alter table SystemUser_Account 
        add constraint FK139A6D887BEA6A7E 
        foreign key (accounts_id) 
        references Account;

    alter table SystemUser_Account_AUD 
        add constraint FKBE44975975D1432 
        foreign key (REV) 
        references REVINFO;

    alter table SystemUser_Bulletin 
        add constraint FK8EEEC1AC367C0B98 
        foreign key (bulletins_id) 
        references Bulletin;

    alter table SystemUser_Bulletin 
        add constraint FK8EEEC1AC64555A3D 
        foreign key (SystemUser_id) 
        references SystemUser;

    alter table SystemUser_Bulletin_AUD 
        add constraint FK114A397D75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table SystemUser_RoleXUser_AUD 
        add constraint FKAFF1FF975D1432 
        foreign key (REV) 
        references REVINFO;

    create index TAXLOG_TIME_IDX on TaxRateLog (timestamp);

    create index UOM_NAME_IDX on UnitOfMeasure (name);

    alter table UnitOfMeasure_AUD 
        add constraint FK8B1E7E9475D1432 
        foreign key (REV) 
        references REVINFO;

    alter table UserBRMSRecord 
        add constraint FKEA537C12D8D414D7 
        foreign key (userRequest_id) 
        references UserRequest;

    create index UR_TOKEN_IDX on UserRequest (token);

    alter table Vendor 
        add constraint FK9883916813E27D12 
        foreign key (contact_id) 
        references Person;

    alter table VendorPropertyType_AUD 
        add constraint FKF028328875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table VendorXVendorPropertyType 
        add constraint FK6F8A7827D50C94DD 
        foreign key (vendor_id) 
        references Vendor;

    alter table VendorXVendorPropertyType 
        add constraint FK6F8A7827FF9A87FA 
        foreign key (type_id) 
        references VendorPropertyType;

    alter table VendorXVendorPropertyType_AUD 
        add constraint FK37E03A7875D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Vendor_AUD 
        add constraint FKA038CB3975D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Vendor_Address_AUD 
        add constraint FKF42F6B4E75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Vendor_CommunicationMethod 
        add constraint FK884AF6E0F3928A32 
        foreign key (communicationMethods_id) 
        references CommunicationMethod;

    alter table Vendor_CommunicationMethod 
        add constraint FK884AF6E03F37A71A 
        foreign key (vendors_id) 
        references Vendor;

    alter table Vendor_CommunicationMethod_AUD 
        add constraint FKEA60B4B175D1432 
        foreign key (REV) 
        references REVINFO;

    alter table Vendor_PhoneNumber_AUD 
        add constraint FK90E5B3B175D1432 
        foreign key (REV) 
        references REVINFO;

    create index COUNTY_FIPS_IDX on ZipPlusFour (countyFips);

    create index ZIP_IDX on ZipPlusFour (zip);

    alter table acxacpt_AUD 
        add constraint FKFA2B070D75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table axapt_AUD 
        add constraint FKB065255F75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table axcxu_aud 
        add constraint FK27E6F47A75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table cctranlog_faildetails 
        add constraint FK362E01FEEA1EA233 
        foreign key (cctranlog_id) 
        references ORDER_LOG;

    alter table cctranlog_vendorList 
        add constraint FK65DD662CEA1EA233 
        foreign key (cctranlog_id) 
        references ORDER_LOG;

    alter table comp_favlist_catxitem 
        add constraint FK3BD333EBE4345037 
        foreign key (favoriteslist_id) 
        references FavoritesList;

    alter table comp_favlist_catxitem 
        add constraint FK3BD333EB34FADEEF 
        foreign key (items_id) 
        references CatalogXItem;

    alter table customerOutFromCred 
        add constraint FKFB1F76C8AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table customerOutFromCred 
        add constraint FKFB1F76C8440D5C8D 
        foreign key (customerOutoingFromCred_id) 
        references CxmlCredential;

    alter table customerOutFromCred_AUD 
        add constraint FKDDA3009975D1432 
        foreign key (REV) 
        references REVINFO;

    alter table customerOutSenderCred 
        add constraint FK875A4FD3F111D462 
        foreign key (customerOutoingSenderCred_id) 
        references CxmlCredential;

    alter table customerOutSenderCred 
        add constraint FK875A4FD3AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table customerOutSenderCred_AUD 
        add constraint FKAF645C2475D1432 
        foreign key (REV) 
        references REVINFO;

    alter table customerOutToCred 
        add constraint FKBB8919AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table customerOutToCred 
        add constraint FKBB8919CA6ABBDC 
        foreign key (customerOutoingToCred_id) 
        references CxmlCredential;

    alter table customerOutToCred_AUD 
        add constraint FKB75B826A75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table cxcpt_AUD 
        add constraint FK42BA79E375D1432 
        foreign key (REV) 
        references REVINFO;

    alter table cxcxpt_AUD 
        add constraint FKB62881FF75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table favoriteslist_catalogxitem 
        add constraint FK229D1F9CE4345037 
        foreign key (favoriteslist_id) 
        references FavoritesList;

    alter table favoriteslist_catalogxitem 
        add constraint FK229D1F9C34FADEEF 
        foreign key (items_id) 
        references CatalogXItem;

    alter table from_credential_table 
        add constraint FK5AAAFDDBD0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration;

    alter table from_credential_table 
        add constraint FK5AAAFDDBE0E054D6 
        foreign key (fromCredentials_id) 
        references CxmlCredential;

    alter table from_credential_table_AUD 
        add constraint FKEF1EC62C75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table liXShipment_vendorComments 
        add constraint FKAC720FA6A518092D 
        foreign key (liXShipment_id) 
        references LineItemXShipment;

    alter table lineItem_vendorComments 
        add constraint FKB4713574B7777FDD 
        foreign key (lineItem_id) 
        references LineItem;

    alter table outFromCredentialTable 
        add constraint FK524C227FE8A80542 
        foreign key (outgoingFromCredentials_id) 
        references CxmlCredential;

    alter table outFromCredentialTable 
        add constraint FK524C227FAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table outFromCredentialTable_AUD 
        add constraint FK3294F8D075D1432 
        foreign key (REV) 
        references REVINFO;

    alter table outSenderCredentialTable 
        add constraint FK85502C14F444CD8D 
        foreign key (outgoingSenderCredentials_id) 
        references CxmlCredential;

    alter table outSenderCredentialTable 
        add constraint FK85502C14AFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table outSenderCredentialTable_AUD 
        add constraint FKCAA02FE575D1432 
        foreign key (REV) 
        references REVINFO;

    alter table outToCredentialTable 
        add constraint FK42E3DC0EA73C6553 
        foreign key (outgoingToCredentials_id) 
        references CxmlCredential;

    alter table outToCredentialTable 
        add constraint FK42E3DC0EAFD1BCFD 
        foreign key (Credential_id) 
        references Credential;

    alter table outToCredentialTable_AUD 
        add constraint FK368752DF75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table sender_credential_table 
        add constraint FK49D502B0BFA78821 
        foreign key (senderCredentials_id) 
        references CxmlCredential;

    alter table sender_credential_table 
        add constraint FK49D502B0D0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration;

    alter table sender_credential_table_AUD 
        add constraint FK7D60588175D1432 
        foreign key (REV) 
        references REVINFO;

    alter table sir_errors 
        add constraint FK239AC02E8A0FF28C 
        foreign key (sir_id) 
        references SyncItemResult;

    alter table sir_warnings 
        add constraint FK7264705A8A0FF28C 
        foreign key (sir_id) 
        references SyncItemResult;

    alter table to_credential_table 
        add constraint FKBD1F632AD0B0DA57 
        foreign key (CxmlConfiguration_id) 
        references CxmlConfiguration;

    alter table to_credential_table 
        add constraint FKBD1F632ABEAB89E7 
        foreign key (toCredentials_id) 
        references CxmlCredential;

    alter table to_credential_table_AUD 
        add constraint FK8B31EBFB75D1432 
        foreign key (REV) 
        references REVINFO;

    alter table vxvxvpt_AUD 
        add constraint FK54BC884775D1432 
        foreign key (REV) 
        references REVINFO;

    create sequence hibernate_sequence;
