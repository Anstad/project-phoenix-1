package com.apd.phoenix.integration.ws.rest;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jbpm.task.Status;
import org.jbpm.task.query.TaskSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.rest.BaseRestEndpoint;
import com.apd.phoenix.service.workflow.UserTaskService;

@Path("/workflow/order")
@Stateless
public class WorkflowRestService extends BaseRestEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(WorkflowRestService.class);

    @Inject
    private CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    private UserTaskService userTaskService;

    @GET
    @Path("/approve/{user}/{token}")
    public Response approve(@PathParam("user") String user, @PathParam("token") String token,
            @QueryParam("action") String action, @QueryParam("id") String id) {
        String response;
        try {
            completeTask(user, token, true);
            response = "<html><body><p>The order has been approved and submitted for processing. Thank you.</p></body></html>";

        }
        catch (Exception e) {
            LOG.info("Error approving task", e);
            response = "<html><body><p>There was an error processing your request.</p></body></html>";
            return Response.status(500).entity(response).type(MediaType.TEXT_HTML).build();
        }
        return Response.ok(response, MediaType.TEXT_HTML).build();
    }

    @GET
    @Path("/deny/{user}/{token}")
    public Response deny(@PathParam("user") String user, @PathParam("token") String token,
            @QueryParam("action") String action, @QueryParam("id") String id) {
        String response;
        try {
            completeTask(user, token, false);
            response = "<html><body><p>The order has been denied and will be cancelled.</p></body></html>";

        }
        catch (Exception e) {
            LOG.info("Error denying task", e);
            response = "<html><body><p>There was an error processing your request.</p></body></html>";
            return Response.status(500).entity(response).type(MediaType.TEXT_HTML).build();
        }
        return Response.ok(response, MediaType.TEXT_HTML).build();
    }

    private void completeTask(String user, String token, boolean approve){
    	CustomerOrder customerOrder = customerOrderRequestBp.getCustomerOrderByToken(token);
        TaskSummary taskSummary = userTaskService.getApprovalTask(customerOrder, false);
        LOG.info("Approving order, via task: {} {}", customerOrder.getId(), taskSummary.getId());
        Map<String, Object> taskContent = userTaskService.getTaskContent(taskSummary.getId());
        Map<String, Object> params = new HashMap<>();
        params.put("approved", approve);
        
        //Only attempt to start the task if it can be started
        if (taskSummary.getStatus().equals(Status.Ready) || 
        	taskSummary.getStatus().equals(Status.Reserved)	) {
        	userTaskService.startTask(taskSummary.getId(), user);
        }
        
        userTaskService.completeTask(taskSummary.getId(), (long)taskContent.get("orderId"), user, params);
    }
}
