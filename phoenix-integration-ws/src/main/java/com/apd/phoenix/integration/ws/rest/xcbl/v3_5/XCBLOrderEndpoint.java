/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.integration.ws.rest.xcbl.v3_5;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.rest.BaseRestEndpoint;
import com.perfect.liteenvelope.MessageAcknowledgement;

@Stateless
@Path("/xcbl")
public class XCBLOrderEndpoint extends BaseRestEndpoint {

    protected static final Logger LOG = LoggerFactory.getLogger(XCBLOrderEndpoint.class);

    @Inject
    private OSNReceiverService osnReceiverService;

    /**
     * Send the XCBL Order document to the batch/integration service for further processing
     * @param order 
     *
     * @param entity the entity
     * @return the response
     */
    @POST
    @Consumes("text/xml")
    @Produces("application/xml")
    public Response receiveOrder(String order) {
        Response response = null;
        try {
            if (LOG.isDebugEnabled()) {
                LOG.debug("XCBLOrderEndpoint payload received: " + order);
            }
            osnReceiverService.processXCBLOrder(order);
            response = Response.ok().build();

        }
        catch (Exception e) {
            response = Response.serverError().build();
        }
        return response != null ? response : Response.serverError().build();
    }
}