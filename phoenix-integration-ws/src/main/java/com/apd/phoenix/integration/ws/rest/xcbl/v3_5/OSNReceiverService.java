package com.apd.phoenix.integration.ws.rest.xcbl.v3_5;

import com.perfect.liteenvelope.MessageAcknowledgement;

public interface OSNReceiverService {

    public MessageAcknowledgement processOSNOrder(String initialPayload);
    
    public void processXCBLOrder(String initialPayload);
}
