package com.apd.phoenix.web.cashout;

import java.util.List;
import javax.ejb.Stateless;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.business.OrderTypeBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.Department;
import com.apd.phoenix.service.model.Desktop;
import com.apd.phoenix.service.model.FieldOptions;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.OrderType;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.jpa.AssignedCostCenterDao;
import com.apd.phoenix.service.persistence.jpa.DepartmentDao;
import com.apd.phoenix.service.persistence.jpa.DesktopDao;
import com.apd.phoenix.service.persistence.jpa.FieldOptionsDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@Named
@Stateless
public class CashoutConverterBean {
    
    private static final Logger logger = LoggerFactory.getLogger(CashoutConverterBean.class);

    @Inject
    private FieldOptionsDao optionsDao;

    public Converter getOptionsConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return optionsDao.findById(Long.valueOf(value), FieldOptions.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((FieldOptions) value).getId());
            }
        };
    }

    @Inject
    private PoNumberDao poDao;

    public Converter getPoNumberConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return poDao.findById(Long.valueOf(value), PoNumber.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((PoNumber) value).getId());
            }
        };
    }

    @Inject
    private AddressBp addressBp;

    public Converter getAddressConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                try {
                    return addressBp.findById(Long.valueOf(value), Address.class);
                }
                catch (NumberFormatException e) {
                    return null;
                }
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Address) value).getId());
            }
        };
    }

    @Inject
    private AssignedCostCenterDao assignedCostCenterDao;

    public Converter getCostCenterConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return assignedCostCenterDao.findById(Long.valueOf(value), AssignedCostCenter.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }

                return String.valueOf(((AssignedCostCenter) value).getId());
            }
        };
    }

    @Inject
    private DepartmentDao departmentDao;

    public Converter getDepartmentConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (StringUtils.isBlank(value)) {
                    return null;
                }
                Department searchDepartment = new Department();
                searchDepartment.setName(value);
                List<Department> searchList = departmentDao.searchByExactExample(searchDepartment, 0, 0);
                if (searchList.isEmpty()) {
                    Department toReturn = new Department();
                    toReturn.setName(value);
                    return toReturn;
                }
                return searchList.get(0);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                if (value instanceof String) {
                    return (String) value;
                }
                return String.valueOf(((Department) value).getName());
            }
        };
    }

    @Inject
    private OrderTypeBp orderTypeBp;

    public Converter getOrderTypeConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return orderTypeBp.findById(Long.valueOf(value), OrderType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((OrderType) value).getId());
            }
        };
    }

    @Inject
    private DesktopDao desktopDao;

    public Converter getDesktopConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                if (StringUtils.isBlank(value)) {
                    return null;
                }
                Desktop searchDesktop = new Desktop();
                searchDesktop.setName(value);
                List<Desktop> searchList = desktopDao.searchByExactExample(searchDesktop, 0, 0);
                if (searchList.isEmpty()) {
                    Desktop toReturn = new Desktop();
                    toReturn.setName(value);
                    return toReturn;
                }
                return searchList.get(0);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null) {
                    return "";
                }
                if (value instanceof String) {
                    return (String) value;
                }
                return String.valueOf(((Desktop) value).getName());
            }
        };
    }    
    
    @Inject
    private VendorBp vendorBp;

    public Converter getVendorConverter() {
        
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                Vendor search = new Vendor();
                search.setName(value);
                try {
                    List<Vendor> result = vendorBp.searchByExactExample(search,0,1);
                    if (result != null && !result.isEmpty()) {
                        return result.get(0);
                    }
                } catch (Exception e) {
                    logger.error("Error finding Vendor in CashoutConverterBean", e);
                }
                return search;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null || !(value instanceof Vendor)) {
                    return "";
                } else {
                    Vendor v = (Vendor) value;
                    return v.getName() == null ? "" : v.getName();
                }
            }
            
        };
    }   
    
    @Inject
    private ItemCategoryBp itemCategoryBp;
   
    public Converter getItemCategoryConverter() {
        
        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                ItemCategory search = new ItemCategory();
                search.setName(value);
                try {
                    List<ItemCategory> result = itemCategoryBp.searchByExactExample(search,0,1);
                    if (result != null && !result.isEmpty()) {
                        return result.get(0);
                    }
                } catch (Exception e) {
                    logger.error("Error finding ItemCategory in CashoutConverterBean", e);                   
                }
                return search;
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {
                if (value == null || !(value instanceof ItemCategory)) {
                    return "";
                } else {
                    ItemCategory c = (ItemCategory) value;
                    return c.getName() == null ? "" : c.getName();
                }
            }
            
        };
    }

    public List<OrderType> getAllOrderTypes() {
        return orderTypeBp.findAll(OrderType.class, 0, 0);
    }
}
