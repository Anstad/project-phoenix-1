package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.RoleBp;
import com.apd.phoenix.service.model.Role;

@Named
@ConversationScoped
public class RoleSearchBean extends AbstractSearchBean<Role> implements Serializable {

    @Inject
    RoleBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name" };
        return toReturn;
    }

    @Override
    protected List<Role> search() {
        Role search = new Role();
        search.setName(this.getValues().get(0).getValue());
        return bp.searchByExample(search, 0, 0);
    }

}
