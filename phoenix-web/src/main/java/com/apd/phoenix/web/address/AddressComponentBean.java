package com.apd.phoenix.web.address;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.MiscShipTo;

/**
 * This backing bean is used for showing the MiscShipTo fields for an address. The boolean showAdditionalFields indicates 
 * whether the MiscShipTo fields should be displayed, and it uses reflection to iterate through the fields in MiscShipTo.
 */

@Named
@Stateful
@ConversationScoped
public class AddressComponentBean implements Serializable {

    private static final String BILLTO_TYPE = "BILLTO";

    private static final long serialVersionUID = 1805125814198055406L;

    @Inject
    private AddressBp addressBp;

    //indicates whether the MiscShipTo fields should be displayed
    private boolean showAdditionalFields = false;

    //a map from the name of the MiscShipTo field to the value entered in the web page
    private Map<String, String> valueMap;

    //the address whose MiscShipTo is being updated
    private Address address;

    private List<Address> addressList;

    public boolean isShowAdditionalFields() {
        return showAdditionalFields;
    }

    public void setShowAdditionalFields(boolean showAdditionalFields) {
        this.showAdditionalFields = showAdditionalFields;
    }

    /**
     * This method takes an address, and returns a list of fields on MiscShipTo. It uses reflection to iterate 
     * though the fields of MiscShipTo.
     * 
     * @param address
     * @return
     */
    public List<String> getAdditionalFields(Address address) {
        if (this.address != address) {
            this.address = address;
            this.valueMap = null;
            this.showAdditionalFields = false;
        }
        return this.getFieldsWithoutReset();
    }

    private List<String> getFieldsWithoutReset() {
		List<String> toReturn = new ArrayList<>();
		for (Field f : MiscShipTo.class.getDeclaredFields()) {
			toReturn.add(f.getName());
		}
		toReturn.remove("id");
		toReturn.remove("serialVersionUID");
		return toReturn;
	}

    /**
     * This method returns the valueMap variable on this bean, creating it from the MiscShipTo fields on 
     * the address if it doesn't already exist
     * 
     * @return
     */
    public Map<String, String> getValueMap() {
		if (this.valueMap == null) {
			//if the map is null, create a new one, populating it from the MiscShipTo fields on the address
			this.valueMap = new HashMap<>();

			if (address != null && address.getMiscShipTo() != null) {
				for (String key : this.getFieldsWithoutReset()) {
					String value = null;
		            try {
						Field field = MiscShipTo.class.getDeclaredField(key);
		                field.setAccessible(true);
		                value = (String) field.get(address.getMiscShipTo());
						if (!StringUtils.isBlank(value)) {
							this.valueMap.put(key, value);
						}
		            }
		            catch (Exception e) {
		                //do nothing
		            }
				}
			}
		}
		return this.valueMap;
	}

    /**
     * Takes an Address and a backing bean object, sets the MiscShipTo on the address, and calls the "addAddress" 
     * method on the bean.
     * 
     * @param address
     * @param bean
     */
    public void storeAddress(Address address, Object bean) {
		
		//first, if the address is a ShipTo address, stores the values onto the MiscShipTo
		if (address != null && address.getType() != null && address.getType().getName().equals("SHIPTO") && this.valueMap != null) {
			
			if (address.getMiscShipTo() == null) {
				address.setMiscShipTo(new MiscShipTo());
			}
			for (String key : this.valueMap.keySet()) {
				if (!StringUtils.isBlank(this.valueMap.get(key))) {
		            try {
						Field field = MiscShipTo.class.getDeclaredField(key);
		                field.setAccessible(true);
		                field.set(address.getMiscShipTo(), this.valueMap.get(key));
		            }
		            catch (Exception e) {
		                //do nothing
		            }
				}
			}
		}
		if (address != null && address.getType() != null && !address.getType().getName().equals("SHIPTO")) {
			address.setMiscShipTo(null);
		}
		
		//then, calls the "addAddress" method on the backing bean
		try {
			//TODO: this currently uses reflection, but there should be an interface for backing beans for entities 
			//that have sets of addresses being manipulated. That way we can have rigid typing.
			bean.getClass().getMethod("addAddress", (Class<?>[]) null).invoke(bean, (Class<?>[]) null);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			//do nothing
		}
	}

    public List<Address> setAndGetAddressList(List<Address> addressList) {
        this.addressList = addressList;
        return this.addressList;
    }

    public void copyBillTo(Address address) {
        for (Address addressInList : this.addressList) {
            if (addressInList.getType() != null && BILLTO_TYPE.equals(addressInList.getType().getName())) {
                addressBp.cloneEntity(addressInList, address);
                this.address = address;
                this.valueMap = null;
                this.showAdditionalFields = false;
                address.setType(null);
                return;
            }
        }
    }
}
