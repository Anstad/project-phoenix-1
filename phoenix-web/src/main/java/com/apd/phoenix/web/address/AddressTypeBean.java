package com.apd.phoenix.web.address;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.apd.phoenix.web.searchbeans.AddressTypeSearchBean;
import com.apd.phoenix.service.model.AddressType;
import com.apd.phoenix.service.persistence.jpa.AddressTypeDao;

/**
 * Backing bean for AddressType entities.
 * <p>
 * This class provides CRUD functionality for all AddressType entities. It focuses
 * purely on Java EE 6 standards (e.g. <tt>&#64;ConversationScoped</tt> for
 * state management, <tt>PersistenceContext</tt> for persistence,
 * <tt>CriteriaBuilder</tt> for searches) rather than introducing a CRUD framework or
 * custom base class.
 */

@Named
@Stateful
@ConversationScoped
public class AddressTypeBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /*
     * Support creating and retrieving AddressType entities
     */

    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.currentInstance.getId() == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Select Address Type to edit");
            message.setDetail("You need to select an Address Type to edit.");
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private AddressType currentInstance;

    public AddressType getAddressType() {
        return this.currentInstance;
    }

    @Inject
    private AddressTypeDao dao;

    @Inject
    private AddressTypeSearchBean searchBean;

    @Inject
    private Conversation conversation;

    public String create() {

        this.conversation.begin();
        return "addressTypeAdd?faces-redirect=true";
    }

    public void retrieve() {
        if (this.searchBean.getSelection() != null) {
            this.currentInstance = this.searchBean.getSelection();
            this.searchBean.setSelection(null);
        }

        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }

        if (this.conversation.isTransient()) {
            this.conversation.begin();
        }

        if (this.id == null) {
            this.currentInstance = this.example;
        }
        else {
            this.currentInstance = dao.findById(id, AddressType.class);
        }

    }

    /*
     * Support updating and deleting AddressType entities
     */

    public String update() {
        if (!validToUpdate()) {
            return null;
        }

        if (!this.conversation.isTransient()) {
            this.conversation.end();
        }
        try {

            if (currentInstance.getId() == null) {
                dao.create(this.currentInstance);
                return "addressTypeAdd?faces-redirect=true";
            }
            else {
                dao.update(this.currentInstance);
                return "addressTypeEdit?faces-redirect=true";
            }
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    public String delete() {
        this.conversation.end();
        try {
            dao.delete(this.currentInstance.getId(), AddressType.class);
            return "addressTypeEdit?faces-redirect=true";
        }
        catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), e.getMessage()));
            return null;
        }
    }

    /*
     * Support searching AddressType entities with pagination
     */

    private int page;
    private long count;
    private List<AddressType> pageItems;

    private AddressType example = new AddressType();

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return 10;
    }

    public AddressType getExample() {
        return this.example;
    }

    public void setExample(AddressType example) {
        this.example = example;
    }

    public void search() {
        this.page = 0;
    }

    public void paginate() {
        this.pageItems = dao.searchByExample(this.example, getPage() * getPageSize(), getPageSize());

        this.count = dao.resultQuantity(this.example);
    }

    public List<AddressType> getPageItems() {
        return this.pageItems;
    }

    public long getCount() {
        return this.count;
    }

    /*
     * Support listing and POSTing back AddressType entities (e.g. from inside an
     * HtmlSelectOneMenu)
     */

    public List<AddressType> getAll() {

        return dao.findAll(AddressType.class, 0, 0);
    }

    public List<AddressType> searchByExample(AddressType search) {
        return dao.searchByExample(search, 0, 0);
    }

    public List<AddressType> searchByString(String s) {
        AddressType search = new AddressType();
        search.setName(s);
        return searchByExample(search);
    }

    @Resource
    private SessionContext sessionContext;

    public Converter getConverter() {

        final AddressTypeBean ejbProxy = this.sessionContext.getBusinessObject(AddressTypeBean.class);

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return dao.findById(Long.valueOf(value), AddressType.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((AddressType) value).getId());
            }
        };
    }

    /*
     * Support adding children to bidirectional, one-to-many tables
     */

    private AddressType add = new AddressType();

    public AddressType getAdd() {
        return this.add;
    }

    public AddressType getAdded() {
        AddressType added = this.add;
        this.add = new AddressType();
        return added;
    }

    private boolean validToUpdate() {
        AddressType searchAddressType = new AddressType();
        searchAddressType.setName(this.currentInstance.getName());
        List<AddressType> searchList = this.dao.searchByExactExample(searchAddressType, 0, 0);
        if (searchList.size() != 0 && !searchList.get(0).getId().equals(this.currentInstance.getId())) {
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("Name already exists");
            message.setDetail("An address type already exists with the specified name.");
            FacesContext.getCurrentInstance().addMessage(null, message);
            return false;
        }
        return true;
    }
}