package com.apd.phoenix.web.searchbeans;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.utility.CsvExportService;
import com.apd.phoenix.service.utility.FileDownloader;
import com.apd.phoenix.core.PageNumberDisplay;

/**
 * This abstract class is implemented by any concrete bean that will be used for searching. See the 
 * wiki page for an explanation of how to implement the search functionality.
 * 
 * @author RHC
 *
 * @param <T> The type of entity that will be searched for
 */
public abstract class AbstractSearchBean<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractSearchBean.class);

    private boolean searched = false;

    private boolean requireSelection = false;

    private List<Value> values;

    private List<T> results = new ArrayList<T>();

    private T selection;

    private T currentResult;

    private Map<String, String> currentRowMap;

    private PageNumberDisplay numberDisplay;

    @Inject
    protected CsvExportService resultsCsv;

    /**
     * This method returns an Array of Strings which will be used as labels for the search fields. For 
     * a simple search, this will just return a single-element array, with the element being "Name".
     * 
     * @return The Array of Strings labelling the search fields
     */
    public abstract String[] getCriteria();

    /**
     * This method obtains the search results.
     * 
     * @return A List of results
     */
    protected abstract List<T> search();

    /**
     * Returns the List of Values set by the user in the text fields. The size of the List will be equal 
     * to the size of the Array returned by the "getCriteria()" method, and the ordering of the List will 
     * map to the ordering of the Array from the "getCriteria()" method.
     * 
     * @return
     */
    public List<Value> getValues() {
        if (this.values == null) {
            this.values = new ArrayList<Value>();
            for (int i = 0; i < getCriteria().length; i++) {
                this.values.add(new Value());
            }
        }
        return this.values;
    }

    /**
     * This method is called by the search.xhtml JSF page, returning the first results of the 
     * searchOnDb method. The number of results is determined by the numberDisplay variable.
     * 
     * @return The search results
     */
    public List<T> getResults() {
        if (this.searched) {
            //gets the results, possibly just the results for this page, or all results
            results = search();
            //if there are additional elements beyond the last element on the current page
            if (this.results.size() > numberDisplay.getSize() * numberDisplay.getPage()) {
                //returns the elements that are on the current page
                return this.results.subList((numberDisplay.getPage() - 1) * numberDisplay.getSize(), numberDisplay
                        .getPage()
                        * numberDisplay.getSize());
                //if there are additional elements beyond the last element on the previous page
            }
            else if (this.results.size() > numberDisplay.getSize() * (numberDisplay.getPage() - 1)) {
                //returns the last of the items, beyond the previous page
                return this.results.subList((numberDisplay.getPage() - 1) * numberDisplay.getSize(), results.size());
            }
            //otherwise, returns all items or the number to display on one page, whichever is smaller.
            return this.results.subList(0, Math.min(results.size(), numberDisplay.getPage() * numberDisplay.getSize()));
        }
        else {
            return new ArrayList<T>();
        }
    }

    /**
     * Returns whether a search has been performed.
     * 
     * @return Boolean indicating whether a search has been performed
     */
    public boolean isSearched() {
        return this.searched;
    }

    /**
     * Set whether a search has been performed.
     * 
     * @param searched
     */
    private void setSearched(boolean searched) {
        this.searched = searched;
    }

    /**
     * Returns the entity that has been selected by the user after performing a search.
     * 
     * @return The selected entity
     */
    public T getSelection() {
        return selection;
    }

    /**
     * Used to set the entity that has been selected. Once an entity is selected, the search is complete, 
     * so the "searched" field is set to false.
     * 
     * @param selection
     */
    public void setSelection(T selection) {
        this.results = new ArrayList<T>();
        this.selection = selection;
        this.searched = false;
    }

    /**
     * This method is equivalent to setSelection(null), and returns the selected value. It is written as a 
     * workaround for the bug in Richfaces where null values cannot be used as parameters.
     */
    public T clearSelection() {
        this.results = new ArrayList<T>();
        T toReturn = selection;
        setSelection(null);
        return toReturn;
    }

    /**
     * This class is a workaround for the fact that value mapping in JSF needs getters and setters. This 
     * class stores the String value that the user has entered in one of the search text fields.
     * 
     * @author RHC
     *
     */
    public class Value {

        private String value;

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public void setValue(ValueChangeEvent ev) {
            setValue((String) ev.getNewValue());
            ((UIInput) ev.getComponent()).setLocalValueSet(false);
        }
    }

    /**
     * This method performs validation, checking that an item has been selected.
     * 
     * @param context
     * @param component
     * @param value
     */
    public void validateSelection(FacesContext context, UIComponent component, Object value) {
        if (this.requireSelection && this.selection == null) {
            ((UIInput) component).setValid(false);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            if (this.searched) {
                message.setSummary("Select a search result");
                message.setDetail("You need to select a search result.");
            }
            else {
                message.setSummary("Perform a search");
                message.setDetail("You need to perform a search.");
            }
            context.addMessage(component.getClientId(), message);
            return;
        }
    }

    /**
     * This method is called by the search.xhtml page, and is used to set whether selecting an item is required.
     * 
     * @param toSet
     * @return
     */
    public String setRequireSelection(boolean toSet) {
        this.requireSelection = toSet;
        return "Search:";
    }

    /**
     * This method returns an Array of Strings which will be used as labels for the search result columns. By default, 
     * this method returns a single-element array with the only entry being the string "Result".
     * 
     * This method should be overridden if more columns are required. For example, for SystemUser, this method might 
     * return the array {"First", "Last", "Email", "Login"}.
     * 
     * @return The Array of Strings labelling the search results
     */
    public String[] getColumns() {
        String[] toReturn = new String[1];
        toReturn[0] = "Result";
        return toReturn;
    }

    /**
     * This method takes a search result of type T, and returns a Map, from the values returned by the getColumns 
     * method to the value in the search result passed in. By default, this method returns 
     * {{getColumns()[0], searchResult.toString()}} if getColumns() returns an array of size greater than zero, 
     * otherwise it returns an empty map.
     * 
     * This method should be overridden if the values to be displayed aren't the toString method. For example, for 
     * System user, this method might return a Map with "First" mapped to "John", "Second" mapped to "Smith", and 
     * so on.
     * 
     * This method is only called once per row, to prevent extra processing. However, the entity passed in is 
     * detached, so lazy load exceptions WILL be thrown if steps aren't taken to avoid it. See CatalogSearchBean for a 
     * workaround.
     * 
     * @param searchResult
     * @return
     */
    public Map<String, String> resultRow(T searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        if (this.getColumns().length > 0) {
            toReturn.put(this.getColumns()[0], searchResult.toString());
        }
        return toReturn;
    }

    /**
     * This method is called by the search.xhtml page, getting the value of a cell.
     * 
     * @param result
     * @param column
     * @return
     */
    public String getCell(T result, String column) {
        if (result == null) {
            return "None Selected";
        }
        if (!result.equals(this.currentResult)) {
            this.currentResult = result;
            this.currentRowMap = resultRow(this.currentResult);
        }
        if (this.currentRowMap.containsKey(column)) {
            return this.currentRowMap.get(column);
        }
        else {
            return "";
        }
    }

    /**
     * This method takes the name of the messages component, and performs the search functionality. If there is an 
     * error, the error message is appended to the messages.
     * 
     * @param searchMessageId
     */
    public void performSearch(String searchMessageId) {
        this.setSearched(true);
        try {
            numberDisplay = new PageNumberDisplay(this.getResultQuantity());
        }
        catch (Exception e) {
            logger.error(e.getMessage(), e);
            FacesMessage message = new FacesMessage();
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            message.setSummary("An error occurred while searching");
            message.setDetail("An unexpected error occured while searching.");
            UIComponent messageComponent = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance())
                    .getNamingContainer();
            String messageId = messageComponent != null ? messageComponent.getClientId() + ":" + searchMessageId : null;
            FacesContext.getCurrentInstance().addMessage(messageId, message);
            this.setSearched(false);
            return;
        }
    }

    /**
     * The total number of results that match the search. Defaults to the size of the List returned by the 
     * search() method.
     * 
     * @return The number of results.
     */
    public int getResultQuantity() {
        return search().size();
    }

    /**
     * This method starts the download of the CSV file of the results.
     * 
     * @return
     */
    public String downloadFile() {
        File toDownload = this.resultsCsv();
        FileDownloader.downloadFile(toDownload);
        toDownload.delete();
        return null;
    }

    /**
     * This method returns a CSV file of the results. If it is not overridden, the file consists of all of the items 
     * on the current page.
     * 
     * @return
     */
    public File resultsCsv() {
        List<Object[]> results = new ArrayList<Object[]>();
        for (T result : this.search()) {
            Object[] row = new Object[this.getColumns().length];
            for (int i = 0; i < this.getColumns().length; i++) {
                row[i] = this.getCell(result, this.getColumns()[i]);
            }
            results.add(row);
        }
        return resultsCsv.getFile(this.getColumns(), results);
    }

    /**
     * Returns the pageNumberDisplay object. 
     * <br />
     * NOTE: If you use the PageNumberDisplay object in the search() method, you MUST implement the getResultQuantity 
     * method. Otherwise, an NPE will be thrown, caused by trying to use the pageDisplay object without it being 
     * initialized.
     * 
     * @return
     */
    public PageNumberDisplay getPageDisplay() {
        return this.numberDisplay;
    }
}
