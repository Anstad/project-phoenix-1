package com.apd.phoenix.web.phonenumber;

import javax.ejb.Stateful;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.apd.phoenix.service.business.PhoneNumberBp;
import com.apd.phoenix.service.model.PhoneNumber;

//Gives a bean the functionality to validate phone numbers in the form (XXX)-XXX-XXXX
@Stateful
@Dependent
public class PhoneNumberValidationBean {

    
    private PhoneNumber currentNumber = new PhoneNumber();
    
    @Inject
    protected PhoneNumberBp phoneNumberBp;
	
	public String getPhoneNumberReadable() {
		return phoneNumberBp.getReadableNumber(this.getCurrentNumber());
	}

	public void setPhoneNumberReadable(String phoneNumberReadable) {
		this.phoneNumberBp.setReadableNumber(this.getCurrentNumber(), phoneNumberReadable);
	}

    public String getPhoneNumberReadableRegex() {
        return this.phoneNumberBp.getReadableNumberRegex();
    }

	public PhoneNumber getCurrentNumber() {
		return currentNumber;
	}

	public void setCurrentNumber(PhoneNumber currentNumber) {
		this.currentNumber = currentNumber;
	}
}
