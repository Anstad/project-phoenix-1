package com.apd.phoenix.web.searchbeans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.persistence.jpa.CostCenterDao;
import com.apd.phoenix.service.model.CostCenter;
import com.apd.phoenix.web.PropertiesBean;
import java.io.Serializable;

@Named
@ConversationScoped
public class CostCenterSearchBean extends AbstractSearchBean<CostCenter> implements Serializable {

    private static final long serialVersionUID = -4511922699323752011L;

    @Inject
    private PropertiesBean propertiesBean;

    @Inject
    CostCenterDao dao;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Name", "Number" };
        return toReturn;
    }

    @Override
    protected List<CostCenter> search() {
        CostCenter search = new CostCenter();
        search.setName(this.getValues().get(0).getValue());
        search.setNumber(this.getValues().get(1).getValue());
        return dao.searchByExample(search, 0, 0);
    }

    @Override
    public String[] getColumns() {
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            String[] toReturn = { "Name", "Number", "System ID" };
            return toReturn;
        }
        else {
            String[] toReturn = { "Name", "Number" };
            return toReturn;
        }
    }

    @Override
    public Map<String, String> resultRow(CostCenter searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getName());
        toReturn.put(getColumns()[1], searchResult.getNumber());
        if (propertiesBean.get("project.state", "isPreRelease", "false").equals("true")) {
            toReturn.put(getColumns()[2], searchResult.getId().toString());
        }
        return toReturn;
    }

}
