/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.web.searchbeans;

import com.apd.phoenix.service.business.DesktopBp;
import com.apd.phoenix.service.model.Desktop;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author anicholson
 */
@Named
@ConversationScoped
public class DesktopSearchBean extends AbstractSearchBean<Desktop> implements Serializable {

    @Inject
    DesktopBp desktopBp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Value" };
        return toReturn;
    }

    @Override
    protected List<Desktop> search() {
        Desktop search = new Desktop();
        search.setName(this.getValues().get(0).getValue());
        return desktopBp.searchByExample(search, 0, 0);
    }

}
