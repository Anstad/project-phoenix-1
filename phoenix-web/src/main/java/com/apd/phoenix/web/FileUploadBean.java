package com.apd.phoenix.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.apd.phoenix.service.business.OrderAttachmentBp;
import com.apd.phoenix.service.business.ServiceLogBp;
import com.apd.phoenix.service.model.OrderAttachment;
import com.apd.phoenix.service.model.ServiceLog;
import com.apd.phoenix.service.model.SystemUser;
import com.apd.phoenix.service.utility.FileDownloader;

@Stateful
@Dependent
public class FileUploadBean {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUploadBean.class);
	
	@Inject
	private Conversation conversation;
	
	@Inject
    private OrderAttachmentBp uploadedFileBp;	
	
	@Inject
	private ServiceLogBp serviceLogBp;
    
    private String fileName;
    
    private long fileSize;

    public String downloadFile(OrderAttachment file) {
    	return FileDownloader.downloadFile(uploadedFileBp.getFile(file), file.getFileName());
    }
    
    public void preUploadFile() {
    	this.fileName = null;
    	this.deleteTempDirectory(conversation.getId());
    }
    
    public void fileUploadListener(FileUploadEvent event) throws Exception {
    	this.fileName=event.getUploadedFile().getName();
    	this.fileSize = event.getUploadedFile().getSize();
    	this.setTempFile(event.getUploadedFile().getInputStream(), this.conversation.getId(), this.getFileName());
    }
    
    public OrderAttachment saveUploadedFile(ServiceLog serviceLog, SystemUser systemUser) {
    	OrderAttachment file = new OrderAttachment();
    	file.setFileName(getFileName());
    	file.setUser(systemUser);
    	file = uploadedFileBp.create(file);
    	if(serviceLog != null){
    		serviceLog.getAttachments().add(file);
    		serviceLogBp.update(serviceLog);
    	}
    	file = uploadedFileBp.findById(file.getId(), OrderAttachment.class);
    	try (
    			FileInputStream fis = new FileInputStream(this.getTempFile(conversation.getId(), getFileName()).getAbsoluteFile());
    	) {
			uploadedFileBp.setFile(file, fis, this.fileSize);
		} catch (IOException e) {
			logger.info("IO Exception when saving uploaded file", e);
		}
    	return file;
    }

    private File getTempFile(String convoNumber, String fileName) {
        return new File("/var/tmp/" + convoNumber + "/" + fileName);
    }

    private void setTempFile(InputStream fileStream, String convoNumber, String fileName) throws IOException {
	    String filename = "/var/tmp/" + convoNumber + "/" + fileName;
	    File tempDirectory = new File("/var/tmp/" + convoNumber);
	    File file = new File(filename);
	    if (!tempDirectory.exists()) {
	        tempDirectory.mkdir();
	    }
	    if (file.exists()) {
	        file.delete();
	    }
	    try (
	    		FileOutputStream fileOutputStream = new FileOutputStream(file.getAbsoluteFile());
	    	) {
		    file.createNewFile();
		    IOUtils.copy(fileStream, fileOutputStream);
	    } finally {	    	
	    	IOUtils.closeQuietly(fileStream);
	    }
    }
    
    private void deleteTempDirectory(String convoNumber) {
    	File tempDirectory = new File("/var/tmp/" + convoNumber);
    	if (tempDirectory.exists()) {
    		tempDirectory.delete();
    	}
    }
    
    @PreDestroy
    protected void destroy() {
    	try {
    		this.deleteTempDirectory(conversation.getId());
    	}
    	catch (Exception e) {
    		logger.warn("Error deleting temp directory, turn on debug logging to view stack trace");
    		if (logger.isDebugEnabled()) {
    			logger.debug("Stack trace", e);
    		}
    	}
    }
    
    public String getFileName() {
		return fileName;
	}

}
