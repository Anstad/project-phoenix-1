package com.apd.phoenix.web;

import java.io.Serializable;
import javax.inject.Named;
import com.apd.phoenix.core.utility.PropertiesLoader;

@Named
public class PropertiesBean implements Serializable {

    private static final long serialVersionUID = -5229651643823847245L;

    @SuppressWarnings("static-method")
    public String get(String file, String key) {
        return PropertiesLoader.getAsProperties(file).getProperty(key);
    }

    @SuppressWarnings("static-method")
    public String get(String file, String key, String defaultValue) {
        return PropertiesLoader.getAsProperties(file).getProperty(key, defaultValue);
    }
}