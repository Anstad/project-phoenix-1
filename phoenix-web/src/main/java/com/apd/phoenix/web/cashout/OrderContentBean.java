/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.web.cashout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Named;
import com.apd.phoenix.service.business.EntityComparator;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;

/**
 *
 * @author RHC
 */
@Named
@Stateless
public class OrderContentBean implements Serializable {

    private static final long serialVersionUID = -8193274311089613421L;

    public List<LineItem> getFees(CustomerOrder order) {

        List<LineItem> toReturn = itemsList(order);

        for (LineItem item : order.getItems()) {
            if (!item.isFee()) {
                toReturn.remove(item);
            }
        }

        return toReturn;
    }

    public List<LineItem> getMerchandise(CustomerOrder order) {

        List<LineItem> toReturn = itemsList(order);

        for (LineItem item : order.getItems()) {
            if (item.isFee()) {
                toReturn.remove(item);
            }
        }

        return toReturn;
    }

    private List<LineItem> itemsList(CustomerOrder order) {
        List<LineItem> toReturn = new ArrayList<LineItem>(order.getItems());

        Collections.sort(toReturn, new EntityComparator());

        return toReturn;
    }
}
