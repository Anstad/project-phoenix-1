package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.DepartmentBp;
import com.apd.phoenix.service.model.Department;

@Named
@ConversationScoped
public class DepartmentSearchBean extends AbstractSearchBean<Department> implements Serializable {

    @Inject
    DepartmentBp departmentBp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "Value" };
        return toReturn;
    }

    @Override
    protected List<Department> search() {
        Department search = new Department();
        search.setName(this.getValues().get(0).getValue());
        return departmentBp.searchByExample(search, 0, 0);
    }

}
