package com.apd.phoenix.web.searchbeans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.model.SystemUser;

@Named
@ConversationScoped
public class AbstractUserSearchBean extends AbstractSearchBean<SystemUser> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    SystemUserBp bp;

    @Override
    public String[] getCriteria() {
        String[] toReturn = { "First Name", "Last Name", "Login", "Email Address" };
        return toReturn;
    }

    @Override
    protected List<SystemUser> search() {
        String firstName = this.getValues().get(0).getValue();
        String lastName = this.getValues().get(1).getValue();
        String login = this.getValues().get(2).getValue();
        String email = this.getValues().get(3).getValue();
        return bp.filteredUserList(firstName, lastName, login, email);
    }

    @Override
    public String[] getColumns() {
        String[] toReturn = { "First", "Last", "Email", "Login" };
        return toReturn;
    }

    @Override
    public Map<String, String> resultRow(SystemUser searchResult) {
        Map<String, String> toReturn = new HashMap<String, String>();
        toReturn.put(getColumns()[0], searchResult.getPerson().getFirstName());
        toReturn.put(getColumns()[1], searchResult.getPerson().getLastName());
        toReturn.put(getColumns()[2], searchResult.getPerson().getEmail());
        toReturn.put(getColumns()[3], searchResult.getLogin());
        return toReturn;
    }

}
