package com.apd.phoenix.customerservice.view.jsf.bean.returns;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.LineItemXShipmentBp;
import com.apd.phoenix.service.business.LineItemXShipmentXTaxTypeBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.ReturnOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipmentXTaxType;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.TaxType.TaxTypeEnum;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.model.TaxRateAtPurchase;
import com.apd.phoenix.service.workflow.WorkflowService;

@Named
@Stateful
@ConversationScoped
public class ReturnDetailsBean {

    @Inject
    private ReturnOrderBp returnBp;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private LineItemXShipmentBp lineItemXShipmentBp;

    @Inject
    private LineItemBp lineItemBp;

    @Inject
    private LineItemStatusBp itemStatusBp;

    @Inject
    private LineItemXShipmentXTaxTypeBp taxBp;

    @Inject
    private OrderLogBp orderLogBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    private ReturnOrder returnOrder;

    private static final Logger LOG = LoggerFactory.getLogger(ReturnDetailsBean.class);

    private BigDecimal restockingFee = BigDecimal.ZERO;

    public ReturnOrder getReturn() {
        return returnOrder;
    }

    public void setReturn(ReturnOrder returnOrder) {
        this.returnOrder = returnBp.hydrateForReturnDetails(returnOrder);
    }

    public BigDecimal getRestockingFee() {
        return restockingFee;
    }

    public void setRestockingFee(BigDecimal restockingFee) {
        this.restockingFee = restockingFee;
    }

    public boolean isEditable() {
        if (returnOrder == null) {
            return false;
        }
        return ReturnOrder.ReturnOrderStatus.UNRECONCILED.equals(returnOrder.getStatus());
    }

    public String getReturnTotal() {
        return returnBp.getReturnTotal(this.returnOrder).toPlainString();
    }

    public void reconcileReturn() {
        this.addTaxAndReconcile();
        if (restockingFee != null && restockingFee.compareTo(BigDecimal.ZERO) > 0) {
            this.createRestockingFee();
        }
    }

    public void closeReturn() {
        LOG.info("Closing return!");
        this.returnOrder.setStatus(ReturnOrderStatus.CLOSED);
        this.returnOrder.setCloseDate(new Date());
        this.returnBp.update(returnOrder);
        logReturnRelatedAction("close return");
    }

    private void addTaxAndReconcile() {
        LOG.info("Reconciling return!");
        returnOrder = returnBp.update(returnOrder);
        if (returnOrder.getOrder() != null && returnOrder.getOrder().getId() != null) {
        	//tax rates at purchase are obtained
            List<TaxRateAtPurchase> jurisdictionTaxRates = new ArrayList<TaxRateAtPurchase>(lineItemXShipmentBp.taxRates(returnOrder.getOrder().getId()));
            
            //total tax percentage is the sum of each jurisdiction's tax rate. It will be something like 0.07.
            BigDecimal totalTaxPercentage = BigDecimal.ZERO;
            
            //adds the tax percentages together to get the total tax amount, and selects the State tax, which will be 
            //moved to the end of the list.
            TaxRateAtPurchase toSetLast = null;
            List<TaxRateAtPurchase> toRemove = new ArrayList<>();
            for (TaxRateAtPurchase jurisdictionTaxRate : jurisdictionTaxRates) {
            	//removes any tax rates with zero or null values
            	if (jurisdictionTaxRate.getValue() != null && jurisdictionTaxRate.getValue().compareTo(BigDecimal.ZERO) != 0) {
                	totalTaxPercentage = totalTaxPercentage.add(jurisdictionTaxRate.getValue());
            	}
            	else {
            		toRemove.add(jurisdictionTaxRate);
            	}
            	//if this tax rate is the state tax, moves it to the end of the list
            	if (jurisdictionTaxRate.getType() != null && jurisdictionTaxRate.getType().getEnumValue().equals(TaxTypeEnum.STATE)) {
            		toSetLast = jurisdictionTaxRate;
            	}
            }
            
            if (toSetLast != null) {
                //moves state tax to the end of the list; this tax is the last that should be calculated
            	jurisdictionTaxRates.remove(toSetLast);
            	jurisdictionTaxRates.add(toSetLast);
            }
            
            jurisdictionTaxRates.removeAll(toRemove);
            
            
            for (LineItemXReturn item : returnOrder.getItems()) {
            	BigDecimal addedTax = BigDecimal.ZERO;
            	BigDecimal lixsTaxAmount = item.getLineItem().getPaidTax();
                for (TaxRateAtPurchase rate : jurisdictionTaxRates) {
                    item.toString();
                    rate.toString();
                    if (rate.getValue() != null && item.getLineItem().getUnitPrice() != null
                            && item.getQuantity() != null && item.getLineItem().isTaxable()) {
                    	BigDecimal remainingTax = lixsTaxAmount.subtract(addedTax);
                    	
                    	BigDecimal jurisdictionTax = rate.getValue().multiply(item.getLineItem().getUnitPrice()).multiply(
                                new BigDecimal(item.getQuantity().toString())).setScale(2, RoundingMode.UP);
                    	if (jurisdictionTax.compareTo(remainingTax) > 0) {
                			jurisdictionTax = remainingTax;
                		}
                		//adds this jurisdiction's tax to the lineitemxshipment as a lineitemxshipmentxtaxtype
                		addedTax = addedTax.add(jurisdictionTax);        
                        LineItemXShipmentXTaxType newTax = new LineItemXShipmentXTaxType();
                        newTax.setLineItemXReturn(item);
                        newTax.setType(rate.getType());
                        newTax.setValue(jurisdictionTax);
                        
                        item.getTaxes().add(newTax);
                        taxBp.update(newTax);
                    }
                }
            }
        }
        returnBp.update(returnOrder);
        logReturnRelatedAction( "reconcile return");
        workflowService.processReturn(returnOrder);
    }

    private void logReturnRelatedAction(String description) {
        CustomerOrder order = customerOrderBp.findById(returnOrder.getOrder().getId(), CustomerOrder.class);
        order.getOrderLogs().add(
                orderLogBp.logChangeOrder(returnOrder.getOrder(), description + " for RA number "
                        + returnOrder.getRaNumber(), ((HttpServletRequest) FacesContext.getCurrentInstance()
                        .getExternalContext().getRequest()).getRemoteUser()));
        returnOrder.setOrder(customerOrderBp.update(order));
        returnOrder = returnBp.hydrateForReturnDetails(returnOrder);
    }

    private void createRestockingFee() {
        LineItem fee = lineItemBp.createFee();
        fee.setUnitPrice(restockingFee);
        fee.setApdSku("Restocking Fee");
        fee.setOrder(returnOrder.getOrder());
        fee.setShortName("Restocking fee");
        fee.setDescription("Restocking fee");
        int lineNumber = 0;
        for (LineItem item : returnOrder.getOrder().getItems()) {
            if (item.getLineNumber() > lineNumber) {
                lineNumber = item.getLineNumber();
            }
        }
        fee.setLineNumber(lineNumber + 1);
        fee.setSupplierPartId(fee.getApdSku());
        fee = lineItemBp.update(fee);
        lineItemBp.setStatus(fee, itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.ORDERED));
        returnOrder.setRestockingFee(fee);
        BigDecimal remainingRestocking = restockingFee;
        LineItemXReturn lastItem = null;
        for (LineItemXReturn item : returnOrder.getItems()) {
            if (returnOrder.getItems().size() != 0 && remainingRestocking != null) {
                item.setRestockingFee(restockingFee.divide(new BigDecimal("" + returnOrder.getItems().size()), 2,
                        RoundingMode.DOWN));
                remainingRestocking = remainingRestocking.subtract(item.getRestockingFee());
                lastItem = item;
            }
        }
        if (lastItem != null && lastItem.getRestockingFee() != null && remainingRestocking != null) {
            lastItem.setRestockingFee(lastItem.getRestockingFee().add(remainingRestocking));
        }
        returnBp.update(returnOrder);
        ShipmentDto shipmentDto = new ShipmentDto();
        try {
            shipmentDto.setCustomerOrderDto(DtoFactory.createPurchaseOrderDto(this.returnOrder.getOrder()));
        }
        catch (ParsingException e) {
            LOG.error("Unable to ship fee, due to a DTO Factory parse exception.");
            return;
        }
        shipmentDto.setTrackingNumber(this.returnOrder.getRaNumber());
        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
        BigDecimal totalShipPrice = BigDecimal.ZERO;
        LineItemXShipmentDto lixs = new LineItemXShipmentDto();
        lixs.setLineItemDto(DtoFactory.createLineItemDto(fee));
        lixs.setQuantity(BigInteger.ONE);
        lixs.setShipping(BigDecimal.ZERO);
        totalShipPrice = totalShipPrice.add(fee.getUnitPrice().multiply(new BigDecimal(fee.getQuantity())));
        shipmentDto.getLineItemXShipments().add(lixs);
        shipmentDto.setShipmentPrice(totalShipPrice);
        workflowService.processOrderUpdate(this.returnOrder.getOrder().getId());
        workflowService.processOrderShipment(shipmentDto);
        logReturnRelatedAction("added restocking fee of " + restockingFee.toPlainString());
    }

}