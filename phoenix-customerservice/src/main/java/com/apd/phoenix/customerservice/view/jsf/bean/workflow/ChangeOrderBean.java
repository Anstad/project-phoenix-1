package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import com.apd.phoenix.customerservice.view.jsf.bean.login.LoginBean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.ReturnOrderBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.CanceledQuantity.CancelReason;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXReturn.ReturnReason;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.order.PlaceOrderService;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;
import com.apd.phoenix.service.utility.JSFUtils;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.web.AbstractControllerBean;


/**
 * This class provides backing bean functionality for the CSR change order page.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@ConversationScoped
public class ChangeOrderBean extends AbstractControllerBean<CustomerOrder> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ChangeOrderBean.class);
        @Inject
        private OrderDetailsBean orderBean;
        @Inject
        private LineItemBp lineItemBp;
        @Inject 
        private ItemBp itemBp;
        @Inject
        private LineItemStatusBp itemStatusBp;
        @Inject
        private CustomerOrderBp orderBp;
        @Inject
        private WorkflowService workflowService;
        @Inject
        private Conversation conversation;
        @Inject
        private ReturnOrderBp returnBp;
        @Inject
        private PlaceOrderService placeOrderService;
        @Inject
        private LoginBean loginBean;
        @Inject 
        private OrderLogBp orderLogBp;
        @Inject
        private CommentBp commentBp;
        
        //maps each object to an action, used to prevent two different actions from being done on the same object
        private Map<LineItem, PendingWorkflowAction> objectActionMap = new HashMap<>();
        private LineItem currentLineItem;
        private BigDecimal currentShipping;
        private WorkflowAction currentAction;
        private ReturnReason currentReturnReason;
        private CancelReason currentCancelReason;
        private int currentActionQuantity;
        private String raNumber;
        private String shipNumber;
        private boolean needsTrackingNumber = false;
        private boolean returnShipping = false;
        private Comment currentComment = new Comment();
        @Inject
        private ItemCategoryBp itemCategoryBp;
        private BigDecimal currentPrice;
        
        @PostConstruct
        public void init() {
            if (conversation.isTransient()) {
                conversation.begin();
            }
        }
        
        /**
         * Called when the "Save" button is pressed on the change order page. Persists any changes, and kicks off any necessary workflow
         */
        public String update() {
        	if (this.orderBean.getOrder() != null && !this.orderBean.getOrder().hasAllowedStatus()) {
                FacesMessage message = new FacesMessage();
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                message.setSummary("Orders with this status cannot be changed.");
                message.setDetail("Orders with this status cannot be changed.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                return null;
        	}
        	
    		
            List<PendingWorkflowAction> keysToReHash = new ArrayList<PendingWorkflowAction>();
            //for each item, if it doesn't exist yet, pulls out and saves the value in the map 
            //(because the hash will change when the order is persisted)
            
            for (LineItem item : orderBean.getOrder().getItems()) {
                    if (item.getId() == null) {
                            item.setOrder(orderBean.getOrder());                         
                            keysToReHash.add(objectActionMap.remove(item));
                    }
            }
            this.orderBean.getOrder().refreshOrderTotal();
                    //persists and repopulates the order, with the newly created items
            orderBean.setOrder(orderBp.update(orderBean.getOrder()));

            for (PendingWorkflowAction action : keysToReHash) {
                    for (LineItem item : orderBean.getOrder().getItems()) {
                            if (action.actionTarget.getLineNumber().equals(item.getLineNumber())) {
                                    action.actionTarget = item;
                                    objectActionMap.put(item, action);
                                break;
                            }
                    }
            }

            //finally, executes the actions
            for (WorkflowAction action : WorkflowAction.values()) {
                    executeBatchWorkflowAction(action);
            }

        	if (this.currentComment.getContent() != null) {
    			this.currentComment.setCommentDate(new Date());
    			this.currentComment.setUser(loginBean.getSystemUser());
    			this.orderBean.getOrder().getComments().add(this.currentComment);
    			this.commentBp.addComment(this.orderBean.getOrder(), currentComment);
    		}
        	this.setCurrentComment(new Comment());
            orderBean.setOrder(orderBp.update(orderBean.getOrder()));

            return null;
        }
        
        /**
         * Executes the action on the LineItems.
         * 
         * @param action
         */
        private void executeBatchWorkflowAction(WorkflowAction action) {
                //collects a list of all of the pending actions with the given action type
                List<PendingWorkflowAction> toExecute = new ArrayList<>();
                for (LineItem item : objectActionMap.keySet()) {
                        if (action.equals(objectActionMap.get(item).action) && this.orderBean.getOrder().getItems().contains(item)) {
                                toExecute.add(objectActionMap.get(item));
                        }
                }
                //if there are no pending actions with this type, returns
                if (toExecute.size() == 0) {
                        return;
                }
                try {
                    CustomerOrder co = this.orderBean.getOrder();
                    String logDescription = action.name() + ": ";
                    LineItemStatus newItemStatus;
                    switch (action) {
                        case SHIP:
						logDescription = createAndProcessShipment(toExecute,
								co, logDescription);
                            break;
                        case RETURN:
                            ReturnOrder returnOrder = new ReturnOrder();
                            returnOrder.setOrder(co);
                            returnOrder.setRaNumber(this.getRaNumber());
                            returnOrder.setStatus(ReturnOrder.ReturnOrderStatus.UNRECONCILED);
                            returnOrder.setCreatedDate(new Date());
                            for (PendingWorkflowAction returnAction : toExecute) {
                                LineItemXReturn newReturnItem = new LineItemXReturn();
                                newReturnItem.setLineItem(lineItemBp.findById(returnAction.actionTarget.getId(), LineItem.class));
                                newReturnItem.setQuantity(BigInteger.valueOf(returnAction.quantity));
                                newReturnItem.setReason(returnAction.reason);
                                newReturnItem.setReturnShipping(returnAction.returnShipping);
                                newReturnItem.calculateReturnShippingAmount();
                                logDescription = logDescription + "Returned " + newReturnItem.getQuantity().toString() 
                                        + " of line number " + returnAction.actionTarget.getLineNumber() + ". ";
                                returnOrder.getItems().add(newReturnItem);
                            }
                            returnBp.update(returnOrder);
                            break;
                        case CANCEL:
                            ShipmentDto cancelDto = new ShipmentDto();
                            cancelDto.setCustomerOrderDto(DtoFactory.createPurchaseOrderDto(co));
                            cancelDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
                            for (PendingWorkflowAction cancelAction : toExecute) {
                                if(cancelAction.quantity >= cancelAction.actionTarget.getQuantity()) {
                                    logDescription = logDescription + "CANCELED line number " 
                                        + cancelAction.actionTarget.getLineNumber() + ". ";
                                }
                                else {
                                    logDescription = logDescription + "PARTIAL_CANCEL for " + 
                                    	cancelAction.quantity + " of line number " 
                                        + cancelAction.actionTarget.getLineNumber() + ". ";
                                }
                                Item item = null;
                                if (cancelAction.actionTarget.getItem() != null) {
                                        item = itemBp.searchById(cancelAction.actionTarget.getItem().getId());
                                }
                                LineItemXShipmentDto cancelledItem = new LineItemXShipmentDto();
                                cancelledItem.setLineItemDto(DtoFactory.createLineItemDto(cancelAction.actionTarget,item));
                                cancelledItem.setQuantity(BigInteger.valueOf(cancelAction.quantity));
                                cancelledItem.setVendorComments(new ArrayList<String>());
                                cancelledItem.getVendorComments().add(cancelAction.reason);
                                cancelDto.getLineItemXShipments().add(cancelledItem);
                            }                        
                            workflowService.cancelOrder(co.getApdPo().getValue(), cancelDto);
                            break;
                        case REJECT:
                            LineItemStatus rejectedStatus = itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.REJECTED);
                            POAcknowledgementDto rejectDto = new POAcknowledgementDto();
                            rejectDto.setApdPo(co.getApdPo().getValue());
                            rejectDto.setPartnerId(co.getCredential().getPartnerId());
                            rejectDto.setPurchaseOrderDto(DtoFactory.createPurchaseOrderDto(co));
                            rejectDto.setLineItems(new ArrayList<LineItemDto>());
                            for (PendingWorkflowAction rejectAction : toExecute) {
                                Item item = null;
                                if (rejectAction.actionTarget.getItem() != null) {
                                        item = itemBp.searchById(rejectAction.actionTarget.getItem().getId());
                                }
                                LineItemDto toAdd = DtoFactory.createLineItemDto(rejectAction.actionTarget,item);
                                toAdd.setStatus(DtoFactory.createLineItemStatusDto(rejectedStatus));
                                toAdd.setQuantity(new BigInteger("" + rejectAction.quantity));
                                rejectDto.getLineItems().add(toAdd);
                                logDescription = logDescription + "rejected line number " + 
                                        rejectAction.actionTarget.getLineNumber() + ". ";
                            }
                            workflowService.processOrderAcknowledgement(rejectDto);
                            break;       
                        case ACCEPT:
                            LineItemStatus acceptedStatus = itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.ACCEPTED);
                            POAcknowledgementDto acceptDto = new POAcknowledgementDto();
                            acceptDto.setApdPo(co.getApdPo().getValue());
                            acceptDto.setPartnerId(co.getCredential().getPartnerId());
                            acceptDto.setLineItems(new ArrayList<LineItemDto>());
                            acceptDto.setPurchaseOrderDto(DtoFactory.createPurchaseOrderDto(co));
                            for (PendingWorkflowAction acceptAction : toExecute) {
                                Item item = null;
                                if (acceptAction.actionTarget.getItem() != null) {
                                    item = itemBp.searchById(acceptAction.actionTarget.getItem().getId());
                                }
                                LineItemDto toAdd = DtoFactory.createLineItemDto(acceptAction.actionTarget,item);
                                toAdd.setStatus(DtoFactory.createLineItemStatusDto(acceptedStatus));
                                acceptDto.getLineItems().add(toAdd);
                                logDescription = logDescription + "Accepted " + acceptAction.actionTarget.getQuantity().toString() 
                                        + " of line number " + acceptAction.actionTarget.getLineNumber() + ". ";
                            }
                            workflowService.processOrderAcknowledgement(acceptDto);
                            break;
                        case RESEND:
                            Map<Vendor, Map<LineItem, Integer>> vendorsItems = new HashMap<Vendor, Map<LineItem, Integer>>();
                            for (PendingWorkflowAction shipAction : toExecute) {
                                    if (vendorsItems.get(shipAction.actionTarget.getVendor()) == null) {
                                    	vendorsItems.put(shipAction.actionTarget.getVendor(), new HashMap<LineItem, Integer>());
                                    }
                                    shipAction.actionTarget.setQuantity(shipAction.quantity);
                                    vendorsItems.get(shipAction.actionTarget.getVendor()).put(shipAction.actionTarget, shipAction.quantity);
                                    logDescription = logDescription + "resent line number " 
                                    + shipAction.actionTarget.getLineNumber() + ". ";
                            }
                            if (!vendorsItems.keySet().isEmpty()) {
                            	co.setResendAttempts((co.getResendAttempts() != null ? co.getResendAttempts() : 0) + 1);
                            }
                            for (Vendor vendor : vendorsItems.keySet()) {
                                    placeOrderService.resendItems(co, vendorBp.update(vendor), vendorsItems.get(vendor));
                            }
                            break;
                        case CREATE:
                    		BigDecimal taxRate = getTaxRate();
                    		BigDecimal itemRate = BigDecimal.ZERO;
                            for (PendingWorkflowAction itemAction : toExecute) {
                                lineItemBp.setStatus(itemAction.actionTarget, itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.CREATED));
                                lineItemBp.setStatus(itemAction.actionTarget, itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.ORDERED));
                                itemAction.actionTarget.setQuantity(itemAction.quantity);
                                itemAction.actionTarget.setUnitPrice(itemAction.feePrice);                                        
                                itemAction.actionTarget.setOrder(co);
                                itemAction.actionTarget.setSupplierPartId(itemAction.actionTarget.getApdSku());
                                if (itemAction.actionTarget.getTaxable()) {
                                    itemRate = taxRate;
                                }
                                BigDecimal itemTax = itemAction.actionTarget.getTotalPrice().add(itemAction.actionTarget.getEstimatedShippingAmount()).multiply(itemRate).setScale(2, RoundingMode.HALF_UP);
                                itemAction.actionTarget.setMaximumTaxToCharge(itemTax);
                                this.orderBean.getOrder().setMaximumTaxToCharge(this.orderBean.getOrder().getMaximumTaxToCharge().add(itemTax));
                                logDescription = logDescription + "created line number " 
                                    + itemAction.actionTarget.getLineNumber() + ". ";
                                lineItemBp.update(itemAction.actionTarget);
                            }
                            this.orderBean.getOrder().refreshOrderTotal();
                            orderBean.setOrder(orderBp.update(orderBean.getOrder()));
                            workflowService.processOrderUpdate(co.getId());
                            break;
                        case UPDATE_ORDERED_QUANTITY:
                            for (PendingWorkflowAction itemAction : toExecute) {
                                    if (itemAction.actionTarget.getItemXShipments().size() == 0) {
                                        itemAction.actionTarget = lineItemBp.findById(itemAction.actionTarget.getId(), LineItem.class);
                                        itemAction.actionTarget.setQuantity(itemAction.quantity);
                                        logDescription = logDescription + "updated quantity to " +
                                            itemAction.actionTarget.getQuantity() +" for line number " 
                                            + itemAction.actionTarget.getLineNumber() + ". ";
                                        lineItemBp.update(itemAction.actionTarget);
                                    }
                            }
                            workflowService.processOrderUpdate(co.getId());
                            break;
                        case UPDATE_FEE_PRICE:
                            for (PendingWorkflowAction itemAction : toExecute) {
                                    if (itemAction.actionTarget.getItemXShipments().size() == 0) {
                                            itemAction.actionTarget = lineItemBp.findById(itemAction.actionTarget.getId(), LineItem.class);                                        
                                            itemAction.actionTarget.setUnitPrice(itemAction.feePrice);
                                            logDescription = logDescription + "updated fee price to "
                                                +itemAction.actionTarget.getUnitPrice().toPlainString() +
                                                " for item line number " + itemAction.actionTarget.getLineNumber() + ". ";
                                            lineItemBp.update(itemAction.actionTarget);
                                    }
                            }
                            workflowService.processOrderUpdate(co.getId());
                            break;
                        case MARK_AS_RESOLVED:
                            for (PendingWorkflowAction itemAction : toExecute) {
                                itemAction.actionTarget = lineItemBp.findById(itemAction.actionTarget.getId(), LineItem.class);   
                                logDescription = logDescription + "manually marked as resolved item number "
                                		+ itemAction.actionTarget.getLineNumber() + ". ";
                                newItemStatus = itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.MANUALLY_RESOLVED);
                                lineItemBp.setStatus(itemAction.actionTarget, newItemStatus);
                                lineItemBp.update(itemAction.actionTarget);
                            }
                            break;
                        case REFRESH_IN_WORKFLOW:
                            workflowService.processOrderUpdate(co.getId());
                            logDescription = logDescription + "refreshed the order in workflow ";
                            break;
                    }                    
                    co.getOrderLogs().add(orderLogBp.logChangeOrder(co, logDescription, loginBean.getUsername()));
                    for (PendingWorkflowAction pendingAction : toExecute) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("com.apd.phoenix.messages.customerService").getString("changeOrder.action.success") + action + " Line Item " + pendingAction.actionTarget.getLineNumber(), ""));
                    }
                } catch (Exception e) {
                  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, ResourceBundle.getBundle("com.apd.phoenix.messages.customerService").getString("changeOrder.action.error") + action, ""));  
                  LOGGER.error("Exception caught",e);
                }
                return;
        }

		private String createAndProcessShipment(
				List<PendingWorkflowAction> toExecute, CustomerOrder co,
				String logDescription) throws ParsingException {
			ShipmentDto shipmentDto = DtoFactory.createBasicShipmentDto(co, this.shipNumber);
			BigDecimal totalShipPrice = BigDecimal.ZERO;
			for (PendingWorkflowAction shipAction : toExecute) {
			    LineItemXShipmentDto lixs = lineItemBp.createLineItemXShipmentDto(shipAction.actionTarget, shipAction.quantity, shipAction.shipping);
			    shipmentDto.getLineItemXShipments().add(lixs);
			    totalShipPrice = totalShipPrice.add(shipAction.shipping);
			    totalShipPrice = totalShipPrice.add(shipAction.actionTarget.getUnitPrice().multiply(new BigDecimal(shipAction.quantity)));
			    logDescription = logDescription + "Shipped " + lixs.getQuantity().toString() 
			            + " of line number " + shipAction.actionTarget.getLineNumber() + ". ";
			    
			}
			shipmentDto.setShipmentPrice(totalShipPrice);
			workflowService.processOrderShipment(shipmentDto);
			return logDescription;
		}

		
        /**
         * Called when the "Confirm" button in the line item modal is pressed. Saves the changes on the bean.
         */
        public void updateCurrentLineItem() {
                this.currentLineItem.setUnitPrice(this.currentPrice);
                if (!this.orderBean.getOrder().getItems().contains(this.currentLineItem)) {                        
                        this.orderBean.getOrder().getItems().add(this.currentLineItem);
                        this.setCurrentAction(WorkflowAction.CREATE);
                }
                String reason = null;
                if(this.currentCancelReason != null) {
                	reason = this.currentCancelReason.getLabel();
                }
                if(this.currentReturnReason != null) {
                	reason = this.currentReturnReason.getLabel();
                }
                if(this.returnShipping){
                	this.currentLineItem.setReturnShipping(this.returnShipping);
                }
                setAction(this.currentAction, this.currentLineItem, this.currentActionQuantity, this.currentShipping, this.currentPrice, this.returnShipping, reason);
                clearLineItem();
        }
        
        /**
         * Called by the "Ship" button
         */
        public void shipOrder() {
                this.setOrderAction(WorkflowAction.SHIP, null);
        }
        
        /**
         * Called by the "Return" button
         */
        public void returnOrder() {
                this.setOrderAction(this.currentAction, this.currentReturnReason.getLabel());
                this.currentReturnReason = null;
                this.currentAction = null;
        }
        
        /**
         * Called by the "Cancel" button
         */
        public void cancelOrder() {
                this.setOrderAction(this.currentAction, this.currentCancelReason.getLabel());
                this.currentCancelReason = null;
                this.currentAction = null;
        }
        
        /**
         * Called by the "Refresh In Workflow" button
         */
        public void refreshOrderInWorkflow() {
                this.setOrderAction(WorkflowAction.REFRESH_IN_WORKFLOW, null);
        }
        
        /**
         * Called by shipOrder, returnOrder, and cancelOrder. Sets the actions for each line item, and performs the action.
         * 
         * @param action
         */
        private void setOrderAction(WorkflowAction action, String reason) {
            for (LineItem item : orderBean.getOrder().getItems()) {
            	int quantity = item.getQuantity();
            	if (action.equals(WorkflowAction.SHIP) || action.equals(WorkflowAction.CANCEL)) {
            		quantity = item.getQuantity() - item.getQuantityShipped() - item.getQuantityCancelled();
            	}
            	else if (action.equals(WorkflowAction.RETURN)) {
            		quantity = item.getQuantityShipped();
            	}
            	
                if (item.getId() != null && quantity != 0) {
                	BigDecimal shippingToUse = BigDecimal.ZERO;
                	if (WorkflowAction.SHIP.equals(action)) {
                		shippingToUse = this.getRemainingShipping(item);
                	}
                    setAction(action, item, quantity, shippingToUse, BigDecimal.ZERO, returnShipping, reason);
                }
            }
        }
        
        public void clearLineItem() {
                this.currentAction = null;
                this.currentLineItem = null;
                this.currentCancelReason = null;
                this.currentReturnReason = null;
                this.currentActionQuantity = 0;
                this.currentShipping = null;
                this.currentPrice= BigDecimal.ZERO;
                this.shipNumber = "";
                this.returnShipping = false;
                clearFields("editItemModalForm");
        }
        
        /**
         * Creates a new line item, to be edited.
         */
        public void newLineItem() {
                LineItem newItem = new LineItem();
                newItem.setQuantity(0);
                newItem.setUnspscClassification(ManualOrderBean.DEFAULT_UNSPSC);
                //setting initial status to "CREATED" until after the order is saved
                this.setAction(WorkflowAction.CREATE, newItem, 0, BigDecimal.ZERO, BigDecimal.ZERO, false, null);
                setLineItemNumber(newItem);
                newItem.setOrder(this.orderBean.getOrder());
                this.setCurrentLineItem(newItem);
        }
        
        /**
         * Creates a new "fee" line item, to be edited.
         */
        public void newFee() {
                LineItem newFee = lineItemBp.createFee();
                this.setAction(WorkflowAction.CREATE, newFee, 1, BigDecimal.ZERO, BigDecimal.ZERO, false, null);
                setLineItemNumber(newFee);
                newFee.setOrder(this.orderBean.getOrder());
                this.setCurrentLineItem(newFee);
        }
        
        /**
         * Sets the line number for the current item.
         */
        private void setLineItemNumber(LineItem updateItem) {
                updateItem.setLineNumber(orderBean.getOrder().getMaxLineNumber() + 1);
        }
        
        /**
         * Given a line item, returns the status that it will have when the changes are saved.
         * 
         * @param item
         * @return
         */
        public String getPendingLineItemStatus(LineItem item) {
                if (item == null) {
                        return "";
                }
                if (this.objectActionMap.containsKey(item)) {
                		if(this.objectActionMap.get(item).action.name() == WorkflowAction.CANCEL.name() && this.objectActionMap.get(item).quantity >= item.getQuantity()) {
                			return LineItemStatusEnum.CANCELED.name();
                		}
                		else if(this.objectActionMap.get(item).action.name() == WorkflowAction.CANCEL.name() && this.objectActionMap.get(item).quantity < item.getQuantity()) {
                			return LineItemStatusEnum.PARTIAL_CANCEL.name();
                		}
                		else if(this.objectActionMap.get(item).action.name() == WorkflowAction.RETURN.name() && this.objectActionMap.get(item).quantity >= item.getQuantity()) {
                			return LineItemStatusEnum.RETURNED.name();
                		}
                		else if(this.objectActionMap.get(item).action.name() == WorkflowAction.RETURN.name() && this.objectActionMap.get(item).quantity < item.getQuantity()) {
                			return LineItemStatusEnum.PARTIAL_RETURN.name();
                		}
                		else if(this.objectActionMap.get(item).action.name() == WorkflowAction.SHIP.name() && this.objectActionMap.get(item).quantity >= item.getQuantity()) {
                			return LineItemStatusEnum.FULLY_SHIPPED.name();
                		}
                		else if(this.objectActionMap.get(item).action.name() == WorkflowAction.SHIP.name() && this.objectActionMap.get(item).quantity < item.getQuantity()) {
                			return LineItemStatusEnum.PARTIAL_SHIP.name();
                		}
                		else {
                			return this.objectActionMap.get(item).action.name();
                		}
                }
                else if (item.getStatus() != null) {
                        return item.getStatus().getValue();
                }
                return "CREATED";
        }
        
        /**
         * Given a line item, returns the quantity that it will have when the changes are saved.
         * 
         * @param item
         * @return
         */
        public int getPendingLineItemQuantity(LineItem item) {
                if (item == null) {
                        return 0;
                }
                int toReturn = item.getQuantity();
                if (this.objectActionMap.containsKey(item) && 
                                (this.objectActionMap.get(item).action == WorkflowAction.CREATE || this.objectActionMap.get(item).action == WorkflowAction.UPDATE_ORDERED_QUANTITY)) {
                        toReturn = this.objectActionMap.get(item).quantity;
                }
                return toReturn;
        }
        
        public int getPendingLineItemShipped(LineItem item) {
        	if (item == null) {
        		return 0;
        	}
        	int toReturn = 0;
        	if (this.objectActionMap.containsKey(item) && (this.objectActionMap.get(item).action == WorkflowAction.SHIP)) {
        		toReturn = this.objectActionMap.get(item).quantity;
        	}
        	else {
        		toReturn = item.getQuantityShipped();
        	}
        	return toReturn;
        }
        
        public int getPendingLineItemReturned(LineItem item) {
        	if (item == null) {
        		return 0;
        	}
        	int toReturn = 0;
        	if (this.objectActionMap.containsKey(item) && (this.objectActionMap.get(item).action == WorkflowAction.RETURN)) {
        		toReturn = this.objectActionMap.get(item).quantity;
        	}
        	else {
        		toReturn = item.getQuantityReturned();
        	}
        	return toReturn;
        }
        
        public int getPendingLineItemCanceled(LineItem item) {
        	if (item == null) {
        		return 0;
        	}
        	int toReturn = 0;
        	if (this.objectActionMap.containsKey(item) && (this.objectActionMap.get(item).action == WorkflowAction.CANCEL)) {
        		toReturn = this.objectActionMap.get(item).quantity;
        	}
        	else {
        		toReturn = item.getQuantityCancelled();
        	}
        	return toReturn;
        }
        
        public BigDecimal getPendingLineItemShipping(LineItem item) {
			if (this.objectActionMap.containsKey(item) && WorkflowAction.SHIP.equals(this.objectActionMap.get(item).action)) {
				return this.objectActionMap.get(item).shipping;
			}
			else {
				return this.getRemainingShipping(item);
			}
        }
        
        private BigDecimal getRemainingShipping(LineItem item) {
        	if (item != null && item.getEstimatedShippingAmount() != null){
	        	BigDecimal totalRemainingShipping = item.getEstimatedShippingAmount();
	        	if(item.getPaidShipping() != null && item.getEstimatedShippingAmount().compareTo(item.getPaidShipping()) > 0) {
					totalRemainingShipping = totalRemainingShipping.subtract(item.getPaidShipping());
				}
	        	totalRemainingShipping.subtract(item.calculateTotalReturnShipping());
				return totalRemainingShipping;
        	}
			return BigDecimal.ZERO;
        }
        
        /**
         * Gets the current line item.
         * 
         * @return
         */
        public LineItem getCurrentLineItem() {
                return this.currentLineItem;
        }
        
        /**
         * Sets the current line item.
         * 
         * @param item
         */
        public void setCurrentLineItem(LineItem item) {
                clearLineItem();
                this.currentLineItem = item;
                this.currentPrice = currentLineItem.getUnitPrice();
                this.currentActionQuantity = currentLineItem.getQuantity();
                this.currentShipping = currentLineItem.getEstimatedShippingAmount();
                if (this.objectActionMap.containsKey(this.currentLineItem)) {
                        this.currentActionQuantity = this.objectActionMap.get(this.currentLineItem).quantity;
                        this.currentAction = this.objectActionMap.get(this.currentLineItem).action;
                        this.currentShipping = this.objectActionMap.get(this.currentLineItem).shipping;
                        if(this.currentLineItem.isFee()){
                        	this.currentPrice = this.objectActionMap.get(this.currentLineItem).feePrice;
                        }
                }
                if (this.currentLineItem.isFee()) {
                        this.currentActionQuantity = 1;
                }
        }
        
        /**
         * Gets the action for the current line item.
         * 
         * @return
         */
        public WorkflowAction getCurrentAction() {
                return this.currentAction;
        }
        
        /**
         * Sets the action for the current line item.
         * 
         * @param action
         */
        public void setCurrentAction(WorkflowAction action) {
                this.currentAction = action;
        }
        
        /**
         * Getter for the new shipping that will be set for the item.
         * 
         * @return
         */
        public BigDecimal getCurrentShipping() {
                return currentShipping;
        }

        /**
         * Setter for the new shipping that will be set for the item.
         * 
         * @param currentShipping
         */
        public void setCurrentShipping(BigDecimal currentShipping) {
                this.currentShipping = currentShipping;
        }

        /**
         * Getter for the quantity of the item that the action will be performed on.
         * 
         * @return
         */
        public int getCurrentActionQuantity() {
                return currentActionQuantity;
        }

        /**
         * Setter for the quantity of the item that the action will be performed on.
         * 
         * @param currentActionQuantity
         */
        public void setCurrentActionQuantity(int currentActionQuantity) {
                this.currentActionQuantity = currentActionQuantity;
        }
        
        /**
         * If one or more of the items have been shipped, then the price cannot be updated.
         * 
         * @return
         */
        public boolean isCurrentPriceUpdatable() {
        	if (this.currentLineItem == null) {
        		return false;
        	}
        	if (this.orderBean.getOrder() == null || this.orderBean.getOrder().getType() == null || this.orderBean.getOrder().getStatus() == null) {
        		return false;
        	}
        	if (this.orderBean.getOrder().getType().getValue().equals("ADHOC") && this.orderBean.getOrder().getStatus().getValue().equals(OrderStatus.OrderStatusEnum.PLACED.getValue())) {
        		return true;
        	}
        	if (this.currentLineItem != null && this.currentLineItem.getId() == null ) {
        		return true;
        	}
        	return false;
        }
        
        /**
         * Gets the current RA number for the return order.
         * 
         * @return
         */
        public String getRaNumber() {
                return this.raNumber;
        }
        
        /**
         * Sets the current RA number for the return order.
         * 
         * @param raNumber
         * @return
         */
        public void setRaNumber(String raNumber) {
                this.raNumber = raNumber;
        }
        
        /**
         * Gets the current number of boxes for the return order.
         * 
         * @return
         */
        public String getShipNumber() {
                return this.shipNumber;
        }
        
        /**
         * Sets the current number of boxes for the return order.
         * 
         * @param boxNumber
         * @return
         */
        public void setShipNumber(String shipNumber) {
                this.shipNumber = shipNumber;
        }

        /**
         * Sets the WorkflowAction for a quantity of an object.
         * 
         * @param action
         * @param target
         * @param quantity
         */
        private void setAction(WorkflowAction action, LineItem target, int quantity, BigDecimal shipping, BigDecimal price, boolean returnShipping, String reason) {
            PendingWorkflowAction newPendingAction = new PendingWorkflowAction(action, target, quantity, shipping, price, returnShipping, reason);
            objectActionMap.put(target, newPendingAction);
            if (action == WorkflowAction.SHIP) {
            	this.needsTrackingNumber = true;
            }
        }

    /**
     * @return the currentPrice
     */
    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    /**
     * @param currentPrice the currentPrice to set
     */
    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }
        
        /**
         * An enum listing the different possible workflow actions.
         * 
         * @author RHC
         *
         */
        public enum WorkflowAction {
                UPDATE_ORDERED_QUANTITY, SHIP, RETURN, CANCEL, REJECT, ACCEPT, 
                    RESEND, CREATE, UPDATE_FEE_PRICE, MARK_AS_RESOLVED, REFRESH_IN_WORKFLOW;
        }

        /**
         * Used to generate a selectOneMenu for the WorkflowActions.
         * 
         * @return
         */
    public SelectItem[] getAllowedWorkflowActions(LineItem item) {
    	List<WorkflowAction> actions = new ArrayList<>();
    	for (WorkflowAction action : WorkflowAction.values()) {
    		if (actionAllowed(item, action)) {
    			actions.add(action);
    		}
    	}
        SelectItem[] items = new SelectItem[actions.size()];
        int i = 0;
        for (WorkflowAction g : actions) {
            items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }
    
    /**
     * Returns true if the workflow action should be allowed, false otherwise
     * 
     * @param item
     * @param action
     * @return
     */
    private boolean actionAllowed(LineItem item, WorkflowAction action) {
    	if (item == null || action == null) {
    		return false;
    	}
    	if (WorkflowAction.REFRESH_IN_WORKFLOW.equals(action)) {
    		//refresh can only be applied to the entire order
    		return false;
    	}
    	if (!canEditPo() && !WorkflowAction.SHIP.equals(action) && !WorkflowAction.CANCEL.equals(action) && !WorkflowAction.RETURN.equals(action)) {
    		return false;
    	}
    	if (WorkflowAction.RESEND.equals(action) && item.getVendor() == null) {
    		return false;
    	}
    	if (item.getId() != null && WorkflowAction.CREATE.equals(action)) {
    		return false;
    	}
    	if (item.getId() == null && !WorkflowAction.CREATE.equals(action)) {
    		return false;
    	}
    	return true;
    }
    
    private boolean canEditPo() {
    	//TODO: fill in per PHOEN-3756
    	return true;
    }
        
    /**
     * Stores the action, target, and quantity that the action will be performed on.
     * 
     * @author RHC
     *
     */
        private class PendingWorkflowAction {
                private WorkflowAction action;
                private LineItem actionTarget;
                private int quantity;
                private BigDecimal shipping;
                private BigDecimal feePrice;
                private String reason;
				private boolean returnShipping;
                private PendingWorkflowAction(WorkflowAction action, LineItem actionTarget, int quantity, BigDecimal shipping, BigDecimal price, boolean returnShipping, String reason) {
                        this.action = action;
                        this.actionTarget = actionTarget;
                        this.quantity = quantity;
                        this.shipping = shipping;
                        this.feePrice = price;
                        this.reason = reason;
                        this.returnShipping = returnShipping;
                }
        }
        
        @Inject
        private UnitOfMeasureDao uomDao;
        
        /**
         * Returns all units of measure
         * 
         * @return
         */
        public List<UnitOfMeasure> getAllUoms() {
                return uomDao.findAll(UnitOfMeasure.class, 0, 0);
        }

        /**
         * returns a converter for units of measure.
         * 
         * @return
         */
    public Converter getUomConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return uomDao.findById(Long.valueOf(value), UnitOfMeasure.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((UnitOfMeasure) value).getId());
            }
        };
    }

		public CancelReason getCurrentCancelReason() {
			return currentCancelReason;
		}

		public void setCurrentCancelReason(CancelReason currentCancelReason) {
			this.currentCancelReason = currentCancelReason;
		}

		public ReturnReason getCurrentReturnReason() {
			return currentReturnReason;
		}

		public void setCurrentReturnReason(ReturnReason currentReturnReason) {
			this.currentReturnReason = currentReturnReason;
		}
		
		public CancelReason[] getCancelReasons() {
        	return CancelReason.values();
        }
        
        public ReturnReason[] getReturnReasons() {
        	return ReturnReason.values();
        }
        
        public void setActionCancel() {
        	this.currentAction = WorkflowAction.CANCEL;
        }
        
        public void setActionReturn() {
        	this.currentAction = WorkflowAction.RETURN;
        }
        
        public void setActionShip() {
        	this.currentAction = WorkflowAction.SHIP;
        }
        
        public void setActionRefresh() {
        	this.currentAction = WorkflowAction.REFRESH_IN_WORKFLOW;
        }
        
        public void clearForm() {
            JSFUtils.clearForm("editItemModalForm:actionInputGroup");
        }

		public boolean isNeedsTrackingNumber() {
			return needsTrackingNumber;
		}

		public void setNeedsTrackingNumber(boolean needsTrackingNumber) {
			this.needsTrackingNumber = needsTrackingNumber;
		}
	
	@Inject
	private ShipmentBp shipmentBp;
	
	public void shipNumberValidator(FacesContext context, UIComponent component, Object value) {
		try {
			String trackingNumber = (String) value;
			if (StringUtils.isNotBlank(trackingNumber)) {
				Shipment searchShipment = new Shipment();
				searchShipment.setTrackingNumber(trackingNumber.trim());
				if (shipmentBp.resultQuantity(searchShipment) > 0) {
					((UIInput) component).setValid(false);
			        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, 
			        		"Duplicate tracking number", "Duplicate tracking number"));
				}
			}
		}
		catch (Exception e) {
			LOGGER.warn("Error validating tracking number", e);
		}
	}
	
	public void raNumberValidator(FacesContext context, UIComponent component, Object value) {
		try {
			String raNumber = (String) value;
			for (ReturnOrder returnOrder : this.orderBean.getOrder().getReturnOrders()) {
				if (returnOrder.getRaNumber().equals(raNumber) && !ReturnOrderStatus.CLOSED.equals(returnOrder.getStatus())) {
					((UIInput) component).setValid(false);
			        context.addMessage(component.getClientId(), new FacesMessage(FacesMessage.SEVERITY_ERROR, 
			        		"Duplicate RA number", "Duplicate RA number"));
			        return;
				}
			}
		}
		catch (Exception e) {
			LOGGER.warn("Error validating RA number", e);
		}
	}
        
        @Inject
        private VendorBp vendorBp;
        
        public List<Vendor> getAllVendors() {
            List<Vendor> toReturn = new ArrayList<>();
            toReturn.addAll(vendorBp.findAll(Vendor.class, 0, 0));
            return toReturn;
        }
        
        public List<ItemCategory> getAllCategories() {
            List<ItemCategory> toReturn = new ArrayList<>();
            toReturn.addAll(itemCategoryBp.findAll(ItemCategory.class, 0, 0));
            return toReturn;       
        }
        
        private BigDecimal getTaxRate() {        	
        
        	String zip = this.orderBean.getOrder().getAddress().getZip();
            String county = this.orderBean.getOrder().getAddress().getCounty();
            String city = this.orderBean.getOrder().getAddress().getCity();
            String state = this.orderBean.getOrder().getAddress().getState();
            String geocode = this.orderBean.getOrder().getAddress().getGeoCode();
            
            BigDecimal taxRate = BigDecimal.ZERO;
            try {
            	TaxRate taxRateEntity = orderBp.getTaxRate(zip, county, city, state, geocode);
                taxRate = taxRateEntity.getCombinedSales();               
            } catch (com.apd.phoenix.service.business.CustomerOrderBp.NoTaxException e) {
            	
    		}                    
        	return taxRate;
        }   
        
    public Long getMaximumQuantity() {
    	if (this.currentLineItem == null || this.currentAction == null) {
    		return (long) 0;
    	}
        switch (this.currentAction) {
    	case RETURN: 
    		return (long) (this.currentLineItem.getQuantityShipped() - this.currentLineItem.getQuantityReturned());
    	case UPDATE_ORDERED_QUANTITY: 
    		if (this.currentLineItem.getQuantityShipped() == 0) {
    			return Long.MAX_VALUE;
    		}
    		else {
    			return (long) 0;
    		}
    	case CREATE:
    		return Long.MAX_VALUE;
    	case CANCEL: 
    		return (long) this.currentLineItem.getQuantityRemaining();
    	default:
    		return (long) this.currentLineItem.getQuantity();
        }
    }

	@Override
	public Converter getConverter() {
		//empty method for extending AbstractControllerBean
		return null;
	}

	public boolean getReturnShipping() {
		return returnShipping;
	}

	public void setReturnShipping(boolean returnShipping) {
		this.returnShipping = returnShipping;
	}
	
	public Comment getCurrentComment() {
		return currentComment;
	}

	@Lock(LockType.WRITE)
	public void setCurrentComment(Comment currentComment) {
		this.currentComment = currentComment;
	}
}
