/**
 * 
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.jbpm.task.Status;
import org.jbpm.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.workflow.UserTaskService;
import com.apd.phoenix.service.workflow.WorkflowService;

@Named
@Stateful
@SessionScoped
public class WorkflowTestBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowTestBean.class);

    private static final String WORKFLOW_TASKS = "/admin/customerservice/workflow/workflow_prototype?faces-redirect=true";

    private Long id;

    private String orderId;

    private String apdPo;

    private String vendorName;

    private String email;

    private String authority;

    @Inject
    private WorkflowService workflowService;

    @Inject
    private UserTaskService userTaskService;

    @Inject
    private CustomerOrderBp orderBp;

    @PostConstruct
    public void init() {

    }

    public void retrieve() {
        if (FacesContext.getCurrentInstance().isPostback()) {
            return;
        }
    }

    public String startWorkflow() {
        logger.info("Starting order workflow for orderId=" + orderId);
        long order = Long.parseLong(orderId);
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setId(order);
        workflowService.processOrder(customerOrder);
        return WORKFLOW_TASKS;
    }

    public String exitAllTasks() {
        List<Task> tasks = new ArrayList<>();
        for (Task task : tasks) {
            long id = task.getId();
            if (task.getTaskData().getStatus().equals(Status.Suspended)) {
                userTaskService.resumeTask(id, "Administrator");
            }

            Task temp = userTaskService.getTask(id);
            if (temp.getTaskData().getStatus().equals(Status.InProgress)
                    || temp.getTaskData().getStatus().equals(Status.Ready)
                    || temp.getTaskData().getStatus().equals(Status.Reserved)) {
                logger.info("Exiting task id=" + temp.getId());
                if (!userTaskService.exitTask(temp.getId())) {
                    logger.error("Error exiting task id=" + temp.getId());
                }
            }
        }
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String sendQuickAck() {
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String sendFullAck() {
        return WORKFLOW_TASKS;
    }

    public String testFullAck() {
        //        workflowService.testFullAck(this.apdPo);
        return WORKFLOW_TASKS;
    }

    public String testFullShipment() {
        //        workflowService.testFullShipment(this.apdPo);
        return WORKFLOW_TASKS;
    }

    public String testFullInvoice() {
        //        workflowService.testFullInvoice(this.apdPo);
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String sendVendorComFailure() {
        logger.error("This test functionality is deprecated.");
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String sendShipmentNotification() {
        logger.error("This test functionality is deprecated.");
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String sendInvoice() {
        logger.error("This test functionality is deprecated.");
        return WORKFLOW_TASKS;
    }

    @Deprecated
    public String cancelOrder() {
        logger.error("This test functionality is deprecated.");
        long order = Long.parseLong(orderId);
        this.workflowService.cancelOrder(orderBp.findById(order, CustomerOrder.class).getApdPo().getValue(), orderId);
        return WORKFLOW_TASKS;
    }

    public String testOrderRejectedShipTo() {
        //        workflowService.testOrderRejectedShipTo(order, email, authority);
        return WORKFLOW_TASKS;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getApdPo() {
        return apdPo;
    }

    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}