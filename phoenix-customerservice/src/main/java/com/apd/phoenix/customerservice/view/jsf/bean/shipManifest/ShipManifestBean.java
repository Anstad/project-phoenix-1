package com.apd.phoenix.customerservice.view.jsf.bean.shipManifest;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;

@Named
@Stateless
public class ShipManifestBean {

    @Inject
    ShipManifestService shipmanifestService;

    private String error = "";

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public String aggregateAndUpload() {
        error = "";
        if (shipmanifestService.sendManifestToJumptrack()) {
            return "jumptrackConfirmed.xhtml";
        }
        error = "An error occured when starting the upload process.";
        return "jumptrack.xhtml";
    }

    public String sendStopNoticeRequest() {
        error = "";
        if (shipmanifestService.sendStopNotificationRequest()) {
            return "jumptrackConfirmed.xhtml";
        }
        error = "An error occured when starting the upload process.";
        return "jumptrack.xhtml";
    }

    public String verifyUsscoManifests() {
        error = "";
        if (shipmanifestService.verifyUsscoManifests()) {
            return "jumptrackConfirmed.xhtml";
        }
        error = "An error occured when starting the upload process.";
        return "jumptrack.xhtml";
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
