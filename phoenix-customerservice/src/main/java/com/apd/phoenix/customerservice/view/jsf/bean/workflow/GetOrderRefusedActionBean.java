/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.GetOrderRefusedActionBean;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.OrderHumanTaskFormBean;

@Named
@Stateful
@RequestScoped
public class GetOrderRefusedActionBean extends OrderHumanTaskFormBean {

    private static final Logger LOG = LoggerFactory.getLogger(GetOrderRefusedActionBean.class);

    @Override
    public void init() {
    }

    public void retrieve() {
        LOG.info("retrieve called");
    }

    public String cancel() {
        return completeTask(true);
    }

    public String retry() {
        return completeTask(false);
    }

    public String completeTask(boolean cancelOrder) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("cancelOrder", cancelOrder);
        return this.completeOrderTask(params);
    }
}