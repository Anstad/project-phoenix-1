package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.BodyPart;
import javax.mail.Part;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.core.PageNumberDisplay;
import com.apd.phoenix.customerservice.view.jsf.bean.ServiceLogBean;
import com.apd.phoenix.customerservice.view.jsf.bean.ViewUtils;
import com.apd.phoenix.customerservice.view.jsf.bean.login.LoginBean;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerCreditInvoiceBp;
import com.apd.phoenix.service.business.CustomerInvoiceBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.InvoiceBp;
import com.apd.phoenix.service.business.MessageMetadataBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.business.VendorInvoiceBp;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.AccountXAccountPropertyType;
import com.apd.phoenix.service.model.CanceledQuantity;
import com.apd.phoenix.service.model.ChangeOrderLog;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ExceptionOrderLog;
import com.apd.phoenix.service.model.Invoice;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemLog;
import com.apd.phoenix.service.model.LineItemXReturn;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MessageMetadata.CommunicationType;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.NotificationProperties;
import com.apd.phoenix.service.model.OrderAttachment;
import com.apd.phoenix.service.model.OrderLog;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.OrderStatus;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.ReturnOrder.ReturnOrderStatus;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.utility.FileDownloader;
import com.apd.phoenix.service.workflow.PhoenixTaskSummary;
import com.apd.phoenix.service.workflow.UserTaskService;
import com.apd.phoenix.service.workflow.WorkflowService;
import com.apd.phoenix.web.FileUploadBean;

@Named
@Stateful
@ConversationScoped
@Lock(LockType.READ)
@AccessTimeout(unit = TimeUnit.SECONDS, value = 30)
public class OrderDetailsBean {
        private static final String CSR_VIEW_MESSAGE_PERMISSION_PREFIX = "customer service view message ";
		private static final String CSR_VIEW_ALL_MESSAGES_PERMISSION = "customer service view all messages";
		private static final String TECH_SUPPORT = "Tech Support";
		private static final String ALLOW_PO_EDITING = "allow PO editing";
		private static final Logger logger = LoggerFactory.getLogger(OrderDetailsBean.class);
	private static final int CC_DIGITS_TO_SHOW = 4;
	
	@Inject
	private CustomerOrderBp orderBp;

	@Inject
	private ServiceLogBean serviceLogBean;
	@Inject
	private LoginBean loginBean;	
	@Inject
	private FileUploadBean fileUploadBean;
	
	private CustomerOrder order = new CustomerOrder();
	private Map<OrderLog, String> recipientsMap = new HashMap<>();
	private List<Comment> orderComments = new ArrayList<>();
	private List<OrderLog> orderNotifications = new ArrayList<>();
	private PageNumberDisplay commentsPagination = new PageNumberDisplay(0);
	private PageNumberDisplay serviceLogsPagination = new PageNumberDisplay(0);
	private PageNumberDisplay notificationsPagination = new PageNumberDisplay(0);
	private Comment currentComment = new Comment();
	private OrderLog currentNotification = new OrderLog();
	@Inject
	private MessageService messageService;
	@Inject
	private MessageUtils messageUtils;
	@Inject
	private MessageMetadataBp messageMetadataBp;
	
	@Inject
	private SecurityContext securityContext;
	
	@Inject
	private EmailFactoryBp emailFactoryBp;
	private OrderLog newNotification = new OrderLog();
	
	private String resendEmailRecipient;
        
    @Inject
    private VendorInvoiceBp vendorInvoiceBp;
    
    @Inject
    private CustomerInvoiceBp customerInvoiceBp;
    
    private boolean resendEmailIncludeOriginalCC = false;
    
    private boolean resendEmailIncludeOriginalBCC = false;
    
	private String messageContent;
	private MessageType selectedMessageType;
	private String messageDestination;
	private CommunicationType selectedCommunicationType;
	private EventType selectedEventType;
	@Inject
	private CustomerOrderBp customerOrderBp;
    
    @Inject
    private OrderLogBp orderLogBp;
    
	private Shipment shipment;
	private List<DisplayItem> itemsToDisplay = new ArrayList<>();
	
    private List<PhoenixTaskSummary> tasks;
    
    @Inject
    private UserTaskService taskService;
	
    private Map<LineItemXReturn, ReturnOrder> returnMap = new HashMap<>();
    @Inject
    private WorkflowService workflowService;
    @Inject
    private CommentBp commentBp;
    private boolean showAddComment = false;
    
    @Inject
    private SystemUserBp systemUserBp;
    
    @Inject
    OrderStatusBp orderStatusBp;
    
    @Inject
    CustomerCreditInvoiceBp customerCreditInvoiceBp;
    
    @Inject
    InvoiceBp invoiceBp;
    
    private String user;
    
    private String bulkUpdateApdPos;
    
    private String bulkUpdateInvoiceNumbers;
	
    private ResendType resendEdiType;
	
	private List<String> resendEdiDropdownNumbers = new ArrayList<>();
	private String selectedResendDropdownNumber;
	private ArrayList<String> errorMessages;

	public CustomerOrder getOrder() {
		return order;
	}
	
	 @PostConstruct
	 @Lock(LockType.WRITE)
	 public void init() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        user = context.getUserPrincipal().getName();
	 }

	@Lock(LockType.WRITE)
	public void setOrder(CustomerOrder enteredOrder) {
		this.order = orderBp.hydrateForOrderDetails(enteredOrder);
		this.order.refreshOrderTotal();
		this.order.getUser().getPerson().toString();
		orderComments = new ArrayList<>();
		orderComments.addAll(this.order.getComments());
		Collections.sort(orderComments, new CommentComparator());
		Map<EventType, String> eventMap = new HashMap<>();
		for (NotificationProperties notification : this.order.getNotificationProperties()) {
			eventMap.put(notification.getEventType(), notification.getRecipients());
		}
		orderNotifications = new ArrayList<>();
		recipientsMap = new HashMap<>();
		for (OrderLog log : this.order.getOrderLogs()) {
			if (log.getMessageMetadata() != null) {
				orderNotifications.add(log);
				recipientsMap.put(log, eventMap.get(log.getEventType()));
			}
		}
		Collections.sort(orderNotifications, new OrderLog.OrderLogComparator());
		commentsPagination = new PageNumberDisplay(orderComments.size());
		serviceLogsPagination = new PageNumberDisplay(this.serviceLogBean.getServiceLogs().size());
		notificationsPagination = new PageNumberDisplay(orderNotifications.size());
		List<LineItem> orderedItems = ViewUtils.asList(this.order.getItems());
		this.itemsToDisplay = new ArrayList<DisplayItem>();
		returnMap = new HashMap<>();
		if (this.order.getReturnOrders() != null) {
			for (ReturnOrder returnOrder : this.order.getReturnOrders()) {
				if (returnOrder.getItems() != null && !ReturnOrderStatus.CLOSED.equals(returnOrder.getStatus())) {
					for (LineItemXReturn itemXReturn : returnOrder.getItems()) {
						returnMap.put(itemXReturn, returnOrder);
					}
				}
			}
		}
		for (LineItem item : orderedItems) {
			DisplayItem toAdd = new DisplayItem();
			toAdd.setItem(item);
			for (LineItemXShipment shipment : item.getItemXShipments()) {
				toAdd.setQuantityShipped(toAdd.getQuantityShipped().add(shipment.getQuantity()));
			}
			this.itemsToDisplay.add(toAdd);
			for (CanceledQuantity cancelled : item.getCanceledQuantities()) {
				DisplayItem returnToAdd = new DisplayItem();
				returnToAdd.setIsCancel(true);
				returnToAdd.setItem(item);
				returnToAdd.setCancelledQuantity(cancelled);
				returnToAdd.setQuantityShipped(cancelled.getQuantity());
				this.itemsToDisplay.add(returnToAdd);
			}
			for (LineItemXReturn itemXReturn : item.getItemXReturns()) {
				if (returnMap.get(itemXReturn) != null && !ReturnOrderStatus.CLOSED.equals(returnMap.get(itemXReturn).getStatus())) {
					DisplayItem returnToAdd = new DisplayItem();
					returnToAdd.setIsReturn(true);
					returnToAdd.setItem(item);
					returnToAdd.setItemXReturn(itemXReturn);
					returnToAdd.setQuantityShipped(itemXReturn.getQuantity());
					this.itemsToDisplay.add(returnToAdd);
				}
			}
		}
    	logger.info("getting tasks!");
    	tasks = new ArrayList<>();
    	tasks.addAll(taskService.getOrderTasks(order));
	}
	
	public List<Comment> getOrderComments() {
		return this.orderComments.subList(
				commentsPagination.getSize() * (commentsPagination.getPage() - 1), 
				Math.min(orderComments.size(), commentsPagination.getSize() * commentsPagination.getPage()));
	}
	
	public List<OrderLog> getOrderNotifications() {
		return this.orderNotifications.subList(
				notificationsPagination.getSize() * (notificationsPagination.getPage() - 1), 
				Math.min(orderNotifications.size(), notificationsPagination.getSize() * notificationsPagination.getPage()));
	}
	
	public boolean isOutboundNotification(OrderLog notification) {
		return CommunicationType.OUTBOUND.equals(notification.getMessageMetadata().getCommunicationType());
	}
	
	//Do not allow resend notification for EDI from order details page there is a separate tab for that functionality	
	public boolean isNotEdi(OrderLog notification) {
		return !MessageType.EDI.equals(notification.getMessageMetadata().getMessageType());
	}
	
	public Map<OrderLog, String> getRecipientsMap() {
		return this.recipientsMap;
	}
	
	public PageNumberDisplay getCommentsPagination() {
		return this.commentsPagination;
	}
	
	public PageNumberDisplay getServiceLogsPagination() {
		return this.serviceLogsPagination;
	}
	
	public PageNumberDisplay getNotificationsPagination() {
		return this.notificationsPagination;
	}
	
	public Comment getCurrentComment() {
		return currentComment;
	}

	@Lock(LockType.WRITE)
	public void setCurrentComment(Comment currentComment) {
		this.currentComment = currentComment;
	}

	@Lock(LockType.WRITE)
	public void addCurrentComment() {
		if (this.currentComment.getId() == null) {
			this.currentComment.setCommentDate(new Date());
			this.currentComment.setUser(loginBean.getSystemUser());
			this.order.getComments().add(this.currentComment);
			this.setOrder(this.commentBp.addComment(order, currentComment));
		}
		this.setCurrentComment(new Comment());
		this.showAddComment = false;
	}

	@Lock(LockType.WRITE)
	public void discardCurrentComment() {
		this.showAddComment = false;
		this.setCurrentComment(new Comment());
	}

	public OrderLog getCurrentNotification() {
		return currentNotification;
	}

	@Lock(LockType.WRITE)
	public void setCurrentNotification(OrderLog currentNotification) {
		this.currentNotification = currentNotification;
	}

	@Lock(LockType.WRITE)
	public void discardCurrentNotification() {
		this.setCurrentNotification(new OrderLog());
	}

	@Lock(LockType.WRITE)
	public void sendCurrentNotification() {
		Message originalMessage = messageUtils.retrieveMessage(currentNotification.getMessageMetadata());
		Message messageCopy = null;
		if (currentNotification.getMessageMetadata().getMessageType().equals(MessageType.EMAIL)) {
			messageCopy = emailFactoryBp.createOrderEmailToResend(originalMessage, this.resendEmailRecipient, this.resendEmailIncludeOriginalCC, this.resendEmailIncludeOriginalBCC);
	        messageService.sendMessage(messageCopy, currentNotification.getEventType());
	        MessageMetadata messageMetadata = this.messageMetadataBp.findById(this.currentNotification.getMessageMetadata().getId(), MessageMetadata.class);
	        messageMetadata.setFilePath(messageCopy.getMetadata().getFilePath());
	        this.messageMetadataBp.update(messageMetadata);
        }
		else {
			logger.warn("Trying to resend a non-email message");
		}
		this.discardCurrentNotification();
		this.setOrder(this.orderBp.findById(this.order.getId(), CustomerOrder.class));
	}
	
	public boolean isCurrentMessageText() {
		MessageMetadata metadata = currentNotification.getMessageMetadata();
		if(metadata != null && metadata.getContentType() != null && !metadata.getContentType().contains("text")) {
			return false;
		}
		return true;
	}
	
	public String getCurrentMessage() {
		MessageMetadata metadata = currentNotification.getMessageMetadata();
		try {
			if(!isCurrentMessageText()) {
				metadata.setContentUrl(true);
			}
			Message message = messageUtils.retrieveMessage(metadata);
			if(!metadata.isContentUrl()) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(message.getContent()));
				StringBuilder resultsBuilder = new StringBuilder();
				String line = null;
				while((line = bufferedReader.readLine()) != null) {
				    resultsBuilder.append(line);
				}
				return resultsBuilder.toString();
			}
			return message.getMetadata().getPresignedUrl().toString();
		} catch (Exception e) {
			return "Message not found";
		}
	}
	
	
	public String getCardString() {
		return this.order.getPaymentInformation().getCard().getIdentifier();
	}

	@Lock(LockType.WRITE)
	public void clearNotificationInput() {
		messageContent = "";
		messageDestination = "";
		selectedCommunicationType = CommunicationType.INBOUND;
		selectedMessageType = MessageType.EMAIL;
	}

    public MessageType[] getMessageTypes() {
        return MessageType.values();
    }

	public OrderLog getNewNotification() {
		return newNotification;
	}

	public void setNewNotification(OrderLog newNotification) {
		this.newNotification = newNotification;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public MessageType getSelectedMessageType() {
		return selectedMessageType;
	}

	public void setSelectedMessageType(MessageType selectedMessageType) {
		this.selectedMessageType = selectedMessageType;
	}

	public String getMessageDestination() {
		return messageDestination;
	}

	public void setMessageDestination(String messageDestination) {
		this.messageDestination = messageDestination;
	}

	public String getResendEmailRecipient() {
		return resendEmailRecipient;
	}

	public void setResendEmailRecipient(String resendEmailRecipient) {
		this.resendEmailRecipient = resendEmailRecipient;
	}

	public Boolean getResendEmailIncludeOriginalCC() {
		return resendEmailIncludeOriginalCC;
	}

	public void setResendEmailIncludeOriginalCC(Boolean resendEmailIncludeOriginalCC) {
		this.resendEmailIncludeOriginalCC = resendEmailIncludeOriginalCC;
	}

	public Boolean getResendEmailIncludeOriginalBCC() {
		return resendEmailIncludeOriginalBCC;
	}

	public void setResendEmailIncludeOriginalBCC(
			Boolean resendEmailIncludeOriginalBCC) {
		this.resendEmailIncludeOriginalBCC = resendEmailIncludeOriginalBCC;
	}

	public CommunicationType getSelectedCommunicationType() {
		return selectedCommunicationType;
	}

	public void setSelectedCommunicationType(CommunicationType selectedCommunicationType) {
		this.selectedCommunicationType = selectedCommunicationType;
	}

	public EventType getSelectedEventType() {
		return selectedEventType;
	}

	public void setSelectedEventType(EventType selectedEventType) {
		this.selectedEventType = selectedEventType;
	}

	public static int getCcDigitsToShow() {
		return CC_DIGITS_TO_SHOW;
	}

	public void setRecipientsMap(Map<OrderLog, String> recipientsMap) {
		this.recipientsMap = recipientsMap;
	}

	public void setOrderComments(List<Comment> orderComments) {
		this.orderComments = orderComments;
	}

	public void setOrderNotifications(List<OrderLog> orderNotifications) {
		this.orderNotifications = orderNotifications;
	}

	public void setCommentsPagination(PageNumberDisplay commentsPagination) {
		this.commentsPagination = commentsPagination;
	}

	public void setNotificationsPagination(PageNumberDisplay notificationsPagination) {
		this.notificationsPagination = notificationsPagination;
	}

    public CommunicationType[] getCommunicationTypes() {
        return CommunicationType.values();
    }

	public Shipment getShipment() {
		return shipment;
	}

	public void setShipment(Shipment shipment) {
		this.shipment = shipment;
	}
	
	public Set<LineItemXShipment> getShipmentItems() {
		if (this.shipment == null) {
			return new HashSet<>();
		}
		return this.shipment.getItemXShipments();
	}

	public List<DisplayItem> getItemsToDisplay() {
		return itemsToDisplay;
	}

	public void setItemsToDisplay(List<DisplayItem> itemsToDisplay) {
		this.itemsToDisplay = itemsToDisplay;
	}

	public Map<LineItemXReturn, ReturnOrder> getReturnMap() {
		return returnMap;
	}

	public void setReturnMap(Map<LineItemXReturn, ReturnOrder> returnMap) {
		this.returnMap = returnMap;
	}
	
	public String getBulkUpdateApdPos() {
		return this.bulkUpdateApdPos;
	}
	
	public void setBulkUpdateApdPos(String bulkUpdateApdPos) {
		this.bulkUpdateApdPos = bulkUpdateApdPos;
	}

	@Lock(LockType.WRITE)
	public String submitBulkPos() {
		String[] pos = this.bulkUpdateApdPos.split(",");
		for (String po : pos) {
			this.setOrder(this.customerOrderBp.searchByApdPo(po));
			this.reprocessOrder();
		}
		return null;
	}

	@Lock(LockType.WRITE)
	public String resendBulkPoAcks() {
		String[] pos = this.bulkUpdateApdPos.split(",");
		for (String po : pos) {
			CustomerOrder customerOrder = customerOrderBp.searchByApdPo(po);
			POAcknowledgementDto poAcknowledgementDto = customerOrderBp.createFullAcknowledgement(customerOrder);
			poAcknowledgementDto = customerOrderBp.populateOrderAcknowledgement(customerOrder,
					poAcknowledgementDto);
			Credential credential = customerOrder.getCredential();
	    	messageService.sendPurchaseOrderAcknowledgment(poAcknowledgementDto, credential.getPoAcknowledgementMessageType());
		}
		return null;
	}
	
	@Lock(LockType.WRITE)
	public String resendBulkInvoices() {
		String[] invoiceNumbers = this.bulkUpdateInvoiceNumbers.split(",");
		errorMessages  = new ArrayList<String>();
		for (String invoiceNumber : invoiceNumbers) {
			List<Invoice> invoices = invoiceBp.findCustomerInvoiceByInvoiceNumber(invoiceNumber);
			if(invoices == null || invoices.isEmpty()){
				errorMessages.add("No invoice found for invoice number " + invoiceNumber);
			}
			else if(invoices.size() == 1){
				Invoice invoice = invoices.get(0);
				String errorMessage = sendInvoice(invoice);
				if(!StringUtils.isEmpty(errorMessage)){
					errorMessages.add(errorMessage);
				}
			} else {
				//multiple invoices found
				Invoice validInvoice = null;
				boolean foundMultipleCustomerInvoices = false;
				for(Invoice invoice:invoices){
					if(invoice instanceof CustomerCreditInvoice || invoice instanceof CustomerInvoice){
						if(validInvoice == null){
							validInvoice = invoice;
						}else{
							errorMessages.add("Multiple invoice found for invoice number " + invoiceNumber);
							foundMultipleCustomerInvoices = true;
						}
					}
				}
				if(validInvoice == null){
					errorMessages.add("No invoice found for invoice number " + invoiceNumber);
				}else{
					if(!foundMultipleCustomerInvoices){
						String errorMessage = sendInvoice(validInvoice);
						if(!StringUtils.isEmpty(errorMessage)){
							errorMessages.add(errorMessage);
						}
					}
				}
			}
		}
		if(!errorMessages.isEmpty()){
			logger.error("The following errros occurred " + errorMessages.toString());
		}
		return null;
	}
	
	public String displayBulkInvoiceErrorMessages(){
		if(errorMessages == null || StringUtils.isEmpty(bulkUpdateInvoiceNumbers)){
			return "";
		}
		if(errorMessages.isEmpty()){
			return "The following invoices were resent sucessfully: " + bulkUpdateInvoiceNumbers;
		}
		String returnMessage = "The following errors occurred: ";
		for(String errorMessage:errorMessages){
			returnMessage = returnMessage + errorMessage + "; ";
		}
		return returnMessage;
	}

	private String sendInvoice(Invoice invoice) {
		if(invoice instanceof CustomerCreditInvoice){
			sendCreditInvoice((CustomerCreditInvoice) invoice);
		} else if(invoice instanceof CustomerInvoice){
			sendDebitInvoice((CustomerInvoice) invoice);
		} else{
			return "Invoice with number " + invoice.getInvoiceNumber() + " is not a cusomer invoice";
		}
		return null;
	}

	private void sendDebitInvoice(CustomerInvoice invoice) {
		CustomerOrder invoiceOrder = invoice.getShipment().getOrder();
		Credential credential = invoiceOrder.getCredential();
		MessageType invoiceMessageType = credential.getInvoiceMessageType();
		switch (invoiceMessageType) {
			case EDI:
				resendEDIInvoice(invoice, invoiceOrder);
				break;
			case EMAIL:
				try {
					messageService.sendInvoice(DtoFactory.createCustomerInvoiceDto(invoice), MessageType.EMAIL, Boolean.TRUE);
				} catch (ParsingException e) {
					logger.error("Unexpectd dto parse error for invoice number" + invoice.getInvoiceNumber());
				}
				break;
			default:
				logger.error("Message type " + invoiceMessageType.toString() + " not supported for resend");
			break;
		}
		
	}

	private void sendCreditInvoice(CustomerCreditInvoice invoice) {
		CustomerOrder invoiceOrder = invoice.getReturnOrder().getOrder();
		Credential credential = invoiceOrder.getCredential();
		MessageType invoiceMessageType = credential.getCreditInvoiceMessageType();
		switch (invoiceMessageType) {
			case EDI:
				resendEDICreditInvoice(invoice);
				break;
			case EMAIL:
				try {
					messageService.sendCreditInvoice(DtoFactory.createCustomerCreditInvoiceDto(invoice), MessageType.EMAIL, Boolean.TRUE);
				} catch (ParsingException e) {
					logger.error("Unexpectd dto parse error for invoice number" + invoice.getInvoiceNumber());
				}
				break;
			default:
				logger.error("Message type " + invoiceMessageType.toString() + " not supported for credit invoice resend");
			break;
		}
		
	}

	private class CommentComparator implements Comparator<Object> {

	    @Override
	    public int compare(Object arg0, Object arg1) {
	    	if (!(arg1 instanceof Comment)) {
	    		return -1;
	    	}
	    	if (!(arg0 instanceof Comment)) {
	    		return 1;
	    	}
	    	Comment comment0 = (Comment) arg0;
	    	Comment comment1 = (Comment) arg1;
	    	if (comment0.getCommentDate().before(comment1.getCommentDate())) {
	    		return -1;
	    	}
	    	if (comment0.getCommentDate().after(comment1.getCommentDate())) {
	    		return 1;
	    	}
	    	return 0;
	    }
	}
	
	public class DisplayItem {
		private boolean isReturn = false;
		private boolean isCancel = false;
		private LineItem item;
		private LineItemXReturn itemXReturn;
		private CanceledQuantity cancelledQuantity;
		private BigInteger quantityShipped = BigInteger.ZERO;
		
		public boolean getIsReturn() {
			return isReturn;
		}
		public void setIsReturn(boolean isReturn) {
			this.isReturn = isReturn;
		}
		public boolean getIsCancel() {
			return isCancel;
		}
		public void setIsCancel(boolean isCancel) {
			this.isCancel = isCancel;
		}
		public boolean getIsReturnOrCancel() {
			return isReturn || isCancel;
		}
		public LineItem getItem() {
			return item;
		}
		public void setItem(LineItem item) {
			this.item = item;
		}
		public LineItemXReturn getItemXReturn() {
			return itemXReturn;
		}
		public void setItemXReturn(LineItemXReturn itemXReturn) {
			this.itemXReturn = itemXReturn;
		}
		public CanceledQuantity getCancelledQuantity() {
			return cancelledQuantity;
		}
		public void setCancelledQuantity(CanceledQuantity cancelledQuantity) {
			this.cancelledQuantity = cancelledQuantity;
		}
		public BigInteger getQuantityShipped() {
			return quantityShipped;
		}
		public void setQuantityShipped(BigInteger quantityShipped) {
			this.quantityShipped = quantityShipped;
		}
	}
	
	/**
	 * This method takes and OrderLog, and returns a String for how the log should be displayed.
	 * 
	 * @param log
	 * @return
	 */
	public String logToString(OrderLog log) {
		if (log == null) {
			return "";
		}
		else if (log instanceof LineItemLog) {
			LineItemLog itemLog = (LineItemLog) log;
			for (LineItem item : this.order.getItems()) {
				if (item.getLogs() != null && item.getLogs().contains(itemLog) && item.getLineNumber() != null && itemLog.getLineItemStatus() != null) {
					String note = ((LineItemLog) log).getNote();
					return "Line " + item.getLineNumber() + " was " + itemLog.getLineItemStatus().getValue() + (StringUtils.isEmpty(note) ? "" : "-" + note) + " on " + log.getUpdateTimestamp();
				}
			}
			return "";
		}
		else if (log instanceof CreditCardTransactionLog && !((CreditCardTransactionLog) log).isTransactionSuccess() && StringUtils.isNotBlank(((CreditCardTransactionLog) log).getFailureReason())) {
			return (log.getEventType() != null ? log.getEventType().name() : log.getOrderStatus().getValue()) + " on " + log.getUpdateTimestamp() + " (failed due to \"" + ((CreditCardTransactionLog) log).getFailureReason() + "\")";
		}
		else if (log instanceof ChangeOrderLog){
			ChangeOrderLog changeLog = (ChangeOrderLog) log;
			if(StringUtils.isEmpty(changeLog.getDescription())){
				    logger.error("Unexpected null on change order log for order: " + order.getApdPo().getValue());
				return "";
			}
			if(StringUtils.isEmpty(changeLog.getChangingUser())){
				return "The system automatically " + changeLog.getDescription() + " on " + log.getUpdateTimestamp();
			}
			return "User, " + changeLog.getChangingUser() + ", performed action " + changeLog.getDescription() + " on " + log.getUpdateTimestamp();
		}
		else if (log instanceof ExceptionOrderLog) {
			ExceptionOrderLog exceptionLog = (ExceptionOrderLog) log;
			if (StringUtils.isNotBlank(exceptionLog.getDescription())) {
				return "Workflow exception (" + exceptionLog.getDescription() + ") on " + log.getUpdateTimestamp();
			}
			return "Workflow exception on " + log.getUpdateTimestamp();
		}
		else {
			//default, just says the status and the date
			if ((log.getOrderStatus() == null && log.getEventType() == null) || log.getUpdateTimestamp() == null) {
				return "";
			}
			return (log.getEventType() != null ? log.getEventType().name() : log.getOrderStatus().getValue()) 
					+ " on " + log.getUpdateTimestamp();
		}
	}

	@Lock(LockType.WRITE)
	public String downloadMessageAttachment(OrderLog notification) {

		Message message = messageUtils.retrieveMessage(notification.getMessageMetadata());
		
        try (InputStream messageContent = message.getContent()) {
            MimeMessage emailMessage = new MimeMessage(null, messageContent);
			MimeMultipart mmp = (MimeMultipart)emailMessage.getContent();
			for (int i = 0; i < mmp.getCount(); i++) {
				BodyPart bodyPart = mmp.getBodyPart(i);
				if (Part.ATTACHMENT.toString().equals(bodyPart.getDisposition())) {
			        try (InputStream stream = bodyPart.getInputStream()) {
			        	return FileDownloader.downloadFile(stream, bodyPart.getFileName());
			        } catch (IOException e) {
			        	//do nothing
					}
				}
			}
        }
        catch (Exception e) {
        	//do nothing
        }
        try (InputStream stream = message.getContent()) {
        	return FileDownloader.downloadFile(stream, "message.txt");
        } catch (IOException e) {
        	//do nothing
        	return null;
		}
    }
    
    /**
     * Takes an order notification, and determines whether the "Download Attachment" button should be shown.
     * 
     * @param notification
     * @return
     */
    public boolean shouldShowDownloadAttachment(OrderLog notification) {
    	if (notification == null || notification.getMessageMetadata() == null) {
    		return false;
    	}
    	return MessageType.EMAIL.equals(notification.getMessageMetadata().getMessageType());
    }
    
    public String getAttachmentName(OrderLog notification) {
    	
    	if (this.shouldShowDownloadAttachment(notification)) {
    		if (StringUtils.isNotBlank(notification.getMessageMetadata().getAttachmentName())) {
    			return notification.getMessageMetadata().getAttachmentName();
    		} else {
    			return "[attachment name not found]";
    		}
    	}
    	return "[no attachment]";
    }

    /**
     * Takes an order notification, and determines whether the "View Raw Message" button should be shown.
     * 
     * @param notification
     * @return
     */
    public boolean shouldShowViewRawMessage(OrderLog notification) {
    	return !this.shouldShowDownloadAttachment(notification);
    }
    
    

	@Lock(LockType.WRITE)
    public void reprocessOrder() {
    	if (this.canResubmitOrder()) {
    			this.order = customerOrderBp.addLog(this.order, orderLogBp.logChangeOrder(this.order, " resubmitted to workflow ", user));
    			this.order.setOrderDate(new Date());
    			this.customerOrderBp.setOrderStatus(this.order, this.orderStatusBp.getOrderStatus(OrderStatusEnum.ORDERED));
    			this.workflowService.processOrder(this.customerOrderBp.findById(this.order.getId(), CustomerOrder.class));
    			this.setOrder(this.order);
    	}    		
    }
    
    public boolean isInWorkflowLimbo() {
    	return this.orderBp.isInWorkflowLimbo(this.order);
    }
    
    /**
     * Determines whether the user can resubmit the order
     * 
     * @return
     */
    public boolean canResubmitOrder() {
    	Calendar c = Calendar.getInstance();
    	c.setTime(new Date());
    	c.add(Calendar.MINUTE, -30);
    	return this.isTechSupport() && this.order != null && this.order.getOrderDate() != null 
    			&& this.order.getOrderDate().before(c.getTime()) && this.isInWorkflowLimbo();
    }
    
    public List<OrderLog> getOrderHistoryList() {
    	List<OrderLog> toReturn = new ArrayList<>();
    	toReturn.addAll(this.order.getOrderLogs());
    	Collections.sort(toReturn, new OrderLog.OrderLogComparator());
    	return toReturn;
    }

	public boolean isShowAddComment() {
		return showAddComment;
	}

	public void setShowAddComment(boolean showAddComment) {
		this.showAddComment = showAddComment;
	}
	
	public BigDecimal newBigDecimal(int value) {
		return new BigDecimal(value);
	}
	
	public String getOrderPhoneNumber() {
		if (this.order.getAddress() != null && this.order.getAddress().getMiscShipTo() != null 
				&& StringUtils.isNotBlank(this.order.getAddress().getMiscShipTo().getRequesterPhone())) {
			return this.order.getAddress().getMiscShipTo().getRequesterPhone();
		}
		for (PhoneNumber number : this.order.getUser().getPerson().getPhoneNumbers()) {
			return number.getValue();
		}
		return "no phone number";
	}
	
	public String getOrderEmail() {
		if (this.order.getUser() == null || this.order.getUser().getPerson() == null 
				|| StringUtils.isBlank(this.order.getUser().getPerson().getEmail())) {
			return "";
		}
		String[] emails = this.order.getUser().getPerson().getEmail().split(",");
		StringBuilder stringBuilder = new StringBuilder();
		for (String email : emails) {
			if (stringBuilder.length() != 0) {
				stringBuilder.append(", ");
			}
			stringBuilder.append(email);
		}
		return stringBuilder.toString();
	}
	
	public String getAccountingId() {
		for (AccountXAccountPropertyType property : this.order.getAccount().getProperties()) {
			if (property.getType().getName().equals("Accounting ID") && StringUtils.isNotBlank(property.getValue())) {
				return property.getValue();
			}
		}
		return this.order.getAccount().getSolomonCustomerId();
	}
	
	public boolean canViewNotification(OrderLog notification) {
		return securityContext.hasPermission(CSR_VIEW_ALL_MESSAGES_PERMISSION) || securityContext.hasPermission(CSR_VIEW_MESSAGE_PERMISSION_PREFIX + notification.getEventType().getLabel());
	}
        
        public List<String> getVendorInvoiceNumbers(){
            List<String> invoiceNumbers = new ArrayList<>();
            for(Invoice invoice: vendorInvoiceBp.findInvoicesByOrder(order)) {
            	BigDecimal amount = BigDecimal.ZERO;
	        	if (invoice.getVendorInvoiceTotal() != null && invoice.getVendorInvoiceTotal().compareTo(BigDecimal.ZERO) > 0) {
	        		amount = invoice.getVendorInvoiceTotal();
            		invoiceNumbers.add(invoice.getInvoiceNumber()+ "  Amount = "+ amount.toPlainString());
	            } else {
	            	invoiceNumbers.add(invoice.getInvoiceNumber());	
	            } 	        		
            	
            }
            return invoiceNumbers;           
        }
        
        public String getVendorInvoiceStatus(){
            if(getVendorInvoiceNumbers().isEmpty()){
                return "Not Invoiced by Vendor";
            }else{
                for(LineItem item:order.getItems()){
                    if(item.getQuantityInvoicedByVendor().compareTo(new BigInteger(item.getQuantity().toString())) < 0){
                        return "Partially Invoiced By Vendor";
                    }
                }
            }
            return "Fully Invoiced by Vendor";
        }
        
        public boolean showInvalidateCustomerOrder() {
            if(this.isTechSupport() &&  
            		(this.order.getStatus() != null && 
            		(!OrderStatusEnum.SHIPPED.getValue().equals(this.order.getStatus().getValue()) &&
            		 !OrderStatusEnum.CANCELED.getValue().equals(this.order.getStatus().getValue()))) ) {
            	return true;
            }
            return false;
        }
        
        public boolean showResolveCustomerOrder() {
            return this.isTechSupport();
        }
        
        public boolean isTechSupport() {
        	return systemUserBp.getRolesForUser(user).contains(TECH_SUPPORT);
        }

    	@Lock(LockType.WRITE)
        public void invalidateCustomerOrder() {
        	OrderStatus orderStatus = orderStatusBp.getOrderStatusByValue(OrderStatusEnum.INVALIDATED.getValue());
        	if(this.order.getCustomerPo() != null && this.order.getCustomerPo().getValue() != null){
	        	String customerPO = this.order.getCustomerPo().getValue();
	        	this.order.getCustomerPo().setValue(customerPO + "-inValid");
	        	this.order.getOrderLogs().add(orderLogBp.logChangeOrder(this.order, "invalidate Customer PO " + customerPO , user ));
        	} else {
        		this.order.getOrderLogs().add(orderLogBp.logChangeOrder(this.order, "invalidated Order" , user ));
        	}
        	for (PhoenixTaskSummary task : this.taskService.getOpenOrderTasks(this.order)) {
        		this.taskService.exitTask(task.getId());
        	}
        	this.orderBp.setOrderStatus(this.order, orderStatus);
        	this.order = this.orderBp.update(this.order);
        	this.setOrder(this.order);
        }

    	@Lock(LockType.WRITE)
        public void resolveCustomerOrder() {
        	OrderStatus orderStatus = orderStatusBp.getOrderStatusByValue(OrderStatusEnum.MANUALLY_RESOLVED.getValue());
        	this.order.getOrderLogs().add(orderLogBp.logChangeOrder(this.order, "manually resolved Order" , user ));
        	for (PhoenixTaskSummary task : this.taskService.getOpenOrderTasks(this.order)) {
        		this.taskService.exitTask(task.getId());
        	}
        	this.orderBp.setOrderStatus(this.order, orderStatus);
        	this.order = this.orderBp.update(this.order);
        	this.setOrder(this.order);
        }
        
        public boolean canUserEditPo() {
    		if(this.getOrder() != null && this.getOrder().getCredential() != null && this.getOrder().getCredential().getProperties() != null) {
            	for (CredentialXCredentialPropertyType property : this.getOrder().getCredential().getProperties()) {
                	if (property.getType().getName().equalsIgnoreCase(ALLOW_PO_EDITING)) {
                		if(property.getValue()!= null && "N".equalsIgnoreCase(property.getValue())) {
                			//If allow PO editing flag is false, then only tech support can change the order
                			return this.isTechSupport();
                		}
                	}
                }
        	}
    		return true;
        }

	    public List<PhoenixTaskSummary> getTasks() {
	    	if (tasks == null) {
	    		tasks = new ArrayList<>();
	    	}
	        return tasks;
	    }

		@Lock(LockType.WRITE)
	    public void saveUploadedFile(){
	    	OrderAttachment file = getFileUploadBean().saveUploadedFile(null, loginBean.getSystemUser());	    	
	    	this.order.getAttachments().add(file);
	    	this.order = this.orderBp.update(this.order);
	    	this.setOrder(this.order);
	    }

		public FileUploadBean getFileUploadBean() {
			return fileUploadBean;
		}

		public void setFileUploadBean(FileUploadBean fileUploadBean) {
			this.fileUploadBean = fileUploadBean;
		}
		
		public String findUsAccount(){
			//Don't try to search on empty order
			if(this.order != null && this.order.getId() != null){
				return customerOrderBp.getUsAccountNumber(this.order);
			}else{
				return null;
			}
		}
		
		public void setupResend855(){
			this.setResendEdiType(ResendType.EDI_855);
		}
		
		public void setupResend856(){
			this.setResendEdiType(ResendType.EDI_856);
			setResendEdiDropdownNumbers(new ArrayList<String>());
			for(Shipment shipment: order.getShipments()){
				if(shipment.getTrackingNumber() != null){
					getResendEdiDropdownNumbers().add(shipment.getTrackingNumber());
				}
			}
		}
		
		public void setupResend810(){
			this.setResendEdiType(ResendType.EDI_810);
			setResendEdiDropdownNumbers(new ArrayList<String>());
			List<CustomerInvoice> invoices = customerInvoiceBp.findAllByApdPo(this.order.getApdPo().getValue());
			for(CustomerInvoice invoice : invoices){
				if(invoice.getInvoiceNumber() != null){
					this.resendEdiDropdownNumbers.add(invoice.getInvoiceNumber());
				}
			}
		}
		
		public void setupResend810Credit(){
			//get managed entity, since return orders are lazy
			this.order = customerOrderBp.hydrateForOrderDetails(this.order);
			this.setResendEdiType(ResendType.EDI_810_CREDIT);
			setResendEdiDropdownNumbers(new ArrayList<String>());
			for(CustomerCreditInvoice creditInvoice : customerCreditInvoiceBp.getCreditInvoicesForOrder(this.order)){
				if(creditInvoice.getInvoiceNumber() != null){
					resendEdiDropdownNumbers.add(creditInvoice.getInvoiceNumber());
				}
			}
		}
		
		public String generateDefaultResendEdiDropdownLabel(){
			switch(this.resendEdiType){
				case EDI_856:
					return "Select a tracking number";
				case EDI_810:
				case EDI_810_CREDIT:
					return "Select an invoice number";
				default:
					return "No options for this dropdown type";
			}
		}
		
		public boolean renderResendEdiDropdown(){
			return ResendType.EDI_810.equals(this.resendEdiType) || ResendType.EDI_856.equals(this.resendEdiType) || ResendType.EDI_810_CREDIT.equals(this.resendEdiType);
		}
		
		public void resendDocument(){
			this.order = customerOrderBp.hydrateForOrderDetails(this.order);
			switch(this.resendEdiType){
				case EDI_855:
					POAcknowledgementDto poAcknowledgementDto = customerOrderBp.createFullAcknowledgementForResend(this.order);
					poAcknowledgementDto = customerOrderBp.populateOrderAcknowledgement(this.order, poAcknowledgementDto);
					messageService.sendPurchaseOrderAcknowledgment(poAcknowledgementDto, MessageType.EDI, Boolean.TRUE);
					orderLogBp.logChangeOrder(this.order, "Resent 855", loginBean.getUsername());
					break;
				case EDI_856:
					for(Shipment shipment: order.getShipments()){
						if(shipment.getTrackingNumber()!= null && shipment.getTrackingNumber().equals(selectedResendDropdownNumber)){
							try {
								messageService.sendShipmentNotice(DtoFactory.createShipmentDto(shipment), MessageType.EDI, Boolean.TRUE);
							} catch (ParsingException e) {
								logger.error("Could not parse shipment with tracking number: " + shipment.getTrackingNumber());
							}
						}
					}
					break;
				case EDI_810:
					List<CustomerInvoice> invoices = customerInvoiceBp.findAllByApdPo(this.order.getApdPo().getValue());
					for(CustomerInvoice invoice : invoices){
						if(invoice.getInvoiceNumber() != null && invoice.getInvoiceNumber().equals(selectedResendDropdownNumber)){
							resendEDIInvoice(invoice, this.order);
						}
					}
					break;
				case EDI_810_CREDIT:
					for(CustomerCreditInvoice creditInvoice : customerCreditInvoiceBp.getCreditInvoicesForOrder(this.order)){
						if(creditInvoice.getInvoiceNumber() != null && creditInvoice.getInvoiceNumber().equals(selectedResendDropdownNumber)){
							resendEDICreditInvoice(creditInvoice);
						}
					}
					break;
				default:
					logger.error("unexpected resend message type" + this.resendEdiType);
			}
			this.setResendEdiType(null);
			
		}

		private void resendEDICreditInvoice(CustomerCreditInvoice creditInvoice) {
			CustomerCreditInvoiceDto customerCreditInvoiceDto;
			try {
				customerCreditInvoiceDto = DtoFactory.createCustomerCreditInvoiceDto(creditInvoice);
				messageService.sendCreditInvoice(customerCreditInvoiceDto, MessageType.EDI, Boolean.TRUE);
			} catch (ParsingException e) {
				logger.error("Could not parse credit invoice with number " + selectedResendDropdownNumber);
			}
		}

		private void resendEDIInvoice(CustomerInvoice invoice, CustomerOrder order) {
			try {
				CustomerInvoiceDto customerInvoiceDto = DtoFactory.createCustomerInvoiceDto(invoice);
				customerInvoiceDto.setReceiverCode(order.getCredential().getEdiReceiverCode());
				customerInvoiceDto.setEdiVersion(order.getCredential().getEdiVersion());
				customerInvoiceDto.setPartnerId(order.getCredential().getPartnerId());
				messageService.sendInvoice(customerInvoiceDto, MessageType.EDI, Boolean.TRUE);
			} catch (ParsingException e) {
				logger.error("Could not parse invoice with number " + selectedResendDropdownNumber);
			}
		}
		
		public ResendType getResendEdiType() {
			return resendEdiType;
		}

		public void setResendEdiType(ResendType resendEdiType) {
			this.resendEdiType = resendEdiType;
		}

		public List<String> getResendEdiDropdownNumbers() {
			return resendEdiDropdownNumbers;
		}

		public void setResendEdiDropdownNumbers(
				List<String> resendEdiDropdownNumbers) {
			this.resendEdiDropdownNumbers = resendEdiDropdownNumbers;
		}

		public String getSelectedResendDropdownNumber() {
			return selectedResendDropdownNumber;
		}

		public void setSelectedResendDropdownNumber(
				String selectedResendDropdownNumber) {
			this.selectedResendDropdownNumber = selectedResendDropdownNumber;
		}

		public enum ResendType{
			EDI_855("Edi 855"),EDI_856("Edi 856"), EDI_810("Edi 810"), EDI_810_CREDIT("Edi 810 Credit");
			
			private String label;
			
			private ResendType(String inputLabel) {
				this.label = inputLabel;
			}
			
			public String getLabel(){
				return label;
			}
		}
		
		public boolean isAnyNotificationEdi(){
			return isAckEdi() || isShipmentNoticeEdi() || isInvoiceEdi() || isCreditInvoiceEdi();
		}
		
	public boolean isAckEdi() {
		if (this.order != null && this.order.getCredential() != null) {
			return isEdiMessageType(this.order.getCredential()
					.getPoAcknowledgementMessageType());
		}
		return false;
	}

	public boolean isShipmentNoticeEdi() {

		if (this.order != null && this.order.getCredential() != null) {
			return isEdiMessageType(this.order.getCredential()
					.getShipmentNotificationMessageType());
		}
		return false;
	}

	public boolean isInvoiceEdi() {

		if (this.order != null && this.order.getCredential() != null) {
			return isEdiMessageType(this.order.getCredential()
					.getInvoiceMessageType());
		}
		return false;

	}

	public boolean isCreditInvoiceEdi() {

		if (this.order != null && this.order.getCredential() != null) {
			return isEdiMessageType(this.order.getCredential()
					.getCreditInvoiceMessageType());
		}
		return false;
	}
		
		public boolean isEdiMessageType(MessageType type){
			return MessageType.EDI.equals(type);
		}

		public String getBulkUpdateInvoiceNumbers() {
			return bulkUpdateInvoiceNumbers;
		}

		public void setBulkUpdateInvoiceNumbers(String bulkUpdateInvoiceNumbers) {
			this.bulkUpdateInvoiceNumbers = bulkUpdateInvoiceNumbers;
		}
}
