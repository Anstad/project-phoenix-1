/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateful
@ConversationScoped
public class ViewReportResultsBean implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ViewReportResultsBean.class);

    @PostConstruct
    public void start() {
        LOGGER.info("Calling view report results!");
    }

    private static final long serialVersionUID = -6823602323059021400L;

    private AbstractReportBean<?> reportBean;

    public String[] getColumns() {
        if (reportBean != null) {
            return reportBean.getResultHeader();
        }
        return new String[0];
    }

    public List<Object[]> getResults() {
    	if (reportBean != null) {
    		return reportBean.getResults();
    	}
    	return new ArrayList<>();
    }

    public void setReportBean(AbstractReportBean<?> reportBean) {
        this.reportBean = reportBean;
    }
}
