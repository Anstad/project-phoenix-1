/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.VendorInvoiceBp;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.VendorInvoice;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.ValidationErrorDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;

@Named
@Stateful
@ConversationScoped
public class RemediateInvoiceBean extends OrderHumanTaskFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemediateInvoiceBean.class);

    private VendorInvoice vendorInvoice;
    
    private Set<ValidationErrorDto> validationErrors = new HashSet<>();

    @Inject
    private VendorInvoiceBp vendorInvoiceBp;

    @Inject
    private LineItemBp lineItemBp;

	private String vendorName;

    @Override
    public void init() {
        Long vendorInvoiceId = (Long) this.taskContent.get("inputInvoiceId");
        try {
        	VendorInvoiceDto vendorInvoiceDto = (VendorInvoiceDto) this.taskContent.get("inputInvoiceDto");
        	for (LineItemDto item : vendorInvoiceDto.getActualItems()) {
        		validationErrors.addAll(item.getValidationErrorDtos());
        		if(getVendorName() == null && !StringUtils.isEmpty(item.getVendorName())){
        			setVendorName(item.getVendorName());
        		}
        	}
        }
        catch (Exception e) {
        	LOGGER.warn("Error when getting validation errors for invoice remediation, probably caused by legacy invoice: " + e.getMessage());
        	LOGGER.debug("Stack trace", e);
        }
        if (vendorInvoiceId != null) {
            vendorInvoice = vendorInvoiceBp.findById(vendorInvoiceId, VendorInvoice.class);
            if (vendorInvoice.getActualItems() != null) {
                vendorInvoice.getActualItems().size();
            }
        }
        else {
            LOGGER.error("Vendor Invoice ID not found!");
        }
    }

    public String approve() {
        BigDecimal amount = BigDecimal.ZERO;
        for (LineItem item : vendorInvoice.getActualItems()) {
            amount = amount.add(item.getCost().multiply(new BigDecimal(item.getQuantity())));
            lineItemBp.update(item);
        }
        vendorInvoice.setAmount(amount);
        vendorInvoice.setApprovalDate(new Date());
        vendorInvoice.setValid(true);
        vendorInvoiceBp.update(vendorInvoice);
        return this.completeOrderTask(new HashMap<String, Object>());
    }

    public String confirmRejection() {
        return this.completeOrderTask(new HashMap<String, Object>());
    }

    public VendorInvoice getVendorInvoice() {
        return vendorInvoice;
    }

    public void setVendorInvoice(VendorInvoice vendorInvoice) {
        this.vendorInvoice = vendorInvoice;
    }

	public Set<ValidationErrorDto> getValidationErrors() {
		return validationErrors;
	}

	public void setValidationErrors(Set<ValidationErrorDto> validationErrors) {
		this.validationErrors = validationErrors;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
}
