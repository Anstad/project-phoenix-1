/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.LineItem;

@Named
@Stateful
@RequestScoped
public class ResendOrderToVendorBean extends OrderHumanTaskFormBean {

    private static final String escalationGroup = "Tech Support";

    private static final Logger LOG = LoggerFactory.getLogger(ResendOrderToVendorBean.class);

    private String vendorName;

    @Override
    public void init() {
        this.vendorName = (String) this.taskContent.get("vendorName");
    }

    public void retrieve() {
    }

    public String cancel() {
        List<Long> lineItemsToCancel = new ArrayList<>();;
        for (LineItem lineItem : customerOrder.getItems()){
            if (lineItem.getVendor().getName().equals(vendorName)){
                lineItemsToCancel.add(lineItem.getId());
            }
        }
        return this.completeTask(false, lineItemsToCancel);
//        Map<String, Object> params = new HashMap<>();
//        params.put("resendOrderToVendor", false);
//        params.put("lineItemsToCancel", lineItemsToCancel);
//        return this.forwardOrComplete(params, escalationGroup);
    }

    public String retry() {
        return completeTask(true, null);
    }

    public String completeTask(boolean resendOrderToVendor, List lineItemsToCancel) {
        Map<String, Object> params = new HashMap<>();
        params.put("resendOrderToVendor", resendOrderToVendor);
        params.put("lineItemsToCancel", lineItemsToCancel);
        return this.completeOrderTask(params);
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }
}