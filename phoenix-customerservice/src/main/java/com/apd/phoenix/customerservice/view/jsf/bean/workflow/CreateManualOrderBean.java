/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.LineItem;

@Named
@Stateful
@ConversationScoped
public class CreateManualOrderBean extends OrderHumanTaskFormBean {

    private static final Logger LOG = LoggerFactory.getLogger(CreateManualOrderBean.class);

    @Inject
    LineItemBp lineItemBp;

    List<LineItem> lineItems;

    @Inject
    private Conversation conversation;

    @Override
    public void init() {
        lineItems = new ArrayList<LineItem>();
        for (LineItem lineItem : customerOrder.getItems()) {
            lineItems.add(lineItem);
            if (StringUtils.isBlank(lineItem.getShortName())) {
                lineItem.setShortName(lineItem.getDescription());
            }
        }
        this.conversation.begin();
    }

    public void retrieve() {

    }

    public String complete(){
    	Map<String, Object> results = new HashMap<>();
    	updateOrder();
    	customerOrder = customerOrderBp.update(customerOrder);
    	this.conversation.end();
    	return this.completeOrderTask(results);
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

    public void removeLineItem(LineItem lineItem) {
        lineItems.remove(lineItem);
        this.updateOrder();
    }

    public void updateOrder() {
        customerOrder.getItems().removeAll(customerOrder.getItems());
        customerOrder.getItems().addAll(lineItems);
        for (LineItem lineItem : customerOrder.getItems()) {
            lineItem.setOrder(customerOrder);
        }
        customerOrder.refreshOrderTotal();
    }

}
