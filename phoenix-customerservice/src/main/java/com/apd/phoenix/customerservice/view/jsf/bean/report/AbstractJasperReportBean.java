package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.bean.ViewUtils;
import com.apd.phoenix.service.business.SecurityContext;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.persistence.jpa.AccountDao;

public abstract class AbstractJasperReportBean {

    @Inject
    private SystemUserBp userBp;

    @Inject
    private SecurityContext securityContext;

    @Inject
    private AccountDao accountDao;

    private static final Logger LOG = LoggerFactory.getLogger(AbstractJasperReportBean.class);

    public List<String> getAvailableAccounts() {
        if (securityContext.hasPermission("view data for all accounts")) {
            LOG.info("Getting all root accounts for report");
            return ViewUtils.asList(accountDao.getRootAccounts());
        }
        else {
            return ViewUtils.asList(this.userBp.getAccountsForUser(securityContext.getUserLogin()));
        }
    }

}
