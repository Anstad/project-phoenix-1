/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateful
@ConversationScoped
public class ApReportBean extends AbstractReportBean<ArReportBean> implements Serializable {

    private static final long serialVersionUID = 5556845450144240260L;

    private static final Logger LOG = LoggerFactory.getLogger(ApReportBean.class);

    @Override
	protected List<Object[]> getResults() {
		// TODO integrate with Jaspersoft
		return new ArrayList<>();
	}

    @Override
    protected String[] getResultHeader() {
        String[] toReturn = { "Account Name", "User Name", "Dept/Account/Cost Center", "APD PO", "Cust/Billing PO",
                "Track #", "Invoice #", "Accounting Id", "GLAccount", "Subaccount", "Order Extras", "Item Extras",
                "Merchandise", "Tax", "Shipping", "Total", "APD Cost", "Bill To Company Name", "Ship To State",
                "Ship To Zip Code", "Date Ordered", "Date Shipped", "Date Invoiced" };
        return toReturn;
    }
}
