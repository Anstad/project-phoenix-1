package com.apd.phoenix.customerservice.view.jsf.utility;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.AddressXAddressPropertyType;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.persistence.jpa.PersonDao;

@Named
@RequestScoped
public class ShippingLabelPDFCreator implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShippingLabelPDFCreator.class);
    private static final float TOP_MARGIN = -25;
    private static final float LEFT_MARGIN = 25;
    private static final float BOTTOM_MARGIN = 25;
    private static final float FONT_SIZE = 12;
    private static final float LINE_SPACING = 5;
    private static final float INCH = 72;
    private static final float WIDTH = 7;
    private static final float HEIGHT = 5;
    private static final float MAX_STRING_LENGTH = 15; //Horizontal calculations are estimations and may vary with font.
    private static final float MAX_NUM_LINES = 6;

    private static final String ACCOUNT_USPS = "United States Postal Service - Nationwide";
    private static final String SHIPPING_LABEL_COMPANY_USPS = "UNITED STATES POSTAL SERVICE";

    @Inject
    private PersonDao personDao;

    private float trX;
    private float trY;

    private PDDocument document;
    private PDPage page;
    private PDPageContentStream contentStream;

    public ShippingLabelPDFCreator() {
        super();

        try {
            // Create a document and add a page to it
            document = new PDDocument();
            page = new PDPage();
            document.addPage(page);
            PDRectangle pdr = new PDRectangle();
            pdr.setLowerLeftX(0);
            pdr.setLowerLeftY(0);
            pdr.setUpperRightX(WIDTH * INCH);
            pdr.setUpperRightY(HEIGHT * INCH);
            page.setMediaBox(pdr);
            trX = page.getMediaBox().getUpperRightX();
            trY = page.getMediaBox().getUpperRightY();

            // Start a new content stream which will "hold" the to be created content
            contentStream = new PDPageContentStream(document, page);
        }
        catch (IOException e) {
            LOGGER.error("An error occured:", e);
            try {
                contentStream.close();
            }
            catch (IOException e1) {
                LOGGER.error("Could not close content stream:", e1);
            }
            try {
                document.close();
            }
            catch (IOException e1) {
                LOGGER.error("Could not close document:", e1);
            }
        }

    }

    public void downloadShippingLabel(CustomerOrder co) {

        try {
            createReturnAddress();
            createOrderNumberInfo(co);
            createShipToAddress(co);

            // Make sure that the content stream is closed:
            contentStream.close();

            ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();

            // Save the results and ensure that the document is properly closed:
            document.save(pdfOutputStream);

            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
                    .getResponse();
            // Header
            response.setHeader("Expires", "0");
            response.setContentType("application/pdf");
            response.setContentLength(pdfOutputStream.size());

            // Write the PDF
            ServletOutputStream responseOutputStream = response.getOutputStream();
            responseOutputStream.write(pdfOutputStream.toByteArray());
            responseOutputStream.flush();
            responseOutputStream.close();
            FacesContext.getCurrentInstance().responseComplete();

        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
            try {
                contentStream.close();
            }
            catch (IOException e1) {
                LOGGER.error("Could not close content stream:", e1);
            }
        }
        finally {
            try {
                document.close();
            }
            catch (IOException e) {
                LOGGER.error("Could not close document:", e);
            }
        }

    }

    private void createReturnAddress() throws IOException {
        // Create a new font object selecting one of the PDF base fonts
        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(LEFT_MARGIN, this.trY + TOP_MARGIN);
        contentStream.drawString(CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties()
                .getString("shippinglabel.returnaddress.name"));
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties()
                .getString("shippinglabel.returnaddress.line1"));
        contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
        contentStream.drawString(CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties()
                .getString("shippinglabel.returnaddress.line2"));

        contentStream.endText();
    }

    private void createOrderNumberInfo(CustomerOrder co) throws IOException {
        float x = this.trX - (MAX_STRING_LENGTH * FONT_SIZE);
        float y = (this.trY / 2) + (MAX_NUM_LINES * (FONT_SIZE + LINE_SPACING));

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(x, y);
        if (co.getCustomerPo() != null) {
            contentStream.drawString(co.getCustomerPo().getValue());
        }
        if (co.getApdPo() != null) {
            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString(co.getApdPo().getValue());
        }
        Person person = personDao.eagerLoad(co.getUser().getPerson());
        for (PhoneNumber phone : person.getPhoneNumbers()) {
            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString("(" + phone.getType().getName() + ")" + phone.toString());
        }

        contentStream.endText();
    }

    private void createShipToAddress(CustomerOrder co) throws IOException {
        float x = (this.trX / 2) - (FONT_SIZE * MAX_STRING_LENGTH / 2);
        float y = BOTTOM_MARGIN + ((FONT_SIZE + LINE_SPACING) * MAX_NUM_LINES);

        PDFont font = PDType1Font.HELVETICA_BOLD;

        contentStream.beginText();
        contentStream.setFont(font, FONT_SIZE);

        contentStream.moveTextPositionByAmount(x, y);
        contentStream.drawString(CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties()
                .getString("shippinglabel.shipto.label"));
        contentStream.moveTextPositionByAmount(5 * FONT_SIZE, 0);
        if (co.getAddress() != null) {

            if (co.getAddress().getName() != null) {
                contentStream.drawString(co.getAddress().getName());
            }
            if (co.getAddress().getCompany() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getCompany());
            }
            //TODO: Need to move this Hard coding of company name for USPS to EDI parsing. 
            else if (co.getAccount() != null && ACCOUNT_USPS.equalsIgnoreCase(co.getAccount().getName())) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(SHIPPING_LABEL_COMPANY_USPS);
            }
            if (co.getAddress().getLine1() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getLine1());
            }
            if (co.getAddress().getLine2() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getLine2());
            }
            contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
            contentStream.drawString(co.getAddress().getCity() + ", " + co.getAddress().getState() + " "
                    + co.getAddress().getZip());
            if (co.getAddress().getCountry() != null) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(co.getAddress().getCountry());
            }
            for (AddressXAddressPropertyType field : co.getAddress().getFields()) {
                contentStream.moveTextPositionByAmount(0, -(FONT_SIZE + LINE_SPACING));
                contentStream.drawString(field.getType().getName() + ": ");
                contentStream.moveTextPositionByAmount(calculateWidth(field.getType().getName() + ": ", font), 0);
                contentStream.drawString(field.getValue());
                contentStream.moveTextPositionByAmount(-calculateWidth(field.getType().getName() + ": ", font), 0);
            }
        }
        contentStream.endText();
    }

    private float calculateWidth(String string, PDFont font) throws IOException {
        return font.getStringWidth(string) / 1000 * FONT_SIZE;
    }

}