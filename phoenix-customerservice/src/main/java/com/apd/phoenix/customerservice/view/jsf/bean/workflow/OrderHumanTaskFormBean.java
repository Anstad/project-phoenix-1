/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.executor.command.api.Command;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.PoNumber;
import com.apd.phoenix.service.workflow.UserTaskService;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class OrderHumanTaskFormBean extends HumanTaskFormBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(OrderHumanTaskFormBean.class);

    protected CustomerOrder customerOrder;

    private static final String DEFAULT_PO_VALUE = "####";

    private static final String TECH_SUPPORT = "Tech Support";

    protected static final String CHANGE_ORDER_FORM = "/admin/orderSearch/order-view-workflow?faces-redirect=true";

    private Long id;

    @Inject
    protected CustomerOrderBp customerOrderBp;

    @Inject
    OrderDetailsBean orderDetailsBean;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Inject
    private WorkflowTaskBean workflowTaskBean;

    @Inject
    private UserTaskService userTaskService;

    @Override
    public void initDomainInfo() {
        id = (Long) taskContent.get("orderId");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder lazy = processLookupBp.getOrderFromProcessId(id);

        //this case should not be reached for orders placed after PHOEN-4762 is resolved
        if (lazy == null) {
            lazy = customerOrderBp.searchByApdPo(workflowTaskBean.getApdPo(userTaskService
                    .taskToSummary(userTaskService.getTask(taskId))));
            //assumed that orders placed before PHOEN-4762 is resolved have process ID equal to order ID. This will not hold once 
            //PHOEN-4493 is resolved
            id = lazy.getId();
        }

        //TODO: Leverage the DTO to populate the customer order object as needed for order task forms
        customerOrder = customerOrderBp.hydrateForOrderDetails(lazy);

        this.init();
    }

    public abstract void init();

    protected String completeOrderTask(Map<String, Object> params) {
        return this.completeTask(params, id, CompleteTaskCommand.Type.ORDER);
    }

    //    protected String forwardOrComplete(Map<String, Object> params, String forwardToId) {
    //        return this.forwardOrComplete(params, customerOrder.getId(), CompleteTaskCommand.Type.ORDER, forwardToId);
    //    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public String getApdPo() {
        PoNumber apdPo = customerOrder.getApdPo();
        if (apdPo != null) {
            return apdPo.getValue();
        }
        else {
            return DEFAULT_PO_VALUE;
        }
    }

    public Boolean canCancel() {
        String username = this.getUser();
        return systemUserBp.getRolesForUser(username).contains(TECH_SUPPORT);
    }

    public String toChangeOrderForm() {
        orderDetailsBean.setOrder(customerOrder);
        return CHANGE_ORDER_FORM;
    }
}
