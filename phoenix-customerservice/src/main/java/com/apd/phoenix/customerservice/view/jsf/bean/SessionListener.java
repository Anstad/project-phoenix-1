package com.apd.phoenix.customerservice.view.jsf.bean;

import javax.inject.Inject;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.jboss.weld.context.ConversationContext;
import org.jboss.weld.context.http.Http;

@WebListener
public class SessionListener implements HttpSessionListener {

    @Inject
    @Http
    private ConversationContext conversationContext;

    //timeout in seconds, default is 1800 (half an hour)
    private static final int MINIMUM_TIMEOUT = 7200; //two hours

    @Override
    public void sessionCreated(HttpSessionEvent sessionEvent) {
        if (sessionEvent != null && sessionEvent.getSession() != null
                && sessionEvent.getSession().getMaxInactiveInterval() < MINIMUM_TIMEOUT) {
            sessionEvent.getSession().setMaxInactiveInterval(MINIMUM_TIMEOUT);
        }
        long conversationTimeoutMs = MINIMUM_TIMEOUT * 1000;
        conversationContext.setDefaultTimeout(conversationTimeoutMs);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent arg0) {
        // do nothing
    }

}
