/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.report.ReportService;
import com.apd.phoenix.service.utility.FileDownloader;

@Named
@Stateful
@ConversationScoped
public class ShipmasterReportBean extends AbstractJasperReportBean implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(ShipmasterReportBean.class);

    @Inject
    private ReportService reportService;

    private Date shipDate;

    private String account;

    public String generateReport() {
        LOG.info("downloading Shipmaster report");
        List<String> accounts = new ArrayList<>();
        accounts.add(account);
        return FileDownloader.downloadFile(reportService.generateCustomerServiceShipmasterReport(accounts, shipDate));
    }

    public Date getShipDate() {
        return shipDate;
    }

    public void setShipDate(Date shipDate) {
        this.shipDate = shipDate;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
