package com.apd.phoenix.customerservice.view.jsf.utility;

import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.utility.PropertiesLoader;

public class PropertiesLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesLoader.class);

    protected static Properties getAsProperties(String name) {
        Properties props = new Properties();

        try {
            props.load(PropertiesLoader.class.getClassLoader().getResourceAsStream("com/apd/phoenix/messages/" + name));
        }
        catch (Exception e) {
            LOGGER.error("Error loading properties file '" + name + "' :", e);
        }

        return props;
    }
}
