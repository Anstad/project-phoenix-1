/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import javax.ejb.Stateful;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.AuthorizeShipToBean;
import com.apd.phoenix.customerservice.view.jsf.bean.workflow.OrderHumanTaskFormBean;
import com.apd.phoenix.service.model.Address;
import javax.enterprise.context.RequestScoped;

@Named
@Stateful
@RequestScoped
public class AuthorizeShipToBean extends OrderHumanTaskFormBean {

    private static final Logger LOG = LoggerFactory.getLogger(AuthorizeShipToBean.class);

    private Address address;

    @Override
    public void init() {
        this.address = customerOrderBp.getAddress(customerOrder);
    }

    public void retrieve() {

    }

    public String authorize() {
        return this.completeOrderTask(null);
    }

    public Address getAddress() {
        return address;
    }
}
