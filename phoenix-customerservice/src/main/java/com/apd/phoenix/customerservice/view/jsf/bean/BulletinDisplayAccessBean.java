package com.apd.phoenix.customerservice.view.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import com.apd.phoenix.service.business.BulletinBp;
import com.apd.phoenix.service.model.Bulletin;

/**
 * This request-scoped bean is a helper bean for BulletinDisplayBean. It loads the bulletin list from persistence, 
 * saving the list in memory, and returns the list.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@RequestScoped
public class BulletinDisplayAccessBean implements Serializable {

    private static final long serialVersionUID = 41743704237803173L;

    private List<Bulletin> bulletinList;

    @Inject
    private BulletinBp bulletinBp;

    public boolean hasLoadedBulletins() {
        return bulletinList != null;
    }

    /**
     * Returns the list of bulletins that should be displayed. If they have not been loaded, loads them from 
     * persistence; otherwise, returns the bulletins loaded into memory.
     * 
     * @return
     */
    public List<Bulletin> getBulletinList() {
        if (bulletinList == null) {
            //if the list is null, loads form memory
            bulletinList = bulletinBp.getCSRBulletins();
            if (bulletinList == null) {
                bulletinList = new ArrayList<Bulletin>();
            }
        }
        return bulletinList;
    }
}
