package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.customerservice.view.jsf.utility.CustomerServicePropertiesLoader;
import com.apd.phoenix.service.business.ItemCategoryBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.model.AccountXCredentialXUser;
import com.apd.phoenix.service.model.FieldOptions;
import com.apd.phoenix.service.model.ItemCategory;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.UnitOfMeasure;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.persistence.jpa.UnitOfMeasureDao;
import com.apd.phoenix.web.cashout.AbstractCashoutBean;

/**
 * This class provides backing functionality for the manual-order.xhtml page. It contains several Converters, a 
 * CustomerOrder object, and methods to add items to the order.
 * 
 * @author RHC
 *
 */
@Named
@Stateful
@ConversationScoped
public class ManualOrderBean extends AbstractCashoutBean implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(ManualOrderBean.class);

    public static final String DEFAULT_UNSPSC = "44120000";
    private static final long serialVersionUID = -2388341212538843974L;

    private static final String STATE_EXEMPT_FOR_SALES_TAX = "state exemptions for sales tax";
    private static final String SHIP_STATES = "ship states";

    public void addItemToOrder(LineItem item) {
        if (StringUtils.isBlank(item.getApdSku())) {
            item.setApdSku(item.getSupplierPartId());
        }
        this.addLineItem(item, item.getQuantity(), null);
        newItem();
    }

    private LineItem currentItem;

    public LineItem getCurrentItem() {
        return this.currentItem;
    }

    public void setCurrentItem(LineItem currentItem) {
        this.currentItem = currentItem;
    }

    public void newItem() {
        this.currentItem = new LineItem();
        this.currentItem.setOrder(this.currentOrder);
        //Initialized values which should be written over when the bean populates them
        this.currentItem.setQuantity(1);

        boolean defaultTaxable = true;
        //marks the order as taxable, unless "charge sales tax" is set to "no" on the Credential
        String chargeSales = credentialPropertiesMap().get("charge sales tax");
        if (chargeSales != null && (chargeSales.equalsIgnoreCase("no") || chargeSales.equalsIgnoreCase("false"))) {
            defaultTaxable = false;
        }

        if (credentialPropertiesMap().get(STATE_EXEMPT_FOR_SALES_TAX) != null) {

            String exmptState = credentialPropertiesMap().get(STATE_EXEMPT_FOR_SALES_TAX);

            if (this.getPageValues().getValue().get(this.getPageValues().getField().get(SHIP_STATES)) instanceof FieldOptions) {
                FieldOptions state = (FieldOptions) this.getPageValues().getValue().get(
                        this.getPageValues().getField().get(SHIP_STATES));

                if (state != null && state.getValue().equals(exmptState)) {
                    defaultTaxable = false;
                }
            }

        }

        this.currentItem.setTaxable(defaultTaxable);
        this.currentItem.setLineNumber(-1); //Used by the forge view
        this.currentItem.setUnspscClassification(DEFAULT_UNSPSC);
        this.currentItem.setCost(BigDecimal.ZERO);
    }

    @Inject
    private VendorBp vendorBp;

    @Inject
    private OrderLogBp orderLogBp;

    private List<Vendor> allVendors;

    public Converter getVendorConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return vendorBp.findById(Long.valueOf(value), Vendor.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((Vendor) value).getId());
            }
        };
    }

    public List<Vendor> getAllVendors() {
        if (allVendors == null) {
            List<Vendor> results = vendorBp.findAll(Vendor.class, 0, 0);
            allVendors = new ArrayList<Vendor>();
            for (Vendor vendor : results) {
                if (vendor.getCommunicationMessageType().equals(MessageType.EMAIL)) {
                    allVendors.add(vendor);
                }
            }
        }
        return allVendors;
    }

    @Inject
    private UnitOfMeasureDao uomDao;

    private List<UnitOfMeasure> allUnitsOfMeasure;

    public Converter getUnitOfMeasureConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return uomDao.findById(Long.valueOf(value), UnitOfMeasure.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((UnitOfMeasure) value).getId());
            }
        };
    }

    public List<UnitOfMeasure> getAllUnitsOfMeasure() {

        if (this.allUnitsOfMeasure == null) {
            this.allUnitsOfMeasure = uomDao.findAll(UnitOfMeasure.class, 0, 0);
        }
        return this.allUnitsOfMeasure;
    }

    @Inject
    private ItemCategoryBp categoryBp;

    private List<ItemCategory> allCategories;

    public Converter getCategoryConverter() {

        return new Converter() {

            @Override
            public Object getAsObject(FacesContext context, UIComponent component, String value) {
                return categoryBp.findById(Long.valueOf(value), ItemCategory.class);
            }

            @Override
            public String getAsString(FacesContext context, UIComponent component, Object value) {

                if (value == null) {
                    return "";
                }

                return String.valueOf(((ItemCategory) value).getId());
            }
        };
    }

    public List<ItemCategory> getAllCategories() {

        if (this.allCategories == null) {
            allCategories = categoryBp.findAll(ItemCategory.class, 0, 0);
        }
        return allCategories;
    }

    @Inject
    private ManualOrderSelectionBean selectionBean;

    @Override
    protected AccountXCredentialXUser getCurrentAXCXU() {
        return selectionBean.getAxcxu();
    }

    @Override
    protected Map<String, String> credentialPropertiesMap() {
        return selectionBean.getPropertyMap();
    }

    @Override
    protected PropertiesConfiguration getProperties() {
        return CustomerServicePropertiesLoader.getInstance().getCustomerServiceProperties();
    }

    @Override
    protected void prePlaceOrder() {
        // do nothing, no pre-order setup required
    }

    @Override
    protected String postPlaceOrder() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        if (context != null && context.getUserPrincipal() != null
                && !StringUtils.isEmpty(context.getUserPrincipal().getName())) {
            orderLogBp.logChangeOrder(this.currentOrder, "create manual order", context.getUserPrincipal().getName());
        }
        else {
            logger.error("Could not determine user who placed order.");
        }
        return "manualOrderConfirm?faces-redirect=true";
    }

    @Override
    public String placeOrder() {
        boolean hasFailed = false;
        if (this.currentOrder.getItems().isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "There must be at least one item on the order",
                            "There must be at least one item on the order"));
            hasFailed = true;
        }
        for (LineItem item : this.currentOrder.getItems()) {
            if (item.getVendor() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Vendor"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Vendor")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getApdSku())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a SKU"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a SKU")));
                hasFailed = true;
            }
            if (item.getCategory() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Category"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Category")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getDescription())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Description"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Description")));
                hasFailed = true;
            }
            if (item.getUnitOfMeasure() == null) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Unit of Measure"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a Unit of Measure")));
                hasFailed = true;
            }
            if (StringUtils.isBlank(item.getUnspscClassification())) {
                FacesContext.getCurrentInstance().addMessage(
                        null,
                        new FacesMessage(FacesMessage.SEVERITY_ERROR, "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a UNSPSC"), "Item ".concat(item.getLineNumber() + "").concat(
                                " is missing a UNSPSC")));
                hasFailed = true;
            }
        }
        if (hasFailed) {
            return null;
        }
        return super.placeOrder();
    }

    @Override
    protected String orderTypeValue() {
        return "MANUAL";
    }

    @Override
    protected boolean canPersistBeforePlace() {
        return false;
    }

    public enum TaxableType {
        YES, NO;
    }

    public SelectItem[] getTaxableTypes() {
        SelectItem[] items = new SelectItem[TaxableType.values().length];
        int i = 0;
        for (TaxableType g : TaxableType.values()) {
            items[i++] = new SelectItem(g, g.toString());
        }
        return items;
    }

    private TaxableType itemTaxable;

    public TaxableType getItemTaxable() {
        if (this.currentItem.isTaxable() != null && this.currentItem.isTaxable()) {
            this.itemTaxable = TaxableType.YES;
        }
        else {
            this.itemTaxable = TaxableType.NO;
        }
        return itemTaxable;
    }

    public void setItemTaxable(TaxableType itemTaxable) {
        this.itemTaxable = itemTaxable;
        if (TaxableType.YES.equals(this.itemTaxable)) {
            this.currentItem.setTaxable(true);
        }
        else {
            this.currentItem.setTaxable(false);
        }
    }
}
