/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.order;

import java.math.BigDecimal;
import com.apd.phoenix.service.business.CreditCardTransactionLogBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CreditCardTransactionLog;
import com.apd.phoenix.service.model.CustomerOrder;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author nreidelb
 */
@Named
@Stateful
@ConversationScoped
public class TransactionHistoryBean {

    private String apdPo;

    @Inject
    private Conversation conversation;
    @Inject
    private CustomerOrderBp customerOrderBp;
    @Inject
    private CreditCardTransactionLogBp cardTransactionLogBp;

    private CustomerOrder customerOrder;
    private CreditCardTransactionLog currentCreditCardTransactionLog;

    public String setCustomerOrderByApdPO() {
        if (conversation.isTransient()) {
            conversation.begin();
        }
        customerOrder = customerOrderBp.searchByApdPo(apdPo, true);
        if (customerOrder != null) {
            customerOrderBp.hydrateForOrderDetails(customerOrder);
            return "transactionHistory.xhtml?faces-redirect=true";
        }
        FacesMessage message = new FacesMessage();
        message.setSeverity(FacesMessage.SEVERITY_ERROR);
        message.setSummary("No order could be found with that APD PO");
        message.setDetail("No order could be found with that APD PO");
        FacesContext.getCurrentInstance().addMessage(null, message);
        return null;
    }

    public String setTransactionLog(CreditCardTransactionLog log) {
        this.currentCreditCardTransactionLog = cardTransactionLogBp.hydrateCardTransactionLogForReport(log.getId());
        this.currentCreditCardTransactionLog.setLineItemXShipments(cardTransactionLogBp
                .getHydratedLineItemXShipments(log.getId()));
        return "creditCardTransactionHistory.xhtml?faces-redirect=true";
    }

    /**
     * @return the apdPo
     */
    public String getApdPo() {
        return apdPo;
    }

    /**
     * @param apdPo the apdPo to set
     */
    public void setApdPo(String apdPo) {
        this.apdPo = apdPo;
    }

    /**
     * @return the customerOrder
     */
    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    /**
     * @param customerOrder the customerOrder to set
     */
    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    /**
     * @return the currentCreditCardTransactionLog
     */
    public CreditCardTransactionLog getCurrentCreditCardTransactionLog() {
        return currentCreditCardTransactionLog;
    }

    /**
     * @param currentCreditCardTransactionLog the currentCreditCardTransactionLog to set
     */
    public void setCurrentCreditCardTransactionLog(CreditCardTransactionLog currentCreditCardTransactionLog) {
        this.currentCreditCardTransactionLog = currentCreditCardTransactionLog;
    }

    public BigDecimal getAmountDue() {
        BigDecimal amountDue = BigDecimal.ZERO;
        if (this.customerOrder != null) {
            amountDue = this.customerOrder.getCalculatedTotal().subtract(this.customerOrder.getCalculatedPaidTotal());
        }
        return amountDue;
    }
}
