package com.apd.phoenix.customerservice.view.jsf.utility;

import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerServicePropertiesLoader {

    private static Logger logger = LoggerFactory.getLogger(CustomerServicePropertiesLoader.class);
    private PropertiesConfiguration customerServiceProperties;
    private static final CustomerServicePropertiesLoader singleton = new CustomerServicePropertiesLoader();

    public static CustomerServicePropertiesLoader getInstance() {
        return singleton;
    }

    private CustomerServicePropertiesLoader() {
        try (InputStream is = CustomerServicePropertiesLoader.class.getClassLoader().getResourceAsStream(
                "com/apd/phoenix/messages/customerService.properties")) {
            customerServiceProperties = new PropertiesConfiguration();
            customerServiceProperties.load(is);
        } catch (ConfigurationException | IOException ex) {
            logger.error("Could not load \"customerService.properties\": ", ex);
        }
    }

    public PropertiesConfiguration getCustomerServiceProperties() {
        return customerServiceProperties;
    }
}
