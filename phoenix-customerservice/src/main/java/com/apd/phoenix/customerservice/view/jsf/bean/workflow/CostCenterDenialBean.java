/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
@Stateful
@RequestScoped
public class CostCenterDenialBean extends OrderHumanTaskFormBean {

    private static final Logger LOG = LoggerFactory.getLogger(CostCenterDenialBean.class);

    @Override
    public void init() {
    }

    public void retrieve() {

    }

    public String retry() {
        return this.complete(true);
    }

    public String cancel() {
        return this.complete(false);
    }

    public String complete(Boolean retry){
    	Map<String, Object> results = new HashMap<>();
    	results.put("retry", retry);
    	return this.completeOrderTask(results);
    }
}
