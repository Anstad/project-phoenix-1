package com.apd.phoenix.customerservice.view.jsf.utility;

import java.util.Properties;
import com.apd.phoenix.customerservice.view.jsf.utility.PropertiesLoader;

public class ValidationProperties extends PropertiesLoader {

    private static Properties props = PropertiesLoader.getAsProperties("validation.properties");

    public static String get(String key) {
        return props.getProperty(key);
    }
}
