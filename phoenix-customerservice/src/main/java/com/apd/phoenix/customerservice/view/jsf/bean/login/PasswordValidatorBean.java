package com.apd.phoenix.customerservice.view.jsf.bean.login;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@SessionScoped
public class PasswordValidatorBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String currentPassword = "";

    @Size(min = 5, max = 15, message = "Password length must be between {min} and {max} characters.")
    private String password = "";

    private String confirm = "";

    //@AssertTrue(message = "Passwords do not match!")
    public boolean passwordsMatch() {
        return password.equals(confirm);
    }

    public void storeNewPassword() {
        //TODO: implement actual business logic here

        if (!passwordsMatch()) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Passwords do not match!", "Password not changed!"));
            return;
        }

        if (currentPassword.equals("test")) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Successfully changed!", "Successfully changed!"));
        }
        else {
            FacesContext.getCurrentInstance()
                    .addMessage(
                            null,
                            new FacesMessage(FacesMessage.SEVERITY_ERROR, "Current Password Invalid!",
                                    "Password not changed!"));
        }
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }
}