/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;

@Named
@Stateful
@RequestScoped
public class PreauthorizationDeclinedBean extends OrderHumanTaskFormBean {

    CreditCardTransactionResult cardTransactionResult;

    @Override
    public void init() {
        cardTransactionResult = (CreditCardTransactionResult) this.taskContent.get("cardTransactionResult");
    }

    public String retry() {
        return this.complete(true);
    }

    public String cancel() {
        return this.complete(false);
    }

    private String complete(boolean retryAuthorization){
        Map<String, Object> params = new HashMap<>();
        params.put("retryAuthorization", retryAuthorization);
        return this.completeOrderTask(params);
    }
}
