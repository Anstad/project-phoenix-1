/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.customerservice.view.jsf.bean.workflow;

import java.util.Map;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import org.jbpm.task.Task;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.executor.command.api.CompleteTaskCommand;
import com.apd.phoenix.service.workflow.UserTaskService;

public abstract class HumanTaskFormBean {

    protected static final String TO_TASK_SCREEN = "/admin/customerService/customer-service?faces-redirect=true";

    @Inject
    UserTaskService userTaskService;

    @Inject
    WorkflowTaskBean workflowTaskBean;

    @Inject
    SystemUserBp systemUserBp;

    protected long taskId;

    protected Map<String, Object> taskContent;

    @PostConstruct
    public void postConstruct() {
        taskId = workflowTaskBean.getTaskToWork();
        taskContent = userTaskService.getTaskContent(taskId);
        this.initDomainInfo();
    }

    public abstract void initDomainInfo();

    protected String completeTask(Map<String, Object> params, long domainId, CompleteTaskCommand.Type domainIdType) {
        String user = this.getUser();
        Task task = userTaskService.getTask(taskId);
        if (!task.getTaskData().getActualOwner().getId().equals(user)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("You are not the owner of this task!"));
            return "";
        }
        userTaskService.suspendTask(taskId, user);
        userTaskService.completeTask(taskId, domainId, domainIdType, user, params);
        return TO_TASK_SCREEN;
    }

    //    protected String forwardOrComplete(Map<String, Object> params, long domainId,
    //            CompleteTaskCommand.Type domainIdType, String forwardToGroup) {
    //        String user = this.getUser();
    //        List<String> roles = systemUserBp.getRolesForUser(user);
    //        if (roles.contains(forwardToGroup)) {
    //            userTaskService.suspendTask(taskId, user);
    //            userTaskService.completeTask(taskId, domainId, domainIdType, user, params);
    //        }
    //        else {
    //            userTaskService.forwardTask(taskId, user, forwardToGroup);
    //        }
    //        return TO_TASK_SCREEN;
    //    }

    public String getUser() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.getUserPrincipal().getName();
    }

    public long getTaskId() {
        return this.taskId;
    }
}
