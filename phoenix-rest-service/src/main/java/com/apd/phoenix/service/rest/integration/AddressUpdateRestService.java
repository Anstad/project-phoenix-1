/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.rest.integration;

import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.AddressUpdateDto;
import com.apd.phoenix.service.rest.workflow.InvoiceWorkflowService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@Path("/addresses")
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class AddressUpdateRestService {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceWorkflowService.class);
    public static final int ADDRESS_UPDATE_BATCH_SIZE = 400;

    @Inject
    AddressBp addressService;

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    @POST
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateAddresses(AddressUpdateDto orgRelationshipsDto) {
        Response response = Response.serverError().build();
        try {
            if(tm.getTransaction()==null){
                tm.begin();                
            }
            if (orgRelationshipsDto.getAddresses().size() > ADDRESS_UPDATE_BATCH_SIZE) {
                AddressUpdateDto batchDto = new AddressUpdateDto();
                batchDto.setCompanyName(orgRelationshipsDto.getCompanyName());
                batchDto.setAddresses(new ArrayList<AddressDto>());
                List<AddressDto> batchAddreses = orgRelationshipsDto.getAddresses().subList(0, ADDRESS_UPDATE_BATCH_SIZE);
                batchDto.getAddresses().addAll(batchAddreses);
                addressService.updateIntegratedCustomerAddresses(batchDto);
                LOG.info("Committig batch of " + Integer.toString(ADDRESS_UPDATE_BATCH_SIZE) + " addresses from 816.");
                tm.commit();
                orgRelationshipsDto.setAddresses(orgRelationshipsDto.getAddresses().subList(ADDRESS_UPDATE_BATCH_SIZE,
                        orgRelationshipsDto.getAddresses().size()));
                updateAddresses(orgRelationshipsDto);
            }
            else {
                addressService.updateIntegratedCustomerAddresses(orgRelationshipsDto);                
                LOG.info("Committig last addresses from 816.");
                tm.commit();
                LOG.info("Successfully loaded all addresses from 816.");
            }
            response = Response.ok().build();
        } catch (SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            LOG.error("FAILED TO UPDATE ADDRESSES " + ex.getMessage());
            tm.rollback();
        }
        finally{
            return response;
        }
    }
}
