package com.apd.phoenix.service.rest.tax;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.MilitaryZipBp;
import com.apd.phoenix.service.business.TaxRateBp;
import com.apd.phoenix.service.business.ZipPlusFourBp;
import com.apd.phoenix.service.model.MilitaryZip;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.ZipPlusFour;

@Path("/tax")
@Stateless
public class TaxService {

    private static final Logger LOG = LoggerFactory.getLogger(TaxService.class);

    @Inject
    TaxRateBp taxRateBp;

    @Inject
    ZipPlusFourBp zipPlusFourBp;

    @Inject
    MilitaryZipBp militaryZipBp;

    @Path("/bulkUpdate")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response bulkUpdate(List<TaxRate> rates) {
        taxRateBp.createTaxRates(rates);
        return Response.ok().build();
    }

    @Path("/delete")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response delete(TaxRate rate) {
        LOG.info("Deleting rate record {}, {}", rate.getZip(), rate.getGeocode());
        taxRateBp.deleteTaxRate(rate);
        return Response.ok().build();
    }

    //Zipplusfour
    @Path("/zipPlusFour/bulkUpdate")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response zipPlusFourBulkUpdate(List<ZipPlusFour> zips) {
        zipPlusFourBp.createZipPlusFours(zips);
        return Response.ok().build();
    }

    @Path("/zipPlusFour/delete")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response zipPlusFourDelete(ZipPlusFour zip) {

        LOG.info("Deleting zip record {},{}", zip.getZip(), zip.getHi());
        zipPlusFourBp.deleteZipPlusFour(zip);
        return Response.ok().build();
    }

    //military zip
    @Path("/militaryZip/bulkUpdate")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response militaryZipBulkUpdate(List<MilitaryZip> zips) {
        militaryZipBp.createMilitaryZips(zips);
        return Response.ok().build();
    }

    @Path("/militaryZip/delete")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response militaryZipDelete(MilitaryZip zip) {

        LOG.info("Deleting zip record {},{}", zip.getZip(), zip.getCity());
        militaryZipBp.deleteMilitaryZip(zip);
        return Response.ok().build();
    }
}
