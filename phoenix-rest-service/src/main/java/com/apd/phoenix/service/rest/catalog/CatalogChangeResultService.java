/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.rest.catalog;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.catalog.CatalogChangeResult;
import com.apd.phoenix.service.catalog.CatalogResultAggregator;
import com.apd.phoenix.service.rest.workflow.InvoiceWorkflowService;

@Path("/catalogChangeResult")
@Stateless
public class CatalogChangeResultService {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceWorkflowService.class);

    @Inject
    CatalogResultAggregator catalogResultAggregator;

    @POST
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    public void processResult(CatalogChangeResult message) {
        //        LOG.info("Message recieved at endpoint!");
        catalogResultAggregator.process(message);
        //        LOG.info("Message processed at endpoint!");
    }
}
