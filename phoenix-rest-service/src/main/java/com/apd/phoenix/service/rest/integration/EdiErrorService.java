/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.rest.integration;

import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.rest.workflow.InvoiceWorkflowService;
import com.apd.phoenix.service.utility.ErrorEmailService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@Path("/errorEmail")
@Stateless
public class EdiErrorService {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceWorkflowService.class);

    @Inject
    ErrorEmailService errorEmailService;

    @POST
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    public void sendErrorEmail(EdiDocumentErrorsDto errorDto) {
        errorEmailService.createErrorEmail(errorDto);
    }
}
