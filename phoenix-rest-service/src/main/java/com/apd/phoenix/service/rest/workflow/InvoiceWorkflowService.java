/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.rest.workflow;

import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.workflow.WorkflowService;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
@Path("/workflow/invoice")
@Stateless
public class InvoiceWorkflowService {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceWorkflowService.class);

    @Inject
    WorkflowService workflowService;

    @POST
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createInvoice(VendorInvoiceDto invoiceDto) {
        LOG.info("Sending invoiceDto.");
        //workflowService.processInvoice(invoiceDto);
        return Response.ok().build();
    }

}
