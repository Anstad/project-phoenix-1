package com.apd.phoenix.service.rest.message;

import java.io.IOException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.PoNumberBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.persistence.jpa.OrderLogDao;

@Path("/messages")
@Stateless
public class MessageRestService {

    private static final Logger LOG = LoggerFactory.getLogger(MessageRestService.class);

    @Inject
    MessageService messageService;

    @Inject
    PoNumberBp poNumberBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    OrderLogDao orderLogDao;

    @Inject
    MessageMetadataDao messageMetadataDao;

    @Path("/ping")
    @GET
    public Response sanityCheck(Object object) {

        String response = "Ping";
        return Response.ok(response).build();
    }

    @Path("/{messageEventType}/{token}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createMessageForOrder(MessageDto messageDto, @PathParam("token") String token,
            @PathParam("messageEventType") String messageEventType) throws IOException {

        LOG.info("Creating message for token: {}", token);
        if (!messageService.receiveOrderMessage(messageDto, token, messageEventType)) {
            return Response.serverError().build();
        }
        return Response.ok().build();
    }

}
