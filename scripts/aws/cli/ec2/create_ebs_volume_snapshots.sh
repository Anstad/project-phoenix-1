#!/bin/bash

# Create snapshots of all EBS volumes for EC2 instances running in a particular AWS VPC environment:
AWS_ENV_ID=$1
AWS_VPC_ID=""

if [ "$AWS_ENV_ID" = "DEV" ] 
then
   AWS_VPC_ID="vpc-0392be6e"
elif [ "$AWS_ENV_ID" = "QA" ] 
then
   AWS_VPC_ID="vpc-9c9467f9"
elif [ "$AWS_ENV_ID" = "PROD" ] 
then
   AWS_VPC_ID="vpc-77b4ba15"
else
   AWS_VPC_ID=""
fi

if [ "x$AWS_VPC_ID" = "x" ]
then
   echo "Snapshot creation FAILED.  One of the following environment IDs must be provided: (DEV, QA, PROD)"
else
   echo "Creating snapshots of all EBS volumes in the $AWS_ENV_ID environment (e.g. vpc-id=$AWS_VPC_ID)"
   ec2-describe-instances --filter "vpc-id=$AWS_VPC_ID" | grep "BLOCKDEVICE" | cut -f3 | awk '{ system("ec2-create-snapshot "$0)}'
fi
