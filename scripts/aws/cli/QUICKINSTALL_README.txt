Instructions (entire installation should take no more than 15 minutes)
----------------------------------------------------------------------

* Unzip aws-cli.zip to a sensible location on the local filesystem (e.g. "/Applications/aws-cli" on Mac or "/usr/share/aws-cli" on Linux)
* Follow instructions in the file named "README (use this for installing the cli tools).txt"