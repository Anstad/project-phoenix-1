INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'text', 0);
INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'dropdown', 0);
INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'radio', 0);
INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'checkbox', 0);
INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'calendar', 0);
INSERT INTO FIELDTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'output', 0);
	
--/* Shipping information */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'requester phone', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'requester name', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship name', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship company', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship address 1', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship address 2', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship city', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship states', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship zip', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship north america', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship us only', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship us and canada', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'existing shipto address selection', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'desktop', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'building or department', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'building or department combobox', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'building or department free', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'cost center', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'cost center combobox', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'cost center free', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'shipping method', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship extra 1', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship extra 2', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship extra 3', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
--/* Billing information */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'custom po number', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'blanket po number', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'email', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'credit card name on card', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'credit card number', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'credit card expiration', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'calendar'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'credit card CVV', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'payment type selection', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'Credit Card', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'payment type selection'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'Invoice', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'payment type selection'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill name', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill company', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill address 1', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill address 2', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill city', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill states', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill zip', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill north america', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill us only', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill us and canada', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'existing billto address selection', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill extra 1', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill extra 2', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'bill extra 3', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
--/* Other user input */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'comments', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
	--/* TODO: datamodel change */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'line item comments', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
	--/* Dates. TODO: remove boolean fields on CashoutPage */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'ship date', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'calendar'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'delivery date', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'calendar'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'cancel date', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'calendar'));
	
--/* Customer service input */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'order origin', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'order type', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'contracting officer', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'text'));
	
--/* Cost output */

INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'shipping cost', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'output'));
	
INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'handling cost', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'output'));
	
INSERT INTO FIELD (ID, NAME, VERSION, FIELDTYPE_ID) VALUES (hibernate_sequence.nextval, 'credit card CVV indicator', 0, 
	(SELECT t.id FROM FIELDTYPE t WHERE t.name LIKE 'dropdown'));

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'AddressID');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'AddressID(base 36)');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Contact Name');
	
INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Contact Phone');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Desktop');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:APD_InterchangeID');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:CarrierRouting');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:DeliveryDate');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:Dept');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:Facility');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:Fedex No');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:LocationID');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:Mailstop');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:NOWL');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:OrderType');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:POL');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:SolomonID');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'EXT:USAccount');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Header Comments');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'OldRelease');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Order Email');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Street 2');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Street 3');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Requester Phone');

INSERT INTO MESSAGEMAPPING (ID, VERSION, NAME) VALUES (hibernate_sequence.nextval, 0,
	'Requester');	
	
@field/country_fields;


@field/ship_state_fields;


@field/bill_state_fields;


@field/shipping_method;
	
