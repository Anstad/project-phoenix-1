INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Dated Goods', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Binding Filing Labeling', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Ink and Toner', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'OEM', 0, (SELECT ic.id FROM ITEMCATEGORY ic WHERE ic.name LIKE 'Ink and Toner'));

INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Reman', 0, (SELECT ic.id FROM ITEMCATEGORY ic WHERE ic.name LIKE 'Ink and Toner'));

INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Paper', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'General Supplies', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Breakroom/Janitorial/Maint.', 0, null);

INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Writing Instruments', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Office/Desk Accessories', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Business Machines', 0, null);

INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Computer Equipment', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Furniture', 0, null);
	
INSERT INTO ITEMCATEGORY (ID, NAME, VERSION, PARENT_ID)
	VALUES (hibernate_sequence.nextval, 'Fee', 0, null);
	