INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'changed sku number', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'status', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'comment', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'standardprice', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'list price', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'street', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'GSAprice', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL)
        VALUES (hibernate_sequence.nextval, 'GSAindicator', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'AFprice', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'APDcost', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'quantity per unit', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'multiples', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'UPSable', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'isJWOD', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'TAA', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'NON-TAA', 0, 0, 
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/nottaa.png', 'Non TAA Item');
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'EPP', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'hazmat', 0, 0, 
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/hazmat.png', 'hazmat');
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'specialOrder', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'quantity', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'discontinuedDate', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'previousSku', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'customOrder', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'ets_list', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'availability', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'icons', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'force substitute', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'description', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'taxable', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'last calculated price', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'last calculated profit', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'current profit', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'last price change date', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL)
        VALUES (hibernate_sequence.nextval, 'MBE', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'walmart', 0, 0, 
'https://d2eodrsfnjvy5e.cloudfront.net/phoenix-icons/walmart-icon.gif', 'Customer Specific Icon Example');
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'customerReplacementSku', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'customerReplacementVendor', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'substituteSku', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'substituteVendor', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'substituteReason', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'image URL', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'thumb URL', 0, 0, null, null);
INSERT INTO ITEMPROPERTYTYPE (ID, NAME, SUPPLEMENTARY, VERSION, ICONURL, TOOLTIPLABEL) 
	VALUES (hibernate_sequence.nextval, 'unit of measure', 0, 0, null, null);
