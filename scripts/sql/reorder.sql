INSERT INTO LINEITEM (ID, ITEM_ID, ORDER_ID, DESCRIPTION, ESTIMATEDSHIPPINGAMOUNT, QUANTITY, UNITPRICE, UNITOFMEASURE_ID, version, LINENUMBER)
	VALUES(hibernate_sequence.nextval,
		(SELECT ITEM.id FROM ITEM 
			WHERE ITEM.description LIKE 'Weekly planner with To Do, Notes and Special Information tabbed sections. Simulated leather covers with storage pocket.'),
		(SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1337 AND rownum = 1),
		'Weekly planner with To Do, Notes and Special Information tabbed sections. Simulated leather covers with storage pocket.',
		1337, 10,5,
		(SELECT UNITOFMEASURE.id FROM UNITOFMEASURE
			WHERE UNITOFMEASURE.name='EA'),3,1
	);
INSERT INTO LINEITEM (ID, ITEM_ID, ORDER_ID,DESCRIPTION,ESTIMATEDSHIPPINGAMOUNT,QUANTITY,UNITPRICE,UNITOFMEASURE_ID,version, LINENUMBER)
	VALUES(hibernate_sequence.nextval,
		(SELECT ITEM.id FROM ITEM 
			WHERE ITEM.description LIKE 'Record book features a black hardbound cover highlighted with maroon spine and corners. Numbered pages and acid-free paper.'),
		(SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1337 AND rownum = 1),
		'Record book features a black hardbound cover highlighted with maroon spine and corners. Numbered pages and acid-free paper.',
		1337, 15,51,
		(SELECT UNITOFMEASURE.id FROM UNITOFMEASURE
			WHERE UNITOFMEASURE.name='EA'),3,2
	);
INSERT INTO LINEITEM (ID, ITEM_ID, ORDER_ID,DESCRIPTION,ESTIMATEDSHIPPINGAMOUNT,QUANTITY,UNITPRICE,UNITOFMEASURE_ID,version, LINENUMBER)
	VALUES(hibernate_sequence.nextval,
		(SELECT ITEM.id FROM ITEM 
			WHERE ITEM.description LIKE 'Non-woven fabric can be cut, sewn, stapled, drawn on and more.'),
		(SELECT CUSTOMERORDER.id FROM CUSTOMERORDER 
			WHERE customerOrder.ESTIMATEDSHIPPINGAMOUNT=1337 AND rownum = 1),
		'Non-woven fabric can be cut, sewn, stapled, drawn on and more.',
		1337, 910,3,
		(SELECT UNITOFMEASURE.id FROM UNITOFMEASURE
			WHERE UNITOFMEASURE.name='EA'),3,3
	);

