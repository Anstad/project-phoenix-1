--/* Initialization */

@disable_constraints;

@truncate_data;

@enable_constraints;

CREATE TABLE IDS_USED (NAME varchar2(255), ID number);

--/* Upload Data */

@fields;

@roles_and_permissions;

@credential/properties;

@credential/communication_methods;

@catalog/item;

@po_types;

@service_reason;

@contact_method;

@tax_types;

@general/props.sql;

@account/account_props.sql;

@account/payment_props.sql;

@order/order_props;

@Data_Load_Full;

@sequences;


--/* Loading data necessary for administration (will be removed) */

@admin_users;


--/* Upload demo data */
@account/account.sql;

@account/payment.sql;

@account/address_and_phone.sql;

@credential/cintas_apdstore;

@catalog/USSCO_vendor_catalog;

@catalog/vendor_address_and_phone.sql;

@users/cintas_users

@order/cost_center.sql

@order/customer_order.sql


@solr_items;

@account/reports_accounts.sql;

@order/reports_orders.sql;

@order_log.sql;

@reorder;
@credential_types;
@test_card;


--/* Create Views */
@views/phone_views.sql

@views/address_views.sql

@views/address_groups_list_view.sql

@views/credential_views.sql

@views/catalog_views.sql

@cxml/cxml_data.sql


@item_properties.sql;


@validation_errors.sql;


--/* cleanup */
DROP TABLE IDS_USED;
SPOOL off
