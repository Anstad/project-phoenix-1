INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the placing of orders', 'create order', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit order', 'edit order', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to view reports', 'view report', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the user to create reports', 'add report', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to browse the catalogs assigned to them', 'browse catalog', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the user to cashout of the shopping site', 'cashout', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add a credential', 'add credential', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit a credential', 'edit credential', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add a vendor catalog', 'add vendor catalog', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit a vendor catalog', 'edit vendor catalog', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add a customer catalog', 'add customer catalog', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit a customer catalog', 'edit customer catalog', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add a user', 'add user', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit a user', 'edit user', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add an account', 'add account', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit an account', 'edit account', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add an approval process', 'add approval process', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit an approval process', 'edit approval process', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add system data', 'add system data', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit system data', 'edit system data', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to add cashout page', 'add cashout page', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the employee to edit cashout page', 'edit cashout page', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 1, 'Allow the user to approve orders', 'approve orders', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 0, 1, 'Allow the employee to view payment information, including CC numbers', 'view payment information', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "Place Special Order" link in the shopping site.', 'shopping view special order form', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "Place Custom Order" link in the shopping site.', 'shopping view custom order form', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "Toner Recycling Program" link in the shopping site.', 'shopping view toner recycle form', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "Rebate Center" link in the shopping site.', 'shopping view rebate center', 0);
insert into permission p 
(ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION)
VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "User Guide" link in the shopping site.', 'shopping view user guide', 0);

insert into permission p 
(ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION)
VALUES (hibernate_sequence.nextval, 0, 1, 0, 'View the "Returns Guide" link in the shopping site.', 'shopping view returns guide', 0);

insert into permission p 
(ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION)
VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to access task management module in the shopping site.', 'shopping view tasks', 0);
	
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to backorder items if they are currently out of stock.', 'shopping order backordered items', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to place an order. A credential without this is a demo.', 'shopping live credential', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to add items. A credential without this is a "browse" credential.', 'shopping add to cart', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to add items with a price of zero dollars.', 'shopping add zero price to cart', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Show the "Open Market" banner above the shopping site.', 'shopping show open market label', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to add and remove personal favorites lists.', 'shopping maintain user favorites', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to view all orders placed with their account.', 'shopping view account history', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to view all orders placed with their credential.', 'shopping view credential history', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allows the user to see their issue logs in shopping.', 'shopping view issue logs', 0);

INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to span root accounts.', 'can span root accounts', 0);
	
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to log in to the administration application.', 'login to admin', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to log in to the ecommerce application.', 'login to shopping', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allow the user to log in to the customer service application.', 'login to customer service', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Permits full visibility of DB results from all Accounts.', 'view data for all accounts', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allows the user to see all claimed tasks.', 'customer service manager', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allows the user to view all notifications.', 'customer service view all messages', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allows the user to view Purchase Order notifications.', 'customer service view message Purchase Order Sent', 0);
INSERT INTO PERMISSION (ID, ALLOWDELETE, ALLOWREAD, ALLOWWRITE, DESCRIPTION, NAME, VERSION) 
	VALUES (hibernate_sequence.nextval, 0, 1, 0, 'Allows the user to view Purchase Order notifications.', 'customer service view dynamics/jumptrack', 0);
	
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Super User Level 1', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Super User Level 2', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Super User Level 3', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'User', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Browser', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Main Administrator', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Adminstrator', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Admin', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Customer Service', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'APD Customer Service', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Accounting', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Sales', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Merchandising', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Marketing', 0);
INSERT INTO ROLE (ID, ISACTIVE, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 1, 'Order Approver', 0);

INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add credential'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit credential'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add vendor catalog'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit vendor catalog'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add customer catalog'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit customer catalog'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add user'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit user'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add account'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit account'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add approval process'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit approval process'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add system data'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit system data'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'add cashout page'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit cashout page'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'view payment information'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Main Administrator'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'view data for all accounts'));
	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'User'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'create order'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'User'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'browse catalog'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'User'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'cashout'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'User'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'login to shopping'));

insert into role_permission 
(roles_id, permissions_id)
values ((select id from role r where r.NAME = 'User'),(select id from permission p where p.NAME = 'shopping view user guide'));

insert into role_permission 
(roles_id, permissions_id)
values ((select id from role r where r.NAME = 'User'),(select id from permission p where p.NAME = 'shopping view returns guide'));

	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Admin'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'login to admin'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Admin'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'view data for all accounts'));
	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Browser'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'browse catalog'));
	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Order Approver'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'approve orders'));
	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Customer Service'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'create order'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Customer Service'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'edit order'));
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Customer Service'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'login to customer service'));
	
INSERT INTO ROLE_PERMISSION (ROLES_ID, PERMISSIONS_ID)
	VALUES ((SELECT r.ID FROM ROLE r WHERE r.NAME LIKE 'Accounting'), (SELECT p.ID FROM PERMISSION p WHERE p.NAME LIKE 'view report'));
	
	
	
	
	
	
	
	
	
	
