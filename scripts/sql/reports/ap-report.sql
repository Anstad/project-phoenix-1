
SELECT 
rootAccount.name AS "Customer Name", 
--/* 810 Shipto Name (?) */
--/* Solomon Vendor ID */
--/* OpRule Vendor Account # */
apdPo.value AS "APD PO", 
customerPo.value AS "Customer PO", 
--/* Tracking Number (?) */
vendor.name AS "Remit To", 
invoice.invoiceNumber AS "Invoice Number", 
--/* 810 Status (?) */
'Order Total' AS "Quantity",
SUM(LINEITEM.QUANTITY * COALESCE(LINEITEM.UNITPRICE, 0)) AS "Merchandise Price", 
SUM(COALESCE(LINEITEM.maximumTaxToCharge, 0)) AS "Tax", 
SUM(COALESCE(LINEITEM.ESTIMATEDSHIPPINGAMOUNT, 0)) AS "Shipping, Restocking Fee", 
INVOICE.AMOUNT,
customerorder.orderDate AS "Order Date", 
invoice.createdDate AS "Invoice Date" 
FROM INVOICE 
LEFT JOIN CUSTOMERORDER ON (INVOICE.CUSTOMERORDER_ID = CUSTOMERORDER.ID)
LEFT JOIN ADDRESS shipAddress ON (CUSTOMERORDER.ADDRESS_ID = shipAddress.ID)
LEFT JOIN PONUMBER_CUSTOMERORDER apdPoJoin 
	JOIN PONUMBERTYPE apdPoType ON (apdPoType.NAME = 'APD')
	JOIN PONUMBER apdPo 
	ON (apdPo.ID = apdPoJoin.PONUMBERS_ID AND apdPo.TYPE_ID = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN PONUMBER_CUSTOMERORDER customerPoJoin 
	JOIN PONUMBERTYPE customerPoType ON (customerPoType.NAME = 'Customer')
	JOIN PONUMBER customerPo 
	ON (customerPo.ID = customerPoJoin.PONUMBERS_ID AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID = customerPoJoin.ORDERS_ID)
LEFT JOIN CREDENTIAL ON (CUSTOMERORDER.CREDENTIAL_ID = CREDENTIAL.ID)
LEFT JOIN Credential_PropertyType glAccount 
	JOIN CREDENTIALPROPERTYTYPE glCredentialPropertyType 
	ON (glAccount.TYPE_ID = glCredentialPropertyType.ID AND glCredentialPropertyType.NAME = 'AR GL account')
ON (glAccount.CRED_ID = CREDENTIAL.ID)
LEFT JOIN Credential_PropertyType subAccount 
	JOIN CREDENTIALPROPERTYTYPE subCredentialPropertyType 
	ON (subAccount.TYPE_ID = subCredentialPropertyType.ID AND subCredentialPropertyType.NAME = 'AR subaccount')
ON (subAccount.CRED_ID = CREDENTIAL.ID)
LEFT JOIN ACCOUNT thisAccount ON (thisAccount.ID = CUSTOMERORDER.ACCOUNT_ID)
LEFT JOIN ACCOUNT rootAccount ON (thisAccount.ROOTACCOUNT_ID = rootAccount.ID OR (thisAccount.ID = rootAccount.ID AND thisAccount.ROOTACCOUNT_ID IS NULL))
LEFT JOIN INVOICE_LINEITEM ON (INVOICE.ID = INVOICE_LINEITEM.INVOICE_ID)
LEFT JOIN LINEITEM ON (INVOICE_LINEITEM.ACTUALITEMS_ID = LINEITEM.ID)
LEFT JOIN VENDOR ON (LINEITEM.VENDOR_ID = VENDOR.ID)
WHERE (invoice.FROM_CLASS = 'VENDOR_CREDIT_INVOICE' OR invoice.FROM_CLASS = 'VENDOR_INVOICE')
GROUP BY INVOICE.ID, rootAccount.name, apdPo.value, customerPo.value, vendor.name, invoice.invoiceNumber, 
customerorder.orderDate, invoice.createdDate, INVOICE.AMOUNT;

