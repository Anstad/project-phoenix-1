
SELECT 
thisAccount.name AS "Account Name", 
vendor.name AS "Vendor", 
apdPo.value AS "APD PO", 
customerPo.value AS "Customer PO", 
customerorder.orderDate AS "Order Date", 
invoice.createdDate AS "Invoice Date",
invoice.invoiceNumber AS "Invoice Number", 
actualLineItem.apdSku AS "APD SKU", 
actualLineItem.supplierPartId AS "Vendor SKU", 
actualLineItem.shortName AS "Name", 
lineItemStatus.value AS "Sale Status", 
COALESCE(expectedLineItem.cost, 0) AS "Expected Unit Cost", 
COALESCE(actualLineItem.cost, 0) AS "Actual Unit Cost", 
COALESCE(expectedLineItem.cost, 0) * expectedLineItem.quantity AS "Expected Total Cost", 
COALESCE(actualLineItem.cost, 0) * actualLineItem.quantity AS "Actual Total Cost"
FROM INVOICE 
LEFT JOIN CUSTOMERORDER ON (INVOICE.CUSTOMERORDER_ID = CUSTOMERORDER.ID)
LEFT JOIN PONUMBER_CUSTOMERORDER apdPoJoin 
	JOIN PONUMBERTYPE apdPoType ON (apdPoType.NAME = 'APD')
	JOIN PONUMBER apdPo 
	ON (apdPo.ID = apdPoJoin.PONUMBERS_ID AND apdPo.TYPE_ID = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN PONUMBER_CUSTOMERORDER customerPoJoin 
	JOIN PONUMBERTYPE customerPoType ON (customerPoType.NAME = 'Customer')
	JOIN PONUMBER customerPo 
	ON (customerPo.ID = customerPoJoin.PONUMBERS_ID AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID = customerPoJoin.ORDERS_ID)
LEFT JOIN ACCOUNT thisAccount ON (thisAccount.ID = CUSTOMERORDER.ACCOUNT_ID)
LEFT JOIN CREDENTIAL ON (CREDENTIAL.ID = CUSTOMERORDER.CREDENTIAL_ID)
LEFT JOIN Credential_PropertyType thresholdProperty
	JOIN CREDENTIALPROPERTYTYPE thresholdPropertyType
	ON (thresholdProperty.TYPE_ID = thresholdPropertyType.ID AND thresholdPropertyType.NAME = 'cost-price discrepancy threshold')
ON (thresholdProperty.CRED_ID = CREDENTIAL.ID)
LEFT JOIN INVOICE_LINEITEM ON (INVOICE.ID = INVOICE_LINEITEM.INVOICE_ID)
LEFT JOIN LINEITEM actualLineItem ON (INVOICE_LINEITEM.ACTUALITEMS_ID = actualLineItem.ID)
LEFT JOIN LINEITEM expectedLineItem ON (actualLineItem.EXPECTED_ID = expectedLineItem.ID)
LEFT JOIN VENDOR ON (actualLineItem.VENDOR_ID = VENDOR.ID)
LEFT JOIN LINEITEMSTATUS ON (actualLineItem.STATUS_ID = LINEITEMSTATUS.ID)
WHERE (COALESCE(actualLineItem.cost, 0) - COALESCE(expectedLineItem.cost, 0)) / GREATEST(COALESCE(expectedLineItem.cost, 0), 0.01) > COALESCE(TO_NUMBER(thresholdProperty.VALUE), 0.1);

