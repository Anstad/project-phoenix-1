INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Return - Damaged Item', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Invoices', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Credits', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Cancel Order - Incorrect Item', 0);
	
INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Order Status', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'General Inquiry', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Customer Maintenance', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'Technical Issue', 0);

INSERT INTO SERVICEREASON (ID, NAME, VERSION)
	VALUES (hibernate_sequence.nextval, 'NA', 0);