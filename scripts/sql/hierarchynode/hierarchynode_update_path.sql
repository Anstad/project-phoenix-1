DECLARE
	-- this script is used to generate the indexedPaths for the hierarchy nodes.
	-- for more information, see: http://wiki.apache.org/solr/HierarchicalFaceting

	-- used to determine which value on a node will determine the path
	-- values can include DESCRIPTION, ID, and PUBLICID
	-- NOTE: If this changes, change it in the generatePath method of HierarchyNode.java
	pathvalue VARCHAR(31) := 'DESCRIPTION';
	-- these are the delimeters that can be set for the database strings
	-- this delimits the different nodes in the path
	-- NOTE: If this changes, change it in HierarchyNode.java
	pathdelimeter VARCHAR(7) := '!';
	-- this delimits the different paths
	-- NOTE: If this changes, change it in HierarchyNode.java
	parentpath VARCHAR(4000);
	parentdirectpath VARCHAR(4000);
	depth_level VARCHAR(7) := '';
BEGIN
   FOR node IN (SELECT * FROM HIERARCHYNODE ORDER BY PUBLICID ASC)
   LOOP
      BEGIN
     	SELECT parentnode.INDEXEDPATHS INTO parentpath FROM HIERARCHYNODE parentnode WHERE parentnode.ID = node.PARENT_ID;
	    parentdirectpath := parentpath;	   
	    parentdirectpath := SUBSTR ( parentdirectpath, INSTR(parentdirectpath, pathdelimeter) );
     	EXECUTE IMMEDIATE 'UPDATE HIERARCHYNODE u SET u.INDEXEDPATHS='''||''||''
     		||depth_level||parentdirectpath||pathdelimeter||'''||u.'||pathvalue||' WHERE u.ID = '||node.ID;
      EXCEPTION
      	WHEN OTHERS THEN
	         EXECUTE IMMEDIATE 'UPDATE HIERARCHYNODE u SET u.INDEXEDPATHS='''||pathdelimeter||
	         	'''||u.'||pathvalue||' WHERE u.ID = '||node.ID;
      END;
   END LOOP;
   
END;
/
