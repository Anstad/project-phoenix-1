-- Creates View of all Account Ids and related Full Phone Numbers
Create View Account_PhoneNumber_Pivot as
WITH Account_PhoneNumber_pivot AS (
            SELECT PHONENUMBERTYPE.Name, PHONENUMBER.Account_ID,
            (NVL2( PhoneNumber.AREACODE ,'('||PhoneNumber.AREACODE||') ','')   || NVL2(PhoneNumber.EXCHANGE,PhoneNumber.EXCHANGE || '-','')|| PhoneNumber.LINENUMBER || NVL2(PhoneNumber.EXTENSION,' xt:'||PhoneNumber.EXTENSION,'')) As Phone_Number
            From PhoneNumber Inner Join PHONENUMBERTYPE on PhoneNumber.Type_id = PhoneNumberType.id
            )
    SELECT *
    FROM   Account_PhoneNumber_pivot
    PIVOT (
           Max(Phone_Number)        --<-- pivot_clause
           FOR Name          --<-- pivot_for_clause
           IN  ('Home' as Home,'Work' as Work,'Fax' as Fax ,'Cell' as Cell)   --<-- pivot_in_clause
         )
          Where Account_ID is not null;
          
-- Creates View of all Person Ids and related Full Phone Numbers
Create View Person_PhoneNumber_Pivot as
WITH Person_PhoneNumber_pivot AS (
            SELECT PHONENUMBERTYPE.Name, PHONENUMBER.Person_ID,
            (NVL2( PhoneNumber.AREACODE ,'('||PhoneNumber.AREACODE||') ','')   || NVL2(PhoneNumber.EXCHANGE,PhoneNumber.EXCHANGE || '-','')|| PhoneNumber.LINENUMBER || NVL2(PhoneNumber.EXTENSION,' xt:'||PhoneNumber.EXTENSION,'')) As Phone_Number
            From PhoneNumber Inner Join PHONENUMBERTYPE on PhoneNumber.Type_id = PhoneNumberType.id
            )
    SELECT *
    FROM   Person_PhoneNumber_pivot
    PIVOT (
           Max(Phone_Number)        --<-- pivot_clause
           FOR Name          --<-- pivot_for_clause
           IN  ('Home' as Home,'Work' as Work,'Fax' as Fax ,'Cell' as Cell)   --<-- pivot_in_clause
         )
          Where Person_ID is not null;

-- Creates View of all Vendor Ids and related Full Phone Numbers
Create View Vendor_PhoneNumber_Pivot as
WITH Vendor_PhoneNumber_Pivot AS (
            SELECT PHONENUMBERTYPE.Name, PHONENUMBER.Vendor_ID,
            (NVL2( PhoneNumber.AREACODE ,'('||PhoneNumber.AREACODE||') ','')   || NVL2(PhoneNumber.EXCHANGE,PhoneNumber.EXCHANGE || '-','')|| PhoneNumber.LINENUMBER || NVL2(PhoneNumber.EXTENSION,' xt:'||PhoneNumber.EXTENSION,'')) As Phone_Number
            From PhoneNumber Inner Join PHONENUMBERTYPE on PhoneNumber.Type_id = PhoneNumberType.id
            )
    SELECT *
    FROM   Vendor_PhoneNumber_pivot
    PIVOT (
           Max(Phone_Number)        --<-- pivot_clause
           FOR Name          --<-- pivot_for_clause
           IN  ('Home' as Home,'Work' as Work,'Fax' as Fax ,'Cell' as Cell)   --<-- pivot_in_clause
         )
          Where Vendor_ID is not null;