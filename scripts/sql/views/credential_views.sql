-- Creates single Column of comma seperated Credential.NAME(s) and Credential.PUNCHOUTIDENTITY(ies) based on Account relationship
Create View AccountCredential_List As
SELECT
     ACCOUNT.ID AS ACCOUNT_ID,
     ACCOUNT.NAME AS ACCOUNT_NAME,
     listagg (Credential.NAME, ',') WITHIN GROUP (ORDER BY Credential.NAME) CredentialNames,
     listagg (Credential.PUNCHOUTIDENTITY, ',') WITHIN GROUP (ORDER BY Credential.PUNCHOUTIDENTITY) CredentialPunchouts 
FROM
      ACCOUNT
      Inner Join Credential On ACCOUNT.ID = Credential.ROOTACCOUNT_ID
      Where Account.ROOTACCOUNT_ID IS null
      Group By Account.Name, Account.ID 
      Order By Account.ID;
      
