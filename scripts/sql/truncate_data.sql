BEGIN
  FOR t IN
  (SELECT t.table_name
   FROM user_tables t)
  LOOP
    dbms_utility.exec_ddl_statement('truncate table "' || t.table_name || '"');
  END LOOP;
  
  FOR v IN
  (SELECT v.view_name
   FROM user_views v)
  LOOP
    dbms_utility.exec_ddl_statement('drop view "' || v.view_name || '"');
  END LOOP;
END;
/