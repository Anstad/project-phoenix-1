INSERT INTO CARDINFORMATION (ID, DEBIT, EXPIRATION, NAME_ON_CARD, CARDVAULTTOKEN, GHOST, VERSION) 
VALUES (hibernate_sequence.nextval, 0, '01-Mar-2018', 'ANDY CONFER', '1234', 0, 0);
INSERT INTO CARDINFORMATION (ID, DEBIT, EXPIRATION, NAME_ON_CARD, CARDVAULTTOKEN, GHOST, VERSION) 
VALUES (hibernate_sequence.nextval, 1, '01-Jan-2015', 'Joe Maloney', '5678', 0, 0);


INSERT INTO PERSON (ID, EMAIL, FIRSTNAME, LASTNAME, MIDDLEINITIAL, PREFIX, SUFFIX, VERSION) 
VALUES (hibernate_sequence.nextval, 'ebuzz@cmc.com', 'Buzzerio', 'Emily', null, 'Ms', null, 0);
INSERT INTO PERSON (ID, EMAIL, FIRSTNAME, LASTNAME, MIDDLEINITIAL, PREFIX, SUFFIX, VERSION) 
VALUES (hibernate_sequence.nextval, 'aconfer@cintas.com', 'Andy', 'Confer', null, 'Mr', null, 0);
INSERT INTO IDS_USED (NAME, ID) VALUES ('person_id', hibernate_sequence.nextval);
INSERT INTO PERSON (ID, EMAIL, FIRSTNAME, LASTNAME, MIDDLEINITIAL, PREFIX, SUFFIX, VERSION)
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 'maloneyj@cintas.com', 'Joe', 
	'Maloney', null, null, null, 0);
INSERT INTO SYSTEMUSER (ID, CREATIONDATE, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LASTLOGINDATE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PAYMENTINFORMATION_ID, PERSON_ID)
	VALUES (hibernate_sequence.nextval, null, 0, 0, 1, '19-Mar-2013', 'maloneyj', '$2a$12$NCg9YlEHMTDkOy1HpfXcYeS7sZPKiWGW/rxeCBKw04rZDdVyRwAMO', '$2a$12$NCg9YlEHMTDkOy1HpfXcYe', 'active', 0, null, (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'));

--/* Use temp table to store PaymentInformation Ids
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_pay_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cmc_pay_ped_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_pay_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('cintas_young_pay_id', hibernate_sequence.nextval);


INSERT INTO PAYMENTINFORMATION (ID, VERSION, CARD_ID, CONTACT_ID, INVOICESENDMETHOD_ID, PAYMENTTYPE_ID, SUMMARY) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_pay_id'),
		0, null,  
		(select p.id from PERSON p where p.EMAIL like 'ebuzz@cmc.com'), 
		(select i.id from INVOICESENDMETHOD i where i.NAME like 'Mail'),
		(select t.id from PAYMENTTYPE t where t.NAME like 'Invoice'), 0);
		
UPDATE ACCOUNT SET PAYMENTINFORMATION_ID = (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cmc_pay_id') 
	WHERE NAME like '%Pediatric%';

			
			
INSERT INTO PAYMENTINFORMATION (ID, VERSION, CARD_ID, CONTACT_ID, INVOICESENDMETHOD_ID, PAYMENTTYPE_ID, SUMMARY) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_pay_id'), 
		0, 
		(select c.id from CARDINFORMATION c where c.CARDVAULTTOKEN like '12345'),  
		(select p.id from PERSON p where p.EMAIL like 'aconfer@cintas.com'), 
		(select i.id from INVOICESENDMETHOD i where i.NAME like 'Email User'),
		(select t.id from PAYMENTTYPE t where t.NAME like 'Credit Card'), 0);
		
UPDATE ACCOUNT SET PAYMENTINFORMATION_ID = (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_pay_id') 
	WHERE NAME like '%Cintas%';
	
INSERT INTO PAYMENTINFORMATION (ID, VERSION, CARD_ID, CONTACT_ID, INVOICESENDMETHOD_ID, PAYMENTTYPE_ID) 
VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_young_pay_id'), 
		0, 
		(select c.id from CARDINFORMATION c where c.CARDVAULTTOKEN like '5678'),  
		(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'person_id'), 
		(select i.id from INVOICESENDMETHOD i where i.NAME like 'Summary Bill'),
		(select t.id from PAYMENTTYPE t where t.NAME like 'Credit Card'));
		
UPDATE ACCOUNT SET PAYMENTINFORMATION_ID = (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'cintas_young_pay_id') 
	WHERE NAME like '%Young%';

