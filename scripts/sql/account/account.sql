--/* CMC Account Examples - Tree 3 level Deep*/
INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '01/Dec/2010', 1, 'CSS Hospitals And Clinics', 
0, null, null, null, '000000');

INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '01/Dec/2010', 1, 'CMC Main', 
0, 
(Select a.id from ACCOUNT a where a.name = 'CSS Hospitals And Clinics'), 
null, 
(Select a.id from ACCOUNT a where a.name = 'CSS Hospitals And Clinics'), '000000');

INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '14/feb/2012', 1, 'CMC Main- Pediatric Hemo/Oncology', 
0, 
(Select a.id from ACCOUNT a where a.name = 'CMC Main'), 
null, 
(Select a.id from ACCOUNT a where a.name = 'CSS Hospitals And Clinics'), '000000');

--/*Cintas Account Examples*/
INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '11/Jul/1997', 1, 'Cintas', 
0, null, null, null, '000000');

INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '21/May/1998', 1, 'Cintas Youngstown', 
0, 
(Select a.id from ACCOUNT a where a.name = 'Cintas'), 
null, 
(Select a.id from ACCOUNT a where a.name = 'Cintas'), '000000');

INSERT INTO ACCOUNT (CXMLCONFIGURATION_ID, ID, CREATIONDATE, ISACTIVE, NAME, VERSION, PARENTACCOUNT_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SOLOMONCUSTOMERID) 
VALUES (null, hibernate_sequence.nextval, '08/jun/1999', 1, 'Cintas Special Orders', 
0, 
(Select a.id from ACCOUNT a where a.name = 'Cintas'), 
null, 
(Select a.id from ACCOUNT a where a.name = 'Cintas'), '000000');



--/* Account/Account Prop Type Examples */
INSERT INTO ACCOUNTXACCOUNTPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ACCOUNT_ID) 
VALUES (hibernate_sequence.nextval, 'Cintas-Accounts Payable', 0, 
(Select t.id from ACCOUNTPROPERTYTYPE t where t.NAME = 'BillTo Name'), 
(Select a.id from ACCOUNT a where a.NAME = 'Cintas'));

-- INSERT INTO ACCOUNTXACCOUNTPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ACCOUNT_ID) 
-- VALUES (hibernate_sequence.nextval, 'test', 0, 
-- (Select t.id from ACCOUNTPROPERTYTYPE t where t.NAME = 'Subdomain'), 
-- (Select a.id from ACCOUNT a where a.NAME = 'Cintas'));

-- INSERT INTO ACCOUNTXACCOUNTPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ACCOUNT_ID) 
-- VALUES (hibernate_sequence.nextval, 'test', 0, 
-- (Select t.id from ACCOUNTPROPERTYTYPE t where t.NAME = 'Subdomain'), 
-- (Select a.id from ACCOUNT a where a.NAME = 'CSS Hospitals And Clinics'));

INSERT INTO ACCOUNTXACCOUNTPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ACCOUNT_ID) 
VALUES (hibernate_sequence.nextval, 'CMC Accounts Payable', 0, 
(Select t.id from ACCOUNTPROPERTYTYPE t where t.NAME = 'BillTo Name'), 
(Select a.id from ACCOUNT a where a.NAME = 'CSS Hospitals And Clinics'));



--/* CSS Account/Account Group Examples */
INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Hospital'), 
(Select a.id from ACCOUNT a where a.NAME = 'CSS Hospitals And Clinics'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Clinic'), 
(Select a.id from ACCOUNT a where a.NAME = 'CSS Hospitals And Clinics'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'CSS'), 
(Select a.id from ACCOUNT a where a.NAME = 'CSS Hospitals And Clinics'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Hospital'), 
(Select a.id from ACCOUNT a where a.NAME = 'CMC Main'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'CSS'), 
(Select a.id from ACCOUNT a where a.NAME = 'CMC Main'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Clinic'), 
(Select a.id from ACCOUNT a where a.NAME = 'CMC Main- Pediatric Hemo/Oncology'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'CSS'), 
(Select a.id from ACCOUNT a where a.NAME = 'CMC Main- Pediatric Hemo/Oncology'));

--/*Cintas Commerical Account/Account Group Examples */
INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Commercial'), 
(Select a.id from ACCOUNT a where a.NAME = 'Cintas'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Commercial'), 
(Select a.id from ACCOUNT a where a.NAME = 'Cintas Youngstown'));

INSERT INTO ACCOUNT_ACCOUNTGROUP (GROUPS_ID, ACCOUNTS_ID) VALUES (
(Select g.id from ACCOUNTGROUP g where g.NAME = 'Commercial'), 
(Select a.id from ACCOUNT a where a.NAME = 'Cintas Special Orders'));

INSERT INTO MISCSHIPTO(ID,ADDRESSID,DELIVERTONAME,MAILSTOP) VALUES (hibernate_sequence.nextval,'NGAS000007','Ann Clark','Bldg:M7 Fl:2 Rm:2147');

INSERT INTO ADDRESS (ID, CITY, LINE1, STATE, ZIP, COUNTRY,MISCSHIPTO_ID,ACCOUNT_ID,PRIMARY,VERSION,TYPE_ID) 
    VALUES (hibernate_sequence.nextval, 'REDONDO BEACH', 'ONE SPACE PARK', 
            'CA','90278','US',
            (select m.id from miscShipTo m where m.addressId like 'NGAS000007'AND rownum = 1),
            (select a.id from account a WHERE a.NAME LIKE 'CSS Hospitals And Clinics'),0,0,
            (select t.id from addresstype t where t.NAME LIKE 'SHIPTO'));
 
