INSERT INTO CXMLCONFIGURATION (ID, DEPLOYMENTMODE, "NAME", SENDERSHAREDSECRET, SYSTEMUSERXPATHEXPRESSION, VERSION, credentialNameXpathExpression) 
	VALUES (hibernate_sequence.nextval, 'PRODUCTION', 'Test_Config', 'secret', '/cXML/Request/PunchOutSetupRequest/Extrinsic[@name=''User'']/text()', 0, '/cXML/Request/PunchOutSetupRequest/Extrinsic[@name=''Credential'']/text()');
INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'Network1', 'test', 0);
INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'Network2', 'test', 0);
INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'Network3', 'test', 0);
INSERT INTO FROM_CREDENTIAL_TABLE (CXMLCONFIGURATION_ID, FROMCREDENTIALS_ID) 
	VALUES ((select c.id from cxmlconfiguration c where c.name='Test_Config'), (select c.id from cxmlcredential c where c.domain = 'Network1' and c.IDENTIFIER='test'));
INSERT INTO TO_CREDENTIAL_TABLE (CXMLCONFIGURATION_ID, TOCREDENTIALS_ID) 
	VALUES ((select c.id from cxmlconfiguration c where c.name='Test_Config'), (select c.id from cxmlcredential c where c.domain = 'Network2' and c.IDENTIFIER='test'));
INSERT INTO SENDER_CREDENTIAL_TABLE (CXMLCONFIGURATION_ID, SENDERCREDENTIALS_ID) 
	VALUES ((select c.id from cxmlconfiguration c where c.name='Test_Config'), (select c.id from cxmlcredential c where c.domain = 'Network3' and c.IDENTIFIER='test'));
UPDATE ACCOUNT SET CXMLCONFIGURATION_ID=(select c.id from cxmlconfiguration c where c.name='Test_Config') WHERE NAME='CSS Hospitals And Clinics';

insert into person values(hibernate_sequence.nextval,null,'CXML','Tester','W',null,null,0);
insert into systemuser (ID, CHANGEPASSWORD, CONCURRENTACCESS, ISACTIVE, LOGIN, PASSWORD, SALT, STATUS, VERSION, PERSON_ID) 
	values(hibernate_sequence.nextval,0,0,1,'cxml_tester','$2a$12$sBg5273drc7KN0ZfQvEkJerHbULw4N470MyUuC8QFFVOf8szMjDjO','$2a$12$sBg5273drc7KN0ZfQvEkJe','active',0, (select id from person where firstname='CXML' and lastname='Tester'));

INSERT INTO SYSTEMUSER_ACCOUNT (USERS_ID, ACCOUNTS_ID)
	VALUES ((SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN LIKE 'cxml_tester'), (SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CSS Hospitals And Clinics'));

INSERT INTO ACCOUNTXCREDENTIALXUSER (ID, PUNCHLEVEL, VERSION, USACCOUNTNUMBER, ACCOUNT_ID, ADDRESS_ID, APPROVERUSER_ID, CRED_ID, PAYMENTINFORMATION_ID, USER_ID)
	VALUES (hibernate_sequence.nextval, '200', 0, '123456789', (SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CSS Hospitals And Clinics'), 
	null, null, (SELECT c.ID FROM CREDENTIAL c WHERE C.NAME LIKE 'Solr Credential'), null, 
	(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN LIKE 'cxml_tester'));

INSERT INTO ACCOUNTXCREDENTIALXUSER (ID, PUNCHLEVEL, VERSION, USACCOUNTNUMBER, ACCOUNT_ID, ADDRESS_ID, APPROVERUSER_ID, CRED_ID, PAYMENTINFORMATION_ID, USER_ID, ACTIVE)
	VALUES (hibernate_sequence.nextval, '200', 0, '123456789', (SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CSS Hospitals And Clinics'), 
	null, null, (SELECT c.ID FROM CREDENTIAL c WHERE C.NAME LIKE 'Solr Credential'), null, 
	(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN LIKE 'admin'), 1);

INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'user', 'apdnorthrop-t', 0);
INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'staples.com', 'apdnorthrop-t', 0);
INSERT INTO CXMLCREDENTIAL (ID, "DOMAIN", IDENTIFIER, VERSION) 
	VALUES (hibernate_sequence.nextval, 'user', 'staples.com', 0);
INSERT INTO OUTFROMCREDENTIALTABLE (CREDENTIAL_ID, OUTGOINGFROMCREDENTIALS_ID) 
	VALUES ((SELECT c.ID FROM CREDENTIAL c WHERE C.NAME LIKE 'Solr Credential'), (select c.id from cxmlcredential c where c.domain = 'user' and c.IDENTIFIER='apdnorthrop-t'));
INSERT INTO OUTTOCREDENTIALTABLE (CREDENTIAL_ID, OUTGOINGTOCREDENTIALS_ID) 
	VALUES ((SELECT c.ID FROM CREDENTIAL c WHERE C.NAME LIKE 'Solr Credential'), (select c.id from cxmlcredential c where c.domain = 'user' and c.IDENTIFIER='staples.com'));
INSERT INTO OUTSENDERCREDENTIALTABLE (CREDENTIAL_ID, OUTGOINGSENDERCREDENTIALS_ID) 
	VALUES ((SELECT c.ID FROM CREDENTIAL c WHERE C.NAME LIKE 'Solr Credential'), (select c.id from cxmlcredential c where c.domain = 'staples.com' and c.IDENTIFIER='apdnorthrop-t'));

UPDATE CREDENTIAL SET OUTGOINGSENDERSHAREDSECRET='test' WHERE NAME LIKE 'Solr Credential';

UPDATE CREDENTIAL SET PUNCHOUTURL='https://bci.stapleslink.com:13079/invoke/StaplescXML/receive11' WHERE NAME LIKE 'Solr Credential';
