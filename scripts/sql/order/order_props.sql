INSERT INTO PERIODTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Annually', 0);
INSERT INTO PERIODTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Quarterly', 0);
INSERT INTO PERIODTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Monthly', 0);
INSERT INTO PERIODTYPE (ID, NAME, VERSION) VALUES (hibernate_sequence.nextval, 'Weekly', 0);

INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was quoted', 'PUNCHED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was quoted', 'QUOTED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was ordered', 'ORDERED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being placed', 'PLACED_PROCESSING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was placed', 'PLACED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is validating', 'VALIDATING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is pending approval', 'PENDING_APPROVAL', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order pending a task being completed', 'PENDING_TASK', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was approved', 'APPROVED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was denied', 'DENIED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being sent to the vendor', 'SENT_TO_VENDOR_PROCESSING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being acknowledged', 'WAITING_TO_FILL_PROCESSING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is waiting to be filled', 'WAITING_TO_FILL', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been sent to the vendor', 'SENT_TO_VENDOR', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was refused', 'REFUSED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been accepted by the vendor', 'ACCEPTED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being shipped', 'PARTIAL_SHIP', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been partially billed', 'PARTIAL_BILL', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been billed', 'BILLED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being vouched', 'VOUCHING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been vouched', 'VOUCHED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being shipped', 'SHIPPED_PROCESSING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has shipped', 'SHIPPED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is being cancelled', 'CANCELED_PROCESSING', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was canceled', 'CANCELED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was completed', 'COMPLETED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order has been partially charged', 'PARTIAL_CHARGE', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was charged', 'CHARGED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order is imported from the previous system', 'CAPS', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'The order was manually marked as resolved', 'MANUALLY_RESOLVED', 0);
INSERT INTO ORDERSTATUS (ID, DESCRIPTION, VALUE, VERSION) VALUES (hibernate_sequence.nextval, 'A user Invalidated the order','INVALIDATED',0);

INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, '', 'EDI', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, '', 'CXML', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, '', 'XCBL', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, 'eCommerce', 'URL', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, 'Manual Order', 'MANUAL', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, 'Procurement Platform', 'PROCUREMENT', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, 'Punchout Order', 'PUNCHOUT', 0);
INSERT INTO ORDERTYPE (ID, DESCRIPTION, "VALUE", VERSION) VALUES (hibernate_sequence.nextval, 'Ad-hoc', 'ADHOC', 0);


