UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report parent order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report po id';

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		null, (SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'CREATED'), 0, 0, 0, 0);
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Address line 1', 'City', 'State', '12345', 0, 0, 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, PARENTORDER_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'CREATED'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 0, 0, 0);

INSERT INTO PONUMBER (ID, VERSION, VALUE, TYPE_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report po id'), 0,
		'apd-po-5', (SELECT poType.ID FROM PONUMBERTYPE poType WHERE poType.NAME = 'APD'));
		
INSERT INTO PONUMBER_CUSTOMERORDER (PONUMBERS_ID, ORDERS_ID) 
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report po id'), 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report order id'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		0, 'Spiralbound money/rent receipt book.', 'ITEM', 'Hewlett-Packard', 'ABFSC1152', 5, 1.00, 
		'Spiralbound money/rent receipt book.', 'ABFSC1152', 1, 9.95, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Spiralbound money/rent receipt book.'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem2 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		0, 'Record book features a black hardbound cover highlighted with maroon spine and corners. Numbered pages and acid-free paper.', 'ITEM', 'Hewlett-Packard', 'ABFARB810R30', 1, 1.00, 
		'Adams® Black and Maroon Record Ledger', 'ABFARB810R30', 1, 7.39, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Adams® Black and Maroon Record Ledger'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Another Address', 'Atlanta', 'GA', '54321', 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, PARENTORDER_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'CREATED'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 0, 0, 0);
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		0, 'Spiralbound money/rent receipt book.', 'ITEM', 'Hewlett-Packard', 'ABFSC1152', 5, 3.00, 
		'Spiralbound money/rent receipt book.', 'ABFSC1152', 1, 9.95, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Spiralbound money/rent receipt book.'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
