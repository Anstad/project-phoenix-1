UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report po id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Address line 1', 'City', 'State', '12345', 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CSS Hospitals And Clinics'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'APPROVED'), 0, 0, 0, 0);

INSERT INTO PONUMBER (ID, VERSION, VALUE, TYPE_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report po id'), 0,
		'apd-po-3', (SELECT poType.ID FROM PONUMBERTYPE poType WHERE poType.NAME = 'APD'));
		
INSERT INTO PONUMBER_CUSTOMERORDER (PONUMBERS_ID, ORDERS_ID) 
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report po id'), 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report order id'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		0, 'Poster frame with black border.', 'ITEM', 'AT-A-GLANCE', 'VER49063', 8, 1.00, 
		'DAX® Coloredge Poster Frame', 'VER49063', 1, 19.95, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Furniture'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'DAX® Coloredge Poster Frame'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';

INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 
		0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '12-Jun-2013', 'IDB9174364');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 5, 0.75, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 3.09, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));