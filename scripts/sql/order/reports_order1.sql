UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report po id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report notificationprops id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report message id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Address line 1', 'City', 'State', '12345', 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'APPROVED'), 0, 0, 0, 0);

INSERT INTO PONUMBER (ID, VERSION, VALUE, TYPE_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report po id'), 0,
		'apd-po-1', (SELECT poType.ID FROM PONUMBERTYPE poType WHERE poType.NAME = 'APD'));
		
INSERT INTO PONUMBER_CUSTOMERORDER (PONUMBERS_ID, ORDERS_ID) 
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report po id'), 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report order id'));

INSERT INTO NOTIFICATIONPROPERTIES (ID, ADDRESSING, VERSION, ORDER_ID, EVENTTYPE, RECIPIENTS)
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report notificationprops id'), 'BCC', 0, 
		(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report order id'), 
		'ORDER_ACKNOWLEDGEMENT',
		'teamphoenix@americanproduct.com');

--INSERT INTO MESSAGE (ID, CONTENT, VERSION)
--	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report message id'), 'message content', 0);

INSERT INTO ORDER_LOG (ID, VERSION, EVENTTYPE, CUSTOMERORDER_ID, FROM_CLASS)
	VALUES (hibernate_sequence.nextval, 0, 'ORDER_ACKNOWLEDGEMENT',
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 'OrderLog');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		1, 'Appointment book', 'ITEM', 'AT-A-GLANCE', 'AAG70EP0105', 3, 0.85, 
		'AT-A-GLANCE® The Action Planner® Weekly Appointment Book', 'AAG70EP0105', 1, 18.20, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'AT-A-GLANCE® The Action Planner® Weekly Appointment Book'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem2 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		0, 'Executive weekly/monthly planner with photos. Ruled without appointment times. Magnet closure on covers.', 'ITEM', 'AT-A-GLANCE', 'AAG70NF8105', 1, 1.50, 
		'AT-A-GLANCE® Executive Fashion Weekly/Monthly Planner', 'AAG70NF8105', 1, 15.15, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'AT-A-GLANCE® Executive Fashion Weekly/Monthly Planner'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem3 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem3 id'),
		1, 'OEM ink tank for Canon® PIXMA iP3300, iP3500, iP4200, iP4300, iP4500, iP5200, iP5200R, iP6600D. iP6700D, MP500, MP510, MP520, MP530, MP610, MP530, MP600, MP800, MP800R, MP810, MP830, MP850, MP950, MP960, MP970, MX700, Pro9000, Pro9000MKII.', 'ITEM', 'Canon', 'CNM0620B015', 10, 0.05, 
		'Canon® 0620B015 (CLI-8) Eight-Color Multipack Ink Tank', 'CNM0620B015', 1, 24.95, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Office/Desk Accessories'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Canon® 0620B015 (CLI-8) Eight-Color Multipack Ink Tank'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';

INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '25-Apr-2013', 'IDB98745934875');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';

INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 3, 0.90, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 2.0, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'County Tax'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 0.09, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';

INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 1, 1.25, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 1.9, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';

INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 10, 0.0, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem3 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 1.1, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
	
	
	
	
