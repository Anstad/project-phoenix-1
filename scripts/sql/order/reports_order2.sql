UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report parent order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report po id';

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		null, (SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'APPROVED'), 0, 0, 0, 0);
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Address line 1', 'City', 'State', '12345', 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, PARENTORDER_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'APPROVED'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 0, 0, 0);

INSERT INTO PONUMBER (ID, VERSION, VALUE, TYPE_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report po id'), 0,
		'apd-po-2', (SELECT poType.ID FROM PONUMBERTYPE poType WHERE poType.NAME = 'APD'));
		
INSERT INTO PONUMBER_CUSTOMERORDER (PONUMBERS_ID, ORDERS_ID) 
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report po id'), 
	(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'report order id'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		0, 'Large capacity toaster oven with multi-use tray that toasts, bakes and broils.', 'ITEM', 'Faber-Castell', 'OGFOG20', 1, 2.00, 
		'Coffee Pro Toaster Oven with Multi-Use Pan', 'OGFOG20', 1, 59.95, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Breakroom/Janitorial/Maint.'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Coffee Pro Toaster Oven with Multi-Use Pan'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem2 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		0, 'Retractable USB connector in a slim design.', 'ITEM', 'Faber-Castell', 'VER49063', 20, 5.00, 
		'Verbatim® PinStripe USB Drive', 'VER49063', 1, 37.39, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Computer Equipment'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Verbatim® PinStripe USB Drive'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';
		
INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 
		0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '1-Jun-2013', 'IDB98374534534');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 1, 2.10, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 0.9, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 15, 4.00, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 0.5, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';

INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 
		0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '3-Jun-2013', 'IDB10192835847');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 5, 0.00, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem2 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 2.0, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
			
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report order id';
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report address id';

INSERT INTO ADDRESS (ID, LINE1, CITY, STATE, ZIP, PRIMARY, VERSION)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
			'Another Address', 'Atlanta', 'GA', '54321', 0, 0);

INSERT INTO CUSTOMERORDER (ID, VERSION, ACCOUNT_ID, ADDRESS_ID, CREDENTIAL_ID, USER_ID, STATUS_ID, PARENTORDER_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT, ORDERTOTAL)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 0, 
		(SELECT a.ID FROM ACCOUNT a WHERE a.NAME LIKE 'CMC Main- Pediatric Hemo/Oncology'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME = 'Solr Credential'),
		(SELECT u.ID FROM SYSTEMUSER u WHERE u.LOGIN = 'jrichardson'),
		(SELECT s.ID FROM ORDERSTATUS s WHERE s.VALUE = 'APPROVED'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report parent order id'), 0, 0, 0, 0);
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitem1 id';
		
INSERT INTO LINEITEM (ID, CORE, DESCRIPTION, ITEMTYPE, MANUFACTURERNAME, MANUFACTURERPARTID, QUANTITY, ESTIMATEDSHIPPINGAMOUNT, SHORTNAME, SUPPLIERPARTID, TAXABLE, UNITPRICE, VERSION, CATEGORY_ID, ITEM_ID, ORDER_ID, UNITOFMEASURE_ID, VENDOR_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		0, 'Retractable USB connector in a slim design.', 'ITEM', 'Faber-Castell', 'VER49063', 20, 5.00, 
		'Verbatim® PinStripe USB Drive', 'VER49063', 1, 37.39, 0, 
		(SELECT itemcat.ID FROM ITEMCATEGORY itemcat WHERE itemcat.NAME = 'Computer Equipment'), 
		(SELECT i.ID FROM ITEM i WHERE i.NAME = 'Verbatim® PinStripe USB Drive'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), 
		(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'),
		(SELECT v.ID FROM VENDOR v WHERE v.NAME = 'American Product Distributors'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';
		
INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 
		0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '1-Jun-2013', 'IDB1029653984');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 18, 4.0, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 1.0, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report shipment id';

INSERT INTO SHIPMENT (ID, VERSION, ADDRESS_ID, ORDER_ID, SHIPTIME, TRACKINGNUMBER)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'), 
		0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report address id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report order id'), '12-Jun-2013', 'IDB102983748');
		
UPDATE IDS_USED SET ID=hibernate_sequence.nextval WHERE NAME='report lineitemxshipment id';
		
INSERT INTO LINEITEMXSHIPMENT (ID, QUANTITY, SHIPPING, VERSION, LINEITEM_ID, SHIPMENTS_ID)
	VALUES ((SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'), 2, 1.0, 0, 
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitem1 id'),
		(SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report shipment id'));
		
INSERT INTO LINEITEMXSHIPMENT_TAXTYPE (ID, VALUE, VERSION, LINEITEMXSHIPMENT_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 3.0, 0, (SELECT used.ID FROM IDS_USED used WHERE used.NAME = 'report lineitemxshipment id'),
		(SELECT t.ID FROM TAXTYPE t WHERE t.NAME = 'State Tax'));