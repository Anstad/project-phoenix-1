INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'USPS', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'shipping method'));
INSERT INTO FIELDOPTIONS (ID, VALUE, VERSION, FIELD_ID) VALUES (hibernate_sequence.nextval, 'FedEx', 0, (SELECT f.id 
	FROM FIELD f WHERE f.name LIKE 'shipping method'));