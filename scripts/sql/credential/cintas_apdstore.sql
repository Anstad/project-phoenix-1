INSERT INTO CREDENTIAL (ID, REQUIREZIPCODEINPUT, AUTHORIZEADDRESS, EXPIRATIONDATE, NAME, STORECREDIT, VERSION, CASHOUTPAGE_ID, CATALOG_ID, COMMUNICATIONMETHOD_ID, PAYMENTINFORMATION_ID, ROOTACCOUNT_ID, SKUTYPE_ID, WILLCALLEDIOVERRIDE, WRAPANDLABEL, ADOT)
	VALUES (hibernate_sequence.nextval, 0, 0, null, 'Cintas/APDStore', 0.0, 0, null, (select * from catalog where name = 'Solr Customer Catalog'), 
	(SELECT comm.ID FROM COMMUNICATIONMETHOD comm WHERE comm.NAME LIKE 'EDI'), 
	null, 
	(Select a.id from ACCOUNT a where a.name = 'Cintas'), 
	(SELECT s.id FROM SKUTYPE s WHERE s.name LIKE 'APD'), 0, 0, 0);


@credential/cintas_apdstore_properties;


@credential/cintas_apdstore_cashout;

	
INSERT INTO IDS_USED (NAME, ID) VALUES ('contact_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('notification_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('bulletin_id', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('po_id1', hibernate_sequence.nextval);
INSERT INTO IDS_USED (NAME, ID) VALUES ('po_id2', hibernate_sequence.nextval);
	
INSERT INTO BULLETIN (ID, EXPIRATIONDATE, NAME, MESSAGE, VERSION) 
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'bulletin_id'), '1-Sep-2013', 'Summer Sale!', 'Get a 10% discount off all pens through August!', 0);
	
INSERT INTO CREDENTIAL_BULLETIN (CREDENTIAL_ID, BULLETINS_ID)
	VALUES ((SELECT c.ID FROM CREDENTIAL c WHERE c.NAME LIKE 'Cintas/APDStore'), (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'bulletin_id'));

INSERT INTO PERSON (ID, FIRSTNAME, LASTNAME, EMAIL, VERSION)
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'contact_id'), 'Order', 'Management', 'ordermgmt@americanproduct.com', 0);
	
INSERT INTO NOTIFICATIONPROPERTIES (ID, ADDRESSING, VERSION, ORDER_ID, EVENTTYPE, ACC_ID, CRED_ID, RECIPIENTS)
	VALUES (
		(SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'notification_id'), 
		'BCC', 0, null, 'ORDER_ACKNOWLEDGEMENT',
		(Select a.id from ACCOUNT a where a.name = 'Cintas'),
		(SELECT c.ID FROM CREDENTIAL c WHERE c.NAME LIKE 'Cintas/APDStore'),
		'ordermgmt@americanproduct.com'
	);
	
INSERT INTO IPADDRESS (ID, VALUE, VERSION, ACCOUNT_ID, CRED_ID)
	VALUES (hibernate_sequence.nextval, '127.0.0.1', 0, null, (SELECT c.ID FROM CREDENTIAL c WHERE c.NAME LIKE 'Cintas/APDStore'));

INSERT INTO PONUMBER (ID, EXPIRATIONDATE, STARTDATE, VALUE, VERSION, TYPE_ID)
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'po_id1'), null, null, '1234', 0, (SELECT t.ID FROM PONUMBERTYPE t WHERE t.NAME LIKE 'APD'));
	
INSERT INTO PONUMBER (ID, EXPIRATIONDATE, STARTDATE, VALUE, VERSION, TYPE_ID)
	VALUES ((SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'po_id2'), null, null, '9876', 0, (SELECT t.ID FROM PONUMBERTYPE t WHERE t.NAME LIKE 'Customer'));

INSERT INTO CREDENTIAL_PONUMBER (CREDENTIAL_ID, BLANKETPOS_ID)
	VALUES ((SELECT c.ID FROM CREDENTIAL c WHERE c.NAME LIKE 'Cintas/APDStore'), (SELECT var.ID FROM IDS_USED var WHERE var.NAME LIKE 'po_id1'));
