INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('login', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'type'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('45', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'session timeout'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('Continue back to login', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'return button text'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('production', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'deployment'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('apdstore', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'vendor path prefix'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('MRO', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'button label'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('4002', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'AR GL account'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('609192091', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'DUNS number (for ariba)'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('http://estore.americanproduct.com/cgi-bin/punchin', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'punchout URL'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('https://mercury.rev.net/apd-punchout/cgi-bin/punchoutexit', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'punchoutexit URL'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('http://estore.americanproduct.com/cgi-bin/punchin', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'orderrequest URL'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('https://mercury.rev.net/apd-punchout/cgi-bin/shared/punchout?', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'shop again URL base'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('http://www.cnts.americanproduct.com/', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'HTTP root'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('https://mercury.rev.net/apd-punchout/', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'HTTPS root'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('/www/htdocs/commercial/apd.americanproduct.com/apd', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'HTTP path'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('/www/htdocs/ssl/apd-punchout/apd', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'HTTPS path'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('https://mercury.rev.net/apd-punchout/images/bannerfull-new.jpg', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'image: header'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('/template/doublepunchouttop-cintas.html', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'login frame template'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('/template/doublepunchouttop-cintas.html', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'punch frame template'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('OMS ID', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'from credential'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('APDNC OMS', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'from punch identity'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('APDNC OMS', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'from order identity'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('STORE ID', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'to credential'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('APDNC estore', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'to punch identity'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('APDNC estore', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'to order identity'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('CUSTOMER', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'sender credential'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('cnts-mro', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'sender credential punch user'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('cnts-mro', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'sender credential punch password'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('cnts-mro', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'sender credential order user'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('cnts-mro', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'sender credential order password'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('eclinx', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'credit card processor'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('0362913', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'credit card merchant number'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('10.0', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'merchandise discount'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('96.0', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'cost of goods'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('2.50', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'cc proc perc of order'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('4.0', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'rev.net proc perc of order'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('true', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'send PDF attachment'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('s', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'SKU on emails'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('v', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'cc payments'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('amex discover mastercard visa', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'cc payments allowed types'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'invoice payments'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('a', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'invoice number required'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use billing PO for invoices'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use default credit card number'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'charge sales tax'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use alternate shiptos'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use alternate billtos'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'require authorized shiptos'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'blank address 2/3 when authorized'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'allow rejected items to ship'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use last shipto'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use common units of measure'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use ISO units of measure'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('by order', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'admin orders: pick supplier'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'protect shiptos'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'concatenate item number to description'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('n', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'allow attachments to orders'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('7', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'days to keep order on hold'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('90', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'default order history timeframe'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'default address when adding user'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'allow PO editing'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'use order comments'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('y', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'retry refused order'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('Your order can not be completed due to approval constraints on your profile.', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'spending limit message'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('BILLING INFORMATION:', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'billto heading title'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('SHIP TO:', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'shipto heading title'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('Choose P-Card:', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'multiple billto pull down heading'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));
INSERT INTO CREDENTIAL_PROPERTYTYPE (VALUE, TYPE_ID, ID, VERSION, CRED_ID) 
	VALUES ('Choose Ship To:', (SELECT t.id FROM CREDENTIALPROPERTYTYPE t WHERE t.name LIKE 'multiple shipto pull down heading'), 
	hibernate_sequence.nextval, 0, (SELECT c.id FROM CREDENTIAL c WHERE c.name LIKE 'Cintas/APDStore'));