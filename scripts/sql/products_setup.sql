--/* Updating the next sequence value for hibernate to a number greater than the primary keys used in this script. */
alter sequence hibernate_sequence increment by 50;
select hibernate_sequence.nextval from dual;
alter sequence hibernate_sequence increment by 1;

insert into skutype values(1,'APD',0);
insert into skutype values(2,'WALMART',0);

insert into account values(1,null,1,'account1','ANY',0,null,null,null,null,null);
insert into account values(2,null,1,'account2','ANY',0,null,1,null,null,1);
insert into vendor values(1,null,'vendor1',null);
insert into vendor values(2,null,'vendor2',null);

--/* Vendor Catalogs */
insert into catalog values(1,null,null,null,null,null,1);
insert into catalog values(2,null,null,null,null,null,2);

--/* Customer Catalogs */
insert into catalog values(3,null,null,null,null,null,null);
insert into catalog values(4,null,null,null,null,null,null);


-- /* insert into item values(ID,DESCRIPTION,NAME,VERSION,ITEMCATEGORY_ID,MANUFACTURER_ID,REPLACEMENT_ID,VENDORCATALOG_ID) */
insert into item values(1,'window cleaner','windex',0,null,null,null,1);
insert into item values(2,'12 Rolls of Paper Towels','Bounty Paper Towels',0,null,null,null,1);

-- /* 
-- credential (ID,ACTIVATIONDATE,AUTHORIZEADDRESS,EXPIRATIONDATE,NAME,PUNCHLEVEL,PUNCHOUTIDENTITY,STORECREDIT,VERSION,CASHOUTPAGE_ID,
--             CATALOG_ID,COMMUNICATIONMETHOD_ID,PROCESS_ID,ROOTACCOUNT_ID,SKUTYPE_ID)
-- */
insert into credential values(1,null,1,null,'credential1',null,null,null,1,null,1,null,null,null,2);
insert into credential values(2,null,1,null,'credential2',null,null,null,1,null,2,null,null,null,2);
-- /* catalogxitem (ID,COREITEMEXPIRATIONDATE,COREITEMSTARTDATE,PRICE,VERSION,CATALOG_ID,CUSTOMERSKU_ID,ITEM_ID,
--                  PRICINGTYPE_ID,SUBSTITUTEITEM_ID) */
insert into catalogxitem values(1,null,null,1.99,0,1,null,1,null,null);
insert into catalogxitem values(2,null,null,2.99,0,2,null,2,null,null);


insert into customercost values(1,9.99,0,1,1);

--/* Setup user accounts..move to diff file */
insert into systemuser values(1,null,0,0,1,null,'apd','password','active',0);
insert into person values(1,null,'Adam','Smith','W',null,null,0,1,null);

-- /* insert into accountxcredentialxuser values(id,punchlevel,version,account_id,address_id,approveruser_id,cred_id,paymentinformation_id,user_id) */

insert into accountxcredentialxuser values(1,null,0,1,null,null,1,null,1);
insert into accountxcredentialxuser values(2,null,0,1,null,null,2,null,1);

--/* insert into hierarchynode values(id,description,version,parent_id) */

insert into hierarchynode values(1,'Office Supplies',0,null);
insert into hierarchynode values(2,'MRO Supplies',0,null);
insert into hierarchynode values(3,'Corporate Identity',0,null);
insert into hierarchynode values(4,'Electronics',0,null);

insert into hierarchynode values(5,'Binders',0,1);
insert into hierarchynode values(6,'Paper',0,1);
insert into hierarchynode values(7,'Toner Cartridges',0,1);
insert into hierarchynode values(8,'Desk Accessories',0,1);

--/* insert into sku values(id,version,hierarchynode_id,item_id,type_id) */
-- ID
-- VERSION
-- HIERARCHYNODE_ID
-- ITEM_ID
-- TYPE_ID
insert into sku values(1,0,1,1,1);
insert into sku values(2,0,2,2,1);
-- insert into sku values(3,0,3,3,1);
-- insert into sku values(4,0,4,4,1);
-- insert into sku values(5,0,5,5,1);
-- insert into sku values(6,0,6,6,1);
-- insert into sku values(7,0,7,7,1);
-- insert into sku values(8,0,8,8,1);


--/* insert into skutype_hierarchynode values(skutype_id,hierarchynodes_id); */
insert into skutype_hierarchynode values(1,1);
insert into skutype_hierarchynode values(1,2);
insert into skutype_hierarchynode values(1,3);
insert into skutype_hierarchynode values(1,4);

insert into skutype_hierarchynode values(2,3);
insert into skutype_hierarchynode values(2,4);


--/* insert into hierarchynode_skutype values(hierarchynode_id,skutypes_id); */
insert into hierarchynode_skutype values(1,1);
insert into hierarchynode_skutype values(2,1);
insert into hierarchynode_skutype values(3,1);
insert into hierarchynode_skutype values(4,1);

insert into hierarchynode_skutype values(3,2);
insert into hierarchynode_skutype values(4,2);