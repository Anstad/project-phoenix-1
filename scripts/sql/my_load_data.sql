--/* Initialization */

@disable_constraints;

@truncate_data;

@enable_constraints;


CREATE TABLE IDS_USED (NAME varchar2(255), ID number);
--/* Loading data necessary for administration (will be removed) */

@admin_users;


--/* Upload Props Data */
@general/props.sql;

@account/account_props.sql;

@account/payment_props.sql;


--/* Upload Demo Data */
@account/account.sql;

@account/payment.sql;

@account/address_and_phone.sql;




--/* Trevors stuff */
@roles_and_permissions;

@credential;


--/* cleanup */
DROP TABLE IDS_USED;
--@enable_constraints;
