UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_item';

INSERT INTO ITEM (ID, LASTMODIFIED, DESCRIPTION, NAME, SEARCHTERMS, VERSION, ABILITYONESUBSTITUTE_ID, HIERARCHYNODE_ID, ITEMCATEGORY_ID, MANUFACTURER_ID, REPLACEMENT_ID, VENDORCATALOG_ID, UNITOFMEASURE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), '1-Feb-2014', 'Monthly desk pad calendar featuring different floral design each month, past and future calendar reference, ruled daily blocks and poly corners.',
	'AT-A-GLANCE® Flowers Desk Pad', '"Visual Organizer®" AT-A-GLANCE Calendars "Calendars-Desk Pad" Scheduling "Appointment Tracking" Time-Management Dating Dates Recycled',
	0, null, (SELECT h.ID FROM HIERARCHYNODE h WHERE h.DESCRIPTION LIKE 'Desk Pad Calendars '||'&'||' Refills'), null, 
	(SELECT m.ID FROM MANUFACTURER m where m.name='Hewlett-Packard'), null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_vendor_catalog'),
	(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, 'available', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'status'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, '37.39', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'list price'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO SKU (ID, VALUE, VERSION, ITEM_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 'AAG5035', 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'),
	(SELECT sType.ID FROM SKUTYPE sType WHERE sType.NAME LIKE 'APD'));

--/* Assembly_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);
	
INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Assembly_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Recycle_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Y', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Green_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Y', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* MSDS_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'MSDS_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* UNSPSC classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), '44121621', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'UNSPSC'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Global Product Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	1, 'Global Product Type', 'Calendars-Desk Pad');
	
--/* Size specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	2, 'Size', '22 x 17');
	
--/* Page Color/Theme specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	3, 'Page Color/Theme', 'Floral');
	
--/* Edition Year specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	5, 'Edition Year', '2013');
	
--/* Calendar Term specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	6, 'Calendar Term', '12-Month (Jan.-Dec.)');
	
--/* Julian Dates specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	7, 'Julian Dates', 'Yes');
	
--/* Seasonal Availability specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	9, 'Seasonal Availability', 'Yes');
	
--/* Page Format specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	10, 'Page Format', 'One Month per Page');
	
--/* Calendar Reference Blocks specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	11, 'Calendar Reference Blocks', 'Next Month; Past Month');
	
--/* Calendar Format specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	12, 'Calendar Format', 'Monthly');
	
--/* Dated/Undated specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	13, 'Dated/Undated', 'Dated');
	
--/* Refillable specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	15, 'Refillable', 'Nonrefillable');
	
--/* Binding Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	19, 'Binding Type', 'Headband');
	
--/* Corner Color(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	20, 'Corner Color(s)', 'Clear');
	
--/* Binding Color(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	23, 'Binding Color(s)', 'Green');
	
--/* Holder Style specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	24, 'Holder Style', 'Headband with Bottom Corners');
	
--/* Holder Material specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	25, 'Holder Material', 'Paper Headband; Poly Corners');
	
--/* Reversible specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	27, 'Reversible', 'No');
	
--/* Post-Consumer Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	86, 'Post-Consumer Recycled Content Percent', '30%');
	
--/* Total Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	87, 'Total Recycled Content Percent', '30%');

INSERT INTO CATALOGXITEM (ID, LASTMODIFIED,  PRICE, VERSION, ITEM_ID, CATALOG_ID, SUBSTITUTEITEM_ID) 
	VALUES (hibernate_sequence.nextval, '1-Feb-2014', 0.99, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_catalog'),
		(SELECT cxi.ID FROM CATALOGXITEM cxi, ITEM item WHERE cxi.ITEM_ID=item.ID AND item.NAME LIKE '%The Action Planner® Weekly Appointment Book%'));
	
INSERT INTO ITEMIMAGE (ID, IMAGENAME, IMAGEURL, VERSION, IMAGETYPE_ID, ITEM_ID) 
	VALUES (hibernate_sequence.nextval, 'Appointment Book', 'http://content.oppictures.com/Master_Images/Master_Variants/Variant_240/169847.jpg', 0, null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO ITEMIMAGE (ID, IMAGENAME, IMAGEURL, VERSION, IMAGETYPE_ID, ITEM_ID) 
	VALUES (hibernate_sequence.nextval, 'Appointment Book 2', 'http://content.oppictures.com/Master_Images/Master_Variants/Variant_500/169854.JPG', 0, null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));		
			