UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_item';

INSERT INTO ITEM (ID, LASTMODIFIED, DESCRIPTION, NAME, SEARCHTERMS, VERSION, ABILITYONESUBSTITUTE_ID, HIERARCHYNODE_ID, ITEMCATEGORY_ID, MANUFACTURER_ID, REPLACEMENT_ID, VENDORCATALOG_ID, UNITOFMEASURE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), '1-Feb-2014', 'Executive weekly/monthly planner ruled with appointment times. Includes notepad and special information section.',
	'AT-A-GLANCE® Executive Weekly/Monthly Appointment Book with Zipper Closure', 'Appointment "Appointment Book" "Appointment Books" "Appointment Books/Refills" AT-A-GLANCE Calendar "Dayminder Appointment Book" Weekly/Monthly Recycled',
	0, null, (SELECT h.ID FROM HIERARCHYNODE h WHERE h.DESCRIPTION LIKE 'Weekly/Monthly Planners'), null, 
	(SELECT m.ID FROM MANUFACTURER m where m.name='Faber-Castell'), null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_vendor_catalog'),
	(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, 'available', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'status'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, '37.39', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'list price'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO SKU (ID, VALUE, VERSION, ITEM_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 'AAG70N34505', 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'),
	(SELECT sType.ID FROM SKUTYPE sType WHERE sType.NAME LIKE 'APD'));

--/* Assembly_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);
	
INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Assembly_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Recycle_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Y', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Green_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Y', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* EPACPGCompliant_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'Information Not Provided By Manufacturer', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'EPACPGCompliant_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* MSDS_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'MSDS_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* UNSPSC classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), '44112005', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'UNSPSC'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Non_Returnable_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'D', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Non_Returnable_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Global Product Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	1, 'Global Product Type', 'Calendars and Planners-Appointment Books');
	
--/* Size specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	2, 'Size', '4 5/8 x 8');
	
--/* Page Color/Theme specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	3, 'Page Color/Theme', 'White');
	
--/* Appointment Ruling specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	4, 'Appointment Ruling', 'Hourly 8 AM to 5 PM');
	
--/* Edition Year specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	5, 'Edition Year', '2013');
	
--/* Calendar Term specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	6, 'Calendar Term', '12-Month (Jan.-Dec.)');
	
--/* Seasonal Availability specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	9, 'Seasonal Availability', 'Yes');
	
--/* Page Format specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	10, 'Page Format', 'One Month per Two-Page Spread; One Week per Two-Page Spread');
	
--/* Calendar Reference Blocks specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	11, 'Calendar Reference Blocks', 'Current Month; Next Month');
	
--/* Calendar Format specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	12, 'Calendar Format', 'Weekly/Monthly');
	
--/* Dated/Undated specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	13, 'Dated/Undated', 'Dated');
	
--/* Refillable specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	15, 'Refillable', 'Refillable');
	
--/* Separate Sections specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	16, 'Separate Sections', 'Special Information');
	
--/* Closure specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	18, 'Closure', 'Zipper');
	
--/* Binding Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	19, 'Binding Type', 'Concealed Wire');
	
--/* Cover Color(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	21, 'Cover Color(s)', 'Black');
	
--/* Cover Material(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	22, 'Cover Material(s)', 'Flexible');
	
--/* Pocket(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	28, 'Pocket(s)', 'Business Card; Storage');
	
--/* Disclaimer Statement specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	88, 'Disclaimer Statement', 'Mylar is a Dupont® registered trademark.');

INSERT INTO CATALOGXITEM (ID, LASTMODIFIED,  PRICE, VERSION, ITEM_ID, CATALOG_ID, SUBSTITUTEITEM_ID) 
	VALUES (hibernate_sequence.nextval, '1-Feb-2014', 12.99, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_catalog'),
		(SELECT cxi.ID FROM CATALOGXITEM cxi, ITEM item WHERE cxi.ITEM_ID=item.ID AND item.NAME LIKE '%The Action Planner® Weekly Appointment Book%'));

INSERT INTO ITEMIMAGE (ID, IMAGENAME, IMAGEURL, VERSION, IMAGETYPE_ID, ITEM_ID) 
	VALUES (hibernate_sequence.nextval, 'Appointment Book', 'http://www.ataglance.com/images/70951_M_1.png', 0, null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO ITEMIMAGE (ID, IMAGENAME, IMAGEURL, VERSION, IMAGETYPE_ID, ITEM_ID) 
	VALUES (hibernate_sequence.nextval, 'Appointment Book 2', 'http://www.ataglance.com/images/70951_M_2.png', 0, null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));		
	
UPDATE CATALOGXITEM SET "COREITEMEXPIRATIONDATE" = (TO_DATE('2023/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')), 
"COREITEMSTARTDATE" = (TO_DATE('2003/05/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss')) WHERE ID = (SELECT cxi.ID FROM CATALOGXITEM cxi, ITEM item 
WHERE cxi.ITEM_ID=item.ID AND item.NAME LIKE 'AT-A-GLANCE® Executive Weekly/Monthly Appointment Book with Zipper Closure');	