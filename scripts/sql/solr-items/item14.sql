UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_item';

INSERT INTO ITEM (ID, LASTMODIFIED, DESCRIPTION, NAME, SEARCHTERMS, VERSION, ABILITYONESUBSTITUTE_ID, HIERARCHYNODE_ID, ITEMCATEGORY_ID, MANUFACTURER_ID, REPLACEMENT_ID, VENDORCATALOG_ID, UNITOFMEASURE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), '1-Feb-2014', 'Slanted desktop sign holder.',
	'DAX® Slanted Sign Holder', '"Plastic Frame" "Certificate Holder" Frames DAX "Picture Frames" "Sign Holder" Signage Visual Graphics Indicators Directory Directories',
	0, null, (SELECT h.ID FROM HIERARCHYNODE h WHERE h.DESCRIPTION LIKE 'Sign Holders'), null, 
	(SELECT m.ID FROM MANUFACTURER m where m.name='AT-A-GLANCE'), null, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_vendor_catalog'),
	(SELECT u.ID FROM UNITOFMEASURE u WHERE u.NAME = 'EA'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, 'available', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'status'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO ITEMXITEMPROPERTYTYPE (ID, VALUE, VERSION, TYPE_ID, ITEM_ID)
	VALUES (hibernate_sequence.nextval, '37.39', 0, (SELECT t.ID FROM ITEMPROPERTYTYPE t WHERE t.NAME LIKE 'list price'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));
	
INSERT INTO SKU (ID, VALUE, VERSION, ITEM_ID, TYPE_ID)
	VALUES (hibernate_sequence.nextval, 'DAXN270985HT', 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'),
	(SELECT sType.ID FROM SKUTYPE sType WHERE sType.NAME LIKE 'APD'));

--/* Assembly_Code classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);
	
INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Assembly_Code'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Recycle_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'Recycle_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* MSDS_Indicator classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), 'N', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'MSDS_Indicator'));
	
INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* UNSPSC classification */
	
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_codetype';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_classcode';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemclass';

INSERT INTO CODETYPE (ID, NAME, VERSION) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'), '55121718', 0);

INSERT INTO CLASSIFICATIONCODE (ID, VERSION, CODETYPE_ID) VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), 
	0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_codetype'));

INSERT INTO ITEMCLASSIFICATION (ID, VERSION, CLASSIFICATIONCODE_ID, ITEMCLASSIFICATIONIMAGE_ID, ITEMCLASSTYPE_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), 0, 
	(SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_classcode'), null,
	(SELECT t.ID FROM ITEMCLASSIFICATIONTYPE t WHERE t.NAME LIKE 'UNSPSC'));

INSERT INTO ITEMCLASSIFICATION_ITEM (ITEMCLASSIFICATIONS_ID, ITEMS_ID)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemclass'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

--/* Global Product Type specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	1, 'Global Product Type', 'Signs '||'&'||' Sign Holders-Horizontal Wall');
	
--/* Frame Color specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	2, 'Frame Color', 'Clear');
	
--/* Size specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	3, 'Size', '11 x 8 1/2');
	
--/* Insert Size specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	5, 'Insert Size', '11 x 8 1/2');
	
--/* Material(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	6, 'Material(s)', 'Plastic');
	
--/* Orientation specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	7, 'Orientation', 'Horizontal');
	
--/* Width specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	10, 'Width', '11"');
	
--/* Height specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	12, 'Height', '8 1/2"');
	
--/* Loading specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	13, 'Loading', 'Side');
	
--/* Placement specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	21, 'Placement', 'Desktop');
	
--/* Face Color(s) specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	22, 'Face Color(s)', 'Clear');
	
--/* Quantity specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	25, 'Quantity', '1 each');
	
--/* Pre-Consumer Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	85, 'Pre-Consumer Recycled Content Percent', '0%');
	
--/* Post-Consumer Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	86, 'Post-Consumer Recycled Content Percent', '0%');
	
--/* Total Recycled Content Percent specification */

UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_itemspec';
UPDATE IDS_USED ids SET ids.ID=(hibernate_sequence.nextval) WHERE ids.NAME LIKE 'solr_specproperty';

INSERT INTO ITEMSPECIFICATION (ID, ITEMSPECIFICATIONID, VERSION, ITEM_ID) 
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'), 0, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'));

INSERT INTO SPECIFICATIONPROPERTY (ID, VERSION, ITEMSPECIFICATION_ID, SEQUENCE, NAME, VALUE)
	VALUES ((SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_specproperty'), 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_itemspec'),
	87, 'Total Recycled Content Percent', '0%');

INSERT INTO CATALOGXITEM (ID, LASTMODIFIED,  PRICE, VERSION, ITEM_ID, CATALOG_ID) 
	VALUES (hibernate_sequence.nextval, '1-Feb-2014', 3.99, 0, (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_item'), (SELECT ids.ID FROM IDS_USED ids WHERE ids.NAME LIKE 'solr_catalog'));
	