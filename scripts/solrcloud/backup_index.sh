#!/bin/bash
echo "Beginning Solr Index backup...."
echo "Checking current backups...."
SOLR_HOME=/opt/solr/solrHome
NUMBER_TO_KEEP=5
NUMBER_OF_BACKUPS=`ls -lrt $SOLR_HOME/apd-*.tar.gz | wc -l`
echo "Found $NUMBER_OF_BACKUPS backups, max to keep is $NUMBER_TO_KEEP"
NUMBER_TO_DELETE=$[$NUMBER_OF_BACKUPS - $NUMBER_TO_KEEP]
if [ $NUMBER_TO_DELETE -gt 0 ] ; then
	echo "Removing stale backup(s)"
	ls -lrt $SOLR_HOME/apd-*.tar.gz | head -n$NUMBER_TO_DELETE | awk '{print $9}' | xargs rm -r
fi
echo "Starting backup...."
tar czvf $SOLR_HOME/apd-`date +%H-%M-%S-%m-%d-%Y`.tar.gz $SOLR_HOME/apd 2> /dev/null
echo "Done creating backup"
exit 0
