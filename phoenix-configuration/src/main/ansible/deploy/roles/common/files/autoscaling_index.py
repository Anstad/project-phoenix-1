#!/usr/bin/env python
#
# Returns the index id of the current instance within an autoscaling group
#
import boto
import sys
import urllib2

if len(sys.argv) < 2:
    print >>sys.stderr, "First argument must be the AutoScalingGroup ID"

group_name = sys.argv[1]
print "Looking up group: %s" % group_name

conn = boto.connect_autoscale()
instances = [ i for i in conn.get_all_autoscaling_instances() if i.health_status == 'HEALTHY' and i.lifecycle_state == 'InService' and i.group_name == group_name ]
instance_ids = [ instance.instance_id for instance in instances ]
instance_ids.sort()

my_id = urllib2.urlopen('http://169.254.169.254/latest/meta-data/instance-id').read()
my_id = 'i-7b0da103'

if my_id in instance_ids:
    print instance_ids.index(my_id)
else:
    print "My instance id (%s) not found in group %s instances.\n Instance list: %s" % (my_id, group_name, instance_ids)
