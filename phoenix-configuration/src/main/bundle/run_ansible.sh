#!/bin/sh
export ANSIBLE_PLAYBOOK_TAGS=$1
export VERSION=$2
export BUCKET=$3
export ENV=$4

. /root/.bash_profile

# Download and extract the latest playbook from S3
eval "ansible -m s3 -a 'bucket=apd-deployment-artifacts object=/ansible-playbooks/ansible-deploy-$VERSION.tar.gz dest=/tmp/ansible-deploy.tar.gz mode=get overwrite=true' -c local local"
rm -r -f /tmp/deploy
tar xzvf /tmp/ansible-deploy.tar.gz -C /tmp

export QAFILE="/tmp/deploy/group_vars/"$ENV"_vars.yml"
echo "$QAFILE"

chmod +x /tmp/deploy/hosts/ec2.py

if [ -f "$QAFILE" ]; then
      /bin/cp -f /tmp/deploy/group_vars/"$ENV"_vars.yml /var/env_vars.yml
      echo "getting QA VARS"
else
     # If the environment variables are stored in S3, use them instead
      eval "ansible -m s3 -a 'bucket=$BUCKET-$ENV-configs object=/env_vars.yml dest=/var/env_vars.yml mode=get overwrite=true' -c local local"
fi

# Finally run the playbook
cd /tmp/deploy
INTERNAL_IP=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
eval "ansible-playbook -i hosts ./site.yml --limit=$INTERNAL_IP -c local -e '@/var/env_vars.yml' --tags='$ANSIBLE_PLAYBOOK_TAGS'"

