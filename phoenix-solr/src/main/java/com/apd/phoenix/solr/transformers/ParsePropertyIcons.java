package com.apd.phoenix.solr.transformers;

import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;

public class ParsePropertyIcons extends Transformer {

    @Override
    public Object transformRow(Map<String, Object> row, Context context) {
        String iconName = (String) row.get("NAME");
        String iconUrl = (String) row.get("ICONURL");
        String toolTipLabel = (String) row.get("TOOLTIPLABEL");
        String value = (String) row.get("VALUE");
        row.remove("NAME");
        row.remove("ICONURL");
        row.remove("TOOLTIPLABEL");
        row.remove("VALUE");
        if (StringUtils.isNotBlank(iconName) && StringUtils.isNotBlank(value)) {
            String nameValuePair = iconName + " | " + value;
            row.put("ItemPropertyNameValuePair", nameValuePair);
        }

        if (StringUtils.equalsIgnoreCase(value, "n") || StringUtils.equalsIgnoreCase(value, "no")) {
            return row;
        }

        if (StringUtils.isNotBlank(iconUrl)) {
            if (StringUtils.isNotBlank(toolTipLabel)) {
                String mapValue = iconUrl + " | " + toolTipLabel;
                row.put("propertyIconMap", mapValue);
            }
        }
        else {
            String mapValue = iconUrl + " | " + iconName;
            row.put("propertyIconMap", mapValue);
        }
        return row;
    }
}
