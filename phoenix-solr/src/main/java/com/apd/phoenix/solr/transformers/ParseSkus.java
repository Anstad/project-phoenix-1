package com.apd.phoenix.solr.transformers;

import java.util.Map;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;

public class ParseSkus extends Transformer {

    @Override
    public Object transformRow(Map<String, Object> row, Context context) {
        String name = (String) row.get("NAME");
        String value = (String) row.get("VALUE");

        row.remove("NAME");
        row.remove("VALUE");

        if ("APD".equals(name)) {
            row.put("APD_SKU", value);
        }
        else if ("manufacturer".equals(name)) {
            row.put("MANUFACTURER_SKU", value);
        }
        else if ("customer".equals(name)) {
            row.put("CUSTOMER_SKU", value);
        }
        else if ("vendor".equals(name)) {
            row.put("VENDOR_SKU", value);
        }

        return row;
    }
}
