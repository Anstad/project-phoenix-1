package com.apd.phoenix.solr.transformers;

import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;

public class ClassificationIcons extends Transformer {

    @Override
    public Object transformRow(Map<String, Object> row, Context context) {
        String iconName = (String) row.get("NAME");
        String iconUrl = (String) row.get("ICONURL");
        String toolTipLabel = (String) row.get("TOOLTIPLABEL");
        row.remove("NAME");
        row.remove("ICONURL");
        row.remove("TOOLTIPLABEL");
        if (StringUtils.isNotBlank(iconUrl)) {
            if (StringUtils.isNotBlank(toolTipLabel)) {
                String mapValue = iconUrl + " | " + toolTipLabel;
                row.put("classificationIconMap", mapValue);
            }
            else {
                String mapValue = iconUrl + " | " + iconName;
                row.put("classificationIconMap", mapValue);
            }
        }

        return row;
    }
}
