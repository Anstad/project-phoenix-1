package com.apd.phoenix.solr.transformers;

import java.util.Map;
import org.apache.solr.handler.dataimport.Context;
import org.apache.solr.handler.dataimport.Transformer;

public class CombineNameValuePair extends Transformer {

    @Override
    public Object transformRow(Map<String, Object> row, Context context) {
        String name = (String) row.get("NAME");
        String value = (String) row.get("VALUE");
        row.remove("NAME");
        row.remove("VALUE");
        String filters = name + " | " + value;

        row.put("filters", filters);

        return row;
    }
}
