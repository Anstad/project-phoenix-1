<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version last-->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="cost-discrepancy-report" language="groovy" pageWidth="1550" pageHeight="595" orientation="Landscape" columnWidth="1510" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="bdeda0c5-bce9-4cb2-9831-77e88d51dd83">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.jasperserver.url" value="http://10.0.1.115/jasperserver-pro/"/>
	<property name="ireport.jasperserver.report.resource" value="/Reports/APD/invoice-reports/cost-discrepancy-report_files/cost-discrepancy-report_jrxml"/>
	<property name="ireport.jasperserver.reportUnit" value="/Reports/APD/invoice-reports/cost-discrepancy-report"/>
	<parameter name="APD_PO" class="java.lang.String" isForPrompting="false"/>
	<parameter name="START_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="END_DATE" class="java.lang.String" isForPrompting="false"/>
	<queryString language="SQL">
		<![CDATA[SELECT
COALESCE(thisAccount.name, ' ') AS "Account Name",
COALESCE(vendor.name, ' ') AS "Vendor",
COALESCE(vendor.controlAccountNumber, ' ') AS "Control Vendor Acct Number",
COALESCE(apdPo.value, ' ') AS "APD PO",
COALESCE(customerPo.value, ' ') AS "Customer PO",
customerorder.orderDate AS "Order Date",
invoice.invoiceDate AS "Vendor Invoice Date",
COALESCE(invoice.invoiceNumber, ' ') AS "Vendor Invoice Number",
COALESCE(expectedLineItem.apdSku, ' ') AS "APD SKU",
COALESCE(actualLineItem.supplierPartId, ' ') AS "Vendor SKU",
COALESCE(actualLineItem.shortName, actualLineItem.description, ' ') AS "Product Name/Description",
COALESCE(expectedLineItem.quantity, 0) AS "Expected QTY Billed",
COALESCE(actualLineItem.quantity, 0) AS "Actual QTY Billed",
COALESCE(expectedLineItem.cost, 0) AS "Expected Unit Cost",
COALESCE(actualLineItem.cost, 0) AS "Actual Unit Cost",
COALESCE(expectedLineItem.cost, 0) * expectedLineItem.quantity AS "Expected Total Cost",
COALESCE(actualLineItem.cost, 0) * actualLineItem.quantity AS "Actual Total Cost",
(SELECT LISTAGG (lineItem_vendorComments.vendorComment, ', ') WITHIN GROUP (ORDER BY lineItem_vendorComments.vendorComment desc) "GROUP_LIST"
        FROM lineItem_vendorComments WHERE lineItem_vendorComments.LINEITEM_ID = ACTUALLINEITEM.ID) AS "Rejection Reason",
CASE WHEN invoice.valid = 1 THEN 'VALID' WHEN invoice.valid = 0 THEN 'INVALID' ELSE 'NULL (legacy invoices)' END AS "Vendor Invoice Status"
FROM INVOICE
LEFT JOIN CUSTOMERORDER ON (INVOICE.CUSTOMERORDER_ID = CUSTOMERORDER.ID)
LEFT JOIN PONUMBER_CUSTOMERORDER apdPoJoin
	JOIN PONUMBERTYPE apdPoType ON (apdPoType.NAME = 'APD')
	JOIN PONUMBER apdPo
	ON (apdPo.ID = apdPoJoin.PONUMBERS_ID AND apdPo.TYPE_ID = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN PONUMBER_CUSTOMERORDER customerPoJoin
	JOIN PONUMBERTYPE customerPoType ON (customerPoType.NAME = 'Customer')
	JOIN PONUMBER customerPo
	ON (customerPo.ID = customerPoJoin.PONUMBERS_ID AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID = customerPoJoin.ORDERS_ID)
LEFT JOIN ACCOUNT thisAccount ON (thisAccount.ID = CUSTOMERORDER.ACCOUNT_ID)
LEFT JOIN CREDENTIAL ON (CREDENTIAL.ID = CUSTOMERORDER.CREDENTIAL_ID)
LEFT JOIN Credential_PropertyType thresholdProperty
	JOIN CREDENTIALPROPERTYTYPE thresholdPropertyType
	ON (thresholdProperty.TYPE_ID = thresholdPropertyType.ID AND thresholdPropertyType.NAME = 'cost-price discrepancy threshold')
ON (thresholdProperty.CRED_ID = CREDENTIAL.ID)
LEFT JOIN INVOICE_LINEITEM ON (INVOICE.ID = INVOICE_LINEITEM.INVOICE_ID)
LEFT JOIN LINEITEM actualLineItem ON (INVOICE_LINEITEM.ACTUALITEMS_ID = actualLineItem.ID)
INNER JOIN LINEITEM expectedLineItem ON (actualLineItem.EXPECTED_ID = expectedLineItem.ID)
LEFT JOIN VENDOR ON (expectedLineItem.VENDOR_ID = VENDOR.ID)
LEFT JOIN LINEITEMSTATUS ON (actualLineItem.STATUS_ID = LINEITEMSTATUS.ID)
WHERE (COALESCE(actualLineItem.cost, 0) - COALESCE(expectedLineItem.cost, 0)) / GREATEST(COALESCE(expectedLineItem.cost, 0), 0.01) > COALESCE(TO_NUMBER(thresholdProperty.VALUE), 0.1) AND
(apdPo.value LIKE COALESCE($P{APD_PO}, '%')) AND INVOICE.FROM_CLASS = 'VENDOR_INVOICE' AND 
invoice.createdDate BETWEEN TO_DATE(COALESCE($P{START_DATE}, '0001-01-01'), 'YYYY-MM-DD') AND TO_DATE(COALESCE($P{END_DATE}, '9999-12-31'), 'YYYY-MM-DD')]]>
	</queryString>
	<field name="Account Name" class="java.lang.String"/>
	<field name="Vendor" class="java.lang.String"/>
	<field name="Control Vendor Acct Number" class="java.lang.String"/>
	<field name="APD PO" class="java.lang.String"/>
	<field name="Customer PO" class="java.lang.String"/>
	<field name="Order Date" class="java.sql.Timestamp"/>
	<field name="Vendor Invoice Date" class="java.sql.Timestamp"/>
	<field name="Vendor Invoice Number" class="java.lang.String"/>
	<field name="APD SKU" class="java.lang.String"/>
	<field name="Vendor SKU" class="java.lang.String"/>
	<field name="Product Name/Description" class="java.lang.String"/>
	<field name="Expected QTY Billed" class="java.math.BigDecimal"/>
	<field name="Actual QTY Billed" class="java.math.BigDecimal"/>
	<field name="Expected Unit Cost" class="java.math.BigDecimal"/>
	<field name="Actual Unit Cost" class="java.math.BigDecimal"/>
	<field name="Expected Total Cost" class="java.math.BigDecimal"/>
	<field name="Actual Total Cost" class="java.math.BigDecimal"/>
	<field name="Rejection Reason" class="java.lang.String"/>
	<field name="Vendor Invoice Status" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement x="400" y="0" width="100" height="30" uuid="e9ad99b1-0251-4a42-8904-37f2151830a2"/>
				<text><![CDATA[Account Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="100" height="30" uuid="21eb95dd-872a-4280-9a68-1957f1ba652d"/>
				<text><![CDATA[Vendor]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="0" width="100" height="30" uuid="21eb95dd-872a-4280-9a68-1957f1ba652d"/>
				<text><![CDATA[Control Vendor Acct Number]]></text>
			</staticText>
			<staticText>
				<reportElement x="500" y="0" width="100" height="30" uuid="ca59c782-d7c7-492f-92ca-6fe5facd69d2"/>
				<text><![CDATA[APD PO]]></text>
			</staticText>
			<staticText>
				<reportElement x="600" y="0" width="100" height="30" uuid="40662ff0-c402-4caa-a54c-d4faf6154867"/>
				<text><![CDATA[Customer PO]]></text>
			</staticText>
			<staticText>
				<reportElement x="700" y="0" width="100" height="30" uuid="82ea946c-43ca-4369-b505-24b8d46a01b6"/>
				<text><![CDATA[Order Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="300" y="0" width="100" height="30" uuid="fea1caea-01e4-426f-b33f-176b6e5cdb57"/>
				<text><![CDATA[Vendor Invoice Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="200" y="0" width="100" height="30" uuid="ea0d4433-d166-4aec-aa88-6d8948c768ff"/>
				<text><![CDATA[Vendor Invoice Number]]></text>
			</staticText>
			<staticText>
				<reportElement x="800" y="0" width="100" height="30" uuid="050ee588-3ff1-4be0-b5c5-9d22282c6e37"/>
				<text><![CDATA[APD SKU]]></text>
			</staticText>
			<staticText>
				<reportElement x="900" y="0" width="100" height="30" uuid="9907966c-b3bc-4895-b77d-ebdbe7cb02ae"/>
				<text><![CDATA[Vendor SKU]]></text>
			</staticText>
			<staticText>
				<reportElement x="1000" y="0" width="100" height="30" uuid="754e0990-317c-4131-9832-dccc55da1058"/>
				<text><![CDATA[Product Name/Description]]></text>
			</staticText>
			<staticText>
				<reportElement x="1100" y="0" width="100" height="30" uuid="7ef0666f-be8d-49bd-932f-83104b392cd7"/>
				<text><![CDATA[Expected QTY Billed]]></text>
			</staticText>
			<staticText>
				<reportElement x="1200" y="0" width="100" height="30" uuid="4d175e66-f6b4-451d-925d-a4dc3299a97f"/>
				<text><![CDATA[Actual QTY Billed]]></text>
			</staticText>
			<staticText>
				<reportElement x="1300" y="0" width="100" height="30" uuid="7ef0666f-be8d-49bd-932f-83104b392cd7"/>
				<text><![CDATA[Expected Unit Cost]]></text>
			</staticText>
			<staticText>
				<reportElement x="1400" y="0" width="100" height="30" uuid="4d175e66-f6b4-451d-925d-a4dc3299a97f"/>
				<text><![CDATA[Actual Unit Cost]]></text>
			</staticText>
			<staticText>
				<reportElement x="1500" y="0" width="100" height="30" uuid="d289a9e1-2c26-4865-bed9-d1da587e97cc"/>
				<text><![CDATA[Expected Total Cost]]></text>
			</staticText>
			<staticText>
				<reportElement x="1600" y="0" width="100" height="30" uuid="faeab2ab-c2fb-48ae-90a8-8df61ca9ada7"/>
				<text><![CDATA[Actual Total Cost]]></text>
			</staticText>
			<staticText>
				<reportElement x="1700" y="0" width="100" height="30" uuid="faeab2ab-c2fb-48ae-90a8-8df61ca9ada7"/>
				<text><![CDATA[Rejection Reason]]></text>
			</staticText>
			<staticText>
				<reportElement x="1800" y="0" width="100" height="30" uuid="faeab2ab-c2fb-48ae-90a8-8df61ca9ada7"/>
				<text><![CDATA[Vendor Invoice Status]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="160" splitType="Stretch">
			<textField>
				<reportElement x="400" y="0" width="100" height="30" uuid="94d4737f-6d88-439f-bfb6-1cee65386e20"/>
				<textFieldExpression><![CDATA[$F{Account Name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="100" y="0" width="100" height="30" uuid="0b0c4e42-b695-4e19-b252-3cd1f0608f50"/>
				<textFieldExpression><![CDATA[$F{Control Vendor Acct Number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="500" y="0" width="100" height="30" uuid="1931baa2-821b-4227-a3ab-d7bb2b8e8551"/>
				<textFieldExpression><![CDATA[$F{APD PO}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="600" y="0" width="100" height="30" uuid="925511bc-4af0-4ae0-a811-cbe276c2d3d7"/>
				<textFieldExpression><![CDATA[$F{Customer PO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="700" y="0" width="100" height="30" uuid="27e454eb-0e6d-447c-a94f-a378c51ad7c8"/>
				<textFieldExpression><![CDATA[$F{Order Date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="300" y="0" width="100" height="30" uuid="dd1b065b-87a5-454f-9c1d-8f47bc52b904"/>
				<textFieldExpression><![CDATA[$F{Vendor Invoice Date}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="200" y="0" width="100" height="30" uuid="55128abe-7c38-4ebf-80c5-02f3aab337f5"/>
				<textFieldExpression><![CDATA[$F{Vendor Invoice Number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="800" y="0" width="100" height="30" uuid="07a5d058-3049-4cba-828b-a98c844f1941"/>
				<textFieldExpression><![CDATA[$F{APD SKU}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="900" y="0" width="100" height="30" uuid="2f5b96f1-0560-4c7e-bc56-f050abaf8f41"/>
				<textFieldExpression><![CDATA[$F{Vendor SKU}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1000" y="0" width="100" height="30" uuid="b7429c7b-19a8-4c89-bad9-299867113a24"/>
				<textFieldExpression><![CDATA[$F{Product Name/Description}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1100" y="0" width="100" height="30" uuid="507d767b-8d67-42e3-9db2-60564ef55ea0"/>
				<textFieldExpression><![CDATA[$F{Expected QTY Billed}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1200" y="0" width="100" height="30" uuid="57c91cb4-8279-4cc7-8a92-4e76a522c3ea"/>
				<textFieldExpression><![CDATA[$F{Actual QTY Billed}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1300" y="0" width="100" height="30" uuid="507d767b-8d67-42e3-9db2-60564ef55ea0"/>
				<textFieldExpression><![CDATA[$F{Expected Unit Cost}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1400" y="0" width="100" height="30" uuid="57c91cb4-8279-4cc7-8a92-4e76a522c3ea"/>
				<textFieldExpression><![CDATA[$F{Actual Unit Cost}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1500" y="0" width="100" height="30" uuid="b51abebb-3401-402e-be72-cacdb92c6044"/>
				<textFieldExpression><![CDATA[$F{Expected Total Cost}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1600" y="0" width="100" height="30" uuid="c580ff4f-b475-438f-a551-b578ee3f9256"/>
				<textFieldExpression><![CDATA[$F{Actual Total Cost}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1700" y="0" width="100" height="30" uuid="2f5b96f1-0560-4c7e-bc56-f050abaf8f41"/>
				<textFieldExpression><![CDATA[$F{Rejection Reason}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="1800" y="0" width="100" height="30" uuid="2f5b96f1-0560-4c7e-bc56-f050abaf8f41"/>
				<textFieldExpression><![CDATA[$F{Vendor Invoice Status}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="0" y="0" width="100" height="30" uuid="0b0c4e42-b695-4e19-b252-3cd1f0608f50"/>
				<textFieldExpression><![CDATA[$F{Vendor}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
