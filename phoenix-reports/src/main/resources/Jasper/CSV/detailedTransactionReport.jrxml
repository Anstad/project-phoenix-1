<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="detailedTransactionReport" language="groovy" pageWidth="792" pageHeight="612" orientation="Landscape" columnWidth="752" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="3d187f39-2c72-4a3d-85f6-3d4efd6f43db">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="174"/>
	<parameter name="ROOT_ACCOUNT_NAME" class="java.util.List" isForPrompting="false"/>
	<parameter name="USER_LOGIN" class="java.lang.String" isForPrompting="false"/>
	<parameter name="APD_PO" class="java.lang.String" isForPrompting="false"/>
	<parameter name="START_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="END_DATE" class="java.lang.String" isForPrompting="false"/>
	<queryString language="SQL">
		<![CDATA[SELECT
COALESCE(account.name, ' ')                                                                                      AS "Account",
COALESCE(SYSTEMUSER.LOGIN, ' ')                                                                                  AS "User",
COALESCE(shipAddress.LINE1, ' ')                                                                                 AS "Ship Line 1",
COALESCE(shipAddress.LINE2, ' ')                                                                                 AS "Ship Line 2",
COALESCE(shipAddress.CITY, ' ')                                                                                  AS "Ship City",
COALESCE(shipAddress.STATE, ' ')                                                                                 AS "Ship State",
COALESCE(shipAddress.ZIP, ' ')                                                                                   AS "Ship ZIP",
COALESCE(shipAddress.COUNTRY, ' ')                                                                               AS "Ship Country",
COALESCE(customerPo.VALUE, ' ')                                                                                  AS "Customer PO",
COALESCE(apdPo.VALUE, ' ')                                                                                       AS "APD PO",
ORDER_LOG.TIMESTAMP                                                                                              AS "Sent To Vendor",
CUSTOMERORDER.ORDERDATE                                                                                          AS "Request Date",
COALESCE(PERSON.PREFIX, ' ')                                                                                     AS "Requester Prefix",
COALESCE(PERSON.FIRSTNAME, ' ')                                                                                  AS "Requester First",
COALESCE(PERSON.MIDDLEINITIAL, ' ')                                                                              AS "Requester Middle",
COALESCE(PERSON.LASTNAME, ' ')                                                                                   AS "Requester Last",
COALESCE(PERSON.SUFFIX, ' ')                                                                                     AS "Requester Suffix",
COALESCE(PERSON.EMAIL, ' ')                                                                                      AS "Requester Email",
COALESCE(PHONENUMBER.AREACODE, ' ')                                                                              AS "Requester Area Code",
COALESCE(PHONENUMBER.COUNTRYCODE, ' ')                                                                           AS "Requester Country Code",
COALESCE(PHONENUMBER.EXCHANGE, ' ')                                                                              AS "Requester Exchange",
COALESCE(PHONENUMBER.EXTENSION, ' ')                                                                             AS "Requester Extension",
COALESCE(PHONENUMBER.LINENUMBER, ' ')                                                                            AS "Requester Line Num",
COALESCE(SKU.VALUE, ' ')                                                                                         AS "Customer SKU",
COALESCE(LINEITEM.APDSKU, ' ')                                                                                   AS "APD SKU",
COALESCE(LINEITEM.SUPPLIERPARTID, ' ')                                                                           AS "Supplier SKU",
COALESCE(LINEITEM.DESCRIPTION, ' ')                                                                              AS "Item Description",
COALESCE(VENDOR.NAME, ' ')                                                                                       AS "Vendor Name",
COALESCE(UNITOFMEASURE.NAME, ' ')                                                                                AS "Unit Of Measure",
LINEITEM.QUANTITY                                                                                                AS "Total Qty Ordered",
LINEITEMXRETURN.REASON                                                                                           AS "Return Reason",
LINEITEM.UNITPRICE                                                                                               AS "Unit Price",
COALESCE(SHIPMENT.TRACKINGNUMBER, ' ')                                                                           AS "Tracking Number",
LINEITEMXSHIPMENT.QUANTITY                                                                                       AS "Qty Shipped",
COALESCE(shipstatus.VALUE, ' ')                                                                                  AS "Current Shipment Status",
shiplog.UPDATETIMESTAMP                                                                                          AS "Last Status Update",
lineitemxshipment.shipping                                                                                       AS "Ship Price"

FROM
lineitemxshipment
LEFT JOIN lineitem ON(lineitemxshipment.lineitem_id = lineitem.id)
LEFT JOIN shipment ON(lineitemxshipment.shipments_id = shipment.id)
LEFT JOIN customerorder ON(lineitem.order_id = customerorder.id)
LEFT JOIN account ON(customerorder.account_id = account.id)
LEFT JOIN account ROOTACCOUNT ON(ACCOUNT.ROOTACCOUNT_ID = ROOTACCOUNT.ID)
LEFT JOIN systemuser ON(customerorder.user_id = systemuser.id)
LEFT JOIN ADDRESS shipAddress ON(CUSTOMERORDER.ADDRESS_ID = shipAddress.ID)
LEFT JOIN PERSON ON(PERSON.USER_ID = SYSTEMUSER.ID)
LEFT JOIN PHONENUMBER ON(PERSON.ID = PHONENUMBER.PERSON_ID)
LEFT JOIN SKU ON(lineitem.customersku_id = sku.id)
LEFT JOIN VENDOR ON(VENDOR.ID = LINEITEM.VENDOR_ID)
LEFT JOIN UNITOFMEASURE ON(UNITOFMEASURE.ID = LINEITEM.UNITOFMEASURE_ID)
LEFT JOIN
    (SELECT *
    FROM ORDER_LOG results1
    WHERE results1.updatetimestamp = ( SELECT MAX(results2.UPDATETIMESTAMP) FROM ORDER_LOG results2 WHERE results1.shipment_id = results2.shipment_id )
    ) shiplog
    INNER JOIN ORDERSTATUS shipstatus ON(shiplog.orderstatus_id = shipstatus.id)
ON(shiplog.shipment_id = shipment.id)
LEFT JOIN (ORDERSTATUS
    INNER JOIN ORDER_LOG
        ON(ORDERSTATUS.ID = ORDER_LOG.ORDERSTATUS_ID AND ORDERSTATUS.VALUE = 'SENT_TO_VENDOR')
    )
ON(ORDERSTATUS.ID = ORDER_LOG.ORDERSTATUS_ID)
LEFT JOIN (PONUMBER_CUSTOMERORDER apdPoJoin
    INNER JOIN PONUMBERTYPE apdPoType
        ON (apdPoType.NAME = 'APD')
    INNER JOIN PONUMBER apdPo
        ON apdPo.ID          = apdPoJoin.PONUMBERS_ID
        AND apdPo.TYPE_ID    = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN (PONUMBER_CUSTOMERORDER customerPoJoin
    INNER JOIN PONUMBERTYPE customerPoType
        ON (customerPoType.NAME = 'Customer')
    INNER JOIN PONUMBER customerPo
        ON customerPo.ID       = customerPoJoin.PONUMBERS_ID
        AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID   = customerPoJoin.ORDERS_ID)
LEFT JOIN COSTCENTER ON (COSTCENTER.ID = CUSTOMERORDER.ASSIGNEDCC_CC_ID)
LEFT JOIN LINEITEMXRETURN ON (LINEITEM.ID = LINEITEMXRETURN.LINEITEM_ID)
WHERE LOWER(SYSTEMUSER.LOGIN) LIKE LOWER(COALESCE($P{USER_LOGIN}, '%')) AND
($X{IN, ACCOUNT.NAME, ROOT_ACCOUNT_NAME} OR $X{IN, ROOTACCOUNT.NAME, ROOT_ACCOUNT_NAME}) AND
(apdPo.value LIKE COALESCE($P{APD_PO}, '%')) AND
CUSTOMERORDER.ORDERDATE BETWEEN TO_DATE(COALESCE($P{START_DATE}, '0001-01-01'), 'YYYY-MM-DD') AND TO_DATE(COALESCE($P{END_DATE}, '9999-12-31'), 'YYYY-MM-DD')
GROUP BY
account.name,
SYSTEMUSER.LOGIN,
shipAddress.LINE1,
shipAddress.LINE2,
shipAddress.CITY,
shipAddress.STATE,
shipAddress.ZIP,
shipAddress.COUNTRY,
customerPo.VALUE,
apdPo.VALUE,
ORDER_LOG.TIMESTAMP,
CUSTOMERORDER.ORDERDATE,
PERSON.PREFIX,
PERSON.FIRSTNAME,
PERSON.MIDDLEINITIAL,
PERSON.LASTNAME,
PERSON.SUFFIX,
PERSON.EMAIL,
PHONENUMBER.AREACODE,
PHONENUMBER.COUNTRYCODE,
PHONENUMBER.EXCHANGE,
PHONENUMBER.EXTENSION,
PHONENUMBER.LINENUMBER,
SKU.VALUE,
LINEITEM.APDSKU,
LINEITEM.SUPPLIERPARTID,
LINEITEM.DESCRIPTION,
VENDOR.NAME,
UNITOFMEASURE.NAME,
LINEITEM.QUANTITY,
LINEITEMXRETURN.REASON,
LINEITEM.UNITPRICE,
SHIPMENT.TRACKINGNUMBER,
LINEITEMXSHIPMENT.QUANTITY,
shipstatus.VALUE,
shiplog.UPDATETIMESTAMP,
lineitemxshipment.shipping]]>
	</queryString>
	<field name="Account" class="java.lang.String"/>
	<field name="User" class="java.lang.String"/>
	<field name="Ship Line 1" class="java.lang.String"/>
	<field name="Ship Line 2" class="java.lang.String"/>
	<field name="Ship City" class="java.lang.String"/>
	<field name="Ship State" class="java.lang.String"/>
	<field name="Ship ZIP" class="java.lang.String"/>
	<field name="Ship Country" class="java.lang.String"/>
	<field name="Customer PO" class="java.lang.String"/>
	<field name="APD PO" class="java.lang.String"/>
	<field name="Sent To Vendor" class="oracle.sql.TIMESTAMP"/>
	<field name="Request Date" class="oracle.sql.TIMESTAMP"/>
	<field name="Requester Prefix" class="java.lang.String"/>
	<field name="Requester First" class="java.lang.String"/>
	<field name="Requester Middle" class="java.lang.String"/>
	<field name="Requester Last" class="java.lang.String"/>
	<field name="Requester Suffix" class="java.lang.String"/>
	<field name="Requester Email" class="java.lang.String"/>
	<field name="Requester Area Code" class="java.lang.String"/>
	<field name="Requester Country Code" class="java.lang.String"/>
	<field name="Requester Exchange" class="java.lang.String"/>
	<field name="Requester Extension" class="java.lang.String"/>
	<field name="Requester Line Num" class="java.lang.String"/>
	<field name="Customer SKU" class="java.lang.String"/>
	<field name="APD SKU" class="java.lang.String"/>
	<field name="Supplier SKU" class="java.lang.String"/>
	<field name="Item Description" class="java.lang.String"/>
	<field name="Vendor Name" class="java.lang.String"/>
	<field name="Unit Of Measure" class="java.lang.String"/>
	<field name="Total Qty Ordered" class="java.math.BigDecimal"/>
	<field name="Return Reason" class="java.lang.String"/>
	<field name="Unit Price" class="java.math.BigDecimal"/>
	<field name="Tracking Number" class="java.lang.String"/>
	<field name="Qty Shipped" class="java.math.BigDecimal"/>
	<field name="Current Shipment Status" class="java.lang.String"/>
	<field name="Last Status Update" class="oracle.sql.TIMESTAMP"/>
	<field name="Ship Price" class="java.math.BigDecimal"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="78" splitType="Stretch">
			<staticText>
				<reportElement uuid="d2adf54e-d945-4ab0-8616-64756902eb6d" x="0" y="0" width="752" height="78">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font size="24"/>
				</textElement>
				<text><![CDATA[Detailed Transaction Report]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band height="35" splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="61" splitType="Stretch">
			<rectangle>
				<reportElement uuid="202b2f34-2bb8-42ff-b45b-9dac3a4c8473" x="396" y="19" width="100" height="20"/>
			</rectangle>
			<staticText>
				<reportElement uuid="176439c8-661a-47b5-82fc-43323eb278e0" mode="Opaque" x="0" y="0" width="100" height="40" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Account / Customer]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f60af46c-138c-4984-ba28-c85a104814ae" mode="Opaque" x="100" y="0" width="134" height="40" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Ship To]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d93b218f-2c70-417f-9e91-318838ad371c" mode="Opaque" x="234" y="0" width="100" height="40" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Customer/Billing PO APD Order #]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="89e12e6b-0ee8-41a5-a8ab-1c125f55d0ba" mode="Opaque" x="334" y="0" width="80" height="40" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Order Created Sent To Vendor]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="6220c8c4-5bae-479f-a879-4911358382b0" mode="Opaque" x="414" y="20" width="22" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Unit]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2e55a193-5822-463e-b0a0-5287e5348a64" mode="Opaque" x="436" y="20" width="50" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total Qty Ordered]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b55d2c6e-f923-40f9-a7de-355fd991886d" mode="Opaque" x="600" y="20" width="46" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Qty Shipped]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="eaff6b0e-6ca9-440c-86ba-9dcff0ba9df4" mode="Opaque" x="486" y="20" width="38" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Return Reason]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="9f0c6de4-823a-4d66-81fe-f3f80bcee80c" mode="Opaque" x="524" y="20" width="34" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Unit Price]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="0b1d7295-a757-4442-957a-70b7ff4c45cd" mode="Opaque" x="558" y="20" width="42" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Tracking Number]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d060b138-f7f2-4777-b7e7-62a3943f2268" mode="Opaque" x="646" y="20" width="30" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Ship Price]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="7abcc418-491e-4fdc-9834-3de0e5255629" mode="Opaque" x="676" y="20" width="76" height="41" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Current Shipment Status]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="623c4d87-a697-4f77-835d-1059770937bf" mode="Opaque" x="414" y="0" width="338" height="20" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box>
					<bottomPen lineWidth="2.0"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Contact Info]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="dc0fdeb6-34ea-4fcc-bc1a-a07d590c1b32" mode="Opaque" x="0" y="40" width="414" height="21" forecolor="#FFFFFF" backcolor="#CC0000">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<box>
					<topPen lineWidth="2.0"/>
				</box>
				<textElement textAlignment="Center">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Part Numbers / Item Description / Ordered From]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="166" splitType="Stretch">
			<rectangle>
				<reportElement uuid="d3ccc797-de54-40d6-89fc-a9e7d82e0ee9" x="0" y="80" width="752" height="86" backcolor="#FFFFCC"/>
			</rectangle>
			<textField>
				<reportElement uuid="812b965a-ed15-4889-9352-a3fc4385ec5f" x="0" y="0" width="100" height="40">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Account}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="47d3030b-c93f-414b-a263-d2c1270712ca" x="0" y="40" width="100" height="40">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{User}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="816ed76f-a0b1-497d-9a35-933dfa4ecf4b" x="100" y="0" width="134" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Ship Line 1}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="9ffa84af-5e25-472c-bb00-dbe4bfb11e7b" x="100" y="20" width="134" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Ship Line 2}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="89639eb3-e08d-4dd0-a0b3-386f062638ac" x="100" y="40" width="134" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Ship City} + ", " + $F{Ship State} + " " + $F{Ship ZIP}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="a0fa843c-a120-44d2-8278-0ca8edc8cb14" x="100" y="60" width="100" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Ship Country}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="46249451-2bb7-43c2-b1f4-373e221491cf" x="234" y="40" width="100" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Customer PO}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="a6f19b08-5421-4a39-9e60-567112e3ee85" x="234" y="60" width="100" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{APD PO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="33c883ce-83ea-40a5-804d-1998496af5be" x="334" y="60" width="120" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Sent To Vendor}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="7ccff944-5639-4637-b7ad-5602ffa471cd" x="334" y="40" width="120" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Request Date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="d8906ee3-895f-4894-baa5-40bef2242828" isPrintRepeatedValues="false" x="454" y="20" width="616" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA["Requester Name: " + ($F{Requester Prefix} == null ? "":($F{Requester Prefix} + " ")) + ($F{Requester First} == null ? "":($F{Requester First} + " ")) + ($F{Requester Middle} == null ? "":($F{Requester Middle} + " ")) + ($F{Requester Last} == null ? "":($F{Requester Last} + " ")) + ($F{Requester Suffix} == null ? "":$F{Requester Suffix})]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="0026d2e5-4979-46e6-8285-13e340c8bb49" x="454" y="60" width="298" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA["Email Address: " + $F{Requester Email}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="c5023ca1-15e4-40b9-9c88-b7b12cedbcba" x="0" y="85" width="140" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA["Customer Part #: " + $F{Customer SKU}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="4935a858-d2a4-41ee-acc7-372e9dfa4d57" x="0" y="105" width="140" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA["APD Part #: " + $F{APD SKU}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="1d0b0630-b358-4b33-bec4-a99e8a67357f" x="0" y="125" width="140" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA["Vendor Part #: " + $F{Supplier SKU}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="763d3549-d77a-423f-94c5-37a119aab35d" x="140" y="105" width="260" height="40">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Item Description}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="7a8b709f-8ba8-4738-a94c-fcaafc3643e0" x="140" y="145" width="260" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement/>
				<textFieldExpression><![CDATA[$F{Vendor Name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="e972829f-dca3-4209-aa95-3daad809f5de" x="455" y="40" width="616" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement>
					<font size="10"/>
				</textElement>
				<textFieldExpression><![CDATA["Requester Phone: " + ($F{Requester Country Code} == null ? "":($F{Requester Country Code} + "-")) + $F{Requester Area Code} + "-" + $F{Requester Exchange} + "-" + $F{Requester Line Num} + ($F{Requester Extension} == null ? "":("-" + $F{Requester Extension}))]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="01e76dbd-03f0-44fb-917d-bb37d156bfac" x="416" y="125" width="20" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Unit Of Measure}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="b4358656-88b9-416d-9fe5-863ac608a0d3" x="436" y="125" width="50" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Total Qty Ordered}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="4b1f326b-5bb6-405b-af85-efa86d96b66a" x="486" y="125" width="38" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Return Reason}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="5cb7865e-5a88-4b0a-b869-96385f058d6f" x="524" y="125" width="34" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Unit Price}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="9af715dc-c179-4bc6-8490-38a1d4812e53" x="558" y="125" width="42" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Tracking Number}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f73fc285-5b49-4722-9fe8-ccaaf65b49ce" x="600" y="125" width="46" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Qty Shipped}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="a11ee184-f5f7-4344-be5f-1c14b61a98b7" x="646" y="125" width="30" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Ship Price}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="89e97309-a065-4f73-a7eb-cce5ff9b1e7d" x="676" y="125" width="76" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Current Shipment Status}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="f4f3ad16-2bc6-4e03-ad6e-d7726cb8254e" x="676" y="105" width="76" height="20">
					<property name="net.sf.jasperreports.print.keep.full.text" value="true"/>
				</reportElement>
				<textElement textAlignment="Center"/>
				<textFieldExpression><![CDATA[$F{Last Status Update}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="45" splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="54" splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="42" splitType="Stretch"/>
	</summary>
</jasperReport>
