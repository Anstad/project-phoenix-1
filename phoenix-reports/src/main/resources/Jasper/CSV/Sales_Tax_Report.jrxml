<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Sales_Tax_Report" language="groovy" pageWidth="3250" pageHeight="555" orientation="Landscape" columnWidth="3214" leftMargin="18" rightMargin="18" topMargin="18" bottomMargin="18" uuid="7bae5fee-6bc9-48ea-872d-844a912ae58a">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="ROOT_ACCOUNT_NAME" class="java.util.List" isForPrompting="false"/>
	<parameter name="STATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="START_DATE" class="java.lang.String" isForPrompting="false"/>
	<parameter name="END_DATE" class="java.lang.String" isForPrompting="false"/>
	<queryString>
		<![CDATA[SELECT
  shipToAddress.STATE        AS "ShipTo State",
  shipToAddress.COUNTY       AS "ShipTo County",
  shipToAddress.CITY         AS "ShipTo City",
  shipToAddress.ZIP          AS "ShipTo ZIP",
  shipToAddress.GEOCODE      AS "ShipTo GeoCode",
  ACCOUNT.NAME               AS "Account Name",
  apdPo.VALUE                AS "APD PO",
  INVOICE.INVOICENUMBER      AS "Invoice Number",
  SHIPMENT.TRACKINGNUMBER    AS "Tracking Number",
  VENDOR.NAME                AS "Product Source",
  customerPo.VALUE           AS "Customer PO",
  LINEITEM.LINENUMBER        AS "Item Number",
  COALESCE(lineitemxshipment.quantity*lineitem.unitprice, -(lineitemxreturn.quantity*lineitem.unitprice), 0) AS "Total Order",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(stateTax.VALUE, 0)          AS "State Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(countyTax.VALUE, 0)          AS "CountyTax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(cityTax.VALUE, 0)            AS "City Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(districtTax.VALUE, 0)       AS "District Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(countyLocalTax.VALUE, 0)    AS "County Local Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(cityLocalTax.VALUE, 0)    AS "City Local Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * (COALESCE(stateTax.VALUE, 0) + COALESCE(countyTax.VALUE, 0) + COALESCE(cityTax.VALUE, 0) + COALESCE(districtTax.VALUE, 0) + COALESCE(countyLocalTax.VALUE, 0) + COALESCE(cityLocalTax.VALUE, 0))      AS "Total Tax",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * COALESCE(SHIPMENT.TOTALFREIGHTPRICE, 0) AS "Total Freight",

  CASE WHEN CUSTOMERORDER.TAXASSIGNED = 1 THEN 0 WHEN LINEITEM.TAXABLE = 1 THEN 0 ELSE COALESCE(lineitemxshipment.quantity*lineitem.unitprice, -(lineitemxreturn.quantity*lineitem.unitprice), 0) END AS "Total Exempt Sales",
  (CASE WHEN lineitemxreturn.id is not null THEN -1 ELSE 1 END) * (COALESCE(lineitemxshipment.quantity*lineitem.unitprice, lineitemxreturn.quantity*lineitem.unitprice, 0) + COALESCE(stateTax.VALUE, 0) + COALESCE(countyTax.VALUE, 0) + COALESCE(cityTax.VALUE, 0) + COALESCE(districtTax.VALUE, 0) + COALESCE(countyLocalTax.VALUE, 0) + COALESCE(cityLocalTax.VALUE, 0) + COALESCE(SHIPMENT.TOTALFREIGHTPRICE, 0)) AS "Total Sales",
  shipToAddress.LINE1        AS "ShipTo Address1",
  shipToAddress.LINE2        AS "ShipTo Address2",
  billToAddress.LINE1        AS "BillTo Address1",
  billToAddress.LINE2        AS "BillTo Address2",
  billToAddress.STATE        AS "BillTo State",
  billToAddress.CITY         AS "BillTo City",
  billToAddress.ZIP          AS "BillTo ZIP",
  CUSTOMERORDER.ORDERDATE    AS "Order Date",
  SHIPMENT.SHIPTIME          AS "Ship Date",
  COALESCE(INVOICE.CREATEDDATE, ccTransactionLog.timestamp, ccTransactionLog.updateTimestamp, RETURNORDER.reconciledDate) AS "Charge/Invoice Date"
FROM SHIPMENT
FULL OUTER JOIN RETURNORDER ON (1=0)
JOIN CUSTOMERORDER ON (CUSTOMERORDER.ID = SHIPMENT.ORDER_ID OR CUSTOMERORDER.ID = RETURNORDER.ORDER_ID)
LEFT JOIN INVOICE
ON (RETURNORDER.ID = INVOICE.RETURNORDER_ID OR INVOICE.SHIPMENT_ID = SHIPMENT.ID)
LEFT JOIN PAYMENTINFORMATION
ON PAYMENTINFORMATION.ID = CUSTOMERORDER.PAYMENTINFORMATION_ID
LEFT JOIN ADDRESS shipToAddress
ON shipToAddress.ID = CUSTOMERORDER.ADDRESS_ID
LEFT JOIN ADDRESS billToAddress
ON billToAddress.ID = PAYMENTINFORMATION.BILLINGADDRESS_ID
LEFT JOIN ACCOUNT
ON ACCOUNT.ID = CUSTOMERORDER.ACCOUNT_ID
LEFT JOIN ACCOUNT ROOTACCOUNT
ON ACCOUNT.ROOTACCOUNT_ID = ROOTACCOUNT.ID
LEFT JOIN (PONUMBER_CUSTOMERORDER apdPoJoin
INNER JOIN PONUMBERTYPE apdPoType
    ON (apdPoType.NAME = 'APD')
    INNER JOIN PONUMBER apdPo
    ON apdPo.ID          = apdPoJoin.PONUMBERS_ID
    AND apdPo.TYPE_ID    = apdPoType.ID)
ON (CUSTOMERORDER.ID = apdPoJoin.ORDERS_ID)
LEFT JOIN (PONUMBER_CUSTOMERORDER customerPoJoin
    INNER JOIN PONUMBERTYPE customerPoType
    ON (customerPoType.NAME = 'Customer')
    INNER JOIN PONUMBER customerPo
    ON customerPo.ID       = customerPoJoin.PONUMBERS_ID
    AND customerPo.TYPE_ID = customerPoType.ID)
ON (CUSTOMERORDER.ID   = customerPoJoin.ORDERS_ID)
LEFT JOIN LINEITEMXSHIPMENT
ON SHIPMENT.ID = LINEITEMXSHIPMENT.SHIPMENTS_ID
LEFT JOIN LINEITEMXRETURN
    ON RETURNORDER.ID = LINEITEMXRETURN.RETURNORDER_ID
LEFT JOIN LINEITEM
ON (LINEITEM.ID = LINEITEMXSHIPMENT.LINEITEM_ID OR LINEITEM.ID = LINEITEMXRETURN.LINEITEM_ID)
LEFT JOIN ORDER_LOG ccTransactionLog ON (ccTransactionLog.ID IN (SELECT ORDER_LOG_ID FROM ORDER_LOG_LINEITEMXSHIPMENT WHERE ORDER_LOG_LINEITEMXSHIPMENT.LINEITEMXSHIPMENTS_ID = LINEITEMXSHIPMENT.ID) OR ccTransactionLog.creditcard_returnorder_id = RETURNORDER.ID) AND ccTransactionLog.transactionSuccess = 1
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE stateTax
    INNER JOIN TAXTYPE stateTaxType
    ON stateTax.TYPE_ID      = stateTaxType.ID
    AND stateTaxType.NAME    = 'State Tax')
ON (LINEITEMXSHIPMENT.ID = stateTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = stateTax.LINEITEMXRETURN_ID)
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE cityTax
    INNER JOIN TAXTYPE cityTaxType
    ON cityTax.TYPE_ID       = cityTaxType.ID
    AND cityTaxType.NAME     = 'City Tax')
ON (LINEITEMXSHIPMENT.ID = cityTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = cityTax.LINEITEMXRETURN_ID)
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE countyTax
    INNER JOIN TAXTYPE countyTaxType
    ON countyTax.TYPE_ID     = countyTaxType.ID
    AND countyTaxType.NAME   = 'County Tax')
ON (LINEITEMXSHIPMENT.ID = countyTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = countyTax.LINEITEMXRETURN_ID)
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE districtTax
    INNER JOIN TAXTYPE districtTaxType
    ON districtTax.TYPE_ID   = districtTaxType.ID
    AND districtTaxType.NAME = 'District Tax')
ON (LINEITEMXSHIPMENT.ID = districtTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = districtTax.LINEITEMXRETURN_ID)
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE countyLocalTax
    INNER JOIN TAXTYPE countyLocalTaxType
    ON countyLocalTax.TYPE_ID   = countyLocalTaxType.ID
    AND countyLocalTaxType.NAME = 'County Local Tax')
ON (LINEITEMXSHIPMENT.ID    = countyLocalTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = countyLocalTax.LINEITEMXRETURN_ID)
LEFT JOIN (LINEITEMXSHIPMENT_TAXTYPE cityLocalTax
    INNER JOIN TAXTYPE cityLocalTaxType
    ON cityLocalTax.TYPE_ID   = cityLocalTaxType.ID
    AND cityLocalTaxType.NAME = 'City Local Tax')
ON (LINEITEMXSHIPMENT.ID  = cityLocalTax.LINEITEMXSHIPMENT_ID OR LINEITEMXRETURN.ID = cityLocalTax.LINEITEMXRETURN_ID)
LEFT JOIN VENDOR
ON VENDOR.ID = LINEITEM.VENDOR_ID
WHERE ($X{IN, ACCOUNT.NAME, ROOT_ACCOUNT_NAME} OR $X{IN, ROOTACCOUNT.NAME, ROOT_ACCOUNT_NAME}) AND
COALESCE(INVOICE.CREATEDDATE, ccTransactionLog.timestamp, ccTransactionLog.updateTimestamp, RETURNORDER.reconciledDate)
    BETWEEN TO_DATE(COALESCE($P{START_DATE}, '0001-01-01'), 'YYYY-MM-DD') AND TO_DATE(COALESCE($P{END_DATE}, '9999-12-31'), 'YYYY-MM-DD') AND
LOWER(COALESCE(shipToAddress.STATE, ' ')) LIKE LOWER(COALESCE($P{STATE}, '%'))]]>
	</queryString>
	<field name="ShipTo State" class="java.lang.String"/>
	<field name="ShipTo County" class="java.lang.String"/>
	<field name="ShipTo City" class="java.lang.String"/>
	<field name="ShipTo ZIP" class="java.lang.String"/>
	<field name="ShipTo GeoCode" class="java.lang.String"/>
	<field name="Account Name" class="java.lang.String"/>
	<field name="APD PO" class="java.lang.String"/>
	<field name="Invoice Number" class="java.lang.String"/>
	<field name="Tracking Number" class="java.lang.String"/>
	<field name="Product Source" class="java.lang.String"/>
	<field name="Customer PO" class="java.lang.String"/>
	<field name="Item Number" class="java.math.BigDecimal"/>
	<field name="Total Order" class="java.math.BigDecimal"/>
	<field name="State Tax" class="java.math.BigDecimal"/>
	<field name="CountyTax" class="java.math.BigDecimal"/>
	<field name="City Tax" class="java.math.BigDecimal"/>
	<field name="District Tax" class="java.math.BigDecimal"/>
	<field name="County Local Tax" class="java.math.BigDecimal"/>
	<field name="City Local Tax" class="java.math.BigDecimal"/>
	<field name="Total Tax" class="java.math.BigDecimal"/>
	<field name="Total Freight" class="java.math.BigDecimal"/>
	<field name="Total Exempt Sales" class="java.math.BigDecimal"/>
	<field name="Total Sales" class="java.math.BigDecimal"/>
	<field name="ShipTo Address1" class="java.lang.String"/>
	<field name="ShipTo Address2" class="java.lang.String"/>
	<field name="BillTo Address1" class="java.lang.String"/>
	<field name="BillTo Address2" class="java.lang.String"/>
	<field name="BillTo State" class="java.lang.String"/>
	<field name="BillTo City" class="java.lang.String"/>
	<field name="BillTo ZIP" class="java.lang.String"/>
	<field name="Order Date" class="oracle.sql.TIMESTAMP"/>
	<field name="Ship Date" class="oracle.sql.TIMESTAMP"/>
	<field name="Charge/Invoice Date" class="oracle.sql.TIMESTAMP"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<columnHeader>
		<band height="23" splitType="Stretch">
			<staticText>
				<reportElement x="0" y="0" width="100" height="20" uuid="8a369b99-e16e-4268-b280-7d529d5aacec"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo State]]></text>
			</staticText>
			<staticText>
				<reportElement x="100" y="0" width="100" height="20" uuid="0a265d6b-2b4e-4932-9221-59fd8a9713e8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo County]]></text>
			</staticText>
			<staticText>
				<reportElement x="200" y="0" width="100" height="20" uuid="6096e7ab-56ba-487a-8874-870bd1eefdf8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo City]]></text>
			</staticText>
			<staticText>
				<reportElement x="300" y="0" width="100" height="20" uuid="215ad5de-d336-4143-9100-bcc34e6b8168"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo ZIP]]></text>
			</staticText>
			<staticText>
				<reportElement x="400" y="0" width="100" height="20" uuid="c118f286-0ae4-4397-9dbc-9d04151964ab"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo GeoCode]]></text>
			</staticText>
			<staticText>
				<reportElement x="500" y="0" width="100" height="20" uuid="9bd2d483-7ba0-4ebf-996c-bdc627fbb552"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Account Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="600" y="0" width="100" height="20" uuid="746d0a7b-f2eb-44d3-a93e-cae2d4c7c8e0"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[APD PO]]></text>
			</staticText>
			<staticText>
				<reportElement x="700" y="0" width="100" height="20" uuid="746d0a7b-f2eb-44d3-a93e-cae2d4c7c8e0"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Invoice Number]]></text>
			</staticText>
			<staticText>
				<reportElement x="800" y="0" width="100" height="20" uuid="e965bc27-9061-47f4-9499-0ea6d3aa5576"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Tracking Number]]></text>
			</staticText>
			<staticText>
				<reportElement x="900" y="0" width="100" height="20" uuid="f96c6b25-0a9c-4bb0-8a93-bc529c6ebc69"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Product Source]]></text>
			</staticText>
			<staticText>
				<reportElement x="1000" y="0" width="100" height="20" uuid="0b0a726f-985d-46ab-8cd5-a9db5d6d4493"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Customer PO]]></text>
			</staticText>
			<staticText>
				<reportElement x="1100" y="0" width="100" height="20" uuid="bfd9e07c-6ed9-42e6-8f01-c954200ee2ba"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Item Number]]></text>
			</staticText>
			<staticText>
				<reportElement x="1200" y="0" width="100" height="20" uuid="d517b589-8031-4d25-ad9b-9a6431c10061"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Total Merchandise]]></text>
			</staticText>
			<staticText>
				<reportElement x="1300" y="0" width="100" height="20" uuid="53ddeddc-c2a2-46d3-a344-99c3b22fabab"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[State Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1400" y="0" width="100" height="20" uuid="5fbf3b71-0bcc-4a10-a2dc-f4c64b467aae"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[County Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1500" y="0" width="100" height="20" uuid="00d1f9bd-eb2c-4a3b-b2be-1bc1f202eac2"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[City Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1600" y="0" width="100" height="20" uuid="b7d74771-4927-481c-af4b-6ad9d2f5d843"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[District Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1700" y="0" width="100" height="20" uuid="d0ddfa07-b6ea-4ddc-a18e-e5b19492d39d"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[County Local Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1800" y="0" width="100" height="20" uuid="9b17107c-1422-4bc6-8641-3d2e9c7a98d5"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[City Local Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="1900" y="0" width="100" height="20" uuid="e9fe6726-26f8-42fa-8bb4-f48ea5cbf41c"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Total Tax]]></text>
			</staticText>
			<staticText>
				<reportElement x="2000" y="0" width="100" height="20" uuid="35de9ff1-fc38-42f5-9a62-8f29b279b43c"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Total Freight]]></text>
			</staticText>
			<staticText>
				<reportElement x="2100" y="0" width="100" height="20" uuid="75484fc9-0bd6-44d2-b622-cec396fa060e"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Total Exempt Sales]]></text>
			</staticText>
			<staticText>
				<reportElement x="2200" y="0" width="100" height="20" uuid="dbf6e452-bcc9-41c5-9760-0d8173c87b97"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Total Sales]]></text>
			</staticText>
			<staticText>
				<reportElement x="2300" y="0" width="100" height="20" uuid="a236c001-396b-40e5-a694-640d63bbe725"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo Address1]]></text>
			</staticText>
			<staticText>
				<reportElement x="2400" y="0" width="100" height="20" uuid="965e4466-65c8-4006-877e-51c886a1d7f8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[ShipTo Address2]]></text>
			</staticText>
			<staticText>
				<reportElement x="2500" y="0" width="100" height="20" uuid="6103522d-cf9c-4018-b0f7-f4ed6c920742"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[BillTo Address1]]></text>
			</staticText>
			<staticText>
				<reportElement x="2600" y="0" width="100" height="20" uuid="86679947-dd16-42a3-b660-8bfd4544409f"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[BillTo Address2]]></text>
			</staticText>
			<staticText>
				<reportElement x="2700" y="0" width="100" height="20" uuid="8ee878e3-fa1e-4f87-8a7e-daf1f262f2a5"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[BillTo State]]></text>
			</staticText>
			<staticText>
				<reportElement x="2800" y="0" width="100" height="20" uuid="fb2e5681-3af6-4bc7-be48-fe6e4977f866"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[BillTo City]]></text>
			</staticText>
			<staticText>
				<reportElement x="2900" y="0" width="100" height="20" uuid="f8aaf088-e00c-45c0-a43c-1e59712c52c7"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[BillTo ZIP]]></text>
			</staticText>
			<staticText>
				<reportElement x="3000" y="0" width="100" height="20" uuid="e95e011e-a653-4716-9e29-59ed92418e7c"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Order Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="3100" y="0" width="100" height="20" uuid="3aa498ef-2c30-44da-8f8b-445a7175820e"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Ship Date]]></text>
			</staticText>
			<staticText>
				<reportElement x="3200" y="0" width="100" height="20" uuid="33cfe6c7-0e0e-4ad8-9c97-e7dd33ea4b3e"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<text><![CDATA[Charge/Invoice Date]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="22" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="0" y="0" width="100" height="20" uuid="30e316e5-89cf-49ec-b309-35d75b7f7bed"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo State}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="100" y="0" width="100" height="20" uuid="dbb6cf2e-84d3-4c35-8640-5303efafd1a8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo County}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="200" y="0" width="100" height="20" uuid="599bf366-8424-4524-b11b-76e19b5d062f"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo City}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="300" y="0" width="100" height="20" uuid="7b5fdb62-b0fe-4c92-b3ef-cb1407bb810c"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo ZIP}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="400" y="0" width="100" height="20" uuid="41fd3ca8-8d4a-452c-a71a-36b38282e799"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo GeoCode}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="500" y="0" width="100" height="20" uuid="f4856f8f-eb05-457a-974c-394259b2dd63"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Account Name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="600" y="0" width="100" height="20" uuid="cd441e98-f645-4800-b133-bb15845fea70"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{APD PO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="700" y="0" width="100" height="20" uuid="cd441e98-f645-4800-b133-bb15845fea70"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Invoice Number}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="800" y="0" width="100" height="20" uuid="3891026d-70aa-4a3f-b128-e82cfe00c9b7"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Tracking Number}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="900" y="0" width="100" height="20" uuid="9b805260-1dc9-47ae-bbdb-4144811212c8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Product Source}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1000" y="0" width="100" height="20" uuid="a38e8c89-ea7c-4482-9f01-d11d3e69eaac"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Customer PO}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1100" y="0" width="100" height="20" uuid="593cbad0-7e32-41f1-9a22-9cfe78da2799"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Item Number}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1200" y="0" width="100" height="20" uuid="032da8df-7803-4e60-b438-5180bb2597ad"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total Order}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1300" y="0" width="100" height="20" uuid="4b0823ee-5a7a-498f-9a3c-d721584c7f88"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{State Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1400" y="0" width="100" height="20" uuid="b2be21db-b1d5-4d1d-b555-9f70eecb53d8"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{CountyTax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1500" y="0" width="100" height="20" uuid="74d513c2-4613-4228-ae21-aef80963611c"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{City Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1600" y="0" width="100" height="20" uuid="94dcb794-d40c-4d53-9036-12028948b9d5"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{District Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1700" y="0" width="100" height="20" uuid="989ebae1-9bb8-476c-ab92-3d1226ff3c60"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{County Local Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1800" y="0" width="100" height="20" uuid="1d6ae237-9bd9-4ebf-8c8e-f558619334e0"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{City Local Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="1900" y="0" width="100" height="20" uuid="6831e8d9-792b-4dc5-8d53-c215880c3139"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total Tax}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2000" y="0" width="100" height="20" uuid="cb10aa08-e3d3-4606-bc8d-da00a822583a"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total Freight}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2100" y="0" width="100" height="20" uuid="fcb6764b-5628-4af0-827c-08603a31cb65"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total Exempt Sales}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2200" y="0" width="100" height="20" uuid="de931666-fe79-417c-ad70-9e18a67e0228"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Total Sales}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2300" y="0" width="100" height="20" uuid="83b38912-6925-4cfc-87ad-1d374c5e6958"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo Address1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2400" y="0" width="100" height="20" uuid="2b937eb7-ef22-425f-a54f-72ee7576b627"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ShipTo Address2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2500" y="0" width="100" height="20" uuid="8906af81-0da5-4d19-8e8d-4a5feb15a25e"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BillTo Address1}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2600" y="0" width="100" height="20" uuid="c554bb1c-f5b2-45c7-9a81-c6b752fc4c34"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BillTo Address2}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2700" y="0" width="100" height="20" uuid="040a342a-262e-4bb0-b5f7-313146caf95a"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BillTo State}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2800" y="0" width="100" height="20" uuid="7cf4eca3-7fc9-4cab-9757-e146a9ddfdfb"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BillTo City}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="2900" y="0" width="100" height="20" uuid="12b4110f-a872-4f8b-98bf-1f7375588093"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{BillTo ZIP}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="3000" y="0" width="100" height="20" uuid="4f0a7087-5052-4971-9095-474c71b660b1"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Order Date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="3100" y="0" width="100" height="20" uuid="d299557d-2e05-42bf-a92a-7b364c1c1864"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Ship Date}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="3200" y="0" width="100" height="20" uuid="3e2d2800-a4e9-4314-b023-cf5c91176899"/>
				<textElement>
					<font size="5"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Charge/Invoice Date}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
</jasperReport>
