package com.apd.phoenix.service.executor.jms.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import com.apd.phoenix.service.executor.api.ExecutorRequestAdminService;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@TransactionAttribute(value = TransactionAttributeType.REQUIRED)
public class ExecutorRequestAdminServiceImpl implements ExecutorRequestAdminService {

    @Override
    public int clearAllRequests() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public int clearAllErrors() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

}
