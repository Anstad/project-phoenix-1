package com.apd.phoenix.service.executor.command.api;

public interface SignalCommandConstants {

    public static final String VOUCHED_SIGNAL_REF = "invoiced";
    public static final String ORDER_ACK_SIGNAL_REF = "orderAck";
    public static final String SHIPPED_SIGNAL_REF = "shipped";
    public static final String VENDOR_COMMUNICATION_FAILED_SIGNAL_REF = "vendorComFailed";
    public static final String SIGNAL_TEST_SIGNAL_REF = "testSignal";
    public static final String INLINE_SIGNAL_TEST_SIGNAL_REF = "sampleEvent";
    public static final String SHIPTO_AUTHORIZED_REF = "shiptoauthorized";
    public static final String CANCEL_ORDER_REF = "ordercanceled";
    public static final String ORDER_REFUSED_REF = "orderrefused";
    public static final String ORDER_COMPLETED_REF = "ordercompleted";
    public static final String ORDER_UPDATE_REF = "orderupdate";
}
