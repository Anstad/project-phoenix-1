package com.apd.phoenix.service.executor.command.api;

public interface StartProcessCommandConstants {

    public static final String STANDARD_ORDER_PROCCESS_ID = "phoenix.standard-order-simple";
    public static final String SIGNAL_TEST_PROCESS_ID = "phoenix.signalTest";
    public static final String INLINE_SIGNAL_TEST = "phoenix.example-inline-event";
    public static final String VENDOR_SHIPMENT_MANIFEST_RETRIEVAL = "phoenix.vendor-manifest-retrieval";
    public static final String ORDER_REJECTED_SHIPTO_PROCESS_ID = "phoenix.order-rejected-shipto";
    public static final String USER_CREATION_PROCESS_ID = "phoenix.user-creation";
    public static final String ITEM_REQUEST_PROCESS_ID = "phoenix.itemRequest";
    public static final String USER_MODIFICATION_PROCESS_ID = "phoenix.user-modification-request";
    public static final String RETURN_ORDER_PROCESS_ID = "phoenix.processReturn";
    public static final String CREDIT_INVOICE_PROCESS_ID = "phoenix.processCreditInvoice";
    public static final String OLD_CART_ID = "phoenix.oldCart";
    public static final String ORDER_STUCK_ID = "phoenix.OrderStuck";
}
