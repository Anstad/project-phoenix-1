/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.executor.api;

public interface ExecutorRequestAdminService {

    public int clearAllRequests();

    public int clearAllErrors();
}
