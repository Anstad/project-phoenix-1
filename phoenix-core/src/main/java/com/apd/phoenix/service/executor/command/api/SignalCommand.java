package com.apd.phoenix.service.executor.command.api;

public final class SignalCommand extends Command {

    public enum Name {
        ORDER_ACK(SignalCommandConstants.ORDER_ACK_SIGNAL_REF), SHIPPED(SignalCommandConstants.SHIPPED_SIGNAL_REF), VOUCHED(
                SignalCommandConstants.VOUCHED_SIGNAL_REF), VENDOR_COMMUNICATION_FAILED(
                SignalCommandConstants.VENDOR_COMMUNICATION_FAILED_SIGNAL_REF), SIGNAL_TEST(
                SignalCommandConstants.SIGNAL_TEST_SIGNAL_REF), INLINE_SIGNAL_TEST(
                SignalCommandConstants.INLINE_SIGNAL_TEST_SIGNAL_REF), SHIPTO_AUTHORIZED(
                SignalCommandConstants.SHIPTO_AUTHORIZED_REF), CANCEL_ORDER(SignalCommandConstants.CANCEL_ORDER_REF), ORDER_REFUSED(
                SignalCommandConstants.ORDER_REFUSED_REF), ORDER_COMPLETED(SignalCommandConstants.ORDER_COMPLETED_REF), ORDER_UPDATE(
                SignalCommandConstants.ORDER_UPDATE_REF);

        private String name;

        private Name(String type) {
            this.name = type;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
