package com.apd.phoenix.service.executor.jms.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.ExecutorServiceEntryPoint;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.Executor;
import com.apd.phoenix.service.executor.api.ExecutorRequestAdminService;

@Stateless
public class ExecutorServiceEntryPointImpl implements ExecutorServiceEntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorServiceEntryPointImpl.class);

    @Inject
    private Executor executor;

    @Inject
    private ExecutorRequestAdminService adminService;

    public Executor getExecutor() {
        return executor;
    }

    public void setExecutor(Executor executor) {
        this.executor = executor;
    }

    public ExecutorRequestAdminService getAdminService() {
        return adminService;
    }

    public void setAdminService(ExecutorRequestAdminService adminService) {
        this.adminService = adminService;
    }

    @Override
    public int clearAllRequests() {
        return adminService.clearAllRequests();
    }

    @Override
    public int clearAllErrors() {
        return adminService.clearAllErrors();
    }

    @Override
    public Long scheduleRequest(String commandName, CommandContext ctx) {
        return executor.scheduleRequest(commandName, ctx);
    }

    @Override
    public void cancelRequest(Long requestId) {
        executor.cancelRequest(requestId);
    }

    @Override
    public void init() {
        LOGGER.info("**Initializing JMS executor Service**");
        executor.init();
    }

    @Override
    public void destroy() {
        executor.destroy();
    }

    @Override
    public int getInterval() {
        return executor.getInterval();
    }

    @Override
    public void setInterval(int waitTime) {
        executor.setInterval(waitTime);
    }

    @Override
    public int getRetries() {
        return executor.getRetries();
    }

    @Override
    public void setRetries(int defaultNroOfRetries) {
        executor.setRetries(defaultNroOfRetries);
    }

    @Override
    public int getThreadPoolSize() {
        return executor.getThreadPoolSize();
    }

    @Override
    public void setThreadPoolSize(int nroOfThreads) {
        executor.setThreadPoolSize(nroOfThreads);
    }

}
