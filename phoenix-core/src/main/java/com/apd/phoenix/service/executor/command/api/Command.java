package com.apd.phoenix.service.executor.command.api;

public abstract class Command {

    public enum Type {
        ORDER(CommandConstants.ORDER_TYPE), ACCOUNT(CommandConstants.ACCOUNT_TYPE), USER_REQUEST(
                CommandConstants.USER_REQUEST_TYPE);

        private String type;

        private Type(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return type;
        }
    }

    public enum Callback {
        HELLOWORLD(CommandConstants.HELLOWORLD_CALLBACK);

        private String callback;

        private Callback(String callback) {
            this.callback = callback;
        }

        @Override
        public String toString() {
            return callback;
        }
    }
}
