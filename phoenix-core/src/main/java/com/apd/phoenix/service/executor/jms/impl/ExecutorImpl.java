package com.apd.phoenix.service.executor.jms.impl;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.Executor;

@Stateless
public class ExecutorImpl implements Executor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorImpl.class);

    @Resource(mappedName = "java:/activemq/ConnectionFactory")
    private ConnectionFactory connectionFactory;

    @Resource(mappedName = "java:/activemq/workflow-command")
    private Queue queue;

    private int retries = 3;

    @Override
    public void init() {
    }

    @Override
    public void destroy() {
    }

    @Override
    public Long scheduleRequest(String commandName, CommandContext ctx) {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            Session msession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer messageProducer = msession.createProducer(queue);
            LOGGER.info("Scheduling resquest");
            ObjectMessage message = msession.createObjectMessage(ctx);
            message.setStringProperty("commandName", commandName);
            if (ctx != null && ctx.getData("id") != null) {
                message.setStringProperty("JMSXGroupID", ctx.getData("id").toString());
            }
            message.setIntProperty("retries", retries);

            messageProducer.send(message);
            LOGGER.info("Message ID: " + message.getJMSMessageID());
            connection.close();
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            }
            catch (Exception e) {
                LOGGER.error("Could not close connection", e);
            }
        }
        return null;
    }

    @Override
    public void cancelRequest(Long requestId) {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public int getInterval() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public void setInterval(int waitTime) {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public int getRetries() {
        return retries;
    }

    @Override
    public void setRetries(int defaultNroOfRetries) {
        retries = defaultNroOfRetries;
    }

    @Override
    public int getThreadPoolSize() {
        throw new UnsupportedOperationException("This features is not implemented");
    }

    @Override
    public void setThreadPoolSize(int nroOfThreads) {
        throw new UnsupportedOperationException("This features is not implemented");
    }

}
