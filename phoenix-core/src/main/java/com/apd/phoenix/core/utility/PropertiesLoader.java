package com.apd.phoenix.core.utility;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class PropertiesLoader {

    private static final Logger LOG = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties getAsProperties(String name) {
        Properties props = new Properties();
        String propertyHome = System.getProperty("phoenix.config.home");
        LOG.debug("phoenix config home: {}", propertyHome);
        String propsFile = propertyHome + name;
        if (!propsFile.endsWith(".properties")) {
            propsFile += ".properties";
        }
        try (
        		FileInputStream fis = new FileInputStream(propsFile);
        	) {
            props.load(fis);
        }
        catch (Exception e) {
            LOG.error("Not found! {}", name);
        }

        return props;
    }

    public static Properties getAsProperties(Class classLoaderReference, String name) {
        Properties props = new Properties();
        if (!name.endsWith(".properties")) {
            name += ".properties";
        }
        try (
        		InputStream is = classLoaderReference.getClassLoader().getResourceAsStream(name);
        	) {
            props.load(is);
        }
        catch (Exception e) {
            LOG.error("Not found! {},{}", classLoaderReference.getName(), name);
        }
        return props;
    }
}
