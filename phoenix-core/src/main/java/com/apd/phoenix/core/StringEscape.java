package com.apd.phoenix.core;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringEscape {

    private static final Logger LOGGER = LoggerFactory.getLogger(StringEscape.class);

    public static String escapeForUrl(String value) {

        try {
            value = URLEncoder.encode(value, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            LOGGER.warn("Unable to encode value for URL", e);
        }

        return value;
    }

    /**
     * Takes in a value on an order, and removes the prepended Cashout page label, if any.
     * 
     * TODO: use an encoding (such as CSV) or add validation to shopping. As is, "|" is a special character. If 
     * that change is made, this method will have to change, as will CashoutPageContainer, OrderAcknowledgementEmailTemplate, 
     * CustomerInvoiceEmailTemplate, reports, and the EDI translators.
     * 
     * @param value
     * @return
     */
    public static String removeMappingLabel(String value) {

        if (StringUtils.isBlank(value)) {
            return "";
        }
        int startIndex = value.indexOf("|") + 1;

        return value.substring(startIndex);
    }

    public static String getMappingLabel(String value, String defaultLabel) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        if (value.contains("|")) {
            return value.split("\\|")[0];
        }
        return defaultLabel;
    }
}
