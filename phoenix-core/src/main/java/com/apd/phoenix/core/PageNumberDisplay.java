package com.apd.phoenix.core;

import java.util.ArrayList;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 * This class stores the information necessary for the page navigation bar at the bottom of the browse section. 
 * The page navigation is divided into three sections, with an ellipse between each section. If there are 50 pages 
 * of results, and the user is on page 15, the pages would be rendered as:
 * <pre>
 * 1 2 ... 13 14 15 16 17 ... 49 50 </pre>
 * <p>
 * The the different sets of numbers, whether to render the ellipses, and the current page are stored in this 
 * object.
 * </p>
 * <p>
 * THE ONLY METHODS THAT SHOULD BE CALLED BY BACKING BEANS ARE THE CONSTRUCTOR, getPage(), getSize(), AND 
 * resetPagination(). ALL OTHERS ARE MEANT FOR USE SOLELY BY THE JSF PAGES.
 * </p>
 *
 * @author RHC
 *
 */
public class PageNumberDisplay {

    private static final int LEADING_MAX_QUANTITY = 2;
    private static final int QUANTITY_BEFORE_CURRENT_PAGE = 2;
    private static final int QUANTITY_AFTER_CURRENT_PAGE = 2;
    private static final int TRAILING_MAX_QUANTITY = 2;
    private int page;
    private PageSize pageSize = PageSize.FIVE;
    private int resultQuantity;

    /**
     * Returns the current page number.
     *
     * @return An int whose value is the current page number.
     */
    public int getPage() {
        return page;
    }

    /**
     * Returns the current page number.
     *
     * @return An int whose value is the current page number.
     */
    public int getSize() {
        return this.pageSize.toInt();
    }

    /**
     * Called when the pagination has to be reset to the first page.
     */
    public void resetPagination() {
        this.setPage(1);
    }

    /**
     * Constructor for this class. This constructor is called whenever the page size changes, or the number of 
     * results changes. When this happens, the current page is set to the first page, and the values are 
     * recalculated.
     *
     * @param pageSize - the number of items on each page
     * @param resultQuantity - the total number of items at the current level of the hierarchy
     */
    public PageNumberDisplay(int resultQuantity) {
        this.resultQuantity = resultQuantity;
        this.resetPagination();
    }

    /**
     * This enum is used to store the potential sizes of the product browse pages. 
     *
     * THIS ENUM SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @author RHC
     *
     */
    public enum PageSize {

        FOUR("4"), FIVE("5"), TEN("10"), TWENTY_FIVE("25"), FIFTY("50"), SEVENTY_FIVE("75"), HUNDRED("100");

        private final String label;

        private PageSize(String label) {
            this.label = label;
        }

        /**
         * Returns a human-readable label. This will be shown in the dropdown on the browse page.
         *
         * @param label - label for the page size in the dropdown
         */
        public String getLabel() {
            return this.label;
        }

        /**
         * Used in this bean to calculate the integer value of the PageSize.
         *
         * @return - the integer page size
         */
        public int toInt() {
            return Integer.parseInt(this.label);
        }
    }

    /**
     * Returns an array of SelectItems, for the dropdown.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return
     */
    public SelectItem[] getSizes() {
        SelectItem[] items = new SelectItem[PageSize.values().length];
        int i = 0;
        for (PageSize p : PageSize.values()) {
            items[i++] = new SelectItem(p, p.getLabel());
        }
        return items;
    }

    /**
     * Returns the PageSize value which is the current number of items on the page.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return The current size of the page.
     */
    public PageSize getPageSize() {
        return pageSize;
    }

    /**
     * Used to set the current size of the page.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @param size - the new size of the page
     */
    public void setPageSize(PageSize size) {
        if (!this.pageSize.equals(size)) {
            this.pageSize = size;
            this.resetPagination();
        }
    }

    /**
     * Returns the leading set of numbers to be rendered.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return A List of Integers that are the numbers to be rendered.
     */
    public List<Integer> getLeadingSetOfNumbers() {
        List<Integer> toReturn = new ArrayList<>();

        if (this.getRenderFirstEllipse()) {
            for (int i = 1; i <= LEADING_MAX_QUANTITY; i++) {
                toReturn.add(i);
            }
        }

        return toReturn;
    }

    /**
     * Returns the middle set of numbers to be rendered.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return A List of Integers that are the numbers to be rendered.
     */
    public List<Integer> getMiddleSetOfNumbers() {
        List<Integer> toReturn = new ArrayList<>();

        if (!this.getRenderFirstEllipse()) {
            for (int i = 1; i < this.getPage(); i++) {
                toReturn.add(i);
            }
        }
        else {
            for (int i = this.getPage() - QUANTITY_BEFORE_CURRENT_PAGE; i < this.getPage(); i++) {
                toReturn.add(i);
            }
        }

        toReturn.add(this.getPage());

        if (!this.getRenderSecondEllipse()) {
            for (int i = this.getPage() + 1; i <= this.pageQuantity(); i++) {
                toReturn.add(i);
            }
        }
        else {
            for (int i = this.getPage() + 1; i <= this.getPage() + QUANTITY_AFTER_CURRENT_PAGE; i++) {
                toReturn.add(i);
            }
        }

        return toReturn;
    }

    /**
     * Returns the last set of numbers to be rendered.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return A List of Integers that are the numbers to be rendered.
     */
    public List<Integer> getTrailingSetOfNumbers() {
        List<Integer> toReturn = new ArrayList<>();

        if (this.getRenderSecondEllipse()) {
            for (int i = this.pageQuantity() - TRAILING_MAX_QUANTITY + 1; i <= this.pageQuantity(); i++) {
                toReturn.add(i);
            }
        }
        return toReturn;
    }

    /**
     * Returns whether the first ellipse should be rendered.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return Boolean indicating whether the ellipse should be rendered.
     */
    public boolean getRenderFirstEllipse() {
        return this.getPage() - 1 >= 1 + LEADING_MAX_QUANTITY + QUANTITY_BEFORE_CURRENT_PAGE;
    }

    /**
     * Returns whether the second ellipse should be rendered.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return Boolean indicating whether the ellipse should be rendered.
     */
    public boolean getRenderSecondEllipse() {
        return this.pageQuantity() - this.getPage() >= 1 + QUANTITY_AFTER_CURRENT_PAGE + TRAILING_MAX_QUANTITY;
    }

    /**
     * Used to store a new page number. When called, it recalculates all of the other values.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @param page - the number of the new page
     */
    public void setPage(Integer page) {
        if (page > 0 && page <= this.pageQuantity()) {
            this.page = page;
        }
    }

    /**
     * Returns true if the current page is the first page of results, otherwise false.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return
     */
    public boolean isFirst() {
        return this.page == 1;
    }

    /**
     * Returns true if the current page is the last page of results, otherwise false.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return
     */
    public boolean isLast() {
        return this.page == this.pageQuantity();
    }

    /**
     * Returns true if the pagination should be displayed, otherwise false.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return
     */
    public boolean getShouldShowPagination() {
        //Returns whether the number of pages is one.
        return this.pageQuantity() != 1;
    }

    /**
     * Returns the number of pages.
     *
     * THIS METHOD SHOULD NOT BE USED IN BACKING BEANS, ONLY IN THE COMPOSITE COMPONENTS.
     *
     * @return
     */
    private int pageQuantity() {
        return (this.resultQuantity - 1) / this.pageSize.toInt() + 1;
    }

    public int getResultQuantity() {
        return resultQuantity;
    }

    public void setResultQuantity(int resultQuantity) {
        this.resultQuantity = resultQuantity;
        if (page > this.pageQuantity() && page > 1) {
            this.setPage(this.pageQuantity());
        }
    }

    public List<?> currentPageElements(List<?> allElements) {
        if (allElements == null) {
            return allElements;
        }
        List<?> toReturn = allElements.subList(this.getSize() * (this.getPage() - 1), Math.min(allElements.size(), this
                .getSize()
                * this.getPage()));
        if (toReturn != null && toReturn.isEmpty() && this.getPage() > 1) {
            this.setPage(this.getPage() - 1);
            return this.currentPageElements(allElements);
        }
        return toReturn;
    }

}