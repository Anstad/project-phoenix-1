//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.29 at 08:58:07 PM EDT 
//


package com.ussco.oagis._0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.openapplications.oagis._9.AmountType;
import org.openapplications.oagis._9.NameType;


/**
 * <p>Java class for ItemListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CountryCode" type="{http://www.openapplications.org/oagis/9}CountryCodeContentType" minOccurs="0"/>
 *         &lt;element name="ListStartDate" type="{http://www.openapplications.org/oagis/9}DateTimeType" minOccurs="0"/>
 *         &lt;element name="ListEndDate" type="{http://www.openapplications.org/oagis/9}DateTimeType" minOccurs="0"/>
 *         &lt;element name="FacilityNumber" type="{http://www.openapplications.org/oagis/9}NameType" minOccurs="0"/>
 *         &lt;element name="VendorNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VendorShortName" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ListAmount" type="{http://www.openapplications.org/oagis/9}AmountType" minOccurs="0"/>
 *         &lt;element name="ListUnitCode" type="{http://www.openapplications.org/oagis/9}UnitCodeContentType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemListType", propOrder = {
    "countryCode",
    "listStartDate",
    "listEndDate",
    "facilityNumber",
    "vendorNumber",
    "vendorShortName",
    "listAmount",
    "listUnitCode"
})
public class ItemListType {

    @XmlElement(name = "CountryCode")
    protected String countryCode;
    @XmlElement(name = "ListStartDate")
    protected String listStartDate;
    @XmlElement(name = "ListEndDate")
    protected String listEndDate;
    @XmlElement(name = "FacilityNumber")
    protected NameType facilityNumber;
    @XmlElement(name = "VendorNumber")
    protected String vendorNumber;
    @XmlElement(name = "VendorShortName")
    protected String vendorShortName;
    @XmlElement(name = "ListAmount")
    protected AmountType listAmount;
    @XmlElement(name = "ListUnitCode")
    protected String listUnitCode;

    /**
     * Gets the value of the countryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the listStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListStartDate() {
        return listStartDate;
    }

    /**
     * Sets the value of the listStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListStartDate(String value) {
        this.listStartDate = value;
    }

    /**
     * Gets the value of the listEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListEndDate() {
        return listEndDate;
    }

    /**
     * Sets the value of the listEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListEndDate(String value) {
        this.listEndDate = value;
    }

    /**
     * Gets the value of the facilityNumber property.
     * 
     * @return
     *     possible object is
     *     {@link NameType }
     *     
     */
    public NameType getFacilityNumber() {
        return facilityNumber;
    }

    /**
     * Sets the value of the facilityNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType }
     *     
     */
    public void setFacilityNumber(NameType value) {
        this.facilityNumber = value;
    }

    /**
     * Gets the value of the vendorNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorNumber() {
        return vendorNumber;
    }

    /**
     * Sets the value of the vendorNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorNumber(String value) {
        this.vendorNumber = value;
    }

    /**
     * Gets the value of the vendorShortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorShortName() {
        return vendorShortName;
    }

    /**
     * Sets the value of the vendorShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorShortName(String value) {
        this.vendorShortName = value;
    }

    /**
     * Gets the value of the listAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getListAmount() {
        return listAmount;
    }

    /**
     * Sets the value of the listAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setListAmount(AmountType value) {
        this.listAmount = value;
    }

    /**
     * Gets the value of the listUnitCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getListUnitCode() {
        return listUnitCode;
    }

    /**
     * Sets the value of the listUnitCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setListUnitCode(String value) {
        this.listUnitCode = value;
    }

}
