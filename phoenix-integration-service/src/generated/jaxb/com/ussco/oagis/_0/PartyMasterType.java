//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.09.29 at 08:58:07 PM EDT 
//


package com.ussco.oagis._0;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.openapplications.oagis._9.CodesType;
import org.openapplications.oagis._9.ContactType;
import org.openapplications.oagis._9.DescriptionType;
import org.openapplications.oagis._9.FinancialPartyType;
import org.openapplications.oagis._9.IdentifierType;
import org.openapplications.oagis._9.LocationType;
import org.openapplications.oagis._9.NameType;
import org.openapplications.oagis._9.NamedIDsType;
import org.openapplications.oagis._9.NoteType;
import org.openapplications.oagis._9.OpenAttachmentType;
import org.openapplications.oagis._9.PartyIDsType;
import org.openapplications.oagis._9.PartyType;
import org.openapplications.oagis._9.PaymentMethodCodeType;
import org.openapplications.oagis._9.UserAreaType;


/**
 * <p>Java class for PartyMasterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PartyMasterType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}PartyIDs" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}Name" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}Location" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}Contact" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}AccountIDs" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}PaymentTermID" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}PaymentMethodCode" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}TaxExemptCodes" minOccurs="0"/>
 *           &lt;group ref="{http://www.openapplications.org/oagis/9}FreeFormTextGroup" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}FinancialParty" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}ParentParty" minOccurs="0"/>
 *           &lt;element ref="{http://www.ussco.com/oagis/0}ChildParty" maxOccurs="unbounded" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}PartnerRoleCodes" minOccurs="0"/>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}UserArea" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element ref="{http://www.openapplications.org/oagis/9}Attachment" minOccurs="0"/>
 *           &lt;element name="VendorShortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *           &lt;element name="VendorAbbreviation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PartyMasterType", propOrder = {
    "partyIDs",
    "name",
    "location",
    "contact",
    "accountIDs",
    "paymentTermID",
    "paymentMethodCode",
    "taxExemptCodes",
    "description",
    "note",
    "financialParty",
    "parentParty",
    "childParty",
    "partnerRoleCodes",
    "userArea",
    "attachment",
    "vendorShortName",
    "vendorAbbreviation"
})
public class PartyMasterType {

    @XmlElement(name = "PartyIDs", namespace = "http://www.openapplications.org/oagis/9")
    protected PartyIDsType partyIDs;
    @XmlElement(name = "Name", namespace = "http://www.openapplications.org/oagis/9")
    protected List<NameType> name;
    @XmlElement(name = "Location", namespace = "http://www.openapplications.org/oagis/9")
    protected List<LocationType> location;
    @XmlElement(name = "Contact", namespace = "http://www.openapplications.org/oagis/9")
    protected List<ContactType> contact;
    @XmlElement(name = "AccountIDs", namespace = "http://www.openapplications.org/oagis/9")
    protected NamedIDsType accountIDs;
    @XmlElement(name = "PaymentTermID", namespace = "http://www.openapplications.org/oagis/9")
    protected IdentifierType paymentTermID;
    @XmlElement(name = "PaymentMethodCode", namespace = "http://www.openapplications.org/oagis/9")
    protected PaymentMethodCodeType paymentMethodCode;
    @XmlElement(name = "TaxExemptCodes", namespace = "http://www.openapplications.org/oagis/9")
    protected CodesType taxExemptCodes;
    @XmlElement(name = "Description", namespace = "http://www.openapplications.org/oagis/9")
    protected List<DescriptionType> description;
    @XmlElement(name = "Note", namespace = "http://www.openapplications.org/oagis/9")
    protected List<NoteType> note;
    @XmlElement(name = "FinancialParty", namespace = "http://www.openapplications.org/oagis/9")
    protected FinancialPartyType financialParty;
    @XmlElement(name = "ParentParty", namespace = "http://www.openapplications.org/oagis/9")
    protected PartyType parentParty;
    @XmlElement(name = "ChildParty")
    protected List<ChildPartyType> childParty;
    @XmlElement(name = "PartnerRoleCodes", namespace = "http://www.openapplications.org/oagis/9")
    protected CodesType partnerRoleCodes;
    @XmlElement(name = "UserArea", namespace = "http://www.openapplications.org/oagis/9")
    protected UserAreaType userArea;
    @XmlElement(name = "Attachment", namespace = "http://www.openapplications.org/oagis/9")
    protected OpenAttachmentType attachment;
    @XmlElement(name = "VendorShortName")
    protected String vendorShortName;
    @XmlElement(name = "VendorAbbreviation")
    protected String vendorAbbreviation;

    /**
     * Gets the value of the partyIDs property.
     * 
     * @return
     *     possible object is
     *     {@link PartyIDsType }
     *     
     */
    public PartyIDsType getPartyIDs() {
        return partyIDs;
    }

    /**
     * Sets the value of the partyIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyIDsType }
     *     
     */
    public void setPartyIDs(PartyIDsType value) {
        this.partyIDs = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the name property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getName().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NameType }
     * 
     * 
     */
    public List<NameType> getName() {
        if (name == null) {
            name = new ArrayList<NameType>();
        }
        return this.name;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactType }
     * 
     * 
     */
    public List<ContactType> getContact() {
        if (contact == null) {
            contact = new ArrayList<ContactType>();
        }
        return this.contact;
    }

    /**
     * Gets the value of the accountIDs property.
     * 
     * @return
     *     possible object is
     *     {@link NamedIDsType }
     *     
     */
    public NamedIDsType getAccountIDs() {
        return accountIDs;
    }

    /**
     * Sets the value of the accountIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link NamedIDsType }
     *     
     */
    public void setAccountIDs(NamedIDsType value) {
        this.accountIDs = value;
    }

    /**
     * Gets the value of the paymentTermID property.
     * 
     * @return
     *     possible object is
     *     {@link IdentifierType }
     *     
     */
    public IdentifierType getPaymentTermID() {
        return paymentTermID;
    }

    /**
     * Sets the value of the paymentTermID property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentifierType }
     *     
     */
    public void setPaymentTermID(IdentifierType value) {
        this.paymentTermID = value;
    }

    /**
     * Gets the value of the paymentMethodCode property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentMethodCodeType }
     *     
     */
    public PaymentMethodCodeType getPaymentMethodCode() {
        return paymentMethodCode;
    }

    /**
     * Sets the value of the paymentMethodCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentMethodCodeType }
     *     
     */
    public void setPaymentMethodCode(PaymentMethodCodeType value) {
        this.paymentMethodCode = value;
    }

    /**
     * Gets the value of the taxExemptCodes property.
     * 
     * @return
     *     possible object is
     *     {@link CodesType }
     *     
     */
    public CodesType getTaxExemptCodes() {
        return taxExemptCodes;
    }

    /**
     * Sets the value of the taxExemptCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodesType }
     *     
     */
    public void setTaxExemptCodes(CodesType value) {
        this.taxExemptCodes = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the description property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DescriptionType }
     * 
     * 
     */
    public List<DescriptionType> getDescription() {
        if (description == null) {
            description = new ArrayList<DescriptionType>();
        }
        return this.description;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NoteType }
     * 
     * 
     */
    public List<NoteType> getNote() {
        if (note == null) {
            note = new ArrayList<NoteType>();
        }
        return this.note;
    }

    /**
     * Gets the value of the financialParty property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialPartyType }
     *     
     */
    public FinancialPartyType getFinancialParty() {
        return financialParty;
    }

    /**
     * Sets the value of the financialParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialPartyType }
     *     
     */
    public void setFinancialParty(FinancialPartyType value) {
        this.financialParty = value;
    }

    /**
     * Gets the value of the parentParty property.
     * 
     * @return
     *     possible object is
     *     {@link PartyType }
     *     
     */
    public PartyType getParentParty() {
        return parentParty;
    }

    /**
     * Sets the value of the parentParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartyType }
     *     
     */
    public void setParentParty(PartyType value) {
        this.parentParty = value;
    }

    /**
     * Gets the value of the childParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the childParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChildParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ChildPartyType }
     * 
     * 
     */
    public List<ChildPartyType> getChildParty() {
        if (childParty == null) {
            childParty = new ArrayList<ChildPartyType>();
        }
        return this.childParty;
    }

    /**
     * Gets the value of the partnerRoleCodes property.
     * 
     * @return
     *     possible object is
     *     {@link CodesType }
     *     
     */
    public CodesType getPartnerRoleCodes() {
        return partnerRoleCodes;
    }

    /**
     * Sets the value of the partnerRoleCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodesType }
     *     
     */
    public void setPartnerRoleCodes(CodesType value) {
        this.partnerRoleCodes = value;
    }

    /**
     * Gets the value of the userArea property.
     * 
     * @return
     *     possible object is
     *     {@link UserAreaType }
     *     
     */
    public UserAreaType getUserArea() {
        return userArea;
    }

    /**
     * Sets the value of the userArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserAreaType }
     *     
     */
    public void setUserArea(UserAreaType value) {
        this.userArea = value;
    }

    /**
     * Gets the value of the attachment property.
     * 
     * @return
     *     possible object is
     *     {@link OpenAttachmentType }
     *     
     */
    public OpenAttachmentType getAttachment() {
        return attachment;
    }

    /**
     * Sets the value of the attachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpenAttachmentType }
     *     
     */
    public void setAttachment(OpenAttachmentType value) {
        this.attachment = value;
    }

    /**
     * Gets the value of the vendorShortName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorShortName() {
        return vendorShortName;
    }

    /**
     * Sets the value of the vendorShortName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorShortName(String value) {
        this.vendorShortName = value;
    }

    /**
     * Gets the value of the vendorAbbreviation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorAbbreviation() {
        return vendorAbbreviation;
    }

    /**
     * Sets the value of the vendorAbbreviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorAbbreviation(String value) {
        this.vendorAbbreviation = value;
    }

}
