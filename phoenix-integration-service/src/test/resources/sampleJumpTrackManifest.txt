<?xml version="1.0" encoding="UTF-8"?>
<request type="jumptrack-api-xml" version="1.3">
	<remove> 
		<delivery>
			<id>Unique Delivery #</id>
		</delivery> 
	</remove>
	<manifest>
		<id>Unique Manifest #</id>
			<route>
				<id>Route1</id>
				<region-name>Region 1</region-name> 
				<distribution-name>Distribution 1</distribution-name> 
				<route-name>Route 1<route-name>
			</route>
			<stop sort-order="123">
				<stop-cd>Stop Code</stop-cd>
				<delivery-date>2012-11-30</delivery-date>
				<site>
					<account>account #</account>
					<name>Customer Name</name>
					<address1>Address 1</address1>
					<address2>Address 2</address2>
					<address3>Address 3</address3>
					<city>City</city>
					<state>State/Province</state>
					<zip>Zip/Postal</zip>
				</site>
				<delivery type="DELIVERY_TYPE_CODE">
					<id>Unique Delivery #</id>
					<display-cd>Display Code</display-cd>
					<delivery-po>P.O. #</delivery-po>
					<delivery-note>Note for the delivery.</delivery-note>
					<delivery-line qty-target="1">
						<name>Product Name</name>
						<product-no>Product No</product-no>
						<uom>Unit of M.</uom>
						<desc>Description</desc>
						<license-plate>100001</license-plate>
				</delivery-line>
			</delivery>
		</stop>
	</manifest>
</request>
					