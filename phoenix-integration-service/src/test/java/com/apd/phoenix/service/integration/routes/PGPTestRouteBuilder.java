package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.PGPDataFormat;

public class PGPTestRouteBuilder extends RouteBuilder {

    private String source;

    private String sink;

    private String privateKeyFileName;

    private String publicKeyFileName;

    private String keyUserid;

    private String password;

    private boolean isArmored;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi processing: ${exception.stacktrace}");

        PGPDataFormat pgpEncrypt = new PGPDataFormat();
        pgpEncrypt.setKeyFileName(publicKeyFileName);
        pgpEncrypt.setKeyUserid(keyUserid);
        pgpEncrypt.setArmored(isArmored);

        PGPDataFormat pgpDecrypt = new PGPDataFormat();
        pgpDecrypt.setKeyFileName(privateKeyFileName);
        pgpDecrypt.setKeyUserid(keyUserid);
        pgpDecrypt.setArmored(isArmored);
        pgpDecrypt.setPassword(password);

        from(source)
        //log unencrypted
                .log(LoggingLevel.INFO, "unencrypted: ${body}")
                //encrypt
                .marshal(pgpEncrypt)
                //log encrypted
                .log(LoggingLevel.INFO, "encrypted: ${body}")
                //decrypt
                .unmarshal(pgpDecrypt).to(sink);

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSink() {
        return sink;
    }

    public void setSink(String sink) {
        this.sink = sink;
    }

    public String getPrivateKeyFileName() {
        return privateKeyFileName;
    }

    public void setPrivateKeyFileName(String privateKeyFileName) {
        this.privateKeyFileName = privateKeyFileName;
    }

    public String getPublicKeyFileName() {
        return publicKeyFileName;
    }

    public void setPublicKeyFileName(String publicKeyFileName) {
        this.publicKeyFileName = publicKeyFileName;
    }

    public String getKeyUserid() {
        return keyUserid;
    }

    public void setKeyUserid(String keyUserid) {
        this.keyUserid = keyUserid;
    }

    public boolean isArmored() {
        return isArmored;
    }

    public void setArmored(boolean isArmored) {
        this.isArmored = isArmored;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
