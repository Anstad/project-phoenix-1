package com.apd.phoenix.service.integration.routes;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Assert;
import org.junit.Test;
import com.apd.phoenix.service.integration.usps.model.UspsCatalogItem;
import com.apd.phoenix.service.model.dto.CatalogDto;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.ItemImageDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import org.junit.Ignore;

/**
 * Tests for USPS EDI 832.
 * 
 * @author Bryan Sauders <bsaunder@redhat.com>
 * 
 */
public class UspsCatalogRouteBuilderTest extends CamelTestSupport {

    private static final String EXPECTED_SAMPLE_CSV = "943879320|832|01|1096758|Remanufacture Toner for HP LASERJETT 1320 / 3390 / 3392; OEM Part #(s): Q5949X, |1096758|TURTN1325|44103103||50.0|97.5|EA|U|U|U|N|N|2DOSEQ-10-B-0033||N|||https://mercury.rev.net/estore/cgi-bin/peekItem?fileid=showitem&product=1096758&credential=263&pp=i9rnvy9k|https://mercury.rev.net/estore/images/su|Turbon||A|SHIPS USPS PRIORITY MAIL-Remanufacture Toner for HP LASERJETT 1320 / 3390 / 3392; OEM Part s Q5949X, Black|C|19850601||1|999999999|1|U|N|U|Y|Y|U|U|U|U|U";
    public static final String EXPECTED_SAMPLE_FROM_DTO0 = "943879320|832|01|APD13371337|Gold Secratary 90000 v2|V13371337|M13371337|1234567||1.0|1.0|EA|U|U|U|N|N|2DOSEQ-10-B-0033||N||||http://www.blingmystapeler.com/gallerly/|Stapel Bling Indutries LLC||A|A 12 carrot gold stapeler. 13 x 3 inches and emblazoned with Sri Lankan rubies.|C|||1|999999999|1|N|U|Y|Y|U|U|U|U|U|U";
    public static final String EXPECTED_SAMPLE_FROM_DTO1 = "943879320|832|01|APD008|stapler|123121333333|123455555|111111||1.0|1.0|EA|U|Y|Y|Y|N|2DOSEQ-10-B-0033||N||||http://www.blingmystapeler.com/gallerly/|Stapel Group||A|A meh stapeler|C|||1|999999999|1|N|U|Y|Y|U|U|U|U|U|U";

    @Produce
    private ProducerTemplate producer;

    @EndpointInject(uri = "direct:uspsSalesCatalogDataSource")
    private Endpoint dataSource;

    @EndpointInject(uri = "direct:uspsSalesCatalogSink")
    private Endpoint catalogSink;

    @EndpointInject(uri = "direct:uspsCatalogCsvSink")
    private Endpoint csvSink;

    @EndpointInject(uri = "direct:csvSinkDLQ")
    private Endpoint csvSinkDLQ;

    @EndpointInject(uri = "direct:catalogSinkDLQ")
    private Endpoint catalogSinkDLQ;

    //@EndpointInject(uri = "file:uspsCatalogCsvOutput")
    //@EndpointInject(uri = "stream:out")
    //private Endpoint csvOutput;

    @EndpointInject(uri = "mock:uspsCatalogCsvOutput")
    private MockEndpoint csvOutput;

    @Override
    protected RouteBuilder createRouteBuilder() {
        EDI832OutboundRouteBuilder crb = new EDI832OutboundRouteBuilder();

        crb.setDataSource(this.dataSource.getEndpointUri());
        crb.setCatalogSink(this.catalogSink.getEndpointUri());
        crb.setCsvSink(this.csvSink.getEndpointUri());
        crb.setCsvOutput(this.csvOutput.getEndpointUri());
        crb.setCatalogSinkDLQ(catalogSinkDLQ.getEndpointUri());
        crb.setCsvSinkDLQ(csvSinkDLQ.getEndpointUri());
        crb.setDataFormatModelPackage("com.apd.phoenix.service.integration.usps.model");

        crb.setKeyFileName("eBuy2TestPublicKey.gpg");
        crb.setKeyUserid("ebuy gpg");
        crb.setArmored(true);
        crb.setEncrypted(false);
        crb.setBackupPartnerEndpoint(csvOutput.getEndpointUri());

        return crb;
    }

    /**
     * Tests that a USPS Catalog is Marshalled.
     * 
     * @throws Exception
     */
    @Ignore
    @Test
    public void marshallUspsCatalogTest() throws Exception {
        // Setup USPS Catalog
        UspsCatalogItem catalog = this.buildSampleCatalogItem();
        UspsCatalogItem catalog2 = this.buildSampleCatalogItem();
        
        List<UspsCatalogItem> catalogs = new LinkedList<>();
        catalogs.add(catalog);
        catalogs.add(catalog2);

        // Configure Mock
        this.csvOutput.expectedMessageCount(1);

        // Send Message
        this.producer.sendBody(this.catalogSink, ExchangePattern.InOut, catalogs);

        // Assert We Received 1 Message
        this.csvOutput.setResultWaitTime(2000);
        this.csvOutput.assertIsSatisfied();

        // Get Message Body
        List<Exchange> exchanges = this.csvOutput.getExchanges();
        Exchange exchange = exchanges.get(0);

        // Split Out Each Line
        String body = new String((byte[]) exchange.getIn().getBody());
        String lines[] = body.split("\\r?\\n");

        // Both Lines Should Match
        Assert.assertTrue(lines.length == 2);
        Assert.assertEquals(EXPECTED_SAMPLE_CSV, lines[0]);
        Assert.assertEquals(EXPECTED_SAMPLE_CSV, lines[1]);
    }

    @Ignore
    @Test
    public void processCatalogEntitiesTest() {
        ArrayList<CatalogXItemDto> items = createSampleDatabase();

        this.producer.sendBody(this.dataSource, ExchangePattern.InOut, items);

        // Get Message Body
        List<Exchange> exchanges = this.csvOutput.getExchanges();
        Exchange exchange = exchanges.get(0);

        // Split Out Each Line
        String body = new String((byte[]) exchange.getIn().getBody());
        String lines[] = body.split("\\r?\\n");

        // Both Lines Should Match
        Assert.assertTrue(lines.length == 2);
        Assert.assertEquals(EXPECTED_SAMPLE_FROM_DTO0, lines[0]);
        Assert.assertEquals(EXPECTED_SAMPLE_FROM_DTO1, lines[1]);
    }

    /**
     * Builds a Sample USPS Catalog. When Marshalled Is Should Match
     * 
     * @return
     */
    private UspsCatalogItem buildSampleCatalogItem() {
        UspsCatalogItem catalog = new UspsCatalogItem();

        Calendar cal = Calendar.getInstance();
        cal.set(1985, 5, 1, 0, 0, 0);
        Date effDate = cal.getTime();

        catalog.setSupplierId("943879320");
        catalog.setTransactionSetId("832");
        catalog.setRecordId("01");
        catalog.setItemNo("1096758");
        catalog
                .setDescription("Remanufacture Toner for HP LASERJETT 1320 / 3390 / 3392; OEM Part #(s): Q5949X, THIS_IS_JUST_FOR_EFFECT_AND_SHOULD_BE_CLIPPED");
        catalog.setSupplierItemNo("1096758");
        catalog.setManufacturerItemNo("TURTN1325");
        catalog.setUnspscCd(44103103);
        catalog.setSupplierHierarchy("");
        catalog.setItemPrice(50.0);
        catalog.setListPrice(97.5);
        catalog.setUom("EA");
        catalog.setBusinessClass("U");
        catalog.setWomenOwned("U");
        catalog.setMinorityBusiness("U");
        catalog.setJwodBusiness("N");
        catalog.setOtherbusiness("N");
        catalog.setContractNo("2DOSEQ-10-B-0033");
        catalog.setContractLine("");
        catalog.setContractMandatory("N");
        catalog.setClinType("");
        catalog.setCatalogPage("");
        catalog
                .setSupplierUrl("https://mercury.rev.net/estore/cgi-bin/peekItem?fileid=showitem&product=1096758&credential=263&pp=i9rnvy9k");
        catalog.setImageFileName("https://mercury.rev.net/estore/images/supplier/apd/image/RemanToner.jpg");
        catalog.setManufacturerName("Turbon");
        catalog.setManufacturerUrl("");
        catalog.setItemStatus("A");
        catalog
                .setNotes("SHIPS USPS PRIORITY MAIL-Remanufacture Toner for HP LASERJETT 1320 / 3390 / 3392; OEM Part s Q5949X, Black");
        catalog.setChangeCd("C");
        catalog.setEffectiveDate(effDate);
        catalog.setExpirationDate(null);
        catalog.setMinLineQty(1);
        catalog.setMaxLineQty(999999999);
        catalog.setIssueIncrement(1);
        catalog.setGreenProd("U");
        catalog.setEnergyStar("N");
        catalog.setBioPreferred("U");
        catalog.setMsdsReq("Y");
        catalog.setRecycledContent("Y");
        catalog.setEpeatRating("U");
        catalog.setNemaStd("U");
        catalog.setEpaWatersense("U");
        catalog.setUsepaPriChemFree("U");
        catalog.setRenewableEnergyResource("U");

        return catalog;
    }

    private ArrayList<CatalogXItemDto> createSampleDatabase() {
        ArrayList<CatalogXItemDto> items = new ArrayList<>();
     
        final CatalogXItemDto catalogXItemDto = new CatalogXItemDto();
        catalogXItemDto.setModified(Boolean.TRUE);
        catalogXItemDto.setPrice(BigDecimal.ONE);
        final ItemDto itemDto = new ItemDto();
        itemDto.setDescription("A 12 carrot gold stapeler. 13 x 3 inches and emblazoned with Sri Lankan rubies.");
        HashSet<ItemImageDto> imageSet = new HashSet<>();
        final ItemImageDto itemImageDto = new ItemImageDto();
        itemImageDto.setImageName("Closeup");
        itemImageDto.setImageUrl("http://www.blingmystapeler.com/gallerly/1337/goldie.png");
        itemImageDto.setPrimary(Boolean.TRUE);
        imageSet.add(itemImageDto);
        itemDto.setItemImages(imageSet);
        itemDto.setManufacturerName("Stapel Bling Indutries LLC");
        itemDto.setName("Gold Secratary 90000 v2");
        final ArrayList<SkuDto> skus = new ArrayList<>();
        final SkuDto manufacturerSku = new SkuDto();
        final SkuTypeDto manufacturerType = new SkuTypeDto();
        manufacturerType.setName("manufacturer");
        manufacturerSku.setType( manufacturerType);
        manufacturerSku.setValue("M13371337");
        skus.add(manufacturerSku);        
        final SkuDto vendorSku = new SkuDto();
        final SkuTypeDto vendorType = new SkuTypeDto();
        vendorType.setName("customer");
        vendorSku.setType( vendorType);
        vendorSku.setValue("V13371337");
        skus.add(vendorSku);                
        final SkuDto apdSku = new SkuDto();
        final SkuTypeDto apdType = new SkuTypeDto();
        apdType.setName("APD");
        apdSku.setType( apdType);
        apdSku.setValue("APD13371337");
        skus.add(apdSku);
        itemDto.setSkus(skus);
        final UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
        unitOfMeasureDto.setName("EA");
        itemDto.setUnitOfMeasure(unitOfMeasureDto);
        itemDto.setCommodityCode("1234567");
        catalogXItemDto.setItem(itemDto);
        final CatalogDto catalogDto = new CatalogDto();
        catalogXItemDto.setCatalog(catalogDto);
        items.add(catalogXItemDto);
        
        final CatalogXItemDto catalogXItemDto2 = new CatalogXItemDto();
        catalogXItemDto2.setModified(Boolean.TRUE);
        
        catalogXItemDto2.setPrice(BigDecimal.ONE);
        final ItemDto itemDto2 = new ItemDto();
        itemDto2.setDescription("A meh stapeler");
        itemDto2.setItemImages(imageSet);
        itemDto2.setManufacturerName("Stapel Group");
        itemDto2.setName("stapler");
        final ArrayList<SkuDto> skus2 = new ArrayList<>();
        final SkuDto manufacturerSku2 = new SkuDto();
        manufacturerSku2.setType( manufacturerType);
        manufacturerSku2.setValue("123455555");
        skus2.add(manufacturerSku2);        
        final SkuDto vendorSku2 = new SkuDto();
        vendorSku2.setType( vendorType);
        vendorSku2.setValue("123121333333");
        skus2.add(vendorSku2);                
        final SkuDto apdSku2 = new SkuDto();
        apdSku2.setType( apdType);
        apdSku2.setValue("APD008");
        skus2.add(apdSku2);
        itemDto2.setSkus(skus2);
        itemDto2.setUnitOfMeasure(unitOfMeasureDto);
        itemDto2.setCommodityCode("111111");
        catalogXItemDto2.setItem(itemDto2);
        final CatalogDto catalogDto2 = new CatalogDto();
        catalogXItemDto2.setCatalog(catalogDto2);
        itemDto2.getClassifications().put("WBE", "true");
        itemDto2.getProperties().put("MBE","true");
        itemDto2.getProperties().put("isJWOD","true");        
        items.add(catalogXItemDto2);
        
        return items;
    }
}