package com.apd.phoenix.service.integration.routes;

import org.apache.camel.builder.RouteBuilder;

public class AMQExampleRouteBuilder extends RouteBuilder {

    private String source;

    private String jmsEndpoint;

    private String sink;

    @Override
    public void configure() throws Exception {
        from(source).to(jmsEndpoint);

        from(jmsEndpoint).to(sink);

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getJmsEndpoint() {
        return jmsEndpoint;
    }

    public void setJmsEndpoint(String jmsEndpoint) {
        this.jmsEndpoint = jmsEndpoint;
    }

    public String getSink() {
        return sink;
    }

    public void setSink(String sink) {
        this.sink = sink;
    }

}
