/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author nreidelb
 */
public class EDI824InboundRouteBulderTest {

    private static final Logger LOG = LoggerFactory.getLogger(EDI824InboundRouteBulderTest.class);

    private CamelContext camelContext;

    private String orgSource = "direct:orgSource";

    private String orgLogSource = "direct:orgLogSource";

    private String orgLogDLQ = "direct:orgLogDLQ";

    private String outputDLQ = "direct:outputDLQ";

    private String orgSink = "mock:usscoOrderAckSink";

    EDI824RouteBuilder builder;
    private MockEndpoint mockUsscoOrderAckSink;
    private ProducerTemplate producer;
    private static final String KEY_FILE_NAME = "gpg/apd-phoenix-private-dev.gpg";
    private static final String KEY_USERID = "American Product Distributors (APD) <juanitarichardson@americanproduct.com>";
    private static final String ENC_PASSWORD = "b3j5hbat";

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI824RouteBuilder();
        builder.setLogOutputDLQ(orgLogDLQ);
        builder.setOrgLogSource(orgLogSource);
        builder.setOutput(orgSink);
        builder.setSource(orgSource);
        builder.setOrgLogOutput(orgLogSource);
        builder.setOutputDLQ(outputDLQ);
        builder.setLogOutputURI(outputDLQ);

        mockUsscoOrderAckSink = camelContext.getEndpoint(orgSink, MockEndpoint.class);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    @Ignore
    public void dto810Test() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(4);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample824.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(orgSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(4, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        EdiDocumentErrorsDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), EdiDocumentErrorsDto.class);
        dto.getRelevantNumberDto();
    }
}
