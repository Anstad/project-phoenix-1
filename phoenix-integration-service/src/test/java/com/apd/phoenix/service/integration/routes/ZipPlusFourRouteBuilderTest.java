package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.apd.phoenix.service.integration.tax.zip.model.ZipPlusFourBindyDto;
import com.apd.phoenix.service.model.TaxRate;
import com.apd.phoenix.service.model.ZipPlusFour;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ZipPlusFourRouteBuilderTest {

    ZipPlusFourRouteBuilder route;

    CamelContext camelContext;

    public static final String createSource = "direct:createsource";

    public static final String createOutput = "mock:createOutput";
    private MockEndpoint mockCreateOutput;

    public static final String updateSource = "direct:updateSource";

    public static final String deleteOutput = "mock:deleteOuput";
    private MockEndpoint mockDeleteOutput;

    ProducerTemplate producer;

    @Before
    public void before() throws Exception {
        route = new ZipPlusFourRouteBuilder();
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();

        route.setCreateSource(createSource);
        route.setCreateOutput(createOutput);
        mockCreateOutput = camelContext.getEndpoint(createOutput, MockEndpoint.class);
        route.setUpdateSource(updateSource);
        route.setDeleteOutput(deleteOutput);
        mockDeleteOutput = camelContext.getEndpoint(deleteOutput, MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void testParseUpdateFile() throws IOException, InterruptedException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("tax/PLUS4T-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String content = writer.toString();

        mockDeleteOutput.setExpectedMessageCount(1);
        mockCreateOutput.setExpectedMessageCount(1);
        producer.sendBody(updateSource, ExchangePattern.InOut, content.getBytes());
        mockDeleteOutput.setResultWaitTime(2000);
        mockCreateOutput.setResultWaitTime(2000);
        mockDeleteOutput.assertIsSatisfied();
        mockCreateOutput.assertIsSatisfied();
    }

    @Test
    public void testParseOriginalFile() throws IOException, SAXException, InterruptedException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("tax/PLUS4-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String content = writer.toString();

        mockCreateOutput.setExpectedMessageCount(1);
        producer.sendBody(createSource, ExchangePattern.InOut, content.getBytes());
        mockCreateOutput.setResultWaitTime(2000);
        mockCreateOutput.assertIsSatisfied();

        Exchange result = mockCreateOutput.getExchanges().get(0);

        ObjectMapper mapper = new ObjectMapper();

        List<ZipPlusFourBindyDto> zips = mapper.readValue((byte[]) result.getIn().getBody(),
                new TypeReference<List<ZipPlusFourBindyDto>>() {
                });
        ZipPlusFourBindyDto zip = zips.get(0);

        Assert.assertEquals("00934", zip.getZip());
        Assert.assertEquals("0001", zip.getLow());
        Assert.assertEquals("0660", zip.getHi());
        Assert.assertEquals("72061", zip.getCountyFips());

    }
}
