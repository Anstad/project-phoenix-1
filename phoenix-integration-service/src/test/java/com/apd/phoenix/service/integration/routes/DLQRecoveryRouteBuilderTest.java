package com.apd.phoenix.service.integration.routes;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DLQRecoveryRouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(DLQRecoveryRouteBuilder.class);

    private static final String input = "activemq:queue.edi-inbound-856-log-dlq";
    private static final String output = "ftp://ftp-dev.vpc.phoenixordering.com/outbound/local-recovery/856?password=password&username=ftp-dev&binary=true&passiveMode=true";
    private static final String brokerUrl = "failover://tcp://localhost:61616";

    private CamelContext camelContext;

    private DLQRecoveryRouteBuilder route;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(brokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(8);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        camelContext.addComponent("activemq", activeMQComponent);

        route = new DLQRecoveryRouteBuilder();
        route.setInput(input);
        route.setOutput(output);
        camelContext.addRoutes(route);

    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void run() throws Exception {
        LOG.info("Starting recovery..");
        camelContext.start();

        Thread.sleep(12000000);

        LOG.info("Stopping recovery.");
    }

}
