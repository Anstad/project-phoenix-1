/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.model.dto.MiscShipToDto;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import javax.edi.model.x12.segment.ReferenceIdentification;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class PurchaseOrderToEdi850TranslatorTest {

    @Test
    public void testGetReferenceIdentificationsBasic() {
        MiscShipToDto miscShipToDto = createBasicMiscShipTo();
        miscShipToDto.setApdSalesPerson("F");
        Collection<ReferenceIdentification> ris = PurchaseOrderToEdi850Translator.getReferenceIdentifications(
                miscShipToDto, new ArrayList<ReferenceIdentification>());
        Assert.assertEquals(5, ris.size());
    }

    @Test
    public void testGetReferenceIdentificationsBasicTooLong() {
        MiscShipToDto miscShipToDto = createBasicMiscShipTo();
        miscShipToDto.setApdSalesPerson("F");
        miscShipToDto.setDesktop("A STRING THAT HAS OVER 40 CHARACTERS ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
        Collection<ReferenceIdentification> ris = PurchaseOrderToEdi850Translator.getReferenceIdentifications(
                miscShipToDto, new HashSet<ReferenceIdentification>());
        Assert.assertEquals(5, ris.size());
    }

    @Test
    public void testGetReferenceIdentificationsEvenSplit() {
        MiscShipToDto miscShipToDto = createBasicMiscShipTo();
        miscShipToDto.setApdSalesPerson("F");
        miscShipToDto.setArea("G");
        miscShipToDto.setContractNumber("H");
        Collection<ReferenceIdentification> ris = PurchaseOrderToEdi850Translator.getReferenceIdentifications(
                miscShipToDto, new HashSet<ReferenceIdentification>());
        Assert.assertEquals(6, ris.size());
    }

    @Test
    public void testGetReferenceIdentificationsPileOn() {
        MiscShipToDto miscShipToDto = createBasicMiscShipTo();
        miscShipToDto.setApdSalesPerson("F");
        miscShipToDto.setArea("G");
        miscShipToDto.setContractNumber("H");
        miscShipToDto.setCarrierName("A QUITE LONG NAME QUITE TOO LONG");
        miscShipToDto.setDistrict("ALSO VERY LONG");
        miscShipToDto.setDesktop("A STRING THAT HAS OVER 40 CHARACTERS ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ");
        Collection<ReferenceIdentification> ris = PurchaseOrderToEdi850Translator.getReferenceIdentifications(
                miscShipToDto, new HashSet<ReferenceIdentification>());
        Assert.assertEquals(2, ris.size());
    }

    @Test
    public void testGetReferenceIdentificationsPileOnTooManySegments() {
        MiscShipToDto miscShipToDto = createBasicMiscShipTo();
        miscShipToDto.setApdSalesPerson("F");
        miscShipToDto.setArea("G");
        miscShipToDto.setContractNumber("H");
        miscShipToDto.setCarrierName("A QUITE LONG NAME QUITE LONG");
        miscShipToDto.setCarrierRouting("A QUITE LONG NAME QUITE LONG2");
        miscShipToDto.setContractingOfficer("A QUITE LONG NAME QUITE LONG3");
        miscShipToDto.setDcNumber("A QUITE LONG NAME QUITE LONG4");
        miscShipToDto.setDeliverToName("A QUITE LONG NAME QUITE LONG5");
        miscShipToDto.setDeliverToPhoneNumber("A QUITE LONG NAME QUITE LONG6");
        miscShipToDto.setDeliveryDate("A QUITE LONG NAME QUITE LONG7");
        miscShipToDto.setDepartment("A QUITE LONG NAME QUITE LONG8");
        miscShipToDto.setDept("A QUITE LONG NAME QUITE LONG9");
        Collection<ReferenceIdentification> ris = PurchaseOrderToEdi850Translator.getReferenceIdentifications(
                miscShipToDto, new HashSet<ReferenceIdentification>());
        Assert.assertEquals(6, ris.size());
    }

    private MiscShipToDto createBasicMiscShipTo() {
        MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setAddressId("A");
        miscShipToDto.setContactName("B");
        miscShipToDto.setAddressId36("C");
        miscShipToDto.setAirportCode("E");
        return miscShipToDto;
    }
}
