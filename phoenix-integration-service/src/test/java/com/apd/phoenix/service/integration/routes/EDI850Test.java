/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.EndpointInject;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class EDI850Test {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderTest.class);
    public static final String GOOD_SKU1 = "UNV79000";
    public static final String GOOD_SKU0 = "UNV27411";
    public static final String GOOD_SKU2 = "UNV21200";
    private static UnitOfMeasureDto uom;
    public static final String BAD_SKU = "UNV27411X";
    public static final String BACKORDER_SKU = "UNV27411X";
    public static final String SAMPLE_SHIP_FROM_FACILITY = "COI";
    public static final String USPS_DIRECTED_ACCOUNT_NUMBER = "201333";
    public static final String CHS_ACCOUNT_NUMBER = "201666";
    public static final String NOVANT_ACCOUNT_NUMBER = "201813";
    public static final String CHS_TEST_FOLDERS_SKU = "UNV12113";
    public static final String CHS_TEST_FOLDERS_DESCRIPTION = "File Folders, 1/3 Cut Assorted, One-Ply Top Tab, Letter, Manila, 100/Box";
    public static final String LOG_SOURCE = "direct:logSource";
    public static final String PARTNER_ENDPOINT = "direct:partnerEndpoint";

    private static PurchaseOrderDto generateOrderNoItems(String apdPo, String usscoAccountNumber, boolean sendCustPo, String name) {
        PurchaseOrderDto purchaseOrder = new PurchaseOrderDto();
        purchaseOrder.setApdPoNumber(apdPo);
        purchaseOrder.setOrderDate(new Date());
        purchaseOrder.setOrderTotal(new BigDecimal(575.25));
        if(usscoAccountNumber.equals(USPS_DIRECTED_ACCOUNT_NUMBER)){
            purchaseOrder.setShipFrom(SAMPLE_SHIP_FROM_FACILITY);
        }
        AddressDto address = new AddressDto();
        address.setLine1("45 PLATEAU ST");
        address.setLine2("DeskTop SCH Patient Registration PO 63052");
        address.setCity("BRYSON CITY");
        address.setState("NC");
        address.setZip("28713");
        final MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setDivision("DivisionOfLabor");
        address.setMiscShipToDto(miscShipToDto);
        purchaseOrder.setShipToAddressDto(address);
        //purchaseOrder.changeBillToAddressDto(address);
        purchaseOrder.changeBillToAddressDto(new AddressDto());
        if(sendCustPo){
            purchaseOrder.setCustomerPoNumber(apdPo);
        } else {
            purchaseOrder.setCustomerPoNumber("");
        }
        AccountDto account = new AccountDto();
        account.setApdAssignedAccountId("MED069NC");
        account.setTerms("Due Upon Receipt");
        account.setName(name);
        purchaseOrder.setAccount(account);
        CredentialDto credentialDto = new CredentialDto();
        final HashMap<String, Serializable> propertiesMap = new HashMap<>();
        propertiesMap.put("US Account #", usscoAccountNumber);
        if(usscoAccountNumber.equals(CHS_ACCOUNT_NUMBER)){
            propertiesMap.put("route code", "CLT");
        }
        if(usscoAccountNumber.equals(NOVANT_ACCOUNT_NUMBER)){
            propertiesMap.put("route code", "NVNC");          
        }
        credentialDto.setProperties(propertiesMap);
        purchaseOrder.setCredential(credentialDto);
        uom = new UnitOfMeasureDto();
        uom.setName("EA");
        return purchaseOrder;
    }

    @EndpointInject(uri = "ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test/?password=password?username=as2-dev")
    private Endpoint ediPartnerEndpoint;

    private CamelContext camelContext;
    private String dataModelSource = "direct:dataModelSource";
    private String resendSource = "direct:resendSource";
    private String endpoint = "direct:endpoint";
    private String dlq = "direct:dlq";
    private String apdSenderId = "apd";

    private ProducerTemplate producer;

    EDI850OutboundRouteBuilder route;
    private String logOutputUri = "direct:logOutputUri";
    private String logOutput = "direct:logOutput";
    private String logOutputDlq = "direct:logOutputDlq";

    @Before
    public void init() throws InterruptedException {
        camelContext = new DefaultCamelContext();
        route = new EDI850OutboundRouteBuilder();

        route.setLogOuputURI(logOutputUri);
        route.setLogOutputDLQ(logOutputDlq);
        route.setEdi850PartnerEndpointDLQ(resendSource);
        route.setPurchaseOrderSource(dataModelSource);
        route.setPurchaseOrderResendSource(resendSource);
        route.setLogOutput(logOutput);
        route.setSenderId(apdSenderId);
        route.setEdiLogSource(LOG_SOURCE);
        route.setEdi850PartnerEndpoint(PARTNER_ENDPOINT);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void testWalmartHQ() throws Exception {
        Pos pos = new Pos();
        pos.backorder = "APD002014630";
        pos.partial = "APD002014631";
        pos.qChange = "APD002014632";
        pos.splitOrder = "APD002014634";
        pos.rejected = "APD002014633";
        testCamelRoutes(pos, "201209", false, "walmart", true);
    }

    @Ignore
    @Test
    public void testWalmartKits() throws Exception {
        Pos pos = new Pos();
        pos.backorder = "APD002014615";
        pos.partial = "APD002014616";
        pos.qChange = "APD002014617";
        pos.splitOrder = "APD002014619";
        pos.rejected = "APD002014618";
        testCamelRoutes(pos, "201255", true, "walmartKits", true);
    }

    @Ignore
    @Test
    public void testWalmartPorterville() throws Exception {
        Pos pos = new Pos();
        pos.backorder = "APD002014620";
        pos.partial = "APD002014621";
        pos.qChange = "APD002014622";
        pos.rejected = "APD002014623";
        pos.splitOrder = "APD002014624";

        testCamelRoutes(pos, "201208", false, "walmartPortervile", true);
    }

    @Ignore
    @Test
    public void testBI() throws Exception {
        Pos pos = new Pos();
        pos.backorder = "APD002014625";
        pos.partial = "APD002014626";
        pos.qChange = "APD002014627";
        pos.rejected = "APD002014628";
        pos.splitOrder = "APD002014629";

        testCamelRoutes(pos, "958128", true, "BO", true);
    }

    @Ignore
    @Test
    public void testUSPS() throws Exception {
        Pos pos = new Pos();
        pos.qChange = "APD002014637";
        pos.rejected = "APD002014638";
        pos.splitOrder = "APD002014639";

        testCamelRoutes(pos, "958138", true, "USPS", false);
    }

    @Ignore
    @Test
    public void testUSPSFacilityDirected() throws Exception {
        Pos pos = new Pos();
        pos.qChange = "APD002014642";
        pos.rejected = "APD002014643";
        pos.splitOrder = "APD002014644";

        testCamelRoutes(pos, USPS_DIRECTED_ACCOUNT_NUMBER, true, "USPSFacilityDirected", false);
    }

    @Ignore
    @Test
    public void testCHS() throws Exception {
        Pos pos = new Pos();
        pos.qChange = "APD002014647";
        pos.rejected = "APD002014648";
        pos.splitOrder = "APD002014649";

        testCamelRoutes(pos, CHS_ACCOUNT_NUMBER, true, "CHS", false);
    }

    @Ignore
    @Test
    public void testNovant() throws Exception {
        Pos pos = new Pos();
        pos.qChange = "APD002014652";
        pos.rejected = "APD002014653";
        pos.splitOrder = "APD002014654";

        testCamelRoutes(pos, NOVANT_ACCOUNT_NUMBER, true, "Novant", false);
    }

    @Ignore
    @Test
    public void testInternal() throws Exception {
        String usscoAccountNumber = "958115";
        Pos pos = new Pos();
        pos.backorder = "APD002014612";
        pos.partial = "??????";
        pos.qChange = "APD??????";
        pos.splitOrder = "APD002015000";
        pos.rejected = "APD002014613";
        testCamelRoutes(pos, usscoAccountNumber, true, "apdInternal", false);
        testCamelRouteManyItems("APD002014606", usscoAccountNumber, true);
        testCamelRouteOneBadItem("APD002014614", usscoAccountNumber, true);
        testCamelRouteOneItem("APD002014537", usscoAccountNumber, true);
    }

    @Ignore
    @Test
    public void testCHSCustom() throws Exception{
        String testType = "CHS";
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "Rejected.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems("APD002053671", "201666", false,testType);
        purchaseOrder.getCredential().getProperties().clear();
        purchaseOrder.getCredential().getProperties().put("route code", "CLT");
        purchaseOrder.getShipToAddressDto().setLine1("330 BILLINGSLEY RD");
        purchaseOrder.getShipToAddressDto().setCity("CHARLOTTE");
        purchaseOrder.lookupBillToAddressDto().setZip("28211");
        MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setContactName("Quiana Friday");
        purchaseOrder.getShipToAddressDto().setMiscShipToDto(miscShipToDto);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue("QRT551");
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("BoardGear Marker Board Conditioner/Cleaner for Dry Erase Boards, 8 oz. Bottle");
        lineItemDto.setQuantity(new BigInteger("3"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId("QRT551");
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    @Ignore
    @Test
    public void testUSPSCustom() throws Exception{
        String testType = "USPS";
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "Rejected.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems("APD002053677", "201333", false,testType);
        purchaseOrder.setCustomerPoNumber("USP008822866");
        purchaseOrder.getCredential().getProperties().clear();
        purchaseOrder.getShipToAddressDto().setLine1("CAROLINA PUEBLO STA");
        purchaseOrder.getShipToAddressDto().setCity("CAROLINA");
        purchaseOrder.getShipToAddressDto().setState("PR");
        purchaseOrder.lookupBillToAddressDto().setZip("9859997");
        MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setContactName("MARIA DUMEN");
        purchaseOrder.getShipToAddressDto().setMiscShipToDto(miscShipToDto);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue("SAN30004EA");
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("Permanent Marker, Fine Point, Green");
        lineItemDto.setQuantity(new BigInteger("5"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId("SAN30004EA");
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    @Test
    public void testNovantCustom() throws Exception{
        String testType = "Novant";
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/outbound/local-john?password=password"
                + "&username=ftp-dev&binary=true&passiveMode=true&fileName=" + testType + "Reject.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems("APD002053680", "201813", false,testType);
        purchaseOrder.setCustomerPoNumber("PO624809");
        purchaseOrder.getCredential().getProperties().put("route code", "NVNC");
        purchaseOrder.getCredential().getProperties().clear();
        purchaseOrder.getShipToAddressDto().setName("John Eckstein");
        purchaseOrder.getShipToAddressDto().setCompanyName("Novant");
        purchaseOrder.getShipToAddressDto().setLine1("600 HIGHLAND OAKS DR");
        purchaseOrder.getShipToAddressDto().setCity("WINSTON SALEM");
        purchaseOrder.getShipToAddressDto().setState("NC");
        //purchaseOrder.lookupBillToAddressDto().setZip("27103");
        MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setContactName("Teresa France");
        miscShipToDto.setDesktop("Desktop Test");
        miscShipToDto.setDept("DIV/DEPT Test");
        miscShipToDto.setMailStop("Mail Stop 1");
        purchaseOrder.getShipToAddressDto().setMiscShipToDto(miscShipToDto);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue("ABFTC1182");
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setShortName("Address Labels, 1-2/5 x 3-1/2, White, 520/Box");
        lineItemDto.setQuantity(new BigInteger("23"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId("ABFTC1182");
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    private void testCamelRoutes(Pos apdPos, String usscoAccountNumber, boolean sendCustPo, String testType,
            boolean backordered) throws Exception {
        if (backordered) {
            testBackorder(apdPos.backorder, usscoAccountNumber, sendCustPo, testType);
            after();
            init();
            testPartial(apdPos.partial, usscoAccountNumber, sendCustPo, testType);
            after();
            init();
        }
        testItemQuantityChanged(apdPos.qChange, usscoAccountNumber, sendCustPo, testType);
        after();
        init();
        testSplitOrder(apdPos.splitOrder, usscoAccountNumber, sendCustPo, testType);
        after();
        init();
        testCamelRouteManyItemsOneBad(apdPos.rejected, usscoAccountNumber, sendCustPo, testType);

    }

    public void testCamelRouteOneItem(String apdPo, String usscoAccountNumber, boolean sendCustPo) throws Exception {
        route
                .setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password&username=as2-dev&binary=true&passiveMode=true&fileName=oneGood.txt");
        camelContext.addRoutes(route);
        camelContext.start();

        PurchaseOrderDto purchaseOrder = generateSampleOrder(apdPo, usscoAccountNumber, sendCustPo);
        purchaseOrder.getItems().remove(1);
        final SkuDto skuDto = new SkuDto();
        skuDto.setValue(GOOD_SKU1);
        purchaseOrder.getItems().get(0).setCustomerSku(skuDto);

        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
        //        List<Exchange> exchanges = mockUspsSink.getReceivedExchanges();
        //     Assert.assertNotNull(exchanges.get(0));
    }

    public void testCamelRouteOneBadItem(String apdPo, String usscoAccountNumber, boolean sendCustPo) throws Exception {
        route
                .setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password&username=as2-dev&binary=true&passiveMode=true&fileName=oneBad.txt");
        //   route.setEdi850PartnerEndpoint("file:?fileName=outputEdiBad.txt");
        camelContext.addRoutes(route);
        camelContext.start();

        PurchaseOrderDto purchaseOrder = generateSampleOrder(apdPo, usscoAccountNumber, sendCustPo);
        purchaseOrder.getItems().remove(1);
        final SkuDto skuDto = new SkuDto();
        skuDto.setValue(BAD_SKU);
        purchaseOrder.getItems().get(0).setCustomerSku(skuDto);

        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
        //        List<Exchange> exchanges = mockUspsSink.getReceivedExchanges();
        //     Assert.assertNotNull(exchanges.get(0));
    }

    public void testCamelRouteManyItems(String apdPo, String usscoAccountNumber, boolean sendCustPo) throws Exception {
        route
                .setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password&username=as2-dev&binary=true&passiveMode=true&fileName=manyGood.txt");
        //  route.setEdi850PartnerEndpoint("file:?fileName=outputEdi2.txt");
        camelContext.addRoutes(route);
        camelContext.start();

        PurchaseOrderDto purchaseOrder = generateSampleOrder(apdPo, usscoAccountNumber, sendCustPo);
        SkuDto skuDto0 = new SkuDto();
        skuDto0.setValue(GOOD_SKU0);
        purchaseOrder.getItems().get(0).setCustomerSku(skuDto0);

        SkuDto skuDto1 = new SkuDto();
        skuDto1.setValue(GOOD_SKU1);
        purchaseOrder.getItems().get(1).setCustomerSku(skuDto1);

        SkuDto skuDto2 = new SkuDto();
        skuDto2.setValue(GOOD_SKU2);
        LineItemDto item2 = new LineItemDto();
        item2.setCustomerSku(skuDto2);
        item2.setDescription("A fine item.");
        item2.setQuantity(new BigInteger("45"));
        item2.setUnitPrice(new BigDecimal(12.99));
        item2.setUnitOfMeasure(uom);
        item2.setSupplierPartId(GOOD_SKU2);
        item2.setLineNumber(new Integer(3));
        purchaseOrder.getItems().add(item2);

        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
        //        List<Exchange> exchanges = mockUspsSink.getReceivedExchanges();
        //     Assert.assertNotNull(exchanges.get(0));
    }

    public void testCamelRouteManyItemsOneBad(String apdPo, String usscoAccountNumber, boolean sendCustPo,
            String testType) throws Exception {
        route
                .setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password&username=as2-dev&binary=true&passiveMode=true&fileName="
                        + testType + "Rejected.txt");
        //  route.setEdi850PartnerEndpoint("file:?fileName=outputEdiOneOfManyBad.txt");
        camelContext.addRoutes(route);
        camelContext.start();

        PurchaseOrderDto purchaseOrder = generateSampleOrder(apdPo, usscoAccountNumber, sendCustPo);
        SkuDto skuDto0 = new SkuDto();
        skuDto0.setValue(BAD_SKU);
        purchaseOrder.getItems().get(0).setCustomerSku(skuDto0);

        SkuDto skuDto1 = new SkuDto();
        skuDto1.setValue(GOOD_SKU1);
        purchaseOrder.getItems().get(1).setCustomerSku(skuDto1);

        SkuDto skuDto2 = new SkuDto();
        skuDto2.setValue(GOOD_SKU2);
        LineItemDto item2 = new LineItemDto();
        item2.setCustomerSku(skuDto2);
        item2.setDescription("A fine item.");
        item2.setQuantity(new BigInteger("45"));
        item2.setUnitPrice(new BigDecimal(12.99));
        item2.setUnitOfMeasure(uom);
        item2.setSupplierPartId(GOOD_SKU2);
        item2.setLineNumber(new Integer(3));
        purchaseOrder.getItems().add(item2);

        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
        //        List<Exchange> exchanges = mockUspsSink.getReceivedExchanges();
        //     Assert.assertNotNull(exchanges.get(0));
    }

    public void testBackorder(String apdPo, String usscoAccountNumber, boolean sendCustPo, String testType) throws Exception{
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "Backorder.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems(apdPo, usscoAccountNumber, sendCustPo,testType);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        final LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue(BACKORDER_SKU);
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("A backorderd item.");
        lineItemDto.setQuantity(new BigInteger("4"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId(BACKORDER_SKU);
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    public void testPartial(String apdPo, String usscoAccountNumber, boolean sendCustPo, String testType) throws Exception{
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "PartialBackorder.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems(apdPo, usscoAccountNumber, sendCustPo,testType);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        final LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue(BACKORDER_SKU);
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("A backorderd item.");
        lineItemDto.setQuantity(new BigInteger("4"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId(BACKORDER_SKU);
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    public void testItemQuantityChanged(String apdPo, String usscoAccountNumber, boolean sendCustPo, String testType) throws Exception{
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "ItemQuantityChanged.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems(apdPo, usscoAccountNumber, sendCustPo, testType);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        final LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue(BACKORDER_SKU);
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("A backorderd item.");
        lineItemDto.setQuantity(new BigInteger("4"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId(BACKORDER_SKU);
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    public void testSplitOrder(String apdPo, String usscoAccountNumber, boolean sendCustPo, String testType) throws Exception{
        route.setEdi850PartnerEndpoint("ftp://ftp-dev.vpc.phoenixordering.com/internal-test/outbound/test?password=password"
                + "&username=as2-dev&binary=true&passiveMode=true&fileName=" + testType + "SplitOrder.txt");
        camelContext.addRoutes(route);
        camelContext.start();
        
        PurchaseOrderDto purchaseOrder = generateOrderNoItems(apdPo, usscoAccountNumber, sendCustPo, testType);
        ArrayList<LineItemDto> itemList = new ArrayList<>();
        final LineItemDto lineItemDto = new LineItemDto();
        SkuDto skuDto = new SkuDto();
        skuDto.setValue(BACKORDER_SKU);
        lineItemDto.setCustomerSku(skuDto);
        lineItemDto.setDescription("A backorderd item.");
        lineItemDto.setQuantity(new BigInteger("4"));
        lineItemDto.setUnitPrice(new BigDecimal("12.40"));
        lineItemDto.setUnitOfMeasure(uom);
        lineItemDto.setSupplierPartId(BACKORDER_SKU);
        lineItemDto.setLineNumber(new Integer(1));
        itemList.add(lineItemDto);
        purchaseOrder.setItems(itemList);
        
        producer.sendBody(dataModelSource, ExchangePattern.InOut, purchaseOrder);
    }

    private static PurchaseOrderDto generateSampleOrder(String apdPo, String usscoAccountNumber, boolean sendCustPo) {
        PurchaseOrderDto purchaseOrder = generateOrderNoItems(apdPo, usscoAccountNumber, sendCustPo, "sampleName");

        LineItemDto lineItem1 = new LineItemDto();
        lineItem1.setApdSku("ECS122ELT650H21A");
        lineItem1.setShortName("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem1.setQuantity(new BigInteger("1"));
        lineItem1.setUnitOfMeasure(uom);
        lineItem1.setUnitPrice(new BigDecimal(374.25));
        lineItem1.setSupplierPartId(GOOD_SKU0);
        lineItem1.setLineNumber(new Integer(1));

        purchaseOrder.getItems().add(lineItem1);

        LineItemDto lineItem2 = new LineItemDto();
        lineItem2.setApdSku("ECS122ELT650H21A");
        lineItem2.setShortName("CompatibleToner with Lexmark 650H21A 25000 Page-Yield Lexmark T650 T652 T654 Bla");
        lineItem2.setQuantity(new BigInteger("1"));
        lineItem2.setUnitOfMeasure(uom);
        lineItem2.setUnitPrice(new BigDecimal(374.25));
        lineItem2.setSupplierPartId(GOOD_SKU1);
        lineItem2.setLineNumber(new Integer(2));

        purchaseOrder.getItems().add(lineItem2);
        return purchaseOrder;
    }

    private class Pos {

        public String rejected;
        public String backorder;
        public String partial;
        public String qChange;
        public String splitOrder;
    }

}
