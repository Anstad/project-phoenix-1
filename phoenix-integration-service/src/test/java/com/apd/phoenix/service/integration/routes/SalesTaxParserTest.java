package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import com.apd.phoenix.service.model.TaxRate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SalesTaxParserTest {

    TaxRateRouteBuilder route;

    CamelContext camelContext;

    public static final String createSource = "direct:createsource";

    public static final String createOutput = "mock:createOutput";
    private MockEndpoint mockCreateOutput;

    public static final String updateSource = "direct:updateSource";

    public static final String deleteOutput = "mock:deleteOuput";
    private MockEndpoint mockDeleteOutput;

    ProducerTemplate producer;

    @Before
    public void before() throws Exception {
        route = new TaxRateRouteBuilder();
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();

        route.setCreateSource(createSource);
        route.setCreateOutput(createOutput);
        mockCreateOutput = camelContext.getEndpoint(createOutput, MockEndpoint.class);
        route.setUpdateSource(updateSource);
        route.setDeleteOutput(deleteOutput);
        mockDeleteOutput = camelContext.getEndpoint(deleteOutput, MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void testParseUpdateFile() throws IOException, InterruptedException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("tax/FLATT-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String content = writer.toString();

        mockDeleteOutput.setExpectedMessageCount(1);
        mockCreateOutput.setExpectedMessageCount(1);
        producer.sendBody(updateSource, ExchangePattern.InOut, content.getBytes());
        mockDeleteOutput.setResultWaitTime(2000);
        mockCreateOutput.setResultWaitTime(2000);
        mockDeleteOutput.assertIsSatisfied();
        mockCreateOutput.assertIsSatisfied();
    }

    @Test
    public void testParseOriginalFile() throws IOException, SAXException, InterruptedException {
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("tax/FLAT-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String content = writer.toString();

        mockCreateOutput.setExpectedMessageCount(1);
        producer.sendBody(createSource, ExchangePattern.InOut, content.getBytes());
        mockCreateOutput.setResultWaitTime(2000);
        mockCreateOutput.assertIsSatisfied();

        Exchange result = mockCreateOutput.getExchanges().get(0);

        ObjectMapper mapper = new ObjectMapper();

        List<TaxRate> rates = mapper.readValue((byte[]) result.getIn().getBody(), new TypeReference<List<TaxRate>>() {
        });
        TaxRate rate = rates.get(0);
        //TaxRate rate = mapper.readValue((byte[]) result.getIn().getBody(), TaxRate.class);

        Assert.assertEquals("00501", rate.getZip());
        Assert.assertEquals("HOLTSVILLE", rate.getCity());
        Assert.assertEquals("SUFFOLK", rate.getCounty());
        Assert.assertEquals("NY", rate.getState());
        Assert.assertEquals(null, rate.getCountyDefault());
        Assert.assertEquals(null, rate.getGeneralDefault());
        Assert.assertEquals("36103", rate.getCountyFips());
        Assert.assertEquals(new BigDecimal("0.040000"), rate.getStateSales());
        Assert.assertEquals(new BigDecimal("0.042500"), rate.getCountySales());
        Assert.assertEquals(new BigDecimal("0.003750"), rate.getCountyLocalSales());
        Assert.assertEquals(new BigDecimal("0.000000"), rate.getCitySales());
        Assert.assertEquals(new BigDecimal("0.000000"), rate.getCityLocalSales());
        Assert.assertEquals(new BigDecimal("0.086250"), rate.getCombinedSales());
        Assert.assertEquals(new BigDecimal("0.040000"), rate.getStateUse());
        Assert.assertEquals(new BigDecimal("0.042500"), rate.getCountyUse());
        Assert.assertEquals(new BigDecimal("0.003750"), rate.getCountyLocalUse());
        Assert.assertEquals(new BigDecimal("0.000000"), rate.getCityUse());
        Assert.assertEquals(new BigDecimal("0.000000"), rate.getCityLocalUse());
        Assert.assertEquals(new BigDecimal("0.086250"), rate.getCombinedUse());
        Assert.assertEquals("20050601", new SimpleDateFormat("yyyyMMdd").format(rate.getEffectiveDate()));
        Assert.assertEquals("3610335254", rate.getGeocode());
        Assert.assertEquals("I", rate.getLocationInCity());
    }
}
