package com.apd.phoenix.service.integration.routes;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AMQExampleRouteBuilderIT {

    private static final String amqSource = "direct:amqSource";

    private static final String jmsEndpoint = "activemq:queue:camel-junit-test";

    private static final String amqSink = "mock:amqSink";
    MockEndpoint mockAmqSink;

    private CamelContext camelContext;

    private AMQExampleRouteBuilder route;

    private ProducerTemplate producer;

    @Before
    public void before() {
        camelContext = new DefaultCamelContext();
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("failover://tcp://jms-dev.vpc.phoenixordering.com:61616");
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(8);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        camelContext.addComponent("activemq", activeMQComponent);

        route = new AMQExampleRouteBuilder();
        route.setSource(amqSource);
        route.setJmsEndpoint(jmsEndpoint);
        route.setSink(amqSink);
        mockAmqSink = camelContext.getEndpoint(amqSink, MockEndpoint.class);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void example() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockAmqSink.setExpectedMessageCount(1);
        producer.sendBody(amqSource, "Hello World");
        mockAmqSink.setResultWaitTime(5000);
        mockAmqSink.assertIsSatisfied();
    }
}
