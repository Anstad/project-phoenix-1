package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EDI997RouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997RouteBuilder.class);
    private CamelContext camelContext;

    private static final String fASource = "direct:fASource";

    private static final String fAOutput = "mock:fAOutput";
    private MockEndpoint mockFAOutput;

    private static final String fAOutputDLQ = "mock:fAOutputDLQ";
    private MockEndpoint mockFAOutputDLQ;

    private static final String fAOutboundSource = "direct:fAOutboundSource";

    private static final String faOutboundOutput = "direct:fAOutboundOuput";

    private static final String fAResendSource = "direct:fAResendSource";

    private static final String fAResendOuput = "direct:fAResendOutput";

    private static final String logSource = "direct:logSource";

    private static final String formattedSource = "direct:formattedSource";

    private static final String outboundLogSource = "direct:outboundLogSource";

    private ProducerTemplate producer;

    EDI997RouteBuilder route;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        route = new EDI997RouteBuilder();
        route.setSource(fASource);
        route.setOutputDLQ(fAOutputDLQ);
        route.setOutboundOutput(faOutboundOutput);
        route.setfAResendSource(fAResendSource);
        route.setfAResendOuput(fAResendOuput);
        route.setFormattedSource(formattedSource);
        route.setOutboundLogSource(outboundLogSource);
        route.setLogSource(logSource);
        route.setLogging(false);
        //Outbound
        route.setOutboundSource(fAOutboundSource);

        mockFAOutput = camelContext.getEndpoint(fAOutput, MockEndpoint.class);
        mockFAOutputDLQ = camelContext.getEndpoint(fAOutputDLQ, MockEndpoint.class);
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    @Ignore
    public void valid997() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockFAOutput.setExpectedMessageCount(1);

        mockFAOutputDLQ.setExpectedMessageCount(0);

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream(
                "edi-997/node1_20131009115135249.dat.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(fASource, ExchangePattern.InOut, ediMessage);

        mockFAOutput.setResultWaitTime(2000);
        mockFAOutputDLQ.setResultWaitTime(2000);
        mockFAOutput.assertIsSatisfied();
        mockFAOutputDLQ.assertIsSatisfied();

    }

    @Test
    @Ignore
    public void invalid997() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        mockFAOutput.setExpectedMessageCount(2);
        mockFAOutputDLQ.setExpectedMessageCount(0);

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-997/sample-997-errors.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(fASource, ExchangePattern.InOut, ediMessage);

        mockFAOutput.setResultWaitTime(2000);
        mockFAOutputDLQ.setResultWaitTime(2000);
        mockFAOutput.assertIsSatisfied();
        mockFAOutputDLQ.assertIsSatisfied();

    }

}
