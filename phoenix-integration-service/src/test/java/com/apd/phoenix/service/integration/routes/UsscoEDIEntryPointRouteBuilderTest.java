package com.apd.phoenix.service.integration.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class UsscoEDIEntryPointRouteBuilderTest {

    private static final String usscoTransactionSource = "ftp://as2-dev@ftp-dev.vpc.phoenixordering.com/internal-test/inbound?password=password&noop=true&binary=true&passiveMode=true";

    //997
    private static final String funcationalAckInboundSource = "direct:funcationalAckInboundSource";

    private static final String funcationalAckInboundSink = "mock:funcationalAckInboundSink";
    MockEndpoint mockfuncationalAckInboundSink;

    private static final String transactionUnknown = "mock:transactionUnknown";
    private MockEndpoint mockTransactionUnknown;

    //855
    private static final String poAckInboundSource = "direct:poAckInboundSource";

    private static final String poAckInboundLogSink = "mock:poAckInboundLogSink";
    private MockEndpoint mockPoAckInboundLogSink;

    private static final String poAckInboundLogDLQ = "direct:poAckInboundLogDLQ";

    private static final String poAckInboundDLQ = "direct:poAckInboundDLQ";

    private static final String poAckInboundSink = "mock:poAckInboundSink";
    private MockEndpoint mockPoAckInboundSink;

    //856
    private static final String shipmentNoticeInboundSource = "direct:shipmentNoticeInboundSource";
    private static final String shipmentNoticeInboundSink = "mock:shipmentNoticeInboundSink";
    private MockEndpoint mockShipmentNoticeInboundSink;
    private static final String shipmentNoticeInboundLogSink = "mock:shipmentNoticeInboundLogSink";
    private MockEndpoint mockShipmentNoticeInboundLogSink;

    private static final String shipmentNoticeInboundLogDLQ = "direct:advancedShipmentNoticeLogDLQ";

    private static final String shipmentNoticeInboundDLQ = "direct:advancedShipmentNoticeWorkflowDLQ";

    //RouteBuilders

    private EDIEntryPointRouteBuilder routeEntry;

    private EDI997RouteBuilder route997;

    private EDI855RouteBuilder route855;

    private EDI856RouteBuilder route856;

    private CamelContext camelContext;

    private ProducerTemplate producer;

    @Before
    public void before() {
        camelContext = new DefaultCamelContext();

        producer = camelContext.createProducerTemplate();
        //Entry Route
        routeEntry = new EDIEntryPointRouteBuilder();
        routeEntry.setPartnerId("USSCO");
        mockTransactionUnknown = camelContext.getEndpoint(transactionUnknown, MockEndpoint.class);
        routeEntry.setTransactionSource(usscoTransactionSource);
        routeEntry.setTransactionFASink(funcationalAckInboundSource);
        routeEntry.setTransactionPRSink(poAckInboundSource);
        routeEntry.setTransactionSHSink(shipmentNoticeInboundSource);
        routeEntry.setTransactionINSink("stream:out");
        routeEntry.setTransactionPOSink("stream:out");
        routeEntry.setTransactionUnknown(transactionUnknown);

        //997 Route
        route997 = new EDI997RouteBuilder();
        mockfuncationalAckInboundSink = camelContext.getEndpoint(funcationalAckInboundSink, MockEndpoint.class);
        route997.setSource(funcationalAckInboundSource);

        //855 Route
        route855 = new EDI855RouteBuilder();
        mockPoAckInboundSink = camelContext.getEndpoint(poAckInboundSink, MockEndpoint.class);
        mockPoAckInboundLogSink = camelContext.getEndpoint(poAckInboundLogSink, MockEndpoint.class);
        route855.setSource(poAckInboundSource);
        route855.setOutput(poAckInboundSink);
        route855.setLogOutput(poAckInboundLogSink);
        route855.setLogOutputDLQ(poAckInboundLogDLQ);
        route855.setOutputDLQ(poAckInboundDLQ);

        //856 Route
        route856 = new EDI856RouteBuilder();
        mockShipmentNoticeInboundSink = camelContext.getEndpoint(shipmentNoticeInboundSink, MockEndpoint.class);
        mockShipmentNoticeInboundLogSink = camelContext.getEndpoint(shipmentNoticeInboundLogSink, MockEndpoint.class);
        route856.setSource(shipmentNoticeInboundSource);
        route856.setOutput(shipmentNoticeInboundSink);
        route856.setLogOutput(shipmentNoticeInboundLogSink);
        route856.setLogOutputDLQ(shipmentNoticeInboundLogDLQ);
        route856.setOutputDLQ(shipmentNoticeInboundDLQ);

    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Test
    public void processFtpTransactions() throws Exception {
        camelContext.addRoutes(routeEntry);
        camelContext.addRoutes(route997);
        camelContext.addRoutes(route855);
        camelContext.addRoutes(route856);
        camelContext.start();

        mockfuncationalAckInboundSink.setExpectedMessageCount(1);
        mockPoAckInboundSink.setExpectedMessageCount(1);
        mockPoAckInboundLogSink.setExpectedMessageCount(1);
        mockShipmentNoticeInboundSink.setExpectedMessageCount(1);
        mockShipmentNoticeInboundLogSink.setExpectedMessageCount(1);
        mockTransactionUnknown.setExpectedMessageCount(0);

        mockfuncationalAckInboundSink.setResultWaitTime(5000);
        mockPoAckInboundSink.setResultWaitTime(5000);
        mockPoAckInboundLogSink.setResultWaitTime(5000);
        mockShipmentNoticeInboundSink.setResultWaitTime(5000);
        mockShipmentNoticeInboundLogSink.setResultWaitTime(5000);
        mockTransactionUnknown.setResultWaitTime(5000);

        mockfuncationalAckInboundSink.assertIsSatisfied();
        mockPoAckInboundLogSink.assertIsSatisfied();
        mockPoAckInboundLogSink.assertIsSatisfied();
        mockShipmentNoticeInboundSink.assertIsSatisfied();
        mockShipmentNoticeInboundLogSink.assertIsSatisfied();
        mockTransactionUnknown.assertIsSatisfied();
    }
}
