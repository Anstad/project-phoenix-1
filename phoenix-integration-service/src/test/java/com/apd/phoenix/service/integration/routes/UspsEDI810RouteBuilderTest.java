/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

/**
 *
 * @author nreidelb
 */
public class UspsEDI810RouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderTest.class);

    private CamelContext camelContext;
    private String dataModelSource = "direct:dataModelSource";
    private String uspsSink = "mock:uspsSink";
    private String uspsLogDLQ = "direct:uspsLogDLQ";

    private ProducerTemplate producer;

    EDI810OutboundRouteBuilder route;
    private MockEndpoint mockUspsSink;

    @Before
    public void init() throws InterruptedException {
        camelContext = new DefaultCamelContext();
        route = new EDI810OutboundRouteBuilder();
        route.setDataModelSource(dataModelSource);
        route.setUspsLogDLQ("stream:out");
        route.setOutboundInvoiceSink("stream:out");
        route.setSenderId("APD");
        route.setPartnerId("usps");
        route.setKeyFileName("eBuy2TestPublicKey.gpg");
        route.setKeyUserid("ebuy gpg");
        route.setArmored(true);
        route.setEncrypted(true);
        /**route.setUspsSink(uspsSink);

        mockUspsSink = camelContext.getEndpoint(uspsSink, MockEndpoint.class);**/

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    public void testCamelRoute() throws Exception {
        camelContext.addRoutes(route);
        camelContext.start();

        InvoiceDto invoice = createInvoiceDto();

        producer.sendBody(dataModelSource, ExchangePattern.InOut, invoice);
        //        List<Exchange> exchanges = mockUspsSink.getReceivedExchanges();
        //     Assert.assertNotNull(exchanges.get(0));
    }

    private InvoiceDto createInvoiceDto(){
        CustomerInvoiceDto invoice = new CustomerInvoiceDto();
        invoice.setAmount(new BigDecimal(1234));
        invoice.setCreatedDate(new Date());
        invoice.setBillingTypeCode("billingTypeCode");
        invoice.setInvoiceNumber("invoiceNumber");
        ShipmentDto shipment = new ShipmentDto();
        PurchaseOrderDto order = new PurchaseOrderDto();
        List<PoNumberDto> poNumbers = new ArrayList<>();
        PoNumberDto customerPo = new PoNumberDto();
        customerPo.setType("Customer");
        customerPo.setValue("customerPoValue");
        poNumbers.add(customerPo);
        order.setPoNumbers(poNumbers);
        order.setOrderDate(new Date());
        order.setApdPoNumber("apdPO");
        final AddressDto shipToAddress = new AddressDto();
        final MiscShipToDto miscShipToDto = new MiscShipToDto();
        miscShipToDto.setFinanceNumber("1234567FinanceNum");
        miscShipToDto.setContractNumber("12345ContractNum");
        shipToAddress.setMiscShipToDto(miscShipToDto);
        order.setShipToAddressDto(shipToAddress);
       shipment.setCustomerOrderDto(order);
        shipment.setTrackingNumber("trackingNumber");
        shipment.setTotalTaxPrice(BigDecimal.TEN);
        List<LineItemXShipmentDto> lineItems = new ArrayList<>() ;
        LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
        LineItemDto item = new LineItemDto();
        final UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
        unitOfMeasureDto.setName("UM");
        item.setUnitOfMeasure(unitOfMeasureDto);
        item.setLineNumber(1);
        item.setApdSku("apdSku");
        item.setCost(new BigDecimal(1337));
        item.setUnitPrice(BigDecimal.TEN);
        item.setCustomerLineNumber("99");
        lineItemXShipmentDto.setLineItemDto(item);
        lineItemXShipmentDto.setQuantity(new BigInteger("10"));
        lineItems.add(lineItemXShipmentDto);
        shipment.setLineItemXShipments(lineItems);
        shipment.setShipmentPrice(BigDecimal.ONE);
        invoice.setShipment(shipment);
        invoice.setInvoiceNumber("an Invoice Number");
        return invoice;
    }
}
