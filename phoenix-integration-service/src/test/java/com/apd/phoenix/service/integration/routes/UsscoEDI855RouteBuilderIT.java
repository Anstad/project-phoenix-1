package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsscoEDI855RouteBuilderIT {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderIT.class);

    private CamelContext camelContext;

    private String usscoOrderAckSource = "direct:usscoOrderAckSource";

    private String usscoOrderAckLogDLQ = "mock:usscoOrderAckLogDLQ";
    private MockEndpoint mockUsscoOrderAckLogDLQ;

    private String usscoOrderAckWorkflowDLQ = "direct:usscoOrderAckWorkflowDLQ";

    private ProducerTemplate producer;

    private EDI855RouteBuilder route;

    private String usscoOrderAckSink = "http://localhost:8180/rest/workflow/order/po-acknowledgement";

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        route = new EDI855RouteBuilder();
        route.setSource(usscoOrderAckSource);
        route.setOutput(usscoOrderAckSink);
        route.setLogOutputDLQ(usscoOrderAckLogDLQ);
        route.setOutputDLQ(usscoOrderAckWorkflowDLQ);

        mockUsscoOrderAckLogDLQ = camelContext.getEndpoint(usscoOrderAckLogDLQ, MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void logHttpExceptionTest() throws Exception {
        String usscoOrderAckLogHttpUri = "http://localhost";
        route.setLogOutputURI(usscoOrderAckLogHttpUri);
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckLogDLQ.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("ussco-sample-855-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckLogDLQ.setResultWaitTime(2000);
        mockUsscoOrderAckLogDLQ.assertIsSatisfied();

    }

    @Test
    public void createOrderLogTest() throws Exception {
        String usscoOrderAckLogHttpUri = "http://localhost:8180/rest/messages/";
        route.setLogOutputURI(usscoOrderAckLogHttpUri);
        camelContext.addRoutes(route);
        camelContext.start();

        mockUsscoOrderAckLogDLQ.setExpectedMessageCount(0);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("ussco-sample-855-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoOrderAckSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckLogDLQ.setResultWaitTime(2000);
        mockUsscoOrderAckLogDLQ.assertIsSatisfied();

    }

}
