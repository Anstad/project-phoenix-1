/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class EncryptTest {

    // public static final String CSV_OUTPUT = "ftp://jbossas@localhost/outbound/usps/noah-local?password=jbossas&binary=true&passiveMode=true";
    public static final String CSV_OUTPUT = "file:///Users/jamiedarst/Downloads/usps855_encrypted_with_apd_public.pgp";
    public static final String CSV_SINK = "direct:csvSink";

    private ProducerTemplate producer;

    private String source = "direct:shipmentSource";

    private String ediEndpoint = "mock:output";

    private MockEndpoint mockEdiEndpoint;

    CamelContext camelContext;

    EDI832OutboundRouteBuilder builder;

    @Before
    public void before() throws Exception {

        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        mockEdiEndpoint = camelContext.getEndpoint(ediEndpoint, MockEndpoint.class);
        builder = new EDI832OutboundRouteBuilder();
        builder.setDataSource(source);
        builder.setCsvOutput(CSV_OUTPUT);
        builder.setCsvSinkDLQ("direct:a");
        builder.setCatalogSink("direct:b");
        builder.setCsvSink(CSV_SINK);
        builder.setCatalogSinkDLQ(ediEndpoint);

        //        builder.setKeyFileName("file:/home/jeckstei/Documents/projects/APD/eBuy2TestPublicKey.gpg");
        //builder.setKeyFileName("eBuy2TestPublicKey.gpg");
        builder.setKeyFileName("gpg/apd-phoenix-public-dev.gpg");
        //builder.setKeyUserid("ebuy gpg");
        builder.setKeyUserid("American Product Distributors (APD) <juanitarichardson@americanproduct.com>");
        builder.setArmored(true);
        builder.setEncrypted(true);
        builder.setBackupPartnerEndpoint(ediEndpoint);

    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    //Test used to generate usps encrypted file for integration testing
    @Test
    public void testEncrypted() throws Exception {

        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("usps855.txt");
        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String message = writer.toString();

        camelContext.addRoutes(builder);
        camelContext.start();
        this.producer.sendBody(CSV_SINK, ExchangePattern.InOut, message);
    }
}
