package com.apd.phoenix.service.integration.routes;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class CatalogRouteBuilderIT {

    private CatalogRouteBuilder route;

    private CamelContext camelContext;

    private ProducerTemplate producer;

    private static final String source = "file:/home/jeckstei/Documents/projects/APD/catalog/input/?maxMessagesPerPoll=1&move=.processed";

    private static final String output = "file:/home/jeckstei/Documents/projects/APD/catalog/output?fileExist=Append";

    private static final String processingEndpoint = "stream:out";

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        route = new CatalogRouteBuilder();

        route.setSource(source);
        route.setOutput(output);
        route.setProcessingEndpoint(processingEndpoint);
        camelContext.addRoutes(route);
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void catalogProcessing() throws Exception {
        camelContext.start();

        Thread.sleep(300000000);
    }
}
