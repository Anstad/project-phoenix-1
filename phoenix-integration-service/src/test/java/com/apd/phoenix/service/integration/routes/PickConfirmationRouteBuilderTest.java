package com.apd.phoenix.service.integration.routes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

public class PickConfirmationRouteBuilderTest {

    private static final String source = "direct:source";

    private static final String endpoint = "mock:endpoint";

    private static final String endpointDlq = "mock:endpointDlq";

    private MockEndpoint mockEndpoint;

    private MockEndpoint mockEndpointDlq;

    private CamelContext camelContext;

    private ProducerTemplate producer;

    private PickConfirmationRouteBuilder route;

    @Before
    public void before() throws Exception {
        camelContext = new DefaultCamelContext();
        producer = camelContext.createProducerTemplate();
        route = new PickConfirmationRouteBuilder();
        route.setSource(source);
        route.setEndpoint(endpoint);
        route.setEndpointDlq(endpointDlq);
        mockEndpoint = camelContext.getEndpoint(endpoint, MockEndpoint.class);
        mockEndpointDlq = camelContext.getEndpoint(endpointDlq, MockEndpoint.class);
        camelContext.addRoutes(route);
        camelContext.start();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void sampleTest() throws InterruptedException {
        List<PickDto> pickList = generatePickDtos();

        mockEndpoint.setExpectedMessageCount(1);
        mockEndpointDlq.setExpectedMessageCount(0);
        producer.sendBody(source, pickList);
        mockEndpoint.setResultWaitTime(6000);
        mockEndpointDlq.setResultWaitTime(6000);
        mockEndpoint.assertIsSatisfied();
        mockEndpointDlq.assertIsSatisfied();
        List<Exchange> results = mockEndpoint.getReceivedExchanges();
        Assert.assertTrue(((String) results.get(0).getIn().getBody()).contains("S4321"));
    }

    private List<PickDto> generatePickDtos() {
		List<PickDto> pickList = new ArrayList<>();
		//pick dto 1
		PickDto pickDto1 = new PickDto();
		pickDto1.setApdOrderNumber("APD01");
		pickDto1.setCustomerPoNumber("APD01x");
		pickDto1.setInventoryId("S1234");
		LineItemDto lineItemDto1 = new LineItemDto();
		lineItemDto1.setOrder(new PurchaseOrderDto());
		lineItemDto1.getOrder().setOrderDate(new Date());
		lineItemDto1.getOrder().setAccount(new AccountDto());
		lineItemDto1.getOrder().getAccount().setSolomonCustomerID("ABC123");
		pickDto1.setLineItem(lineItemDto1);
		pickDto1.setLineReference("1");
		pickDto1.setOrderNumber("1234");
		pickDto1.setQtyBo(new BigInteger("1"));
		pickDto1.setQtyToPick(new BigInteger("1"));
		pickDto1.setShipped(true);
		pickDto1.setShipperId("S4321");
		pickList.add(pickDto1);
		//pick dto 2
		PickDto pickDto2 = new PickDto();
		pickDto2.setApdOrderNumber("APD02");
		pickDto2.setCustomerPoNumber("APD01x");
		pickDto2.setInventoryId("S1234");
		LineItemDto lineItemDto2 = new LineItemDto();
		lineItemDto2.setOrder(new PurchaseOrderDto());
		lineItemDto2.getOrder().setOrderDate(new Date());
		lineItemDto2.getOrder().setAccount(new AccountDto());
		lineItemDto2.getOrder().getAccount().setSolomonCustomerID("ABC123");
		pickDto2.setLineItem(lineItemDto2);
		pickDto2.setLineReference("1");
		pickDto2.setOrderNumber("1234");
		pickDto2.setQtyBo(new BigInteger("1"));
		pickDto2.setQtyToPick(new BigInteger("1"));
		pickDto2.setShipped(true);
		pickDto2.setShipperId("S4321");
		pickList.add(pickDto2);
		return pickList;
	}
}
