/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author nreidelb
 */
public class EDI810CreditIT {

    /*
     * To change this template, choose Tools | Templates
     * and open the template in the editor.
     */

    private CamelContext camelContext;

    private String usscoInvoiceSource = "direct:usscoInvoiceSource";

    private String usscoInvoiceLogDLQ = "mock:usscoInvoiceLogDLQ";
    private MockEndpoint mockUsscoInvoiceLogDLQ;

    private String outputDLQ = "mock:outputDLQ";
    private MockEndpoint mockOutputDLQ;

    private String logOutput = "mock:logOutput";
    private MockEndpoint mockLogOutput;

    private String usscoInvoiceSink = "http://localhost:8080/rest/workflow/order/creditInvoice";

    EDI810InboundRouteBuilder builder;
    private MockEndpoint mockUsscoOrderAckSink;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI810InboundRouteBuilder();
        builder.setLogOutputDLQ(usscoInvoiceLogDLQ);
        builder.setOutput(usscoInvoiceSink);
        builder.setSource(usscoInvoiceSource);
        builder.setInvoiceLogOutput(logOutput);
        builder.setOutputDLQ(outputDLQ);
        builder.setCreditOutput(usscoInvoiceSink);
        mockUsscoInvoiceLogDLQ = camelContext.getEndpoint(usscoInvoiceLogDLQ, MockEndpoint.class);
        mockOutputDLQ = camelContext.getEndpoint(outputDLQ, MockEndpoint.class);
        mockLogOutput = camelContext.getEndpoint(logOutput, MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    /**
     @Test
     public void unmarshal810Test() throws Exception {
     sendInput();
     List<Exchange> results = usscoInvoiceSink.getReceivedExchanges();
     Exchange result = results.get(0);

     Invoice invoice = result.getIn().getBody(Invoice.class);
     Assert.assertNotNull(invoice);
     Assert.assertTrue(true);
     }**/

    @Test
    public void dto810Test() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample810Credit.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoInvoiceSource, ExchangePattern.InOut, ediMessage);

    }

}
