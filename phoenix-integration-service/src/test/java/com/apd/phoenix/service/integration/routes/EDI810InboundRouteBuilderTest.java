/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author nreidelb
 */
public class EDI810InboundRouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderTest.class);

    private CamelContext camelContext;

    private String usscoInvoiceSource = "direct:usscoInvoiceSource";

    private String usscoInvoiceLogDLQ = "mock:usscoInvoiceLogDLQ";
    private MockEndpoint mockUsscoInvoiceLogDLQ;

    private String outputDLQ = "mock:outputDLQ";
    private MockEndpoint mockOutputDLQ;

    private String logOutput = "mock:logOutput";
    private MockEndpoint mockLogOutput;

    private String usscoInvoiceSink = "mock:usscoOrderAckSink";

    private String unused = "direct:unused";

    EDI810InboundRouteBuilder builder;
    private MockEndpoint mockUsscoOrderAckSink;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI810InboundRouteBuilder();
        builder.setLogOutputDLQ(usscoInvoiceLogDLQ);
        builder.setOutput(usscoInvoiceSink);
        builder.setSource(usscoInvoiceSource);
        builder.setInvoiceLogOutput(logOutput);
        builder.setOutputDLQ(outputDLQ);
        builder.setCreditOutput(unused);

        mockUsscoOrderAckSink = camelContext.getEndpoint(usscoInvoiceSink, MockEndpoint.class);
        mockUsscoInvoiceLogDLQ = camelContext.getEndpoint(usscoInvoiceLogDLQ, MockEndpoint.class);
        mockOutputDLQ = camelContext.getEndpoint(outputDLQ, MockEndpoint.class);
        mockLogOutput = camelContext.getEndpoint(logOutput, MockEndpoint.class);

        producer = camelContext.createProducerTemplate();
    }

    /**
     @Test
     public void unmarshal810Test() throws Exception {
     sendInput();
     List<Exchange> results = usscoInvoiceSink.getReceivedExchanges();
     Exchange result = results.get(0);

     Invoice invoice = result.getIn().getBody(Invoice.class);
     Assert.assertNotNull(invoice);
     Assert.assertTrue(true);
     }**/

    @Ignore
    @Test
    public void dto810Test() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        //        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample810.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoInvoiceSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();
        List<Exchange> exchanges = mockUsscoOrderAckSink.getExchanges();

    }

    @Test
    @Ignore
    public void manyItems() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        //       mockUsscoOrderAckSink.setExpectedMessageCount(1);
        mockOutputDLQ.setExpectedMessageCount(0);
        mockUsscoInvoiceLogDLQ.setExpectedMessageCount(0);
        //        mockLogOutput.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-810/sample-1.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoInvoiceSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockOutputDLQ.setResultWaitTime(2000);
        mockUsscoInvoiceLogDLQ.setResultWaitTime(2000);
        mockLogOutput.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();
        mockOutputDLQ.assertIsSatisfied();
        mockUsscoInvoiceLogDLQ.assertIsSatisfied();
        mockLogOutput.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(1, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        VendorInvoiceDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), VendorInvoiceDto.class);
        for (LineItemDto lineItem : dto.getActualItems()) {
            LOG.info("Sku: {}", lineItem.getCustomerSku().getValue());

        }
    }

}
