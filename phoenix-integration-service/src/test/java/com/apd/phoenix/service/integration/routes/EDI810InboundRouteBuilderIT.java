/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import java.io.InputStream;
import java.io.StringWriter;
import org.apache.camel.CamelContext;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class EDI810InboundRouteBuilderIT {

    private static final Logger LOG = LoggerFactory.getLogger(UsscoEDI855RouteBuilderTest.class);

    private CamelContext camelContext;

    private String usscoInvoiceSource = "direct:usscoInvoiceSource";

    private String usscoInvoiceSink = "http://localhost:8080/rest/workflow/invoice/create";

    private String usscoInvoiceLogSource = "direct:usscoInvoiceLogSource";

    private String usscoInvoiceLogSink = "http://localhost:8080/rest/messages/";

    private String usscoInvoiceEdiSource = "direct:usscoInvoiceFormattedSource";

    private String usscoInvoiceLogDLQ = "direct:usscoInvoiceLogDLQ";

    private String outputDLQ = "direct:outputDLQ";

    private String unused = "direct:unused";

    EDI810InboundRouteBuilder builder;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI810InboundRouteBuilder();
        builder.setUsscoInvoiceEDISource(usscoInvoiceEdiSource);
        builder.setLogOutputDLQ(usscoInvoiceLogDLQ);
        builder.setInvoiceLogOutput(usscoInvoiceLogSink);
        builder.setInvoiceLogSource(usscoInvoiceLogSource);
        builder.setLogOutputURI(usscoInvoiceLogSink);
        builder.setOutput(usscoInvoiceSink);
        builder.setSource(usscoInvoiceSource);
        builder.setOutputDLQ(outputDLQ);
        builder.setCreditOutput(unused);

        producer = camelContext.createProducerTemplate();
    }

    @Test
    public void createInvoiceTest() throws Exception {

        camelContext.addRoutes(builder);
        camelContext.start();

        //        mockUsscoInvoiceAckSink.setExpectedMessageCount(0);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample810Many.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(usscoInvoiceSource, ExchangePattern.InOut, ediMessage);

    }
}
