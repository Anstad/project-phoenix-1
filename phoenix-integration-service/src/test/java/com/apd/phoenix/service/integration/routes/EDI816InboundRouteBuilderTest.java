package com.apd.phoenix.service.integration.routes;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;
import javax.edi.model.x12.edi816.OrgRelationships;
import junit.framework.Assert;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.AddressUpdateDto;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EDI816InboundRouteBuilderTest {

    private static final Logger LOG = LoggerFactory.getLogger(EDI816InboundRouteBuilderTest.class);

    private CamelContext camelContext;

    private String orgSource = "direct:orgSource";

    private String orgLogSource = "direct:orgLogSource";

    private String orgLogDLQ = "direct:orgLogDLQ";

    private String outputDLQ = "direct:outputDLQ";

    private String orgSink = "mock:usscoOrderAckSink";

    private List<Exchange> exchanges;

    EDI816InboundRouteBuilder builder;
    private MockEndpoint mockUsscoOrderAckSink;
    private ProducerTemplate producer;

    @Before
    public void init() throws Exception {
        camelContext = new DefaultCamelContext();
        builder = new EDI816InboundRouteBuilder();
        builder.setLogOutputDLQ(orgLogDLQ);
        builder.setOrgLogSource(orgLogSource);
        builder.setOutput(orgSink);
        builder.setSource(orgSource);
        builder.setOrgLogOutput(orgLogSource);
        builder.setOutputDLQ(outputDLQ);
        builder.setLogOutputURI(outputDLQ);

        mockUsscoOrderAckSink = camelContext.getEndpoint(orgSink, MockEndpoint.class);
        producer = camelContext.createProducerTemplate();
    }

    @After
    public void after() throws Exception {
        camelContext.stop();
    }

    @Ignore
    @Test
    public void dto810Test() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("sample810.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(orgSource, ExchangePattern.InOut, ediMessage);

        mockUsscoOrderAckSink.setResultWaitTime(2000);
        mockUsscoOrderAckSink.assertIsSatisfied();
        List<Exchange> exchanges = mockUsscoOrderAckSink.getExchanges();

    }

    @Test
    public void basicCamelTest() throws Exception {
        camelContext.addRoutes(builder);
        camelContext.start();

        mockUsscoOrderAckSink.setExpectedMessageCount(1);
        //InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-816/Walmart_816_edit_this_file_short.txt");
        InputStream ips = this.getClass().getClassLoader().getResourceAsStream("edi-816/Walmart_816.txt");

        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer);
        String ediMessage = writer.toString();

        producer.sendBody(orgSource, ExchangePattern.InOut, ediMessage);
        mockUsscoOrderAckSink.setResultWaitTime(2000);
        // mockUsscoOrderAckSink.assertIsSatisfied();

        List<Exchange> exchanges = mockUsscoOrderAckSink.getReceivedExchanges();
        Assert.assertEquals(2, exchanges.size());
        Exchange exchange = exchanges.get(0);

        ObjectMapper mapper = new ObjectMapper();
        AddressUpdateDto dto = mapper.readValue((byte[]) exchange.getIn().getBody(), AddressUpdateDto.class);
        List<AddressDto> addresses = dto.getAddresses();
        for (AddressDto address : addresses) {
            String city = address.getCity();
            String zip = address.getZip();
            Assert.assertNotNull(city);
            if ("NAPERVILLE".equals(city)) {
                LOG.debug("Correct city for assertion " + city);
                Assert.assertEquals("NAPERVILLE", city);
                Assert.assertEquals("60540", zip);
            }
        }

    }
}
