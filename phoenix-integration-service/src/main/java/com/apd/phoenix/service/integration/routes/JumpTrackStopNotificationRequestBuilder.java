/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.manifest.processor.JumptrackStopNoticeProcessor;
import com.apd.phoenix.service.integration.manifest.processor.JumptrackStopNoticeTranslator;

/**
 *
 * @author rhc
 */
public class JumpTrackStopNotificationRequestBuilder extends RouteBuilder {

    private String source;
    private String dlq;
    private String jumptrackEndpoint;

    @Override
    public void configure() throws Exception {

        JaxbDataFormat requestFormat = new JaxbDataFormat("com.apd.phoenix.service.dataformat.xml.request");

        JaxbDataFormat responseFormat = new JaxbDataFormat(
                com.apd.phoenix.service.dataformat.xml.stopnotification.Response.class.getPackage().getName());

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in jumptrack stop request processing: ${exception.stacktrace}");

        from(source)
        //Translate to xml dto
                .bean(JumptrackStopNoticeTranslator.class, "translateRequestDtoToXml")
                //Marshal to xml
                .marshal(requestFormat)
                //Log
                .log(LoggingLevel.INFO, "${body}")
                //Send to jumptrack endpoint
                .setHeader(Exchange.HTTP_METHOD, constant("POST")).setHeader(Exchange.CONTENT_TYPE,
                        constant("text/html")).to(jumptrackEndpoint)
                //Unmarshall        
                .unmarshal(responseFormat)
                //Process response
                .bean(JumptrackStopNoticeProcessor.class, "processStopNoticeResponse");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDlq() {
        return dlq;
    }

    public void setDlq(String dlq) {
        this.dlq = dlq;
    }

    public String getJumptrackEndpoint() {
        return jumptrackEndpoint;
    }

    public void setJumptrackEndpoint(String jumptrackEndpoint) {
        this.jumptrackEndpoint = jumptrackEndpoint;
    }

}
