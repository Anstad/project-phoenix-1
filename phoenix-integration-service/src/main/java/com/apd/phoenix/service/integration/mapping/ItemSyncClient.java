package com.apd.phoenix.service.integration.mapping;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.cxf.binding.BindingFactoryManager;
import org.apache.cxf.jaxrs.JAXRSBindingFactory;
import org.apache.cxf.jaxrs.client.JAXRSClientFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.rest.cxf.ItemSyncEndpoint;

public class ItemSyncClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemSyncClient.class);

    public static String updateItemData(MultivaluedMap<String, String> itemData) {

        JAXRSClientFactoryBean sf = new JAXRSClientFactoryBean();
        sf.setResourceClass(ItemSyncEndpoint.class);
        sf.setAddress("http://localhost:8080/administration/rest/items/update");
        BindingFactoryManager manager = sf.getBus().getExtension(BindingFactoryManager.class);
        JAXRSBindingFactory factory = new JAXRSBindingFactory();
        factory.setBus(sf.getBus());
        manager.registerBindingFactory(JAXRSBindingFactory.JAXRS_BINDING_ID, factory);
        
        // Catalog Name
        List<String> catalogName = new ArrayList<>();
        catalogName.add("USSCO Vendor Catalog");
        itemData.put("catalogName", catalogName);
        
        // Vendor Name
        List<String> vendorName = new ArrayList<>();
        vendorName.add("USSCO");
        itemData.put(ItemBp.VENDOR_HEADER, vendorName);

        WebClient wc = sf.createWebClient().type("application/x-www-form-urlencoded")
        		.accept("application/xml","application/json");
        LOGGER.info(wc.getCurrentURI().toString());

        Response response = wc.post(itemData);
        LOGGER.info(response.toString());

        return "success";
    }
}
