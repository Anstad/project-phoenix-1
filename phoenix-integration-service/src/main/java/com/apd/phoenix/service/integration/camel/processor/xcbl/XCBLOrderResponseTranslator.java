package com.apd.phoenix.service.integration.camel.processor.xcbl;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Agency;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.AgencyCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Country;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.CountryCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.DetailResponseCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Identifier;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ItemDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ListOfOrderResponseItemDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.NameAddress;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Order;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderParty;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponse;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseDocTypeCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseItemDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseNumber;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseSummary;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Party;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Purpose;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.PurposeCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Reference;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Region;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.RegionCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ResponseType;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.ResponseTypeCode;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader.BuyerParty;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader.OrderHeaderChanges;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader.OrderReference;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader.OriginalOrderHeader;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseHeader.SellerParty;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseItemDetail.ItemDetailChanges;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OrderResponseItemDetail.OriginalItemDetail;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Party.OrderContact;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.Party.PartyID;
import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.integration.exception.MessageTranslationException;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;

public class XCBLOrderResponseTranslator implements MessageTranslator<POAcknowledgementDto, OrderResponse> {

    private static final Logger LOG = LoggerFactory.getLogger(XCBLOrderResponseTranslator.class);

    public static final String DEFAULT_CURRENCY_CODE = "USD";
    public static final String DEFAULT_LANGUAGE_CODE = "en";
    public static final String DEFAULT_SELLER_PARTY_ID_AGENCY = "SSNID";
    public static final String DEFAULT_BUYER_PARTY_ID_AGENCY = "SSNID";
    public static final String DEFAULT_SELLER_NAME = "American Product Distributors Inc";
    public static final String DEFAULT_SELLER_STREET = "8350 Arrowridge Blvd";
    public static final String DEFAULT_SELLER_ZIP = "28273";
    public static final String DEFAULT_SELLER_CITY = "Charlotte";
    public static final String DEFAULT_SELLER_STATE = "NC";
    public static final String DEFAULT_SELLER_DISTRICT = "";
    public static final String DEFAULT_BUYER_DISTRICT = "";
    public static final String DEFAULT_SELLER_COUNTRY = "US";

    public static final String OSI_DATE_FORMAT = "yyyyMMdd'T'HH:mm:ssXXX";

    @Override
    public POAcknowledgementDto toCanonical(OrderResponse xcblOrderResponse) {
        LOG.info("body: {}", xcblOrderResponse);
        throw new UnsupportedOperationException();
    }

    @Override
    public OrderResponse fromCanonical(POAcknowledgementDto poAckDto) {
        OrderResponse xcblOrderResponse = new OrderResponse();
        return xcblOrderResponse;
    }

    public OrderResponse generateOrderResponse(Order order, POAcknowledgementDto poAck)
            throws MessageTranslationException {
        OrderResponse response = new OrderResponse();
        response.setOrderResponseHeader(generateOrderResponseHeader(order, poAck));
        response.setOrderResponseDetail(generateOrderResponseDetail(order, poAck));
        response.setOrderResponseSummary(generateOrderResponseSummary(order, poAck));
        return response;
    }

    public OrderResponseHeader generateOrderResponseHeader(Order order, POAcknowledgementDto poAck)
            throws MessageTranslationException {
        OrderResponseHeader header = new OrderResponseHeader();
        header.setOrderResponseNumber(generateOrderResponseNumber(order, poAck));
        SimpleDateFormat dateFormat = new SimpleDateFormat(OSI_DATE_FORMAT);
        header.setOrderResponseIssueDate(dateFormat.format(new Date()));
        header.setOrderResponseDocTypeCoded(OrderResponseDocTypeCode.ORDER_RESPONSE);
        header.setOrderReference(generateOrderReference(order, poAck));
        header.setSellerParty(generateSellerParty(order, poAck));
        header.setBuyerParty(generateBuyerParty(order, poAck));
        Purpose purpose = new Purpose();
        purpose.setPurposeCoded(PurposeCode.ORIGINAL);
        header.setPurpose(purpose);
        ResponseType responseType = new ResponseType();
        responseType.setResponseTypeCoded(ResponseTypeCode.ACCEPTED);
        header.setResponseType(responseType);
        OriginalOrderHeader originalOrderHeader = new OriginalOrderHeader();
        originalOrderHeader.setOrderHeader(order.getOrderHeader());
        header.setOriginalOrderHeader(originalOrderHeader);
        OrderHeaderChanges orderHeaderChanges = new OrderHeaderChanges();
        orderHeaderChanges.setOrderHeader(order.getOrderHeader());
        header.setOrderHeaderChanges(orderHeaderChanges);
        return header;
    }

    public OrderResponseNumber generateOrderResponseNumber(Order order, POAcknowledgementDto poAck)
            throws MessageTranslationException {
        OrderResponseNumber number = new OrderResponseNumber();

        String buyerOrderNumber = getBuyerOrderNumber(poAck);
        if (StringUtils.isBlank(buyerOrderNumber)) {
            throw new MessageTranslationException("Unable to find buyer order response number on the PO ack DTO.");
        }
        else {
            number.setBuyerOrderResponseNumber(buyerOrderNumber);
        }

        PoNumberDto apdPO = poAck.getPurchaseOrderDto().retrieveApdPoNumber();
        if (apdPO != null) {
            number.setSellerOrderResponseNumber(apdPO.getValue());
        }

        return number;
    }

    public String getBuyerOrderNumber(POAcknowledgementDto poAck) {

        PoNumberDto customerPO = poAck.getPurchaseOrderDto().retrieveCustomerPoNumber();
        PoNumberDto blanketPO = poAck.getPurchaseOrderDto().retrieveBlanketPoNumber();
        PoNumberDto apdPO = poAck.getPurchaseOrderDto().retrieveApdPoNumber();

        if (customerPO != null) {
            return customerPO.getValue();
        }
        else if (blanketPO != null) {
            return blanketPO.getValue();
        }
        else if (apdPO != null) {
            return apdPO.getValue();
        }
        else {
            return null;
        }
    }

    public OrderReference generateOrderReference(Order order, POAcknowledgementDto poAck)
            throws MessageTranslationException {
        String buyerOrderNumber = getBuyerOrderNumber(poAck);
        if (StringUtils.isBlank(buyerOrderNumber)) {
            throw new MessageTranslationException("Unable to find buyer order response number on the PO ack DTO.");
        }

        OrderReference orderRef = new OrderReference();
        Reference ref = new Reference();
        ref.setRefNum(buyerOrderNumber);
        ref.setRefDate(order.getOrderHeader().getOrderIssueDate());
        orderRef.setReference(ref);
        return orderRef;
    }

    public SellerParty generateSellerParty(Order order, POAcknowledgementDto poAck) throws MessageTranslationException {
        SellerParty sellerParty = new SellerParty();
        Party party = new Party();
        PartyID partyId = new PartyID();
        Identifier identifier = new Identifier();
        Agency agency = new Agency();
        agency.setAgencyCoded(AgencyCode.OTHER);
        agency.setAgencyCodedOther(DEFAULT_SELLER_PARTY_ID_AGENCY);
        identifier.setAgency(agency);
        // same as order.shiptoparty/finalrecipient.party..ssnid
        identifier.setIdent(order.getOrderHeader().getOrderParty().getShipToParty().getParty().getPartyID()
                .getIdentifier().getIdent());
        partyId.setIdentifier(identifier);
        party.setPartyID(partyId);
        NameAddress nameAddress = new NameAddress();
        nameAddress.setName1(DEFAULT_SELLER_NAME);
        nameAddress.setStreet(DEFAULT_SELLER_STREET);
        nameAddress.setPostalCode(DEFAULT_SELLER_ZIP);
        nameAddress.setCity(DEFAULT_SELLER_CITY);
        Region region = new Region();
        region.setRegionCoded(RegionCode.OTHER);
        region.setRegionCodedOther(DEFAULT_SELLER_STATE);
        nameAddress.setRegion(region);
        nameAddress.setDistrict(DEFAULT_SELLER_DISTRICT);
        Country country = new Country();
        country.setCountryCoded(CountryCode.US);
        nameAddress.setCountry(country);
        party.setNameAddress(nameAddress);
        sellerParty.setParty(party);
        return sellerParty;
    }

    public BuyerParty generateBuyerParty(Order order, POAcknowledgementDto poAck) throws MessageTranslationException {
        BuyerParty buyerParty = new BuyerParty();
        Party party = new Party();
        PartyID partyId = new PartyID();
        Identifier identifier = new Identifier();
        Agency agency = new Agency();
        agency.setAgencyCoded(AgencyCode.OTHER);
        agency.setAgencyCodedOther(DEFAULT_BUYER_PARTY_ID_AGENCY);
        identifier.setAgency(agency);
        identifier.setIdent(getBuyerPartyId(order.getOrderHeader().getOrderParty().getBuyerParty(), poAck));
        partyId.setIdentifier(identifier);
        party.setPartyID(partyId);
        AddressDto billToAddress = poAck.getPurchaseOrderDto().getPaymentInformationDto().getBillToAddress();
        NameAddress nameAddress = new NameAddress();
        if (StringUtils.isNotEmpty(billToAddress.getName())) {
            nameAddress.setName1(billToAddress.getName());
        }
        else {
            if (poAck.getPurchaseOrderDto().getAccount() != null
                    && StringUtils.isNotEmpty(poAck.getPurchaseOrderDto().getAccount().getName())) {
                nameAddress.setName1(poAck.getPurchaseOrderDto().getAccount().getName());
            }
        }
        nameAddress.setStreet(billToAddress.getLine1());
        nameAddress.setPostalCode(billToAddress.getZip());
        nameAddress.setCity(billToAddress.getCity());
        Region region = new Region();
        region.setRegionCoded(RegionCode.OTHER);
        region.setRegionCodedOther(billToAddress.getState());
        nameAddress.setRegion(region);
        nameAddress.setDistrict("");
        Country country = new Country();
        country.setCountryCoded(CountryCode.US);
        nameAddress.setCountry(country);
        party.setNameAddress(nameAddress);
        OrderContact orderContact = order.getOrderHeader().getOrderParty().getBuyerParty().getParty().getOrderContact();
        party.setOrderContact(orderContact);
        buyerParty.setParty(party);
        return buyerParty;
    }

    public String getBuyerPartyId(OrderParty.BuyerParty buyerParty, POAcknowledgementDto poAck) {
        // First look for more specific party id matching BorgId agency code.
        // If none found, use the top level party id.
        String partyId = buyerParty.getParty().getPartyID().getIdentifier().getIdent();
        if (buyerParty.getParty().getListOfIdentifier() != null) {
            for (Identifier identifier : buyerParty.getParty().getListOfIdentifier().getIdentifier()) {
                if (StringUtils.equalsIgnoreCase(identifier.getAgency().getAgencyCodedOther(), "BorgID")) {
                    partyId = identifier.getIdent();
                }
            }
        }
        return partyId;
    }

    public OrderResponseDetail generateOrderResponseDetail(Order order, POAcknowledgementDto poAck) {
        OrderResponseDetail detail = new OrderResponseDetail();
        ListOfOrderResponseItemDetail itemDetailList = new ListOfOrderResponseItemDetail();

        for (ItemDetail itemDetail : order.getOrderDetail().getListOfItemDetail().getItemDetail()) {

            OrderResponseItemDetail responseItemDetail = new OrderResponseItemDetail();
            responseItemDetail.setItemDetailResponseCoded(DetailResponseCode.ITEM_ACCEPTED);
            responseItemDetail.setItemDetailResponseCodedOther("AcceptedAsIs");
            OriginalItemDetail originalItemDetail = new OriginalItemDetail();
            originalItemDetail.setItemDetail(itemDetail);
            responseItemDetail.setOriginalItemDetail(originalItemDetail);
            ItemDetailChanges itemDetailChanges = new ItemDetailChanges();
            itemDetailChanges.setItemDetail(itemDetail);
            responseItemDetail.setItemDetailChanges(itemDetailChanges);
            itemDetailList.getOrderResponseItemDetail().add(responseItemDetail);
        }

        detail.setListOfOrderResponseItemDetail(itemDetailList);
        return detail;
    }

    public OrderResponseSummary generateOrderResponseSummary(Order order, POAcknowledgementDto poAck) {
        return null;
    }
}
