package com.apd.phoenix.service.integration.tax.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(length = 183, paddingChar = ' ')
public class TaxRateUpdate implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @DataField(pos = 1, length = 182)
    public String record;

    @DataField(pos = 183, length = 1)
    public String action;

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
