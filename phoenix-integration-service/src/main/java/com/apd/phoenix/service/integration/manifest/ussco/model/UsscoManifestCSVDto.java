package com.apd.phoenix.service.integration.manifest.ussco.model;

import java.io.Serializable;
import java.util.Date;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", skipFirstLine = true)
public class UsscoManifestCSVDto implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7582592632253062375L;

    @DataField(pos = 1, trim = true)
    private String tradingPartnerID;

    @DataField(pos = 2, trim = true)
    private String documentID;

    @DataField(pos = 3, trim = true)
    private String selectionTypeCode;

    @DataField(pos = 4, trim = true)
    private String dateTimeSent;

    @DataField(pos = 5, trim = true)
    private String facilityNumber;

    @DataField(pos = 6, trim = true)
    private String facilityAbbreviation;

    @DataField(pos = 7, trim = true)
    private String facilityName;

    @DataField(pos = 8, trim = true)
    private String facilityAddress1;

    @DataField(pos = 9, trim = true)
    private String facilityAddress2;

    @DataField(pos = 10, trim = true)
    private String facilityCity;

    @DataField(pos = 11, trim = true)
    private String facilityState;

    @DataField(pos = 12, trim = true)
    private String facilityPostalCode;

    @DataField(pos = 13, trim = true)
    private String orderNumber;

    @DataField(pos = 14, trim = true)
    private String subOrderNumber;

    @DataField(pos = 15, trim = true)
    private String carrierCode;

    @DataField(pos = 16, trim = true)
    private String shipTruckCode;

    @DataField(pos = 17, trim = true)
    private String shipTruckDockId;

    @DataField(pos = 18, trim = true)
    private String truckCodeDescription;

    @DataField(pos = 19, trim = true)
    private String customerNumber;

    @DataField(pos = 20, trim = true)
    private String customerName;

    @DataField(pos = 21, trim = true)
    private String customerBarCode;

    @DataField(pos = 22, trim = true)
    private String customerReferenceData;

    @DataField(pos = 23, trim = true)
    private String customerRouteDescription;

    @DataField(pos = 24, trim = true)
    private String customerRouteData;

    @DataField(pos = 25, trim = true)
    private String customerPurchaseOrderNumber;

    @DataField(pos = 26, trim = true)
    private String deliveryTypeCode;

    @DataField(pos = 27, trim = true)
    private String deliveryMethodCode;

    @DataField(pos = 28, trim = true)
    private String customerAddress1;

    @DataField(pos = 29, trim = true)
    private String customerAddress2;

    @DataField(pos = 30, trim = true)
    private String customerCity;

    @DataField(pos = 31, trim = true)
    private String customerState;

    @DataField(pos = 32, trim = true)
    private String customerPostalCode;

    @DataField(pos = 33, trim = true)
    private String endConsumerPurchaseOrderData;

    @DataField(pos = 34, trim = true)
    private String endConsumerName;

    @DataField(pos = 35, trim = true)
    private String endConsumerAddress1;

    @DataField(pos = 36, trim = true)
    private String endConsumerAddress2;

    @DataField(pos = 37, trim = true)
    private String endConsumerAddress3;

    @DataField(pos = 38, trim = true)
    private String endConsumerCity;

    @DataField(pos = 39, trim = true)
    private String endConsumerState;

    @DataField(pos = 40, trim = true)
    private String endConsumerPostalCode;

    @DataField(pos = 41, trim = true)
    private String fillFacilityNumber;

    @DataField(pos = 42, trim = true)
    private String hazardousMaterialIndicator;

    @DataField(pos = 43, trim = true)
    private String primaryOrderNumber;

    @DataField(pos = 44, trim = true)
    //12/12/2013
    private String dateProcessed;

    @DataField(pos = 45, trim = true)
    private String specialInstructions1;

    @DataField(pos = 46, trim = true)
    private String specialInstructions2;

    @DataField(pos = 47, trim = true)
    private String dealerInformation1;

    @DataField(pos = 48, trim = true)
    private String dealerInformation2;

    @DataField(pos = 49, trim = true)
    private String dealerInformation3;

    @DataField(pos = 50, trim = true)
    private String dealerInformation4;

    @DataField(pos = 51, trim = true)
    private String dealerInformation5;

    @DataField(pos = 52, trim = true)
    private String dealerInformation6;

    @DataField(pos = 53, trim = true)
    private String shippingInformation1;

    @DataField(pos = 54, trim = true)
    private String shippingInformation2;

    @DataField(pos = 55, trim = true)
    private String shippingInformation3;

    @DataField(pos = 56, trim = true)
    private String shippingInformation4;

    @DataField(pos = 57, trim = true)
    private String shippingInformation5;

    @DataField(pos = 58, trim = true)
    private String shippingInformation6;

    @DataField(pos = 59, trim = true)
    private String cartonID;

    @DataField(pos = 60, trim = true)
    private String bulkCartonIndicator;

    @DataField(pos = 61, trim = true)
    private String cartonWeight;

    @DataField(pos = 62, trim = true)
    private String itemPrefix;

    @DataField(pos = 63, trim = true)
    private String itemStock;

    @DataField(pos = 64, trim = true)
    private String scanDateTime;

    @DataField(pos = 65, trim = true)
    //12/12/2013 12:24:57 PM
    private String manifestDateTime;

    @DataField(pos = 66, trim = true)
    private String cartonBarcode;

    public String getTradingPartnerID() {
        return tradingPartnerID;
    }

    public void setTradingPartnerID(String tradingPartnerID) {
        this.tradingPartnerID = tradingPartnerID;
    }

    public String getDocumentID() {
        return documentID;
    }

    public void setDocumentID(String documentID) {
        this.documentID = documentID;
    }

    public String getSelectionTypeCode() {
        return selectionTypeCode;
    }

    public void setSelectionTypeCode(String selectionTypeCode) {
        this.selectionTypeCode = selectionTypeCode;
    }

    public String getFacilityNumber() {
        return facilityNumber;
    }

    public void setFacilityNumber(String facilityNumber) {
        this.facilityNumber = facilityNumber;
    }

    public String getFacilityAbbreviation() {
        return facilityAbbreviation;
    }

    public void setFacilityAbbreviation(String facilityAbbreviation) {
        this.facilityAbbreviation = facilityAbbreviation;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getFacilityAddress1() {
        return facilityAddress1;
    }

    public void setFacilityAddress1(String facilityAddress1) {
        this.facilityAddress1 = facilityAddress1;
    }

    public String getFacilityAddress2() {
        return facilityAddress2;
    }

    public void setFacilityAddress2(String facilityAddress2) {
        this.facilityAddress2 = facilityAddress2;
    }

    public String getFacilityCity() {
        return facilityCity;
    }

    public void setFacilityCity(String facilityCity) {
        this.facilityCity = facilityCity;
    }

    public String getFacilityState() {
        return facilityState;
    }

    public void setFacilityState(String facilityState) {
        this.facilityState = facilityState;
    }

    public String getFacilityPostalCode() {
        return facilityPostalCode;
    }

    public void setFacilityPostalCode(String facilityPostalCode) {
        this.facilityPostalCode = facilityPostalCode;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getSubOrderNumber() {
        return subOrderNumber;
    }

    public void setSubOrderNumber(String subOrderNumber) {
        this.subOrderNumber = subOrderNumber;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getShipTruckCode() {
        return shipTruckCode;
    }

    public void setShipTruckCode(String shipTruckCode) {
        this.shipTruckCode = shipTruckCode;
    }

    public String getShipTruckDockId() {
        return shipTruckDockId;
    }

    public void setShipTruckDockId(String shipTruckDockId) {
        this.shipTruckDockId = shipTruckDockId;
    }

    public String getTruckCodeDescription() {
        return truckCodeDescription;
    }

    public void setTruckCodeDescription(String truckCodeDescription) {
        this.truckCodeDescription = truckCodeDescription;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerBarCode() {
        return customerBarCode;
    }

    public void setCustomerBarCode(String customerBarCode) {
        this.customerBarCode = customerBarCode;
    }

    public String getCustomerReferenceData() {
        return customerReferenceData;
    }

    public void setCustomerReferenceData(String customerReferenceData) {
        this.customerReferenceData = customerReferenceData;
    }

    public String getCustomerRouteDescription() {
        return customerRouteDescription;
    }

    public void setCustomerRouteDescription(String customerRouteDescription) {
        this.customerRouteDescription = customerRouteDescription;
    }

    public String getCustomerRouteData() {
        return customerRouteData;
    }

    public void setCustomerRouteData(String customerRouteData) {
        this.customerRouteData = customerRouteData;
    }

    public String getCustomerPurchaseOrderNumber() {
        return customerPurchaseOrderNumber;
    }

    public void setCustomerPurchaseOrderNumber(String customerPurchaseOrderNumber) {
        this.customerPurchaseOrderNumber = customerPurchaseOrderNumber;
    }

    public String getDeliveryTypeCode() {
        return deliveryTypeCode;
    }

    public void setDeliveryTypeCode(String deliveryTypeCode) {
        this.deliveryTypeCode = deliveryTypeCode;
    }

    public String getDeliveryMethodCode() {
        return deliveryMethodCode;
    }

    public void setDeliveryMethodCode(String deliveryMethodCode) {
        this.deliveryMethodCode = deliveryMethodCode;
    }

    public String getCustomerAddress1() {
        return customerAddress1;
    }

    public void setCustomerAddress1(String customerAddress1) {
        this.customerAddress1 = customerAddress1;
    }

    public String getCustomerAddress2() {
        return customerAddress2;
    }

    public void setCustomerAddress2(String customerAddress2) {
        this.customerAddress2 = customerAddress2;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerState() {
        return customerState;
    }

    public void setCustomerState(String customerState) {
        this.customerState = customerState;
    }

    public String getCustomerPostalCode() {
        return customerPostalCode;
    }

    public void setCustomerPostalCode(String customerPostalCode) {
        this.customerPostalCode = customerPostalCode;
    }

    public String getEndConsumerPurchaseOrderData() {
        return endConsumerPurchaseOrderData;
    }

    public void setEndConsumerPurchaseOrderData(String endConsumerPurchaseOrderData) {
        this.endConsumerPurchaseOrderData = endConsumerPurchaseOrderData;
    }

    public String getEndConsumerName() {
        return endConsumerName;
    }

    public void setEndConsumerName(String endConsumerName) {
        this.endConsumerName = endConsumerName;
    }

    public String getEndConsumerAddress1() {
        return endConsumerAddress1;
    }

    public void setEndConsumerAddress1(String endConsumerAddress1) {
        this.endConsumerAddress1 = endConsumerAddress1;
    }

    public String getEndConsumerAddress2() {
        return endConsumerAddress2;
    }

    public void setEndConsumerAddress2(String endConsumerAddress2) {
        this.endConsumerAddress2 = endConsumerAddress2;
    }

    public String getEndConsumerAddress3() {
        return endConsumerAddress3;
    }

    public void setEndConsumerAddress3(String endConsumerAddress3) {
        this.endConsumerAddress3 = endConsumerAddress3;
    }

    public String getEndConsumerCity() {
        return endConsumerCity;
    }

    public void setEndConsumerCity(String endConsumerCity) {
        this.endConsumerCity = endConsumerCity;
    }

    public String getEndConsumerState() {
        return endConsumerState;
    }

    public void setEndConsumerState(String endConsumerState) {
        this.endConsumerState = endConsumerState;
    }

    public String getEndConsumerPostalCode() {
        return endConsumerPostalCode;
    }

    public void setEndConsumerPostalCode(String endConsumerPostalCode) {
        this.endConsumerPostalCode = endConsumerPostalCode;
    }

    public String getFillFacilityNumber() {
        return fillFacilityNumber;
    }

    public void setFillFacilityNumber(String fillFacilityNumber) {
        this.fillFacilityNumber = fillFacilityNumber;
    }

    public String getHazardousMaterialIndicator() {
        return hazardousMaterialIndicator;
    }

    public void setHazardousMaterialIndicator(String hazardousMaterialIndicator) {
        this.hazardousMaterialIndicator = hazardousMaterialIndicator;
    }

    public String getPrimaryOrderNumber() {
        return primaryOrderNumber;
    }

    public void setPrimaryOrderNumber(String primaryOrderNumber) {
        this.primaryOrderNumber = primaryOrderNumber;
    }

    public String getSpecialInstructions1() {
        return specialInstructions1;
    }

    public void setSpecialInstructions1(String specialInstructions1) {
        this.specialInstructions1 = specialInstructions1;
    }

    public String getSpecialInstructions2() {
        return specialInstructions2;
    }

    public void setSpecialInstructions2(String specialInstructions2) {
        this.specialInstructions2 = specialInstructions2;
    }

    public String getDealerInformation1() {
        return dealerInformation1;
    }

    public void setDealerInformation1(String dealerInformation1) {
        this.dealerInformation1 = dealerInformation1;
    }

    public String getDealerInformation2() {
        return dealerInformation2;
    }

    public void setDealerInformation2(String dealerInformation2) {
        this.dealerInformation2 = dealerInformation2;
    }

    public String getDealerInformation3() {
        return dealerInformation3;
    }

    public void setDealerInformation3(String dealerInformation3) {
        this.dealerInformation3 = dealerInformation3;
    }

    public String getDealerInformation4() {
        return dealerInformation4;
    }

    public void setDealerInformation4(String dealerInformation4) {
        this.dealerInformation4 = dealerInformation4;
    }

    public String getDealerInformation5() {
        return dealerInformation5;
    }

    public void setDealerInformation5(String dealerInformation5) {
        this.dealerInformation5 = dealerInformation5;
    }

    public String getDealerInformation6() {
        return dealerInformation6;
    }

    public void setDealerInformation6(String dealerInformation6) {
        this.dealerInformation6 = dealerInformation6;
    }

    public String getShippingInformation1() {
        return shippingInformation1;
    }

    public void setShippingInformation1(String shippingInformation1) {
        this.shippingInformation1 = shippingInformation1;
    }

    public String getShippingInformation2() {
        return shippingInformation2;
    }

    public void setShippingInformation2(String shippingInformation2) {
        this.shippingInformation2 = shippingInformation2;
    }

    public String getShippingInformation3() {
        return shippingInformation3;
    }

    public void setShippingInformation3(String shippingInformation3) {
        this.shippingInformation3 = shippingInformation3;
    }

    public String getShippingInformation4() {
        return shippingInformation4;
    }

    public void setShippingInformation4(String shippingInformation4) {
        this.shippingInformation4 = shippingInformation4;
    }

    public String getShippingInformation5() {
        return shippingInformation5;
    }

    public void setShippingInformation5(String shippingInformation5) {
        this.shippingInformation5 = shippingInformation5;
    }

    public String getShippingInformation6() {
        return shippingInformation6;
    }

    public void setShippingInformation6(String shippingInformation6) {
        this.shippingInformation6 = shippingInformation6;
    }

    public String getCartonID() {
        return cartonID;
    }

    public void setCartonID(String cartonID) {
        this.cartonID = cartonID;
    }

    public String getBulkCartonIndicator() {
        return bulkCartonIndicator;
    }

    public void setBulkCartonIndicator(String bulkCartonIndicator) {
        this.bulkCartonIndicator = bulkCartonIndicator;
    }

    public String getCartonWeight() {
        return cartonWeight;
    }

    public void setCartonWeight(String cartonWeight) {
        this.cartonWeight = cartonWeight;
    }

    public String getItemPrefix() {
        return itemPrefix;
    }

    public void setItemPrefix(String itemPrefix) {
        this.itemPrefix = itemPrefix;
    }

    public String getItemStock() {
        return itemStock;
    }

    public void setItemStock(String itemStock) {
        this.itemStock = itemStock;
    }

    public String getCartonBarcode() {
        return cartonBarcode;
    }

    public void setCartonBarcode(String cartonBarcode) {
        this.cartonBarcode = cartonBarcode;
    }

    public String getDateTimeSent() {
        return dateTimeSent;
    }

    public void setDateTimeSent(String dateTimeSent) {
        this.dateTimeSent = dateTimeSent;
    }

    public String getDateProcessed() {
        return dateProcessed;
    }

    public void setDateProcessed(String dateProcessed) {
        this.dateProcessed = dateProcessed;
    }

    public String getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(String scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public String getManifestDateTime() {
        return manifestDateTime;
    }

    public void setManifestDateTime(String manifestDateTime) {
        this.manifestDateTime = manifestDateTime;
    }

}
