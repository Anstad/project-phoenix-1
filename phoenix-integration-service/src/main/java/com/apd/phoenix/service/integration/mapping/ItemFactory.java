package com.apd.phoenix.service.integration.mapping;

import com.apd.phoenix.core.StringEscape;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.openapplications.oagis._9.AttachmentBaseType;
import org.openapplications.oagis._9.DescriptionType;
import org.openapplications.oagis._9.PropertyType;
import org.openapplications.oagis._9.SequencedCodeType;
import org.openapplications.oagis._9.SequencedCodesType;
import org.openapplications.oagis._9.SpecificationType;
import com.apd.phoenix.service.business.ItemBp;
import com.ussco.oagis._0.ClassificationType;
import com.ussco.oagis._0.ItemMasterHeaderType;
import com.ussco.oagis._0.ItemMasterType;

public class ItemFactory {

    public static final String USSCO_IMAGE_URL_BASE = "http://content.oppictures.com/Master_Images/Master_Variants/";
    public static final String USSCO_THUMBNAIL_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_100";
    public static final String USSCO_STANDARD_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_240";
    public static final String USSCO_LARGE_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_400";
    public static final String USSCO_ZOOM_IMAGE_URL = USSCO_IMAGE_URL_BASE + "/Variant_1500";

    public static Map fromUsscoItemMaster(ItemMasterType itemMaster) {
        ItemMasterHeaderType header = itemMaster.getItemMasterHeader();
        Map<String, String> itemData = new HashMap<String, String>();
        // Populate item data from itemMaster classifications
        String classifications = "";
        for (ClassificationType classification : header.getClassification()) {
            if (classification.getType().equals("SKU_Group")) {
                for (SequencedCodesType codes : classification.getCodes()) {
                    for (SequencedCodeType code : codes.getCode()) {
                        if (code.getName().equals("SKU_Group_Name")) {
                            itemData.put(ItemBp.NAME, code.getValue());
                        }
                        else if (code.getName().equals("SKU_Group_Id")) {
                            itemData.put(ItemBp.VENDOR_SKUTYPE + ItemBp.SKU_SUFFIX, code.getValue());
                        }
                    }
                }
            }
            else if (classification.getCodes() != null && classification.getCodes().size() > 0
                    && classification.getCodes().get(0).getCode() != null
                    && classification.getCodes().get(0).getCode().size() > 0) {
                String code = classification.getCodes().get(0).getCode().get(0).getValue();
                if (classification.getType().equals("Product_Class_Category")) {
                    itemData.put(ItemBp.CATEGORY, code);
                }
                else if (classification.getType().equals("Assembly_Code")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("Recycle_Indicator")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("EPACPGCompliant_Code")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("UNSPSC")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("MSDS_Indicator")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
                else if (classification.getType().equals("Assembly_Code")) {
                    classifications += "|" + classification.getType() + "=" + code;
                }
            }
        }
        if (StringUtils.isNotBlank(classifications)) {
            // Strip off the initial pipe
            classifications = classifications.substring(1);
            itemData.put(ItemBp.CLASSIFICATIONS, classifications);
        }
        String specifications = "";
        for (SpecificationType specification : header.getSpecification()) {
            for (PropertyType specProperty : specification.getProperty()) {
                if (specProperty.getNameValue() != null) {
                    specifications += "|" + specProperty.getNameValue().getName() + "="
                            + specProperty.getNameValue().getValue();
                }
                if (specProperty.getDescription() != null) {
                    for (DescriptionType description : specProperty.getDescription()) {
                        if (description.getType().equals("Long_Item_Description")) {
                            itemData.put(ItemBp.DESCRIPTION, description.getValue());
                        }
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(specifications)) {
            // Strip off the initial pipe
            specifications = specifications.substring(1);
            itemData.put(ItemBp.SPECIFICATIONS, specifications);
        }
        String itemImages = "";
        if (header.getDrawingAttachment() != null && !header.getDrawingAttachment().isEmpty()) {
            String mainImageFileURL = USSCO_STANDARD_IMAGE_URL
                    + "/"
                    + StringEscape.escapeForUrl(itemMaster.getItemMasterHeader().getDrawingAttachment().get(0)
                            .getFileName().getValue());
            //item.getItemImages()itemMaster.getItemMasterHeader().getDrawingAttachment();
            //item.setName()
            itemImages += mainImageFileURL;
        }

        for (AttachmentBaseType attachment : itemMaster.getItemMasterHeader().getAttachment()) {
            itemImages += "," + USSCO_STANDARD_IMAGE_URL + "/"
                    + StringEscape.escapeForUrl(attachment.getFileName().getValue());
        }
        itemData.put("image URLs", itemImages);
        return itemData;
    }

}