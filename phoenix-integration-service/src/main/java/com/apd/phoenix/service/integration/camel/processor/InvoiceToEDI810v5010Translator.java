package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.edi.model.x12.v5010.edi810.Invoice;
import javax.edi.model.x12.v5010.edi810.segment.Detail;
import javax.edi.model.x12.v5010.edi810.segment.Header;
import javax.edi.model.x12.v5010.edi810.segment.InvoiceBeginningSegment;
import javax.edi.model.x12.v5010.edi810.segment.InvoiceBody;
import javax.edi.model.x12.v5010.edi810.segment.InvoiceItemGroup;
import javax.edi.model.x12.v5010.edi810.segment.PartyIdentificationGroup;
import javax.edi.model.x12.v5010.edi810.segment.Trailer;
import javax.edi.model.x12.v5010.segment.AddressInformation;
import javax.edi.model.x12.v5010.segment.AllowanceChargeOrService;
import javax.edi.model.x12.v5010.segment.BaselineItemData;
import javax.edi.model.x12.v5010.segment.CarrierDetails;
import javax.edi.model.x12.v5010.segment.DateTimeReference;
import javax.edi.model.x12.v5010.segment.FOBRelatedInstruction;
import javax.edi.model.x12.v5010.segment.GeographicLocation;
import javax.edi.model.x12.v5010.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.v5010.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.v5010.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.v5010.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.v5010.segment.InvoiceShipmentSummary;
import javax.edi.model.x12.v5010.segment.ItemPhysicalDetail;
import javax.edi.model.x12.v5010.segment.Name;
import javax.edi.model.x12.v5010.segment.ProductItemDescription;
import javax.edi.model.x12.v5010.segment.ReferenceNumber;
import javax.edi.model.x12.v5010.segment.TaxInformation;
import javax.edi.model.x12.v5010.segment.TermsOfSale;
import javax.edi.model.x12.v5010.segment.TotalMonetaryValueSummary;
import javax.edi.model.x12.v5010.segment.TransactionSetHeader;
import javax.edi.model.x12.v5010.segment.TransactionSetTrailer;
import javax.edi.model.x12.v5010.segment.TransactionTotal;
import org.apache.commons.httpclient.util.DateUtil;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.TermsOfSaleDto;
import java.math.BigDecimal;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.integration.routes.EDI810v5010OutboundRouteBuilder;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.model.dto.ItemDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InvoiceToEDI810v5010Translator {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceToEDI810v5010Translator.class);

    public static final String CHARGE_INDICATOR = "C";
    public static final String FREIGHT_INDICATOR = "D240";
    public static final String NO_AUTHORIZATION_INFORMATION_PRESENT = "00";
    public static final String FUNCTIONAL_ID_CODE = "IN";
    private static final String ACCREDITED_STANDARDS_COMMITTEE_X12 = "X";
    public static final String X12_PROCEDURES_REVIEW_BOARD_97 = "00501";
    public static final String EDI_810_DOCUMENT = "810";
    private static final String NO_SECURITY_INFORMATION_PRESENT = "00";
    private static final String TEN_PADDED_SPACES = "          ";
    public static final String APD_PADDED_15_CHARS = "PHOEN          ";
    public static final String APD_UNPADDED = "PHOEN";
    public static final String SAMPLE_CONTROL_ID = "U";
    public static final String NO_ACKNOWLEDGEMENT_REQUESTED = "0";
    public static final String USPS_SEPARATOR = ">";
    private static final String SHIP_TO_INDENTIFIER_CODE = "ST";
    private static final String BILL_TO_INDENTIFIER_CODE = "BT";
    private static final String SHIP_FROM_INDENTIFIER_CODE = "SU";
    private static final String SHIPPED_DATE_TIME_QUALIFIER = "011";
    private static final String TRANSPORTATION_METHOD_CODE = "MP";
    private static final String TAX_TYPE_CODE = "TX";
    private static final String SUPPLIER_REF_QUALIFIER = "IA";
    private static final String DEPARTMENT_NUMBER_REF_QUALIFIER = "DP";
    private static final String ORDER_TYPE_REF_QUALIFIER = "MR";
    private static final String ITEM_DESCRIPTION_QUALIFIER = "F";
    public static final String SENDER_MUTUALLY_DEFINED_QUALIFIER = "ZZ";
    public static final String RECEIVER_INTERCHANGE_ID_QUALIFIER = "08";
    public static final String APD_SKU_QUALIFIER = "IN";
    public static final String VENDOR_SKU_QUALIFIER = "VN";
    public static final String DEFAULT_METHOD_OF_HANDELING_CODE = "02";
    public static final String GLN_ID_IDENTIFICATION_CODE = "UL";
    public static final String BASIC_DISCOUNT_OFFERED = "08";
    public static final String NO_DISCOUNT = "05";
    public static final String RECIEPT_OF_GOODS_CODE = "15";
    public static final String ORDER_TYPE_IDENTIFICATION_PROPERTY_TYPE = "EDI: order type identification";
    public static final String SUPPLIER_NUMBER_IDENTIFICATION_PROPERTY_TYPE = "EDI: supplier number identification";
    public static final String CONVERT_FROM_DOLLARS_TO_CENTS = "100";
    public static final String DEPARTMNET_NUMBER_IDENTIFICATION_PROPERTY_TYPE = "EDI: departmnet number identification";
    public static final String KITS_PARTNER_ID = "walmart-kits";
    public static final String DEPT99_PARTNER_ID = "walmart-dept99";
    public static final String CASES_UOM_CONSTANT = "CA";
    public static final String ITEM_WEIGHT_SPECIFICATION_TYPE = "Item Weight";
    public static final String CASES_UNIT_OF_MEASURE_CODE = "CA";
    public static final String EACH_UNIT_OF_MEASURE_CODE = "EA";
    public static final String POUND_UNIT_OF_MEASURE_CODE = "LB";
    public static final String DEPT99_SKU_QUALIFIER = "IN";
    public static final String GLOBAL_TRADE_IDENTIFICATION_NUMBER = "EN";

    public Invoice fromCustomerInvoiceToX12(@org.apache.camel.Header("receiverCode") String receiverCode,
    		@org.apache.camel.Header("interchangeId") String interchangeId,
    		@org.apache.camel.Header("groupId") String groupId,
    		@org.apache.camel.Header("transactionId") String transactionId,
    		@org.apache.camel.Header("testIndicator") String testIndicator,
                @org.apache.camel.Header(EDI810v5010OutboundRouteBuilder.SENDER_ID) String senderId,
                @org.apache.camel.Header(EDI810v5010OutboundRouteBuilder.PARTNER_ID) String partnerId,
    		@org.apache.camel.Body CustomerInvoiceDto invoiceDto) throws Exception {
        
        final ShipmentDto shipmentDto = invoiceDto.getShipment();
        final PurchaseOrderDto customerOrderDto = shipmentDto.getCustomerOrderDto();
        Invoice invoice = new Invoice();
        //ISA
        final InterchangeEnvelopeHeader interchangeEnvelopeHeader = new InterchangeEnvelopeHeader();
        interchangeEnvelopeHeader.setAuthorizationInformationQualifier(NO_AUTHORIZATION_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setAuthorizationInformation(TEN_PADDED_SPACES);
        interchangeEnvelopeHeader.setSecurityInformationQualifier(NO_SECURITY_INFORMATION_PRESENT);
        interchangeEnvelopeHeader.setSecurityInformation(TEN_PADDED_SPACES);
        interchangeEnvelopeHeader.setInterchangeSenderID(APD_PADDED_15_CHARS);
        interchangeEnvelopeHeader.setInterchangeReceiverID(receiverCode);
        interchangeEnvelopeHeader.setInterchangeDate(new Date());
        interchangeEnvelopeHeader.setInterchangeTime(DateUtil.formatDate(new Date(), "HHMM"));
        interchangeEnvelopeHeader.setInterchangeControlNumber(OutboundEdiUtils.zeroPad(interchangeId, 9));
        interchangeEnvelopeHeader.setInterchangeVersionNumber(X12_PROCEDURES_REVIEW_BOARD_97);
        interchangeEnvelopeHeader.setiSAAcknowledgmentRequested(NO_ACKNOWLEDGEMENT_REQUESTED);
        interchangeEnvelopeHeader.setTestIndicator(testIndicator);
        interchangeEnvelopeHeader.setSubelementSeparator(USPS_SEPARATOR);
        interchangeEnvelopeHeader.setSenderInterchangeIDQualifier(SENDER_MUTUALLY_DEFINED_QUALIFIER);
        interchangeEnvelopeHeader.setReceiverInterchangeIDQualifier(RECEIVER_INTERCHANGE_ID_QUALIFIER);
        interchangeEnvelopeHeader.setInterchangeControlID(":");
        invoice.setEnvelopeHeader(interchangeEnvelopeHeader);
        //GS
        final GroupEnvelopeHeader groupEnvelopeHeader = new GroupEnvelopeHeader();
        groupEnvelopeHeader.setFunctionalIDCode(FUNCTIONAL_ID_CODE);
        groupEnvelopeHeader.setApplicationSendersCode(APD_UNPADDED);
        groupEnvelopeHeader.setApplicationReceiversCode(StringUtils.trim(receiverCode));
        groupEnvelopeHeader.setDate(new Date());
        groupEnvelopeHeader.setTime(DateUtil.formatDate(new Date(), "HHMM"));
        //Walmart wants this to be the same as the interchange Id
        groupEnvelopeHeader.setGroupControlNumber(interchangeId);
        groupEnvelopeHeader.setResponsibleAgencyCode(ACCREDITED_STANDARDS_COMMITTEE_X12);
        groupEnvelopeHeader.setVersionReleaseIndustryIdentifierCode(X12_PROCEDURES_REVIEW_BOARD_97+"0");
        invoice.setGroupEnvelopeHeader(groupEnvelopeHeader);
        
        Collection<InvoiceBody> bodies = new ArrayList<>();
        InvoiceBody body = new InvoiceBody();
        Header header = new Header();
        //ST
        TransactionSetHeader transactionSetHeader = new TransactionSetHeader();
        transactionSetHeader.setTransactionSetIdentifierCode(EDI_810_DOCUMENT);
        transactionSetHeader.setTransactionSetControlNumber(OutboundEdiUtils.zeroPad(transactionId, 9));
        header.setTransactionSetHeader(transactionSetHeader);
        //BIG
        InvoiceBeginningSegment big = new InvoiceBeginningSegment();
        big.setInvoiceDate(invoiceDto.getCreatedDate());
        big.setInvoiceNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(removeDash(invoiceDto.getInvoiceNumber()), 22));
        big.setPurchaseOrderDate(customerOrderDto.getOrderDate());
        big.setPurchaseOrderNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(customerOrderDto.retrieveCustomerPoNumber().getValue(), 22));
        big.setTransactionTypeCode(invoiceDto.getBillingTypeCode());
        header.setBeginningSegmentforInvoice(big);
        
        List<ReferenceNumber> refs = new ArrayList<>();
        final String departmentNumber = (String) shipmentDto.getCustomerOrderDto()
                        .getCredential().getProperties().get(DEPARTMNET_NUMBER_IDENTIFICATION_PROPERTY_TYPE);
        //REF - department number
        if(!StringUtils.isEmpty(departmentNumber)){
            refs = createSimpleREF(refs, departmentNumber, DEPARTMENT_NUMBER_REF_QUALIFIER);
        }
        //REF - order type
        final String orderTypeIdentification = (String) shipmentDto.getCustomerOrderDto()
                .getCredential().getProperties().get(ORDER_TYPE_IDENTIFICATION_PROPERTY_TYPE);
        if(!StringUtils.isEmpty(orderTypeIdentification)){
        	refs = createSimpleREF(refs, orderTypeIdentification, ORDER_TYPE_REF_QUALIFIER);
        }
        
        //REF - supplier number in dept99 location     
        final String supplierNumberIdentification = (String) shipmentDto.getCustomerOrderDto()
                .getCredential().getProperties().get(SUPPLIER_NUMBER_IDENTIFICATION_PROPERTY_TYPE);

        if(!StringUtils.isEmpty(supplierNumberIdentification) && DEPT99_PARTNER_ID.equals(partnerId)){
        	refs = createSimpleREF(refs, supplierNumberIdentification, SUPPLIER_REF_QUALIFIER);
        }
        
        //TODO: identify the meaning of this reference number
        if(KITS_PARTNER_ID.equals(partnerId)){
            refs = createSimpleREF(refs, "01", "19");
        }
        header.setReferenceNumbers(refs);
        body.setHeader(header);
        
        List<PartyIdentificationGroup> partyIdentificationGroups = new ArrayList<>();  
        //Bill To
        PartyIdentificationGroup billToPIG = new PartyIdentificationGroup();
        AddressDto billTo = shipmentDto.getCustomerOrderDto().lookupBillToAddressDto();
        if(billTo != null && !DEPT99_PARTNER_ID.equals(partnerId)) {
	        //N1
	        Name billToName = createSimpleN1(billTo, BILL_TO_INDENTIFIER_CODE);
	        billToPIG.setName(billToName);
	        partyIdentificationGroups.add(billToPIG);
        }
        
        //Ship To
        PartyIdentificationGroup shipToPIG = new PartyIdentificationGroup();
        AddressDto shipTo = shipmentDto.getCustomerOrderDto().getShipToAddressDto();
        if(shipTo != null) {
	        //N1
	        Name shipToName = createSimpleN1(shipTo, SHIP_TO_INDENTIFIER_CODE);
	        shipToPIG.setName(shipToName);
	        //N3
	        AddressInformation shipToAddressInformation = new AddressInformation();
	        shipToAddressInformation.setAddressLine1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getLine1(), 55));
	        shipToAddressInformation.setAddressLine2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getLine2(), 55));
	        shipToPIG.setAddressInformation(shipToAddressInformation);
	        //N4
	        GeographicLocation shipToGeographicLocation = new GeographicLocation();
	        shipToGeographicLocation.setCityName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getCity(), 2, 30));
	        shipToGeographicLocation.setStateOrProvinceCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getState(), 2, 2));
	        shipToGeographicLocation.setPostalCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getZip(), 3, 15));
	        shipToGeographicLocation.setCountryCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipTo.getCountry(), 2, 3));
	        shipToPIG.setGeographicLocation(shipToGeographicLocation);
	        partyIdentificationGroups.add(shipToPIG);
        }
        
        //Supplier
        //N1
        PartyIdentificationGroup supplierPIG = new PartyIdentificationGroup();
        Name supplierName = new Name();
        supplierName.setName("American Product Distributors");
        supplierName.setEntityIdentifierCode(SHIP_FROM_INDENTIFIER_CODE);
        supplierPIG.setName(supplierName);
        //REF
        List<ReferenceNumber> supplierRefs = new ArrayList<>();
        //REF - supplier number in kits location
        if(!StringUtils.isEmpty(supplierNumberIdentification) && KITS_PARTNER_ID.equals(partnerId)){
        	 createSimpleREF(supplierRefs, supplierNumberIdentification, SUPPLIER_REF_QUALIFIER);
        }
        supplierPIG.setReferenceNumbers(supplierRefs);
        partyIdentificationGroups.add(supplierPIG);
        
        header.setPartyIdentificationGroups(partyIdentificationGroups);
        
        //ITD
        List<TermsOfSale> termsOfSales = new ArrayList<>();
        
        TermsOfSaleDto termsOfSaleDto = customerOrderDto.getTermsOfSaleDto();
        if(termsOfSaleDto != null) {
	        TermsOfSale termsOfSale = new TermsOfSale();
                if(!StringUtils.isEmpty(termsOfSaleDto.getTermsDiscountPercent())){
                    termsOfSale.setTermsTypeCode(BASIC_DISCOUNT_OFFERED);
                }else{
                    termsOfSale.setTermsTypeCode(NO_DISCOUNT);
                }
	        termsOfSale.setTermsBasisDateCode(RECIEPT_OF_GOODS_CODE);
	        termsOfSale.setTermsDiscountPercent(termsOfSaleDto.getTermsDiscountPercent());
	        termsOfSale.setTermsDiscountDaysDue(termsOfSaleDto.getTermsDiscountDaysDue());
	        termsOfSale.setTermsNetDays(termsOfSaleDto.getTermsNetDays());
	        termsOfSales.add(termsOfSale);
        }
        header.setTermsOfSales(termsOfSales);
        //DTM - shipped date
        List<DateTimeReference> dateTimeReferences = new ArrayList<>();
        DateTimeReference shippedDateRef = new DateTimeReference();
        shippedDateRef.setDateTimeQualifier(SHIPPED_DATE_TIME_QUALIFIER);
        shippedDateRef.setDate(new Date());
        dateTimeReferences.add(shippedDateRef);
        header.setDateTimeReferences(dateTimeReferences);
        
        //FOB
        FOBRelatedInstruction fobRelatedInstruction = new FOBRelatedInstruction();
        if(!StringUtils.isEmpty(shipmentDto.getShippingMethodOfPayment())){
            fobRelatedInstruction.setShipmentMethodOfPayment(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getShippingMethodOfPayment(), 2, 2));
        } else {
            //Default FOB
            fobRelatedInstruction.setShipmentMethodOfPayment("CC");
            //TODO:fix this dirty dirty hack
            fobRelatedInstruction.setLocationQualifier("OR*VARIOUS ZZ ZZ");
            
        }
        header.setFobRelatedInstructions(fobRelatedInstruction);
        CarrierDetails carrierDetails=null;
        if(shipmentDto.getShippingPartnerDto()!=null && 
                shipmentDto.getShippingPartnerDto().getScacCode()!=null){
            carrierDetails = new CarrierDetails();
            carrierDetails.setTransportMethodCode(TRANSPORTATION_METHOD_CODE);
            if(shipmentDto.getShippingPartnerDto()!=null){
                carrierDetails.setStandardCarrierAlphaCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getShippingPartnerDto().getScacCode(), 2, 4));
            }
            carrierDetails.setRouting(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getRouting(), 35));
            carrierDetails.setIdentificationQualifier(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getTrackingNumberQualifier(), 2, 3));
            carrierDetails.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getTrackingNumber(), 50));
        }
        Detail detail= new Detail();
        List<InvoiceItemGroup> items = new ArrayList<>();
        BigInteger totalQty = new BigInteger("0");
        Collections.sort(shipmentDto.getLineItemXShipments(), new LineItemXShipmentDto.LineItemXShipmentDtoComparator());
        for(LineItemXShipmentDto itemXShip : shipmentDto.getLineItemXShipments()){
            LineItemDto item = itemXShip.getLineItemDto();
            InvoiceItemGroup ediItem = new InvoiceItemGroup();
            //IT1
            BaselineItemData it1 = new BaselineItemData();
            it1.setAssignedIdentificationNumber(itemXShip.getLineItemDto().getCustomerLineNumber());
            BigInteger qty = itemXShip.getQuantity();
            totalQty = totalQty.add(qty);
            it1.setQuantityInvoiced(qty.toString());
            it1.setUnitOfMeasureCode(item.getUnitOfMeasure().getName());
            it1.setUnitPrice(item.getUnitPrice());            
            
            if(!DEPT99_PARTNER_ID.equals(partnerId)){
                it1.setProductServiceIDQualifier1(APD_SKU_QUALIFIER);
                it1.setProductServiceID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getApdSku(), 48));                
                it1.setProductServiceIDQualifier2(VENDOR_SKU_QUALIFIER);                
                it1.setProductServiceID2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getManufacturerPartId(), 48));
            }
            else{
                it1.setProductServiceIDQualifier1(DEPT99_SKU_QUALIFIER);
                it1.setProductServiceID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getExtraneousCustomerSku(), 48));
                if(StringUtils.isNotEmpty(item.getUniversalProductCode())) {
                	it1.setProductServiceIDQualifier2("UP");
                	it1.setProductServiceID2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getUniversalProductCode(), 48));
                } else if(StringUtils.isNotEmpty(item.getGlobalTradeItemNumber())) {
                	it1.setProductServiceIDQualifier2(GLOBAL_TRADE_IDENTIFICATION_NUMBER);
                	it1.setProductServiceID2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getGlobalTradeItemNumber(), 48));
                }
                it1.setProdServIDQualifier3("UK");
                it1.setProductServiceID3(OutboundEdiUtils.truncateAndRemoveDisallowedChars( item.getGlobalTradeItemNumber(), 48));
            }
            
            ediItem.setBaselineItemData(it1);
            //PID
            ProductItemDescription productItemDescription = new ProductItemDescription();
            productItemDescription.setItemDescriptionType(ITEM_DESCRIPTION_QUALIFIER);
            productItemDescription.setDescription(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getShortName(), 80));
            ediItem.setProductItemDescription(productItemDescription);
            //PO4
            if(shipmentDto.getInnerPacks()!=null || shipmentDto.getInnerPacks()!=null){
                ItemPhysicalDetail itemPhysicalDetail = new ItemPhysicalDetail();
                if(shipmentDto.getPacks() != null ) {
                    itemPhysicalDetail.setPack(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getPacks().toString(), 6));
                }
                if(shipmentDto.getInnerPacks() != null) {
                    itemPhysicalDetail.setInnerPack(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getInnerPacks().toString(), 6));
                }
                ediItem.setItemPhysicalDetail(itemPhysicalDetail);
            }
            //CAD
            ediItem.setCarrierDetails(carrierDetails);
            
            items.add(ediItem);
        }
        Collections.sort(items, getInvoiceItemGroupComparator());
        detail.setInvoiceItems(items);
        Trailer trailer = new Trailer();
        //TDS
        TotalMonetaryValueSummary tds = new TotalMonetaryValueSummary();
        tds.setTotalInvoiceAmount(invoiceDto.getAmount().multiply(new BigDecimal(CONVERT_FROM_DOLLARS_TO_CENTS)));
        trailer.setTotalMonetaryValueSummary(tds);
        //TXI
        if(shipmentDto.getTotalTaxPrice() != null && !DEPT99_PARTNER_ID.equals(partnerId)){
            TaxInformation taxInformation = new TaxInformation();
            taxInformation.setTaxTypeCode(TAX_TYPE_CODE);
            taxInformation.setMonetaryAmount(shipmentDto.getTotalTaxPrice());
            trailer.setTaxInformation(taxInformation);
        }
        //CAD
        trailer.setCarrierDetails(carrierDetails);
        //SAC
        if(!DEPT99_PARTNER_ID.equals(partnerId)){
            Collection<AllowanceChargeOrService> shippingCharges = new ArrayList<>();
            AllowanceChargeOrService allowanceCharge = new AllowanceChargeOrService();
            allowanceCharge.setAllowanceChargeIndicator(CHARGE_INDICATOR);
            allowanceCharge.setSpecialChargeOrAllowanceCode(FREIGHT_INDICATOR);
            allowanceCharge.setAllowanceOrChargeTotalAmount(shipmentDto.calculateShippingTotal()
                                                .multiply(new BigDecimal(CONVERT_FROM_DOLLARS_TO_CENTS)));
            allowanceCharge.setMethodOfHandlingCode(DEFAULT_METHOD_OF_HANDELING_CODE);
            shippingCharges.add(allowanceCharge);
            trailer.setAllowanceChargeOrServices(shippingCharges);
        }
        //ISS
        if(DEPT99_PARTNER_ID.equals(partnerId)){
            //Check if the unit of measure is always cases
            boolean cases = true;
            BigInteger numberOfUnitsShipped = BigInteger.ZERO;
            BigDecimal weight = BigDecimal.ZERO;
            for(LineItemXShipmentDto lix:shipmentDto.getLineItemXShipments()){
                numberOfUnitsShipped = numberOfUnitsShipped.add(lix.getQuantity());
                if(lix.getLineItemDto()!=null && lix.getLineItemDto().getItem()!=null){
                    ItemDto item = lix.getLineItemDto().getItem();
                    //default to each
                    if(item.getUnitOfMeasure() == null || !CASES_UOM_CONSTANT.equals(item.getUnitOfMeasure().getName())){
                        cases = false;
                    }                
                    BigDecimal weightValue = null;
                    if(item.getItemWeight() != null) {
                    	weightValue = item.getItemWeight();
                    } else if (item.getSpecifications().get(ITEM_WEIGHT_SPECIFICATION_TYPE) != null) {
                    	try {
                    		//used for legacy orders, moving forward will use the ItemWeight field.
                    		weightValue = new BigDecimal(item.getSpecifications().get(ITEM_WEIGHT_SPECIFICATION_TYPE));
                    	} catch (NumberFormatException exp) {
    						LOG.debug("Error parsing weight of an item from specification", exp);
    					}
                    }
                    if (weightValue != null) {
                            try {
                                    weight = weight.add(new BigDecimal(lix.getQuantity().toString()).multiply(weightValue));
                            }
                            catch (Exception e) {
                                    LOG.debug("Error parsing weight of an item", e);
                            }
                    }
                }
            }
            InvoiceShipmentSummary invoiceShipmentSummary = new InvoiceShipmentSummary();
            invoiceShipmentSummary.setNumberOfUnitsShipped(numberOfUnitsShipped);
            if(cases){
                invoiceShipmentSummary.setUnitForMeasurementCode1(CASES_UNIT_OF_MEASURE_CODE);
            }
            else{
                invoiceShipmentSummary.setUnitForMeasurementCode1(EACH_UNIT_OF_MEASURE_CODE);
            }
            invoiceShipmentSummary.setWeight(weight);
            invoiceShipmentSummary.setUnitForMeasurementCode2(POUND_UNIT_OF_MEASURE_CODE);
            trailer.setInvoiceShipmentSummary(invoiceShipmentSummary);
        }        
        //CTT
        TransactionTotal transactionTotal = new TransactionTotal();
        transactionTotal.setNumberOfLineItems(new BigInteger(shipmentDto.getLineItemXShipments().size() + ""));

        trailer.setTransactionTotals(transactionTotal);
        //SE
        TransactionSetTrailer transactionSetTrailer = new TransactionSetTrailer();
        transactionSetTrailer.setTransactionControlNumber(transactionSetHeader.getTransactionSetControlNumber());
        trailer.setTransactionSetTrailer(transactionSetTrailer);
        
            
        body.setTrailer(trailer);
        body.setDetail(detail);        
        body.getTrailer().getTransactionSetTrailer().setNumberOfIncludedSegments(countNumberOfSegments(body));
        bodies.add(body);
        invoice.setBody(bodies);
        //GE
        GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
		groupEnvelopeTrailer.setNumberOfTransactionSets(bodies.size() + "");
		groupEnvelopeTrailer.setGroupControlNumber(groupEnvelopeHeader.getGroupControlNumber());
		
		invoice.setGroupEnvelopeTrailer(groupEnvelopeTrailer);
		//IEA
		InterchangeEnvelopeTrailer itrailer = new InterchangeEnvelopeTrailer();
		itrailer.setNumberOfIncludedGroups("1");
		itrailer.setInterchangeControlNumber(interchangeEnvelopeHeader.getInterchangeControlNumber());
		invoice.setEnvelopeTrailer(itrailer);
		
		
        return invoice;
    }

    private Name createSimpleN1(AddressDto billTo, String identifierCode) {
        Name billToName = new Name();
        billToName.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(billTo.getName(), 60));
        billToName.setEntityIdentifierCode(identifierCode);
        if (billTo.getMiscShipToDto() != null && !StringUtils.isEmpty(billTo.getMiscShipToDto().getGlnID())) {
            billToName.setIdentificationCodeQualifier(GLN_ID_IDENTIFICATION_CODE);
            billToName.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(billTo
                    .getMiscShipToDto().getGlnID(), 80));
        }
        return billToName;
    }

    private List<ReferenceNumber> createSimpleREF(List<ReferenceNumber> refs, final String value, String qualifier) {
        ReferenceNumber departmentNumberRef = new ReferenceNumber();
        departmentNumberRef.setReferenceIdentificationQualifier(qualifier);
        departmentNumberRef.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(value, 50));
        refs.add(departmentNumberRef);
        return refs;
    }

    private Comparator<InvoiceItemGroup> getInvoiceItemGroupComparator() {
        return new Comparator<InvoiceItemGroup>() {

            @Override
            public int compare(InvoiceItemGroup arg0, InvoiceItemGroup arg1) {
                return EDIOrderUtils.ediSegmentCompartor(arg0.getBaselineItemData().getAssignedIdentificationNumber(),
                        arg1.getBaselineItemData().getAssignedIdentificationNumber());
            }
        };
    }

    private String countNumberOfSegments(InvoiceBody body) {
        Integer numberOfSegments = 3;//ST SE BIG 
        numberOfSegments = numberOfSegments + body.getHeader().getReferenceNumbers().size();
        numberOfSegments = numberOfSegments + body.getHeader().getDateTimeReferences().size();
        if (body.getHeader().getFobRelatedInstructions() != null) {
            numberOfSegments++;
        }
        if (body.getHeader().getNoteSpecialInstructions() != null) {
            numberOfSegments = numberOfSegments + body.getHeader().getNoteSpecialInstructions().size();
        }
        if (body.getHeader().getCurrency() != null) {
            numberOfSegments++;
        }
        if (body.getHeader().getPartyIdentificationGroups() != null) {
            for (PartyIdentificationGroup group : body.getHeader().getPartyIdentificationGroups()) {
                if (group.getAdditionalNameInformation() != null) {
                    numberOfSegments++;
                }
                if (group.getAddressInformation() != null) {
                    numberOfSegments++;
                }
                if (group.getGeographicLocation() != null) {
                    numberOfSegments++;
                }
                if (group.getName() != null) {
                    numberOfSegments++;
                }
                if (group.getReferenceNumbers() != null) {
                    numberOfSegments = numberOfSegments + group.getReferenceNumbers().size();
                }
            }
        }
        if (body.getHeader().getTermsOfSales() != null) {
            numberOfSegments = numberOfSegments + body.getHeader().getTermsOfSales().size();
        }
        if (body.getDetail().getInvoiceItems() != null) {
            for (InvoiceItemGroup item : body.getDetail().getInvoiceItems()) {
                numberOfSegments = numberOfSegments + 2;//IT1 PID
                if (item.getCarrierDetails() != null) {
                    numberOfSegments++;
                }
                if (item.getItemPhysicalDetail() != null) {
                    numberOfSegments++;
                }
                if (item.getAllowanceChargeOrServices() != null) {
                    numberOfSegments = numberOfSegments + item.getAllowanceChargeOrServices().size();
                }
            }
        }
        if (body.getTrailer().getAllowanceChargeOrServices() != null) {
            numberOfSegments = numberOfSegments + body.getTrailer().getAllowanceChargeOrServices().size();
        }
        if (body.getTrailer().getTaxInformation() != null) {
            numberOfSegments++;
        }
        if (body.getTrailer().getInvoiceShipmentSummary() != null) {
            numberOfSegments++;
        }
        if (body.getTrailer().getTotalMonetaryValueSummary() != null) {
            numberOfSegments++;
        }
        if (body.getTrailer().getTransactionTotals() != null) {
            numberOfSegments++;
        }
        if (body.getTrailer().getCarrierDetails() != null) {
            numberOfSegments++;
        }
        return numberOfSegments.toString();
    }

    private String removeDash(String invoiceNumber) {
        return invoiceNumber.replaceAll("\\-", "");
    }
}
