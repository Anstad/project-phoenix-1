package com.apd.phoenix.service.integration.camel.processor.cxml;

import static com.apd.phoenix.service.cxml.CxmlConstants.USER_AGENT;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.CustomerInvoiceBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.business.SystemUserBp;
import com.apd.phoenix.service.integration.cxml.model.invoice.CXML;
import com.apd.phoenix.service.integration.cxml.model.invoice.Contact;
import com.apd.phoenix.service.integration.cxml.model.invoice.Country;
import com.apd.phoenix.service.integration.cxml.model.invoice.CountryCode;
import com.apd.phoenix.service.integration.cxml.model.invoice.Description;
import com.apd.phoenix.service.integration.cxml.model.invoice.DocumentReference;
import com.apd.phoenix.service.integration.cxml.model.invoice.DueAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.Extrinsic;
import com.apd.phoenix.service.integration.cxml.model.invoice.From;
import com.apd.phoenix.service.integration.cxml.model.invoice.GrossAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.Header;
import com.apd.phoenix.service.integration.cxml.model.invoice.Identity;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailHeaderIndicator;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailItem;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailItemReference;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailLineIndicator;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailLineShipping;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailOrder;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailOrderInfo;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailRequest;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailRequestHeader;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailShipping;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoiceDetailSummary;
import com.apd.phoenix.service.integration.cxml.model.invoice.InvoicePartner;
import com.apd.phoenix.service.integration.cxml.model.invoice.ItemID;
import com.apd.phoenix.service.integration.cxml.model.invoice.Money;
import com.apd.phoenix.service.integration.cxml.model.invoice.Name;
import com.apd.phoenix.service.integration.cxml.model.invoice.NetAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.OrderIDInfo;
import com.apd.phoenix.service.integration.cxml.model.invoice.OrderReference;
import com.apd.phoenix.service.integration.cxml.model.invoice.Phone;
import com.apd.phoenix.service.integration.cxml.model.invoice.PostalAddress;
import com.apd.phoenix.service.integration.cxml.model.invoice.Request;
import com.apd.phoenix.service.integration.cxml.model.invoice.Sender;
import com.apd.phoenix.service.integration.cxml.model.invoice.SharedSecret;
import com.apd.phoenix.service.integration.cxml.model.invoice.ShippingAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.Street;
import com.apd.phoenix.service.integration.cxml.model.invoice.SubtotalAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.SupplierOrderInfo;
import com.apd.phoenix.service.integration.cxml.model.invoice.Tax;
import com.apd.phoenix.service.integration.cxml.model.invoice.TaxAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.TaxDetail;
import com.apd.phoenix.service.integration.cxml.model.invoice.TaxLocation;
import com.apd.phoenix.service.integration.cxml.model.invoice.TaxableAmount;
import com.apd.phoenix.service.integration.cxml.model.invoice.TelephoneNumber;
import com.apd.phoenix.service.integration.cxml.model.invoice.To;
import com.apd.phoenix.service.integration.cxml.model.invoice.UnitOfMeasure;
import com.apd.phoenix.service.integration.cxml.model.invoice.UnitPrice;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.CxmlCredential;
import com.apd.phoenix.service.model.Person;
import com.apd.phoenix.service.model.PhoneNumber;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.utility.CxmlMessageUtils;

@Stateless
public class CxmlInvoiceDetailRequestTranslator {

    protected static final Logger LOGGER = LoggerFactory.getLogger(CxmlInvoiceDetailRequestTranslator.class);
    public static final String CXML_VERSION = "1.2.024";
    public static final String ENGLISH = "en-US";
    public static final String CXML_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    public static final String INVOICE_ORIGIN_SUPPLIER = "supplier";
    public static final String YES = "yes";
    public static final String NO = "no";
    public static final String ROLE_REMIT_TO = "remitTo";
    public static final String ROLE_BILL_TO = "billTo";
    public static final String ROLE_SOLD_TO = "soldTo";
    public static final String ROLE_SHIP_FROM = "shipFrom";
    public static final String ROLE_SHIP_TO = "shipTo";
    public static final String APD_NAME = "American Product Distributors Inc";
    public static final String APD_CITY = "Charlotte";
    public static final String APD_STATE = "NC";
    public static final String APD_POSTAL_CODE = "28273";
    public static final String APD_STREET = "8350 Arrowridge Blvd, Suite A";
    public static final String APD_ISO_COUNTRY_CODE = "US";
    public static final String APD_COUNTRY = "United States";
    public static final String APD_AREA_OR_CITY_CODE = "877";
    public static final String APD_NUMBER = "7690752";
    public static final String APD_NUMBER_PREFIX = "1";
    public static final String INVOICE_SUBMISSION_METHOD = "invoiceSubmissionMethod";
    public static final String CXML = "cXML";
    public static final String INVOICE_SOURCE_DOCUMENT = "invoiceSourceDocument";
    public static final String SALES_ORDER = "SalesOrder";
    public static final String USD = "USD";
    public static final String TOTAL_ITEM_TAX = "Total item tax";
    public static final String TOTAL_TAX = "Total tax";
    public static final String SALES = "Sales";
    public static final String TAX = "Tax";
    public static final String DEFAULT_NAME = "default";
    public static final String PURCHASE_ORDER = "PurchaseOrder";

    @Inject
    CustomerInvoiceBp customerInvoiceBp;

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    SystemUserBp systemUserBp;

    @Inject
    CredentialBp credentialBp;

    @Inject
    CxmlMessageUtils cxmlMessageUtils;

    public static String prepareXmlHeaders(@Body String body) {
        body = body.replaceFirst("<\\?xml.*\\?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.024/InvoiceDetail.dtd\">");
        return body;
    }

    public CXML fromInvoiceDtoToDetailRequest(@org.apache.camel.Header("deploymentMode") String deploymentMode,
            @org.apache.camel.Header("credentialDeploymentMode") String credDeploymentMode,
            @Body CustomerInvoiceDto invoiceDto) {

        if (StringUtils.isNotBlank(credDeploymentMode)) {
            deploymentMode = credDeploymentMode;
        }
        CXML cxml = new CXML();
        cxml.setVersion(CXML_VERSION);
        cxml.setXmlLang(ENGLISH);
        cxml.setPayloadID(cxmlMessageUtils.generatePayloadId());
        cxml.setTimestamp(buildFormat().format(new Date()));

        cxml.getHeaderOrMessageOrRequestOrResponse().add(createHeader(invoiceDto));
        cxml.getHeaderOrMessageOrRequestOrResponse().add(createRequest(invoiceDto, deploymentMode));
        return cxml;
    }

    private Header createHeader(CustomerInvoiceDto invoiceDto) {
        Header header = new Header();
        Credential credential = credentialBp.retrieveCredential(invoiceDto.getShipment().getCustomerOrderDto()
                .getCredential());
        header.setFrom(createFrom(invoiceDto, credential));
        header.setTo(createTo(invoiceDto, credential));
        header.setSender(createSender(invoiceDto, credential));
        return header;
    }

    private To createTo(CustomerInvoiceDto invoiceDto, Credential credential) {
        To to = new To();
        for (CxmlCredential cxmlCredential : credential.getCustomerOutoingToCred()) {
            to.getCredential().add(createCredential(cxmlCredential));
        }
        return to;
    }

    private Sender createSender(CustomerInvoiceDto invoiceDto, Credential credential) {
        Sender sender = new Sender();
        for (CxmlCredential cxmlCredential : credential.getCustomerOutoingSenderCred()) {
            sender.getCredential().add(createCredential(cxmlCredential, credential.getCustomerOutgoingSharedSecret()));
        }
        sender.setUserAgent(USER_AGENT);
        return sender;
    }

    private com.apd.phoenix.service.integration.cxml.model.invoice.Credential createCredential(
            CxmlCredential cxmlCredential, String customerOutgoingSharedSecret) {
        com.apd.phoenix.service.integration.cxml.model.invoice.Credential cxmlCred = createCredential(cxmlCredential);
        cxmlCred.getSharedSecretOrDigitalSignatureOrCredentialMac().add(
                createSharedSecret(customerOutgoingSharedSecret));
        return cxmlCred;
    }

    private static SharedSecret createSharedSecret(String customerOutgoingSharedSecret) {
        SharedSecret sharedSecret = new SharedSecret();
        sharedSecret.getContent().add(customerOutgoingSharedSecret);
        return sharedSecret;
    }

    private From createFrom(CustomerInvoiceDto invoiceDto, Credential credential) {
        From from = new From();
        for (CxmlCredential cxmlCredential : credential.getCustomerOutoingFromCred()) {
            from.getCredential().add(createCredential(cxmlCredential));
        }
        return from;
    }

    private com.apd.phoenix.service.integration.cxml.model.invoice.Credential createCredential(
            CxmlCredential cxmlCredential) {
        com.apd.phoenix.service.integration.cxml.model.invoice.Credential cxmlCred = new com.apd.phoenix.service.integration.cxml.model.invoice.Credential();
        cxmlCred.setDomain(cxmlCredential.getDomain());
        cxmlCred.setIdentity(createIdentity(cxmlCredential.getIdentifier()));
        return cxmlCred;
    }

    private static Identity createIdentity(String identifier) {
        Identity identity = new Identity();
        identity.getContent().add(identifier);
        return identity;
    }

    private Request createRequest(CustomerInvoiceDto invoiceDto, String deploymentMode) {
        Request request = new Request();
        request.setDeploymentMode(deploymentMode);
        request.getInvoiceDetailRequestOrProviderSetupRequest().add(createInvoiceDetailRequest(invoiceDto));
        return request;
    }

    private InvoiceDetailRequest createInvoiceDetailRequest(CustomerInvoiceDto invoiceDto) {
        InvoiceDetailRequest invoiceDetailRequest = new InvoiceDetailRequest();
        invoiceDetailRequest.setInvoiceDetailRequestHeader(createInvoiceDetailRequestHeader(invoiceDto));
        InvoiceDetailOrder invoiceDetailOrder = createInvoiceDetailOrder(invoiceDetailRequest
                .getInvoiceDetailRequestHeader(), invoiceDto);
        invoiceDetailRequest.getInvoiceDetailOrderOrInvoiceDetailHeaderOrder().add(invoiceDetailOrder);
        invoiceDetailRequest.setInvoiceDetailSummary(createInvoiceDetailSummary(invoiceDto));
        return invoiceDetailRequest;
    }

    private InvoiceDetailRequestHeader createInvoiceDetailRequestHeader(CustomerInvoiceDto invoiceDto) {
        InvoiceDetailRequestHeader invoiceDetailRequestHeader = new InvoiceDetailRequestHeader();
        invoiceDetailRequestHeader.setInvoiceID(invoiceDto.getInvoiceNumber());
        invoiceDetailRequestHeader.setInvoiceDate(buildFormat().format(invoiceDto.getCreatedDate()));
        invoiceDetailRequestHeader.setInvoiceOrigin(INVOICE_ORIGIN_SUPPLIER);
        invoiceDetailRequestHeader.setInvoiceDetailHeaderIndicator(createInvoiceDetailHeaderIndicator(invoiceDto));
        invoiceDetailRequestHeader.setInvoiceDetailLineIndicator(createInvoiceDetailLineIndicator(invoiceDto));
        invoiceDetailRequestHeader.setInvoiceDetailShipping(createInvoiceDetailShipping(invoiceDto));
        Credential credential = credentialBp.retrieveCredential(invoiceDto.getShipment().getCustomerOrderDto()
                .getCredential());
        InvoicePartner apdInvoicePartner = new InvoicePartner();
        apdInvoicePartner.setContact(getApd());
        apdInvoicePartner.getContact().setRole(ROLE_REMIT_TO);
        if (credential != null && credential.getProperties() != null) {
            String addressId = credentialBp.getCredetialProperty(CredentialPropertyTypeEnum.CONTACT_ADDRESSID
                    .getValue(), credential);
            if (StringUtils.isNotBlank(addressId)) {
                apdInvoicePartner.getContact().setAddressID(addressId);
            }
        }
        invoiceDetailRequestHeader.getInvoicePartner().add(apdInvoicePartner);

        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(invoiceDto.getShipment().getCustomerOrderDto()
                .getApdPoNumber());

        Address billingAddress = customerOrder.getPaymentInformation().getBillingAddress();
        //Bill to
        InvoicePartner invoicePartner = new InvoicePartner();
        Contact contact = createContact(billingAddress.getPerson(), billingAddress);
        contact.setRole(ROLE_BILL_TO);
        invoicePartner.setContact(contact);
        invoiceDetailRequestHeader.getInvoicePartner().add(invoicePartner);

        //Sold to
        InvoicePartner invoicePartnerSoldTo = new InvoicePartner();
        Contact contactSoldTo = createContact(billingAddress.getPerson(), billingAddress);
        contactSoldTo.setRole(ROLE_SOLD_TO);
        invoicePartnerSoldTo.setContact(contactSoldTo);
        invoiceDetailRequestHeader.getInvoicePartner().add(invoicePartnerSoldTo);

        Extrinsic invoiceSubmissionMethod = new Extrinsic();
        invoiceSubmissionMethod.setName(INVOICE_SUBMISSION_METHOD);
        invoiceSubmissionMethod.getContent().add(CXML);

        Extrinsic invoiceSourceDocument = new Extrinsic();
        invoiceSourceDocument.setName(INVOICE_SOURCE_DOCUMENT);
        invoiceSourceDocument.getContent().add(PURCHASE_ORDER);

        invoiceDetailRequestHeader.getExtrinsic().add(invoiceSubmissionMethod);
        invoiceDetailRequestHeader.getExtrinsic().add(invoiceSourceDocument);

        return invoiceDetailRequestHeader;
    }

    private static InvoiceDetailHeaderIndicator createInvoiceDetailHeaderIndicator(CustomerInvoiceDto invoiceDto) {
        //Using defaults
        return new InvoiceDetailHeaderIndicator();
    }

    private static InvoiceDetailLineIndicator createInvoiceDetailLineIndicator(CustomerInvoiceDto invoiceDto) {
        InvoiceDetailLineIndicator invoiceDetailLineIndicator = new InvoiceDetailLineIndicator();
        return invoiceDetailLineIndicator;
    }

    private InvoiceDetailOrder createInvoiceDetailOrder(InvoiceDetailRequestHeader invoiceHeader,
            CustomerInvoiceDto invoiceDto) {
        InvoiceDetailOrder invoiceDetailOrder = new InvoiceDetailOrder();
        invoiceDetailOrder.setInvoiceDetailOrderInfo(createInvoiceDetailOrderInfo(invoiceDto));
        int invoiceLineNumber = 0;
        for (LineItemXShipmentDto lineItemXShipmentDto : invoiceDto.getShipment().getLineItemXShipments()) {
            invoiceLineNumber++;
            invoiceDetailOrder.getInvoiceDetailItemOrInvoiceDetailServiceItem().add(
                    createInvoiceDetailItem(lineItemXShipmentDto, invoiceDto, invoiceLineNumber, invoiceHeader
                            .getInvoiceDetailLineIndicator()));
        }
        return invoiceDetailOrder;
    }

    private InvoiceDetailOrderInfo createInvoiceDetailOrderInfo(CustomerInvoiceDto invoiceDto) {
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(invoiceDto.getShipment().getCustomerOrderDto()
                .getApdPoNumber());
        InvoiceDetailOrderInfo invoiceDetailOrderInfo = new InvoiceDetailOrderInfo();
        if (customerOrder != null && StringUtils.isNotBlank(customerOrder.getOrderPayloadId())) {
            invoiceDetailOrderInfo
                    .getOrderReferenceOrMasterAgreementReferenceOrMasterAgreementIDInfoOrSupplierOrderInfoOrOrderIDInfo()
                    .add(createOrderReference(customerOrder));
        }
        else if (customerOrder != null && customerOrder.getCustomerPo() != null) {
            invoiceDetailOrderInfo
                    .getOrderReferenceOrMasterAgreementReferenceOrMasterAgreementIDInfoOrSupplierOrderInfoOrOrderIDInfo()
                    .add(createOrderIDInfo(customerOrder));
        }
        if (customerOrder != null && customerOrder.getApdPo() != null) {
            invoiceDetailOrderInfo
                    .getOrderReferenceOrMasterAgreementReferenceOrMasterAgreementIDInfoOrSupplierOrderInfoOrOrderIDInfo()
                    .add(createSupplierOrderInfo(customerOrder));
        }

        if (invoiceDetailOrderInfo
                .getOrderReferenceOrMasterAgreementReferenceOrMasterAgreementIDInfoOrSupplierOrderInfoOrOrderIDInfo()
                .isEmpty()) {
            LOGGER
                    .warn("No buyer or supplier order numbers could be found on the customer order referenced by the shipment to attach to "
                            + "the outbound cxml invoice detail request. customerOrder.id: " + customerOrder.getId());
        }
        return invoiceDetailOrderInfo;
    }

    private static OrderIDInfo createOrderIDInfo(CustomerOrder customerOrder) {
        OrderIDInfo orderIDInfo = new OrderIDInfo();

        if (customerOrder.getCustomerPo() != null) {
            orderIDInfo.setOrderID(customerOrder.getCustomerPo().getValue());
        }
        return orderIDInfo;
    }

    private static SupplierOrderInfo createSupplierOrderInfo(CustomerOrder customerOrder) {
        SupplierOrderInfo supplierOrderInfo = new SupplierOrderInfo();

        if (customerOrder.getApdPo() != null) {
            supplierOrderInfo.setOrderID(customerOrder.getApdPo().getValue());
        }
        return supplierOrderInfo;
    }

    //    private OrderReference createOrderReference(CustomerInvoiceDto invoiceDto) {
    //        OrderReference orderReference = new OrderReference();
    //        orderReference.setDocumentReference(createDocumentReference(invoiceDto));
    //        return orderReference;
    //    }
    //
    //    private DocumentReference createDocumentReference(CustomerInvoiceDto invoiceDto) {
    //        DocumentReference documentReference = new DocumentReference();
    //        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(invoiceDto.getShipment().getCustomerOrderDto()
    //                .getApdPoNumber());
    //        documentReference.setPayloadID(customerOrder.getOrderPayloadId());
    //        return documentReference;
    //    }

    private OrderReference createOrderReference(CustomerOrder customerOrder) {
        OrderReference orderReference = new OrderReference();
        orderReference.setOrderID(customerOrder.getCustomerPo().getValue());
        orderReference.setDocumentReference(createDocumentReference(customerOrder.getOrderPayloadId()));
        return orderReference;
    }

    private DocumentReference createDocumentReference(String payloadId) {
        DocumentReference documentReference = new DocumentReference();
        documentReference.setPayloadID(payloadId);
        return documentReference;
    }

    private InvoiceDetailItem createInvoiceDetailItem(LineItemXShipmentDto lineItemXShipmentDto,
            CustomerInvoiceDto invoiceDto, int invoiceLineNumber, InvoiceDetailLineIndicator invoiceDetailLineIndicator) {
        InvoiceDetailItem invoiceDetailItem = new InvoiceDetailItem();
        if (!lineItemXShipmentDto.getLineItemDto().getFee()) {
            invoiceDetailItem.setInvoiceLineNumber(Integer.toString(invoiceLineNumber));
        }
        else {
            //Set all fees a negative invoice line number
            invoiceDetailItem.setInvoiceLineNumber("-" + invoiceLineNumber);
        }
        invoiceDetailItem.setQuantity(lineItemXShipmentDto.getQuantity().toString());
        LineItemDto lineItemDto = lineItemXShipmentDto.getLineItemDto();
        invoiceDetailItem.setUnitOfMeasure(createUnitOfMeasure(lineItemDto.getUnitOfMeasure()));
        invoiceDetailItem.setUnitPrice(createUnitPrice(lineItemDto.getUnitPrice()));
        invoiceDetailItem.setInvoiceDetailItemReference(createInvoiceDetailItemReference(lineItemDto, invoiceDto));
        invoiceDetailItem.setSubtotalAmount(createSubtotalAmount(lineItemDto.getUnitPrice().multiply(
                BigDecimal.valueOf(lineItemXShipmentDto.getQuantity().longValue()))));
        if (StringUtils.equalsIgnoreCase(invoiceDetailLineIndicator.getIsTaxInLine(), YES)) {
            invoiceDetailItem.setTax(createTax(lineItemXShipmentDto));
        }
        if (StringUtils.equalsIgnoreCase(invoiceDetailLineIndicator.getIsShippingInLine(), YES)) {
            invoiceDetailItem.setInvoiceDetailLineShipping(createInvoiceDetailLineShipping(lineItemXShipmentDto,
                    invoiceDto));
        }
        return invoiceDetailItem;
    }

    private InvoiceDetailLineShipping createInvoiceDetailLineShipping(LineItemXShipmentDto lineItemXShipmentDto,
            CustomerInvoiceDto invoiceDto) {
        InvoiceDetailLineShipping invoiceDetailLineShipping = new InvoiceDetailLineShipping();
        invoiceDetailLineShipping.setInvoiceDetailShipping(createInvoiceDetailShipping(invoiceDto));
        invoiceDetailLineShipping.setMoney(createMoney(lineItemXShipmentDto.getShipping()));
        return invoiceDetailLineShipping;
    }

    private InvoiceDetailShipping createInvoiceDetailShipping(CustomerInvoiceDto invoiceDto) {
        InvoiceDetailShipping invoiceDetailShipping = new InvoiceDetailShipping();

        Contact shipFrom = getApd();
        shipFrom.setRole(ROLE_SHIP_FROM);
        invoiceDetailShipping.getContactOrCarrierIdentifierOrShipmentIdentifierOrDocumentReference().add(shipFrom);
        CustomerOrder customerOrder = customerOrderBp.searchByApdPo(invoiceDto.getShipment().getCustomerOrderDto()
                .getApdPoNumber());
        Person shipToContact = customerOrder.getAddress().getPerson();
        if (shipToContact == null) {
            shipToContact = systemUserBp.retrieveUserWithPhoneNumbers(
                    invoiceDto.getShipment().getCustomerOrderDto().getSystemUserDto()).getPerson();
        }
        Contact shipTo = createContact(shipToContact, customerOrder.getAddress());
        shipTo.setRole(ROLE_SHIP_TO);
        invoiceDetailShipping.getContactOrCarrierIdentifierOrShipmentIdentifierOrDocumentReference().add(shipTo);
        return invoiceDetailShipping;
    }

    private static Tax createTax(LineItemXShipmentDto lineItemXShipmentDto) {
        Tax tax = new Tax();
        tax.setMoney(createMoney(lineItemXShipmentDto.calculateTotalTaxAmount()));
        tax.setDescription(createDescription(TOTAL_ITEM_TAX));
        tax.getTaxDetail().add(createTaxDetail(lineItemXShipmentDto));
        return tax;
    }

    private static Tax createTax(BigDecimal taxableAmount, BigDecimal taxTotal) {
        Tax tax = new Tax();
        tax.setMoney(createMoney(taxTotal));
        tax.setDescription(createDescription(TOTAL_TAX));
        tax.getTaxDetail().add(createTaxDetail(taxableAmount, taxTotal));
        return tax;
    }

    private static TaxDetail createTaxDetail(LineItemXShipmentDto lineItemXShipmentDto) {
        TaxDetail taxDetail = new TaxDetail();
        //taxDetail.setPercentageRate(BigDecimal.ZERO.toPlainString());
        taxDetail.setCategory(SALES);
        taxDetail.setPurpose(TAX);
        taxDetail.setTaxableAmount(createTaxableAmount(lineItemXShipmentDto.calculateTaxableAmount()));
        taxDetail.setTaxAmount(createTaxAmount(lineItemXShipmentDto.calculateTotalTaxAmount()));
        taxDetail.setTaxLocation(createTaxLocation(ENGLISH));
        return taxDetail;
    }

    private static TaxDetail createTaxDetail(BigDecimal taxableAmount, BigDecimal taxTotal) {
        TaxDetail taxDetail = new TaxDetail();
        //taxDetail.setPercentageRate(BigDecimal.ZERO.toPlainString());
        taxDetail.setCategory(SALES);
        taxDetail.setPurpose(TAX);
        taxDetail.setTaxableAmount(createTaxableAmount(taxableAmount));
        taxDetail.setTaxAmount(createTaxAmount(taxTotal));
        taxDetail.setTaxLocation(createTaxLocation(ENGLISH));
        return taxDetail;
    }

    private static TaxAmount createTaxAmount(BigDecimal tax) {
        TaxAmount taxAmount = new TaxAmount();
        taxAmount.setMoney(createMoney(tax));
        return taxAmount;
    }

    private static TaxLocation createTaxLocation(String english2) {
        TaxLocation taxLocation = new TaxLocation();
        taxLocation.setXmlLang(ENGLISH);
        return taxLocation;
    }

    private static TaxableAmount createTaxableAmount(BigDecimal tax) {
        TaxableAmount taxableAmount = new TaxableAmount();
        taxableAmount.setMoney(createMoney(tax));
        return taxableAmount;
    }

    private static SubtotalAmount createSubtotalAmount(BigDecimal sAmount) {
        SubtotalAmount subtotalAmount = new SubtotalAmount();
        subtotalAmount.setMoney(createMoney(sAmount));
        return subtotalAmount;
    }

    private static InvoiceDetailItemReference createInvoiceDetailItemReference(LineItemDto lineItemDto,
            CustomerInvoiceDto invoiceDto) {
        InvoiceDetailItemReference invoiceDetailItemReference = new InvoiceDetailItemReference();
        if (lineItemDto.getFee()) {
            invoiceDetailItemReference.setLineNumber(Integer.toString(0));
        }
        else {
            invoiceDetailItemReference.setLineNumber(lineItemDto.getCustomerLineNumber());
        }
        invoiceDetailItemReference.setDescription(createDescription(lineItemDto.getShortName()));
        invoiceDetailItemReference.setItemID(createItemID(lineItemDto));
        return invoiceDetailItemReference;
    }

    private static ItemID createItemID(LineItemDto lineItemDto) {
        ItemID itemId = new ItemID();
        itemId.setSupplierPartID(lineItemDto.getSupplierPartId());
        return itemId;
    }

    private static Description createDescription(String descriptionString) {
        Description description = new Description();
        description.setvalue(descriptionString);
        description.setXmlLang(ENGLISH);
        return description;
    }

    private static UnitPrice createUnitPrice(BigDecimal uPrice) {
        UnitPrice unitPrice = new UnitPrice();
        unitPrice.setMoney(createMoney(uPrice));
        return unitPrice;
    }

    private static Money createMoney(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        Money money = new Money();
        money.setCurrency(USD);
        money.setvalue(value.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());
        return money;
    }

    private static UnitOfMeasure createUnitOfMeasure(UnitOfMeasureDto uom) {
        UnitOfMeasure unitOfMeasure = new UnitOfMeasure();
        unitOfMeasure.setvalue(uom.getName());
        return unitOfMeasure;
    }

    private static InvoiceDetailSummary createInvoiceDetailSummary(CustomerInvoiceDto invoiceDto) {
        ShipmentDto shipment = invoiceDto.getShipment();

        InvoiceDetailSummary invoiceDetailSummary = new InvoiceDetailSummary();
        invoiceDetailSummary.setSubtotalAmount(createSubtotalAmount(shipment.calculateLIXSTotal()));
        invoiceDetailSummary.setTax(createTax(shipment.calculateTaxableAmountTotal(), shipment.calculateTaxTotal()));
        invoiceDetailSummary.setShippingAmount(createShippingAmount(shipment.calculateShippingTotal()));
        BigDecimal shipmentTotal = shipment.calculateShipmentTotal();
        invoiceDetailSummary.setGrossAmount(createGrossAmount(shipmentTotal));
        invoiceDetailSummary.setNetAmount(createNetAmount(shipmentTotal));
        invoiceDetailSummary.setDueAmount(createDueAmount(shipmentTotal));
        return invoiceDetailSummary;
    }

    private static ShippingAmount createShippingAmount(BigDecimal amount) {
        ShippingAmount shippingAmount = new ShippingAmount();
        shippingAmount.setMoney(createMoney(amount));
        return shippingAmount;
    }

    private static DueAmount createDueAmount(BigDecimal amount) {
        DueAmount dueAmount = new DueAmount();
        dueAmount.setMoney(createMoney(amount));
        return dueAmount;
    }

    private static NetAmount createNetAmount(BigDecimal amount) {
        NetAmount netAmount = new NetAmount();
        netAmount.setMoney(createMoney(amount));
        return netAmount;
    }

    private static GrossAmount createGrossAmount(BigDecimal amount) {
        GrossAmount grossAmount = new GrossAmount();
        grossAmount.setMoney(createMoney(amount));
        return grossAmount;
    }

    private static Contact getApd() {
        Contact contact = new Contact();
        Name name = new Name();
        name.setXmlLang(ENGLISH);
        name.setvalue(APD_NAME);
        contact.setName(name);
        PostalAddress postalAddress = new PostalAddress();
        postalAddress.setCity(APD_CITY);
        postalAddress.setState(APD_STATE);
        postalAddress.setPostalCode(APD_POSTAL_CODE);
        Street street = new Street();
        street.setvalue(APD_STREET);
        postalAddress.getStreet().add(street);
        Country country = new Country();
        country.setIsoCountryCode(APD_ISO_COUNTRY_CODE);
        country.setvalue(APD_COUNTRY);
        postalAddress.setCountry(country);
        contact.getPostalAddress().add(postalAddress);
        Phone phone = new Phone();
        TelephoneNumber telephoneNumber = new TelephoneNumber();
        CountryCode countryCode = new CountryCode();
        countryCode.setIsoCountryCode(APD_ISO_COUNTRY_CODE);
        countryCode.setvalue(APD_NUMBER_PREFIX);
        telephoneNumber.setCountryCode(countryCode);
        telephoneNumber.setAreaOrCityCode(APD_AREA_OR_CITY_CODE);
        telephoneNumber.setNumber(APD_NUMBER);
        phone.setTelephoneNumber(telephoneNumber);
        contact.getPhone().add(phone);
        return contact;
    }

    private static Contact createContact(Person person, Address address) {
        Contact contact = new Contact();
        if (address.getMiscShipTo() != null && StringUtils.isNotBlank(address.getMiscShipTo().getAddressId())) {
            contact.setAddressID(address.getMiscShipTo().getAddressId());
        }

        Name name = new Name();
        name.setXmlLang(ENGLISH);
        if (person != null && StringUtils.isNotBlank(person.getFormalName())) {
            name.setvalue(person.getFormalName());
        }
        else if (StringUtils.isNotBlank(address.getCompany())) {
            name.setvalue(address.getCompany());
        }
        else {
            name.setvalue("");
        }
        contact.setName(name);

        //Set the postal address
        contact.getPostalAddress().add(createPostalAddress(address));

        //Set the phone number(s)
        if (person != null) {
            for (PhoneNumber phoneNumber : person.getPhoneNumbers()) {
                Phone phone = new Phone();
                phone.setName(phoneNumber.getType().getName());
                phone.setTelephoneNumber(createTelephoneNumber(phoneNumber));
                contact.getPhone().add(phone);
            }
        }
        if (address.getMiscShipTo() != null && StringUtils.isNotBlank(address.getMiscShipTo().getRequesterPhone())) {
            Phone phone = new Phone();
            phone.setName(DEFAULT_NAME);
            phone.setTelephoneNumber(createTelephoneNumber(address.getMiscShipTo().getRequesterPhone()));
            contact.getPhone().add(phone);
        }
        return contact;
    }

    private static PostalAddress createPostalAddress(Address address) {
        PostalAddress postalAddress = new PostalAddress();
        postalAddress.setCity(address.getCity());
        postalAddress.setState(address.getState());
        postalAddress.setPostalCode(address.getZip());

        if (!StringUtils.isEmpty(address.getLine1())) {
            Street street = new Street();
            street.setvalue(address.getLine1());
            postalAddress.getStreet().add(street);
        }

        if (!StringUtils.isEmpty(address.getLine2())) {
            Street street = new Street();
            street.setvalue(address.getLine2());
            postalAddress.getStreet().add(street);
        }

        //Only support for US orders currently
        Country country = new Country();
        country.setIsoCountryCode(APD_ISO_COUNTRY_CODE);
        country.setvalue(APD_COUNTRY);
        postalAddress.setCountry(country);

        return postalAddress;
    }

    private static TelephoneNumber createTelephoneNumber(PhoneNumber phoneNumber) {
        CountryCode countryCode = new CountryCode();
        countryCode.setIsoCountryCode(APD_ISO_COUNTRY_CODE);

        if (StringUtils.isEmpty(phoneNumber.getCountryCode())) {
            countryCode.setvalue(APD_NUMBER_PREFIX);
        }
        else {
            countryCode.setvalue(phoneNumber.getCountryCode());
        }
        TelephoneNumber telephoneNumber = new TelephoneNumber();
        telephoneNumber.setCountryCode(countryCode);
        telephoneNumber.setAreaOrCityCode(phoneNumber.getAreaCode());
        telephoneNumber.setNumber(phoneNumber.getLineNumber());
        return telephoneNumber;
    }

    private static TelephoneNumber createTelephoneNumber(String phoneNumberValue) {
        if (phoneNumberValue == null) {
            return null;
        }
        // First remove all non-numeric characters
        phoneNumberValue = phoneNumberValue.replaceAll("\\D", "");

        // Area code and number value are required for TelephoneNumber.  
        if (phoneNumberValue.length() < 9) {
            return null;
        }
        if (phoneNumberValue.startsWith("1")) {
            // Since no valid US area code begins with "1" and US is the only supported country code, 
            // assume an initial "1" is the country code. 
            phoneNumberValue = phoneNumberValue.substring(1);
            if (phoneNumberValue.length() < 9) {
                return null;
            }
        }

        String areaCode = phoneNumberValue.substring(0, 3);
        String number = phoneNumberValue.substring(3, 9);
        String extension = phoneNumberValue.length() > 9 ? phoneNumberValue.substring(9) : "";

        TelephoneNumber phoneNumber = new TelephoneNumber();
        CountryCode cxmlCountryCode = new CountryCode();
        cxmlCountryCode.setIsoCountryCode(APD_ISO_COUNTRY_CODE);
        cxmlCountryCode.setvalue(APD_NUMBER_PREFIX);
        phoneNumber.setCountryCode(cxmlCountryCode);
        phoneNumber.setAreaOrCityCode(areaCode);
        phoneNumber.setNumber(number);
        if (StringUtils.isNotBlank(extension)) {
            phoneNumber.setExtension(extension);
        }
        return phoneNumber;
    }

    private static SimpleDateFormat buildFormat() {
        return new SimpleDateFormat(CXML_DATE_FORMAT);
    }
}
