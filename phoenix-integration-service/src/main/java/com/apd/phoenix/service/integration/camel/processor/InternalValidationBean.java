package com.apd.phoenix.service.integration.camel.processor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.utility.ErrorEmailService;

@Stateless
public class InternalValidationBean {

    private static final Logger logger = LoggerFactory.getLogger(InternalValidationBean.class);

    @Inject
    private ErrorEmailService errorEmailService;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private OrderStatusBp orderStatusBp;

    public void checkForDuplicateCustomerPo(Exchange e) {

        Long orderId = (Long) e.getIn().getHeader("orderId");
        CustomerOrder customerOrder = customerOrderBp.findById(orderId, CustomerOrder.class);
        boolean foundDuplicate = false;
        boolean valid = false;
        if (customerOrder != null && customerOrder.getCustomerPo() != null
                && StringUtils.isNotEmpty(customerOrder.getCustomerPo().getValue())) {
            for (CustomerOrder otherOrder : customerOrderBp.getOrdersWithCustomerPo(customerOrder.getCustomerPo()
                    .getValue(), customerOrder.getAccount())) {
                //Only look at other orders that have made it through workflow, to ensure one order gets through
                if (customerOrder.getId() != otherOrder.getId() && otherOrder.getPassedInitialValidation()) {
                    foundDuplicate = true;
                    break;
                }
            }
        }
        if (foundDuplicate) {
            logger.error("Found order with duplicate customer PO " + customerOrder.getCustomerPo());
            customerOrderBp.setOrderStatus(customerOrder, orderStatusBp.getOrderStatus(OrderStatusEnum.REJECTED));
            customerOrder = customerOrderBp.internalRejectOrder(customerOrder);
            errorEmailService
                    .sendDuplicateCustomerPOErrorEmail(customerOrder, customerOrder.getCustomerPo().getValue());
        }
        else {
            customerOrder.setPassedInitialValidation(Boolean.TRUE);
            valid = true;
        }
        e.setOut(e.getIn());
        e.getOut().setHeader("valid", valid);
    }

}
