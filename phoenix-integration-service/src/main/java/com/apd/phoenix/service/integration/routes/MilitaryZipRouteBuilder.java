package com.apd.phoenix.service.integration.routes;

import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.camel.converter.MilitaryZipToListTypeConverter;
import com.apd.phoenix.service.integration.tax.military.model.MilitaryZipBindyDto;
import com.apd.phoenix.service.integration.tax.military.update.model.MilitaryZipUpdate;

public class MilitaryZipRouteBuilder extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(MilitaryZipRouteBuilder.class);

    private String createSource;

    private String createOutput;

    private String updateSource;

    private String deleteOutput;

    private static final String zipInternalCreateSource = "direct:militaryZipInternalCreateSource";

    private static final String zipInternalDeleteSource = "direct:militaryZipInternalDeleteSource";

    @Override
    public void configure() throws Exception {
        this.getContext().getTypeConverterRegistry().addTypeConverter(List.class, MilitaryZipBindyDto.class,
                new MilitaryZipToListTypeConverter());

        from(createSource).to(zipInternalCreateSource);

        from(zipInternalCreateSource).convertBodyTo(String.class).log(LoggingLevel.TRACE, "${body}")
        //unmarshall raw data
                .unmarshal().bindy(BindyType.Fixed, MilitaryZipBindyDto.class).convertBodyTo(List.class)
                //Process objects
                .marshal().json(JsonLibrary.Jackson).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                //send to http endpoint
                .log(LoggingLevel.TRACE, "${body}").to(createOutput);

        from(zipInternalDeleteSource).convertBodyTo(String.class).log(LoggingLevel.TRACE, "${body}")
        //unmarshall raw data
                .unmarshal().bindy(BindyType.Fixed, MilitaryZipBindyDto.class)
                //Process objects
                .marshal().json(JsonLibrary.Jackson).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                //send to http endpoint
                .log(LoggingLevel.TRACE, "${body}").to(deleteOutput);

        from(updateSource)
                //unmarshall raw data
                .unmarshal().bindy(BindyType.Fixed, MilitaryZipUpdate.class)
                //Split on each change
                .split().body()
                //route based on action
                .choice().when().jxpath("/in/body/action='I'").log(LoggingLevel.DEBUG, "Inserting").setBody().jxpath(
                        "in/body/record").to(zipInternalCreateSource).when().jxpath("/in/body/action='D'").log(
                        LoggingLevel.DEBUG, "Deleting").setBody().jxpath("in/body/record").to(zipInternalDeleteSource)
                .otherwise().log(LoggingLevel.ERROR, "Unknown action type");
    }

    public String getCreateSource() {
        return createSource;
    }

    public void setCreateSource(String createSource) {
        this.createSource = createSource;
    }

    public String getCreateOutput() {
        return createOutput;
    }

    public void setCreateOutput(String createOutput) {
        this.createOutput = createOutput;
    }

    public String getUpdateSource() {
        return updateSource;
    }

    public void setUpdateSource(String updateSource) {
        this.updateSource = updateSource;
    }

    public String getDeleteOutput() {
        return deleteOutput;
    }

    public void setDeleteOutput(String deleteOutput) {
        this.deleteOutput = deleteOutput;
    }

}
