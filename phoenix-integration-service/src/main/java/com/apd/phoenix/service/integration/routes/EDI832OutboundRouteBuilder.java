package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.model.dataformat.PGPDataFormat;
import org.apache.camel.spi.DataFormat;
import com.apd.phoenix.service.integration.camel.processor.LineItemsToCSVModelProcessor;
import com.apd.phoenix.service.integration.exception.DtoValidationError;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

/**
 * Route Builder for Outbound EDI 832 Routes.
 * 
 * @author RHC
 * 
 */
public class EDI832OutboundRouteBuilder extends EDIRouteBuilder {

    /**
     * Data Source Endpoint. This is where the Original Data Comes From.
     */
    private String dataSource;

    /**
     * Endpoint for Generated USPS Catalog Objects.
     */
    private String catalogSink;

    /**
     * Endpoint for Generated CSV Files.
     */
    private String csvSink;

    /**
     * Outgoing Endpoint. This is the Output where the CSV Should Go when Complete.
     */
    private String csvOutput;

    private String dataFormatModelPackage;

    private String catalogSinkDLQ;
    private String csvSinkDLQ;

    private boolean isEncrypted = false;
    private String keyFileName;
    private String keyUserid;
    private boolean isArmored = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void configure() throws Exception {

        PGPDataFormat pgpSignAndEncrypt = new PGPDataFormat();
        pgpSignAndEncrypt.setKeyFileName(keyFileName);
        pgpSignAndEncrypt.setKeyUserid(keyUserid);
        pgpSignAndEncrypt.setArmored(isArmored);
        // Setup Bindy DataFormat
        DataFormat bindy = new BindyCsvDataFormat(dataFormatModelPackage);

        // Log Class Cast Exceptions
        onException(ClassCastException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Incorrect Type");

        // Log DTO Validation Exceptions
        onException(DtoValidationError.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Invalid Dto");

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in EDI 832 processing: ${exception.stacktrace}");

        // Source Data to USPS Catalog Route
        from(this.dataSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI832.OUTBOUND_832)).process(
                new LineItemsToCSVModelProcessor()).to(this.catalogSink);

        // UspsCatalog to CSV Route
        from(this.catalogSink).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI832.OUTBOUND_832_CATALOG))
                .errorHandler(
                        deadLetterChannel(catalogSinkDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).marshal(bindy).to(this.csvSink);

        // CSV to FTP Route
        from(this.csvSink).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI832.OUTBOUND_832_CSV))
                .errorHandler(
                        deadLetterChannel(csvSinkDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //File name
                //Set output filename
                .setHeader(Exchange.FILE_NAME, simple("cat_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //encryption
                .choice().when(constant(isEncrypted)).log(LoggingLevel.INFO, "unencrypted: ${body}").marshal(
                        pgpSignAndEncrypt).log(LoggingLevel.INFO, "encrypted: ${body}").end().wireTap(
                        getBackupPartnerEndpoint()).to(this.csvOutput);

    }

    /**
     * @return the dataSource
     */
    public String getDataSource() {
        return this.dataSource;
    }

    /**
     * @param dataSource
     *            the dataSource to set
     */
    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @return the catalogSink
     */
    public String getCatalogSink() {
        return this.catalogSink;
    }

    /**
     * @param catalogSink
     *            the catalogSink to set
     */
    public void setCatalogSink(String catalogSink) {
        this.catalogSink = catalogSink;
    }

    /**
     * @return the csvSink
     */
    public String getCsvSink() {
        return this.csvSink;
    }

    /**
     * @param csvSink
     *            the csvSink to set
     */
    public void setCsvSink(String csvSink) {
        this.csvSink = csvSink;
    }

    /**
     * @return the csvOutput
     */
    public String getCsvOutput() {
        return this.csvOutput;
    }

    /**
     * @param csvOutput
     *            the csvOutput to set
     */
    public void setCsvOutput(String csvOutput) {
        this.csvOutput = csvOutput;
    }

    /**
     * @return the catalogSinkDLQ
     */
    public String getCatalogSinkDLQ() {
        return catalogSinkDLQ;
    }

    /**
     * @param catalogSinkDLQ the catalogSinkDLQ to set
     */
    public void setCatalogSinkDLQ(String catalogSinkDLQ) {
        this.catalogSinkDLQ = catalogSinkDLQ;
    }

    /**
     * @return the csvSinkDLQ
     */
    public String getCsvSinkDLQ() {
        return csvSinkDLQ;
    }

    /**
     * @param csvSinkDLQ the csvSinkDLQ to set
     */
    public void setCsvSinkDLQ(String csvSinkDLQ) {
        this.csvSinkDLQ = csvSinkDLQ;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public String getKeyFileName() {
        return keyFileName;
    }

    public void setKeyFileName(String keyFileName) {
        this.keyFileName = keyFileName;
    }

    public String getKeyUserid() {
        return keyUserid;
    }

    public void setKeyUserid(String keyUserid) {
        this.keyUserid = keyUserid;
    }

    public boolean isArmored() {
        return isArmored;
    }

    public void setArmored(boolean isArmored) {
        this.isArmored = isArmored;
    }

    public String getDataFormatModelPackage() {
        return dataFormatModelPackage;
    }

    public void setDataFormatModelPackage(String dataFormatModelPackage) {
        this.dataFormatModelPackage = dataFormatModelPackage;
    }

}
