package com.apd.phoenix.service.integration.routes;

import javax.edi.model.x12.edi855.POAcknowledgement;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.Convert855toOrderLogProcessor;
import com.apd.phoenix.service.integration.camel.processor.EDIX12to997OutboundTranslator;
import com.apd.phoenix.service.integration.camel.processor.MessageServiceBean;
import com.apd.phoenix.service.integration.camel.processor.PoAcknowledgementTranslator;
import com.apd.phoenix.service.integration.camel.processor.SequenceProcessor;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.exception.EDIValidationException;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDI855RouteBuilder extends RouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(EDI855RouteBuilder.class);

    private String source;

    private String output;

    private String orderAckFormattedSource;

    private String orderAckLogSource;

    private String logOutput = "http://placeholder";

    private String outputDLQ;

    private String logOutputDLQ;

    private String logOutputURI;

    private String senderId;

    private String faSource;

    private String faPrepare;

    private String faOutput;

    private String senderCode;

    private String receiverCode;

    private String testIndicator;

    private Boolean send997;

    private int startOrderOffset;

    @Override
    public void configure() throws Exception {
        DataFormat format = new EDIDataFormat(POAcknowledgement.class);

        onException(ClassCastException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Incorrect Type");

        onException(EDIValidationException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "EDI validation exception ${exception.stacktrace}");

        from(source).startupOrder(
                startOrderOffset + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.INBOUND_855))
                .unmarshal(format).choice().when(constant(send997))
                //997
                .to(faSource).end()
                //Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/interchangeEnvelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                //Iterate over each transaction
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/ackHeader/transactionSetHeader/transactionSetControlNumber").multicast()
                .parallelProcessing().to(orderAckLogSource, orderAckFormattedSource);

        from(orderAckLogSource)
                .startupOrder(
                        startOrderOffset
                                + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.INBOUND_855_LOG))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(
                        new Convert855toOrderLogProcessor()).bean(MessageServiceBean.class, "receiveMessage");

        from(orderAckFormattedSource)
                .startupOrder(
                        startOrderOffset
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.INBOUND_855_ORDER_ACK_FORMATTED))
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).bean(PoAcknowledgementTranslator.class,
                        "fromX12EDI").bean(WorkflowServiceCaller.class, "processOrderAcknoweldgement");

        from(faSource)
                .startupOrder(
                        startOrderOffset
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.INBOUND_855_FA_SOURCE))
                //Setup transaction ids
                .bean(SequenceProcessor.class, "generateSequences")
                //
                .setHeader("senderCode", constant(senderCode)).setHeader("receiverCode", constant(receiverCode))
                .setHeader("testIndicator", constant(testIndicator)).process(new EDIX12to997OutboundTranslator())
                .wireTap(faPrepare).choice().when(simple("${header.isValid} != true")).throwException(
                        new EDIValidationException());

        from(faPrepare).startupOrder(
                startOrderOffset
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI855.INBOUND_855_FA_PREPARE))
                .setBody(simple("${header.FA}")).to(faOutput);

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    public String getLogOutput() {
        return logOutput;
    }

    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getFaOutput() {
        return faOutput;
    }

    public void setFaOutput(String faOutput) {
        this.faOutput = faOutput;
    }

    public String getSenderCode() {
        return senderCode;
    }

    public void setSenderCode(String senderCode) {
        this.senderCode = senderCode;
    }

    public String getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    public String getTestIndicator() {
        return testIndicator;
    }

    public void setTestIndicator(String testIndicator) {
        this.testIndicator = testIndicator;
    }

    public String getFaSource() {
        return faSource;
    }

    public void setFaSource(String faSource) {
        this.faSource = faSource;
    }

    public String getFaPrepare() {
        return faPrepare;
    }

    public void setFaPrepare(String faPrepare) {
        this.faPrepare = faPrepare;
    }

    public String getOrderAckFormattedSource() {
        return orderAckFormattedSource;
    }

    public void setOrderAckFormattedSource(String orderAckFormattedSource) {
        this.orderAckFormattedSource = orderAckFormattedSource;
    }

    public String getOrderAckLogSource() {
        return orderAckLogSource;
    }

    public void setOrderAckLogSource(String orderAckLogSource) {
        this.orderAckLogSource = orderAckLogSource;
    }

    /**
     * @return the send997
     */
    public Boolean getSend997() {
        return send997;
    }

    /**
     * @param send997 the send997 to set
     */
    public void setSend997(Boolean send997) {
        this.send997 = send997;
    }

    /**
     * @return the startOrderOffset
     */
    public int getStartOrderOffset() {
        return startOrderOffset;
    }

    /**
     * @param startOrderOffset the startOrderOffset to set
     */
    public void setStartOrderOffset(int startOrderOffset) {
        this.startOrderOffset = startOrderOffset;
    }

}
