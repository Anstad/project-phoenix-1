/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.integration.camel.processor.PurchaseOrderTranslator5010;
import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.EDIX12v5010to997OutboundTranslator;
import com.apd.phoenix.service.integration.camel.processor.OrderRequestProcessor;
import com.apd.phoenix.service.integration.camel.processor.SequenceProcessor;
import com.apd.phoenix.service.integration.camel.processor.WalmartPartnerDeterminer;
import com.apd.phoenix.service.integration.exception.EDIValidationException;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import javax.edi.model.x12.v5010.edi850.PurchaseOrder;

/**
 *
 * @author nreidelb
 */
public class EDI850v5010InboundRouteBuilder extends EDIRouteBuilder {

    private String inboundPurchaseOrderSource;
    private String inboundFormatSource;
    private String inboundLogSource;

    private String senderId;

    private String faSource;
    private String faPrepare;
    private String faOutput;

    private String senderCode;

    private String receiverCode;

    private String testIndicator;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 850v5010 inbound processing: ${exception.stacktrace}");

        onException(EDIValidationException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "EDI validation exception");

        from(inboundPurchaseOrderSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.INBOUND_850v5010))
        //sets the partnerId so that it can be set on the order
                .setHeader("partnerId", constant(senderId)).setHeader("originalMessage").jxpath("in/body")
                //unmarshal
                .unmarshal(new EDIDataFormat(PurchaseOrder.class)).bean(WalmartPartnerDeterminer.class,
                        "resetPartnerId")
                //997
                .to(faSource)
                //Send to processing route
                .to(inboundFormatSource);

        from(inboundFormatSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.INBOUND_850v5010_FORMAT_SOURCE))
                .setHeader("interchangeId").jxpath("/in/body/interchangeEnvelopeHeader/interchangeControlNumber")
                .setHeader("groupId").jxpath("/in/body/groupEnvelopeHeader/groupControlNumber")
                //Translate incoming jaeb pojo to dto
                .bean(PurchaseOrderTranslator5010.class, "fromX12EDI")
                //Setup order request
                .bean(OrderRequestProcessor.class, "setupOrderRequest")
                //Log incoming request
                .wireTap(inboundLogSource).split().jxpath("/in/body").bean(OrderRequestProcessor.class, "processOrder");

        from(faSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.INBOUND_850v5010_FA_SOURCE))
                //Setup transaction ids
                .bean(SequenceProcessor.class, "generateSequences")
                //
                .setHeader("senderCode", constant(senderCode)).setHeader("receiverCode", constant(receiverCode))
                .setHeader("testIndicator", constant(testIndicator)).process(new EDIX12v5010to997OutboundTranslator())
                .wireTap(faPrepare).choice().when(simple("${header.isValid} != true")).throwException(
                        new EDIValidationException());

        from(faPrepare)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.INBOUND_850v5010_FA_PREPARE))
                .setBody(simple("${header.FA}")).to(faOutput);

        from(inboundLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v5010.INBOUND_850v5010_LOG))
                .split().jxpath("/in/body").setHeader("apdPo").jxpath("/in/body/apdPoNumber").setHeader(
                        "communicationType", simple("INBOUND")).setHeader("messageEventType",
                        simple(OrderLogDto.EventTypeDto.PURCHASE_ORDER_SENT.toString())).bean(
                        EDITransactionLogger.class, "logNewPoEDITransaction");
    }

    /**
     * @return the inboundPurchaseOrderSource
     */
    public String getInboundPurchaseOrderSource() {
        return inboundPurchaseOrderSource;
    }

    /**
     * @param inboundPurchaseOrderSource the inboundPurchaseOrderSource to set
     */
    public void setInboundPurchaseOrderSource(String inboundPurchaseOrderSource) {
        this.inboundPurchaseOrderSource = inboundPurchaseOrderSource;
    }

    /**
     * @return the inboundFormatSource
     */
    public String getInboundFormatSource() {
        return inboundFormatSource;
    }

    /**
     * @param inboundFormatSource the inboundFormatSource to set
     */
    public void setInboundFormatSource(String inboundFormatSource) {
        this.inboundFormatSource = inboundFormatSource;
    }

    /**
     * @return the inboundLogSource
     */
    public String getInboundLogSource() {
        return inboundLogSource;
    }

    /**
     * @param inboundLogSource the inboundLogSource to set
     */
    public void setInboundLogSource(String inboundLogSource) {
        this.inboundLogSource = inboundLogSource;
    }

    public String getFaSource() {
        return faSource;
    }

    public void setFaSource(String faSource) {
        this.faSource = faSource;
    }

    public String getFaPrepare() {
        return faPrepare;
    }

    public void setFaPrepare(String faPrepare) {
        this.faPrepare = faPrepare;
    }

    public String getFaOutput() {
        return faOutput;
    }

    public void setFaOutput(String faOutput) {
        this.faOutput = faOutput;
    }

    public String getSenderCode() {
        return senderCode;
    }

    public void setSenderCode(String senderCode) {
        this.senderCode = senderCode;
    }

    public String getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(String receiverCode) {
        this.receiverCode = receiverCode;
    }

    public String getTestIndicator() {
        return testIndicator;
    }

    public void setTestIndicator(String testIndicator) {
        this.testIndicator = testIndicator;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

}
