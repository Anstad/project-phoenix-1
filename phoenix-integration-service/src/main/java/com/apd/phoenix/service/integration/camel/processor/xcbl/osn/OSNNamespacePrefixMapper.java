/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

/**
 *
 * @author rhc
 */
public class OSNNamespacePrefixMapper extends NamespacePrefixMapper {

    private static final String ENVELOPE_PREFIX = "osn";
    private static final String ENVELOPE_PREFIX_URI = "http://perfect.com/LiteEnvelope/xcbl";

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if (ENVELOPE_PREFIX_URI.equals(namespaceUri)) {
            return ENVELOPE_PREFIX;
        }
        else {
            return suggestion;
        }
    }

    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] { ENVELOPE_PREFIX };
    }

}
