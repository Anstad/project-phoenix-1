package com.apd.phoenix.service.integration.solr;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CatalogXItemDocumentMapper {

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItemDocumentMapper.class);

    private CatalogXItemDocumentMapper() {
    }

    /**
     * Maps between the item query results and item object.
     *
     * @param results
     * @param solrDocument
     */
    public static void mapBaseQueryResultsToSolrDocument(Map<String, Object> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        solrDocument.setCatalogxitemId(getLongValue(results.get(SolrAttributeConstants.CATALOGXITEM_ID)));
        solrDocument.setName((String) results.get(SolrAttributeConstants.NAME));
        solrDocument.setCustomerSku((String) results.get(SolrAttributeConstants.CUSTOMER_SKU));
        solrDocument.setDescription((String) results.get(SolrAttributeConstants.DESCRIPTION));
        solrDocument.setCatalogId(getLongValue(results.get(SolrAttributeConstants.CATALOG_ID)));
        solrDocument.setPrice(getFloatValue(results.get(SolrAttributeConstants.PRICE)));
        solrDocument.setSearchTerms((String) results.get(SolrAttributeConstants.SEARCHTERMS));
        solrDocument.setCoreItemExpirationDate((Date) results.get(SolrAttributeConstants.COREEXPIREDATE));
        solrDocument.setCoreItemStartDate((Date) results.get(SolrAttributeConstants.CORESTARTDATE));
        solrDocument.setUnitOfMeasureName((String) results.get(SolrAttributeConstants.UOM_NAME));
        solrDocument.setManufacturerName((String) results.get(SolrAttributeConstants.MNAME));
        solrDocument.setCategory((String) results.get(SolrAttributeConstants.CATEGORY));
        solrDocument.setHierarchyPath((String) results.get(SolrAttributeConstants.HIERARCHY_PATH));
        solrDocument.setHierarchy_path_string((String) results.get(SolrAttributeConstants.HIERARCHY_PATH));
        solrDocument.setVendorName((String) results.get(SolrAttributeConstants.VENDOR_NAME));
        solrDocument.setItem_status((String) results.get(SolrAttributeConstants.STATUS));
        solrDocument.setMultiple(getIntegerValue(results.get(SolrAttributeConstants.MULTIPLE)));
        solrDocument.setMinimum(getIntegerValue(results.get(SolrAttributeConstants.MINIMUM)));
        solrDocument.setHierarchyParentId(getLongValue(results.get(SolrAttributeConstants.HIERARCHY_PARENT_ID)));
        solrDocument.setCustomerReplacementSku((String) results.get(SolrAttributeConstants.REP_SKU));
        solrDocument.setCustomerReplacementVendor((String) results.get(SolrAttributeConstants.REP_VEND_NAME));
        solrDocument.setCustomOrder((String) results.get(SolrAttributeConstants.CUSTOM_ORDER));
        solrDocument.setSpecialOrder((String) results.get(SolrAttributeConstants.SPECIAL_ORDER));
        String overridesString = (String) results.get(SolrAttributeConstants.OVERRIDE_CATALOGS);
        List<String> overridesList = new ArrayList<String>();
        if (StringUtils.isNotBlank(overridesString)) {
            for (String override : overridesString.split(",")) {
                overridesList.add(override);
            }
        }
        solrDocument.setOverrides_id(overridesList);
    }

    public static Float getFloatValue(Object o) {
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).floatValue();
        }
        else {
            return null;
        }
    }

    public static Long getLongValue(Object o) {
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).longValue();
        }
        else if (o instanceof Long) {
            return (Long) o;
        }
        else {
            return null;
        }
    }

    public static Integer getIntegerValue(Object o) {
        if (o instanceof Integer) {
            return ((Integer) o);
        }
        else if (o instanceof Long) {
            return (Integer) o;
        }
        else {
            return null;
        }
    }

    public static void mapImageToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            if (StringUtils.isNotBlank((String) result.get("IMAGEURL"))) {
                String imageUrl = (String) result.get("IMAGEURL");
                solrDocument.getImageUrl().add(imageUrl);
            }
        }
    }

    public static void mapSkuToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.sku(result);
            if (StringUtils.isNotBlank((String) result.get("MANUFACTURER_SKU"))) {
                solrDocument.setManufacturerSku((String) result.get("MANUFACTURER_SKU"));
            }
            if (StringUtils.isNotBlank((String) result.get("APD_SKU"))) {
                solrDocument.setApdSku((String) result.get("APD_SKU"));
            }
            if (StringUtils.isNotBlank((String) result.get("VENDOR_SKU"))) {
                solrDocument.setVendorSku((String) result.get("VENDOR_SKU"));
            }
        }
    }

    public static void mapClassificationIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.classificationIcon(result);
            if (StringUtils.isNotBlank((String) result.get("classificationIconMap"))) {
                String classificationIconMapValue = (String) result.get("classificationIconMap");
                solrDocument.getClassificationIconMap().add(classificationIconMapValue);
            }
        }
    }

    public static void mapPropertyIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.propertyIcon(result);
            if (StringUtils.isNotBlank((String) result.get("propertyIconMap"))) {
                solrDocument.setPropertyIconMap((String) result.get("propertyIconMap"));
            }
        }
    }

    public static void mapCategoryXPropertyIconToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.propertyIcon(result);
            if (StringUtils.isNotBlank((String) result.get("catXPropertyIconMap"))) {
                String catXPropertyIconMapValues = (String) result.get("catXPropertyIconMap");
                solrDocument.getCatXPropertyIconMap().add(catXPropertyIconMapValues);
            }
            if (StringUtils.isNotBlank((String) result.get("ItemPropertyNameValuePair"))) {
                String itemPropertyNameValuePairValues = (String) result.get("ItemPropertyNameValuePair");
                solrDocument.getItemPropertyNameValuePair().add(itemPropertyNameValuePairValues);
            }
        }
    }

    public static void mapPropertyToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            SolrTransformationAdapter.nameValue(result);
            if (StringUtils.isNotBlank((String) result.get("filters"))) {
                String filtersValue = (String) result.get("filters");
                String[] splitString = filtersValue.split(" \\| ");
                if (splitString.length == 2 && !"null".equals(splitString[1])) {
                    solrDocument.getFilters().add(filtersValue);
                }
            }
        }
    }

    public static void mapFavoritesListResultsToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            String companyFavoritesListIdValues = result.get("FAVORITES_LIST_ID") == null ? "" : ((BigDecimal) result
                    .get("FAVORITES_LIST_ID")).toString();
            if (StringUtils.isNotBlank(companyFavoritesListIdValues)) {
                solrDocument.getCompanyFavoritesListIds().add(companyFavoritesListIdValues);
            }
        }
    }

    public static void mapMatchbookResultsToItem(List<Map<String, Object>> results, SolrDocumentPojo solrDocument) {
        if (results == null) {
            return;
        }
        for (Map<String, Object> result : results) {
            String matchBookIds = result.get("MB_ID") == null ? "" : ((BigDecimal) result.get("MB_ID")).toString();
            if (StringUtils.isNotBlank(matchBookIds)) {
                solrDocument.getMb_id().add(matchBookIds);
            }
        }
    }

}
