package com.apd.phoenix.service.integration.hapoller;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apache.commons.lang.StringUtils;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.CatalogXmlHAPollerRouteBuilder;
import com.apd.phoenix.service.integration.routes.EDIInboundRouteBuilder;
import com.apd.phoenix.service.integration.routes.FlatFileEntryPointRouteBuilder;
import com.apd.phoenix.service.integration.routes.InternalMessageValidationRouteBuilder;
import com.apd.phoenix.service.integration.routes.MarfieldShipmentFileRouteBuilder;
import com.apd.phoenix.service.integration.routes.PickListRouteBuilder;
import com.apd.phoenix.service.integration.routes.TaxHAPollerRouteBuilder;
import com.apd.phoenix.service.integration.routes.UsscoShipManifestRouteBuilder;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLPartnerDeterminerRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Named
@Singleton
public class HAFTPServiceBean implements HAFTPPollerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(HAFTPServiceBean.class.getCanonicalName());
    /**
     * A flag whether the service is started.
     */
    private final AtomicBoolean started = new AtomicBoolean(false);

    @Inject
    private CdiCamelContext camelContext;

    Properties commonProperties = PropertiesLoader.getAsProperties("camel.edi-inbound.integration");

    private String nodeName;

    private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

    public Injector<ServerEnvironment> getEnvInjector() {
        return this.env;
    }

    /**
     * @return the name of the server node
     */
    public String getValue() throws IllegalStateException, IllegalArgumentException {
        if (!started.get()) {
            throw new IllegalStateException("The service '" + this.getClass().getName() + "' is not ready!");
        }
        return this.nodeName;
    }

    public void start(StartContext arg0) throws StartException {
        if (!started.compareAndSet(false, true)) {
            throw new StartException("The service is still started!");
        }
        LOGGER.info("Start service '" + this.getClass().getName() + "'");
        this.nodeName = this.env.getValue().getNodeName();
        Properties commonProperties = PropertiesLoader.getAsProperties("camel.edi-inbound.integration");
        Properties sharedXCBLProperties = PropertiesLoader.getAsProperties("camel.xcbl.integration");
        Properties taxProperties = PropertiesLoader.getAsProperties("tax.integration");

        //camelContext = new DefaultCamelContext();

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        PackageScanClassResolver jbossResolver = new JBossPackageScanClassResolver();

        camelContext.setPackageScanClassResolver(jbossResolver);

        //Tax
        TaxHAPollerRouteBuilder taxRoute = new TaxHAPollerRouteBuilder();
        taxRoute.setCreateSource(taxProperties.getProperty("taxCreateSource"));
        taxRoute.setUpdateSource(taxProperties.getProperty("taxUpdateSource"));
        taxRoute.setZipCreateSource(taxProperties.getProperty("zipCreateSource"));
        taxRoute.setZipUpdateSource(taxProperties.getProperty("zipUpdateSource"));
        taxRoute.setMilitaryZipCreateSource(taxProperties.getProperty("militaryZipCreateSource"));
        taxRoute.setMilitaryZipUpdateSource(taxProperties.getProperty("militaryZipUpdateSource"));
        taxRoute.setOutput(taxProperties.getProperty("taxHAEndpoint"));
        try {
            camelContext.addRoutes(taxRoute);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add camel routes: ", e1);
        }

        Properties itemProperties = PropertiesLoader.getAsProperties("camel.item.integration");
        //USSCO ECDB2
        CatalogXmlHAPollerRouteBuilder ecdb2Route = new CatalogXmlHAPollerRouteBuilder();
        ecdb2Route.setItemSource(itemProperties.getProperty("itemSource"));
        ecdb2Route.setRelationshipSource(itemProperties.getProperty("relationshipSource"));
        ecdb2Route.setOutput(itemProperties.getProperty("ecdb2HAEndpoint"));
        try {
            camelContext.addRoutes(ecdb2Route);
        }
        catch (Exception e1) {
            LOGGER.error("Problem adding routes for ECDB2 changes", e1);
        }

        //Picklist
        Properties integrationProperties = PropertiesLoader.getAsProperties("solomon.integration");
        PickListRouteBuilder pickListRoute = new PickListRouteBuilder();
        pickListRoute.setSource(integrationProperties.getProperty("solomonPickListSource"));
        try {
            camelContext.addRoutes(pickListRoute);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add camel routes: ", e1);
        }

        //Edi

        String partners = commonProperties.getProperty("partners");
        String[] partnersList = StringUtils.split(partners, ',');

        if (partnersList != null) {
            for (String partner : partnersList) {
                if (StringUtils.isBlank(partner)) {
                    continue;
                }
                LOGGER.info("Adding partner EDI inbound to JMS: {}", partner);
                Properties partnerProperties = PropertiesLoader.getAsProperties(partner + ".integration");
                EDIInboundRouteBuilder ediInboundRouteBuilder = new EDIInboundRouteBuilder();
                ediInboundRouteBuilder.setOutput(commonProperties.getProperty("ediInbound"));
                ediInboundRouteBuilder.setOutputDLQ("");
                ediInboundRouteBuilder.setPartnerId(partner);
                ediInboundRouteBuilder.setSource(partnerProperties.getProperty("ediInboundSource"));
                try {
                    camelContext.addRoutes(ediInboundRouteBuilder);
                }
                catch (Exception e) {
                    LOGGER.error("An error occured:", e);
                }
            }
        }

        InternalMessageValidationRouteBuilder internalMessageValidationRouteBuilder = new InternalMessageValidationRouteBuilder();
        internalMessageValidationRouteBuilder.setSource("activemq:queue:queue.internal-validation-queue");
        try {
            camelContext.addRoutes(internalMessageValidationRouteBuilder);
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
        try {
            //XCBL
            XCBLPartnerDeterminerRouteBuilder xCBLPartnerDeterminerRouteBuilder = new XCBLPartnerDeterminerRouteBuilder();
            xCBLPartnerDeterminerRouteBuilder.setInboundXCBLSource(sharedXCBLProperties
                    .getProperty("inbound.xcbl.camel.entry.point"));
            xCBLPartnerDeterminerRouteBuilder.setInboundXCBLSink(sharedXCBLProperties
                    .getProperty("inbound.xcbl.entry.point.queue"));
            camelContext.addRoutes(xCBLPartnerDeterminerRouteBuilder);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add xCBL camel routes: ", e1);
        }

        try {
            //Ussco Manifest
            Properties usscoProperties = PropertiesLoader.getAsProperties("ussco.integration");
            UsscoShipManifestRouteBuilder usscoShipManifestRouteBuilder = new UsscoShipManifestRouteBuilder();
            usscoShipManifestRouteBuilder.setSource(usscoProperties.getProperty("manifestSource"));
            camelContext.addRoutes(usscoShipManifestRouteBuilder);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add camel routes: ", e1);
        }

        try {
            //Marfield processing
            int startOrderOffset = 1;
            Properties marfieldProperties = PropertiesLoader.getAsProperties("marfield.integration");

            FlatFileEntryPointRouteBuilder flatFileEntryPointRouteBuilder = new FlatFileEntryPointRouteBuilder();
            flatFileEntryPointRouteBuilder.setTransactionSource(marfieldProperties.getProperty("inboundEdiSource"));
            flatFileEntryPointRouteBuilder.setStartOrderOffset(startOrderOffset);
            startOrderOffset++;
            flatFileEntryPointRouteBuilder.setPartnerId("marfield");
            flatFileEntryPointRouteBuilder.setProcessorEndpoint(marfieldProperties.getProperty("processorEndpoint"));
            camelContext.addRoutes(flatFileEntryPointRouteBuilder);

            MarfieldShipmentFileRouteBuilder marfieldShipmentFileRouteBuilder = new MarfieldShipmentFileRouteBuilder();
            marfieldShipmentFileRouteBuilder.setSource(marfieldProperties.getProperty("processorEndpoint"));
            marfieldShipmentFileRouteBuilder.setShipmentNoticeLogSource(marfieldProperties
                    .getProperty("shipmentNoticeLogSource"));
            marfieldShipmentFileRouteBuilder.setShipmentNoticeFormattedSource(marfieldProperties
                    .getProperty("shipmentNoticeFormattedSource"));
            marfieldShipmentFileRouteBuilder.setLogOutputDLQ(marfieldProperties.getProperty("shipmentNoticeLogDLQ"));
            marfieldShipmentFileRouteBuilder.setOutputDLQ(marfieldProperties.getProperty("shipmentNoticeDLQ"));
            marfieldShipmentFileRouteBuilder.setStartupOrderOffset(startOrderOffset);
            camelContext.addRoutes(marfieldShipmentFileRouteBuilder);
        }
        catch (Exception e1) {
            LOGGER.error("Could not add camel routes: ", e1);
        }

        try {
            camelContext.start();
        }
        catch (Exception e) {
            LOGGER.error("An error occured:", e);
        }
    }

    public void stop(StopContext arg0) {
        if (!started.compareAndSet(true, false)) {
            LOGGER.warn("The service '" + this.getClass().getName() + "' is not active!");
        }
        else {
            LOGGER.info("Stop service '" + this.getClass().getName() + "'");
        }
        try {
            camelContext.stop();
        }
        catch (Exception e) {
            LOGGER.error("Could not stop camel: ", e);
        }
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
