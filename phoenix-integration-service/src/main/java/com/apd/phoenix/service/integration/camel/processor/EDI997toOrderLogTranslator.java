package com.apd.phoenix.service.integration.camel.processor;

import javax.edi.model.x12.edi997.segment.FunctionalAcknowledgementBody;
import javax.edi.model.x12.edi997.segment.TransactionResponseGroup;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class EDI997toOrderLogTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997toOrderLogTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }

        if (exchange.getIn().getBody() instanceof TransactionResponseGroup) {
            TransactionResponseGroup trg = (TransactionResponseGroup) exchange.getIn().getBody();
            String transactionId = trg.getTransactionSetResponseHeader().getTransactionSetControlNumber();
            LOG.debug("transaction id: {}", transactionId);
            if (transactionId == null) {
                throw new Exception("Transaction id not found");
            }
            exchange.getIn().setHeader("transactionId", transactionId);
        }
        if (exchange.getIn().getBody() instanceof FunctionalAcknowledgementBody) {
            FunctionalAcknowledgementBody body = (FunctionalAcknowledgementBody) exchange.getIn().getBody();
            String groupControlNumber = body.getHeader().getFunctionalGroupResponseHeader().getGroupControlNumber();
            if (groupControlNumber == null) {
                throw new Exception("Group Control Number not found");
            }
            exchange.getIn().setHeader("targetGroupControlNumber", groupControlNumber);
        }
        exchange.getIn().setHeader("messageEventType", EventTypeDto.FUNCTIONAL_ACKNOWLEDGEMENT);

    }

}
