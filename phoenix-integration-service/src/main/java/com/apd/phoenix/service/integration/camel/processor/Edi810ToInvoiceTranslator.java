/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.business.SkuTypeBp;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import javax.edi.model.x12.edi810.segment.Header;
import javax.edi.model.x12.edi810.segment.InvoiceBeginningSegment;
import javax.edi.model.x12.edi810.segment.InvoiceBody;
import javax.edi.model.x12.edi810.segment.InvoiceItemGroup;
import javax.edi.model.x12.edi810.segment.Trailer;
import javax.edi.model.x12.segment.BaselineItemData;
import javax.edi.model.x12.edi810.segment.InvoiceAddressGroup;
import javax.edi.model.x12.segment.NoteSpecialInstructions;
import javax.edi.model.x12.segment.TotalMonetaryValueSummary;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AccountDto;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PoNumberDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.VendorDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import java.util.HashSet;
import javax.edi.model.x12.segment.AddressInformation;
import javax.edi.model.x12.segment.AllowanceChargeOrService;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.TermsOfSale;

/**
 *
 * @author nreidelb
 */
public class Edi810ToInvoiceTranslator implements Processor {

    public static final String APD_PO_TYPE = "APD";
    private static final Logger LOG = LoggerFactory.getLogger(Edi810ToInvoiceTranslator.class);
    public static final String BILLING_TYPE_CODE_QUALIFIER = "BLT";
    public static final String SHIP_TO_ADDRESS_IDENTIFICATION_CODE = "ST";
    public static final String BILL_TO_IDENTIFICATION_CODE = "BT";
    public static final String BUYER_SKU_QUALIFIER = "SK";
    public static final String VENDOR_SKU_QUALIFIER = "VN";
    public static final String CUSTOMER_ORDER_QUALIFIER = "CA";
    public static final String REMIT_TO_ADDRESS_IDENTIFICATION_CODE = "RE";
    public static final String CUSTOMER_PO_TYPE = "customer";
    public static final String DEBIT_TRANSACTION = "DR";
    public static final String CREDIT_TRANSACTION = "CR";

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        InvoiceBody invoice = (InvoiceBody) exchange.getIn().getBody();
        String partnerId = exchange.getIn().getHeader("senderId", String.class);
        VendorInvoiceDto invoiceDto = createCustomerOrderDto810(invoice, partnerId);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(invoiceDto, VendorInvoiceDto.class);
    }

    private VendorInvoiceDto createCustomerOrderDto810(InvoiceBody body, String partnerId) throws Exception {
        VendorInvoiceDto invoice = createInvoiceDto(body);
        PurchaseOrderDto customerOrderDto = createCustomerOrderDto(body, partnerId);
        List<LineItemDto> dtoItems = createLineItemDtos(body, partnerId);
        invoice.setActualItems(dtoItems);
        AccountDto account = new AccountDto();
        for (InvoiceAddressGroup group : body.getHeader().getInvoiceAddressGroups()) {
            AddressDto address = generateAddress(group);
            switch (group.getName().getEntityIdentifierCode()) {
                case SHIP_TO_ADDRESS_IDENTIFICATION_CODE:
                    account.setName(group.getName().getName());
                    customerOrderDto.setShipToAddressDto(address);
                    customerOrderDto.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
                    customerOrderDto.getShipToAddressDto().getMiscShipToDto().setAddressId(
                            group.getName().getIdentificationCode());
                    break;
                case BILL_TO_IDENTIFICATION_CODE:
                    //set bill to address
                    account.getAddresses().add(address);
                    break;
                case REMIT_TO_ADDRESS_IDENTIFICATION_CODE:
                default:
                    //do nothing, this address isn't needed
                    break;
            }
        }
        customerOrderDto.setAccount(account);
        invoice.setCustomerOrderDto(customerOrderDto);
        if (invoice instanceof VendorCreditInvoiceDto) {
            invoice = makeCreditChanges(dtoItems, (VendorCreditInvoiceDto) invoice);
        }
        return invoice;
    }

    private PoNumberDto createPoNumberDto(final String poNumber, PurchaseOrderDto customerOrderDto, String type) {
        PoNumberDto customerPoNumberDto = new PoNumberDto();
        customerPoNumberDto.setValue(poNumber);
        customerPoNumberDto.setType(type);
        return customerPoNumberDto;
    }

    private List<LineItemDto> createLineItemDtos(InvoiceBody body, String parnterId){
    	
    	BigDecimal invoiceTax = BigDecimal.ZERO;
    	BigDecimal invoiceShipping = BigDecimal.ZERO;
    	if (body.getTrailer() != null && body.getTrailer().getAllowanceChargeOrServices() != null) {
	    	for (AllowanceChargeOrService sac : body.getTrailer().getAllowanceChargeOrServices()) {
	    		if (sac != null) {
		    		if ("H750".equals(sac.getSpecialChargeOrAllowanceCode()) && sac.getAllowanceOrChargeTotalAmount() != null) {
		    			invoiceTax = sac.getAllowanceOrChargeTotalAmount();
		    		}
		    		if ("D240".equals(sac.getSpecialChargeOrAllowanceCode()) && sac.getAllowanceOrChargeTotalAmount() != null) {
		    			//shipping has decimal omitted, so have to divide by 100
		    			invoiceShipping = sac.getAllowanceOrChargeTotalAmount().divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
		    		}
	    		}
	    	}
    	}
        //list to track which units of measure have been created
        List<UnitOfMeasureDto> unitsOfMeasure = new ArrayList<>();
        List<LineItemDto> dtoItems = new ArrayList<>();
        Iterable<InvoiceItemGroup> invoiceItems = body.getDetail().getInvoiceItems();        
        for(InvoiceItemGroup item:invoiceItems){
            BaselineItemData baseLineItemData = item.getBaselineItemData();
            LineItemDto lineItem = new LineItemDto();
            final StringBuilder stringBuilder = new StringBuilder(baseLineItemData.getQuantityInvoiced());
            final Integer quantity = Integer.parseInt(stringBuilder.toString());
            lineItem.setQuantity(new BigInteger(quantity+""));
            boolean found = false;
            String unit = baseLineItemData.getUnitOfMeasureCode();
            //checks to see if unit of measure has already been created
            for(UnitOfMeasureDto uom:unitsOfMeasure){
                if(uom.getName().equals(unit)){
                    lineItem.setUnitOfMeasure(uom);
                    found = true;
                    break;
                }
            }
            if(!found){
                UnitOfMeasureDto unitOfMeasure = new UnitOfMeasureDto();
                unitOfMeasure.setName(baseLineItemData.getUnitOfMeasureCode());            
                lineItem.setUnitOfMeasure(unitOfMeasure);
                unitsOfMeasure.add(unitOfMeasure);
            }
            lineItem.setCost(baseLineItemData.getUnitPrice());
            if(baseLineItemData.getProductServiceIDQualifier1()!=null){
                setSkuByQualifier(lineItem,baseLineItemData.getProductServiceIDQualifier1(),
                        baseLineItemData.getProductServiceID1());
            }
            if(baseLineItemData.getProductServiceIDQualifier2()!=null){
                setSkuByQualifier(lineItem,baseLineItemData.getProductServiceIDQualifier2(),
                        baseLineItemData.getProductServiceID2());
            }
            if(item.getProductItemDescription()!=null){
                lineItem.setDescription(item.getProductItemDescription().getDescription());
            } else {
                lineItem.setDescription("Received lineItem on Invoice without description");
            }
            
            ArrayList<String> comments = new ArrayList<>();
            if(item.getNoteSpecialInstructions()!=null){
                for(NoteSpecialInstructions comment:item.getNoteSpecialInstructions()){
                    comments.add(comment.getFreeFormMessage());
                }
            }

            String lineNumberString = baseLineItemData.getAssignedIdentificationNumber();
        	if (StringUtils.isNotBlank(lineNumberString)) {
                try {
                	lineItem.setLineNumber(Integer.parseInt(lineNumberString));
                } catch (NumberFormatException e) {
            		LOG.warn("Exception while parsing Line Number: " + lineNumberString);
                }
        	}
            
            lineItem.setVendorComments(comments);
            lineItem.setVendorName(parnterId);
            dtoItems.add(lineItem);
        }
        this.distributeTaxAndShipping(dtoItems, invoiceTax, invoiceShipping);
        return dtoItems;
    }

    private void distributeTaxAndShipping(List<LineItemDto> dtoItems, BigDecimal totalTax, BigDecimal totalShipping) {
        if (totalTax == null) {
            totalTax = BigDecimal.ZERO;
        }
        if (totalShipping == null) {
            totalShipping = BigDecimal.ZERO;
        }
        BigDecimal unassignedTax = totalTax;
        BigDecimal unassignedShipping = totalShipping;
        BigDecimal merchandiseSum = BigDecimal.ZERO;
        for (LineItemDto item : dtoItems) {
            if (item != null && item.getCost() != null && item.getQuantity() != null) {
                merchandiseSum = item.getCost().multiply(new BigDecimal(item.getQuantity())).add(merchandiseSum);
            }
        }
        LineItemDto lastItem = null;
        for (LineItemDto item : dtoItems) {
            lastItem = item;
            if (merchandiseSum != null && merchandiseSum.compareTo(BigDecimal.ZERO) > 0 && item != null) {
                BigDecimal merchandiseFraction = BigDecimal.ZERO;
                if (item.getQuantity() != null && item.getCost() != null) {
                    merchandiseFraction = item.getCost().multiply(new BigDecimal(item.getQuantity())).divide(
                            merchandiseSum, 2, RoundingMode.HALF_UP);
                }
                //set tax on the item
                item.setMaximumTaxToCharge(totalTax.multiply(merchandiseFraction).setScale(2, RoundingMode.HALF_UP));
                if (item.getMaximumTaxToCharge() != null) {
                    if (item.getMaximumTaxToCharge().compareTo(unassignedTax) > 0) {
                        item.setMaximumTaxToCharge(unassignedTax);
                    }
                    unassignedTax = unassignedTax.subtract(item.getMaximumTaxToCharge());
                }
                //set shipping on the item
                item.setEstimatedShippingAmount(totalShipping.multiply(merchandiseFraction).setScale(2,
                        RoundingMode.HALF_UP));
                if (item.getEstimatedShippingAmount() != null) {
                    if (item.getEstimatedShippingAmount().compareTo(unassignedShipping) > 0) {
                        item.setEstimatedShippingAmount(unassignedShipping);
                    }
                    unassignedShipping = unassignedShipping.subtract(item.getEstimatedShippingAmount());
                }
            }
        }
        //adds the remaining, unassigned shipping and tax
        if (lastItem != null) {
            if (lastItem.getMaximumTaxToCharge() != null && unassignedTax != null) {
                lastItem.setMaximumTaxToCharge(lastItem.getMaximumTaxToCharge().add(unassignedTax));
            }
            if (lastItem.getEstimatedShippingAmount() != null && unassignedShipping != null) {
                lastItem.setEstimatedShippingAmount(lastItem.getEstimatedShippingAmount().add(unassignedShipping));
            }
        }
    }

    private VendorInvoiceDto createInvoiceDto( InvoiceBody body){
        final Header header = body.getHeader();
        InvoiceBeginningSegment big = header.getBeginningSegmentforInvoice();
        VendorInvoiceDto invoice = new VendorInvoiceDto();
        if(CREDIT_TRANSACTION.equals(big.getTransactionTypeCode())){
            invoice = new VendorCreditInvoiceDto();
        }               
        invoice.setInvoiceDate(big.getInvoiceDate());

        if (header.getTermsOfSale() != null) {
	        for (TermsOfSale terms : header.getTermsOfSale()) {
	        	if (terms != null && terms.getTermsNetDueDate() != null) {
	        		invoice.setDueDate(terms.getTermsNetDueDate());
	        	}
	        }
        }

        invoice.setInvoiceNumber(big.getInvoiceNumber());
        
        Collection<NoteSpecialInstructions> notes = header.getNoteSpecialInstructions();
        List<String> instructions = new ArrayList<>();
        if(notes!=null){
            for(NoteSpecialInstructions note:notes){
                instructions.add(note.getFreeFormMessage());
            }        
        }
        invoice.setSpecalInstructions(instructions);
        if(header.getReferenceNumbers()!=null){
            for(ReferenceNumber number:header.getReferenceNumbers()){
                if(BILLING_TYPE_CODE_QUALIFIER.equals(number.getReferenceIdentificationQualifier())){
                    invoice.setBillingTypeCode(number.getReferenceIdentification());
                }
            }
        }
        //Trailer
        Trailer trailer = body.getTrailer();
        TotalMonetaryValueSummary summary = trailer.getTotalMonetaryValueSummary();
        invoice.setAmount(summary.getTotalInvoiceAmount());
        
        return invoice;
    }

    private PurchaseOrderDto createCustomerOrderDto(InvoiceBody body, String partnerId) {
        PurchaseOrderDto customerOrderDto = new PurchaseOrderDto();
        final Header header = body.getHeader();
        InvoiceBeginningSegment big = header.getBeginningSegmentforInvoice();
        customerOrderDto.setOrderDate(big.getPurchaseOrderDate());
        List<PoNumberDto> poNumberDtos = new ArrayList<PoNumberDto>();
        PoNumberDto apdPo = createPoNumberDto(big.getPurchaseOrderNumber(), customerOrderDto, APD_PO_TYPE);
        customerOrderDto.setApdPoNumber(big.getPurchaseOrderNumber());
        PoNumberDto customerPo = null;
        if (big.getCustomerOrderNumber() != null) {
            customerOrderDto.setCustomerPoNumber(big.getCustomerOrderNumber());
            customerPo = createPoNumberDto(big.getCustomerOrderNumber(), customerOrderDto, CUSTOMER_PO_TYPE);
            poNumberDtos.add(customerPo);
        }
        if (PoAcknowledgementTranslator.STAPLES_PARTNER_ID.equals(partnerId)) {
            apdPo = createPoNumberDto(big.getReleaseNumber(), customerOrderDto, APD_PO_TYPE);
            customerOrderDto.setApdPoNumber(big.getReleaseNumber());
            customerOrderDto.setCustomerPoNumber(big.getPurchaseOrderNumber());
            customerPo = createPoNumberDto(big.getPurchaseOrderNumber(), customerOrderDto, CUSTOMER_PO_TYPE);
        }
        if (body.getHeader().getReferenceNumbers() != null) {
            for (ReferenceNumber referenceNumber : body.getHeader().getReferenceNumbers()) {
                switch (referenceNumber.getReferenceIdentificationQualifier()) {
                    case CUSTOMER_ORDER_QUALIFIER:
                        if (!PoAcknowledgementTranslator.STAPLES_PARTNER_ID.equals(partnerId)) {
                            apdPo.setValue(referenceNumber.getReferenceIdentification());
                        }
                        else {
                            customerPo = createPoNumberDto(referenceNumber.getReferenceIdentification(),
                                    customerOrderDto, CUSTOMER_PO_TYPE);
                            ;
                        }
                        break;
                    default:
                        LOG.error("Unrecogized Reference Identifiation Qualifier: "
                                + referenceNumber.getReferenceIdentification());
                }
            }
        }
        if (customerPo != null) {
            poNumberDtos.add(customerPo);
        }
        poNumberDtos.add(apdPo);
        customerOrderDto.setPoNumbers(poNumberDtos);
        return customerOrderDto;
    }

    private AddressDto generateAddress(InvoiceAddressGroup group) {
        AddressDto address = new AddressDto();
        if (group.getGeographicLocation() != null) {
            GeographicLocation geographicLocation = group.getGeographicLocation();
            if (geographicLocation != null) {
                address.setCountry(geographicLocation.getCountryCode());
                address.setCity(geographicLocation.getCityName());
                address.setState(geographicLocation.getStateOrProvinceCode());
            }
        }
        AddressInformation addressInformation = group.getAddressInformation();
        if (addressInformation != null) {
            address.setLine1(group.getAddressInformation().getAddressLine1());
            address.setLine2(group.getAddressInformation().getAddressLine2());
        }
        return address;
    }

    private void setSkuByQualifier(LineItemDto lineItem, String qualifier, String productServiceId) {
        //removes dashes which ussco somestimes includes on invoice skus
        productServiceId = removeDashes(productServiceId);
        switch (qualifier) {
            case BUYER_SKU_QUALIFIER:
                lineItem.setApdSku(productServiceId);
                break;
            case VENDOR_SKU_QUALIFIER:
                // Set both customer sku and vendor sku (e.g. supplier part id) here
                lineItem.setSupplierPartId(productServiceId);
                SkuDto vendorSku = new SkuDto();
                SkuTypeDto vendorType = new SkuTypeDto();
                vendorType.setName(SkuTypeBp.SKUTYPE_VENDOR);
                vendorSku.setType(vendorType);
                vendorSku.setValue(productServiceId);
                lineItem.setVendorSku(vendorSku);
                SkuDto customerSku = new SkuDto();
                SkuTypeDto customerType = new SkuTypeDto();
                customerType.setName(SkuTypeBp.SKUTYPE_CUSTOMER);
                customerSku.setType(customerType);
                customerSku.setValue(productServiceId);
                lineItem.setCustomerSku(customerSku);
                break;
        }
    }

    private VendorInvoiceDto makeCreditChanges(List<LineItemDto> dtoItems, VendorCreditInvoiceDto vendorCreditInvoiceDto) {
        vendorCreditInvoiceDto.setItems(new HashSet<LineItemXReturnDto>());
        for (LineItemDto item : dtoItems) {
            final LineItemXReturnDto lineItemXReturnDto = new LineItemXReturnDto();
            lineItemXReturnDto.setLineItemDto(item);
            lineItemXReturnDto.setQuantity(item.getQuantity().abs());
            vendorCreditInvoiceDto.getItems().add(lineItemXReturnDto);
        }
        return vendorCreditInvoiceDto;
    }

    public static String removeDashes(String inputString) {
        return inputString.replaceAll("\\-", "");
    }

}
