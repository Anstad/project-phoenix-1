package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import com.apd.phoenix.service.integration.routes.SolrUpdateRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class SolrUpdateBean {

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setup() throws Exception {

        Properties solrProperties = PropertiesLoader.getAsProperties("solr.integration");

        setupAmq(camelContext, solrProperties.getProperty("jmsBrokerUrl"), "activemq");
        setupAmq(camelContext, solrProperties.getProperty("catalogJmsBrokerUrl"), "catalog-broker");

        SolrUpdateRouteBuilder solrUpdateRouteBuilder = new SolrUpdateRouteBuilder();
        solrUpdateRouteBuilder.setUpdateSource(solrProperties.getProperty("updateSource"));
        solrUpdateRouteBuilder.setUpdateSourceAggregated(solrProperties.getProperty("updateSourceAggregated"));
        solrUpdateRouteBuilder.setAggregationSize(Integer.parseInt(solrProperties.getProperty("aggregationSize")));
        solrUpdateRouteBuilder.setAggregationTimeout(Long.parseLong(solrProperties.getProperty("aggregationTimeout")));
        solrUpdateRouteBuilder.setThrottleMessageCount(Long.parseLong(solrProperties
                .getProperty("throttleMessageCount")));
        solrUpdateRouteBuilder.setThrottleTimePeriod(Long.parseLong(solrProperties.getProperty("throttleTimePeriod")));

        camelContext.addRoutes(solrUpdateRouteBuilder);
        camelContext.start();
    }

    private void setupAmq(CamelContext context, String jmsBrokerUrl, String componentName) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent(componentName, activeMQComponent);
    }
}
