package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.util.ArrayList;
import java.util.Map;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogBp.SyncAction;
import com.apd.phoenix.service.business.CatalogBp.SyncError;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.ItemRollbackException;
import com.apd.phoenix.service.catalog.CatalogChange;
import com.apd.phoenix.service.catalog.CatalogChangeResult;
import com.apd.phoenix.service.catalog.CatalogResultAggregator;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.SyncItemResult;
import com.apd.phoenix.service.model.SyncItemResultSummary;

@Stateless
public class CatalogMessageProcessor {

    private static final int MAX_ITEM_PROCESS_ATTEMPTS = 5;

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogMessageProcessor.class);

    @Inject
    ItemBp itemBp;

    @Inject
    CatalogBp catalogBp;

    @Inject
    CatalogXItemBp catalogXItemBp;

    @Inject
    CatalogResultAggregator catalogResultAggregator;

    public void process(ArrayList<CatalogChange> catalogChanges) {
        for (CatalogChange catalogChange : catalogChanges) {
            CatalogChangeResult catalogChangeResult = null;
            //for each item, attempts to process it using syncItem
            //if an unexpected failure occurs, retries the process a number of times
            //equal to MAX_ITEM_PROCESS_ATTEMPTS
            int attempts = 0;
            boolean lastProcessSucceeded = false;
            while (attempts < MAX_ITEM_PROCESS_ATTEMPTS && !lastProcessSucceeded) {
                attempts++;
                catalogChangeResult = this.processChange(catalogChange);
                //if the item was rolled back and the changes should be persisted, 
                //the process was a failure, and is retried (up to the max)
                lastProcessSucceeded = (!catalogChangeResult.getRollbackChanges() || !catalogChangeResult
                        .getPersistChanges());
            }
            catalogResultAggregator.process(catalogChangeResult);
        }
    }

    private CatalogChangeResult processChange(CatalogChange catalogChange) {
        Map<String, String> itemData = catalogChange.getItemData();
        SyncAction action = catalogChange.getAction();
        Long catalogId = catalogChange.getCatalogId();
        Long oldCatalogId = catalogChange.getOldCatalogId();
        Boolean persistChanges = catalogChange.getPersistChanges();
        Boolean rollbackChanges = false;
        Catalog catalog = catalogBp.findById(catalogId, Catalog.class);
        SyncItemResultSummary summary = new SyncItemResultSummary();
        SyncItemResult result = null;
        String correlationId = catalogChange.getCorrelationId();
        Long totalItems = catalogChange.getTotalItems();
        boolean forCustomerCatalog = catalogChange.isForCustomerCatalog();

        try {
            if (!forCustomerCatalog) {
                result = itemBp.syncItem(itemData, catalog, action, summary, persistChanges);
            }
            else {
                Catalog oldCatalog = catalogBp.findById(oldCatalogId, Catalog.class);
                result = catalogXItemBp.syncItem(itemData, catalog, oldCatalog, action, summary, persistChanges);
            }
        }
        catch (Exception e) {
            LOGGER.warn("Exception! Turn on debug to view stack trace. " + e.toString());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Stack trace", e);
            }
            if (e instanceof EJBException && ((EJBException) e).getCause() instanceof ItemRollbackException) {
                result = ((ItemRollbackException) ((EJBException) e).getCause()).getResult();
            }
            else if (e instanceof ItemRollbackException) {
                result = ((ItemRollbackException) e).getResult();
            }
            else {
                //catches any unexpected Java error
                result = new SyncItemResult();
                if (itemData.containsKey(ItemBp.APD_SKU_HEADER)) {
                    result.setResultApdSku(itemData.get(ItemBp.APD_SKU_HEADER));
                }
                else {
                    result.setResultApdSku("ERROR");
                }
                if (itemData.containsKey(ItemBp.VENDOR_HEADER)) {
                    result.setResultVendor(itemData.get(ItemBp.VENDOR_HEADER));
                }
                else {
                    result.setResultVendor("ERROR");
                }

                if (e instanceof com.apd.phoenix.service.business.ItemBp.NoVendorCatalogSpecifiedException) {
                    result.getErrors().add(SyncError.NO_CATALOG_FOUND.getLabel());
                }
                else {
                    result.getErrors().add(SyncError.UNEXPECTED_ERROR.getLabel() + e.getMessage());
                }
                summary.incrementErrors();
                summary.incrementFailures();
            }
            rollbackChanges = true;
        }
        return new CatalogChangeResult(summary, result, correlationId, catalogId, oldCatalogId, totalItems, action,
                persistChanges, rollbackChanges);
    }
}
