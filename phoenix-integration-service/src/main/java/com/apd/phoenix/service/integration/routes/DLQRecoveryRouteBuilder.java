package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DLQRecoveryRouteBuilder extends RouteBuilder {

    private String input;

    private String output;

    private static final Logger LOG = LoggerFactory.getLogger(DLQRecoveryRouteBuilder.class);

    @Override
    public void configure() throws Exception {
        from(input).process(new Processor() {

            @Override
            public void process(Exchange exchange) throws Exception {
                String rawMessage = (String) exchange.getIn().getHeader("CamelFileNameOnly");
                String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
                String groupId = (String) exchange.getIn().getHeader("groupId");
                String transactionId = (String) exchange.getIn().getHeader("transactionId");
                LOG.info("CamelFileNameOnly: {}", rawMessage);
                LOG.info("interchangeId: {}", interchangeId);
                LOG.info("groupId: {}", groupId);
                LOG.info("transactionId: {}", transactionId);
                exchange.getIn().setBody(rawMessage);
                Thread.sleep(5000);
            }
        }).to(output);

    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

}
