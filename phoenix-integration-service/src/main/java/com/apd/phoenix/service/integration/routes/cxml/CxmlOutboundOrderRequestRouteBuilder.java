package com.apd.phoenix.service.integration.routes.cxml;

import javax.xml.bind.JAXBContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlCxmlRequestTranslator;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlTransactionLogger;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class CxmlOutboundOrderRequestRouteBuilder extends RouteBuilder {

    private String source;

    private String output = "http://component";

    private String outputDLQ;

    private static final String logSource = "direct:cxmlOutORLogSource";

    private static final String logResponseSource = "direct:cxmlOutORLogResponseSource";

    private String deploymentMode = "test";
    private String cxmlOrderRequestOutboundDLQ;

    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxbDataFormat = new JaxbDataFormat();
        JAXBContext jaxbContext = JAXBContext
                .newInstance(com.apd.phoenix.service.integration.cxml.model.cxml.CXML.class);
        jaxbDataFormat.setContext(jaxbContext);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in cxml processing: ${exception.stacktrace}");

        //Receive DTO
        from(source).errorHandler(
                deadLetterChannel(getCxmlOrderRequestOutboundDLQ()).maximumRedeliveries(2).redeliveryDelay(2000)
                        .useExponentialBackOff().backOffMultiplier(2))
        //Prepare for endpoint
                .setHeader(Exchange.HTTP_URI, simple("${header.destination}")).setHeader("apdPo").jxpath(
                        "/in/body/apdPoNumber").setHeader("deploymentMode", constant(deploymentMode))
                //Convert to cxml pojo
                .bean(CxmlCxmlRequestTranslator.class, "fromPurchaseOrderToOrderRequest")
                //Marshal to cxml
                .marshal(jaxbDataFormat)
                //Log
                .to(logSource)
                //Send to external endpoint
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                //Set content type
                .setHeader(Exchange.CONTENT_TYPE, constant("text/xml"))
                //Post to partner
                .log(LoggingLevel.DEBUG, "post contents: ${body}")
                //Send to http endpoint
                .to(output)
                //convert from stream to string
                .convertBodyTo(String.class)
                //Log
                .log(LoggingLevel.DEBUG, "post response: ${body}")
                //Response handler
                .wireTap(logResponseSource);
        //TODO:Handle bad response
        //.to(responseLog);

        from(logSource).setHeader("messageEventType", constant(EventTypeDto.PURCHASE_ORDER_SENT)).setHeader(
                "communicationType", constant(CommunicationType.OUTBOUND)).bean(CxmlTransactionLogger.class,
                "logCxmlTransaction");

        from(logResponseSource).setHeader("messageEventType", constant(EventTypeDto.PURCHASE_ORDER_PARTNER_RESPONSE))
                .setHeader("communicationType", constant(CommunicationType.INBOUND)).bean(CxmlTransactionLogger.class,
                        "logCxmlTransaction");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getDeploymentMode() {
        return deploymentMode;
    }

    public void setDeploymentMode(String deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

    /**
     * @return the cxmlOrderRequestOutboundDLQ
     */
    public String getCxmlOrderRequestOutboundDLQ() {
        return cxmlOrderRequestOutboundDLQ;
    }

    /**
     * @param cxmlOrderRequestOutboundDLQ the cxmlOrderRequestOutboundDLQ to set
     */
    public void setCxmlOrderRequestOutboundDLQ(String cxmlOrderRequestOutboundDLQ) {
        this.cxmlOrderRequestOutboundDLQ = cxmlOrderRequestOutboundDLQ;
    }

}
