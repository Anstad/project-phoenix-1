/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apd.phoenix.service.integration.solr;

/**
 *
 * @author dnorris
 */
public class SolrAttributeConstants {

    private SolrAttributeConstants() {
    }

    public static final String ITEM_ID = "ITEM_ID";
    public static final String NAME = "NAME";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String CATALOG_ID = "CATALOG_ID";
    public static final String CATALOGXITEM_ID = "CATALOGXITEM_ID";
    public static final String PRICE = "PRICE";
    public static final String MNAME = "MNAME";
    public static final String CATEGORY = "HIERARCHY_DESCRIPTION";
    public static final String HIERARCHY_PATH = "INDEXEDPATHS";
    public static final String SEARCHTERMS = "SEARCHTERMS";
    public static final String COREEXPIREDATE = "COREEXPIREDATE";
    public static final String CORESTARTDATE = "CORESTARTDATE";
    public static final String UOM_NAME = "UOM_NAME";
    public static final String ID_FIELD = "CATALOGXITEMID";
    public static final String VENDOR_NAME = "VENDORNAME";
    public static final String STATUS = "ITEM_STATUS";
    public static final String HIERARCHY_PARENT_ID = "HIERARCHY_PARENT_ID";
    public static final String MINIMUM = "MINIMUM";
    public static final String MULTIPLE = "MULTIPLE";
    public static final String REP_SKU = "REP_SKU";
    public static final String REP_VEND_NAME = "REP_VEND_NAME";
    public static final String CUSTOM_ORDER = "CUSTOM_ORDER";
    public static final String SPECIAL_ORDER = "SPECIAL_ORDER";
    public static final Object CUSTOMER_SKU = "CUSTOMER_SKU";
    public static final String OVERRIDE_CATALOGS = "OVERRIDE_CATALOGS";

}
