package com.apd.phoenix.service.integration.routes.cxml;

import javax.xml.bind.JAXBContext;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlFulfillRequestTranslator;
import com.apd.phoenix.service.integration.camel.processor.cxml.CxmlTransactionLogger;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import org.apache.camel.LoggingLevel;

public class CxmlOutboundConfirmationRequestRouteBuilder extends RouteBuilder {

    /**
     * @return the logSource
     */
    public static String getLogSource() {
        return logSource;
    }

    /**
     * @param aLogSource the logSource to set
     */
    public static void setLogSource(String aLogSource) {
        logSource = aLogSource;
    }

    private String source;

    private String output;

    private String outputDLQ;

    private String deploymentMode;

    private static String logSource = "direct:cxmlOutCRLogSource";

    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxbDataFormat = new JaxbDataFormat();
        JAXBContext jaxbContext = JAXBContext
                .newInstance(com.apd.phoenix.service.integration.cxml.model.fulfill.CXML.class);
        jaxbDataFormat.setContext(jaxbContext);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in cxml processing: ${exception.stacktrace}");

        //Receive DTO
        from(source)
        //Setup DLQ
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Prepare for endpoint
                .setHeader(Exchange.HTTP_URI, simple("${header.destination}"))
                //Set headers
                .setHeader("apdPo").jxpath("/in/body/apdPo").setHeader("deploymentMode", simple(deploymentMode))
                //Convert to cXML pojo
                .bean(CxmlFulfillRequestTranslator.class, "fromPoAckDtoToOrderConfirmation")
                //Marshal to cXML
                .marshal(jaxbDataFormat)
                //Prepare for output
                .bean(CxmlFulfillRequestTranslator.class, "prepareXmlHeaders")
                //Log
                .wireTap(logSource)
                //Send to external endpoint
                .log(LoggingLevel.INFO, "Sending to http endpoint: ${header.CamelHttpUri}")
                //Send to endpoint
                .to(output)
                //Log response
                .log(LoggingLevel.INFO, "response: ${body}");

        from(logSource).setHeader("messageEventType", constant(EventTypeDto.ORDER_ACKNOWLEDGEMENT)).setHeader(
                "communicationType", constant(CommunicationType.OUTBOUND)).bean(CxmlTransactionLogger.class,
                "logCxmlTransaction");

    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @param output the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * @return the outputDLQ
     */
    public String getOutputDLQ() {
        return outputDLQ;
    }

    /**
     * @param outputDLQ the outputDLQ to set
     */
    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    /**
     * @return the deploymentMode
     */
    public String getDeploymentMode() {
        return deploymentMode;
    }

    /**
     * @param deploymentMode the deploymentMode to set
     */
    public void setDeploymentMode(String deploymentMode) {
        this.deploymentMode = deploymentMode;
    }

}
