/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import java.util.List;
import javax.edi.model.x12.edi824.segment.ApplicationAdviceGroupChild;
import javax.edi.model.x12.edi824.segment.ApplicationAdviceGroupParent;
import javax.edi.model.x12.edi824.segment.Header;
import javax.edi.model.x12.edi824.segment.ApplicationAdviceBody;
import javax.edi.model.x12.segment.NoteSpecialInstructions;
import javax.edi.model.x12.segment.ReferenceNumber;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.model.dto.error.EdiErrorDto;
import com.apd.phoenix.service.model.dto.ReferenceNumberDto;
import com.apd.phoenix.service.model.dto.TechErrorsDto;
import com.apd.phoenix.service.model.dto.error.ErrorDataDto;
import com.apd.phoenix.service.model.dto.error.RelevantNumberDto;

/**
 * 
 * @author
 */
public class Edi824Translator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(Edi824Translator.class);
    public static final String APD_PO_TYPE = "APD";
    public static final String SKU_QUALIFIER = "IX";
    public static final String INVOICE_NUMBER_QUALIFIER = "IK";
    public static final String PURCHASE_ORDER_LINE_QUALIFIER = "BV";
    public static final String PURCHASE_ORDER_NUMBER_QUALIFIER = "PO";
    public static final String RECCOMENDED_ACTION_CODE = "ACT";
    public static final String ADDITIONAL_ERROR_INFORMATION_CODE = "NCD";
    public static final String SHIPMENT_ID_QUALIFIER = "SI";
    public static final String SKU_TYPE = "Sku";
    public static final String SHIPMENT_ID_TYPE = "Shipment Id";
    public static final String PURCHASE_ORDER_NUMBER_TYPE = "Purchase Order Number";
    public static final String PURCHASE_ORDER_LINE_TYPE = "Purchase Order Line";
    public static final String INVOICE__NUMBER_TYPE = "Invoice Number";

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        ApplicationAdviceBody body = (ApplicationAdviceBody) exchange.getIn().getBody();
        String rawEdi = exchange.getIn().getHeader("rawEdi", String.class);
        EdiDocumentErrorsDto ediErrorDto = createEdiDocumentErrorsDto(body);
        ediErrorDto.setRawEdi(rawEdi);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(ediErrorDto, EdiDocumentErrorsDto.class);
    }

    private EdiDocumentErrorsDto createEdiDocumentErrorsDto(ApplicationAdviceBody body) {

        EdiDocumentErrorsDto ediDocumentErrorsDto = new EdiDocumentErrorsDto();
        List<RelevantNumberDto> refNumberDtos = createRelevantNumberDto(body);
        // Ref. Identification in header.
        ediDocumentErrorsDto.setRelevantNumberDto(refNumberDtos);
        // Get OTI loop
        List<EdiErrorDto> otiList = createErrorList(body);
        ediDocumentErrorsDto.setEdiErrorDtos(otiList);
        ediDocumentErrorsDto.setTimestamp(body.getHeader().getAppAdviceBeginSegment().getDate());

        return ediDocumentErrorsDto;
    }

    private List<EdiErrorDto> createErrorList(ApplicationAdviceBody body) {

		List<EdiErrorDto> otiList = new ArrayList<EdiErrorDto>();
		List<RelevantNumberDto> refDtoList = new ArrayList<>();
		Iterable<ApplicationAdviceGroupParent> parents = body.getDetail()
					.getAppAdvice();
                for (ApplicationAdviceGroupParent parent : parents) {
                        EdiErrorDto ediErrorDto = new EdiErrorDto();
                        ediErrorDto.setRelevantNumbers(new ArrayList<RelevantNumberDto>());
                   RelevantNumberDto errorReferenceNumber = new RelevantNumberDto();
                   switch(parent.getOrigTxnIdentification().getRefIdentificationQualifier()){
                       case SKU_QUALIFIER:
                           errorReferenceNumber.setType(SKU_TYPE);
                           break;
                       case INVOICE_NUMBER_QUALIFIER:
                           errorReferenceNumber.setType(INVOICE__NUMBER_TYPE);
                           break;
                       default:
                           errorReferenceNumber.setType("");
                   }
                   errorReferenceNumber.setValue(parent
                                        .getOrigTxnIdentification().getRefIdentification());
                   ediErrorDto.getRelevantNumbers().add(errorReferenceNumber);
                        
                   ediErrorDto.setEdiType(parent
                                        .getOrigTxnIdentification().getTxnSetIdentifierCode());
                   ediErrorDto.setInvoiceLine(parent.getOrigTxnIdentification().getGroupControlNumber());

                   Iterable<ReferenceNumber> refNums = parent
                                    .getRefIdentInvalidTxn();
                   for (ReferenceNumber ref : refNums) {
                            RelevantNumberDto refDto = new RelevantNumberDto();
                            switch(ref.getReferenceIdentificationQualifier()){
                                case PURCHASE_ORDER_LINE_QUALIFIER:
                                    refDto.setType(PURCHASE_ORDER_LINE_TYPE);
                                    break;
                                case PURCHASE_ORDER_NUMBER_QUALIFIER:
                                    refDto.setType(PURCHASE_ORDER_NUMBER_TYPE);
                                    break;
                                default:
                                    refDto.setType("");
                            }

                            refDto.setValue(ref.getReferenceIdentification());
                            refDto.setDocType(ref.getReferenceDescription());
                            refDtoList.add(refDto);
                    }
                    ediErrorDto.setRelevantNumbers(refDtoList);

                    // get children
                    Iterable<ApplicationAdviceGroupChild> kids = parent
                                    .getAppAdviceChild();
                    List<ErrorDataDto> tedList = null;
                    ErrorDataDto errorDataDto = null;
                    for (ApplicationAdviceGroupChild child : kids) {
                            tedList = new ArrayList();
                            errorDataDto = new ErrorDataDto();
                            TechErrorsDto errorsDto = new TechErrorsDto();
                            errorsDto.setTechErrorDescription(child
                                            .getTechErrorDescription().getFreeFormMessage());
                             final ArrayList errorSpecs = new ArrayList();
                             final ArrayList reccomendations = new ArrayList();
                             Iterable<NoteSpecialInstructions> notes = child
                                            .getNoteSpecialInstructions();
                            for (NoteSpecialInstructions note : notes) {
                                    switch(note.getNoteReferenceCode()){
                                        case RECCOMENDED_ACTION_CODE:
                                            reccomendations.add(note.getFreeFormMessage());
                                            break;
                                        case ADDITIONAL_ERROR_INFORMATION_CODE:
                                           errorSpecs.add(note.getFreeFormMessage());
                                            break;
                                    }
                            }
                            errorDataDto.setTechErrorDescription(child.getTechErrorDescription().getFreeFormMessage());

                            errorDataDto.setErrorSpecifications(errorSpecs);
                            errorDataDto.setRecommendedActions(reccomendations);
                            tedList.add(errorDataDto); // add to DTO list

                    }
                    ediErrorDto.setErrorDataDtos(tedList); // Set parent with list of NTE, TED
                                                                            // segments.
                    otiList.add(ediErrorDto);

            }
            return otiList;
	}

    private List<RelevantNumberDto> createRelevantNumberDto(ApplicationAdviceBody body) {
		List<RelevantNumberDto> listRefDtos = null;
                RelevantNumberDto refDto = new RelevantNumberDto();
                final Header head = body.getHeader();
                listRefDtos = new ArrayList<>();
                Iterable<ReferenceNumber> refNums = head
                                .getRefIdentificationInvalidTxn();
                for (ReferenceNumber number : refNums) {
                    switch(number.getReferenceIdentificationQualifier()){
                        case PURCHASE_ORDER_NUMBER_QUALIFIER:
                            refDto.setType(PURCHASE_ORDER_NUMBER_TYPE);
                            break;
                        case SHIPMENT_ID_QUALIFIER:
                            refDto.setType(SHIPMENT_ID_TYPE);
                            break;
                        case SKU_QUALIFIER:
                            refDto.setType(SKU_TYPE);
                            break;
                    }
                        refDto.setValue(number.getReferenceIdentification());
                        refDto.setDocType(number.getReferenceDescription());
                        listRefDtos.add(refDto);
                }
		return listRefDtos;
	}
}