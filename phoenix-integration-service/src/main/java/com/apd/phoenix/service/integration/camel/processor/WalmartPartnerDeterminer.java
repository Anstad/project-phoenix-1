/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import org.apache.camel.Exchange;
import javax.edi.model.x12.v5010.edi850.PurchaseOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class WalmartPartnerDeterminer {

    public static final String PARTNER_ID = "partnerId";
    private static final Logger LOG = LoggerFactory.getLogger(WalmartPartnerDeterminer.class);
    public static final String KITS_SENDER_ID = "925485USNR";
    public static final String DEPT99_SENDER_ID = "925485US00";
    public static final String KITS_PARTNER_ID = "walmart-kits";
    public static final String DEPT99_PARTNER_ID = "walmart-dept99";

    public void resetPartnerId(Exchange exchange) {
        PurchaseOrder body = (PurchaseOrder) exchange.getIn().getBody();
        if (body.getInterchangeEnvelopeHeader().getInterchangeSenderID() != null) {
            if (KITS_SENDER_ID.equals(body.getInterchangeEnvelopeHeader().getInterchangeSenderID().trim())) {
                exchange.getIn().getHeaders().put(PARTNER_ID, KITS_PARTNER_ID);
                return;
            }
            if (DEPT99_SENDER_ID.equals(body.getInterchangeEnvelopeHeader().getInterchangeSenderID().trim())) {
                exchange.getIn().getHeaders().put(PARTNER_ID, DEPT99_PARTNER_ID);
                return;
            }
        }
        LOG.error("Unexpected interchange sener Id for Walmart order: "
                + body.getInterchangeEnvelopeHeader().getInterchangeSenderID() + " defaulting to partnerId "
                + exchange.getIn().getHeader(PARTNER_ID));
    }

}
