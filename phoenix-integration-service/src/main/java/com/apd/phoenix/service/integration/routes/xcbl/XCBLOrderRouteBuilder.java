package com.apd.phoenix.service.integration.routes.xcbl;

import javax.xml.bind.JAXBContext;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.camel.processor.OrderRequestProcessor;
import com.apd.phoenix.service.integration.camel.processor.SequenceProcessor;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLTransactionLogger;
import com.apd.phoenix.service.integration.exception.XCBLValidationException;
import com.apd.phoenix.service.model.dto.OrderLogDto;

/**
 * Note:
 * - The class resolved from inboundOrderTranslatorClass MUST implement com.apd.phoenix.service.integration.camel.processor.MessageTranslator
 * - The class resolved from inboundOrderDataFormatClass must yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * - The class resolved from outboundMessageAckTranslatorClass MUST implement org.apache.camel.Processor
 * - The class resolved from outboundMessageAckDataFormatClass must yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance 
 * @author RH
 *
 */
public class XCBLOrderRouteBuilder extends RouteBuilder {

    private String inboundOrderSource;
    private String inboundOrderDataFormatClass;
    private String inboundFormatSource;
    private String inboundOrderTranslatorClass;
    private String inboundLogSource;
    private String inboundOutput;

    private String senderId;

    private String outboundMessageAckSource;
    private String outboundMessageAckTranslatorClass;
    private String outboundMessageAckPrepare;
    private String outboundMessageAckDataFormatClass;
    private String outboundMessageAckOutput;

    private String sellerMPID;

    private String buyerMPID;

    private String testIndicator;

    @Override
    public void configure() throws Exception {

        JaxbDataFormat orderDataFormat = new JaxbDataFormat();
        JAXBContext orderJaxbContext = JAXBContext.newInstance(Class.forName(inboundOrderDataFormatClass));
        orderDataFormat.setContext(orderJaxbContext);

        JaxbDataFormat messageAckDataFormat = new JaxbDataFormat();
        JAXBContext messageAckJaxbContext = JAXBContext.newInstance(Class.forName(outboundMessageAckDataFormatClass));
        messageAckDataFormat.setContext(messageAckJaxbContext);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in xcbl processing: ${exception.stacktrace}");

        onException(XCBLValidationException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "XCBL validation exception");

        from(inboundOrderSource)
        //sets the partnerId so that it can be set on the order
                .setHeader("partnerId", constant(senderId)).setHeader("originalMessage").jxpath("in/body")
                //unmarshal
                .unmarshal(orderDataFormat)
                //Is Message Ack requested
                .choice()
                //Partner Message Ack request
                .when(simple("${header.messageAckRequested}")).to(outboundMessageAckSource)
                //Partner Message Ack not requested
                .otherwise().log(LoggingLevel.INFO, XCBLOrderRouteBuilder.class.getName(),
                        "Partner did not request Message Ack")
                //Send to processing route
                .to(inboundFormatSource);

        from(inboundFormatSource)
        //Translate incoming jaxb pojo to dto
                .bean(Class.forName(inboundOrderTranslatorClass), "toCanonical")
                //Setup order request
                .bean(OrderRequestProcessor.class, "setupOrderRequestForAccount")
                //Log incoming request
                .wireTap(inboundLogSource).split().jxpath("/in/body").bean(OrderRequestProcessor.class, "processOrder");

        from(outboundMessageAckSource)
        //Setup transaction ids
                .bean(SequenceProcessor.class, "generateSequences")
                //
                .setHeader("sellerMPID", constant(sellerMPID)).setHeader("buyerMPID", constant(buyerMPID)).setHeader(
                        "testIndicator", constant(testIndicator)).bean(
                        Class.forName(outboundMessageAckTranslatorClass), "fromCanonical").wireTap(
                        outboundMessageAckPrepare).choice().when(simple("${header.isValid} != true")).throwException(
                        new XCBLValidationException());

        from(outboundMessageAckPrepare).setBody(simple("${header.messageAck}")).to(outboundMessageAckOutput);

        from(inboundLogSource).split().jxpath("/in/body").setHeader("apdPo").jxpath("/in/body/apdPoNumber").setHeader(
                "communicationType", simple("INBOUND")).setHeader("messageEventType",
                simple(OrderLogDto.EventTypeDto.PURCHASE_ORDER_SENT.toString())).bean(XCBLTransactionLogger.class,
                "logNewPoTransaction");
    }

    /**
     * @return the inboundOrderSource
     */
    public String getInboundOrderSource() {
        return inboundOrderSource;
    }

    /**
     * @param inboundOrderSource the inboundOrderSource to set
     */
    public void setInboundOrderSource(String inboundOrderSource) {
        this.inboundOrderSource = inboundOrderSource;
    }

    public String getInboundOrderDataFormatClass() {
        return inboundOrderDataFormatClass;
    }

    public void setInboundOrderDataFormatClass(String inboundOrderDataFormatClass) {
        this.inboundOrderDataFormatClass = inboundOrderDataFormatClass;
    }

    public String getInboundOrderTranslatorClass() {
        return inboundOrderTranslatorClass;
    }

    public void setInboundOrderTranslatorClass(String inboundOrderTranslatorClass) {
        this.inboundOrderTranslatorClass = inboundOrderTranslatorClass;
    }

    public String getOutboundMessageAckDataFormatClass() {
        return outboundMessageAckDataFormatClass;
    }

    public void setOutboundMessageAckDataFormatClass(String outboundMessageAckDataFormatClass) {
        this.outboundMessageAckDataFormatClass = outboundMessageAckDataFormatClass;
    }

    /**
     * @return the inboundFormatSource
     */
    public String getInboundFormatSource() {
        return inboundFormatSource;
    }

    /**
     * @param inboundFormatSource the inboundFormatSource to set
     */
    public void setInboundFormatSource(String inboundFormatSource) {
        this.inboundFormatSource = inboundFormatSource;
    }

    /**
     * @return the inboundLogSource
     */
    public String getInboundLogSource() {
        return inboundLogSource;
    }

    /**
     * @param inboundLogSource the inboundLogSource to set
     */
    public void setInboundLogSource(String inboundLogSource) {
        this.inboundLogSource = inboundLogSource;
    }

    /**
     * @return the inboundOutput
     */
    public String getInboundOutput() {
        return inboundOutput;
    }

    /**
     * @param inboundOutput the inboundOutput to set
     */
    public void setInboundOutput(String inboundOutput) {
        this.inboundOutput = inboundOutput;
    }

    public String getOutboundMessageAckSource() {
        return outboundMessageAckSource;
    }

    public void setOutboundMessageAckSource(String outboundMessageAckSource) {
        this.outboundMessageAckSource = outboundMessageAckSource;
    }

    public String getOutboundMessageAckPrepare() {
        return outboundMessageAckPrepare;
    }

    public void setOutboundMessageAckPrepare(String outboundMessageAckPrepare) {
        this.outboundMessageAckPrepare = outboundMessageAckPrepare;
    }

    public String getOutboundMessageAckOutput() {
        return outboundMessageAckOutput;
    }

    public void setOutboundMessageAckOutput(String outboundMessageAckOutput) {
        this.outboundMessageAckOutput = outboundMessageAckOutput;
    }

    public String getOutboundMessageAckTranslatorClass() {
        return outboundMessageAckTranslatorClass;
    }

    public void setOutboundMessageAckTranslatorClass(String outboundMessageAckTranslatorClass) {
        this.outboundMessageAckTranslatorClass = outboundMessageAckTranslatorClass;
    }

    public String getSellerMPID() {
        return sellerMPID;
    }

    public void setSellerMPID(String sellerMPID) {
        this.sellerMPID = sellerMPID;
    }

    public String getBuyerMPID() {
        return buyerMPID;
    }

    public void setBuyerMPID(String buyerMPID) {
        this.buyerMPID = buyerMPID;
    }

    public String getTestIndicator() {
        return testIndicator;
    }

    public void setTestIndicator(String testIndicator) {
        this.testIndicator = testIndicator;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

}
