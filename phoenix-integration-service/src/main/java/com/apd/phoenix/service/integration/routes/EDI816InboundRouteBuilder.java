/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.Edi816Translator;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import javax.edi.model.x12.edi816.OrgRelationships;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;

public class EDI816InboundRouteBuilder extends EDIRouteBuilder {

    private String source;

    private String output;

    private String orgFormattedSource = "direct:orgFormattedSource";

    private String orgLogSource = "direct:orgLogSource";

    private String outputDLQ;

    private String logOutputDLQ;

    private String logOutput = "http://placeholder";

    private String logOutputURI;

    private String senderId;

    @Override
    public void configure() throws Exception {
        DataFormat format = new EDIDataFormat(OrgRelationships.class);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        from(source).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI816.INBOUND_816)).unmarshal(
                format)
                // Parse transaction information
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber")
                // Iterate 
                .split().jxpath("/in/body/body").setHeader("transactionId").jxpath(
                        "/in/body/header/transactionSetHeader/transactionSetControlNumber").multicast()
                .parallelProcessing().to(orgFormattedSource);

        from(orgFormattedSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI816.INBOUND_816_FORMATTED))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).process(new Edi816Translator())
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json")).marshal().json(JsonLibrary.Jackson).to(
                        output);
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source
     *            the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * @return the output
     */
    public String getOutput() {
        return output;
    }

    /**
     * @param output
     *            the output to set
     */
    public void setOutput(String output) {
        this.output = output;
    }

    /**
     * @return the orgFormattedSource
     */
    public String getUsscoorgEDISource() {
        return orgFormattedSource;
    }

    /**
     * @param orgFormattedSource
     *            the orgFormattedSource to set
     */
    public void setUsscoorgEDISource(String usscoorgEDISource) {
        this.orgFormattedSource = usscoorgEDISource;
    }

    /**
     * @return the logOutputDLQ
     */
    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    /**
     * @param logOutputDLQ
     *            the logOutputDLQ to set
     */
    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    /**
     * @return the orgLogSource
     */
    public String getOrgLogSource() {
        return orgLogSource;
    }

    /**
     * @param orgLogSource
     *            the orgLogSource to set
     */
    public void setOrgLogSource(String orgLogSource) {
        this.orgLogSource = orgLogSource;
    }

    /**
     * @return the logOutput
     */
    public String getOrgLogOutput() {
        return logOutput;
    }

    /**
     * @param logOutput
     *            the logOutput to set
     */
    public void setOrgLogOutput(String orgLogOutput) {
        this.logOutput = orgLogOutput;
    }

    public String getLogOutputURI() {
        return logOutputURI;
    }

    public void setLogOutputURI(String logOutputURI) {
        this.logOutputURI = logOutputURI;
    }

    /**
     * @return the outputDLQ
     */
    public String getOutputDLQ() {
        return outputDLQ;
    }

    /**
     * @param outputDLQ
     *            the outputDLQ to set
     */
    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

}
