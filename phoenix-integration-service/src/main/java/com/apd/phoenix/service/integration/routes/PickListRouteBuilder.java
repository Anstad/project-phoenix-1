package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.picklist.model.PickCsv;
import com.apd.phoenix.service.integration.picklist.processor.PickListProcessor;

public class PickListRouteBuilder extends RouteBuilder {

    private String source;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");
        //csv from solomon
        from(source)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
        //convert to string
                .convertBodyTo(String.class)
                //set raw message header
                .setHeader("rawMessage", simple("${body}"))
                //bindy unmarshal to pojo
                .unmarshal().bindy(BindyType.Csv, PickCsv.class)
                //process picklist
                .bean(PickListProcessor.class, "process");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}
