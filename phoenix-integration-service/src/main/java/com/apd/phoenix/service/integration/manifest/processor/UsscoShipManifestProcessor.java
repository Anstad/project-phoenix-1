package com.apd.phoenix.service.integration.manifest.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import javax.persistence.PersistenceException;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.TransactionManager;

import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.manifest.service.api.ShipManifestService;
import com.apd.phoenix.service.integration.manifest.ussco.model.UsscoManifestCSVDto;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ShipManifest;
import com.apd.phoenix.service.model.ShipManifest.ManifestType;
import com.apd.phoenix.service.model.ShipManifestLog;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import com.apd.phoenix.service.persistence.jpa.ShipManifestDao;
import com.apd.phoenix.service.persistence.jpa.ShipManifestLogDao;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class UsscoShipManifestProcessor {
	
	private static final Logger LOG = LoggerFactory.getLogger(UsscoShipManifestProcessor.class);

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    @Inject
    ShipManifestDao shipManifestDao;
    
    @Inject
    EmailService emailService;
    
    @Inject
    PoNumberDao poNumberDao;
    
    @Inject
    ShipManifestLogDao shipManifestLogDao;
    
    @Inject
    ShipManifestService shipManifestService;
    
    @Inject
    private CustomerOrderBp customerOrderBp;
    
    public void processUsscoManifest(@Header("CamelFileNameOnly") String fileName, @Header("rawMessage") String rawMessage, UsscoManifestCSVDto manifestDto) {
    	List<Response> success = new ArrayList<>();
    	List<Response> failure = new ArrayList<>();
    	Response response;
    	if(shipManifestService.isUsscoFileNameAllowed(fileName)) {
    		 response = process(manifestDto);
    	} else {
    		response = new Response();
    		response.setValid(false);
    		response.getMessages().add("Invalid filename.");
    	}
    	if(response.isValid()) {
			success.add(response);
		}
		else {
			failure.add(response);
		}
    	
    	sendResultMessage(success, failure, rawMessage, fileName);
    	logProcessedManifest(fileName);
    }

    public void processUsscoManifest(@Header("CamelFileNameOnly") String fileName, @Header("rawMessage") String rawMessage, List<UsscoManifestCSVDto> manifestDtos) {
    	List<Response> success = new ArrayList<>();
    	List<Response> failure = new ArrayList<>();
    	Response response;
    	if(shipManifestService.isUsscoFileNameAllowed(fileName)) {
			for(UsscoManifestCSVDto manifestDto : manifestDtos) {
				response = process(manifestDto);
				if(response.isValid()) {
					success.add(response);
				}
				else {
					failure.add(response);
				}
			}
    	} else {
    		response = new Response();
    		response.setValid(false);
    		response.getMessages().add("Invalid filename.");
    		failure.add(response);
    	}
		sendResultMessage(success, failure, rawMessage, fileName);
		logProcessedManifest(fileName);
	}
    
    private void logProcessedManifest(String fileName) {
    	try {
			tm.begin();
		} catch (NotSupportedException | SystemException e1) {
			LOG.error("Could not start transaction");
		}
    	ShipManifestLog shipManifestLog = new ShipManifestLog();
    	shipManifestLog.setDateProcessed(new Date());
    	shipManifestLog.setFilename(fileName);
    	shipManifestLogDao.create(shipManifestLog);
    	
    	try {
			tm.commit();
		} catch (SecurityException | IllegalStateException | RollbackException
				| HeuristicMixedException | HeuristicRollbackException
				| SystemException e) {
			LOG.error("Could not commit transaction");
		}
    }
    
    private Response process(UsscoManifestCSVDto manifestDto) {
    	Response response = new Response();
		response.setManifestDto(manifestDto);
		response.setValid(true);
		try {
			tm.begin();
		} catch (NotSupportedException | SystemException e1) {
			response.getMessages().add("Unable to start transaction for order.");
			response.setValid(false);
		}
		LOG.info("Processing Ussco manifest for order: {}", manifestDto.getCustomerPurchaseOrderNumber());
		
		ShipManifest shipManifest = new ShipManifest();
		shipManifest.setManifestDate(new Date());
		shipManifest.setManifested(false);
		shipManifest.setType(ManifestType.USSCO);
		shipManifest.setNumBoxes(BigInteger.valueOf(1));
		
		CustomerOrder customerOrder = customerOrderBp.searchByApdPo(manifestDto.getCustomerPurchaseOrderNumber());
		if(customerOrder == null) {
			response.getMessages().add("Unable to find lineItem for Ussco manifest entry.");
			response.setValid(false);
		}
		shipManifest.setCustomerOrder(customerOrder);
		
		shipManifest.setTradingPartnerID(manifestDto.getTradingPartnerID());
		shipManifest.setDocumentID(manifestDto.getDocumentID());
		shipManifest.setSelectionTypeCode(manifestDto.getSelectionTypeCode());
		if(StringUtils.isNotEmpty(manifestDto.getDateTimeSent())) {
			Date dateTimeSent;
			try {
				dateTimeSent = generateDate(manifestDto.getDateTimeSent(), "MM/dd/yyyy hh:mm:ss a");
				shipManifest.setDateTimeSent(dateTimeSent);
			} catch (ParseException e) {
				response.getMessages().add("Unable to parse date time sent.");
				response.setValid(false);
			}
			
		}
		shipManifest.setFacilityNumber(manifestDto.getFacilityNumber());			
		shipManifest.setFacilityAbbreviation(manifestDto.getFacilityAbbreviation());			
		shipManifest.setFacilityName(manifestDto.getFacilityName());			
		shipManifest.setFacilityAddress1(manifestDto.getFacilityAddress1());			
		shipManifest.setFacilityAddress2(manifestDto.getFacilityAddress2());			
		shipManifest.setFacilityCity(manifestDto.getFacilityCity());			
		shipManifest.setFacilityState(manifestDto.getFacilityState());			
		shipManifest.setFacilityPostalCode(manifestDto.getFacilityPostalCode());			
		shipManifest.setOrderNumber(manifestDto.getOrderNumber());			
		shipManifest.setSubOrderNumber(manifestDto.getSubOrderNumber());			
		shipManifest.setCarrierCode(manifestDto.getCarrierCode());			
		shipManifest.setShipTruckCode(manifestDto.getShipTruckCode());		
		shipManifest.setShipTruckDockId(manifestDto.getShipTruckDockId());		
		shipManifest.setTruckCodeDescription(manifestDto.getTruckCodeDescription());			
		shipManifest.setCustomerNumber(manifestDto.getCustomerNumber());			
		shipManifest.setCustomerName(manifestDto.getCustomerName());		
		shipManifest.setCustomerBarCode(manifestDto.getCustomerBarCode());			
		shipManifest.setCustomerReferenceData(manifestDto.getCustomerReferenceData());			
		shipManifest.setCustomerRouteDescription(manifestDto.getCustomerRouteDescription());		
		shipManifest.setCustomerRouteData(manifestDto.getCustomerRouteData());			
		shipManifest.setCustomerPurchaseOrderNumber(manifestDto.getCustomerPurchaseOrderNumber());			
		shipManifest.setDeliveryTypeCode(manifestDto.getDeliveryTypeCode());			
		shipManifest.setDeliveryMethodCode(manifestDto.getDeliveryMethodCode());			
		shipManifest.setCustomerAddress1(manifestDto.getCustomerAddress1());		
		shipManifest.setCustomerAddress2(manifestDto.getCustomerAddress2());			
		shipManifest.setCustomerCity(manifestDto.getCustomerCity());			
		shipManifest.setCustomerState(manifestDto.getCustomerState());		
		shipManifest.setCustomerPostalCode(manifestDto.getCustomerPostalCode());			
		shipManifest.setEndConsumerPurchaseOrderData(manifestDto.getEndConsumerPurchaseOrderData());		
		shipManifest.setEndConsumerName(manifestDto.getEndConsumerName());		
		shipManifest.setEndConsumerAddress1(manifestDto.getEndConsumerAddress1());			
		shipManifest.setEndConsumerAddress2(manifestDto.getEndConsumerAddress2());			
		shipManifest.setEndConsumerAddress3(manifestDto.getEndConsumerAddress3());		
		shipManifest.setEndConsumerCity(manifestDto.getEndConsumerCity());			
		shipManifest.setEndConsumerState(manifestDto.getEndConsumerState());		
		shipManifest.setEndConsumerPostalCode(manifestDto.getEndConsumerPostalCode());		
		shipManifest.setFillFacilityNumber(manifestDto.getFillFacilityNumber());
		shipManifest.setHazardousMaterialIndicator(manifestDto.getHazardousMaterialIndicator());		
		shipManifest.setPrimaryOrderNumber(manifestDto.getPrimaryOrderNumber());
		if(StringUtils.isNotEmpty(manifestDto.getDateProcessed())) {
			Date dateProcessed;
			try {
				dateProcessed = generateDate(manifestDto.getDateProcessed(), "MM/dd/yyyy");
				shipManifest.setDateProcessed(dateProcessed);
			} catch (ParseException e) {
				response.getMessages().add("Unable to parse date processed.");
				response.setValid(false);
			}
		}
		shipManifest.setSpecialInstructions1(manifestDto.getSpecialInstructions1());		
		shipManifest.setSpecialInstructions2(manifestDto.getSpecialInstructions2());		
		shipManifest.setDealerInformation1(manifestDto.getDealerInformation1());			
		shipManifest.setDealerInformation2(manifestDto.getDealerInformation2());		
		shipManifest.setDealerInformation3(manifestDto.getDealerInformation3());			
		shipManifest.setDealerInformation4(manifestDto.getDealerInformation4());			
		shipManifest.setDealerInformation5(manifestDto.getDealerInformation5());			
		shipManifest.setDealerInformation6(manifestDto.getDealerInformation6());		
		shipManifest.setShippingInformation1(manifestDto.getShippingInformation1());		
		shipManifest.setShippingInformation2(manifestDto.getShippingInformation2());			
		shipManifest.setShippingInformation3(manifestDto.getShippingInformation3());			
		shipManifest.setShippingInformation4(manifestDto.getShippingInformation4());			
		shipManifest.setShippingInformation5(manifestDto.getShippingInformation5());			
		shipManifest.setShippingInformation6(manifestDto.getShippingInformation6());			
		shipManifest.setCartonID(manifestDto.getCartonID());			
		shipManifest.setBulkCartonIndicator(manifestDto.getBulkCartonIndicator());		
		shipManifest.setCartonWeight(manifestDto.getCartonWeight());			
		shipManifest.setItemPrefix(manifestDto.getItemPrefix());			
		shipManifest.setItemStock(manifestDto.getItemStock());
		if(StringUtils.isNotEmpty(manifestDto.getManifestDateTime())) {
			Date scanDateTime;
			try {
				scanDateTime = generateDate(manifestDto.getScanDateTime(), "MM/dd/yyyy hh:mm:ss a");
				shipManifest.setScanDateTime(scanDateTime);
			} catch (ParseException e) {
				response.getMessages().add("Unable to parse scan date time.");
				response.setValid(false);
			}
			
		}
		if(StringUtils.isNotEmpty(manifestDto.getManifestDateTime())) {
			Date manifestDateTime;
			try {
				manifestDateTime = generateDate(manifestDto.getManifestDateTime(), "MM/dd/yyyy hh:mm:ss a");
				shipManifest.setManifestDate(manifestDateTime);
			} catch (ParseException e) {
				response.getMessages().add("Unable to parse manifested date time.");
				response.setValid(false);
			}
			
		}
		shipManifest.setCartonBarcode(manifestDto.getCartonBarcode());
		
		//Do not save if there are errors
		if(response.isValid()) {
			try {
				shipManifestDao.create(shipManifest);
			}
			catch(PersistenceException e) {
				response.getMessages().add("The carton id already exists. Duplicate detected.");
				response.setValid(false);
			}
		}
		
		try {
			tm.commit();
		} catch (SecurityException | IllegalStateException
				| RollbackException | HeuristicMixedException
				| HeuristicRollbackException | SystemException e) {
			response.getMessages().add("Unable to commit transaction for order.");
			response.setValid(false);
		}
		
		return response;
    }

    private Date generateDate(String dateString, String format) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.parse(dateString);
    }
    
    public void processUsscoManifestException(@Header("CamelFileNameOnly") String fileName, @Header("rawMessage") String rawMessage, @Body String body) {
    	LOG.info("Error parsing ussco manifest");
    	List<Response> success = new ArrayList<>();
    	List<Response> failed = new ArrayList<>();
    	Response response = new Response();
    	response.setValid(false);
    	response.getMessages().add(body);
    	failed.add(response);
    	
    	sendResultMessage(success, failed, rawMessage, fileName);
    }
    
    private void sendResultMessage(List<Response> success, List<Response> failed, String rawMessage, String filename) {
        try(InputStream inputStream = new ByteArrayInputStream(rawMessage.getBytes())){
        LOG.info("Sending email for Ussco manifest report");
        Properties usscoProperties = PropertiesLoader.getAsProperties("ussco.integration");
        String from = usscoProperties.getProperty("shipManifestReportFrom","admin@apdmarketplace.com");
        Set<String> recipients = new HashSet<>();
        recipients.add(usscoProperties.getProperty("shipManifestReportTo"));
        Set<String> ccRecipients = new HashSet<>();
        String cc = usscoProperties.getProperty("shipManifestReportCC");
        if(StringUtils.isNotEmpty(cc)) {
        	ccRecipients.add(cc);
        }
        String subject = "[Ussco] Ship Manifest Report";
        
        List<Attachment> attachments = new ArrayList<>();
        Attachment attachment = new Attachment();
        attachment.setFileName(filename);
        attachment.setMimeType(MimeType.txt);
        attachment.setContent(inputStream);
        attachments.add(attachment);
        String emailBody = "<h1>Ussco Ship Manifest Report</h1>"
        		+ "<p>Attempted processing attached file on "+new Date()+"</p>";
        emailBody += "<h3>Failed:</h3><p>";
        for(Response response: failed) {
        	if(response.getManifestDto() != null) {
        		emailBody += "Error: '"+response.getManifestDto().getCustomerPurchaseOrderNumber()+", "+response.getManifestDto().getCartonBarcode()+"'<br />";
        	}
        	emailBody += "Reasons:<br />";
        	for(String message : response.messages) {
        		emailBody += "&nbsp;" + message + "<br />";
        	}
        }
        emailBody += "</p>";
        emailBody += "<h3>Successful:</h3><p>";
        for(Response response: success) {
        	emailBody += "Importing: '"+response.getManifestDto().getCustomerPurchaseOrderNumber()+", "+response.getManifestDto().getCartonBarcode()+"'<br />";
        }
        emailBody += "</p>";
        
       
        
        MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, ccRecipients, subject, emailBody, attachments);
		
		emailService.sendEmail(message);
        

        } catch (IOException ex) {
            LOG.error("Could not rawMessage to inputStream", ex);
        }
    }
    
    public class Response {
    	private UsscoManifestCSVDto manifestDto;
    	private boolean isValid;
    	private List<String> messages = new ArrayList<>();
		public UsscoManifestCSVDto getManifestDto() {
			return manifestDto;
		}
		public void setManifestDto(UsscoManifestCSVDto manifestDto) {
			this.manifestDto = manifestDto;
		}
		public boolean isValid() {
			return isValid;
		}
		public void setValid(boolean isValid) {
			this.isValid = isValid;
		}
		public List<String> getMessages() {
			return messages;
		}
		public void setMessages(List<String> messages) {
			this.messages = messages;
		}   	
    	
    }
}
