package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.OrderTypeDto;
import org.apache.camel.Converter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.edi.model.x12.edi850.PurchaseOrder;
import javax.edi.model.x12.edi850.segment.PurchaseOrderBeginningSegment;
import javax.edi.model.x12.edi850.segment.PurchaseOrderBody;
import javax.edi.model.x12.edi850.segment.PurchaseOrderItemGroup;
import javax.edi.model.x12.edi850.segment.ReferenceMessageGroup;
import javax.edi.model.x12.edi850.segment.ShipmentInformationGroup;
import javax.edi.model.x12.edi850.segment.ShippingBillingGroup;
import javax.edi.model.x12.segment.AdditionalNameInformation;
import javax.edi.model.x12.segment.AddressInformation;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.MessageText;
import javax.edi.model.x12.segment.POBaselineItemData;
import javax.edi.model.x12.segment.PersonContact;
import javax.edi.model.x12.segment.ReferenceNumber;
import org.apache.commons.lang.StringUtils;

@Converter
public class PurchaseOrderTranslator4010 {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderTranslator4010.class);
    public static final String BILL_TO_CODE = "BT";
    public static final String SHIP_TO_CODE = "ST";
    private static final String GEOCODE_QUALIFIER = "UL";
    public static final String FINAL_LOCATION_CODE = "MQ";
    public static final String CUSTOMER_SKU_QUALIFIER = "IN";
    public static final String APD_SKU_QUALIFIER = "VN";
    /**
     * the 12 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Identification Number (GTIN). 
     * Also known as the Universal Product Code (U.P.C.)
     */
    public static final String UNIVERSAL_PRODUCT_CODE_QUALIFIER = "UP";
    public static final String EMPTY_STRING = "";
    public static final String SHIP_TO_DISTRIBUTION_CENTER_CODE = "BY";
    public static final String TELEPHONE_QUALIFIER = "TE";
    public static final String PRIMARY_CONTACT_QUALIFIER = "AJ";
    public static final String DEPARTMENT_QUALIFIER = "BD";
    public static final String BUYER_SIZE_CODE = "IZ";
    public static final String BUYER_COLOR = "BO";
    /**
     * The 13 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Identification Number (GTIN) 
     **/
    public static final String GLOBAL_TRADE_IDENTIFICATION_NUMBER = "EN";
    public static final String UPC_EAN_CASE_CODE = "UA";
    /**
     * the 14 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Item Number (GTIN)
     */
    public static final String GLOBAL_TRADE_ITEM_NUMBER = "UK";
    public static final String DO_NOT_DELIVER_AFTER_QUALIFIER = "063";
    public static final String DIVISION_TYPE_CODE = "19";
    public static final String MERCHANDISE_TYPE_CODE = "MR";
    public static final String VENDOR_ORDER_NUMBER_TYPE_CODE = "VN";
    public static final String DEPARTMENT_TYPE_CODE = "DP";
    public static final String BUYER_SIZE_CODE_VALUE = "buyer size code";
    public static final String BUYER_COLOR_VALUE = "buyer color";
    public static final String GLOBAL_TRADE_IDENTIFICATION_NUMBER_VALUE = "global trade identification number";
    public static final String UPCEAN_CASE_CODE_VALUE = "u.p.c/ean case code";
    public static final String GLOBAL_TRADE_ITEM_NUMBER_VALUE = "global trade item number";
    public static final String UNIVERSAL_PRODUCT_CODE_VALUE = "universal product code";
    public static final String ORDER_CONTACT_QUALIFIER = "OC";
    public static final String USPS_QUALIFIER_FOR_CUSTOMER_SKU = "SW";
    public static final String EDI_ORDER_TYPE = "EDI";
    public static final String CONTRACT_NUMBER_QUALIFIER = "CT";
    private HashMap<String, UnitOfMeasureDto> unitsOfMeasure;

    public List<PurchaseOrderDto> fromX12EDI(PurchaseOrder po) {
        LOG.info("body: {}", po);
        List<PurchaseOrderDto> dtos = new ArrayList<>();
        for(PurchaseOrderBody purchaseOrderEdi: po.getPurchaseOrderBody()){
            PurchaseOrderDto poDto = new PurchaseOrderDto();
            poDto.setInterchangeId(po.getEnvelopeHeader().getInterchangeControlNumber());
            poDto.setGroupId(po.getGroupEnvelopeHeader().getGroupControlNumber());
            poDto.setTransactionId(purchaseOrderEdi.getHeader().getTransactionSetHeader().getTransactionSetControlNumber());
            poDto.setShipToAddressDto(new AddressDto());
            poDto.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto()); 
            OrderTypeDto orderTypeDto = new OrderTypeDto();
            orderTypeDto.setValue(EDI_ORDER_TYPE);
            poDto.setOrderType(orderTypeDto);
            final PurchaseOrderBeginningSegment beginningSegment = purchaseOrderEdi.getHeader().getBeginningSegment();
            poDto.setCustomerPoNumber(beginningSegment.getPurchaseOrderNumber());
            poDto.setOrderDate(beginningSegment.getPurchaseOrderDate());
            if(purchaseOrderEdi.getHeader().getContacts()!=null){
                for(PersonContact personContact : purchaseOrderEdi.getHeader().getContacts()){
                    processPersonContact(personContact, poDto);
                }
            }
            if(purchaseOrderEdi.getHeader().getReferenceNumbers()!=null){
                for(ReferenceNumber referenceNumber :purchaseOrderEdi.getHeader().getReferenceNumbers()){
                    processHeaderReferenceNumber(referenceNumber, poDto);
                }
            }
            if(purchaseOrderEdi.getHeader().getShippingBillingGroups()!=null){
                for(ShippingBillingGroup shipBillGroup:purchaseOrderEdi.getHeader().getShippingBillingGroups()){
                    processShippingBillingGroup(shipBillGroup, poDto);
                }
            }
            unitsOfMeasure = new HashMap<>();
            Map<String,PurchaseOrderDto> orderMap = new HashMap<>();
            for(PurchaseOrderItemGroup item:purchaseOrderEdi.getDetail().getPurchaseOrderItems()){
                processItem(item, orderMap, poDto);
            }
        dtos.addAll(orderMap.values());
        if(orderMap.values().isEmpty()){
            dtos.add(poDto);
        }
    }
    return dtos;

    }

    private void processLineItemQualifier(LineItemDto lineItem, String prodServiceIDQualifier, String prodServID) {
        if (lineItem.getItem() == null) {
            final ItemDto itemDto = new ItemDto();
            lineItem.setItem(itemDto);
        }
        if (lineItem.getItem().getSkus() == null) {
            lineItem.getItem().setSkus(new ArrayList<SkuDto>());
        }
        if (prodServiceIDQualifier != null) {
            switch (prodServiceIDQualifier) {
                case USPS_QUALIFIER_FOR_CUSTOMER_SKU:
                case CUSTOMER_SKU_QUALIFIER:
                    SkuDto customerSku = new SkuDto();
                    SkuTypeDto skuTypeDto = new SkuTypeDto();
                    skuTypeDto.setName(SkuTypeBp.SKUTYPE_CUSTOMER);
                    customerSku.setType(skuTypeDto);
                    customerSku.setValue(prodServID);
                    lineItem.setCustomerSku(customerSku);
                    lineItem.getItem().getSkus().add(customerSku);
                    break;
                case APD_SKU_QUALIFIER:
                    lineItem.setApdSku(prodServID);
                    SkuDto apdSku = new SkuDto();
                    SkuTypeDto apdSkuTypeDto = new SkuTypeDto();
                    apdSkuTypeDto.setName(SkuTypeBp.SKUTYPE_APD);
                    apdSku.setType(apdSkuTypeDto);
                    apdSku.setValue(prodServID);
                    lineItem.getItem().getSkus().add(apdSku);
                    break;
                default:
                    String type = "";
                    switch (prodServiceIDQualifier) {
                        case BUYER_SIZE_CODE:
                            type = BUYER_SIZE_CODE_VALUE;
                            break;
                        case BUYER_COLOR:
                            type = BUYER_COLOR_VALUE;
                            break;
                        case GLOBAL_TRADE_IDENTIFICATION_NUMBER:
                            type = GLOBAL_TRADE_IDENTIFICATION_NUMBER_VALUE;
                            break;
                        case UPC_EAN_CASE_CODE:
                            type = UPCEAN_CASE_CODE_VALUE;
                            break;
                        case GLOBAL_TRADE_ITEM_NUMBER:
                            type = GLOBAL_TRADE_ITEM_NUMBER_VALUE;
                            break;
                        case UNIVERSAL_PRODUCT_CODE_QUALIFIER:
                            type = UNIVERSAL_PRODUCT_CODE_VALUE;
                            break;
                        default:
                            break;
                    }
                    if (!type.equals("")) {
                        lineItem.getItem().getProperties().put(type, prodServID);
                    }
            }
        }

    }

    private void setUnitOfMeasure(String unit, LineItemDto lineItem) {
        if (unitsOfMeasure.containsKey(unit)) {
            lineItem.setUnitOfMeasure(unitsOfMeasure.get(unit));
        }
        else {
            UnitOfMeasureDto unitOfMeasure = new UnitOfMeasureDto();
            unitOfMeasure.setName(unit);
            lineItem.setUnitOfMeasure(unitOfMeasure);
            unitsOfMeasure.put(unit, unitOfMeasure);
        }
    }

    private void processPersonContact(PersonContact personContact, PurchaseOrderDto poDto) {
        if (TELEPHONE_QUALIFIER.equals(personContact.getCommunicationNumberQualifier())
                && PRIMARY_CONTACT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(personContact.getCommunicationNumber());
        }
        else {
            if (TELEPHONE_QUALIFIER.equals(personContact.getCommunicationNumberQualifier())
                    && StringUtils.isEmpty(poDto.getShipToAddressDto().getMiscShipToDto().getRequesterPhone())) {
                poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
                poDto.getShipToAddressDto().getMiscShipToDto()
                        .setRequesterPhone(personContact.getCommunicationNumber());
            }
        }
        if (DEPARTMENT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setDepartment(personContact.getName());
        }
        if (ORDER_CONTACT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
            poDto.getShipToAddressDto().setName(personContact.getName());
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(personContact.getCommunicationNumber());
            //Fax number avaliable personContact.getCommunicationNumber2()
            poDto.getShipToAddressDto().getMiscShipToDto().setOrderEmail(personContact.getCommunicationNumber3());
        }
    }

    private void processHeaderReferenceNumber(ReferenceNumber referenceNumber, PurchaseOrderDto poDto) {
        switch (referenceNumber.getReferenceIdentificationQualifier()) {
            case DIVISION_TYPE_CODE:
                poDto.getShipToAddressDto().getMiscShipToDto()
                        .setDivision(referenceNumber.getReferenceIdentification());
                break;
            case DEPARTMENT_TYPE_CODE:
                poDto.getShipToAddressDto().getMiscShipToDto().setDepartment(
                        referenceNumber.getReferenceIdentification());
                break;
            case MERCHANDISE_TYPE_CODE:
                poDto.setMerchandiseTypeCode(referenceNumber.getReferenceIdentification());
                break;
        }
    }

    private void processShippingBillingGroup(ShippingBillingGroup shipBillGroup, PurchaseOrderDto poDto) {
        final String entityIdentifierCode = shipBillGroup.getName().getEntityIdentifierCode();
        if (entityIdentifierCode != null) {
            switch (entityIdentifierCode) {
                case BILL_TO_CODE:
                    AddressDto billToAddresss = new AddressDto();
                    billToAddresss.setMiscShipToDto(new MiscShipToDto());
                    billToAddresss.getMiscShipToDto().setGlnID(shipBillGroup.getName().getIdentificationCode());
                    poDto.changeBillToAddressDto(billToAddresss);
                    break;
                case SHIP_TO_DISTRIBUTION_CENTER_CODE:
                case SHIP_TO_CODE:
                    AddressDto shipToAddress = poDto.getShipToAddressDto();
                    final GeographicLocation geographicLocation = shipBillGroup.getGeographicLocation();
                    if (geographicLocation != null) {
                        shipToAddress.setCity(geographicLocation.getCityName());
                        shipToAddress.setState(geographicLocation.getStateOrProvinceCode());
                        shipToAddress.setZip(geographicLocation.getPostalCode());
                        shipToAddress.setCountry(geographicLocation.getCountryCode());
                    }
                    if (GEOCODE_QUALIFIER.equals(shipBillGroup.getName().getIdentificationCodeQualifier())) {
                        shipToAddress.getMiscShipToDto().setGlnID(shipBillGroup.getName().getIdentificationCode());
                    }
                    final AddressInformation addressInformation = shipBillGroup.getAddressInformation();
                    if (addressInformation != null) {
                        shipToAddress.setLine1(addressInformation.getAddressLine1());
                        shipToAddress.setLine2(addressInformation.getAddressLine2());
                    }
                    poDto.setShipToAddressDto(shipToAddress);
                    break;
                default:
                    break;

            }
        }
    }

    private void processItem(PurchaseOrderItemGroup item, Map<String, PurchaseOrderDto> orderMap, PurchaseOrderDto poDto) throws NumberFormatException {
        POBaselineItemData baseLineItemData = item.getBaselineItemData();
        LineItemDto lineItem = new LineItemDto();
        LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
        lineItemStatusDto.setValue(LineItemStatusEnum.CREATED.getValue());
        lineItem.setStatus(lineItemStatusDto);
        //TODO: ensure this is calulated properly
        lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
        lineItem.setQuantity(baseLineItemData.getQuantity());
        String unit = baseLineItemData.getUnitOfMeasure();
        setUnitOfMeasure( unit, lineItem);
        lineItem.setUnitPrice(baseLineItemData.getUnitPrice());
        processLineItemQualifier(lineItem,baseLineItemData.getProdServiceIDQualifier1(),baseLineItemData.getProdServID1());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual2(), baseLineItemData.getProdServID2());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual3(), baseLineItemData.getProdServID3());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual4(), baseLineItemData.getProdServID4());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual5(), baseLineItemData.getProdServID5());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual6(), baseLineItemData.getProdServID6());
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual7(), baseLineItemData.getProdServID7());
        lineItem.setCustomerLineNumber(baseLineItemData.getAssignedIdentifier());
        if(item.getItemDescription()!=null){
            lineItem.setDescription(item.getItemDescription().getDescription());
        } else {
            //Description not allowed to be null.
            lineItem.setDescription("Recieved without description as EDI message.");
        }
        List<String> comments = new ArrayList<>();
        lineItem.setCustomerComments(comments);
        if(item.getShipmentInformationGroup()!=null && !item.getShipmentInformationGroup().isEmpty()){ 
            final ShipmentInformationGroup shipmentInformation = item.getShipmentInformationGroup().iterator().next();
            //indicates potential need to split order into multiple orders
            PurchaseOrderDto order = orderMap.get(shipmentInformation.getShipToName().getIdentificationCode());
            if(order == null){
                order = new PurchaseOrderDto(poDto);
                order.getShipToAddressDto().setCity(shipmentInformation.getGeographicLocation().getCityName());
                order.getShipToAddressDto().setCountry(shipmentInformation.getGeographicLocation().getCountryCode());
                int i = 1;
                for(AdditionalNameInformation additional: shipmentInformation.getAdditionalNameInformation()){
                    switch(i){
                    case 1:
                        order.getShipToAddressDto().getMiscShipToDto().setStreet2(additional.getName());
                        i++;
                        break;
                    case 2:
                        order.getShipToAddressDto().getMiscShipToDto().setStreet3(additional.getName());
                        break;
                    default:
                        LOG.error("Too many additional name information lines.");
                    }
                }
                if(shipmentInformation.getAddressInformation()!=null){
                    order.getShipToAddressDto().setLine1(shipmentInformation.getAddressInformation().getAddressLine1());
                }
                order.getShipToAddressDto().setState(shipmentInformation.getGeographicLocation().getStateOrProvinceCode());
                order.getShipToAddressDto().setZip(shipmentInformation.getGeographicLocation().getPostalCode());
                order.getShipToAddressDto().getMiscShipToDto().setFedstripNumber(shipmentInformation.getShipToName().getIdentificationCode());
                if(StringUtils.isNotBlank(shipmentInformation.getShipToName().getName())){
                    order.getShipToAddressDto().getMiscShipToDto().setDeliverToName(shipmentInformation.getShipToName().getName());
                }
                order.getShipToAddressDto().getMiscShipToDto().setFinanceNumber(item.getTermsOfSale().getDescription());
                order.setDeliveryDate(item.getDateTimeReference().getDate());
                for(ReferenceNumber ref : item.getReferenceNumber()){
                    switch(ref.getReferenceIdentificationQualifier()){
                        case CONTRACT_NUMBER_QUALIFIER:
                            order.getShipToAddressDto().getMiscShipToDto().setContractNumber(ref.getReferenceIdentification());
                            break;
                    }
                }
                if(item.getReferenceMessageGroup()!=null){
                    for(ReferenceMessageGroup messageGroup:item.getReferenceMessageGroup()){
                        for(MessageText message:messageGroup.getMessageTexts()){
                            comments.add(message.getFreeFormMessageText());
                        }
                    }
                }
            }
            order.getItems().add(lineItem);
            orderMap.put(shipmentInformation.getShipToName().getIdentificationCode(), order);
        }
        
     }
}
