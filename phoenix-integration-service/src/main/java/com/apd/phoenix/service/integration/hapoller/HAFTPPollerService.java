package com.apd.phoenix.service.integration.hapoller;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;

public interface HAFTPPollerService extends Service<String> {

    public static final ServiceName SINGLETON_SERVICE_NAME = ServiceName.JBOSS.append("edi-partner-ftp-poller", "ha",
            "singleton");

    public Injector<ServerEnvironment> getEnvInjector();
}
