package com.apd.phoenix.service.integration.camel.processor.cxml;

import static com.apd.phoenix.service.integration.camel.processor.cxml.CxmlCxmlRequestTranslator.ITEM_TYPE;
import static com.apd.phoenix.service.integration.camel.processor.cxml.CxmlCxmlRequestTranslator.US_DOLLARS;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import com.apd.phoenix.service.cxml.CxmlConstants;
import com.apd.phoenix.service.integration.cxml.model.fulfill.CXML;
import com.apd.phoenix.service.integration.cxml.model.fulfill.CarrierIdentifier;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Comments;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ConfirmationHeader;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ConfirmationItem;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ConfirmationRequest;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ConfirmationStatus;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Contact;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Country;
import com.apd.phoenix.service.integration.cxml.model.fulfill.CountryCode;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Credential;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Description;
import com.apd.phoenix.service.integration.cxml.model.fulfill.DocumentReference;
import com.apd.phoenix.service.integration.cxml.model.fulfill.From;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Header;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Identity;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Money;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Name;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ObjectFactory;
import com.apd.phoenix.service.integration.cxml.model.fulfill.OrderReference;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Phone;
import com.apd.phoenix.service.integration.cxml.model.fulfill.PostalAddress;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Request;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Sender;
import com.apd.phoenix.service.integration.cxml.model.fulfill.SharedSecret;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipControl;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeHeader;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeItem;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticePortion;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipNoticeRequest;
import com.apd.phoenix.service.integration.cxml.model.fulfill.ShipmentIdentifier;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Shipping;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Street;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Tax;
import com.apd.phoenix.service.integration.cxml.model.fulfill.TelephoneNumber;
import com.apd.phoenix.service.integration.cxml.model.fulfill.To;
import com.apd.phoenix.service.integration.cxml.model.fulfill.Total;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.CXMLCredentialDto;
import com.apd.phoenix.service.model.dto.CredentialDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.TransactionDto.TransactionOperation;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.persistence.jpa.MessageMetadataDao;
import com.apd.phoenix.service.utility.CxmlMessageUtils;

public class CxmlFulfillRequestTranslator {

    public static final String CXML_VERSION = "1.2.024";

    public static final String ENGLISH = "en-US";

    private static final String CARRIER_INDENTIFIER_DUNS_KEY = "DUNS";

    private static final String CARRIER_INDENTIFIER_SCAC_KEY = "SCAC";

    private static final String CARRIER_INDENTIFIER_NAME_KEY = "companyName";

    private static final String COUNTRY_CODE_USA_ISO = "US";

    private static final String COUNTRY_CODE_USA_VALUE = "1";

    private static final ObjectFactory factory = new ObjectFactory();

    @Inject
    MessageMetadataDao messageMetadataDao;

    @Inject
    CxmlMessageUtils transactionUtil;

    public String prepareXmlHeaders(@Body String body) {
        body = body.replaceFirst("<\\?xml.*\\?>", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.2.024/Fulfill.dtd\">");
        return body;
    }

    public CXML fromPoAckDtoToOrderConfirmation(@org.apache.camel.Header("interchangeId") String interchangeId,
            @org.apache.camel.Header("transactionId") String transactionId,
            @org.apache.camel.Header("deploymentMode") String deploymentMode,
            @org.apache.camel.Header("credentialDeploymentMode") String credDeploymentMode,
            POAcknowledgementDto poAcknowledgementDto) {
        if (StringUtils.isNotBlank(credDeploymentMode)) {
            deploymentMode = credDeploymentMode;
        }
        CXML confirmationRequest = createConfirmationRequest(poAcknowledgementDto, interchangeId, transactionId,
                deploymentMode);
        return confirmationRequest;
    }

    public List<CXML> fromAdvShipNoticeToShipNoticeRequest(
    		@org.apache.camel.Header("interchangeId") String interchangeId,
            @org.apache.camel.Header("originalInterchangeId") String originalInterchangeId,
            @org.apache.camel.Header("deploymentMode") String deploymentMode, 
            @org.apache.camel.Header("credentialDeploymentMode") String credDeploymentMode, 
            @org.apache.camel.Body ShipmentDto inputShipmentDto) throws Exception {
    	
    	if (StringUtils.isNotBlank(credDeploymentMode)) {
    		deploymentMode = credDeploymentMode;
    	}
    	Properties commonProperties = PropertiesLoader.getAsProperties("integration");
    	
    	if(deploymentMode == null || deploymentMode.isEmpty()) {
    		throw new Exception("Cxml transalator error - Missing deployment mode");
    	}
        AdvanceShipmentNoticeDto advanceShipmentNoticeDto = new AdvanceShipmentNoticeDto();
        final ArrayList<ShipmentDto> shipList = new ArrayList<>();
        shipList.add(inputShipmentDto);
        advanceShipmentNoticeDto.setShipmentDtos(shipList);
        advanceShipmentNoticeDto.setApdPo(inputShipmentDto.getCustomerOrderDto().getApdPoNumber());
        if(inputShipmentDto.getCustomerOrderDto().retrieveCustomerPoNumber()!=null){
            advanceShipmentNoticeDto.setCustomerPo(inputShipmentDto.getCustomerOrderDto()
                                                .retrieveCustomerPoNumber().getValue());
        }
        advanceShipmentNoticeDto.setOrderTransactionId(inputShipmentDto.
                                                        getCustomerOrderDto().getTransactionId());
        advanceShipmentNoticeDto.setSenderId(inputShipmentDto.getCustomerOrderDto().getPartnerAccountId());
    	
    	List<CXML> shipNotices = new ArrayList<>();
    	for(ShipmentDto shipmentDto : advanceShipmentNoticeDto.getShipmentDtos()) {
	    	CXML cxml = generateCurrentCXMLObject(shipmentDto.getCustomerOrderDto().getCredential());
	    	cxml.setPayloadID(transactionUtil.generatePayloadId());
	        //Request
	        Request request = new Request();
	        request.setDeploymentMode(deploymentMode);
	        //Ship notice request
	        ShipNoticeRequest shipNoticeRequest = new ShipNoticeRequest();
	        //Ship notice header
	        ShipNoticeHeader shipNoticeHeader = new ShipNoticeHeader();
	        
	        shipNoticeHeader.setShipmentID(transactionUtil.generationTransactionId());
	        shipNoticeHeader.setNoticeDate(transactionUtil.generateTimeStamp());
	        //Set delivery date to tomorrow
			Calendar c = Calendar.getInstance(); 
			c.setTime(new Date()); 
			c.add(Calendar.DATE, 1);
	        shipNoticeHeader.setShipmentDate(transactionUtil.generateTimeStamp(c.getTime()));
	        shipNoticeHeader.setDeliveryDate(transactionUtil.generateTimeStamp(c.getTime()));
                
            if(shipmentDto.getShipTime()!=null){
                shipNoticeHeader.setShipmentDate(transactionUtil.generateTimeStamp(shipmentDto.getShipTime()));
            }
            if(shipmentDto.getDeliveredTime()!=null){
                shipNoticeHeader.setDeliveryDate(transactionUtil.generateTimeStamp(shipmentDto.getDeliveredTime()));
            }
	        //Contact
	        Contact contactShipFrom = new Contact();
	        contactShipFrom.setRole("shipFrom");
	        Name nameFromAddress = new Name();
	        nameFromAddress.setvalue(commonProperties.getProperty("apd.address.from.name"));
	        nameFromAddress.setXmlLang(ENGLISH);
	        contactShipFrom.setName(nameFromAddress);
	        //From PostalAddress
	        PostalAddress postalAddressFromAddress = new PostalAddress();
	        Street fromStreet = new Street();
	        fromStreet.setvalue(commonProperties.getProperty("apd.address.from.street"));
	        postalAddressFromAddress.getStreet().add(fromStreet);
	        postalAddressFromAddress.setCity(commonProperties.getProperty("apd.address.from.city"));
	        postalAddressFromAddress.setState(commonProperties.getProperty("apd.address.from.state"));
	        postalAddressFromAddress.setPostalCode(commonProperties.getProperty("apd.address.from.zip"));
	        Country countryFromAddress = new Country();
	        countryFromAddress.setIsoCountryCode(commonProperties.getProperty("apd.address.from.country.iso"));
	        countryFromAddress.setvalue(commonProperties.getProperty("apd.address.from.country.name"));
	        postalAddressFromAddress.setCountry(countryFromAddress);
	        contactShipFrom.getPostalAddress().add(postalAddressFromAddress);
	        //End from address
	        //Phone

	        Phone fromPhone = new Phone();
	        TelephoneNumber fromTelephoneNumber = new TelephoneNumber();
	        CountryCode fromCountryCode = new CountryCode();
	        
	        fromCountryCode.setIsoCountryCode(COUNTRY_CODE_USA_ISO);
	        fromCountryCode.setvalue(COUNTRY_CODE_USA_VALUE);
	        fromTelephoneNumber.setCountryCode(fromCountryCode);
	        fromTelephoneNumber.setAreaOrCityCode(commonProperties.getProperty("apd.address.from.phone.area"));
	        fromTelephoneNumber.setNumber(commonProperties.getProperty("apd.address.from.phone.line"));
	        fromPhone.setTelephoneNumber(fromTelephoneNumber);
	        contactShipFrom.getPhone().add(fromPhone);
	

	        shipNoticeHeader.getContact().add(contactShipFrom);
                
	        //Customer service
            Contact contactCustomerService = new Contact();
            contactCustomerService.setRole("customerService");
            Name nameCustomerService = new Name();
            nameCustomerService.setvalue(commonProperties.getProperty("apd.address.csr.name"));
            nameCustomerService.setXmlLang(ENGLISH);
            contactCustomerService.setName(nameCustomerService);
            //From PostalAddress
            PostalAddress postalAddressCustomerService = new PostalAddress();
            Street csrStreet = new Street();
            csrStreet.setvalue(commonProperties.getProperty("apd.address.csr.street"));
            postalAddressCustomerService.getStreet().add(csrStreet);
            postalAddressCustomerService.setCity(commonProperties.getProperty("apd.address.csr.city"));
            postalAddressCustomerService.setState(commonProperties.getProperty("apd.address.csr.state"));
            postalAddressCustomerService.setPostalCode(commonProperties.getProperty("apd.address.csr.zip"));
            postalAddressCustomerService.setState(commonProperties.getProperty("apd.address.csr.state"));
            Country countrycustomerService = new Country();
            countrycustomerService.setIsoCountryCode(commonProperties.getProperty("apd.address.csr.country.iso"));
            countrycustomerService.setvalue(commonProperties.getProperty("apd.address.csr.country.name"));
            postalAddressCustomerService.setCountry(countrycustomerService);
            contactCustomerService.getPostalAddress().add(postalAddressCustomerService);
            shipNoticeHeader.getContact().add(contactCustomerService);
            //Phone
            Phone csrPhone = new Phone();
            TelephoneNumber csrTelephoneNumber = new TelephoneNumber();
            CountryCode csrCountryCode = new CountryCode();

            csrCountryCode.setIsoCountryCode(COUNTRY_CODE_USA_ISO);
            csrCountryCode.setvalue(COUNTRY_CODE_USA_VALUE);
            csrTelephoneNumber.setCountryCode(csrCountryCode);
            csrTelephoneNumber.setAreaOrCityCode(commonProperties.getProperty("apd.address.csr.phone.area"));
            csrTelephoneNumber.setNumber(commonProperties.getProperty("apd.address.csr.phone.line"));
            csrPhone.setTelephoneNumber(csrTelephoneNumber);
            contactCustomerService.getPhone().add(csrPhone);
            
	        //End of contact
	        //Comments
            if(shipmentDto.getComments()!=null){
                for(String commentText : shipmentDto.getComments()) {
                        Comments comments = new Comments();
                        comments.setvalue(commentText);
                        shipNoticeHeader.getComments().add(comments);
                }
            }
	        //End of comments
	        //Ship control
	        ShipControl shipControl = new ShipControl();
	        CarrierIdentifier carrierIdentifierDuns = new CarrierIdentifier();
	        carrierIdentifierDuns.setDomain(CARRIER_INDENTIFIER_DUNS_KEY);
	        carrierIdentifierDuns.setvalue(commonProperties.getProperty("apd.address.from.duns"));
	        shipControl.getCarrierIdentifier().add(carrierIdentifierDuns);
    	
	        CarrierIdentifier carrierIdentifierName = new CarrierIdentifier();
	        carrierIdentifierName.setDomain(CARRIER_INDENTIFIER_NAME_KEY);
	        carrierIdentifierName.setvalue(commonProperties.getProperty("apd.address.from.name"));
	        shipControl.getCarrierIdentifier().add(carrierIdentifierName);
        	
	        
	        ShipmentIdentifier shipmentIdentifier = new ShipmentIdentifier();
	        shipmentIdentifier.setvalue(advanceShipmentNoticeDto.getApdPo());
	        shipControl.setShipmentIdentifier(shipmentIdentifier);
	        shipNoticeRequest.getShipControl().add(shipControl);
	        //End of ship control
	        //Ship notice portion
	        ShipNoticePortion shipNoticePortion = new ShipNoticePortion();
	        OrderReference orderReference = new OrderReference();
	        
	        DocumentReference documentReference = new DocumentReference();
	        documentReference.setPayloadID(originalInterchangeId);
	        orderReference.setDocumentReference(documentReference);
	        shipNoticePortion.setOrderReference(orderReference);
	        
	        for(LineItemXShipmentDto lineItemXShipmentDto : shipmentDto.getLineItemXShipments()) {
	        	BigInteger quantity = lineItemXShipmentDto.getQuantity();
	        	String lineNumber = lineItemXShipmentDto.getLineItemDto().getCustomerLineNumber();
	        	ShipNoticeItem shipNoticeItem = new ShipNoticeItem();
	        	shipNoticeItem.setQuantity(quantity.toString());
	        	shipNoticeItem.setLineNumber(lineNumber);
	        	UnitOfMeasureDto unitOfMeasureDto = lineItemXShipmentDto.getLineItemDto().getUnitOfMeasure();
	        	if(unitOfMeasureDto != null) {
	        		shipNoticeItem.setUnitOfMeasure(unitOfMeasureDto.getName());
	        	}
	        	shipNoticePortion.getShipNoticeItem().add(shipNoticeItem);
	        }
	        
	        
	        shipNoticeRequest.getShipNoticePortion().add(shipNoticePortion);
	        //End of shipNoticePortion
	        shipNoticeRequest.setShipNoticeHeader(shipNoticeHeader);
	        //End of ship notice header
	        request.getConfirmationRequestOrShipNoticeRequestOrCopyRequestOrTimeCardInfoRequestOrReceiptRequestOrTimeCardRequest().add(shipNoticeRequest);
	        //End of ship notice request
	        cxml.getHeaderOrMessageOrRequestOrResponse().add(request);
	        //End of request
	        shipNotices.add(cxml);
    	}
    	
        return shipNotices;
    }

    private CXML createConfirmationRequest(POAcknowledgementDto poAcknowledgementDto, String interchangeId,
            String transactionId, String deploymentMode) {
        com.apd.phoenix.service.integration.cxml.model.fulfill.CXML cxml = generateCurrentCXMLObject(poAcknowledgementDto
                .getPurchaseOrderDto().getCredential());
        cxml.setPayloadID(interchangeId);
        cxml.setVersion(CXML_VERSION);
        com.apd.phoenix.service.integration.cxml.model.fulfill.Request request = new com.apd.phoenix.service.integration.cxml.model.fulfill.Request();
        request.setDeploymentMode(deploymentMode);
        List<Object> orderRequests = request
                .getConfirmationRequestOrShipNoticeRequestOrCopyRequestOrTimeCardInfoRequestOrReceiptRequestOrTimeCardRequest();
        ConfirmationRequest confirmationRequest = new ConfirmationRequest();
        ConfirmationHeader confirmationHeader = new ConfirmationHeader();
        confirmationHeader.setConfirmID(transactionId);
        String orderStatus = poAcknowledgementDto.getPurchaseOrderDto().getStatus().toUpperCase();

        //TODO verify whether the following code needs to be modified or is correct.
        if (orderStatus.equals(OrderStatusEnum.REFUSED.getValue())) {
            confirmationHeader.setType("reject");
        }
        else {
            boolean change = false;
            for (LineItemDto item : poAcknowledgementDto.getLineItems()) {
                if (!item.getStatus().getValue().equals(LineItemStatusEnum.ACCEPTED.getValue())) {
                    change = true;
                    break;
                }
            }
            if (change) {
                confirmationHeader.setType("detail");
            }
            else {
                confirmationHeader.setType("accept");
            }
        }
        //TODO: Determine where this should be done in workflow and add this to the buisness logic
        if (StringUtils.isEmpty(poAcknowledgementDto.getPurchaseOrderDto().getLastOrderConfirmationId())) {
            confirmationHeader.setOperation(TransactionOperation.NEW.getCXML().getValue());
        }
        else {
            confirmationHeader.setOperation(TransactionOperation.UPDATE.getCXML().getValue());
        }
        confirmationHeader.setNoticeDate(transactionUtil.generateTimeStamp());
        confirmationHeader.setTotal(calculateTotal(poAcknowledgementDto.getLineItems()));

        BigDecimal shippingValue = poAcknowledgementDto.getPurchaseOrderDto().getEstimatedShippingAmount();
        if (shippingValue != null && shippingValue.compareTo(BigDecimal.ZERO) == 1) {
            shippingValue.setScale(2, RoundingMode.HALF_UP);
            Shipping shipping = new Shipping();
            shipping.setMoney(buildMoney(shippingValue.toPlainString()));
            Description shippingDescription = new Description();
            shippingDescription.setvalue("");
            shippingDescription.setXmlLang(ENGLISH);
            shipping.setDescription(shippingDescription);
            confirmationHeader.setShipping(shipping);
        }

        BigDecimal estimatedTaxValue = poAcknowledgementDto.getPurchaseOrderDto().getMaximumTaxToCharge();
        if (estimatedTaxValue != null && estimatedTaxValue.compareTo(BigDecimal.ZERO) == 1) {
            estimatedTaxValue.setScale(2, RoundingMode.HALF_UP);
            Tax tax = new Tax();
            tax.setMoney(buildMoney(estimatedTaxValue.toPlainString()));
            Description taxDescription = new Description();
            taxDescription.setvalue("");
            taxDescription.setXmlLang(ENGLISH);
            tax.setDescription(taxDescription);
            confirmationHeader.setTax(tax);
        }

        confirmationRequest.setConfirmationHeader(confirmationHeader);
        OrderReference orderReference = new OrderReference();
        DocumentReference documentReference = new DocumentReference();
        documentReference.setPayloadID(poAcknowledgementDto.getPurchaseOrderDto().getOrderPayloadId());
        orderReference.setDocumentReference(documentReference);
        confirmationRequest.setOrderReference(orderReference);

        for (LineItemDto lineItem : poAcknowledgementDto.getLineItems()) {
            ConfirmationItem confirmationItem = new ConfirmationItem();
            confirmationItem.setLineNumber(lineItem.getCustomerLineNumber());
            confirmationItem.setQuantity(lineItem.getQuantity().toString());
            ConfirmationStatus confirmationStatus = new ConfirmationStatus();
            confirmationStatus.setQuantity(lineItem.getQuantity().toString());
            confirmationStatus.setUnitOfMeasure(lineItem.getUnitOfMeasure().getName());
            String itemStatus = lineItem.getStatus().getValue().toUpperCase();
            if (itemStatus.equals(LineItemStatusEnum.BACKORDERED.getValue())) {
                confirmationStatus.setType("backordered");
            }
            else if (itemStatus.equals(LineItemStatusEnum.PARTIAL_BACKORDERED.getValue())) {
                confirmationStatus.setType("backordered");
                confirmationStatus.setQuantity(lineItem.getBackorderedQuantity().toString());
            }
            else if (itemStatus.equals(LineItemStatusEnum.ITEM_QUANTITY_CHANGED.getValue())) {
                confirmationStatus.setType("detail");
                Date shipDate = poAcknowledgementDto.getPurchaseOrderDto().getShipDate();
                Date deliveryDate = poAcknowledgementDto.getPurchaseOrderDto().getDeliveryDate();
                confirmationStatus.setShipmentDate(generateShipDate(shipDate));
                confirmationStatus.setDeliveryDate(generateDeliveryDate(deliveryDate));
            }
            else if (itemStatus.equals(LineItemStatusEnum.ACCEPTED.getValue())) {
                confirmationStatus.setType("accept");
                Date shipDate = poAcknowledgementDto.getPurchaseOrderDto().getShipDate();
                Date deliveryDate = poAcknowledgementDto.getPurchaseOrderDto().getDeliveryDate();
                confirmationStatus.setShipmentDate(generateShipDate(shipDate));
                confirmationStatus.setDeliveryDate(generateDeliveryDate(deliveryDate));
            }
            else if (itemStatus.equals(LineItemStatusEnum.REJECTED.getValue())) {
                confirmationStatus.setType("rejected");
            }
            else {
                confirmationStatus.setType("accept");
                Date shipDate = poAcknowledgementDto.getPurchaseOrderDto().getShipDate();
                Date deliveryDate = poAcknowledgementDto.getPurchaseOrderDto().getDeliveryDate();
                confirmationStatus.setShipmentDate(generateShipDate(shipDate));
                confirmationStatus.setDeliveryDate(generateDeliveryDate(deliveryDate));
            }
            confirmationItem.getConfirmationStatus().add(confirmationStatus);
            confirmationItem.setUnitOfMeasure(lineItem.getUnitOfMeasure().getName());
            //APD does not use composite items
            confirmationItem.setItemType(ITEM_TYPE);

            confirmationRequest.getConfirmationItem().add(confirmationItem);
        }
        orderRequests.add(confirmationRequest);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(request);
        return cxml;
    }

    private String generateShipDate(Date shipDate) {
        if (shipDate != null) {
            return transactionUtil.generateTimeStamp(shipDate);
        }
        return transactionUtil.generateTimeStamp();
    }

    private String generateDeliveryDate(Date deliveryDate) {
        if (deliveryDate != null) {
            return transactionUtil.generateTimeStamp(deliveryDate);
        }
        return transactionUtil.generateTimeStamp();
    }

    private Total calculateTotal(List<LineItemDto> lineItems) {
        Total total = new Total();
        BigDecimal totalValue = new BigDecimal(BigInteger.ZERO);
        for (LineItemDto item : lineItems) {
            if (!LineItemStatusEnum.REJECTED.getValue().equals(item.getStatus().getValue())) {
                totalValue = totalValue.add(item.getUnitPrice().multiply(new BigDecimal(item.getQuantity())));
            }
        }
        Money money = buildMoney(totalValue.toPlainString());
        total.setMoney(money);

        return total;
    }

    private Money buildMoney(String quantity) {
        Money money = new Money();
        money.setCurrency(US_DOLLARS);
        money.setvalue(quantity);
        return money;
    }

    public com.apd.phoenix.service.integration.cxml.model.fulfill.CXML generateCurrentCXMLObject(
            CredentialDto credentialDto) {
        com.apd.phoenix.service.integration.cxml.model.fulfill.CXML cxml = new com.apd.phoenix.service.integration.cxml.model.fulfill.CXML();
        cxml.setTimestamp(transactionUtil.generateTimeStamp());
        cxml.setVersion(CXML_VERSION);
        cxml.setXmlLang(ENGLISH);
        cxml.getHeaderOrMessageOrRequestOrResponse().add(generateHeader(credentialDto));
        return cxml;
    }

    private Header generateHeader(CredentialDto credentialDto) {
        Header header = new Header();
        header.setFrom(generateFrom(credentialDto));
        header.setSender(generateSender(credentialDto));
        header.setTo(generateTo(credentialDto));

        return header;
    }

    private From generateFrom(CredentialDto credentialDto) {
        From from = factory.createFrom();
        for (CXMLCredentialDto fromCred : credentialDto.getCustomerOutgoingFromCredentials()) {
            Credential fromCredential = factory.createCredential();
            fromCredential.setDomain(fromCred.getDomain());
            Identity fromIdentity = factory.createIdentity();
            fromIdentity.getContent().add(fromCred.getIdentifier());
            fromCredential.setIdentity(fromIdentity);
            from.getCredential().add(fromCredential);
        }
        return from;
    }

    private To generateTo(CredentialDto credentialDto) {
        To to = factory.createTo();
        for (CXMLCredentialDto toCred : credentialDto.getCustomerOutgoingToCredentials()) {
            Credential toCredential = factory.createCredential();
            toCredential.setDomain(toCred.getDomain());
            Identity toIdentity = factory.createIdentity();
            toIdentity.getContent().add(toCred.getIdentifier());
            toCredential.setIdentity(toIdentity);
            to.getCredential().add(toCredential);
        }
        return to;
    }

    private Sender generateSender(CredentialDto credentialDto) {
        Sender sender = factory.createSender();
        for (CXMLCredentialDto senderCred : credentialDto.getCustomerOutgoingSenderCredentials()) {
            Credential senderCredential = factory.createCredential();
            senderCredential.setDomain(senderCred.getDomain());
            Identity senderIdentity = factory.createIdentity();
            senderIdentity.getContent().add(senderCred.getIdentifier());
            senderCredential.setIdentity(senderIdentity);
            sender.getCredential().add(senderCredential);
        }
        SharedSecret sharedSecret = factory.createSharedSecret();
        sharedSecret.getContent().add(credentialDto.getCustomerOutgoingSharedSecret());
        sender.getCredential().get(0).getSharedSecretOrDigitalSignatureOrCredentialMac().add(sharedSecret);
        sender.setUserAgent(CxmlConstants.USER_AGENT);
        return sender;
    }
}
