package com.apd.phoenix.service.integration.routes;

import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.integration.mapping.ECDB2DataMapper;
import com.apd.phoenix.service.integration.mapping.SimilarItemsDataMapper;
import com.ussco.oagis._0.SyncItemMasterType;
import com.ussco.oagis._0.SyncProductRelationshipType;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;

/**
 * A Camel Java DSL Router
 */
public class CatalogDataImportRouteBuilder extends RouteBuilder {

    private String relationshipSource = "";
    private String relationshipTarget = "";
    private String itemSource = "";
    private String itemTarget = "";

    /**
     * Let's configure the Camel routing rules using Java code...
     */
    @Override
    public void configure() {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in catalog processing: ${exception.stacktrace}");

        JaxbDataFormat syncRelationshipFormat = new JaxbDataFormat(SyncProductRelationshipType.class.getPackage()
                .getName());
        JaxbDataFormat syncItemMasterFormat = new JaxbDataFormat(SyncItemMasterType.class.getPackage().getName());

        from(relationshipSource).convertBodyTo(String.class).log(LoggingLevel.TRACE, "${body}").unmarshal(
                syncRelationshipFormat).process(new SimilarItemsDataMapper()).split(body()).setHeader(
                Exchange.CONTENT_TYPE, constant("application/json")).marshal().json(JsonLibrary.Jackson).to(
                relationshipTarget);

        from(itemSource).convertBodyTo(String.class).log(LoggingLevel.TRACE, "${body}").unmarshal(syncItemMasterFormat)
                .process(new ECDB2DataMapper()).setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .marshal().json(JsonLibrary.Jackson).to(itemTarget);
    }

    public String getRelationshipSource() {
        return relationshipSource;
    }

    public void setRelationshipSource(String relationshipSource) {
        this.relationshipSource = relationshipSource;
    }

    public String getRelationshipTarget() {
        return relationshipTarget;
    }

    public void setRelationshipTarget(String relationshipTarget) {
        this.relationshipTarget = relationshipTarget;
    }

    public String getItemSource() {
        return itemSource;
    }

    public void setItemSource(String itemSource) {
        this.itemSource = itemSource;
    }

    public String getItemTarget() {
        return itemTarget;
    }

    public void setItemTarget(String itemTarget) {
        this.itemTarget = itemTarget;
    }

}