package com.apd.phoenix.service.integration.camel.processor;

import java.io.InputStream;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.io.IOUtils;

public class StreamToByteProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        InputStream stream = (InputStream) exchange.getIn().getBody();
        exchange.getOut().setBody(IOUtils.toByteArray(stream));
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());

    }

}
