package com.apd.phoenix.service.integration.picklist.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",")
public class PickCsv implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2621729271859425014L;

    @DataField(pos = 1, required = true, trim = true)
    private String shipperId;

    @DataField(pos = 2, required = true, trim = true)
    private String orderNumber;

    @DataField(pos = 3, required = true, trim = true)
    private String customerPoNumber;

    @DataField(pos = 4, required = true, trim = true)
    private String apdOrderNumber;

    @DataField(pos = 5, required = true, trim = true)
    private String inventoryId;

    @DataField(pos = 6, required = true, trim = true)
    private String lineReference;

    @DataField(pos = 7, required = true, trim = true)
    private String qtyToPick;

    @DataField(pos = 8, required = false, trim = true)
    private String qtyBo;

    public String getShipperId() {
        return shipperId;
    }

    public void setShipperId(String shipperId) {
        this.shipperId = shipperId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerPoNumber() {
        return customerPoNumber;
    }

    public void setCustomerPoNumber(String customerPoNumber) {
        this.customerPoNumber = customerPoNumber;
    }

    public String getApdOrderNumber() {
        return apdOrderNumber;
    }

    public void setApdOrderNumber(String apdOrderNumber) {
        this.apdOrderNumber = apdOrderNumber;
    }

    public String getInventoryId() {
        return inventoryId;
    }

    public void setInventoryId(String inventoryId) {
        this.inventoryId = inventoryId;
    }

    public String getLineReference() {
        return lineReference;
    }

    public void setLineReference(String lineReference) {
        this.lineReference = lineReference;
    }

    public String getQtyToPick() {
        return qtyToPick;
    }

    public void setQtyToPick(String qtyToPick) {
        this.qtyToPick = qtyToPick;
    }

    public String getQtyBo() {
        return qtyBo;
    }

    public void setQtyBo(String qtyBo) {
        this.qtyBo = qtyBo;
    }

}
