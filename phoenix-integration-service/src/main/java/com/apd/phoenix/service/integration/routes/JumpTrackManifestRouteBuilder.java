/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.manifest.processor.JumptrackShipManifestProcessor;
import com.apd.phoenix.service.integration.manifest.processor.JumptrackShipManifestTranslator;

/**
 *
 * @author nreidelb
 */
public class JumpTrackManifestRouteBuilder extends RouteBuilder {

    private String source;
    private String dlq;
    private String jumptrackEndpoint;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in jumptrack manifest processing: ${exception.stacktrace}");

        JaxbDataFormat manifestFormat = new JaxbDataFormat("com.apd.phoenix.service.dataformat.xml.manifest");
        JaxbDataFormat manifestResponseFormat = new JaxbDataFormat(
                "com.apd.phoenix.service.dataformat.xml.manifestresponse");

        //listen to queue
        from(source)
        //Error handler
                .errorHandler(
                        deadLetterChannel(dlq).maximumRedeliveries(2).redeliveryDelay(2000).useExponentialBackOff()
                                .backOffMultiplier(2))
                //Translate to xml pojo
                .bean(JumptrackShipManifestTranslator.class, "translateDtoToXml")
                //Marshal to xml
                .marshal(manifestFormat)
                //Send to jumptrack endpoint
                .setHeader(Exchange.HTTP_METHOD, constant("POST")).setHeader(Exchange.CONTENT_TYPE,
                        constant("text/html")).to(jumptrackEndpoint)
                //Unmarshal response to pojo
                .unmarshal(manifestResponseFormat)
                //Translate xml pojo to dto
                .bean(JumptrackShipManifestTranslator.class, "translateXmlToResponseDto")
                //Process response
                .bean(JumptrackShipManifestProcessor.class, "processResponse");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getJumptrackEndpoint() {
        return jumptrackEndpoint;
    }

    public void setJumptrackEndpoint(String jumptrackEndpoint) {
        this.jumptrackEndpoint = jumptrackEndpoint;
    }

    public String getDlq() {
        return dlq;
    }

    public void setDlq(String dlq) {
        this.dlq = dlq;
    }

}
