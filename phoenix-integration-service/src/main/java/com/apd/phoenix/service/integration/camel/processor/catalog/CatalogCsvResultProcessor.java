package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogCsvBp;
import com.apd.phoenix.service.catalog.CatalogCsvResult;
import com.apd.phoenix.service.catalog.CatalogCsvMessage.CatalogCsvTypeEnum;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogCsv;

@Stateless
public class CatalogCsvResultProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogCsvResultProcessor.class);

    @Inject
    private CatalogCsvBp catalogCsvBp;

    @Inject
    private CatalogBp catalogBp;

    public void process(CatalogCsvResult message) throws Exception {
        try {
            processInTransaction(message);
        }
        catch (Exception e) {
            LOGGER.error("Unable to begin processing csv row");
            throw e;
        }
    }

    private void processInTransaction(CatalogCsvResult message) {
        try {
            //Ensure the directory exists
            File tempDirectory = new File("/var/tmp");
            if (!tempDirectory.exists()) {
                tempDirectory.mkdir();
            }

            String filenameSuffix = message.getCatalogCsvType().getFileNameSuffix();

            String path = "/var/tmp/" + ((Long) message.getCatalogId()).hashCode() + filenameSuffix + ".csv";
            String storagePath = "catalogs/" + ((Long) message.getCatalogId()).hashCode() + filenameSuffix + ".csv";

            StringBuilder sb = new StringBuilder();
            CatalogCsv catalogCsv = null;
            FileWriter fileWriter = null;
            int itemsProcessed = message.getRows().size();
            for (String row : message.getRows()) {
                sb.append(row);
                sb.append("\n");
            }
            //If this is the first message for a correlationId, write the header!!
            catalogCsv = catalogCsvBp.getByCorrelationId(message.getCorrelationId());
            if (catalogCsv == null) {
                File file = new File(path);
                if (file.exists()) {
                    file.delete();
                    file.createNewFile();
                }
                fileWriter = new FileWriter(path);
                fileWriter.write(message.getHeader());
                catalogCsv = new CatalogCsv();
                catalogCsv.setCorrelationId(message.getCorrelationId());
                catalogCsv.setTotalItems(message.getTotalItems());
                catalogCsv.setCatalogId(message.getCatalogId());
                catalogCsv.setProcessedItems(0L);
                catalogCsv = catalogCsvBp.create(catalogCsv);
            }

            if (fileWriter == null) {
                fileWriter = new FileWriter(path, true);
            }
            catalogCsv.setProcessedItems(catalogCsv.getProcessedItems() + itemsProcessed);
            catalogCsv = catalogCsvBp.update(catalogCsv);
            LOGGER.info("writing!");
            fileWriter.append(sb.toString());
            fileWriter.close();
            if (catalogCsv.getTotalItems().compareTo(catalogCsv.getProcessedItems().longValue()) == 0) {
                LOGGER.info("DONE!");
                if (message.getCatalogCsvType().equals(CatalogCsvTypeEnum.SMART_OCI_CATALOG_CSV)) {
                    File fileToUpload = new File(path);
                    LOGGER.info("Smart OCI appending serial number");
                    List<String> lines = FileUtils.readLines(fileToUpload);
                    int count = 0;
                    for (String line : lines) {
                        if (count != 0)
                            lines.set(count, count + line);
                        count++;
                    }
                    LOGGER.info("Done Smart OCI appending serial number");
                    FileUtils.writeLines(fileToUpload, lines);
                }
                FileInputStream value = new FileInputStream(path);
                File fileToUpload = new File(path);
                catalogBp.setFile(storagePath, value, fileToUpload.length());
                Catalog catalog = catalogBp.findById(message.getCatalogId(), Catalog.class);
                if (catalog != null) {
                    catalog.setLastProcessEnd(new Date());
                    catalogBp.update(catalog);
                }
            }
        }
        catch (IOException e) {
            LOGGER.error("Could not write to the csv", e);
        }
    }

}
