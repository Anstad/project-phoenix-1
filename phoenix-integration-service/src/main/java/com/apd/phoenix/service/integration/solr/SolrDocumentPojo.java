package com.apd.phoenix.service.integration.solr;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.solr.client.solrj.beans.Field;

public class SolrDocumentPojo {

    @Field("id")
    private Long id;
    @Field("catalogxitemID")
    private Long catalogxitemId;
    @Field("propertyIconMap")
    private String propertyIconMap;
    @Field("classificationIconMap")
    private List<String> classificationIconMap;
    @Field("catXPropertyIconMap")
    private List<String> catXPropertyIconMap;
    @Field("CATALOG_ID")
    private Long catalogId;
    @Field("name")
    private String name;
    @Field("mname")
    private String manufacturerName;
    @Field("hierarchy_path")
    private String hierarchyPath;
    @Field("category")
    private String category;
    @Field("description")
    private String description;
    @Field("imageUrl")
    private List<String> imageUrl;
    @Field("filters")
    private List<String> filters;
    @Field("hierarchy_parent_id")
    private Long hierarchyParentId;
    @Field("price")
    private Float price;
    @Field("searchterms")
    private String searchTerms;
    @Field("CUSTOMER_SKU")
    private String customerSku;
    @Field("APD_SKU")
    private String apdSku;
    @Field("MANUFACTURER_SKU")
    private String manufacturerSku;
    @Field("VENDOR_SKU")
    private String vendorSku;
    @Field("vendorName")
    private String vendorName;
    @Field("coreitemexpirationdate")
    private Date coreItemExpirationDate;
    @Field("coreitemstartdate")
    private Date coreItemStartDate;
    @Field("UNIT_OF_MEASURE_NAME")
    private String unitOfMeasureName;
    @Field("ItemPropertyNameValuePair")
    private List<String> itemPropertyNameValuePair;
    @Field("item_status")
    private String item_status;
    @Field("mb_id")
    private List<String> mb_id;
    @Field("overrides_id")
    private List<String> overrides_id;
    @Field("hierarchy_path_string")
    private String hierarchy_path_string;
    @Field("item_multiple")
    private Integer multiple;
    @Field("item_minimum")
    private Integer minimum;
    @Field("companyFavoritesListIds")
    private List<String> companyFavoritesListIds;
    @Field("customerReplacementSku")
    private String customerReplacementSku;
    @Field("customerReplacementVendor")
    private String customerReplacementVendor;
    @Field("specialOrder")
    private String specialOrder;
    @Field("customOrder")
    private String customOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Long catalogId) {
        this.catalogId = catalogId;
    }

    public Long getCatalogxitemId() {
        return catalogxitemId;
    }

    public void setCatalogxitemId(Long catalogxitemId) {
        this.catalogxitemId = catalogxitemId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    public Date getCoreItemExpirationDate() {
        return coreItemExpirationDate;
    }

    public void setCoreItemExpirationDate(Date coreItemExpirationDate) {
        this.coreItemExpirationDate = coreItemExpirationDate;
    }

    public Date getCoreItemStartDate() {
        return coreItemStartDate;
    }

    public void setCoreItemStartDate(Date coreItemStartDate) {
        this.coreItemStartDate = coreItemStartDate;
    }

    public String getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public void setUnitOfMeasureName(String unitOfMeasureName) {
        this.unitOfMeasureName = unitOfMeasureName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getHierarchyParentId() {
        return hierarchyParentId;
    }

    public void setHierarchyParentId(Long hierarchyParentId) {
        this.hierarchyParentId = hierarchyParentId;
    }

    public String getHierarchyPath() {
        return hierarchyPath;
    }

    public void setHierarchyPath(String hierarchyPath) {
        this.hierarchyPath = hierarchyPath;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCustomerSku() {
        return customerSku;
    }

    public void setCustomerSku(String customerSku) {
        this.customerSku = customerSku;
    }

    public String getManufacturerSku() {
        return manufacturerSku;
    }

    public void setManufacturerSku(String manufacturerSku) {
        this.manufacturerSku = manufacturerSku;
    }

    public String getApdSku() {
        return apdSku;
    }

    public void setApdSku(String apdSku) {
        this.apdSku = apdSku;
    }

    public String getVendorSku() {
        return vendorSku;
    }

    public void setVendorSku(String vendorSku) {
        this.vendorSku = vendorSku;
    }

    public String getPropertyIconMap() {
        return propertyIconMap;
    }

    public void setPropertyIconMap(String propertyIconMap) {
        this.propertyIconMap = propertyIconMap;
    }

    public String getItem_status() {
        return item_status;
    }

    public void setItem_status(String item_status) {
        this.item_status = item_status;
    }

    public String getHierarchy_path_string() {
        return hierarchy_path_string;
    }

    public void setHierarchy_path_string(String hierarchy_path_string) {
        this.hierarchy_path_string = hierarchy_path_string;
    }

    public List<String> getClassificationIconMap() {
        if (classificationIconMap == null) {
            classificationIconMap = new ArrayList<>();
        }
        return classificationIconMap;
    }

    public void setClassificationIconMap(List<String> classificationIconMap) {
        this.classificationIconMap = classificationIconMap;
    }

    public List<String> getCatXPropertyIconMap() {
        if (catXPropertyIconMap == null) {
            catXPropertyIconMap = new ArrayList<>();
        }
        return catXPropertyIconMap;
    }

    public void setCatXPropertyIconMap(List<String> catXPropertyIconMap) {
        this.catXPropertyIconMap = catXPropertyIconMap;
    }

    public List<String> getImageUrl() {
        if (imageUrl == null) {
            imageUrl = new ArrayList<>();
        }
        return imageUrl;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<String> getFilters() {
        if (filters == null) {
            filters = new ArrayList<>();
        }
        return filters;
    }

    public void setFilters(List<String> filters) {
        this.filters = filters;
    }

    public List<String> getItemPropertyNameValuePair() {
        if (itemPropertyNameValuePair == null) {
            itemPropertyNameValuePair = new ArrayList<>();
        }
        return itemPropertyNameValuePair;
    }

    public void setItemPropertyNameValuePair(List<String> itemPropertyNameValuePair) {
        this.itemPropertyNameValuePair = itemPropertyNameValuePair;
    }

    public List<String> getMb_id() {
        if (mb_id == null) {
            mb_id = new ArrayList<>();
        }
        return mb_id;
    }

    public void setMb_id(List<String> mb_id) {
        this.mb_id = mb_id;
    }

    public List<String> getOverrides_id() {
        if (overrides_id == null) {
            overrides_id = new ArrayList<>();
        }
        return overrides_id;
    }

    public void setOverrides_id(List<String> overrides_id) {
        this.overrides_id = overrides_id;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Integer getMinimum() {
        return minimum;
    }

    public void setMinimum(Integer minimum) {
        this.minimum = minimum;
    }

    public List<String> getCompanyFavoritesListIds() {
        if (companyFavoritesListIds == null) {
            companyFavoritesListIds = new ArrayList<>();
        }
        return companyFavoritesListIds;
    }

    public void setCompanyFavoritesListIds(List<String> companyFavoritesListIds) {
        this.companyFavoritesListIds = companyFavoritesListIds;
    }

    public String getCustomerReplacementSku() {
        return customerReplacementSku;
    }

    public void setCustomerReplacementSku(String customerReplacementSku) {
        this.customerReplacementSku = customerReplacementSku;
    }

    public String getCustomerReplacementVendor() {
        return customerReplacementVendor;
    }

    public void setCustomerReplacementVendor(String customerReplacementVendor) {
        this.customerReplacementVendor = customerReplacementVendor;
    }

    public String getSpecialOrder() {
        return specialOrder;
    }

    public void setSpecialOrder(String specialOrder) {
        this.specialOrder = specialOrder;
    }

    public String getCustomOrder() {
        return customOrder;
    }

    public void setCustomOrder(String customOrder) {
        this.customOrder = customOrder;
    }

}
