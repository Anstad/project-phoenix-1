package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

public class QueueCatchallRouteBuilder extends RouteBuilder {

    private String queue;
    private String dlq;

    @Override
    public void configure() throws Exception {
        from(queue).log(LoggingLevel.ERROR, "Unexpected partnerId: ${header.partnerId}").to(dlq);

    }

    public String getDlq() {
        return dlq;
    }

    public void setDlq(String dlq) {
        this.dlq = dlq;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

}
