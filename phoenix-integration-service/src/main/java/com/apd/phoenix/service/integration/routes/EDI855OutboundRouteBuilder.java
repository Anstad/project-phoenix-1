/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.PoAcknowledgementToEdi855Translator;
import com.apd.phoenix.service.integration.camel.processor.WiretapStringPrepareProcessor;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.EmailNotifierOnFTPDown;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.PGPDataFormat;

/**
 *
 * @author nreidelb
 */
public class EDI855OutboundRouteBuilder extends EDIRouteBuilder {

    private String datamodelSource;
    private String partnerEndpoint;
    private String logDLQ;
    private boolean isEncrypted = false;
    private String keyFileName;
    private String keyUserid;
    private boolean isArmored = true;
    private String outboundAckLogSource;
    private String ftpEndPointDown;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 855 outbound processing: ${exception.stacktrace}");

        EDIDataFormat ediDataFormat = new EDIDataFormat(javax.edi.model.x12.edi855.POAcknowledgement.class);

        PGPDataFormat pgpSignAndEncrypt = new PGPDataFormat();
        pgpSignAndEncrypt.setKeyFileName(keyFileName);
        pgpSignAndEncrypt.setKeyUserid(keyUserid);
        pgpSignAndEncrypt.setArmored(isArmored);

        from(datamodelSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI855.OUTBOUND_855))
                .errorHandler(
                        deadLetterChannel(ftpEndPointDown).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Set po number
                .setHeader("apdPo").jxpath("/in/body/apdPo").process(new PoAcknowledgementToEdi855Translator())
                .setHeader("interchangeId").jxpath("/in/body/interchangeEnvelopeHeader/interchangeControlNumber")
                .setHeader("groupId").jxpath("/in/body/groupEnvelopeHeader/groupControlNumber").setHeader(
                        "transactionId")
                .jxpath("/in/body/body[1]/header/transactionSetHeader/transactionSetControlNumber")
                .log(LoggingLevel.INFO, "Trans id: :" + simple("${header.transactionId}"))
                //Set message destination
                .setHeader("destination", constant(partnerEndpoint))
                .marshal(ediDataFormat)
                //Set file name
                .setHeader(Exchange.FILE_NAME, simple("ack855_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //Encrypt
                .log(LoggingLevel.DEBUG, "non encrypted: ${body}").choice().when(constant(isEncrypted)).choice().when(
                        simple("${header.resend}")).setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION_RESEND)).otherwise()
                .setHeader("messageEventType", constant(OrderLogDto.EventTypeDto.ORDER_ACKNOWLEDGEMENT_PRE_ENCRYPTION))
                .end().wireTap(outboundAckLogSource)
                /* By default, wiretap doesn't make a copy of the body, so this onPrepare processor will copy the body 
                 * to avoid it potentially showing up encyrpted in the logs
                 */
                .onPrepare(new WiretapStringPrepareProcessor()).marshal(pgpSignAndEncrypt).log(LoggingLevel.DEBUG,
                        "encrypted: ${body}")
                //End choice
                .end().choice().when(simple("${header.resend}")).setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.ORDER_ACKNOWLEDGEMENT_RESEND)).otherwise().setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.ORDER_ACKNOWLEDGEMENT)).end().wireTap(
                        outboundAckLogSource).wireTap(getBackupPartnerEndpoint()).to(partnerEndpoint);

        from(outboundAckLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI855.OUTBOUND_LOG_855))
        //Message event type
                //Communication type
                .setHeader("communicationType", constant(MessageMetadata.CommunicationType.OUTBOUND)).bean(
                        EDITransactionLogger.class, "logEDITransaction");

        from(ftpEndPointDown).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI855.FTP_DOWN_855)).choice()
                .when(header("partnerId").contains("usps")).bean(EmailNotifierOnFTPDown.class,
                        "uspsFTPServerDownEmailNotify").to(logDLQ).stop().otherwise().to(logDLQ);
    }

    /**
     * @return the datamodelSource
     */
    public String getDatamodelSource() {
        return datamodelSource;
    }

    /**
     * @param datamodelSource the datamodelSource to set
     */
    public void setDatamodelSource(String datamodelSource) {
        this.datamodelSource = datamodelSource;
    }

    /**
     * @return the partnerEndpoint
     */
    public String getPartnerEndpoint() {
        return partnerEndpoint;
    }

    /**
     * @param partnerEndpoint the partnerEndpoint to set
     */
    public void setPartnerEndpoint(String partnerEndpoint) {
        this.partnerEndpoint = partnerEndpoint;
    }

    /**
     * @return the logDLQ
     */
    public String getLogDLQ() {
        return logDLQ;
    }

    /**
     * @param logDLQ the logDLQ to set
     */
    public void setLogDLQ(String logDLQ) {
        this.logDLQ = logDLQ;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public String getKeyFileName() {
        return keyFileName;
    }

    public void setKeyFileName(String keyFileName) {
        this.keyFileName = keyFileName;
    }

    public String getKeyUserid() {
        return keyUserid;
    }

    public void setKeyUserid(String keyUserid) {
        this.keyUserid = keyUserid;
    }

    public boolean isArmored() {
        return isArmored;
    }

    public void setArmored(boolean isArmored) {
        this.isArmored = isArmored;
    }

    /**
     * @return the outboundAckLogSource
     */
    public String getOutboundAckLogSource() {
        return outboundAckLogSource;
    }

    /**
     * @param outboundAckLogSource the outboundAckLogSource to set
     */
    public void setOutboundAckLogSource(String outboundAckLogSource) {
        this.outboundAckLogSource = outboundAckLogSource;
    }

    public String getFtpEndPointDown() {
        return ftpEndPointDown;
    }

    public void setFtpEndPointDown(String ftpEndPointDown) {
        this.ftpEndPointDown = ftpEndPointDown;
    }
}
