package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.processor.InternalValidationBean;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;

public class InternalMessageValidationRouteBuilder extends RouteBuilder {

    private String source;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in internal validation: ${exception.stacktrace}");

        from(getSource())
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).bean(InternalValidationBean.class,
                "checkForDuplicateCustomerPo").choice().when(simple("${header.valid}")).bean(
                WorkflowServiceCaller.class, "processOrderPostValidation").end();

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
}
