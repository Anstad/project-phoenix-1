/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes.xcbl;

import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLPartnerDeterminer;
import org.apache.camel.LoggingLevel;

/**
 *
 * @author nreidelb
 */
public class XCBLPartnerDeterminerRouteBuilder extends RouteBuilder {

    private String inboundXCBLSource;
    private String inboundXCBLSink;

    @Override
    public void configure() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.ERROR,
                "Error in XCBL processing: ${exception.stacktrace}");

        from(getInboundXCBLSource())
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).bean(XCBLPartnerDeterminer.class,
                "determinePartner").to(getInboundXCBLSink());
    }

    /**
     * @return the inboundXCBLSource
     */
    public String getInboundXCBLSource() {
        return inboundXCBLSource;
    }

    /**
     * @param inboundXCBLSource the inboundXCBLSource to set
     */
    public void setInboundXCBLSource(String inboundXCBLSource) {
        this.inboundXCBLSource = inboundXCBLSource;
    }

    /**
     * @return the inboundXCBLSink
     */
    public String getInboundXCBLSink() {
        return inboundXCBLSink;
    }

    /**
     * @param inboundXCBLSink the inboundXCBLSink to set
     */
    public void setInboundXCBLSink(String inboundXCBLSink) {
        this.inboundXCBLSink = inboundXCBLSink;
    }

}
