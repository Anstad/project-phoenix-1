package com.apd.phoenix.service.integration.marfield.processor;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.SkuTypeBp;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.Attachment.MimeType;

@Stateless
public class MarfieldShipmentFileConverter {

    private static final int PO_NUMBER = 0;
    private static final int BILL_REF_NUMBER = 1;
    private static final int PRODUCT_CODE = 2;
    private static final int TRACKING_NUMBER = 3;
    private static final int SHIPMENT_DATE = 4;
    @SuppressWarnings("unused")
    // Not currently used
    private static final int SYS_ORDER_NUMBER = 5;
    private static final int ORDER_ITEM_SEQUENCE_ID = 6;
    private static final int EXTENDED_PRICE = 7;
    private static final int SHIPPING_CHARGE = 8;
    private static final int QUANTITY = 9;

    @Inject
    EmailService emailService;

    private static final Logger LOG = LoggerFactory.getLogger(MarfieldShipmentFileConverter.class);

    public String convertToString(byte[] body) {
        String content = new String(body);
        return content;
    }

    public void process(Exchange exchange) throws Exception {
    	List<String> failed = new ArrayList<>();
    	List<String> success = new ArrayList<>();
    	
    	String content = (String) exchange.getIn().getBody();
        LOG.info(content);
        content = content.trim();
        String[] lines = content.split("\\r?\\n");
        Map<String,ShipmentDto> shipments = new HashMap<>();
        for (String line : lines) {
            LOG.info("Line: " + line);
            if (line.startsWith("PONumber")) {
                LOG.debug("Ignoring first line with column information.");
            }
            else {
            	try {
            		processLine(line, shipments);
            		success.add(line);
            	} catch(Exception exp) {
            		failed.add(line);
            	}
            }
        }
        sendResultMessage(success, failed, content);
        exchange.setOut(exchange.getIn());
        exchange.getOut().setBody(shipments.values());
        exchange.getOut().getHeaders().put("rawMessage", content);
    }

    private Map<String, ShipmentDto> processLine(String line, Map<String, ShipmentDto> shipments) {

        try {
            String[] lineParts = line.split("\t");
            String apdPo = lineParts[BILL_REF_NUMBER];
            String trackingNumber = lineParts[TRACKING_NUMBER];
            ShipmentDto shipment = shipments.get(trackingNumber);
            if (shipment == null) {
                //This shipment hasn't been created yet
                shipment = new ShipmentDto();
                shipment.setCustomerOrderDto(new PurchaseOrderDto());
                shipment.getCustomerOrderDto().setApdPoNumber(apdPo);
                shipment.setTrackingNumber(trackingNumber);
                shipment.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
            }
            else {
                String apdPoNumberAlreadyAssociatedWithShipment = shipment.getCustomerOrderDto().getApdPoNumber();
                if (!apdPo.equals(apdPoNumberAlreadyAssociatedWithShipment)) {
                    LOG.error("ERROR: unexpected tracking number " + trackingNumber
                            + " found on orders with both  APD PO " + apdPo + " and "
                            + apdPoNumberAlreadyAssociatedWithShipment);
                }
            }
            LineItemXShipmentDto itemXShipment = new LineItemXShipmentDto();
            LineItemDto lineItem = new LineItemDto();
            itemXShipment.setLineItemDto(lineItem);

            String productCode = lineParts[PRODUCT_CODE];
            SkuDto vendorSku = new SkuDto();
            vendorSku.setValue(productCode);
            SkuTypeDto skuType = new SkuTypeDto();
            skuType.setName(SkuTypeBp.SKUTYPE_VENDOR);
            vendorSku.setType(skuType);
            lineItem.setVendorSku(vendorSku);

            String shipmentDate = lineParts[SHIPMENT_DATE];
            String[] dateFormatts = { "YY/M/DD", "YY/MM/DD" };
            try {
                Date shipDate = DateUtils.parseDate(shipmentDate, dateFormatts);
                shipment.setShipTime(shipDate);
            }
            catch (ParseException e) {
                LOG.error("Could not parse date " + shipmentDate);
            }

            String orderItemSequenceId = lineParts[ORDER_ITEM_SEQUENCE_ID];
            lineItem.setLineNumber(new Integer(orderItemSequenceId));

            String extendedPrice = lineParts[EXTENDED_PRICE];
            lineItem.setUnitPrice(new BigDecimal(extendedPrice));

            String shippingCharge = lineParts[SHIPPING_CHARGE];
            itemXShipment.setShipping(new BigDecimal(shippingCharge));

            String quantity = lineParts[QUANTITY];
            quantity = quantity.replaceAll("[^\\d.]", "");
            itemXShipment.setQuantity(new BigInteger(quantity));

            shipment.getLineItemXShipments().add(itemXShipment);

            shipments.put(trackingNumber, shipment);
        }
        catch (Exception exp) {
            throw exp;
        }
        return shipments;

    }

    private void sendResultMessage(List<String> success, List<String> failed, String rawMessage) {
    	try {    		
    		InputStream inputStream = new ByteArrayInputStream(rawMessage.getBytes());
            LOG.info("Sending email for marfield shipping report");
            Properties pickProperties = PropertiesLoader.getAsProperties("marfield.integration");
            String from = pickProperties.getProperty("marfieldShipmentReportEmailFrom","admin@apdmarketplace.com");
            Set<String> recipients = new HashSet<>();
            recipients.add(pickProperties.getProperty("marfieldShipmentReportEmailTo"));
            Set<String> ccRecipients = new HashSet<>();
            String cc = pickProperties.getProperty("marfieldShipmentReportEmailCC");
            if(StringUtils.isNotEmpty(cc)) {
            	ccRecipients.add(cc);
            }
            
            String subject = "[Marfield] Shipment Report";
            
            List<Attachment> attachments = new ArrayList<>();
            Attachment attachment = new Attachment();
            attachment.setFileName("marfiledshipment.txt");
            attachment.setMimeType(MimeType.txt);
            attachment.setContent(inputStream);
            attachments.add(attachment);
            String emailBody = "<h1>Marfield Shipment Processing Report</h1>"
            		+ "<p>Attempted processing attached file on "+new Date()+"</p>";
            emailBody += "<h3>Failed:</h3><p>";
            for(String strRow: failed) {
            	emailBody += "Warning: Ignoring '"+strRow+"'<br />";
            	
            }
            emailBody += "</p>";
            emailBody += "<h3>Successful:</h3><p>";
            for(String strRow: success) {
            	emailBody += "Imported Shipment '"+strRow+"'<br />";
            }
            emailBody += "</p>";
            MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, ccRecipients, subject, emailBody, attachments);
    		
    		emailService.sendEmail(message);
            } catch (Exception ex) {
                LOG.error("Could not rawMessage to inputStream", ex);
            }
    }
}
