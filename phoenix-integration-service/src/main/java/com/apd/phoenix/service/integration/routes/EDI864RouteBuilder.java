package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.integration.camel.processor.EDI864Parser;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class EDI864RouteBuilder extends EDIRouteBuilder {

    private String source;
    private String logSource;
    private String emailSource;

    @Override
    public void configure() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 864 inbound processing: ${exception.stacktrace}");

        from(source).startupOrder(
                getStartOrderOffset() + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI864.SOURCE))
                .multicast().parallelProcessing().to(getEmailSource(), getLogSource());

        from(getEmailSource()).startupOrder(
                getStartOrderOffset() + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI864.EMAIL))
                .bean(WorkflowServiceCaller.class, "processEdi864");

        from(getLogSource()).startupOrder(
                getStartOrderOffset() + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI864.LOG)).bean(
                EDI864Parser.class, "process864").bean(EDITransactionLogger.class, "logTXTransaction");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLogSource() {
        return logSource;
    }

    public void setLogSource(String logSource) {
        this.logSource = logSource;
    }

    public String getEmailSource() {
        return emailSource;
    }

    public void setEmailSource(String emailSource) {
        this.emailSource = emailSource;
    }

}
