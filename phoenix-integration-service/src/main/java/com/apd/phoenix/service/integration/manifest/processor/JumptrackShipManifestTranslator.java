package com.apd.phoenix.service.integration.manifest.processor;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Body;
import com.apd.phoenix.service.dataformat.xml.manifest.Request;
import com.apd.phoenix.service.dataformat.xml.manifestresponse.Response;
import com.apd.phoenix.service.model.dto.DeliveryDto;
import com.apd.phoenix.service.model.dto.DeliveryLineDto;
import com.apd.phoenix.service.model.dto.ManifestDto;
import com.apd.phoenix.service.model.dto.ManifestRequestDto;
import com.apd.phoenix.service.model.dto.ManifestSuccessDto;
import com.apd.phoenix.service.model.dto.RouteDto;
import com.apd.phoenix.service.model.dto.ShipmentUpdateDto;
import com.apd.phoenix.service.model.dto.SiteDto;
import com.apd.phoenix.service.model.dto.StopDto;

public class JumptrackShipManifestTranslator {

    public static final String JUMPTRACK_FILE_TYPE = "jumptrack-api-xml";
    public static final String JUMPTRACK_VERSION = "1.3";

    public Request translateDtoToXml(@Body ManifestRequestDto manifestRequestDto) {
        
        Request request = new Request();
        request.setType(JUMPTRACK_FILE_TYPE);
        request.setVersion(JUMPTRACK_VERSION);
        ArrayList<Request.Manifest> manifestList = new ArrayList<>();
        for(ManifestDto manifestDto : manifestRequestDto.getManifests()){
            Request.Manifest manifestXml = new Request.Manifest();
            
            //Route
            Request.Manifest.Route routeXml = new Request.Manifest.Route();
            RouteDto routeDto = manifestDto.getRouteDto();
            routeXml.setRouteName(routeDto.getRoute());
            routeXml.setDistributionName(routeDto.getDistribution());
            routeXml.setRegionName(routeDto.getRegion());
            manifestXml.setRoute(routeXml);
            
            //Stops
            ArrayList<Request.Manifest.Stop> stopsXml = new ArrayList<>();
            for(StopDto stop:manifestDto.getStops()){
                Request.Manifest.Stop stopXml = new Request.Manifest.Stop();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                stopXml.setDeliveryDate(sdf.format(stop.getDeliveryDate()));
                
                
                //Site
                Request.Manifest.Stop.Site siteXml = new Request.Manifest.Stop.Site();
                SiteDto siteDto = stop.getSiteDto();
                siteXml.setAccount(siteDto.getAccount());
                siteXml.setAddress1(siteDto.getAddress1());
                siteXml.setAddress2(siteDto.getAddress2());
                siteXml.setAddress3(siteDto.getAddress3());
                siteXml.setCity(siteDto.getCity());
                siteXml.setName(siteDto.getName());
                siteXml.setState(siteDto.getState());
                //Don't send zip +4 to Jumptrack
                if(siteDto.getZip() != null && siteDto.getZip().length() > 5){
                	siteXml.setZip(siteDto.getZip().substring(0,5));
                } else {
                	siteXml.setZip(siteDto.getZip());
                }
                stopXml.setSite(siteXml);
                
                //Delivery
                List<Request.Manifest.Stop.Delivery> deliverysXml = new ArrayList<>();
                Request.Manifest.Stop.Delivery deliveryXml = new Request.Manifest.Stop.Delivery();
                DeliveryDto deliveryDto = stop.getDeliveryDto();
                deliveryXml.setId(deliveryDto.getId());
                deliveryXml.setType(deliveryDto.getType().getLabel());
                deliveryXml.setDeliveryPo(deliveryDto.getDeliveryPo());
                //Delivery Line
                List<Request.Manifest.Stop.Delivery.DeliveryLine> deliveryLinesXml = new ArrayList<>();
                for(DeliveryLineDto deliveryLineDto : deliveryDto.getDeliveryLineDtos()) {
                	Request.Manifest.Stop.Delivery.DeliveryLine deliveryLineXml = new Request.Manifest.Stop.Delivery.DeliveryLine();
                	deliveryLineXml.setName(deliveryLineDto.getName());
                	deliveryLineXml.setDesc(deliveryLineDto.getDescription());
                	deliveryLineXml.setQtyTarget(deliveryLineDto.getQtyTarget().toString());
                	deliveryLineXml.setUom(deliveryLineDto.getUom().getLabel());
                	deliveryLinesXml.add(deliveryLineXml);
                }
                deliveryXml.setDeliveryLine(deliveryLinesXml);
                
                deliverysXml.add(deliveryXml);
                stopXml.setDelivery(deliverysXml);
                stopsXml.add(stopXml);
            }
            manifestXml.setStop(stopsXml);
            manifestList.add(manifestXml);
        }
        
        request.setManifest(manifestList);
        return request;
    }

    public ManifestSuccessDto translateXmlToResponseDto(@Body Response response) throws Exception {
        ManifestSuccessDto manifestSuccessDto = new ManifestSuccessDto();
        ArrayList<String> errorMessages = new ArrayList<>();
        Response.Errors errors = response.getErrors();
        for(Response.Errors.Error error:errors.getError()){
            errorMessages.add(error.getMessage());            
        }
        ArrayList<ShipmentUpdateDto> shipments = new ArrayList<>();
        manifestSuccessDto.setErrors(errorMessages);
        for(Response.Results.Manifest manifest: response.getResults().getManifest()){
            for(Response.Results.Manifest.Delivery delivery:manifest.getDelivery()){
                ShipmentUpdateDto shipmentUpdate = new ShipmentUpdateDto();
                shipmentUpdate.setTrackingNumber(delivery.getId());
                shipmentUpdate.setOperation(delivery.getOp());
                shipments.add(shipmentUpdate);
            }
        }
        manifestSuccessDto.setShipments(shipments);
        manifestSuccessDto.setSuccess(response.getSuccess());
        return manifestSuccessDto;
    }
}
