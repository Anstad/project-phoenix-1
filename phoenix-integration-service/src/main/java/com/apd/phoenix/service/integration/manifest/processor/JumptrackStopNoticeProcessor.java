package com.apd.phoenix.service.integration.manifest.processor;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.dataformat.xml.stopnotification.Response;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ShipManifest;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.persistence.jpa.ShipManifestDao;

@Stateless
public class JumptrackStopNoticeProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(JumptrackStopNoticeProcessor.class);

    private static final String JUMPTRACK_SENDER_ID = "jumptrack";

    private static final String JUMPTRACK_MESSAGE_DESTINATION = "APD";

    @Inject
    EmailService emailService;

    @Inject
    MessageService messageService;

    @Inject
    ShipManifestDao shipManifestDao;

    public void processStopNoticeResponse(@Body Response response) throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException,
    HeuristicMixedException, HeuristicRollbackException {
		LOG.info("processing response");
		
		Response.Results results = response.getResults();
		if (results != null) {
			Response.Results.Report report = results.getReport();
			if (report != null) {
				Response.Results.Report.StopNotification stopNotification = report
						.getStopNotification();
				if (stopNotification != null) {
					List<Response.Results.Report.StopNotification.DeliveryStop> deliveryStops = stopNotification
							.getDeliveryStop();
					if (deliveryStops != null) {
						for (Response.Results.Report.StopNotification.DeliveryStop deliveryStop : deliveryStops) {
							CustomerOrder customerOrder = null;
							// Deliveries
							List<Response.Results.Report.StopNotification.DeliveryStop.Delivery> deliveries = deliveryStop
									.getDelivery();
							for (Response.Results.Report.StopNotification.DeliveryStop.Delivery delivery : deliveries) {
								String manifestId = delivery.getId();
								ShipManifest shipManifest = shipManifestDao
										.findById(new Long(manifestId),
												ShipManifest.class);
								if (shipManifest != null) {
									CustomerOrder manifestOrder = shipManifest
											.getCustomerOrder();
									if (customerOrder == null) {
										customerOrder = manifestOrder;
									}
									if(manifestOrder != null) {
										processDelivery(delivery, manifestOrder);
									}
								}
							}
							// Signature
							Response.Results.Report.StopNotification.DeliveryStop.Signature signature = deliveryStop
									.getSignature();
							if (signature != null && customerOrder != null) {
								processSignature(signature, customerOrder);
							}
						}

					}

				}
			}
		}

		LOG.info("Sending email for jumptrack stop notification");
		Properties jumptrackProperties = PropertiesLoader
				.getAsProperties("jumptrack.integration");
		String from = jumptrackProperties.getProperty(
				"jumptrackStopNotificationEmailFrom",
				"admin@apdmarketplace.com");
		Set<String> recipients = new HashSet<>();
		recipients.add(jumptrackProperties
				.getProperty("jumptrackStopNotificationEmailTo"));
		Set<String> ccRecipients = new HashSet<>();
		String cc = jumptrackProperties.getProperty("jumptrackStopNotificationEmailCC");
		if(StringUtils.isNotEmpty(cc)) {
			ccRecipients.add(cc);
		}
		String subject = jumptrackProperties.getProperty("jumptrack.stop.email.subject");

		String emailBody = "<h1>Jumptrack Stop Notification Report</h1>"
				+ "<p>Received stop notification on " + new Date() + "<br />"
				+ "</p>";
		emailBody += "<h3>Failed:</h3><p>";

		Response.Errors errors = response.getErrors();
		if (errors != null && errors.getError() != null) {
			for (Response.Errors.Error error : errors.getError()) {
				emailBody += "Error: " + "code: " + error.getCode()
						+ ", message:" + error.getMessage() + "<br />";
			}
		}
		emailBody += "</p>";
		emailBody += "<h3>Successful: " + response.getSuccess() + "</h3><p>";
		emailBody += "</p>";
		MimeMessage message = emailService.prepareRawMessage(from,
				null,null, recipients, ccRecipients, subject, emailBody, null);

		emailService.sendEmail(message);
	}

    /**
     * Add driver note as order log
     * @param note
     * @param customerOrder
     */
    private void processDriverNote(String note, CustomerOrder customerOrder) {
        LOG.info("Processing driver note");
        String apdPo = customerOrder.getApdPo().getValue();

        MessageDto messageDto = new MessageDto();
        messageDto.setContent(note);
        MessageMetadataDto messageMetadataDto = new MessageMetadataDto();
        messageMetadataDto.setCommunicationType(CommunicationType.INBOUND);
        messageMetadataDto.setDestination(JUMPTRACK_MESSAGE_DESTINATION);
        messageMetadataDto.setFilePath(apdPo + "/jumptrack/" + "jumptrack-driver-note-" + System.currentTimeMillis()
                + ".txt");
        messageMetadataDto.setMessageType(MessageType.IMAGE);
        messageMetadataDto.setSenderId(JUMPTRACK_SENDER_ID);
        messageMetadataDto.setMessageDate(new Date());

        messageDto.setMessageMetadataDto(messageMetadataDto);

        messageService.receiveOrderMessage(messageDto, apdPo, EventTypeDto.DELIVERY_DRIVER_COMMENTS.name());
    }

    private void processDelivery(Response.Results.Report.StopNotification.DeliveryStop.Delivery delivery,
            CustomerOrder customerOrder) {
        //Driver Note
        processDriverNote(delivery.getDriverNote(), customerOrder);

        //Driver Images
        List<Response.Results.Report.StopNotification.DeliveryStop.Delivery.DriverPhoto> driverPhotos = delivery
                .getDriverPhoto();
        for (Response.Results.Report.StopNotification.DeliveryStop.Delivery.DriverPhoto driverPhoto : driverPhotos) {
            processDriverPhoto(driverPhoto, customerOrder);
        }

    }

    /**
     * Add driver photo as order log to order
     * @param driverPhoto
     * @param customerOrder
     */
    private void processDriverPhoto(
            Response.Results.Report.StopNotification.DeliveryStop.Delivery.DriverPhoto driverPhoto,
            CustomerOrder customerOrder) {

        LOG.info("Processing driver photo");
        String apdPo = customerOrder.getApdPo().getValue();

        if (driverPhoto != null && driverPhoto.getBase64() != null) {

            MessageDto messageDto = new MessageDto();
            messageDto.setContent(driverPhoto.getBase64().getValue());
            messageDto.setBase64Encoded(true);
            MessageMetadataDto messageMetadataDto = new MessageMetadataDto();
            String format = driverPhoto.getBase64().getFormat();
            LOG.debug("Driver photo has type: {}", format);
            ContentType contentType = ContentType.png;
            if (StringUtils.isNotEmpty(format)) {
                contentType = ContentType.valueOf(format);
                if (contentType != null) {
                    messageMetadataDto.setContentType(contentType.toString());
                }
                else {
                    contentType = ContentType.png;
                    messageMetadataDto.setContentType(contentType.toString());
                }
            }
            else {
                messageMetadataDto.setContentType(contentType.toString());
            }

            messageMetadataDto.setCommunicationType(CommunicationType.INBOUND);
            messageMetadataDto.setDestination(JUMPTRACK_MESSAGE_DESTINATION);
            messageMetadataDto.setFilePath(apdPo + "/jumptrack/" + "jumptrack-driver-photo-"
                    + System.currentTimeMillis() + "." + contentType.name());
            messageMetadataDto.setMessageType(MessageType.IMAGE);
            messageMetadataDto.setSenderId(JUMPTRACK_SENDER_ID);
            messageMetadataDto.setMessageDate(new Date());

            messageDto.setMessageMetadataDto(messageMetadataDto);

            messageService.receiveOrderMessage(messageDto, apdPo, EventTypeDto.DELIVERY_DRIVER_PHOTO.name());
        }
    }

    /**
     * Add signature as order log to order
     * @param signature
     * @param customerOrder
     */
    private void processSignature(Response.Results.Report.StopNotification.DeliveryStop.Signature signature,
            CustomerOrder customerOrder) {

        LOG.info("Processing signature");
        Response.Results.Report.StopNotification.DeliveryStop.Signature.Image image = signature.getImage();

        String apdPo = customerOrder.getApdPo().getValue();

        if (image != null && image.getBase64() != null) {

            MessageDto messageDto = new MessageDto();
            messageDto.setContent(image.getBase64().getValue());
            messageDto.setBase64Encoded(true);
            MessageMetadataDto messageMetadataDto = new MessageMetadataDto();
            String format = image.getBase64().getFormat();
            LOG.debug("Signature has type: {}", format);
            ContentType contentType = ContentType.png;
            if (StringUtils.isNotEmpty(format)) {
                contentType = ContentType.valueOf(format);
                if (contentType != null) {
                    messageMetadataDto.setContentType(contentType.toString());
                }
                else {
                    contentType = ContentType.png;
                    messageMetadataDto.setContentType(contentType.toString());
                }
            }
            else {
                messageMetadataDto.setContentType(contentType.toString());
            }
            messageMetadataDto.setCommunicationType(CommunicationType.INBOUND);
            messageMetadataDto.setDestination(JUMPTRACK_MESSAGE_DESTINATION);
            messageMetadataDto.setFilePath(apdPo + "/jumptrack/" + "jumptrack-signature-" + System.currentTimeMillis()
                    + "." + contentType.name());
            messageMetadataDto.setMessageType(MessageType.IMAGE);
            messageMetadataDto.setSenderId(JUMPTRACK_SENDER_ID);
            messageMetadataDto.setMessageDate(new Date());

            messageDto.setMessageMetadataDto(messageMetadataDto);

            messageService.receiveOrderMessage(messageDto, apdPo, EventTypeDto.DELIVERY_SIGNATURE.name());
        }
    }

    public enum ContentType {
        png("image/png"), jpg("image/jpeg"), jpeg("image/jpeg"), tiff("image/tiff"), tif("image/tiff"), bmp(
                "image/x-ms-bmp"), gif("image/gif");

        public String contentType;

        ContentType(String contentType) {

            this.contentType = contentType;
        }

        public String toString() {
            return contentType;
        }
    }
}
