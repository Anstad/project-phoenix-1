package com.apd.phoenix.service.integration.camel.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.MessageMetadataBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

@Stateless
public class EDITransactionLogger {

    @Inject
    MessageService messageService;

    @Inject
    MessageMetadataBp messageMetadataBp;

    @Inject
    CustomerOrderBp customerOrderBp;

    private static final Logger LOG = LoggerFactory.getLogger(EDITransactionLogger.class);

    private static final String APD_SENDER_ID = "apd";

    public void logNewPoEDITransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("interchangeId") String interchangeId, @Header("groupId") String groupId,
            @Header("transactionId") String transactionId, @Header("senderId") String senderId,
            @Header("originalMessage") String orignalMessage, PurchaseOrderDto body) throws NotSupportedException,
            SystemException, SecurityException, IllegalStateException, RollbackException, HeuristicMixedException,
            HeuristicRollbackException {
        logEDITransaction(apdPo, communicationType, destination, messageEventType, interchangeId, groupId,
                transactionId, senderId, orignalMessage);
    }

    public void logEDITransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("interchangeId") String interchangeId, @Header("groupId") String groupId,
            @Header("transactionId") String transactionId, @Header("senderId") String senderId, @Body String body)
            throws NotSupportedException, SystemException, SecurityException, IllegalStateException, RollbackException,
            HeuristicMixedException, HeuristicRollbackException {
        LOG.debug("apdPo: {}", apdPo);
        LOG.debug("body: {}", body);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(communicationType);
        metadata.setContentLength(body.length());
        if (StringUtils.isEmpty(destination)) {
            destination = "none";
        }
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-edi-" + System.currentTimeMillis() + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.EDI);
        metadata.setInterchangeId(interchangeId);
        metadata.setGroupId(groupId);
        metadata.setTransactionId(transactionId);
        if (senderId == null && communicationType == CommunicationType.OUTBOUND) {
            metadata.setSenderId(APD_SENDER_ID);
        }
        else {
            metadata.setSenderId(senderId);
        }
        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(body);

        messageService.receiveOrderMessage(message, apdPo, messageEventType.name());
    }

    public void logFATransaction(@Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("interchangeId") String interchangeId,
            @Header("groupId") String groupId, @Header("transactionId") String transactionId, 
            @Header("targetGroupControlNumber") String groupControlNumber,
            @Header("senderId") String senderId, @Body String body) {
        if (communicationType.equals(CommunicationType.INBOUND)) {
            senderId = APD_SENDER_ID;
        }
        List<CustomerOrder> ordersToLog = new ArrayList<>();
        if(StringUtils.isBlank(groupControlNumber)){
	        List<CustomerOrder> customerOrders = messageMetadataBp.getCustomerOrdersPlacedInLastYearByTransaction(transactionId, senderId);
	        if (customerOrders == null || customerOrders.isEmpty()) {
	        	LOG.error("No customer order found for inbound FA");
	        } else {
	        	ordersToLog.addAll(customerOrders);
	        }
        }else{
	        if(StringUtils.isNotBlank(groupControlNumber)){
	        	List<CustomerOrder> customerOrders = messageMetadataBp.getCustomerOrdersPlacedInLastYearByGroupControlNumber(groupControlNumber, senderId);
	        	if(customerOrders == null || customerOrders.isEmpty()){
	        		LOG.error("No customer orders found for inbound FA with group control number " + groupControlNumber 
	        				+ " and  senderId " + senderId);
	        	} else {
		        	ordersToLog.addAll(customerOrders);
		        }
	        }
        }
		for(CustomerOrder customerOrder:ordersToLog){
	        String apdPo = customerOrder.getApdPo().getValue();
	
	        LOG.debug("apdPo: {}", apdPo);
	        LOG.debug("body: {}", body);
	
	        MessageMetadataDto metadata = new MessageMetadataDto();
	        metadata.setCommunicationType(communicationType);
	        metadata.setContentLength(body.length());
	        if (StringUtils.isEmpty(destination)) {
	            destination = "none";
	        }
	        metadata.setDestination(destination);
	        metadata.setFilePath(apdPo + "/" + apdPo + "-edi-" + System.currentTimeMillis() + ".txt");
	        metadata.setMessageDate(new Date());
	        metadata.setMessageType(MessageType.EDI);
	        metadata.setInterchangeId(interchangeId);
	        metadata.setGroupId(groupId);
	        metadata.setTransactionId(transactionId);
	        metadata.setSenderId(senderId);
	        MessageDto message = new MessageDto();
	        message.setMessageMetadataDto(metadata);
	        message.setContent(body);
	
	        messageService.receiveOrderMessage(message, apdPo, OrderLogDto.EventTypeDto.FUNCTIONAL_ACKNOWLEDGEMENT.name());
        }
    }

    public void logTXTransaction(@Header("partnerId") String partnerId, @Header("poNumbers") List<String> poNumbers,
            @Header("senderId") String senderId, @Body String body) {

        MessageDto message = new MessageDto();
        MessageMetadataDto messageMetadataDto = new MessageMetadataDto();
        messageMetadataDto.setCommunicationType(CommunicationType.INBOUND);
        messageMetadataDto.setContentLength(body.length());
        messageMetadataDto.setMessageType(MessageType.EDI);
        messageMetadataDto.setDestination("none");
        messageMetadataDto.setMessageDate(new Date());
        message.setMessageMetadataDto(messageMetadataDto);
        message.setContent(body);

        if (poNumbers != null && !poNumbers.isEmpty()) {
            //Convert the poNumbers to a set to avoid logging duplicates
            HashSet<String> poSet = new HashSet<String>(poNumbers);
            for (String customerPo : poSet) {
                List<CustomerOrder> customerOrders = customerOrderBp.searchByCustomerPo(customerPo, partnerId);
                if (customerOrders.size() == 0) {
                    LOG.error("Could not find order for customer PO " + customerPo + " and partnerId " + partnerId
                            + " from EDI 864");
                }
                else {
                    if (customerOrders.size() > 1) {
                        LOG.error("Found multiple orders with customer PO " + customerPo + " and partnerId "
                                + partnerId);
                    }
                    for (CustomerOrder order : customerOrders) {
                        if (order.getApdPo() != null && !StringUtils.isEmpty(order.getApdPo().getValue())) {
                            String apdPo = order.getApdPo().getValue();
                            messageMetadataDto.setFilePath(apdPo + "/" + apdPo + "-edi864-"
                                    + System.currentTimeMillis() / 1000 + ".txt");
                            messageService.receiveOrderMessage(message, apdPo, EventTypeDto.PARTNER_REPORTED_ERRORS
                                    .name());
                        }
                        else {
                            LOG.error("No APD Po found on order with customer PO " + customerPo + " and partnerId "
                                    + partnerId + " from EDI 864");
                        }
                    }
                }
            }
        }
    }
}
