package com.apd.phoenix.service.integration.catalog.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(separator = ",", quote = "\"", quoting = true)
public class Item implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4712819105804768823L;

    @DataField(pos = 1, required = true)
    private String col1;

    @DataField(pos = 2, required = true)
    private String col2;

    public String getCol1() {
        return col1;
    }

    public void setCol1(String col1) {
        this.col1 = col1;
    }

    public String getCol2() {
        return col2;
    }

    public void setCol2(String col2) {
        this.col2 = col2;
    }

}
