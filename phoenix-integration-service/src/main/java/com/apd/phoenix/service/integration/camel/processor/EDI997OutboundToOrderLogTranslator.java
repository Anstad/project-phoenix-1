package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class EDI997OutboundToOrderLogTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI997OutboundToOrderLogTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = new String((byte[]) exchange.getIn().getBody());
        LOG.debug("Raw Message: {}", rawMessage);

        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }
        String transactionId = (String) exchange.getIn().getHeader("transactionId");

        LOG.debug("transaction id: {}", transactionId);
        if (transactionId == null) {
            throw new Exception("Transaction id not found");
        }

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.OUTBOUND);
        metadata.setContentLength(rawMessage.length());
        metadata.setDestination(destination);
        metadata.setFilePath("acknowledgements/" + transactionId + "/" + transactionId + "-edi977-"
                + System.currentTimeMillis() / 1000 + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.EDI);
        String groupId = (String) exchange.getIn().getHeader("groupId");
        metadata.setGroupId(groupId);
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        metadata.setInterchangeId(interchangeId);
        String senderId = (String) exchange.getIn().getHeader("senderId");
        metadata.setSenderId(senderId);
        String transactionId997 = (String) exchange.getIn().getHeader("transactionId");
        metadata.setTransactionId(transactionId997);

        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(rawMessage);
        exchange.getOut().setBody(message);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("messageEventType", EventTypeDto.FUNCTIONAL_ACKNOWLEDGEMENT);
    }

}
