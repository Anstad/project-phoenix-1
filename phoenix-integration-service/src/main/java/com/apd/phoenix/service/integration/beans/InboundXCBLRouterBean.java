package com.apd.phoenix.service.integration.beans;

import static com.apd.phoenix.service.utility.PartnerPropertyUtils.getRouteEndpointUri;
import static com.apd.phoenix.service.utility.PartnerPropertyUtils.getNoPartnerRouteEndpointUri;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.ExplicitCamelContextNameStrategy;
import org.apache.commons.lang.StringUtils;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLEntryPointRouteBuilder;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLOrderResponseErrorRouteBuilder;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLOrderResponseRouteBuilder;
import com.apd.phoenix.service.integration.routes.xcbl.XCBLOrderRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

;

@Startup
@Singleton
public class InboundXCBLRouterBean {

    private static final Logger LOG = LoggerFactory.getLogger(InboundXCBLRouterBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    private void setup() throws Exception {

    	Properties commonIntegrationProperties = PropertiesLoader.getAsProperties("integration");
        Properties sharedXCBLProperties = PropertiesLoader.getAsProperties("camel.xcbl.integration");
        setupAmq(camelContext, sharedXCBLProperties.getProperty("jmsBrokerUrl"));
        
        String inboundPartners = sharedXCBLProperties.getProperty("xcbl.inbound.partners");
        String[] inboundPartnerList = inboundPartners.split(",");
        
        String inboundXcblMessageAckPartners = sharedXCBLProperties.getProperty("xcbl.inbound.messageAck.partners");
        List<String> inboundXcblMessageAckPartnersList = new ArrayList<>();
        if (StringUtils.isNotBlank(inboundXcblMessageAckPartners)) {
        	inboundXcblMessageAckPartnersList = Arrays.asList(inboundXcblMessageAckPartners.split(","));
        }
        
        String inboundXcblOrderPartners = sharedXCBLProperties.getProperty("xcbl.inbound.order.partners");
        List<String> inboundXcblOrderPartnersList = new ArrayList<>();
        if (StringUtils.isNotBlank(inboundXcblOrderPartners)) {
        	inboundXcblOrderPartnersList = Arrays.asList(inboundXcblOrderPartners.split(","));
        }

        // unsupportedTransactionUri can be common to all partner routes because it currently has no consumers. 
        // Otherwise PartnerPropertyUtils.getPartnerRouteUri would be necessary.
        final String inboundUnsupportedTransactionUri = sharedXCBLProperties.getProperty("xcbl.inbound.dlq.uri");
        
        for (String partner : inboundPartnerList) {
            LOG.info("Adding XCBL routes for partner: {}", partner);

            // First load shared XCBL integration properties
            Properties partnerProperties = PropertiesLoader.getAsProperties("camel.xcbl.integration");
            // Then load partner properties, overriding properties loaded from shared XCBL integration properties
            partnerProperties.putAll(PropertiesLoader.getAsProperties(partner + ".integration"));
            
            // Common partner specific properties
            String sellerMPID = partnerProperties.getProperty("xcbl.seller.mpid");
            String buyerMPID = partnerProperties.getProperty("xcbl.buyer.mpid");
            String testIndicator = partnerProperties.getProperty("xcbl.testIndicator");
            String partnerEndpointUri = partnerProperties.getProperty("xcbl.inbound.partnerEndpoint.uri");
            if (StringUtils.isBlank(partnerEndpointUri)) {
            	partnerEndpointUri = getRouteEndpointUri(partner, "xcbl.inbound.uri", partnerProperties);
            }

            // Main inbound XCBL route for partner
            XCBLEntryPointRouteBuilder xcblEntryPointRouteBuilder = new XCBLEntryPointRouteBuilder();
            xcblEntryPointRouteBuilder.setPartnerId(partner);
            xcblEntryPointRouteBuilder.setTransactionUnsupportedSink(inboundUnsupportedTransactionUri);
            xcblEntryPointRouteBuilder.setTransactionSource(partnerEndpointUri);
            xcblEntryPointRouteBuilder.setXmlnsQualifier(partnerProperties.getProperty("xcbl.xmlns.qualifier"));
            
            //Decryption properties for partner main inbound XCBL route
            String decryptEnabled = partnerProperties.getProperty("xcbl.decryption.enabled");
            if (decryptEnabled != null && Boolean.parseBoolean(decryptEnabled)) {
                xcblEntryPointRouteBuilder.setKeyFileName(partnerProperties.getProperty("xcbl.decryption.keyFileName"));
                xcblEntryPointRouteBuilder.setKeyUserid(partnerProperties.getProperty("xcbl.decryption.keyUserid"));
                xcblEntryPointRouteBuilder.setArmored(new Boolean(partnerProperties.getProperty("xcbl.decryption.armored")));
                xcblEntryPointRouteBuilder.setDecryptionRequired(new Boolean(decryptEnabled));
                xcblEntryPointRouteBuilder.setKeyPassword(partnerProperties.getProperty("xcbl.decryption.password"));
                xcblEntryPointRouteBuilder.setGpgExecutableDirectory(sharedXCBLProperties.getProperty("xcbl.decryption.gpgExecutableDirectory"));
                xcblEntryPointRouteBuilder.setGpgTempDirectory(sharedXCBLProperties.getProperty("xcbl.decryption.gpgTmpDirectory"));
            }
            
            // Setup Inbound XCBL Order Route (if supported)
            // Inbound Order URI
            final String inboundOrderUri = getRouteEndpointUri(partner, 
            		"xcbl.inbound.order.uri",partnerProperties);
            
            if (StringUtils.isBlank(inboundOrderUri) || !inboundXcblOrderPartnersList.contains(partner)) {
            	xcblEntryPointRouteBuilder.setTransactionOrderSink(inboundUnsupportedTransactionUri);
            } else {
            	xcblEntryPointRouteBuilder.setTransactionOrderSink(inboundOrderUri);
            	
                LOG.info("Adding XCBL Order route for partner: {}", partner);
                XCBLOrderRouteBuilder xcblOrderRouteBuilder = new XCBLOrderRouteBuilder();
                
                // Partner specific properties common to all routes
                xcblOrderRouteBuilder.setSenderId(partner);
                xcblOrderRouteBuilder.setSellerMPID(sellerMPID);
                xcblOrderRouteBuilder.setBuyerMPID(buyerMPID);
                xcblOrderRouteBuilder.setTestIndicator(testIndicator);
                
                // Inbound Order URI
                xcblOrderRouteBuilder.setInboundOrderSource(inboundOrderUri);
                
                // Inbound Formatted Order URI
                final String inboundOrderFormattedUri = getRouteEndpointUri(partner, 
                		"xcbl.inbound.order.formatted.uri", partnerProperties);
                xcblOrderRouteBuilder.setInboundFormatSource(inboundOrderFormattedUri);
                
                // Inbound Order Log URI
                final String inboundOrderLogUri = getRouteEndpointUri(partner, 
                		"xcbl.inbound.order.log.uri", partnerProperties);
                xcblOrderRouteBuilder.setInboundLogSource(inboundOrderLogUri);
                
                // APD Order Rest URI
                final String apdOrderRestUri = commonIntegrationProperties.getProperty("apd.purchaseOrder.uri");
                xcblOrderRouteBuilder.setInboundOutput(apdOrderRestUri);
                
                // Prepare Message Ack URI
                final String prepareMessageAckUri = getRouteEndpointUri(partner, 
                		"xcbl.inbound.order.messageAck.prepare.uri", partnerProperties);
                xcblOrderRouteBuilder.setOutboundMessageAckPrepare(prepareMessageAckUri);
                
                // Source Message Ack UI
                final String sourceMessageAckUri = getRouteEndpointUri(partner, 
                		"xcbl.outbound.messageAck.uri", partnerProperties);
                xcblOrderRouteBuilder.setOutboundMessageAckSource(sourceMessageAckUri);
                
                // Outbound Message Ack URI
                final String outboundMessageAckUri = partnerProperties.getProperty("xcbl.outbound.partnerEndpoint.uri");
                xcblOrderRouteBuilder.setOutboundMessageAckOutput(outboundMessageAckUri);
                
                // Message Ack DataFormat Class
                final String messageAckDataFormatClass = partnerProperties.getProperty("xcbl.messageAck.dataformat.class");
                xcblOrderRouteBuilder.setOutboundMessageAckDataFormatClass(messageAckDataFormatClass);
                
                // Message Ack Translator Class
                final String messageAckTranslatorClass = partnerProperties.getProperty("xcbl.messageAck.translator.class");
                xcblOrderRouteBuilder.setOutboundMessageAckTranslatorClass(messageAckTranslatorClass);
                
                // Order DataFormat Class
                final String orderDataFormatClass = partnerProperties.getProperty("xcbl.order.dataformat.class");
                xcblOrderRouteBuilder.setInboundOrderDataFormatClass(orderDataFormatClass);
                
                // Order Translator Class
                final String orderTranslatorClass = partnerProperties.getProperty("xcbl.order.translator.class");
                xcblOrderRouteBuilder.setInboundOrderTranslatorClass(orderTranslatorClass);
                
                camelContext.addRoutes(xcblOrderRouteBuilder);
            }
            
            // Setup Inbound XCBL Message Acknowledgement Route (if supported)
            String inboundMessageAckUri = getRouteEndpointUri(partner, "xcbl.inbound.messageAck.uri", partnerProperties);
            if (StringUtils.isBlank(inboundMessageAckUri) || !inboundXcblMessageAckPartnersList.contains(partner)) {
            	xcblEntryPointRouteBuilder.setTransactionMessageAckSink(inboundUnsupportedTransactionUri);
            } else {
            	// Setup the XCBL Message Ack Route
            	LOG.info("Adding partner xcbl message ack route: {}", partner);
            	xcblEntryPointRouteBuilder.setTransactionMessageAckSink(inboundMessageAckUri);
            }
            
            camelContext.addRoutes(xcblEntryPointRouteBuilder);
        }
        String outboundPartners = sharedXCBLProperties.getProperty("xcbl.outbound.partners");
        String[] outboundPartnerList = outboundPartners.split(",");
        
        String outboundXcblOrderResponsePartners = sharedXCBLProperties.getProperty("xcbl.outbound.orderResponse.partners");
        List<String> outboundXcblOrderResponsePartnersList = new ArrayList<>();
        if (StringUtils.isNotBlank(outboundXcblOrderResponsePartners)) {
        	outboundXcblOrderResponsePartnersList = Arrays.asList(outboundXcblOrderResponsePartners.split(","));
        }
        for (String partner : outboundPartnerList) {
        	
        	// First load shared XCBL integration properties
            Properties partnerProperties = PropertiesLoader.getAsProperties("camel.xcbl.integration");
            // Then load partner properties, overriding properties loaded from shared XCBL integration properties
            partnerProperties.putAll(PropertiesLoader.getAsProperties(partner + ".integration"));
            
        	// Setup Outbound XCBL OrderResponse Route (if supported)
            // Outbound Order URI
            final String outboundOrderResponseUri = getRouteEndpointUri(partner, 
            		"xcbl.outbound.orderResponse.uri",partnerProperties);
            
            if (StringUtils.isNotBlank(outboundOrderResponseUri) && outboundXcblOrderResponsePartnersList.contains(partner)) {
            
                LOG.info("Adding XCBL Order Response route for partner: {}", partner);
                XCBLOrderResponseRouteBuilder xcblOrderResponseRouteBuilder = new XCBLOrderResponseRouteBuilder();
                
                // Outbound OrderResponse URI
                xcblOrderResponseRouteBuilder.setOutboundOrderResponseSource(outboundOrderResponseUri);
                                                
                // MessageAck DataFormat Class
                final String messageAckDataFormatClass = partnerProperties.getProperty("xcbl.messageAck.dataformat.class");
                xcblOrderResponseRouteBuilder.setInboundMessageAckDataFormatClass(messageAckDataFormatClass);
                
                // Order DataFormat Class
                final String orderDataFormatClass = partnerProperties.getProperty("xcbl.order.dataformat.class");
                xcblOrderResponseRouteBuilder.setInboundOrderDataFormatClass(orderDataFormatClass);
                
//                // OrderResponse Translator Class
//                final String orderTranslatorClass = partnerProperties.getProperty("xcbl.order.translator.class");
//                xcblOrderResponseRouteBuilder.setOutboundOrderResponseTranslatorClass(orderTranslatorClass);
                
                // OrderResponse DataFormat Class
                final String orderResponseDataFormatClass = partnerProperties.getProperty("xcbl.orderResponse.dataformat.class");
                xcblOrderResponseRouteBuilder.setOutboundOrderResponseDataFormatClass(orderResponseDataFormatClass);
                
                final String outboundOrderResponseDlqUri = getRouteEndpointUri(partner,"xcbl.outbound.orderResponse.dlq.uri",partnerProperties);
                xcblOrderResponseRouteBuilder.setOutputDLQ(outboundOrderResponseDlqUri);
                
                final String outboundOrderResponseLogUri = getRouteEndpointUri(partner,"xcbl.outbound.orderResponse.log.uri",partnerProperties);
                xcblOrderResponseRouteBuilder.setOutboundOrderResponseLogSource(outboundOrderResponseLogUri);
                
                final String inboundMessageAckLogUri = getRouteEndpointUri(partner,"xcbl.outbound.orderResponse.messageAck.log.uri",partnerProperties);
                xcblOrderResponseRouteBuilder.setInboundMessageAckLogSource(inboundMessageAckLogUri);
                
                final String outboundOrderResponsePartnerUri = partnerProperties.getProperty("xcbl.outbound.partnerEndpoint.uri");
                xcblOrderResponseRouteBuilder.setOutboundOrderResponseOutput(outboundOrderResponsePartnerUri);
                
                camelContext.addRoutes(xcblOrderResponseRouteBuilder);
            }
        	
        }
        
        final String outboundOrderResponseErrorUri = getNoPartnerRouteEndpointUri("xcbl.outbound.orderResponse.uri",outboundXcblOrderResponsePartnersList,"camel.xcbl.integration");
        
        if (StringUtils.isNotBlank(outboundOrderResponseErrorUri)) {
        	XCBLOrderResponseErrorRouteBuilder xcblOrderResponseErrorRouteBuilder = new XCBLOrderResponseErrorRouteBuilder();

            xcblOrderResponseErrorRouteBuilder.setOutputDLQ(sharedXCBLProperties.getProperty("xcbl.outbound.orderResponse.dlq.uri"));
            xcblOrderResponseErrorRouteBuilder.setOutboundOrderErrorSource(outboundOrderResponseErrorUri);
            
            camelContext.addRoutes(xcblOrderResponseErrorRouteBuilder);
        }
        
        camelContext.setNameStrategy(new ExplicitCamelContextNameStrategy("XCBL"));
        camelContext.setPackageScanClassResolver(new JBossPackageScanClassResolver());
        camelContext.start();
    }

    @SuppressWarnings("static-method")
    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(8);
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(10);
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
