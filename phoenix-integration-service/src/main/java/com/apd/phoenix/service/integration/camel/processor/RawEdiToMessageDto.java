package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;

public class RawEdiToMessageDto implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(RawEdiToMessageDto.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        byte[] body = (byte[]) exchange.getIn().getBody();
        String rawMessage = new String(body);
        LOG.debug("Raw Message: {}", rawMessage);

        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }

        String apdPo = (String) exchange.getIn().getHeader("apdPo");
        LOG.debug("transaction id: {}", apdPo);
        if (apdPo == null) {
            throw new Exception("Transaction id not found");
        }

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(CommunicationType.OUTBOUND);
        metadata.setContentLength(rawMessage.length());
        metadata.setDestination(destination);
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        metadata.setInterchangeId(interchangeId);
        String groupId = (String) exchange.getIn().getHeader("groupId");
        metadata.setGroupId(groupId);
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        metadata.setTransactionId(transactionId);
        String senderId = (String) exchange.getIn().getHeader("senderId");
        metadata.setSenderId(senderId);

        String messageFileName = (String) exchange.getIn().getHeader("messageFileName");
        if (messageFileName == null) {
            throw new Exception("Transaction filename not found");
        }
        metadata.setFilePath(apdPo + "/" + messageFileName);

        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.EDI);

        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(rawMessage);
        exchange.getOut().setBody(message);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

}
