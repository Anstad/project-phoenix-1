package com.apd.phoenix.service.integration.routes;

import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;

public class EDIInboundRouteBuilder extends RouteBuilder {

    private String source;

    private String output;

    private String outputDLQ;

    private String partnerId;

    @Override
    public void configure() throws Exception {
        from(source)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("partnerId", constant(partnerId)).to(
                output);

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

}
