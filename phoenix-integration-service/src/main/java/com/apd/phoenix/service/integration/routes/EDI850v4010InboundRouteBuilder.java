/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.EDIX12to997OutboundTranslator;
import com.apd.phoenix.service.integration.camel.processor.OrderRequestProcessor;
import com.apd.phoenix.service.integration.camel.processor.PurchaseOrderTranslator4010;
import com.apd.phoenix.service.integration.camel.processor.SequenceProcessor;
import com.apd.phoenix.service.integration.exception.EDIValidationException;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.model.dto.OrderLogDto;

/**
 *
 * @author nreidelb
 */
public class EDI850v4010InboundRouteBuilder extends EDIRouteBuilder {

    private Class<?> inboundDataformat;
    private String inboundPurchaseOrderSource;
    private String inboundFormatSource;
    private String inboundLogSource;
    private boolean isLogging = true;

    private Object senderId;
    private String faSource;
    private Object senderCode;
    private Object receiverCode;
    private Object testIndicator;
    private String faPrepare;
    private String faOutput;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 850v4010 inbound processing: ${exception.stacktrace}");

        from(inboundPurchaseOrderSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.INBOUND_850v4010))
                .setHeader("partnerId", constant(senderId)).setHeader("originalMessage").jxpath("in/body")

                //unmarshal to pojo
                .unmarshal(new EDIDataFormat(inboundDataformat))
                //997
                //TODO: Add bean validations to jaeb
                //.to(faSource)
                //Send to processing route
                .to(inboundFormatSource);

        from(inboundLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils
                                .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.INBOUND_850v4010_LOG))
                .split().jxpath("/in/body").setHeader("apdPo").jxpath("/in/body/apdPoNumber").setHeader(
                        "communicationType", simple("INBOUND")).setHeader("messageEventType",
                        simple(OrderLogDto.EventTypeDto.PURCHASE_ORDER_SENT.toString()))
                //Logging
                .choice().when(constant(isLogging)).bean(EDITransactionLogger.class, "logNewPoEDITransaction");

        from(inboundFormatSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.INBOUND_850v4010_FORMAT_SOURCE))
                .setHeader("interchangeId").jxpath("/in/body/envelopeHeader/interchangeControlNumber").setHeader(
                        "groupId").jxpath("/in/body/groupEnvelopeHeader/groupControlNumber")
                //Translate incoming jaeb pojo to dto
                .bean(PurchaseOrderTranslator4010.class, "fromX12EDI")
                //Setup empty order request to log against
                .bean(OrderRequestProcessor.class, "setupOrderRequest")
                //Log incoming request
                .wireTap(inboundLogSource).split().jxpath("/in/body").bean(OrderRequestProcessor.class, "processOrder");

        from(faSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.INBOUND_850v4010_FA_SOURCE))
                //Setup transaction ids
                .bean(SequenceProcessor.class, "generateSequences")
                //
                .setHeader("senderCode", constant(senderCode)).setHeader("receiverCode", constant(receiverCode))
                .setHeader("testIndicator", constant(testIndicator)).process(new EDIX12to997OutboundTranslator())
                .wireTap(faPrepare).choice().when(simple("${header.isValid} != true")).throwException(
                        new EDIValidationException());

        from(faPrepare)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils
                                        .getOffset(EDIOrderUtils.RouteOrderOffsetInboundEDI850v4010.INBOUND_850v4010_FA_PREPARE))
                .setBody(simple("${header.FA}")).to(faOutput);
    }

    /**
     * @return the inboundDataformat
     */
    public Class<?> getInboundDataformat() {
        return inboundDataformat;
    }

    /**
     * @param inboundDataformat the inboundDataformat to set
     */
    public void setInboundDataformat(Class<?> inboundDataformat) {
        this.inboundDataformat = inboundDataformat;
    }

    /**
     * @return the inboundPurchaseOrderSource
     */
    public String getInboundPurchaseOrderSource() {
        return inboundPurchaseOrderSource;
    }

    /**
     * @param inboundPurchaseOrderSource the inboundPurchaseOrderSource to set
     */
    public void setInboundPurchaseOrderSource(String inboundPurchaseOrderSource) {
        this.inboundPurchaseOrderSource = inboundPurchaseOrderSource;
    }

    /**
     * @return the inboundFormatSource
     */
    public String getInboundFormatSource() {
        return inboundFormatSource;
    }

    /**
     * @param inboundFormatSource the inboundFormatSource to set
     */
    public void setInboundFormatSource(String inboundFormatSource) {
        this.inboundFormatSource = inboundFormatSource;
    }

    /**
     * @return the inboundLogSource
     */
    public String getInboundLogSource() {
        return inboundLogSource;
    }

    /**
     * @param inboundLogSource the inboundLogSource to set
     */
    public void setInboundLogSource(String inboundLogSource) {
        this.inboundLogSource = inboundLogSource;
    }

    public boolean isLogging() {
        return isLogging;
    }

    public void setLogging(boolean isLogging) {
        this.isLogging = isLogging;
    }

    /**
     * @return the senderId
     */
    public Object getSenderId() {
        return senderId;
    }

    /**
     * @param senderId the senderId to set
     */
    public void setSenderId(Object senderId) {
        this.senderId = senderId;
    }

    public String getFaSource() {
        return faSource;
    }

    public void setFaSource(String faSource) {
        this.faSource = faSource;
    }

    public Object getSenderCode() {
        return senderCode;
    }

    public void setSenderCode(Object senderCode) {
        this.senderCode = senderCode;
    }

    public Object getReceiverCode() {
        return receiverCode;
    }

    public void setReceiverCode(Object receiverCode) {
        this.receiverCode = receiverCode;
    }

    public Object getTestIndicator() {
        return testIndicator;
    }

    public void setTestIndicator(Object testIndicator) {
        this.testIndicator = testIndicator;
    }

    public String getFaPrepare() {
        return faPrepare;
    }

    public void setFaPrepare(String faPrepare) {
        this.faPrepare = faPrepare;
    }

    public String getFaOutput() {
        return faOutput;
    }

    public void setFaOutput(String faOutput) {
        this.faOutput = faOutput;
    }

}
