package com.apd.phoenix.service.integration.camel.processor.xcbl;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.camel.processor.EDIEntryPointProcessor;

/**
 * This class is used in XCBL processing routes to implement content based
 * routing. It serves as a place holder for logic that inspects the xml payload
 * of the message and sets the function ID based on the XCBL transaction/message
 * type. As per current requirements only inbound Order processing needs to be
 * handled at this layer; so the logic currently hard codes the decoration of the
 * message.
 * 
 * @author RH
 * 
 */
public class XCBLEntryPointProcessor implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(XCBLEntryPointProcessor.class);

    @Override
    public void process(Exchange exchange) throws Exception {

        String functionId = "Order";
        LOG.info("XCBL Function Id: {}", functionId);

        exchange.getOut().setBody(exchange.getIn().getBody());
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("functionId", functionId);
    }
}
