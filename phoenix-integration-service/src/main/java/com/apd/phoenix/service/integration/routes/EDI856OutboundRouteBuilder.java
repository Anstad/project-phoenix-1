/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.ShipmentToEdi856Translator;
import com.apd.phoenix.service.integration.camel.processor.WiretapStringPrepareProcessor;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.EmailNotifierOnFTPDown;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import javax.edi.model.x12.edi856.AdvanceShipmentNotice;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.PGPDataFormat;

/**
 *
 * @author nreidelb
 */
public class EDI856OutboundRouteBuilder extends EDIRouteBuilder {

    private String dataModelSource;
    private String partnerEndpoint;
    private String partnerId;
    private String logDLQ;
    private boolean isEncrypted = false;
    private String keyFileName;
    private String keyUserid;
    private boolean isArmored = true;
    private String outboundShipNotificationLogSource;
    private String ftpEndPointDown;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi 856 outbound processing: ${exception.stacktrace}");

        EDIDataFormat edi856DataFormat = new EDIDataFormat(AdvanceShipmentNotice.class);

        PGPDataFormat pgpSignAndEncrypt = new PGPDataFormat();
        pgpSignAndEncrypt.setKeyFileName(keyFileName);
        pgpSignAndEncrypt.setKeyUserid(keyUserid);
        pgpSignAndEncrypt.setArmored(isArmored);

        from(dataModelSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI856.OUTBOUND_856))
                .errorHandler(deadLetterChannel(ftpEndPointDown))
                //Set po number
                .setHeader("apdPo")
                .jxpath("/in/body/customerOrderDto/apdPoNumber")
                //Translate to jaeb
                .process(new ShipmentToEdi856Translator())
                //Set headers
                //Set Control Numbers
                .setHeader("interchangeId").jxpath("/in/body/envelopeHeader/interchangeControlNumber").setHeader(
                        "groupId").jxpath("/in/body/groupEnvelopeHeader/groupControlNumber").setHeader("transactionId")
                .jxpath("/in/body/body[1]/header/transactionSetHeader/transactionSetControlNumber").log(
                        LoggingLevel.INFO, "Trans id: :" + simple("${header.transactionId}"))
                //Set message destination
                .setHeader("destination", constant(partnerEndpoint))
                //Set file name
                .setHeader(Exchange.FILE_NAME, simple("sn856_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}")).setHeader(
                        "partnerId", constant(partnerId))
                //marshal to transaction
                .marshal(edi856DataFormat).log(LoggingLevel.DEBUG, "non encrypted: ${body}")
                //Encrypt
                .choice().when(constant(isEncrypted)).choice().when(simple("${header.resend}")).setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.SHIPMENT_NOTICE_PRE_ENCRYPTION_RESEND))
                .otherwise().setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.SHIPMENT_NOTICE_PRE_ENCRYPTION)).end().wireTap(
                        getOutboundShipNotificationLogSource())
                /* By default, wiretap doesn't make a copy of the body, so this onPrepare processor will copy the body 
                 * to avoid it potentially showing up encyrpted in the logs
                 */
                .onPrepare(new WiretapStringPrepareProcessor()).marshal(pgpSignAndEncrypt).log(LoggingLevel.DEBUG,
                        "encrypted: ${body}")
                //End choice
                .end()
                //Log Encrypted
                .choice().when(simple("${header.resend}")).setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.ADVANCED_SHIPMENT_NOTICE_RESEND)).otherwise().setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.ADVANCED_SHIPMENT_NOTICE)).end().wireTap(
                        getOutboundShipNotificationLogSource()).wireTap(getBackupPartnerEndpoint()).wireTap(
                        getBackupPartnerEndpoint())
                //To endpoint
                .to(partnerEndpoint);

        from(getOutboundShipNotificationLogSource()).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI856.OUTBOUND_LOG_856))
        //Message event type
                //Communication type
                .setHeader("communicationType", constant(MessageMetadata.CommunicationType.OUTBOUND)).bean(
                        EDITransactionLogger.class, "logEDITransaction");

        from(ftpEndPointDown).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI856.FTP_DOWN_856)).choice()
                .when(header("partnerId").contains("usps")).bean(EmailNotifierOnFTPDown.class,
                        "uspsFTPServerDownEmailNotify").to(logDLQ).stop().otherwise().to(logDLQ);

    }

    /**
     * @return the dataModelSource
     */
    public String getDataModelSource() {
        return dataModelSource;
    }

    /**
     * @param dataModelSource the dataModelSource to set
     */
    public void setDataModelSource(String dataModelSource) {
        this.dataModelSource = dataModelSource;
    }

    /**
     * @return the partnerEndpoint
     */
    public String getPartnerEndpoint() {
        return partnerEndpoint;
    }

    /**
     * @param partnerEndpoint the partnerEndpoint to set
     */
    public void setPartnerEndpoint(String partnerEndpoint) {
        this.partnerEndpoint = partnerEndpoint;
    }

    /**
     * @return the partnerId
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId the partnerId to set
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    /**
     * @return the logDLQ
     */
    public String getLogDLQ() {
        return logDLQ;
    }

    /**
     * @param logDLQ the logDLQ to set
     */
    public void setLogDLQ(String logDLQ) {
        this.logDLQ = logDLQ;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public String getKeyFileName() {
        return keyFileName;
    }

    public void setKeyFileName(String keyFileName) {
        this.keyFileName = keyFileName;
    }

    public String getKeyUserid() {
        return keyUserid;
    }

    public void setKeyUserid(String keyUserid) {
        this.keyUserid = keyUserid;
    }

    public boolean isArmored() {
        return isArmored;
    }

    public void setArmored(boolean isArmored) {
        this.isArmored = isArmored;
    }

    /**
     * @return the outboundShipNotificationLogSource
     */
    public String getOutboundShipNotificationLogSource() {
        return outboundShipNotificationLogSource;
    }

    /**
     * @param outboundShipNotificationLogSource the outboundShipNotificationLogSource to set
     */
    public void setOutboundShipNotificationLogSource(String outboundShipNotificationLogSource) {
        this.outboundShipNotificationLogSource = outboundShipNotificationLogSource;
    }

    public String getFtpEndPointDown() {
        return ftpEndPointDown;
    }

    public void setFtpEndPointDown(String ftpEndPointDown) {
        this.ftpEndPointDown = ftpEndPointDown;
    }
}
