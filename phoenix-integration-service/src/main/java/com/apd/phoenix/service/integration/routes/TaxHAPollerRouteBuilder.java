package com.apd.phoenix.service.integration.routes;

import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;

public class TaxHAPollerRouteBuilder extends RouteBuilder {

    private String createSource;

    private String updateSource;

    private String zipCreateSource;

    private String zipUpdateSource;

    private String militaryZipCreateSource;

    private String militaryZipUpdateSource;

    private String output;

    @Override
    public void configure() throws Exception {
        from(createSource)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action", constant("bulkCreate")).to(
                output);

        from(updateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action",
                constant("update")).to(output);

        from(zipCreateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action",
                constant("zipBulkCreate")).to(output);

        from(zipUpdateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action",
                constant("zipUpdate")).to(output);

        from(militaryZipCreateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action",
                constant("militaryZipBulkCreate")).to(output);

        from(militaryZipUpdateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("action",
                constant("militaryZipUpdate")).to(output);

    }

    public String getCreateSource() {
        return createSource;
    }

    public void setCreateSource(String createSource) {
        this.createSource = createSource;
    }

    public String getUpdateSource() {
        return updateSource;
    }

    public void setUpdateSource(String updateSource) {
        this.updateSource = updateSource;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getZipCreateSource() {
        return zipCreateSource;
    }

    public void setZipCreateSource(String zipCreateSource) {
        this.zipCreateSource = zipCreateSource;
    }

    public String getZipUpdateSource() {
        return zipUpdateSource;
    }

    public void setZipUpdateSource(String zipUpdateSource) {
        this.zipUpdateSource = zipUpdateSource;
    }

    public String getMilitaryZipCreateSource() {
        return militaryZipCreateSource;
    }

    public void setMilitaryZipCreateSource(String militaryZipCreateSource) {
        this.militaryZipCreateSource = militaryZipCreateSource;
    }

    public String getMilitaryZipUpdateSource() {
        return militaryZipUpdateSource;
    }

    public void setMilitaryZipUpdateSource(String militaryZipUpdateSource) {
        this.militaryZipUpdateSource = militaryZipUpdateSource;
    }

}
