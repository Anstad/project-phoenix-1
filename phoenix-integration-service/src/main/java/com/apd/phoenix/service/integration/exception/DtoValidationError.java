package com.apd.phoenix.service.integration.exception;

public class DtoValidationError extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -8062378381876505154L;

    public DtoValidationError(String message) {
        super(message);
    }
}
