package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.integration.camel.processor.WorkflowServiceCaller;

public class DenyOrderRouteBuilder extends RouteBuilder {

    private String denyOrderQueue;

    @Override
    public void configure() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        from(denyOrderQueue).bean(WorkflowServiceCaller.class, "denyOrder");

    }

    public String getDenyOrderQueue() {
        return denyOrderQueue;
    }

    public void setDenyOrderQueue(String denyOrderQueue) {
        this.denyOrderQueue = denyOrderQueue;
    }

}
