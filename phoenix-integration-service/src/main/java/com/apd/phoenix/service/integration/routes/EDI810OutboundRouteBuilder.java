/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import com.apd.phoenix.service.integration.camel.processor.EDITransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.InvoiceToEdi810Translator;
import com.apd.phoenix.service.integration.camel.processor.WiretapStringPrepareProcessor;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.EmailNotifierOnFTPDown;
import com.apd.phoenix.service.model.MessageMetadata.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import javax.edi.model.x12.edi810.Invoice;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.PGPDataFormat;

/**
 *
 * @author nreidelb
 */
public class EDI810OutboundRouteBuilder extends EDIRouteBuilder {

    public static final String RENUMBER_HEADER = "renumber";
    private String dataModelSource;
    private String outboundInvoiceSink;
    private String uspsLogDLQ;
    private String senderId;
    private String partnerId;
    private boolean isEncrypted = false;
    private String keyFileName;
    private String keyUserid;
    private boolean armored = true;
    private String testIndicator;
    private String outboundInvoiceLogSource;
    private String ftpEndPointDown;
    private Boolean renumber = Boolean.FALSE;

    @Override
    public void configure() throws Exception {
        EDIDataFormat ediDataFormat = new EDIDataFormat(Invoice.class);

        PGPDataFormat pgpSignAndEncrypt = new PGPDataFormat();
        pgpSignAndEncrypt.setKeyFileName(keyFileName);
        pgpSignAndEncrypt.setKeyUserid(keyUserid);
        pgpSignAndEncrypt.setArmored(armored);

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "${exception.stacktrace}");

        from(dataModelSource)
                .startupOrder(
                        getStartOrderOffset()
                                + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810.OUTBOUND_810))
                .errorHandler(
                        deadLetterChannel(ftpEndPointDown).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                .setHeader("partnerId", constant(partnerId))
                //Test indicator
                .setHeader("testIndicator", constant(testIndicator))
                //Set output filename
                .setHeader(Exchange.FILE_NAME, simple("inv810_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //Outbound communication
                .setHeader("messageCommunicationType", constant(MessageMetadataDto.CommunicationType.OUTBOUND))
                //Set the partnerId
                //.setHeader("partnerId").jxpath("/in/body/shipment/customerOrderDto/partnerId")
                //Set Message Type
                .setHeader("messageType", constant(MessageMetadataDto.MessageType.EDI))
                //sender code
                .setHeader("senderId", constant(senderId))
                //Set message destination
                .setHeader("destination", constant(outboundInvoiceSink))
                //Whether or not to renumber the elements
                .setHeader(RENUMBER_HEADER, constant(getRenumber().toString()))
                //Set po number
                .setHeader("apdPo")
                .jxpath("/in/body/shipment/customerOrderDto/apdPoNumber")
                //storage filename
                .setHeader("messageFileName", simple("${in.header.CamelFileName}"))
                //Transaction event
                .setHeader("messageEventType", simple(OrderLogDto.EventTypeDto.CUSTOMER_INVOICED.name()))

                .process(new InvoiceToEdi810Translator())
                //Set Transaction Headers                     
                .setHeader("interchangeId").jxpath("/in/body/envelopeHeader/interchangeControlNumber").setHeader(
                        "groupId").jxpath("/in/body/groupEnvelopeHeader/groupControlNumber").setHeader("transactionId")
                .jxpath("/in/body/body[1]/header/transactionSetHeader/transactionSetControlNumber").log(
                        LoggingLevel.INFO, "Trans id: :" + simple("${header.transactionId}"))
                .setHeader("partnerId", constant(partnerId))
                //Marshal to edi        
                .marshal(ediDataFormat)
                .log(LoggingLevel.DEBUG, "non encrypted: ${body}")
                //Encrypt
                .choice().when(constant(isEncrypted)).choice().when(simple("${header.resend}")).setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.INVOICE_PRE_ENCRYPTION_RESEND))
                .otherwise().setHeader("messageEventType", constant(OrderLogDto.EventTypeDto.INVOICE_PRE_ENCRYPTION))
                .end().wireTap(outboundInvoiceLogSource)
                /* By default, wiretap doesn't make a copy of the body, so this onPrepare processor will copy the body 
                 * to avoid it potentially showing up encyrpted in the logs
                 */
                .onPrepare(new WiretapStringPrepareProcessor()).marshal(pgpSignAndEncrypt).log(LoggingLevel.DEBUG,
                        "encrypted: ${body}")
                //End choice
                .endChoice().otherwise()
                //Do nothing
                .endChoice().end()
                //Logging
                .choice().when(simple("${header.resend}")).setHeader("messageEventType",
                        constant(OrderLogDto.EventTypeDto.CUSTOMER_INVOICED_RESEND)).otherwise().setHeader(
                        "messageEventType", constant(OrderLogDto.EventTypeDto.CUSTOMER_INVOICED)).end().wireTap(
                        outboundInvoiceLogSource).wireTap(getBackupPartnerEndpoint()).to(outboundInvoiceSink);

        from(outboundInvoiceLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810.OUTBOUND_LOG_810))
        //Message event type
                //Communication type
                .setHeader("communicationType", constant(CommunicationType.OUTBOUND)).bean(EDITransactionLogger.class,
                        "logEDITransaction");
        from(ftpEndPointDown).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI810.FTP_DOWN_810)).choice()
                .when(header("partnerId").contains("usps")).bean(EmailNotifierOnFTPDown.class,
                        "uspsFTPServerDownEmailNotify").to(uspsLogDLQ).stop().otherwise().to(uspsLogDLQ);

    }

    /**
     * @return the dataModelSource
     */
    public String getDataModelSource() {
        return dataModelSource;
    }

    /**
     * @param dataModelSource the dataModelSource to set
     */
    public void setDataModelSource(String dataModelSource) {
        this.dataModelSource = dataModelSource;
    }

    /**
     * @return the outboundInvoiceSink
     */
    public String getOutboundInvoiceSink() {
        return outboundInvoiceSink;
    }

    /**
     * @param outboundInvoiceSink the outboundInvoiceSink to set
     */
    public void setOutboundInvoiceSink(String outboundInvoiceSink) {
        this.outboundInvoiceSink = outboundInvoiceSink;
    }

    /**
     * @return the uspsLogDLQ
     */
    public String getUspsLogDLQ() {
        return uspsLogDLQ;
    }

    /**
     * @param uspsLogDLQ the uspsLogDLQ to set
     */
    public void setUspsLogDLQ(String uspsLogDLQ) {
        this.uspsLogDLQ = uspsLogDLQ;
    }

    /**
     * @return the senderId
     */
    public String getSenderId() {
        return senderId;
    }

    /**
     * @param senderId the senderId to set
     */
    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * @return the partnerId
     */
    public String getPartnerId() {
        return partnerId;
    }

    /**
     * @param partnerId the partnerId to set
     */
    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public boolean isEncrypted() {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted) {
        this.isEncrypted = isEncrypted;
    }

    public String getKeyFileName() {
        return keyFileName;
    }

    public void setKeyFileName(String keyFileName) {
        this.keyFileName = keyFileName;
    }

    public String getKeyUserid() {
        return keyUserid;
    }

    public void setKeyUserid(String keyUserid) {
        this.keyUserid = keyUserid;
    }

    public boolean isArmored() {
        return armored;
    }

    public void setArmored(boolean armored) {
        this.armored = armored;
    }

    /**
     * @return the testIndicator
     */
    public String getTestIndicator() {
        return testIndicator;
    }

    /**
     * @param testIndicator the testIndicator to set
     */
    public void setTestIndicator(String testIndicator) {
        this.testIndicator = testIndicator;
    }

    /**
     * @return the outboundInvoiceLogSource
     */
    public String getOutboundInvoiceLogSource() {
        return outboundInvoiceLogSource;
    }

    /**
     * @param outboundInvoiceLogSource the outboundInvoiceLogSource to set
     */
    public void setOutboundInvoiceLogSource(String outboundInvoiceLogSource) {
        this.outboundInvoiceLogSource = outboundInvoiceLogSource;
    }

    public String getFtpEndPointDown() {
        return ftpEndPointDown;
    }

    public void setFtpEndPointDown(String ftpEndPointDown) {
        this.ftpEndPointDown = ftpEndPointDown;
    }

    public Boolean getRenumber() {
        return renumber;
    }

    public void setRenumber(Boolean renumber) {
        this.renumber = renumber;
    }
}
