package com.apd.phoenix.service.integration.camel.processor.xcbl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.MessageAcknowledgement;
import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto;

public class XCBLMessageAckTranslator implements
        MessageTranslator<FunctionalAcknowledgementDto, MessageAcknowledgement> {

    private static final Logger LOG = LoggerFactory.getLogger(XCBLMessageAckTranslator.class);

    @Override
    public MessageAcknowledgement fromCanonical(FunctionalAcknowledgementDto functionalAckDto) {
        LOG.info("body: {}", functionalAckDto);
        throw new UnsupportedOperationException();
    }

    @Override
    public FunctionalAcknowledgementDto toCanonical(MessageAcknowledgement xcblMessageAck) {
        LOG.info("body: {}", xcblMessageAck);
        throw new UnsupportedOperationException();
    }
}
