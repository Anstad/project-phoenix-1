/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.integration.routes.EDI810OutboundRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.edi.model.x12.edi810.Invoice;
import javax.edi.model.x12.edi810.segment.Detail;
import javax.edi.model.x12.edi810.segment.Header;
import javax.edi.model.x12.edi810.segment.InvoiceBeginningSegment;
import javax.edi.model.x12.edi810.segment.InvoiceBody;
import javax.edi.model.x12.edi810.segment.InvoiceItemGroup;
import javax.edi.model.x12.edi810.segment.Trailer;
import javax.edi.model.x12.segment.AllowanceChargeOrService;
import javax.edi.model.x12.segment.BaselineItemData;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.TaxInformation;
import javax.edi.model.x12.segment.TermsOfSale;
import javax.edi.model.x12.segment.TotalMonetaryValueSummary;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class InvoiceToEdi810Translator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(InvoiceToEdi810Translator.class);

    //See USPS EDI Implimentation Guide V6_2_-2 page 26
    private static final String SHIPPERS_IDENTIFYING_NUMBER_QUALIFIER = "SI";
    //See USPS EDI Implimentation Guide V6_2_-2 page 27
    private static final String STOCK_NUMBER_QUALIFIER = "SW";
    private static final String CUSTOMER_ORDER_LINE_NUMBER_QUALIFIER = "PL";
    public static final String CHARGE_INDICATOR = "C";
    public static final String FREIGHT_INDICATOR = "D240";
    public static final String EDI_810_DOCUMENT = "810";
    public static final String CONTRACT_NUMBER_QUALIFIER = "CT";
    public static final String ITEM_REQUIRED_DATE_QUALIFIER = "002";
    public static final String MUTUALLY_DEFINED_TAX_CODE = "ZZ";
    public static final String VENDOR_ORDER_NUMBER_QUALIFIER = "1V";
    public static final String ONE_GROUP_PER_ENVELOPE = "1";
    public static final String INVOICE_FUNCTIONAL_ID_CODE = "IN";
    public static final String VENDOR_CATALOG_NUMBER_QUALIFIER = "VC";
    public static final String VENDOR_CATALOG_NUMBER_QUALIFIER_TWO = "IN";
    public static final String TERMS_TYPE_CODE = "01";
    public static final String NOVANT_PARTNER_ID = "novant";
    public static final String USPS_PARTNER_ID = "usps";
    public static final String CENTS_IN_ONE_DOLLAR = "100";

    @Override
    public void process(Exchange exchange) throws Exception {
        final String partnerId = exchange.getIn().getHeader("partnerId", String.class);
        
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        String groupId = (String) exchange.getIn().getHeader("groupId");
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        
        final CustomerInvoiceDto invoiceDto = (CustomerInvoiceDto) exchange.getIn().getBody();
        Properties partnerProperties = PropertiesLoader.getAsProperties(invoiceDto.getPartnerId().toLowerCase());
        final ShipmentDto shipmentDto = invoiceDto.getShipment();
        final PurchaseOrderDto customerOrderDto = shipmentDto.getCustomerOrderDto();
        Invoice invoice = new Invoice();
        //ISA
        final InterchangeEnvelopeHeader interchangeEnvelopeHeader = OutboundEdiUtils.generateDefaultInterchangeEnvelopeHeader(interchangeId);
        interchangeEnvelopeHeader.setInterchangeReceiverID(OutboundEdiUtils.padWithSpaces(partnerProperties.getProperty("applicationRecieversCodeISA"),
                                                                                            OutboundEdiUtils.LENGTH_OF_INTERCHANGE_RECEIVER_ID));
        interchangeEnvelopeHeader.setTestIndicator(partnerProperties.getProperty("testIndicator"));
        invoice.setEnvelopeHeader(interchangeEnvelopeHeader);
        
        //GS
        final GroupEnvelopeHeader groupEnvelopeHeader = OutboundEdiUtils.generateDefaultGroupEnvelopeHeader(groupId);
        groupEnvelopeHeader.setFunctionalIDCode(INVOICE_FUNCTIONAL_ID_CODE);
        groupEnvelopeHeader.setApplicationReceiversCode(partnerProperties.getProperty("applicationRecieversCodeGS"));
        invoice.setGroupEnvelopeHeader(groupEnvelopeHeader);
        
        Collection<InvoiceBody> bodies = new ArrayList<>();
        InvoiceBody body = new InvoiceBody();
        Header header = new Header();
        //BIG
        InvoiceBeginningSegment big = new InvoiceBeginningSegment();
        big.setInvoiceDate(invoiceDto.getCreatedDate());
        big.setInvoiceNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(invoiceDto.getInvoiceNumber(), 16));
        big.setPurchaseOrderDate(customerOrderDto.getOrderDate());
        if(customerOrderDto.retrieveCustomerPoNumber()!=null){
            big.setPurchaseOrderNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(customerOrderDto.retrieveCustomerPoNumber().getValue(), 12));
        }
        big.setTransactionTypeCode(invoiceDto.getBillingTypeCode());
        header.setBeginningSegmentforInvoice(big);
        //ST
        TransactionSetHeader transactionSetHeader = new TransactionSetHeader();
        transactionSetHeader.setTransactionSetIdentifierCode(EDI_810_DOCUMENT);
        String transactionSetControlNumber = transactionId;
        transactionSetControlNumber = OutboundEdiUtils.zeroPad(transactionSetControlNumber, OutboundEdiUtils.INTERCHANGE_CONTROL_NUMBER_LENGTH);
        transactionSetHeader.setTransactionSetControlNumber(transactionSetControlNumber);
        header.setTransactionSetHeader(transactionSetHeader);
        //REF
        final List<ReferenceNumber> referenceNumbers = new ArrayList<>();
        ReferenceNumber shippingNumber = createSimpleInvoiceREF(shipmentDto.getTrackingNumber(), SHIPPERS_IDENTIFYING_NUMBER_QUALIFIER);        
        ReferenceNumber apdPoReferenceNumber = createSimpleInvoiceREF(customerOrderDto.getApdPoNumber(), VENDOR_ORDER_NUMBER_QUALIFIER);        
        if(NOVANT_PARTNER_ID.equals(partnerId)){
            //The identifiers were switched in the sample document for Novant
            apdPoReferenceNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipmentDto.getTrackingNumber(), 16));
            shippingNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(customerOrderDto.getApdPoNumber(), 16));
            if(customerOrderDto.getTerms()!=null && customerOrderDto.getTerms().getDaysUntilDue()!=null){
                ArrayList<TermsOfSale> terms = new ArrayList<>();
                //ITD
                TermsOfSale termsOfSale = new TermsOfSale();
                termsOfSale.setTermsTypeCode(TERMS_TYPE_CODE);            
                //genreate terms due date
                Calendar calendar = Calendar.getInstance();
                if(shipmentDto.getShipTime()!=null){
                    calendar.setTime(shipmentDto.getShipTime());
                }else{
                    calendar.setTime(new Date());
                }
                calendar.add(Calendar.DATE, customerOrderDto.getTerms().getDaysUntilDue());
                termsOfSale.setTermsNetDueDate(calendar.getTime());
                terms.add(termsOfSale);            
                header.setTermsOfSale(terms);
            }
        }
        referenceNumbers.add(apdPoReferenceNumber);        
        referenceNumbers.add(shippingNumber);
        header.setReferenceNumbers(referenceNumbers);
        body.setHeader(header);
        
        Detail detail= new Detail();
        List<InvoiceItemGroup> items = new ArrayList<>();
        Collections.sort(shipmentDto.getLineItemXShipments(), new LineItemXShipmentDto.LineItemXShipmentDtoComparator());
        for(LineItemXShipmentDto itemXShip : shipmentDto.getLineItemXShipments()){
            LineItemDto item = itemXShip.getLineItemDto();
            InvoiceItemGroup ediItem = new InvoiceItemGroup();
            //IT1
            BaselineItemData it1 = new BaselineItemData();
            it1.setAssignedIdentificationNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(itemXShip.getLineItemDto().getCustomerLineNumber(),4));
            it1.setQuantityInvoiced(itemXShip.getQuantity().toString());
            if(! StringUtils.isEmpty(item.getCustomerExpectedUoM())){
                it1.setUnitOfMeasureCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getCustomerExpectedUoM(),2,2));
            } else {
                it1.setUnitOfMeasureCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getUnitOfMeasure().getName(),2,2));
            }
            if(item.getUnitPrice()!=null){
                it1.setUnitPrice(item.getUnitPrice().abs());
            }
            
			switch(partnerId != null ? partnerId.toLowerCase() : partnerId){
                case USPS_PARTNER_ID:
                    it1.setProductServiceIDQualifier1(STOCK_NUMBER_QUALIFIER);
                    String prodServiceId;
                    if(item.getCustomerSku() != null){
                    	prodServiceId = item.getCustomerSku().getValue();
                    } else {
                    	prodServiceId = item.getApdSku();
                    }
					it1.setProductServiceID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(prodServiceId, 30));
                    it1.setProductServiceIDQualifier2(CUSTOMER_ORDER_LINE_NUMBER_QUALIFIER);
                    it1.setProductServiceID2(item.getLineNumber().toString());
                    break;
                case NOVANT_PARTNER_ID:
                	String supplierPartIdFormatted = OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getSupplierPartId(), 30);
                    it1.setProductServiceIDQualifier1(VENDOR_CATALOG_NUMBER_QUALIFIER);
                    it1.setProductServiceID1(supplierPartIdFormatted);
                    it1.setProductServiceIDQualifier2(VENDOR_CATALOG_NUMBER_QUALIFIER_TWO);
                    it1.setProductServiceID2(supplierPartIdFormatted);
                    break;
                default:
                    LOG.error("unknown sender Id"+ partnerId);
                    break;
            }            
            ediItem.setBaselineItemData(it1);
            final List<ReferenceNumber> itemReferenceNumbers = new ArrayList<>();            
            final String contractNumber = customerOrderDto.getShipToAddressDto().getMiscShipToDto().getContractNumber();
            if(!StringUtils.isEmpty(contractNumber)){
                //REF
                final ReferenceNumber referenceNumber = createSimpleInvoiceREF(contractNumber, CONTRACT_NUMBER_QUALIFIER);
                itemReferenceNumbers.add(referenceNumber);
            }           
            ediItem.setReferenceNumbers(itemReferenceNumbers);
            
            if(Boolean.TRUE.toString().equals(exchange.getIn().getHeader(EDI810OutboundRouteBuilder.RENUMBER_HEADER))){
            	
            	List<LineItemDto> orderItems = customerOrderDto.getItems();
            	String renumberedLineNumber= getRenumberedLineNumberForLineItem(item,
						orderItems);
				ediItem.getBaselineItemData().setAssignedIdentificationNumber(renumberedLineNumber);
            }
            
            items.add(ediItem);
        }
        
      //Ensure items are in the correct order for Novant
        Collections.sort(items,getInvoiceItemGroupComparatorByAssignedIdentifier());
        detail.setInvoiceItems(items);
        Trailer trailer = new Trailer();
        //TDS
        TotalMonetaryValueSummary tds = new TotalMonetaryValueSummary();
        tds.setTotalInvoiceAmount(invoiceDto.getAmount().multiply(new BigDecimal(CENTS_IN_ONE_DOLLAR)));
        trailer.setTotalMonetaryValueSummary(tds);
        if(shipmentDto.getTotalTaxPrice()!=null && 
                //Novant does not want a total tax price sent when there is a zero tax
                !(shipmentDto.getTotalTaxPrice().equals(BigDecimal.ZERO) 
                    && NOVANT_PARTNER_ID.equals(partnerId))){
            //TXI
            final TaxInformation taxInformation = new TaxInformation();
            taxInformation.setTaxTypeCode(MUTUALLY_DEFINED_TAX_CODE);
            taxInformation.setMonetaryAmount(shipmentDto.getTotalTaxPrice().toPlainString());
            trailer.setTaxInformation(taxInformation);
        }
        Collection<AllowanceChargeOrService> shippingCharges = new ArrayList<>();
        //SAC
        AllowanceChargeOrService allowanceCharge = new AllowanceChargeOrService();
        allowanceCharge.setAllowanceChargeIndicator(CHARGE_INDICATOR);
        allowanceCharge.setSpecialChargeOrAllowanceCode(FREIGHT_INDICATOR);
        allowanceCharge.setAllowanceOrChargeTotalAmount(shipmentDto.getShipmentPrice());
        trailer.setAllowanceChargeOrServices(shippingCharges);
        //SE
        final TransactionSetTrailer transactionSetTrailer = new TransactionSetTrailer();
        transactionSetTrailer.setTransactionControlNumber(transactionSetHeader.getTransactionSetControlNumber());
        trailer.setTransactionSetTrailer(transactionSetTrailer);        
        body.setTrailer(trailer);
        body.setDetail(detail);
        body.getTrailer().getTransactionSetTrailer().setNumberOfIncludedSegments(Integer.toString(getSegementCount(body)));
        bodies.add(body);
        invoice.setBody(bodies);
        //GE
        final GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
        groupEnvelopeTrailer.setNumberOfTransactionSets(Integer.toString(bodies.size()));
        groupEnvelopeTrailer.setGroupControlNumber(groupEnvelopeHeader.getGroupControlNumber());
        invoice.setGroupEnvelopeTrailer(groupEnvelopeTrailer);
        //IEA
        final InterchangeEnvelopeTrailer interchangeEnvelopeTrailer = new InterchangeEnvelopeTrailer();
        interchangeEnvelopeTrailer.setInterchangeControlNumber(interchangeEnvelopeHeader.getInterchangeControlNumber());
        interchangeEnvelopeTrailer.setNumberOfIncludedGroups(ONE_GROUP_PER_ENVELOPE);
        invoice.setEnvelopeTrailer(interchangeEnvelopeTrailer);
        
        exchange.getIn().setBody(invoice);
    }

    private ReferenceNumber createSimpleInvoiceREF(final String value, String qualifier) {
        ReferenceNumber shippingNumber = new ReferenceNumber();
        shippingNumber.setReferenceIdentificationQualifier(qualifier);
        shippingNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(value, 16));
        return shippingNumber;
    }

    private String getRenumberedLineNumberForLineItem(LineItemDto item, List<LineItemDto> orderItems) {
        Collections.sort(orderItems, getLineItemComparatorByCustomerLineNumber());
        for (Integer i = 0; i < orderItems.size(); i++) {
            LineItemDto orderItem = orderItems.get(i);
            if (orderItem.getCustomerLineNumber() != null
                    && orderItem.getCustomerLineNumber().equals(item.getCustomerLineNumber())
                    || orderItem.getLineNumber() != null && orderItem.getLineNumber().equals(item.getLineNumber())) {
                //Line numbers start at 1, not 0
                return Integer.toString(i + 1);
            }
        }
        LOG.error("Could not find lineItemXShipment Line Item on order, don't know how to renumber.");
        return null;
    }

    private Comparator<InvoiceItemGroup> getInvoiceItemGroupComparatorByAssignedIdentifier() {
        return new Comparator<InvoiceItemGroup>() {

            @Override
            public int compare(InvoiceItemGroup arg0, InvoiceItemGroup arg1) {
                if (arg0.getBaselineItemData() == null
                        || StringUtils.isEmpty(arg0.getBaselineItemData().getAssignedIdentificationNumber())) {
                    return 1;
                }
                if (arg1.getBaselineItemData() == null
                        || StringUtils.isEmpty(arg1.getBaselineItemData().getAssignedIdentificationNumber())) {
                    return 0;
                }
                String assignedIdentificationNumber0 = arg0.getBaselineItemData().getAssignedIdentificationNumber();
                String assignedIdentificationNumber1 = arg1.getBaselineItemData().getAssignedIdentificationNumber();
                try {
                    return new BigInteger(assignedIdentificationNumber0).compareTo(new BigInteger(
                            assignedIdentificationNumber1));
                }
                catch (NumberFormatException e) {
                    LOG.debug("unexpected customer line number format " + assignedIdentificationNumber0 + " or "
                            + assignedIdentificationNumber1);
                }
                return assignedIdentificationNumber0.compareTo(assignedIdentificationNumber1);
            }

        };
    }

    private Comparator<LineItemDto> getLineItemComparatorByCustomerLineNumber() {
        return new Comparator<LineItemDto>() {

            @Override
            public int compare(LineItemDto arg0, LineItemDto arg1) {
                if (arg0.getCustomerLineNumber() == null) {
                    return 1;
                }
                if (arg1.getCustomerLineNumber() == null) {
                    return 0;
                }
                String assignedIdentificationNumber0 = arg0.getCustomerLineNumber();
                String assignedIdentificationNumber1 = arg1.getCustomerLineNumber();
                try {
                    return new BigInteger(assignedIdentificationNumber0).compareTo(new BigInteger(
                            assignedIdentificationNumber1));
                }
                catch (NumberFormatException e) {
                    LOG.debug("unexpected customer line number format " + assignedIdentificationNumber0 + " or "
                            + assignedIdentificationNumber1);
                }
                return assignedIdentificationNumber0.compareTo(assignedIdentificationNumber1);
            }

        };
    }

    public int getSegementCount(InvoiceBody body) {
        //ST,SE,BIG,REF*SI,REF*,TDS
        int count = 6;
        if (body.getDetail().getInvoiceItems() != null) {
            for (InvoiceItemGroup item : body.getDetail().getInvoiceItems()) {
                count++;
                count += item.getReferenceNumbers().size();
            }
        }
        if (body.getTrailer().getTaxInformation() != null) {
            count++;
        }
        if (body.getHeader().getTermsOfSale() != null) {
            count++;
        }
        if (body.getTrailer().getAllowanceChargeOrServices() != null) {
            count += body.getTrailer().getAllowanceChargeOrServices().size();
        }
        return count;
    }
}
