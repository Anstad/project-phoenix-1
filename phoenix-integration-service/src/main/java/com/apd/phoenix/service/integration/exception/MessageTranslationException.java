package com.apd.phoenix.service.integration.exception;

public class MessageTranslationException extends Exception {

    private static final long serialVersionUID = 1L;

    public MessageTranslationException(String errorMessage) {
        super(errorMessage);
    }
}
