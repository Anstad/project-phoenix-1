package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.edi.model.x12.v5010.edi850.PurchaseOrder;
import javax.edi.model.x12.v5010.edi850.segment.PurchaseOrderBeginningSegment;
import javax.edi.model.x12.v5010.edi850.segment.PurchaseOrderBody;
import javax.edi.model.x12.v5010.edi850.segment.PurchaseOrderItemGroup;
import javax.edi.model.x12.v5010.edi850.segment.ReferenceIdentificationGroup;
import javax.edi.model.x12.v5010.edi850.segment.ShippingBillingGroup;
import javax.edi.model.x12.v5010.edi850.segment.TextualData;
import javax.edi.model.x12.v5010.segment.AddressInformation;
import javax.edi.model.x12.v5010.segment.DateTimeReference;
import javax.edi.model.x12.v5010.segment.DestinationQuantity;
import javax.edi.model.x12.v5010.segment.GeographicLocation;
import javax.edi.model.x12.v5010.segment.Name;
import javax.edi.model.x12.v5010.segment.POBaselineItemData;
import javax.edi.model.x12.v5010.segment.PersonContact;
import javax.edi.model.x12.v5010.segment.ReferenceNumber;
import javax.edi.model.x12.v5010.segment.TermsOfSale;
import org.apache.camel.Converter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.EDI810v5010OutboundRouteBuilder;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.OrderTypeDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.SkuDto;
import com.apd.phoenix.service.model.dto.SkuTypeDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.factory.DtoFactory;

@Converter
public class PurchaseOrderTranslator5010 {

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderTranslator5010.class);
    public static final String BILL_TO_CODE = "BT";
    public static final String SHIP_TO_CODE = "ST";
    private static final String GEOCODE_QUALIFIER = "UL";
    public static final String FINAL_LOCATION_CODE = "MQ";
    public static final String WALMART_AMBIGUOUS_QUALIFIER = "IN";
    public static final String APD_SKU_QUALIFIER_EXCEPT_FOR_WALMART_KITS = "VN";
    /**
     * the 12 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Identification Number (GTIN). 
     * Also known as the Universal Product Code (U.P.C.)
     */
    public static final String UNIVERSAL_PRODUCT_CODE_QUALIFIER = "UP";
    public static final String EMPTY_STRING = "";
    public static final String SHIP_TO_DISTRIBUTION_CENTER_CODE = "BY";
    public static final String TELEPHONE_QUALIFIER = "TE";
    public static final String PRIMARY_CONTACT_QUALIFIER = "AJ";
    public static final String DEPARTMENT_QUALIFIER = "BD";
    public static final String BUYER_SIZE_CODE = "IZ";
    public static final String BUYER_COLOR = "BO";
    /**
     * The 13 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Identification Number (GTIN) 
     **/
    public static final String GLOBAL_TRADE_IDENTIFICATION_NUMBER = "EN";
    public static final String UPC_EAN_CASE_CODE = "UA";
    /**
     * the 14 digit EAN.UCC (EAN International.Uniform Code Council) 
     * Global Trade Item Number (GTIN)
     */
    public static final String GLOBAL_TRADE_ITEM_NUMBER = "UK";
    public static final String DO_NOT_DELIVER_AFTER_QUALIFIER = "063";
    public static final String DIVISION_TYPE_CODE = "19";
    public static final String MERCHANDISE_TYPE_CODE = "MR";
    public static final String VENDOR_ORDER_NUMBER_TYPE_CODE = "VN";
    public static final String DEPARTMENT_TYPE_CODE = "DP";
    public static final String BUYER_SIZE_CODE_VALUE = "buyer size code";
    public static final String BUYER_COLOR_VALUE = "buyer color";
    public static final String GLOBAL_TRADE_IDENTIFICATION_NUMBER_VALUE = "global trade identification number";
    public static final String UPCEAN_CASE_CODE_VALUE = "u.p.c/ean case code";
    public static final String GLOBAL_TRADE_ITEM_NUMBER_VALUE = "global trade item number";
    public static final String UNIVERSAL_PRODUCT_CODE_VALUE = "universal product code";
    public static final String ORDER_CONTACT_QUALIFIER = "OC";
    public static final String USPS_QUALIFIER_FOR_APD_SKU = "SW";
    public static final String ONE_PERCENT_DISCOUNT = "1";
    public static final String TWO_PERCENT_DISCOUNT = "2";
    public static final String DUE_IN_FOURTY_FIVE_DAYS = "45";
    public static final String DUE_IN_THIRTY_DAYS = "30";
    public static final String DUE_IN_TWENTY_DAYS = "20";
    public static final String DUE_IN_FIFTEEN_DAYS = "15";
    public static final String DUE_IN_TEN_DAYS = "10";
    public static final String DUE_AT_TIME_OF_PURCHASE = "0";
    public static final String EDI_ORDER_TYPE = "EDI";
    public static final String KITS_REQUSETED_ARRIVAL_DATE_QUALIFIER = "996";
    public static final String SHIP_NO_LATER_QUALIFIER = "038";
    public static final String SHIP_NOT_BEFORE_QUALIFIER = "037";
    private HashMap<String, UnitOfMeasureDto> unitsOfMeasure;

    public List<PurchaseOrderDto> fromX12EDI(
                @org.apache.camel.Header(EDI810v5010OutboundRouteBuilder.PARTNER_ID) String partnerId,
                @org.apache.camel.Body PurchaseOrder po) {
        LOG.info("body: {}", po);
        List<PurchaseOrderDto> dtos = new ArrayList<>();
        for(PurchaseOrderBody purchaseOrderEdi: po.getBody()) {
            PurchaseOrderDto poDto = new PurchaseOrderDto();
            OrderTypeDto orderTypeDto = new OrderTypeDto();
            orderTypeDto.setValue(EDI_ORDER_TYPE);
            poDto.setOrderType(orderTypeDto);
            poDto.setInterchangeId(po.getInterchangeEnvelopeHeader().getInterchangeControlNumber());
            poDto.setGroupId(po.getGroupEnvelopeHeader().getGroupControlNumber());
            poDto.setTransactionId(purchaseOrderEdi.getOrderBodyHeader().getTransactionSetHeader().getTransactionSetControlNumber());
            poDto.setShipToAddressDto(new AddressDto());
            poDto.getShipToAddressDto().setMiscShipToDto(new MiscShipToDto());
            final PurchaseOrderBeginningSegment beginningSegment = purchaseOrderEdi.getOrderBodyHeader().getBeginningSegment();
            poDto.setCustomerPoNumber(beginningSegment.getPurchaseOrderNumber());
            poDto.setOrderDate(beginningSegment.getPurchaseOrderDate());
            poDto.setRequisitionName(beginningSegment.getContractNumber());
            if(purchaseOrderEdi.getOrderBodyHeader().getTermsOfSale()!=null){
                TermsOfSale termsOfSale = purchaseOrderEdi.getOrderBodyHeader().getTermsOfSale();
                if(!StringUtils.isEmpty(termsOfSale.getTermsDiscountPercent())){
                    switch(termsOfSale.getTermsDiscountPercent()){
                        case ONE_PERCENT_DISCOUNT:
                            if(termsOfSale.getTermsDiscountDaysDue()!=null && 30 != termsOfSale.getTermsDiscountDaysDue()){
                                LOG.error("invalid terms of sale 1% and days due of "+Integer.toString(termsOfSale.getTermsDiscountDaysDue()));
                                break;
                            }
                            if(termsOfSale.getTermsNetDays()!=null && !DUE_IN_FOURTY_FIVE_DAYS.equals(termsOfSale.getTermsNetDays())){
                                LOG.error("invalid terms of sale 1% and net days of "+termsOfSale.getTermsNetDays());
                                break;
                            }
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.ONE_PERCENT_THIRTY_NET_FOURTY_FIVE));
                            break;
                        case TWO_PERCENT_DISCOUNT:
                            if(termsOfSale.getTermsDiscountDaysDue()!=null && 30 != termsOfSale.getTermsDiscountDaysDue()){
                                LOG.error("invalid terms of sale 2% and days due of "+Integer.toString(termsOfSale.getTermsDiscountDaysDue()));
                                break;
                            }
                            if(termsOfSale.getTermsNetDays()!=null && !"31".equals(termsOfSale.getTermsNetDays())){
                                LOG.error("invalid terms of sale 2% and net days of "+termsOfSale.getTermsNetDays());
                                break;
                            }
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.TWO_PERCENT_THIRTY_NET_THIRTY_ONE));
                            break;
                    }
                } else{
                    switch(termsOfSale.getTermsNetDays()){
                        case DUE_AT_TIME_OF_PURCHASE:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.TIME_OF_PURCHASE));
                            break;
                        case DUE_IN_TEN_DAYS:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.TEN));
                            break;
                        case DUE_IN_FIFTEEN_DAYS:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.FIFTEEN));
                            break;
                        case DUE_IN_TWENTY_DAYS:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.TWENTY));
                            break;
                        case DUE_IN_THIRTY_DAYS:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.THIRTY));
                            break;
                        case DUE_IN_FOURTY_FIVE_DAYS:
                            poDto.setTerms(DtoFactory.createCredentialTermsDto(Credential.Terms.FOURTY_FIVE));
                            break;                    
                    }
                }
            }
            poDto.getShipToAddressDto().getMiscShipToDto().setFinanceNumber(purchaseOrderEdi.getOrderBodyHeader().getTermsOfSale().getDescription());
            if(purchaseOrderEdi.getOrderBodyHeader().getPersonContacts()!=null){
                for(PersonContact personContact : purchaseOrderEdi.getOrderBodyHeader().getPersonContacts()){
                    processPersonContact(personContact, poDto);
                }
            }
            for(ReferenceNumber referenceNumber :purchaseOrderEdi.getOrderBodyHeader().getReferenceNumbers()){
                processHeaderReferenceNumber(referenceNumber, poDto);
            }
            for(DateTimeReference date:purchaseOrderEdi.getOrderBodyHeader().getDateTimeReference()){
                switch(date.getDateTimeQualifier()){
                    case DO_NOT_DELIVER_AFTER_QUALIFIER:
                    case KITS_REQUSETED_ARRIVAL_DATE_QUALIFIER:
                        poDto.setRequestedDeliveryDate(date.getDate());
                        break;
                    case SHIP_NO_LATER_QUALIFIER:
                        poDto.setShipNoLaterDate(date.getDate());
                        break;
                    case SHIP_NOT_BEFORE_QUALIFIER:
                        poDto.setShipNotBeforeDate(date.getDate());
                        break;
                    default:
                        //Ignore exra date fields sent
                        break;
                    
                    
                }
            }
            poDto.setComments(new ArrayList<String>());
            if(purchaseOrderEdi.getOrderBodyHeader().getReferenceIdentifications()!=null){
                for(ReferenceIdentificationGroup referenceGroup:purchaseOrderEdi.getOrderBodyHeader().getReferenceIdentifications()){
                    for(TextualData instruction:referenceGroup.getTextualDatas()){
                        poDto.getComments().add(instruction.getTextualData2());
                    }
                }
            }
            for(ShippingBillingGroup shipBillGroup:purchaseOrderEdi.getOrderBodyHeader().getShippingBillingGroups()){
                processShippingBillingGroup(shipBillGroup, poDto);
            }
            unitsOfMeasure = new HashMap<>();
            Map<String,PurchaseOrderDto> orderMap = new HashMap<>();
            for(PurchaseOrderItemGroup item:purchaseOrderEdi.getDetail().getPurchaseOrderItems()){
                processItem(item, orderMap, poDto,partnerId);
            }
            dtos.addAll(orderMap.values());
            if(orderMap.values().isEmpty()){
                dtos.add(poDto);
            }
        }
        return dtos;

    }

    private void processLineItemQualifier(LineItemDto lineItem, String prodServiceIDQualifier, String prodServID,
            String partnerId) {
        if (lineItem.getItem() == null) {
            final ItemDto itemDto = new ItemDto();
            lineItem.setItem(itemDto);
        }
        if (prodServiceIDQualifier != null) {
            switch (prodServiceIDQualifier) {
                case WALMART_AMBIGUOUS_QUALIFIER:
                    if (InvoiceToEDI810v5010Translator.KITS_PARTNER_ID.equals(partnerId)) {
                        lineItem.setApdSku(prodServID);
                    }
                    else {
                        lineItem.setExtraneousCustomerSku(prodServID);
                    }
                    break;
                case USPS_QUALIFIER_FOR_APD_SKU:
                case APD_SKU_QUALIFIER_EXCEPT_FOR_WALMART_KITS:
                    //kits uses 'VN' to denote manufacturer part ID
                    if (!InvoiceToEDI810v5010Translator.KITS_PARTNER_ID.equals(partnerId)) {
                        lineItem.setApdSku(prodServID);
                    }
                    break;
                case GLOBAL_TRADE_ITEM_NUMBER:
                    lineItem.setGlobalTradeItemNumber(prodServID);
                    break;
                case UNIVERSAL_PRODUCT_CODE_QUALIFIER:
                    lineItem.setUniversalProductCode(prodServID);
                    break;
                case GLOBAL_TRADE_IDENTIFICATION_NUMBER:
                    lineItem.setGlobalTradeIdentificatonNumber(prodServID);
                    break;
                default:
                    String type = "";
                    switch (prodServiceIDQualifier) {
                        case BUYER_SIZE_CODE:
                            type = BUYER_SIZE_CODE_VALUE;
                            break;
                        case BUYER_COLOR:
                            type = BUYER_COLOR_VALUE;
                            break;
                        case UPC_EAN_CASE_CODE:
                            type = UPCEAN_CASE_CODE_VALUE;
                            break;
                        default:
                            break;
                    }
                    if (!type.equals("")) {
                        lineItem.getItem().getProperties().put(type, prodServID);
                    }
            }
        }

    }

    private void addDestination(String idCode, BigInteger quantity, LineItemDto lineItem,
            Map<String, PurchaseOrderDto> childrenOrders, String unit, PurchaseOrderDto po) {
        if (idCode != null && quantity != null) {
            LineItemDto itemCopy = lineItem.clone();
            setUnitOfMeasure(unit, itemCopy);
            itemCopy.setQuantity(quantity);
            PurchaseOrderDto order = childrenOrders.get(idCode);
            if (order == null) {
                order = new PurchaseOrderDto(po);
                order.setItems(new ArrayList<LineItemDto>());
                childrenOrders.put(idCode, order);
            }
            order.getItems().add(itemCopy);
        }
    }

    private void setUnitOfMeasure(String unit, LineItemDto lineItem) {
        if (unitsOfMeasure.containsKey(unit)) {
            lineItem.setUnitOfMeasure(unitsOfMeasure.get(unit));
        }
        else {
            UnitOfMeasureDto unitOfMeasure = new UnitOfMeasureDto();
            unitOfMeasure.setName(unit);
            lineItem.setUnitOfMeasure(unitOfMeasure);
            unitsOfMeasure.put(unit, unitOfMeasure);
        }
    }

    private void processPersonContact(PersonContact personContact, PurchaseOrderDto poDto) {
        if (TELEPHONE_QUALIFIER.equals(personContact.getCommunicationNumberQualifier())
                && PRIMARY_CONTACT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(personContact.getCommunicationNumber());
        }
        else {
            if (TELEPHONE_QUALIFIER.equals(personContact.getCommunicationNumberQualifier())
                    && StringUtils.isEmpty(poDto.getShipToAddressDto().getMiscShipToDto().getRequesterPhone())) {
                poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
                poDto.getShipToAddressDto().getMiscShipToDto()
                        .setRequesterPhone(personContact.getCommunicationNumber());
            }
        }
        //Walmart Uses divDept instead of department
        if (DEPARTMENT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setDepartment(personContact.getName());
        }
        if (ORDER_CONTACT_QUALIFIER.equals(personContact.getContactFunctionCode())) {
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterName(personContact.getName());
            poDto.getShipToAddressDto().getMiscShipToDto().setRequesterPhone(personContact.getCommunicationNumber());
            //Fax number avaliable personContact.getCommunicationNumber2()
            poDto.getShipToAddressDto().getMiscShipToDto().setOrderEmail(personContact.getCommunicationNumber3());
        }
    }

    private void processHeaderReferenceNumber(ReferenceNumber referenceNumber, PurchaseOrderDto poDto) {
        switch (referenceNumber.getReferenceIdentificationQualifier()) {
            case DIVISION_TYPE_CODE:
                poDto.getShipToAddressDto().getMiscShipToDto()
                        .setDivision(referenceNumber.getReferenceIdentification());
                break;
            //Walmart uses div/dept instead of department
            case DEPARTMENT_TYPE_CODE:
                poDto.getShipToAddressDto().getMiscShipToDto().setDepartment(
                        referenceNumber.getReferenceIdentification());
                break;
            case MERCHANDISE_TYPE_CODE:
                poDto.setMerchandiseTypeCode(referenceNumber.getReferenceIdentification());
                break;
        }
    }

    private void processShippingBillingGroup(ShippingBillingGroup shipBillGroup, PurchaseOrderDto poDto) {
        final String entityIdentifierCode = shipBillGroup.getName().getEntityIdentifierCode();
        if (entityIdentifierCode != null) {

            AddressDto shipToAddress = null;
            GeographicLocation geographicLocation = null;
            AddressInformation addressInformation = null;
            Name name = null;
            switch (entityIdentifierCode) {
                case BILL_TO_CODE:
                    AddressDto billToAddresss = new AddressDto();
                    billToAddresss.setMiscShipToDto(new MiscShipToDto());
                    billToAddresss.getMiscShipToDto().setGlnID(shipBillGroup.getName().getIdentificationCode());
                    poDto.changeBillToAddressDto(billToAddresss);
                    break;
                case SHIP_TO_DISTRIBUTION_CENTER_CODE:
                    shipToAddress = poDto.getShipToAddressDto();
                    name = shipBillGroup.getName();
                    if (name != null) {
                        shipToAddress.setCompanyName(name.getName());
                    }
                    geographicLocation = shipBillGroup.getGeographicLocation();
                    if (geographicLocation != null) {
                        shipToAddress.setCity(geographicLocation.getCityName());
                        shipToAddress.setState(geographicLocation.getStateOrProvinceCode());
                        shipToAddress.setZip(geographicLocation.getPostalCode());
                        shipToAddress.setCountry(geographicLocation.getCountryCode());
                    }
                    if (GEOCODE_QUALIFIER.equals(shipBillGroup.getName().getIdentificationCodeQualifier())) {
                        shipToAddress.getMiscShipToDto().setGlnID(shipBillGroup.getName().getIdentificationCode());
                    }
                    addressInformation = shipBillGroup.getAddressInformation();
                    if (addressInformation != null) {
                        shipToAddress.setLine1(addressInformation.getAddressLine1());
                        shipToAddress.setLine2(addressInformation.getAddressLine2());
                    }
                    poDto.setShipToAddressDto(shipToAddress);
                    break;
                case SHIP_TO_CODE:
                    shipToAddress = poDto.getShipToAddressDto();
                    name = shipBillGroup.getName();
                    if (name != null) {
                        shipToAddress.setCompanyName(name.getName());
                    }
                    geographicLocation = shipBillGroup.getGeographicLocation();
                    if (geographicLocation != null) {
                        shipToAddress.setCity(geographicLocation.getCityName());
                        shipToAddress.setState(geographicLocation.getStateOrProvinceCode());
                        shipToAddress.setZip(geographicLocation.getPostalCode());
                        shipToAddress.setCountry(geographicLocation.getCountryCode());
                    }
                    if (GEOCODE_QUALIFIER.equals(shipBillGroup.getName().getIdentificationCodeQualifier())) {
                        shipToAddress.getMiscShipToDto().setGlnID(shipBillGroup.getName().getIdentificationCode());
                    }
                    addressInformation = shipBillGroup.getAddressInformation();
                    if (addressInformation != null) {
                        shipToAddress.setLine1(addressInformation.getAddressLine1());
                        shipToAddress.setLine2(addressInformation.getAddressLine2());
                    }
                    poDto.setShipToAddressDto(shipToAddress);
                    break;
                default:
                    break;

            }
        }
    }

    private void processItem(PurchaseOrderItemGroup item, Map<String, PurchaseOrderDto> orderMap, PurchaseOrderDto poDto, String partnerId) throws NumberFormatException {
        POBaselineItemData baseLineItemData = item.getBaselineItemData();
        LineItemDto lineItem = new LineItemDto();
        LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
        lineItemStatusDto.setValue(LineItemStatusEnum.CREATED.getValue());
        lineItem.setStatus(lineItemStatusDto);
        //TODO: ensure this is calulated properly
        lineItem.setEstimatedShippingAmount(BigDecimal.ZERO);
        lineItem.setQuantity(baseLineItemData.getQuantity());
        String unit = baseLineItemData.getUnitOfMeasure();
        setUnitOfMeasure( unit, lineItem);
        lineItem.setUnitPrice(baseLineItemData.getUnitPrice());
        processLineItemQualifier(lineItem,baseLineItemData.getProdServiceIDQualifier1(),baseLineItemData.getProdServID1(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual2(), baseLineItemData.getProdServID2(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual3(), baseLineItemData.getProdServID3(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual4(), baseLineItemData.getProdServID4(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual5(), baseLineItemData.getProdServID5(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual6(), baseLineItemData.getProdServID6(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual7(), baseLineItemData.getProdServID7(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual8(), baseLineItemData.getProdServID8(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual9(), baseLineItemData.getProdServID9(), partnerId);
        processLineItemQualifier(lineItem, baseLineItemData.getProdServIDQual10(), baseLineItemData.getProdServID10(), partnerId);
        //SetApdSku on itemDto
        final SkuDto skuDto = new SkuDto();
        final SkuTypeDto skuTypeDto = new SkuTypeDto();
        skuTypeDto.setName("APD");
        skuDto.setType(skuTypeDto);
        skuDto.setValue(lineItem.getApdSku());
        if(lineItem.getItem().getSkus()==null){
            lineItem.getItem().setSkus(new ArrayList<SkuDto>());
        }
        lineItem.getItem().getSkus().add(skuDto);
        lineItem.setLineNumber(Integer.parseInt(baseLineItemData.getAssignedIdentifier()));
        if(item.getItemDescription()!=null){
            lineItem.setDescription(item.getItemDescription().getDescription());
        }
        else {
        	lineItem.setDescription("n/a");
        	lineItem.setShortName("n/a");
        }
        List<String> comments = new ArrayList<>();
        if(item.getTextualData()!=null){
            for(TextualData specialInstructions:item.getTextualData()){
                String comment = EMPTY_STRING;
                if( StringUtils.isNotEmpty(specialInstructions.getTextualData1())){
                    comment = comment + specialInstructions.getTextualData1() + " ";
                }
                if(specialInstructions.getTextualData2()!=null){
                    comment = comment + specialInstructions.getTextualData2();
                }
                if(!comment.equals(EMPTY_STRING)){
                    comments.add(comment);
                }
            }
        }
        lineItem.setCustomerComments(comments);
        
        
        
        final DestinationQuantity destinationQuantity = item.getDestinationQuantity();
        if(destinationQuantity!=null){
            String splitUnit = destinationQuantity.getUnitofMeasureCode();
            addDestination(destinationQuantity.getIdCode1(),destinationQuantity.getQuantity1(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode2(),destinationQuantity.getQuantity2(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode3(),destinationQuantity.getQuantity3(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode4(),destinationQuantity.getQuantity4(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode5(),destinationQuantity.getQuantity5(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode6(),destinationQuantity.getQuantity6(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode7(),destinationQuantity.getQuantity7(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode8(),destinationQuantity.getQuantity8(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode9(),destinationQuantity.getQuantity9(),lineItem,orderMap,splitUnit,poDto);
            addDestination(destinationQuantity.getIdCode10(),destinationQuantity.getQuantity10(),lineItem,orderMap,splitUnit,poDto);
        } else {
            poDto.getItems().add(lineItem);
        }
    }
}
