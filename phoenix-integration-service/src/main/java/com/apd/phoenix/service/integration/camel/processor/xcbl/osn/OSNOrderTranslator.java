package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OSNOrder;
import com.apd.phoenix.service.integration.camel.processor.MessageTranslator;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLOrderTranslator;
import com.apd.phoenix.service.integration.exception.MessageTranslationException;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

@LocalBean
@Stateless
public class OSNOrderTranslator implements MessageTranslator<PurchaseOrderDto, OSNOrder> {

    private static final Logger LOG = LoggerFactory.getLogger(OSNOrderTranslator.class);

    @Inject
    private XCBLOrderTranslator xcblOrderTranslator;

    @Override
    public OSNOrder fromCanonical(PurchaseOrderDto purchaseOrderDto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public PurchaseOrderDto toCanonical(OSNOrder osnOrder) throws MessageTranslationException {
        if (LOG.isDebugEnabled()) {
            LOG.debug("body: {}", osnOrder);
        }
        return xcblOrderTranslator.toCanonical(osnOrder.getOrder());
    }
}