package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.spi.PackageScanClassResolver;
import org.apacheextras.camel.jboss.JBossPackageScanClassResolver;
import com.apd.phoenix.service.integration.routes.MilitaryZipRouteBuilder;
import com.apd.phoenix.service.integration.routes.TaxRateRouteBuilder;
import com.apd.phoenix.service.integration.routes.ZipPlusFourRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class TaxRateBean {

    CamelContext camelContext;

    TaxRateRouteBuilder route;

    ZipPlusFourRouteBuilder zipRoute;

    MilitaryZipRouteBuilder militaryZipRoute;

    @PostConstruct
    public void init() throws Exception {
        Properties commonProperties = PropertiesLoader.getAsProperties("tax.integration");

        camelContext = new DefaultCamelContext();

        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));

        PackageScanClassResolver jbossResolver = new JBossPackageScanClassResolver();

        camelContext.setPackageScanClassResolver(jbossResolver);

        route = new TaxRateRouteBuilder();
        zipRoute = new ZipPlusFourRouteBuilder();
        militaryZipRoute = new MilitaryZipRouteBuilder();
        route.setCreateSource(commonProperties.getProperty("taxDataCreateProcessSource"));
        route.setUpdateSource(commonProperties.getProperty("taxDataUpdateProcessSource"));
        route.setCreateOutput(commonProperties.getProperty("taxRateUpdateEndpoint"));
        route.setDeleteOutput(commonProperties.getProperty("taxRateDeleteEndpoint"));
        camelContext.addRoutes(route);
        //zip+4
        zipRoute.setCreateSource(commonProperties.getProperty("zipPlusFourCreateProcessSource"));
        zipRoute.setUpdateSource(commonProperties.getProperty("zipPlusFourUpdateProcessSource"));
        zipRoute.setCreateOutput(commonProperties.getProperty("zipPlusFourUpdateEndpoint"));
        zipRoute.setDeleteOutput(commonProperties.getProperty("zipPlusFourDeleteEndpoint"));
        camelContext.addRoutes(zipRoute);
        //military zip
        militaryZipRoute.setCreateSource(commonProperties.getProperty("militaryZipCreateProcessSource"));
        militaryZipRoute.setUpdateSource(commonProperties.getProperty("militaryZipUpdateProcessSource"));
        militaryZipRoute.setCreateOutput(commonProperties.getProperty("militaryZipUpdateEndpoint"));
        militaryZipRoute.setDeleteOutput(commonProperties.getProperty("militaryZipDeleteEndpoint"));
        camelContext.addRoutes(militaryZipRoute);
        camelContext.start();
    }

    @PreDestroy
    public void destroy() throws Exception {
        camelContext.stop();
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }
}
