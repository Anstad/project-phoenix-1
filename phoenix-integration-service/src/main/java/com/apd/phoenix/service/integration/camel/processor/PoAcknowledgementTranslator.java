package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.edi.model.x12.edi855.segment.ItemAcknowledgementGroup;
import javax.edi.model.x12.edi855.segment.POAcknowledgementBody;
import javax.edi.model.x12.segment.LineItemAcknowledgement;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import org.apache.camel.Body;
import org.apache.camel.Header;

public class PoAcknowledgementTranslator {

    @Inject
    private CustomerOrderBp customerOrderBp;

    private static final Logger LOG = LoggerFactory.getLogger(PoAcknowledgementTranslator.class);
    public static final String STAPLES_PARTNER_ID = "staples";

    @SuppressWarnings("static-method")
	public POAcknowledgementDto fromX12EDI(@Header("senderId") String partnerId, @Body POAcknowledgementBody body) throws Exception {
		POAcknowledgementDto poAcknowledgementDto = new POAcknowledgementDto();
                final PurchaseOrderDto purchaseOrderDto = new PurchaseOrderDto();
                poAcknowledgementDto.setPurchaseOrderDto(purchaseOrderDto);       
        
		List<LineItemDto> lineItems = new ArrayList<>();
                String apdPo = body.getAckHeader().getBeginningSegment().getPurchaseOrderNumber();
                if(STAPLES_PARTNER_ID.equals(partnerId)){
                    apdPo = body.getAckHeader().getBeginningSegment().getReleaseNumber();
                    purchaseOrderDto.setCustomerPoNumber(body.getAckHeader().getBeginningSegment().getPurchaseOrderNumber());
                }
		Collection<ItemAcknowledgementGroup> items =  body.getDetail().getItemAcknowledgements();
		
		CustomerOrder customerOrder = customerOrderBp.searchByApdPo(apdPo);
		//Iterate over each po
		for(ItemAcknowledgementGroup item : items) {
			Collection<LineItemAcknowledgement> itemAcks = item.getItemAcknowledgements();
			//Iterate over each line item
			for(LineItemAcknowledgement itemAck : itemAcks) {
				String status = itemAck.getLineItemStatus();
                                String parsedStatus = "";
                                switch (status){
                                    case "BP":
                                        parsedStatus = LineItemStatusEnum.PARTIAL_BACKORDERED.getValue();
                                        break;
                                    case "IA":
                                    case "AC":
                                        parsedStatus = LineItemStatusEnum.ACCEPTED.getValue();
                                        break;
                                    case "IR":
                                        parsedStatus = LineItemStatusEnum.REJECTED.getValue();
                                        break;
                                    case "IB":
                                        parsedStatus = LineItemStatusEnum.BACKORDERED.getValue();
                                        break;
                                    case "IQ":
                                        parsedStatus = LineItemStatusEnum.REJECTED.getValue();
                                        break;
                                    default:
                                        parsedStatus = "INVALID PARSED STATUS";
                                        break;
                                }
				BigInteger quantity = itemAck.getQuantity();
				String poErrorCode = itemAck.getReferenceNumber();
				String uom = itemAck.getUnitOfMeasure();
				String buyerPartNumber = itemAck.getProductServiceID1();
				String vendorSku = itemAck.getProductServiceID2();
				LOG.debug("status: "+status+",quantity: "+quantity+",poCode: "+poErrorCode+",uom: "+uom+",apdSku: "+buyerPartNumber+",vendorSku: "+vendorSku);
				LineItemDto lineItem = new LineItemDto();
				lineItem.setBuyerPartNumber(buyerPartNumber);
				lineItem.setSupplierPartId(vendorSku);
				UnitOfMeasureDto unitOfMeasure = new UnitOfMeasureDto();
				unitOfMeasure.setName(uom);
				lineItem.setUnitOfMeasure(unitOfMeasure);				
				LineItemStatusDto lineItemStatusDto = new LineItemStatusDto();
				lineItemStatusDto.setValue(parsedStatus);
				if(poErrorCode != null) {
					lineItemStatusDto.setDescription(getErrorCodeMessage(Integer.parseInt(poErrorCode)));
					lineItem.setCanBeRetried(canErrorBeRetried(Integer.parseInt(poErrorCode)));
				}
				lineItem.setStatus(lineItemStatusDto);
				if(LineItemStatusEnum.REJECTED.getValue().equals(parsedStatus)) {					 
					 if(customerOrder != null) {
						 for (LineItem originalLineItem : customerOrder.getItems()) {
							 if (originalLineItem.matchesItemOnOrder(lineItem)) {								 
								 if(quantity.compareTo(BigInteger.valueOf(originalLineItem.getQuantity().intValue())) < 0) {
									 quantity = BigInteger.valueOf(originalLineItem.getQuantity().intValue()).subtract(itemAck.getQuantity());
								 }
								 
							 }
						 }
					 }
					
				}
				lineItem.setQuantity(quantity);
				lineItems.add(lineItem);
			}
		}
		poAcknowledgementDto.setLineItems(lineItems);
		poAcknowledgementDto.setApdPo(apdPo);
		
//		exchange.getOut().setBody(poAcknowledgementDto);
//		exchange.getOut().setHeaders(exchange.getIn().getHeaders());
		return poAcknowledgementDto;
	}

    public static String getErrorCodeMessage(int value) {
        switch (value) {
            case 0:
                return "REQUEST PROCESSED SUCCESSFULLY";
            case 1:
                return "ORDER UNDER REVIEW";
            case 2:
                return "COD SHIPMENT";
            case 3:
                return "DUPLICATE P.O. INDICATOR NOT Y OR N - PROCESSED AS N";
            case 4:
                return "MERGED WITH PREVIOUS ORDER";
            case 5:
                return "WARNING:EXISTING SHIPPING INFO HAS BEEN REPLACED";
            case 8:
                return "CHECK CHANGE INFORMATION";
            case 9:
                return "FILLED FROM ALTERNATE FACILITY";
            case 10:
                return "NO CONFIRMATION FORMAT CHOSEN. WILL DEFAULT TO A";
            case 11:
                return "ACCEPTED (order number) (Alternate Facility)";
            case 20:
                return "ORDERING ACCOUNT CLOSED - ORDER NOT PROCESSED";
            case 21:
                return "ORDERING CUSTOMER NUMBER NOT FOUND";
            case 22:
                return "INCORRECT PASSWORD FOR ORDERING CUSTOMER";
            case 23:
                return "INVALID WILL CALL / SHIP OUT INDICATOR - SHOULD BE W OR S";
            case 24:
                return "DUPLICATE ORDER FOUND - PLEASE VERIFY P.O";
            case 25:
                return "INVALID FACILITY OVERRIDE";
            case 27:
                return "ORDER PROCESSED - UNABLE TO CHANGE";
            case 28:
                return "INVALID POSTAL CODE OVERRIDE-ORDER NOT PROCESSED";
            case 29:
                return "COULD NOT SHIP COMPLETE";
            case 30:
                return "INVALID SIGNON ID / PASSWORD - TRANSMISSION REJECTED";
            case 31:
                return "INVALID STATUS CHANGE VALUE - SHOULD BE C,R, OR H";
            case 32:
                return "NO LINE ITEMS - ORDER CANCELLED";
            case 33:
                return "INVALID SHIP-FROM REQUEST MISSING A1 REC -ORDER CANCELLED";
            case 34:
                return "INVALID ADDRESS TYPE CODE SHOULD BE FR - ORDER CANCELLED";
            case 35:
                return "INVALID SHIP-FROM ADDRESS INFO - ORDER CANCELLED";
            case 36:
                return "EXCEEDED MAX SHIP-FROM RECS - LINE NOT PROCESSED";
            case 39:
                return "MULTI WRAP NOT REQUESTED";
            case 40:
                return "INCOMPLETE WL OR DS INFO - ORDER NOT PROCESSED";
            case 41:
                return "ZIP CODE REQUIRED FOR DROP-SHIP - ORDER NOT PROCESSED";
            case 42:
                return "EXCEEDED MAXIMUM ORDER TEXT - LINE NOT PROCESSED";
            case 43:
                return "INVALID WL OR DS INDICATOR - SHOULD BE W OR S";
            case 44:
                return "INVALID WRAP/LABEL OR DROP-SHIP REQUEST -ORDER NOT PROCESSED";
            case 45:
                return "INVALID DROP SHIP RECS";
            case 46:
                return "INVALID ZIP CODE OVERRIDE";
            case 47:
                return "INVALID ADOT INDICATOR OVERRIDE";
            case 51:
                return "ORDER QUANTITY NOT NUMERIC";
            case 52:
                return "INCORRECT UNIT - CHECK VALID UNITS";
            case 53:
                return "ITEM NOT FOUND - CORRECTION NOT PROCESSED";
            case 54:
                return "NON-NUMERIC LIST PRICE OR TOLERANCE - LINE NOT PROCESSED";
            case 55:
                return "CANNOT CHANGE WRAP AND LABEL TO DROP SHIP";
            case 56:
                return "OUT OF STOCK";
            case 57:
                return "NOT ENOUGH STOCK - CHECK SHIPPED QUANTITY";
            case 58:
                return "NOT ENOUGH STOCK - CHECK ON-HAND QUANTITY59 - NOT ENOUGH SHELF - CHECK SHIPPED QUANTITY";
            case 60:
                return "CANNOT CHANGE DROP SHIP TO WRAP AND LABEL";
            case 61:
                return "NO SUCH ITEM";
            case 62:
                return "ITEM DISCONTINUED BY MANUFACTURER";
            case 63:
                return "DISCONTINUED";
            case 64:
                return "MANUFACTURER TEMPORARILY OUT OF STOCK";
            case 65:
                return "SOLD OUT FOR BALANCE OF YEAR AT THIS FACILITY";
            case 66:
                return "NEW ITEM - AVAILABLE NEXT CATALOG YEAR";
            case 67:
                return "INVALID ITEM CLASS";
            case 68:
                return "SHIP-SEPARATE ITEM - ORDER THRU CUSTOMER SERVICE";
            case 69:
                return "INPUT SEQUENCE ERROR";
            case 70:
                return "BUY FROM SPECIFIED FILLING FACILITY";
            case 71:
                return "SUGGESTED REPLACEMENT - CHECK CHANGED ITEM NUMBER";
            case 72:
                return "ORDER AS - CHECK CHANGED ITEM NUMBER";
            case 73:
                return "ADOT ORDER - CORRECTION NOT ALLOWED";
            case 74:
                return "SECONDARY ADOT ORDER - UNABLE TO UPDATE";
            case 75:
                return "UNAVAILABLE AT ASSIGNED FACT(S)";
            case 76:
                return "LINE CANCELLED UNABLE TO SHIP VIA SELECTED METHOD";
            case 78:
                return "ITEM NOT ALLOWED ON MULTI WRAP ORDER";
            case 79:
                return "HAZMAT,NO AIR SHIPMENT";
            case 80:
                return "WRAP AND LABEL NUMBER NOT FOUND";
            case 81:
                return "RECORD OUT OF SEQUENCE";
            case 90:
                return "ORDER NOT FOUND";
            case 91:
                return "INVALID CHARACTERS SENT - CHECK AND RETRANSMIT";
            case 92:
                return "AWAITING PROCESSING";
            case 93:
                return "AWAITING RECOVERY PROCESSING";
            case 94:
                return "MAXIMUM ORDER OR LINE TEXT EXCEEDED";
            case 95:
                return "REQUESTED FUNCTION NOT AVAILABLE";
            case 96:
                return "WRONG RECORD TYPE - REST OF BUFFER IGNORE";
            case 97:
                return "DATABASE NOT AVAILABLE - RETRANSMIT AFTER 30 MINUTE";
            default:
                return "No error decription found";
        }
    }

    private static boolean canErrorBeRetried(int value) {
        switch (value) {
            case 53:
            case 61:
            case 62:
            case 63:
            case 71:
                return false;
            default:
                return true;
        }
    }
}
