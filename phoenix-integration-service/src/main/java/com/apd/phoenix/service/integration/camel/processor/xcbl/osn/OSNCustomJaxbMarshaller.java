/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor.xcbl.osn;

import java.io.StringWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.OSNOrderResponse;

/**
 * @author nreidelb
 */
public class OSNCustomJaxbMarshaller {

    private static final Logger LOG = LoggerFactory.getLogger(OSNCustomJaxbMarshaller.class);

    public String marshal(@org.apache.camel.Body OSNOrderResponse osnOrderResponse) throws JAXBException {
        JAXBContext jAXBContext = JAXBContext.newInstance(OSNOrderResponse.class);
        Marshaller marshaller = jAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        try {
            marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new OSNNamespacePrefixMapper());
        }
        catch (PropertyException e) {
            LOG.error("Failed to attatch namespaceMapper " + e.getMessage());
        }
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(osnOrderResponse, stringWriter);

        return stringWriter.toString();

    }
}
