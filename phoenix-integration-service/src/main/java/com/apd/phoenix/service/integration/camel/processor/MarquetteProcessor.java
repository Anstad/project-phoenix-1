package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.cxf.common.util.StringUtils;
import org.restlet.engine.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.InvoiceWrapper;
import com.apd.phoenix.service.model.dto.InvoiceDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;

public class MarquetteProcessor {

    private static final String ATTATCHMENT_PREFIX = "customer-invoice";

    private static final Logger LOG = LoggerFactory.getLogger(MarquetteProcessor.class);

    public String invoicesToDetailsFile(@Body InvoiceWrapper customerInvoiceWrapper) {
        List<InvoiceDto> dtos = customerInvoiceWrapper.getDtos();
        String fileContents = "";
        for (InvoiceDto invoice : dtos) {
            PurchaseOrderDto customerOrderDto = null;
            if (invoice instanceof CustomerInvoiceDto) {
                customerOrderDto = ((CustomerInvoiceDto) invoice).getShipment().getCustomerOrderDto();
            }
            else {
                customerOrderDto = ((CustomerCreditInvoiceDto) invoice).getReturnOrderDto().getOrder();
            }
            fileContents = addColumn(fileContents, customerOrderDto.getAccount().getName());
            fileContents = addColumn(fileContents, customerOrderDto.getAccount().getSolomonCustomerID());
            fileContents = addColumn(fileContents, invoice.getInvoiceNumber());
            fileContents = addColumn(fileContents, invoice.getCreatedDate() == null ? null : DateUtils.format(invoice
                    .getCreatedDate(), "MM/dd/YYYY"));
            fileContents = addColumn(fileContents, customerOrderDto.getCustomerPoNumber());
            if (invoice.getAmount() != null) {
                fileContents = addColumn(fileContents, invoice.getAmount().toPlainString());
            }
            else {
                fileContents = addColumn(fileContents, null);
            }
            fileContents = addColumn(fileContents, customerOrderDto.getTerms() == null ? null : customerOrderDto
                    .getTerms().getLabel());
            fileContents = addColumn(fileContents, ATTATCHMENT_PREFIX + invoice.getInvoiceNumber() + ".pdf");
            fileContents = fileContents + "\r\n";
        }
        return fileContents;
    }

    private String addColumn(String fileContents, String addition) {
        return fileContents + (StringUtils.isEmpty(addition) ? "" : StringEscapeUtils.escapeCsv(addition)) + ",";
    }

    public String invoicesToTransmittalForm(@Header(value = "yesterday") Date yesterday,
            @Header(value = "credit") String creditString, @Body InvoiceWrapper customerInvoiceWrapper) {
        boolean credit = false;
        if (!StringUtils.isEmpty(creditString)) {
            credit = true;
        }
        List<InvoiceDto> dtos = customerInvoiceWrapper.getDtos();
        String fileContents = "INVOICE " + (credit ? "CREDIT " : "") + "TRANSMITTAL FORM,\r\n" + ",\r\n"
                + "ASSIGNED TO MARQUETTE COMMERCIAL FINANCE,\r\n" + ",\r\n" + "DATE:,";
        fileContents = fileContents + DateUtils.format(yesterday, "MM/dd/YYYY") + "\r\n" + ",\r\n";
        fileContents = fileContents + "# OF INVOICE" + (credit ? " CREDITS" : "S") + " : ," + dtos.size() + "\r\n"
                + ",\r\n";
        fileContents = fileContents + "GRAND TOTAL OF INVOICE" + (credit ? " CREDITS" : "S") + ":,\"$"
                + sumInvoices(dtos).abs().toString() + "\"";
        LOG.info("Outbound Transmittal form: " + fileContents);
        return fileContents;
    }

    private BigDecimal sumInvoices(List<InvoiceDto> dtos) {
        BigDecimal sum = BigDecimal.ZERO;
        for (InvoiceDto invoice : dtos) {
            if (invoice.getAmount() != null) {
                sum = sum.add(invoice.getAmount());
            }
        }
        return sum;
    }
}
