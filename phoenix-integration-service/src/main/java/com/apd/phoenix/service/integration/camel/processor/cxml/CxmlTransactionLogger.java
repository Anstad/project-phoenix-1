package com.apd.phoenix.service.integration.camel.processor.cxml;

import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

@Stateless
public class CxmlTransactionLogger {

    @Inject
    MessageService messageService;

    private static final Logger LOG = LoggerFactory.getLogger(CxmlTransactionLogger.class);

    public void logCxmlTransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("interchangeId") String interchangeId, @Header("groupId") String groupId,
            @Header("transactionId") String transactionId, @Body String body) throws NotSupportedException,
            SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
        LOG.debug("apdPo: {}", apdPo);
        LOG.debug("body: {}", body);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(communicationType);
        metadata.setContentLength(body.length());
        if (StringUtils.isEmpty(destination)) {
            destination = "none";
        }
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-cxml-" + System.currentTimeMillis() + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.CXML);
        metadata.setInterchangeId(interchangeId);
        metadata.setGroupId(groupId);
        metadata.setTransactionId(transactionId);
        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(body);

        messageService.receiveOrderMessage(message, apdPo, messageEventType.name());
    }
}
