package com.apd.phoenix.service.integration.manifest.processor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import org.apache.camel.Body;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.ManifestSuccessDto;
import com.apd.phoenix.service.model.dto.ShipmentUpdateDto;

@Stateless
public class JumptrackShipManifestProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(JumptrackShipManifestProcessor.class);

    @Inject
    private EmailService emailService;

    public void processResponse(@Body ManifestSuccessDto manifestSuccessDto) {

		List<Attachment> attachments = new ArrayList<>();
		
		
		
		LOG.info("Sending email for jumptrack upload");
		Properties jumptrackProperties = PropertiesLoader
				.getAsProperties("jumptrack.integration");
		String from = jumptrackProperties.getProperty(
				"jumptrackManifestEmailFrom", "admin@apdmarketplace.com");
		Set<String> recipients = new HashSet<>();
		recipients.add(jumptrackProperties
				.getProperty("jumptrackManifestEmailTo"));
		String cc = jumptrackProperties.getProperty("jumptrackManifestEmailCC");
		Set<String> ccRecipients = new HashSet<>();
		if(StringUtils.isNotEmpty(cc)) {
			ccRecipients.add(cc);
		}
		String subject = jumptrackProperties.getProperty("jumptrack.manifest.email.subject");

		
		
		String emailBody = "<h1>Jumptrack Upload Report</h1>"
				+ "<p>Attempted upload of attached request file on " + new Date() + "<br />"
				+ "The reponse to the request is attached.</p>";
		emailBody += "<h3>Failed:</h3><p>";
		for(String error : manifestSuccessDto.getErrors()) {
			emailBody += "Error: " + error + "<br />";
		}
		emailBody += "</p>";
		emailBody += "<h3>Successful: " + manifestSuccessDto.getSuccess() + "</h3><p>";
		for(ShipmentUpdateDto shipmentUpdateDto : manifestSuccessDto.getShipments()) {
			emailBody += "- " + shipmentUpdateDto.getOperation() + ", " + shipmentUpdateDto.getTrackingNumber() + "<br />";
		}
		emailBody += "</p>";
		MimeMessage message = emailService.prepareRawMessage(from,
				null, null, recipients, ccRecipients, subject, emailBody, attachments);

		emailService.sendEmail(message);

	}
}
