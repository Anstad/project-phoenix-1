package com.apd.phoenix.service.integration.camel.processor;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import com.apd.phoenix.service.integration.exception.DtoValidationError;
import com.apd.phoenix.service.integration.usps.model.UspsCatalogItem;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.CatalogXItemDto;
import com.apd.phoenix.service.model.dto.ItemDto;
import com.apd.phoenix.service.model.dto.ItemImageDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import java.util.Date;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts ADP Catalog to a USPS Catalog.
 * 
 * @author Bryan Saunders <bsaunder@redhat.com>
 * 
 */
public class LineItemsToCSVModelProcessor implements Processor {

    private static final Logger logger = LoggerFactory.getLogger(LineItemsToCSVModelProcessor.class);

    public static final String DOCUMENT_TYPE = "832";
    public static final String RECORD_ID = "01";
    public static final String UNKNOWN = "U";
    public static final String YES = "Y";
    public static final String NO = "N";
    public static final String JWOD = "isJWOD";
    public static final String WBE = "WBE";
    public static final String MBE = "MBE";
    public static final String ACTIVE_STATUS = "A";
    public static final String BLANK = "";
    public static final String LIST_PRICE_PROPERTY = "list price";
    public static final String DELETE_CHANGE_CODE = "D";
    public static final String ADD_CHANGE_CODE = "A";
    public static final String UPDATE_CHANGE_CODE = "C";

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void process(Exchange exchange) throws Exception {
        Properties commonProperties = PropertiesLoader.getAsProperties("usps");
        // Get DTO
        List<CatalogXItemDto> dtoList = (List<CatalogXItemDto>) exchange.getIn().getBody();

        // Build Catalogs
        LinkedList<UspsCatalogItem> catalogItems = new LinkedList<>();

        for (CatalogXItemDto catalogXItem : dtoList) {
            UspsCatalogItem catalogItem = new UspsCatalogItem();

            UnitOfMeasureDto uomDto = catalogXItem.getItem().getUnitOfMeasure();
            ItemDto itemDto = catalogXItem.getItem();

            // Required Fields
            catalogItem.setSupplierId(commonProperties.getProperty("edi832.supplierId"));
            catalogItem.setTransactionSetId(DOCUMENT_TYPE);
            catalogItem.setRecordId(RECORD_ID);
            catalogItem.setItemNo(catalogXItem.getItem().lookupApdSku());
            catalogItem.setDescription(catalogXItem.getItem().getName());
            if (catalogXItem.getItem().lookupUnspcClassification() != null) {
                catalogItem.setUnspscCd(Integer.valueOf(catalogXItem.getItem().lookupUnspcClassification()));
            }
            catalogItem.setItemPrice(catalogXItem.getPrice().doubleValue());
            if (catalogXItem.getItem().getProperties().get(LIST_PRICE_PROPERTY) != null) {
                catalogItem.setListPrice(new Double(catalogXItem.getItem().getProperties().get(LIST_PRICE_PROPERTY)));
            }
            else {
                catalogItem.setListPrice(catalogXItem.getPrice().doubleValue());
            }
            if (uomDto != null) {
                catalogItem.setUom(uomDto.getName());
            }
            catalogItem.setBusinessClass(UNKNOWN);
            catalogItem.setWomenOwned(catalogXItem.getItem().getClassifications().containsKey(WBE) ? YES : UNKNOWN);
            catalogItem.setMinorityBusiness(catalogXItem.getItem().getProperties().containsKey(MBE) ? YES : UNKNOWN);
            catalogItem.setJwodBusiness("true".equals(catalogXItem.getItem().getProperties().get(JWOD)) ? YES : NO);
            catalogItem.setOtherbusiness(NO);
            catalogItem.setContractNo(commonProperties.getProperty("edi832.contractNo"));

            Set<ItemImageDto> images = itemDto.getItemImages();
            if (images != null) {
                for (ItemImageDto image : images) {
                    if (image.getPrimary()!=null&&image.getPrimary()) {
                        catalogItem.setImageFileName(image.getImageUrl());
                    }
                }
            }else{
                catalogItem.setImageFileName("IMAGE NOT FOUND");
            }

            catalogItem.setItemStatus(ACTIVE_STATUS);
            catalogItem.setChangeCd(determineAction(catalogXItem));
            if(catalogXItem.getCatalog().getStartDate() != null){
                catalogItem.setEffectiveDate(catalogXItem.getCatalog().getStartDate());
            }else{
                catalogItem.setEffectiveDate(new Date());
            }

            // Not Required Fields
            /**
             * USPS doesn't have access to APD sku numbers so we will try to
             * use the customer sku that matches their system for the supplier Item number
             */
            catalogItem.setSupplierItemNo(catalogXItem.getItem().lookupCustomerSku() != null ? catalogXItem.getItem()
                    .lookupCustomerSku() : catalogXItem.getItem().lookupApdSku());
            catalogItem.setManufacturerItemNo(catalogXItem.getItem().lookupManufacturerSku());
            catalogItem.setSupplierHierarchy(BLANK);
            catalogItem.setContractLine(BLANK);
            catalogItem.setContractMandatory(NO);
            catalogItem.setClinType(BLANK);
            catalogItem.setCatalogPage(BLANK);
            catalogItem.setSupplierUrl(BLANK);
            catalogItem.setManufacturerName(catalogXItem.getItem().getManufacturerName());
            catalogItem.setManufacturerUrl(BLANK);
            catalogItem.setNotes(catalogXItem.getItem().getDescription());
            catalogItem.setExpirationDate(catalogXItem.getCatalog().getExpirationDate());
            catalogItem.setMinLineQty(1);
            catalogItem.setMaxLineQty(999999999);
            catalogItem.setIssueIncrement(1);
            catalogItem.setGreenProd(NO);
            catalogItem.setEnergyStar(UNKNOWN);
            catalogItem.setBioPreferred(YES);
            catalogItem.setMsdsReq(YES);
            catalogItem.setRecycledContent(UNKNOWN);
            catalogItem.setEpeatRating(UNKNOWN);
            catalogItem.setNemaStd(UNKNOWN);
            catalogItem.setEpaWatersense(UNKNOWN);
            catalogItem.setUsepaPriChemFree(UNKNOWN);
            catalogItem.setRenewableEnergyResource(UNKNOWN);

            catalogItems.add(catalogItem);
        }

        // Set Out Exchange Information
        exchange.getOut().setBody(catalogItems);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());

        // Set the Filename for FTP
        exchange.getOut().setHeader("CamelFileName", "usps/832" + "-" + System.currentTimeMillis() / 1000 + ".csv");

    }

    private String determineAction(CatalogXItemDto catalogXItem) throws Exception {
        //Modified, Deleted, or Added might be null, but this is the same as false
        setNullModifiedBooleansToFalse(catalogXItem);
        if (catalogXItem.getDeleted()) {
            return DELETE_CHANGE_CODE;
            //modification irrelevant if deleted after modification. 
            //items which have been created and then deleted should never get here
        }
        else {
            if (catalogXItem.getAdded()) {
                return ADD_CHANGE_CODE;
                //modification irrelevant if it wasn't in their system before it was modified
            }
            else {
                if (catalogXItem.getModified()) {
                    return UPDATE_CHANGE_CODE;
                }
                else {
                    logger.error("Unmodified catalogXItem in load data script.");
                    throw (new DtoValidationError("Unmodified catalogXItem in load data script."));
                }
            }
        }
    }

    private void setNullModifiedBooleansToFalse(CatalogXItemDto catalogXItem) {
        if (catalogXItem.getAdded() == null) {
            catalogXItem.setAdded(Boolean.FALSE);
        }
        if (catalogXItem.getDeleted() == null) {
            catalogXItem.setDeleted(Boolean.FALSE);
        }
        if (catalogXItem.getModified() == null) {
            catalogXItem.setModified(Boolean.FALSE);
        }
    }

}
