package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import javax.edi.model.x12.edi856.segment.ItemLevelInformationGroup;
import javax.edi.model.x12.edi856.segment.OrderInformationGroup;
import javax.edi.model.x12.edi856.segment.PackageCartonOrItemLevelGroup;
import javax.edi.model.x12.edi856.segment.ShipmentLevelGroup;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.PersonDto;
import com.apd.phoenix.service.model.dto.PhoneNumberDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.ShippingPartnerDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;

public class EDI856toAdvanceShipmentNoticeTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI856toAdvanceShipmentNoticeTranslator.class);
    public static final String SCAC_CODE = "SCACCode";
    public static final String STAPLES_BUYER_SKU_QUALIFIER = "SK";
    public static final String USSCO_BUYER_SKU_QUALIFIER = "IN";
    public static final String USSCO_VENDOR_SKU_QUALIFIER = "VN";
    public static final String STAPLES_VENDOR_SKU_QUALIFIER = "VP";

    @Override
    public void process(Exchange exchange) throws Exception {
        OrderInformationGroup orderInformationGroup = (OrderInformationGroup) exchange.getIn().getBody();
        String apdPo = orderInformationGroup.getPurchaseOrderReference().getPurchaseOrderNumber();
        if(StringUtils.isEmpty(apdPo)){
            apdPo = orderInformationGroup.getPurchaseOrderReference().getAlternativePurchaseOrderPlacement();
        }
        LOG.debug("Apd PO {}", apdPo);
        AdvanceShipmentNoticeDto advanceShipmentNoticeDto = new AdvanceShipmentNoticeDto();
        advanceShipmentNoticeDto.setApdPo(apdPo);
        advanceShipmentNoticeDto.setCustomerPo(orderInformationGroup.getPurchaseOrderReference().getPrf5());
        advanceShipmentNoticeDto.setShipmentDtos(new ArrayList<ShipmentDto>());
        //Alternate Tracking number location (used by Staples)
        String shipmentIdentification = (String) exchange.getIn().getHeader("shipmentIdentification");
        ShipmentLevelGroup levelGroup = (ShipmentLevelGroup) exchange.getIn().getHeader("levelGroup");
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setLineItemXShipments(new ArrayList<LineItemXShipmentDto>());
        ShippingPartnerDto shippingPartnerDto = new ShippingPartnerDto();
        shippingPartnerDto.setScacCode(levelGroup.getRoutingCarrierDetails().getIdentificationCode());
        shipmentDto.setShippingPartnerDto(shippingPartnerDto);
        final AddressDto addressDto = new AddressDto();
        addressDto.setCity(levelGroup.getShipmentFromLocation().getCityName());
        addressDto.setState(levelGroup.getShipmentFromLocation().getStateOrProvinceCode());
        addressDto.setZip(levelGroup.getShipmentFromLocation().getPostalCode());
        if(levelGroup.getPersonContact()!=null){
            final PersonDto contact = new PersonDto();
            contact.setFirstName(levelGroup.getPersonContact().getName());
            final Set<PhoneNumberDto> numbers = new HashSet<>();
            final PhoneNumberDto phoneNumberDto = new PhoneNumberDto();
            phoneNumberDto.setValue(levelGroup.getPersonContact().getCommunicationNumber());
            numbers.add(phoneNumberDto);
            contact.setPhoneNumberDtos(numbers);
            addressDto.setPersonDto(contact);
        }
        shipmentDto.setFromAddress(addressDto);
        //all package cartons exist on a single shipment
        for (PackageCartonOrItemLevelGroup packageCartonGroup : orderInformationGroup.getPackageCartonGroup()) {
            //Staples Orders don't have ItemLevelGroups, so the first "packageCartonGroup" is actualy an item level group
            if(packageCartonGroup.getItemLevelInformationGroup()==null){
                packageCartonGroup.setItemLevelInformationGroup(new ArrayList<ItemLevelInformationGroup>());
            }
            if (packageCartonGroup.getItemIdentification() != null
                    || packageCartonGroup.getShipmentItemDetail() != null
                    || packageCartonGroup.getProductItemDescription() != null) {
                ItemLevelInformationGroup item = new ItemLevelInformationGroup();
                item.setItemIdentification(packageCartonGroup.getItemIdentification());
                item.setItemLevel(packageCartonGroup.getPackageCartonHeader());
                item.setProductItemDescription(packageCartonGroup.getProductItemDescription());
                item.setShipmentItemDetail(packageCartonGroup.getShipmentItemDetail());
                packageCartonGroup.getItemLevelInformationGroup().add(item);
            }
            for (ItemLevelInformationGroup itemLevelInformationGroup : packageCartonGroup
                    .getItemLevelInformationGroup()) {
                LineItemXShipmentDto lineItemXShipmentDto = new LineItemXShipmentDto();
                LineItemDto lineItemDto = new LineItemDto();

                String serviceIDQualifier1 = itemLevelInformationGroup.getItemIdentification()
                        .getProductServiceIDQualifier1();
                String serviceID1 = itemLevelInformationGroup.getItemIdentification().getProductServiceID1();

                setSkuByQualifier(serviceIDQualifier1, serviceID1, lineItemDto);

                String serviceIDQualifier2 = itemLevelInformationGroup.getItemIdentification()
                        .getProductServiceIDQualifier2();
                String serviceID2 = itemLevelInformationGroup.getItemIdentification().getProductServiceID2();

                setSkuByQualifier(serviceIDQualifier2, serviceID2, lineItemDto);

                String serviceIDQualifier3 = itemLevelInformationGroup.getItemIdentification()
                        .getProductServiceIDQualifier3();
                String serviceID3 = itemLevelInformationGroup.getItemIdentification().getProductServiceID3();

                setSkuByQualifier(serviceIDQualifier3, serviceID3, lineItemDto);

                BigInteger quantity = new BigInteger(itemLevelInformationGroup.getShipmentItemDetail()
                        .getNumberOfUnitsShipped());

                lineItemXShipmentDto.setQuantity(quantity);
                UnitOfMeasureDto uom = new UnitOfMeasureDto();
                uom.setName(itemLevelInformationGroup.getShipmentItemDetail().getUnitOfMeasureCode());
                lineItemDto.setUnitOfMeasure(uom);
                String lineNumberString = itemLevelInformationGroup.getItemIdentification().getAssignedIdentificationNumber();
            	if (StringUtils.isNotBlank(lineNumberString)) {
	                try {
	                	lineItemDto.setLineNumber(Integer.parseInt(lineNumberString));
	                } catch (NumberFormatException e) {
	            		LOG.warn("Exception while parsing Line Number: " + lineNumberString);
	                }
            	}
                lineItemXShipmentDto.setLineItemDto(lineItemDto);
                boolean isItemOnShipment = false;
                for (LineItemXShipmentDto itemOnShipment : shipmentDto.getLineItemXShipments()) {
                	if (itemOnShipment != null && itemOnShipment.getLineItemDto() != null && lineItemXShipmentDto != null && itemOnShipment.getLineItemDto().matchesItemOnOrder(lineItemXShipmentDto.getLineItemDto())) {
                		itemOnShipment.setQuantity(itemOnShipment.getQuantity().add(lineItemXShipmentDto.getQuantity()));
                		isItemOnShipment = true;
                		break;
                	}
                }
                if (!isItemOnShipment) {
                	shipmentDto.getLineItemXShipments().add(lineItemXShipmentDto);
                }
            }
        }
        if (orderInformationGroup.getReferenceNumber1() != null) {
            shipmentDto.setTrackingNumber(orderInformationGroup.getReferenceNumber1().getReferenceIdentification());
        }
        /**
         * Prefer the tracking number unique to each order information group, but if
         * this is not present, use the shipment identifier for the whole 856.
         **/
        if(StringUtils.isEmpty(shipmentDto.getTrackingNumber())){
            shipmentDto.setTrackingNumber(shipmentIdentification);
        }
        //AdvanceShipmentNoticeDtos having a set of ShipmentDtos is an artifact of earlier code. The set will only 
        //ever contain one ShipmentDto.
        advanceShipmentNoticeDto.getShipmentDtos().add(shipmentDto);

        exchange.getOut().setBody(advanceShipmentNoticeDto);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
    }

    public void setSkuByQualifier(String qualifier, String serviceId, LineItemDto lineItemDto) {
        LOG.debug("Adding Sku: {} : {}", qualifier, serviceId);
        if (qualifier != null) {
            switch (qualifier) {
                case USSCO_BUYER_SKU_QUALIFIER:
                case STAPLES_BUYER_SKU_QUALIFIER:
                    lineItemDto.setBuyerPartNumber(serviceId);
                    break;
                case USSCO_VENDOR_SKU_QUALIFIER:
                case STAPLES_VENDOR_SKU_QUALIFIER:
                    lineItemDto.setSupplierPartId(serviceId);
                    break;
                case "UP":
                    LOG.debug("UPC number not set on dto: {}", serviceId);
                    break;
            }
        }
    }

}