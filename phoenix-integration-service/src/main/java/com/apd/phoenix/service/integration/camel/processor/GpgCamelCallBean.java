/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.component.exec.ExecBinding;
import org.apache.camel.component.exec.ExecResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class GpgCamelCallBean {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(GpgCamelCallBean.class);
    public static final String ENC_PASSWORD = "encPassword";
    public static final String KEY_USER_ID = "keyUserId";
    public static final String TMP_DIRECTORY = "tmpDirectory";

    public void processGpgResult(Exchange exchange) {
        ExecResult execResult = (ExecResult) exchange.getIn().getBody();
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader(ENC_PASSWORD, "PASSWORD_CLEARED");
        try {
            if (execResult.getStderr().available() > 0 && execResult.getExitValue() != 0) {
                LOG.error(IOUtils.toString(execResult.getStderr(), "UTF-8"));
            }
            exchange.getOut().setBody(exchange.getIn().getBody(byte[].class));
        }
        catch (IOException ex) {
            LOG.error(ex.getStackTrace().toString());
        }
    }

    public void setGpgParameters(Exchange exchange){
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        List<String> gpgParams = new ArrayList<>();
        gpgParams.add("--batch");
        gpgParams.add( "--yes");
        gpgParams.add( "--passphrase");
        gpgParams.add( (String) exchange.getIn().getHeader(ENC_PASSWORD));
        gpgParams.add( "-u");
        gpgParams.add( (String) exchange.getIn().getHeader(KEY_USER_ID));
        gpgParams.add( "--decrypt");
        gpgParams.add( exchange.getIn().getHeader(TMP_DIRECTORY) + "/" + exchange.getIn().getHeader("CamelFileName"));
        exchange.getOut().getHeaders().put(ExecBinding.EXEC_COMMAND_ARGS, gpgParams);
    }

    public void setRmParameters(Exchange exchange){
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        List<String> gpgParams = new ArrayList<>();
        gpgParams.add( (String) exchange.getIn().getHeader("CamelFileName"));
        exchange.getOut().getHeaders().put(ExecBinding.EXEC_COMMAND_ARGS, gpgParams);
    }
}
