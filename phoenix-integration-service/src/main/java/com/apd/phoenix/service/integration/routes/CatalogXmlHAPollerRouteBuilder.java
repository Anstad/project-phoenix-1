package com.apd.phoenix.service.integration.routes;

import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;

public class CatalogXmlHAPollerRouteBuilder extends RouteBuilder {

    private String itemSource;

    private String relationshipSource;

    private String output;

    @Override
    public void configure() throws Exception {
        from(itemSource)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("type", constant("item")).to(output);

        from(relationshipSource).setHeader("type", constant("relationship")).to(output);

    }

    public String getItemSource() {
        return itemSource;
    }

    public void setItemSource(String itemSource) {
        this.itemSource = itemSource;
    }

    public String getRelationshipSource() {
        return relationshipSource;
    }

    public void setRelationshipSource(String relationshipSource) {
        this.relationshipSource = relationshipSource;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

}
