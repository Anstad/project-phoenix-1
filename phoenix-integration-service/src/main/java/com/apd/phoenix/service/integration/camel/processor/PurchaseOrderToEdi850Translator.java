package com.apd.phoenix.service.integration.camel.processor;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import javax.edi.model.x12.edi850.PurchaseOrder;
import javax.edi.model.x12.edi850.segment.Detail;
import javax.edi.model.x12.edi850.segment.Header;
import javax.edi.model.x12.edi850.segment.PurchaseOrderBeginningSegment;
import javax.edi.model.x12.edi850.segment.PurchaseOrderBody;
import javax.edi.model.x12.edi850.segment.PurchaseOrderItemGroup;
import javax.edi.model.x12.edi850.segment.ShippingBillingGroup;
import javax.edi.model.x12.edi850.segment.Trailer;
import javax.edi.model.x12.segment.AdditionalNameInformation;
import javax.edi.model.x12.segment.AddressInformation;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.segment.Name;
import javax.edi.model.x12.segment.POBaselineItemData;
import javax.edi.model.x12.segment.PersonContact;
import javax.edi.model.x12.segment.ProductItemDescription;
import javax.edi.model.x12.segment.ReferenceIdentification;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.RoutingCarrierDetails;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetTotals;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import org.apache.camel.Converter;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.dto.AddressDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.MiscShipToDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import static com.apd.phoenix.service.model.CredentialPropertyType.CredentialPropertyTypeEnum;
import java.util.HashSet;
import java.util.Iterator;

@Converter
public class PurchaseOrderToEdi850Translator {

    public static final String INTERCHANGE_VERSION = "00401";
    public static final String EDI_810_DOCUMENT = "850";
    public static final String PO_REF_NUM_CODE_CONSUMER_PO = "PO";
    public static final String PO_REF_NUM_CODE_WRAPANDLABEL = "ZZ";
    public static final String PO_REF_NUM_CODE_REG_ADOT = "SU";
    public static final String PO_REF_NUM_CODE_DETAIL_ADOT = "SS";
    public static final String PO_REF_NUM_CODE_ROUTING_INFO = "RU";
    public static final String PO_REF_NUM_CODE_CONSOLIDATED_PO = "11";
    public static final String TX_SET_PURPOSE_CODE_ORIGINAL = "00";
    public static final String PO_TYPE_CODE_NEW_ORDER = "NE";
    public static final String PO_TYPE_CODE_WILL_CALL = "OS";
    public static final String PO1_PRODUCT_ID_TYPE_BUYER_PART_NUM = "BP";
    public static final String PO1_PRODUCT_ID_TYPE_BUYER_ITEM_NUM = "IN";
    public static final String PO1_PRODUCT_ID_TYPE_VENDOR_ITEM_NUM = "VN";
    public static final String SHIP_TO_ENTITY_IDENTIFIER = "ST";
    public static final String ASSIGNED_BY_BUYER_QUALIFIER = "92";
    public static final String USSCO_ACCOUNT_NUMBER_PROPERTY_KEY = "US Account #";
    public static final String BILL_TO_IDENTIFICATION_CODE = "BT";
    public static final String INTERCHANGE_QUALIFIER_FROM_SAMPLE = "ZZ";
    public static final String ITEM_DESCRIPTOR = "F";
    public static final String SAMPLE_RECIEVER_ID = "01";
    public static final String SAMPLE_RECIEVIER_QUALIFIER = "01";
    public static final String SHIP_FROM_ENTITY_CODE = "SF";
    public static final String USSCO_NAME = "UNITED STATIONERS";
    public static final String ASSIGNED_BY_SELLER_QUALIFIER = "91";
    public static final String DEALER_INFORMATION = "ORI";
    public static final String LETTERS_AND_NOTES = "L1";
    public static final String LABEL_INSTRUCTIONS = "LAB";

    private static final Logger LOG = LoggerFactory.getLogger(PurchaseOrderToEdi850Translator.class);
    public static final String YEAR_MONTH_DAY_FORMAT = "YYYYMMDD";
    private static final String WRAP_AND_LABEL_QUALIFIER = "ZZ";
    private static final String WRAP_AND_LABEL_INDENTIFIER = "WL";
    private static final String ADOT_QUALIFIER = "SU";
    private static final String ADOT_INDENTIFIER = "Y";
    public static final int MAXIMUM_NUMBER_OF_N9_SEGMENTS = 6;
    public static final int MAXIMUM_CHARACTERS_PER_SEGMENT = 40;
    public static final int MINIMUM_WEIGHT_FOR_USPS_SHIPPING = 13;

    @Converter
	public static PurchaseOrder toPurchaseOrder(
			PurchaseOrderDto purchaseOrderDto) {
    	String vendorName = "";
    	if(purchaseOrderDto.getPartnerId() != null) {
    		vendorName = purchaseOrderDto.getPartnerId().toLowerCase() + ".integration";
    	}
    	
		Properties integrationProperties = PropertiesLoader.getAsProperties(vendorName);

		String interchangeControlNumber = OutboundEdiUtils.zeroPad(purchaseOrderDto
				.getInterchangeId());
		String groupControlNumber = purchaseOrderDto.getGroupId();
		String transactionSetControlNumber = OutboundEdiUtils.zeroPad(purchaseOrderDto
				.getTransactionId());

		PurchaseOrder edi850 = new PurchaseOrder();
		final InterchangeEnvelopeHeader interchangeEnvelopeHeader = new InterchangeEnvelopeHeader();
		interchangeEnvelopeHeader
				.setAuthorizationInformationQualifier(OutboundEdiUtils.NO_AUTHORIZATION_INFORMATION_PRESENT);
		interchangeEnvelopeHeader
				.setAuthorizationInformation(OutboundEdiUtils.TEN_PADDED_SPACES);
		interchangeEnvelopeHeader
				.setSecurityInformationQualifier(OutboundEdiUtils.NO_SECURITY_INFORMATION_PRESENT);
		interchangeEnvelopeHeader.setSecurityInformation(OutboundEdiUtils.TEN_PADDED_SPACES);
		interchangeEnvelopeHeader
				.setSenderInterchangeIDQualifier(INTERCHANGE_QUALIFIER_FROM_SAMPLE);
		interchangeEnvelopeHeader.setInterchangeSenderID(OutboundEdiUtils.APD_PARTNER_ID_PADDED);
		String partnerId = integrationProperties.getProperty("partnerSenderCode");
		// Strip double quotes
		partnerId = partnerId.substring(1, partnerId.length() - 1);
		interchangeEnvelopeHeader
				.setReceiverInterchangeIDQualifier(SAMPLE_RECIEVIER_QUALIFIER);
		interchangeEnvelopeHeader.setInterchangeReceiverID(partnerId);
		interchangeEnvelopeHeader.setInterchangeDate(new Date());
		interchangeEnvelopeHeader.setInterchangeTime(DateUtil.formatDate(
				new Date(), "HHMM"));
		interchangeEnvelopeHeader
				.setInterchangeControlNumber(interchangeControlNumber);
		interchangeEnvelopeHeader.setInterchangeControlID(OutboundEdiUtils.SAMPLE_CONTROL_ID);
		interchangeEnvelopeHeader
				.setInterchangeVersionNumber(INTERCHANGE_VERSION);
		interchangeEnvelopeHeader
				.setiSAAcknowledgmentRequested(OutboundEdiUtils.ACKNOWLEDGEMENT_REQUESTED);
		interchangeEnvelopeHeader.setTestIndicator(integrationProperties
				.getProperty("testIndicator"));// change in production
		interchangeEnvelopeHeader.setSubelementSeparator(OutboundEdiUtils.SUBELEMENT_SEPARATOR);
		edi850.setEnvelopeHeader(interchangeEnvelopeHeader);
		final GroupEnvelopeHeader groupEnvelopeHeader = new GroupEnvelopeHeader();
		groupEnvelopeHeader.setFunctionalIDCode(PO_REF_NUM_CODE_CONSUMER_PO);
		groupEnvelopeHeader.setApplicationSendersCode(OutboundEdiUtils.APD_PARTNER_ID); // TODO
		groupEnvelopeHeader.setApplicationReceiversCode(partnerId.trim()); // change
																			// to
																			// production
																			// value
		groupEnvelopeHeader.setDate(new Date());
		groupEnvelopeHeader.setTime(DateUtil.formatDate(new Date(), "HHMM"));
		groupEnvelopeHeader.setGroupControlNumber(groupControlNumber);// TODO
		groupEnvelopeHeader
				.setResponsibleAgencyCode(OutboundEdiUtils.ACCREDITED_STANDARDS_COMMITTEE_X12);
		groupEnvelopeHeader
				.setVersionReleaseIndustryIdentifierCode(OutboundEdiUtils.X12_PROCEDURES_REVIEW_BOARD_97_USSCO);
		edi850.setGroupEnvelopeHeader(groupEnvelopeHeader);

		Collection<PurchaseOrderBody> bodies = new ArrayList<>();
		PurchaseOrderBody body = new PurchaseOrderBody();
		Header header = new Header();
		PurchaseOrderBeginningSegment beg = new PurchaseOrderBeginningSegment();
		beg.setPurchaseOrderDate(new Date(purchaseOrderDto.getOrderDate()
				.getTime()));
		if (purchaseOrderDto.retrieveApdPoNumber() != null) {
			beg.setPurchaseOrderNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.retrieveApdPoNumber()
					.getValue(), 22));
		}
		beg.setTransactionSetPurposeCode(TX_SET_PURPOSE_CODE_ORIGINAL);
		//Will call override
		if(purchaseOrderDto.isWillCallEdiOverride()) {
			beg.setPurchaseOrderTypeCode(PO_TYPE_CODE_WILL_CALL);
		} else {
			beg.setPurchaseOrderTypeCode(PO_TYPE_CODE_NEW_ORDER);
		}
		header.setBeginningSegment(beg);

		TransactionSetHeader transactionSetHeader = new TransactionSetHeader();
		transactionSetHeader.setTransactionSetIdentifierCode(EDI_810_DOCUMENT);
		transactionSetHeader
				.setTransactionSetControlNumber(transactionSetControlNumber);
		header.setTransactionSetHeader(transactionSetHeader);

		ArrayList<RoutingCarrierDetails> routingCarrierDetails = new ArrayList<>();
		//Route Code
		if(purchaseOrderDto.getCredential() != null && purchaseOrderDto.getCredential().getProperties() != null) {
			// Route Code specified by some customers
			String routeCode = (String) purchaseOrderDto.getCredential()
					.getProperties().get(CredentialPropertyTypeEnum.ROUTE_CODE.getValue());
			if (routeCode != null) {
				RoutingCarrierDetails routingCarrierDetail = createSimpleCarrierRoutingObject(routeCode);
				routingCarrierDetails.add(routingCarrierDetail);
			}
			
		}
		//Carrier Routing
		String carrierRouting = purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getCarrierRouting();
		if(StringUtils.isNotEmpty(carrierRouting)) {			
			RoutingCarrierDetails routingCarrierDetail = createSimpleCarrierRoutingObject(carrierRouting);
			routingCarrierDetails.add(routingCarrierDetail);
		}
		header.setRoutingCarrierDetails(routingCarrierDetails);

		Collection<ReferenceNumber> referenceNumbers = new ArrayList<>();
		String customerPoInfo = null;
		if(purchaseOrderDto.getCredential().getProperties() != null){
			customerPoInfo = (String) purchaseOrderDto.getCredential().getProperties().get("Consumer PO info");
		}
		if (purchaseOrderDto.retrieveCustomerPoNumber() != null
				&& StringUtils.isNotBlank(purchaseOrderDto .retrieveCustomerPoNumber().getValue())) {
			String referenceIdentification;
            if(purchaseOrderDto.retrieveBlanketPoNumber() != null && 
                                !StringUtils.isEmpty(purchaseOrderDto.retrieveBlanketPoNumber().getValue())){
            	 referenceIdentification = purchaseOrderDto.retrieveBlanketPoNumber().getValue();
            } else {
                referenceIdentification = purchaseOrderDto.retrieveCustomerPoNumber().getValue();
            }
			referenceNumbers = createSimpleReferenceNumber(referenceNumbers, PO_REF_NUM_CODE_CONSUMER_PO, referenceIdentification, customerPoInfo);
		}
		else {
			referenceNumbers = createSimpleReferenceNumber(referenceNumbers, PO_REF_NUM_CODE_CONSUMER_PO, purchaseOrderDto.retrieveApdPoNumber().getValue(), customerPoInfo);
		}
		//Wrap and Label
		if(purchaseOrderDto.isWrapAndLabel()) {
			referenceNumbers = createSimpleReferenceNumber(referenceNumbers, WRAP_AND_LABEL_QUALIFIER, WRAP_AND_LABEL_INDENTIFIER, null);
		}
		if(purchaseOrderDto.isAdot()) {
			referenceNumbers = createSimpleReferenceNumber(referenceNumbers, ADOT_QUALIFIER, ADOT_INDENTIFIER, null);
		}
		header.setReferenceNumbers(referenceNumbers);		

		ArrayList<ShippingBillingGroup> shippingBillingGroups = new ArrayList<>();
		String shipFromCode = purchaseOrderDto.getShipFrom();
		if (shipFromCode != null) {
			ShippingBillingGroup shipFromGroup = new ShippingBillingGroup();
			Name shipFrom = new Name();
			shipFrom.setEntityIdentifierCode(SHIP_FROM_ENTITY_CODE);
			shipFrom.setName(USSCO_NAME);
			shipFrom.setIdentificationCodeQualifier(ASSIGNED_BY_SELLER_QUALIFIER);
			shipFrom.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipFromCode, 2, 17));
			shipFromGroup.setName(shipFrom);
			shippingBillingGroups.add(shipFromGroup);
		}
		ShippingBillingGroup shipTo = new ShippingBillingGroup();
		Name shipToCompany = new Name();
		shipToCompany.setEntityIdentifierCode(SHIP_TO_ENTITY_IDENTIFIER);
		if(StringUtils.isNotEmpty(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getAddressId())) {
			shipToCompany.setIdentificationCodeQualifier(ASSIGNED_BY_BUYER_QUALIFIER);
			shipToCompany.setIdentificationCode( OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getAddressId(), 2, 17) );
		}

		String companyName = purchaseOrderDto.getShipToAddressDto()
				.getCompanyName();                
	    if(StringUtils.isEmpty(companyName)){
	        //If company name not specified for address, use the company name from the account
	        if(purchaseOrderDto.getAccount() != null && purchaseOrderDto.getAccount().getName() != null){
	            companyName = purchaseOrderDto.getAccount().getName();
	        }
	    }          
	    shipToCompany.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(companyName, 35));
		shipTo.setName(shipToCompany);

		AddressDto shipToAddress = purchaseOrderDto.getShipToAddressDto();
		
        Collection<ReferenceIdentification> referenceIdentifications = new ArrayList<>();
        if (purchaseOrderDto.getRequestedDeliveryDate() != null){
    		SimpleDateFormat dateFormat = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT);
            referenceIdentifications = createSimpleTruncatedReferenceIdentification(referenceIdentifications, LETTERS_AND_NOTES, "SPH", 
            		dateFormat.format(purchaseOrderDto.getRequestedDeliveryDate()));
        }                
        //N9 Misc Shipto
        //sends blanket PO if avaliable
        if(purchaseOrderDto.retrieveBlanketPoNumber()!=null && 
                !StringUtils.isEmpty(purchaseOrderDto.retrieveBlanketPoNumber().getValue())){
            referenceIdentifications = createSimpleTruncatedReferenceIdentification(referenceIdentifications, 
            		LETTERS_AND_NOTES, DEALER_INFORMATION, purchaseOrderDto.retrieveBlanketPoNumber().getValue());
        } else{
            if(shipToAddress.getMiscShipToDto() != null) {
                referenceIdentifications = getReferenceIdentifications(shipToAddress.getMiscShipToDto(), referenceIdentifications);
            }
		}               
        header.setReferenceIdentifications(referenceIdentifications);
		
		AddressInformation addressInformation = generateAddressInformation(shipToAddress);
		if (shipToAddress.getMiscShipToDto() != null
				&& shipToAddress.getMiscShipToDto().getContactName() != null) {
			ArrayList<PersonContact> contacts = new ArrayList<>();
			PersonContact contact = new PersonContact();
			if(StringUtils.isNotEmpty(shipToAddress.getMiscShipToDto().getContactName())) {
				contact.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress.getMiscShipToDto().getContactName(), 40));
			}
			contacts.add(contact);
			shipTo.setPersonContacts(contacts);
		}
		shipTo.setAddressInformation(addressInformation);
		GeographicLocation geographicLocation = generateGeographicLocation(shipToAddress);
		shipTo.setGeographicLocation(geographicLocation);
		
		if(shipToAddress.getName() != null) {
			AdditionalNameInformation stAdditionalNameInformation = new AdditionalNameInformation();
			stAdditionalNameInformation.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress.getName(), 35));
			shipTo.setAdditionalNameInformation(stAdditionalNameInformation);
		}
		shippingBillingGroups.add(shipTo);

		final ShippingBillingGroup billTo = new ShippingBillingGroup();
		Name billToCompany = new Name();
		billToCompany.setEntityIdentifierCode(BILL_TO_IDENTIFICATION_CODE);
		billToCompany.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(companyName, 35));		
		billToCompany
				.setIdentificationCodeQualifier(ASSIGNED_BY_BUYER_QUALIFIER);
		
		billToCompany.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto
				.getPartnerAccountId(), 2, 17));
		billTo.setName(billToCompany);
		final AddressDto billToAddress = purchaseOrderDto.lookupBillToAddressDto();
		if(billToAddress!=null){
                    if(billToAddress.getLine1() != null || billToAddress.getLine2() != null) {
                            AddressInformation billAddressInformation = generateAddressInformation(billToAddress);
                            billTo.setAddressInformation(billAddressInformation);
                    }
                    if(billToAddress.getZip() != null || billToAddress.getCountry() != null || billToAddress.getCity() != null || billToAddress.getState() != null) {
                            GeographicLocation billGeographicLocation = generateGeographicLocation(billToAddress);
                            billTo.setGeographicLocation(billGeographicLocation);
                    }

                    if (purchaseOrderDto.lookupBillToAddressDto().getName() != null) {
                            AdditionalNameInformation btAdditionalNameInformation = new AdditionalNameInformation();
                            String btName = purchaseOrderDto.lookupBillToAddressDto().getName();
                            btAdditionalNameInformation.setName(btName);
                            billTo.setAdditionalNameInformation(btAdditionalNameInformation);
                    }
                }
		shippingBillingGroups.add(billTo);
		header.setShippingBillingGroups(shippingBillingGroups);

		Detail poDetail = new Detail();
		Collection<PurchaseOrderItemGroup> items = new ArrayList<PurchaseOrderItemGroup>();
		BigInteger orderTotalQuantity = BigInteger.ZERO;
		if (purchaseOrderDto.getItems() != null) {
			for (LineItemDto lineItem : purchaseOrderDto.getItems()) {
				PurchaseOrderItemGroup poItem = new PurchaseOrderItemGroup();
				POBaselineItemData itemData = new POBaselineItemData();
				if (lineItem.getLineNumber() != null) {
					itemData.setAssignedIdentifier(OutboundEdiUtils.truncateAndRemoveDisallowedChars(lineItem.getLineNumber()
							.toString(), 20));
				}
				if (lineItem.getUnitOfMeasure() != null) {
					String uomName = lineItem.getUnitOfMeasure()
							.getName();
					uomName = OutboundEdiUtils.truncateAndRemoveDisallowedChars(uomName, 2, 2);
					itemData.setUnitOfMeasure(uomName);
					itemData.setBasisUnitPriceCode(uomName);
				}
				// An 850 needs to have the lineItem cost, not price. Or else
				// APD wont make any money!
				itemData.setUnitPrice(lineItem.getCost());
				itemData.setQuantity(lineItem.getQuantity());
				itemData.setProdServiceIDQualifier1(PO1_PRODUCT_ID_TYPE_BUYER_PART_NUM);
				itemData.setProdServID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(lineItem.getBuyerPartNumber(), 48));
				itemData.setProdServIDQual2(PO1_PRODUCT_ID_TYPE_VENDOR_ITEM_NUM);
				itemData.setProdServID2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(lineItem.getSupplierPartId(), 48));
				ProductItemDescription productItemDescription = new ProductItemDescription();
				productItemDescription.setItemDescriptionType(ITEM_DESCRIPTOR);
				String itemDescription = lineItem.getShortName();
				productItemDescription.setDescription(OutboundEdiUtils.truncateAndRemoveDisallowedChars(itemDescription, 70));
				poItem.setItemDescription(productItemDescription);
				poItem.setBaselineItemData(itemData);
				items.add(poItem);
				orderTotalQuantity = orderTotalQuantity.add(lineItem
						.getQuantity());
			}
		}
		poDetail.setPurchaseOrderItems(items);

		Trailer trailer = new Trailer();
		TransactionSetTotals txSetTotals = new TransactionSetTotals();
		if (purchaseOrderDto.getItems() != null) {
			txSetTotals
					.setNumberofLineItems(purchaseOrderDto.getItems().size());
		} else {
			txSetTotals.setNumberofLineItems(0);
		}
		txSetTotals.setHashTotal(orderTotalQuantity.toString());
		trailer.setTransactionSetTotals(txSetTotals);
		TransactionSetTrailer txSetTrailer = new TransactionSetTrailer();

		body.setHeader(header);
		body.setDetail(poDetail);

		txSetTrailer.setNumberOfIncludedSegments(Integer
				.toString(getSegmentCount(body)));
		txSetTrailer.setTransactionControlNumber(transactionSetControlNumber);
		trailer.setTransactionSetTrailer(txSetTrailer);

		body.setTrailer(trailer);

		bodies.add(body);

		GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
		groupEnvelopeTrailer.setNumberOfTransactionSets(bodies.size() + "");
		groupEnvelopeTrailer.setGroupControlNumber(groupControlNumber);

		InterchangeEnvelopeTrailer itrailer = new InterchangeEnvelopeTrailer();
		itrailer.setNumberOfIncludedGroups("1");
		itrailer.setInterchangeControlNumber(interchangeControlNumber);

		edi850.setEnvelopeHeader(interchangeEnvelopeHeader);
		edi850.setGroupEnvelopeHeader(groupEnvelopeHeader);
		edi850.setPurchaseOrderBody(bodies);
		edi850.setGroupEnvelopeTrailer(groupEnvelopeTrailer);
		edi850.setEnvelopeTrailer(itrailer);

		return edi850;
	}

    private static Collection<ReferenceIdentification> createSimpleTruncatedReferenceIdentification(
            Collection<ReferenceIdentification> referenceIdentifications, String qualifier, String identification,
            String freeForm) {
        final ReferenceIdentification referenceIdentification = new ReferenceIdentification();
        referenceIdentification.setReferenceIdentificationQualifier(qualifier);
        referenceIdentification.setReferenceIdentification(identification);
        referenceIdentification.setFreeFormMessage(OutboundEdiUtils.truncateAndRemoveDisallowedChars(freeForm, 40));
        referenceIdentifications.add(referenceIdentification);
        return referenceIdentifications;
    }

    private static Collection<ReferenceNumber> createSimpleReferenceNumber(
            Collection<ReferenceNumber> referenceNumbers, String qualifier, String identification, String customerPoInfo) {
        ReferenceNumber ref = new ReferenceNumber();
        ref.setReferenceIdentificationQualifier(qualifier);
        ref.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(identification, 35));
        if (customerPoInfo != null) {
            OutboundEdiUtils.truncateAndRemoveDisallowedChars(customerPoInfo, 2);
        }
        referenceNumbers.add(ref);
        return referenceNumbers;
    }

    private static RoutingCarrierDetails createSimpleCarrierRoutingObject(String routeCode) {
        RoutingCarrierDetails routingCarrierDetail = new RoutingCarrierDetails();
        routingCarrierDetail.setRouting(OutboundEdiUtils.truncateAndRemoveDisallowedChars(routeCode, 35));
        return routingCarrierDetail;
    }

    private static int getSegmentCount(PurchaseOrderBody body) {
        int count = 4;
        count += body.getDetail().getPurchaseOrderItems().size() * 2;

        if (body.getHeader().getReferenceIdentifications() != null) {
            count += body.getHeader().getReferenceIdentifications().size();
        }
        if (body.getHeader().getRoutingCarrierDetails() != null) {
            count += body.getHeader().getRoutingCarrierDetails().size();
        }
        if (body.getHeader().getReferenceNumbers() != null) {
            count += body.getHeader().getReferenceNumbers().size();
        }
        for (ShippingBillingGroup sbg : body.getHeader().getShippingBillingGroups()) {
            if (sbg.getName() != null) {
                count += 1;
            }
            if (sbg.getAddressInformation() != null) {
                count += 1;
            }
            if (sbg.getGeographicLocation() != null) {
                count += 1;
            }
            if (sbg.getAdditionalNameInformation() != null) {
                count += 1;
            }
            if (sbg.getPersonContacts() != null) {
                count += sbg.getPersonContacts().size();
            }
        }
        return count;
    }

    private static GeographicLocation generateGeographicLocation(final AddressDto shipToAddress) {
        GeographicLocation geographicLocation = new GeographicLocation();
        geographicLocation.setCityName(OutboundEdiUtils
                .truncateAndRemoveDisallowedChars(shipToAddress.getCity(), 2, 25));
        if (shipToAddress.getCountry() != null) {
            geographicLocation.setCountryCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress
                    .getCountry(), 2, 3));
        }
        if (shipToAddress.getZip() != null) {
            geographicLocation.setPostalCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress.getZip()
                    .replace("-", ""), 3, 10));
        }
        else {
            geographicLocation.setPostalCode(shipToAddress.getZip());
        }
        geographicLocation.setStateOrProvinceCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress
                .getState(), 2, 2));
        return geographicLocation;
    }

    private static AddressInformation generateAddressInformation(final AddressDto shipToAddress) {
        AddressInformation addressInformation = new AddressInformation();
        addressInformation.setAddressLine1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress.getLine1(),
                35));
        addressInformation.setAddressLine2(OutboundEdiUtils.truncateAndRemoveDisallowedChars(shipToAddress.getLine2(),
                35));
        return addressInformation;
    }

    @SuppressWarnings("unchecked")
    /**
     * USSCO has a limit for how many reference identificaions can be sent as well as how many
     * characters there can be per reference identification. The first resolution tries to evenly divide
     * misc ship to information among the remaining segments while the second puts as much as possible 
     * starting with the first segment. This could optimized if a large quantity of information needs to be 
     * sent with these segments.
     */
    static Collection<ReferenceIdentification> getReferenceIdentifications(MiscShipToDto miscShipToDto,
            Collection<ReferenceIdentification> ris) {

        Map<String, Object> properties = null;
        try {
            properties = BeanUtils.describe(miscShipToDto);
        }
        catch (IllegalAccessException e) {
            LOG.error(e.getMessage());
        }
        catch (InvocationTargetException e) {
            LOG.error(e.getMessage());
        }
        catch (NoSuchMethodException e) {
            LOG.error(e.getMessage());
        }
        if (properties != null) {
            final int numberOfProperties = getNumberOfRelevantProperties(properties);
            final int remainingN9s = MAXIMUM_NUMBER_OF_N9_SEGMENTS - getNumberOfORIN9s(ris);
            if (numberOfProperties <= (remainingN9s)) {
                for (Entry<String, Object> entry : properties.entrySet()) {
                    Object value = entry.getValue();
                    if (relevantAddressProperty(value, entry)) {
                        ReferenceIdentification ri = createSimpleReferenceIdentification(value);
                        if (ri.getFreeFormMessage().length() > MAXIMUM_CHARACTERS_PER_SEGMENT) {

                            int i = ri.getFreeFormMessage().indexOf("Rm:");
                            if (i > 0) {
                                String deskTop = ri.getFreeFormMessage();
                                ReferenceIdentification riLabel = new ReferenceIdentification();
                                riLabel.setReferenceIdentificationQualifier(LETTERS_AND_NOTES);
                                riLabel.setReferenceIdentification(LABEL_INSTRUCTIONS);

                                ri.setFreeFormMessage(deskTop.substring(0, i));
                                ris.add(ri);

                                riLabel.setFreeFormMessage(deskTop.substring(i, deskTop.length()));
                                ris.add(riLabel);

                            }
                            else {
                                LOG.error("Failed to add address property: " + ri.getFreeFormMessage()
                                        + " due to restricted length of " + MAXIMUM_CHARACTERS_PER_SEGMENT
                                        + " characters per segment.");
                            }
                        }
                        else {
                            ris.add(ri);
                        }
                    }
                }
            }
            else {
                try {
                    HashSet<ReferenceIdentification> returnRis = evenlyDividePropertiesIntoReferenceIdentification(
                            properties, remainingN9s);
                    ris.addAll(returnRis);
                }
                catch (TooManyCharacterException e) {
                    //failed to evenly divide properties among segments try to put as many as possible per line
                    ris.addAll(putPropertiesIntoRis(properties, remainingN9s));
                }
            }
        }
        return ris;
    }

    private static String getSafeText(String source) {
        String response = source.replace("|", ":");
        return response;
    }

    private static ReferenceIdentification createReferenceIdenification() {
        ReferenceIdentification ri = new ReferenceIdentification();
        ri.setReferenceIdentificationQualifier(LETTERS_AND_NOTES);
        ri.setReferenceIdentification(DEALER_INFORMATION);
        return ri;
    }

    private static boolean relevantAddressProperty(Object value, final Entry<String, Object> entry) {
        final String key = entry.getKey();
        return (value != null && relevantField(key));
    }

    private static ReferenceIdentification createSimpleReferenceIdentification(Object value) {
        ReferenceIdentification ri = createReferenceIdenification();
        ri.setFreeFormMessage(value.toString());
        return ri;
    }

    private static HashSet<ReferenceIdentification> evenlyDividePropertiesIntoReferenceIdentification(Map<String, Object> properties, int remainingN9s) throws TooManyCharacterException {
        final int numberOfProperties = getNumberOfRelevantProperties(properties);
        //divide properties between segments evenly but throw exception if this would exceed the 40 character limit.
        HashSet<ReferenceIdentification> returnRis = new HashSet<>();
        //Put an even number of properties per line
        int propertiesPerLine = (int) Math.ceil((double)numberOfProperties / remainingN9s);
        int propertiesAlreadyOnLine = 0;
        ReferenceIdentification currentReferenceIdentification = createReferenceIdenification();
        currentReferenceIdentification.setFreeFormMessage("");
        Iterator<Entry<String, Object>> propertyIterator = properties.entrySet().iterator();
        while(propertyIterator.hasNext()){
            final Entry<String, Object> entry = propertyIterator.next();
            Object value = entry.getValue();
            if (relevantAddressProperty(value, entry)) {
                if(propertiesAlreadyOnLine < propertiesPerLine){
                    propertiesAlreadyOnLine++;
                    //The first line should not have semi colon
                    if(propertiesAlreadyOnLine == 1){
                        currentReferenceIdentification = createSimpleReferenceIdentification(value);
                    } else{
                        currentReferenceIdentification.setFreeFormMessage(currentReferenceIdentification.getFreeFormMessage() + "; " + getSafeText(value.toString()));
                    }
                }
                else{
                    if(currentReferenceIdentification.getFreeFormMessage().length() > MAXIMUM_CHARACTERS_PER_SEGMENT){
                        throw new TooManyCharacterException();
                    }
                    returnRis.add(currentReferenceIdentification);
                    propertiesAlreadyOnLine = 1;
                    currentReferenceIdentification = createSimpleReferenceIdentification(value);
                }
            }
        }
        if(currentReferenceIdentification.getFreeFormMessage().length() > MAXIMUM_CHARACTERS_PER_SEGMENT){
            throw new TooManyCharacterException();
        }
        returnRis.add(currentReferenceIdentification);
        return returnRis;
    }

    private static Collection<? extends ReferenceIdentification> putPropertiesIntoRis(Map<String, Object> properties, int remainingN9s) {
        ReferenceIdentification currentReferenceIdentification = createReferenceIdenification();
        currentReferenceIdentification.setFreeFormMessage("");        
        HashSet<ReferenceIdentification> returnRIs = new HashSet<>();
        Iterator<Entry<String, Object>> propertyIterator = properties.entrySet().iterator();
        while(propertyIterator.hasNext()){
            final Entry<String, Object> entry = propertyIterator.next();
            Object value = entry.getValue();
                if (relevantAddressProperty(value, entry)) {
                    try{
                        currentReferenceIdentification = addPropertyToRIOrCreateNextRI(returnRIs, value, entry, currentReferenceIdentification, remainingN9s);
                    }catch(ExceededMaximumNumberOfSegmentsException e){
                        LOG.error("Failed to add address property: " + getSafeText(value.toString()) + " due to restricted number of N9 segments to " 
                                        + remainingN9s + ".");
                    }
                }
        }
        returnRIs.add(currentReferenceIdentification);
        return returnRIs;
    }

    private static int getNumberOfRelevantProperties(Map<String, Object> properties) {
        int numberOfValid = 0;
        Iterator<Entry<String, Object>> propertyIterator = properties.entrySet().iterator();
        while (propertyIterator.hasNext()) {
            final Entry<String, Object> entry = propertyIterator.next();
            Object value = entry.getValue();
            if (relevantAddressProperty(value, entry)) {
                numberOfValid++;
            }
        }
        return numberOfValid;
    }

    private static ReferenceIdentification addPropertyToRIOrCreateNextRI(HashSet<ReferenceIdentification> returnRIs,
            Object value, final Entry<String, Object> entry, ReferenceIdentification currentReferenceIdentification,
            int remainingN9s) throws ExceededMaximumNumberOfSegmentsException {
        //Do not add more than maximum number of N9 segments
        if (returnRIs.size() < remainingN9s) {
            if (relevantAddressProperty(value, entry)) {
                String text = getSafeText(value.toString());
                if (text.length() > MAXIMUM_CHARACTERS_PER_SEGMENT) {
                    LOG.error("Failed to add address property: " + text + " due to restricted length of "
                            + MAXIMUM_CHARACTERS_PER_SEGMENT + " characters per segment.");
                }
                else {
                    //When a combination of strings gets too big, create the next segment
                    if (currentReferenceIdentification.getFreeFormMessage().length() + text.length() > MAXIMUM_CHARACTERS_PER_SEGMENT) {
                        returnRIs.add(currentReferenceIdentification);
                        if (returnRIs.size() < remainingN9s) {
                            currentReferenceIdentification = createSimpleReferenceIdentification(value);
                        }
                        else {
                            throw new ExceededMaximumNumberOfSegmentsException();
                        }
                    }
                    else {
                        //No semi-colon before first element
                        if (StringUtils.isEmpty(currentReferenceIdentification.getFreeFormMessage())) {
                            currentReferenceIdentification.setFreeFormMessage(getSafeText(value.toString()));
                        }
                        else {
                            currentReferenceIdentification.setFreeFormMessage(currentReferenceIdentification
                                    .getFreeFormMessage()
                                    + "; " + getSafeText(value.toString()));
                        }
                    }
                }
            }
        }
        else {
            throw new ExceededMaximumNumberOfSegmentsException();
        }
        return currentReferenceIdentification;
    }

    private static int getNumberOfORIN9s(Collection<ReferenceIdentification> ris) {
        int number = 0;
        for (ReferenceIdentification ri : ris) {
            if (DEALER_INFORMATION.equals(ri.getReferenceIdentification())) {
                number++;
            }
        }
        return number;
    }

    private static boolean relevantField(final String key) {
        return !"class".equals(key) && !"financeNumber".equals(key) && !"contractNumber".equals(key)
                && !"fedstripNumber".equals(key) && !"usAccount".equals(key);
    }

    private static class TooManyCharacterException extends Exception {

        public TooManyCharacterException() {
        }
    }

    private static class ExceededMaximumNumberOfSegmentsException extends Exception {

        public ExceededMaximumNumberOfSegmentsException() {
        }
    }
}
