package com.apd.phoenix.service.integration.routes;

import com.apd.phoenix.service.dataformat.edi.EDIDataFormat;
import javax.edi.model.x12.edi850.PurchaseOrder;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.camel.processor.PurchaseOrderToEdi850Translator;
import com.apd.phoenix.service.integration.camel.processor.RawEdiToMessageDto;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.EmailNotifierOnFTPDown;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

public class EDI850OutboundRouteBuilder extends EDIRouteBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(EDI850OutboundRouteBuilder.class);

    private String purchaseOrderSource;

    private String purchaseOrderResendSource;

    private String edi850Endpoint;
    private String edi850PartnerEndpoint;
    private String edi850PartnerEndpointDLQ;

    private String ediLogSource;
    private String logOutput = "http://messages";
    private String logOutputDLQ;
    private String logOutputURI;
    private String senderId;

    private String notifyOnAS2Down;

    //Inbound

    @Override
    public void configure() throws Exception {

        onException(ClassCastException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Incorrect Type");

        DataFormat format = new EDIDataFormat(PurchaseOrder.class);

        from(purchaseOrderSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.OUTBOUND_850))
                .setHeader("messageCommunicationType", constant(CommunicationType.OUTBOUND)).setHeader("partnerId")
                .jxpath("/in/body/partnerId")
                //filename
                .setHeader(Exchange.FILE_NAME, simple("po850_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}")).setHeader(
                        "messageType", constant(MessageType.EDI)).setHeader("messageDestination",
                        constant(edi850PartnerEndpoint)).setHeader("apdPo").jxpath("/in/body/apdPoNumber").setHeader(
                        "messageFileName", simple("${in.header.CamelFileName}")).setHeader("messageEventType",
                        simple(EventTypeDto.ORDERED.name())).bean(new PurchaseOrderToEdi850Translator(),
                        "toPurchaseOrder")
                //Set Transaction Headers                     
                .setHeader("senderId", constant(senderId)).setHeader("interchangeId").jxpath(
                        "/in/body/envelopeHeader/interchangeControlNumber").setHeader("groupId").jxpath(
                        "/in/body/groupEnvelopeHeader/groupControlNumber").setHeader("transactionId").jxpath(
                        "/in/body/purchaseOrderBody[1]/header/transactionSetHeader/transactionSetControlNumber").log(
                        LoggingLevel.INFO, "Trans id: :" + simple("${header.transactionId}")).to(edi850Endpoint);

        from(edi850Endpoint).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.OUTBOUND_850_OUT))
                .errorHandler(
                        deadLetterChannel(notifyOnAS2Down).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).marshal(format).wireTap(
                        getBackupPartnerEndpoint()).multicast().parallelProcessing()
                //to endpoint
                .to(ediLogSource, edi850PartnerEndpoint);

        from(notifyOnAS2Down).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.AS2_DOWN_NOTIFY))
                .choice().when(header("partnerId").contains("USSCO")).bean(EmailNotifierOnFTPDown.class,
                        "as2ServerDownEmailNotify").to(edi850PartnerEndpointDLQ).stop().otherwise().to(
                        edi850PartnerEndpointDLQ);

        from(ediLogSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.OUTBOUND_850_LOG))
                .errorHandler(
                        deadLetterChannel(logOutputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2)).setHeader(Exchange.CONTENT_TYPE,
                        constant("application/json")).process(new RawEdiToMessageDto()).setHeader(Exchange.HTTP_URI,
                        simple(logOutputURI + EventTypeDto.PURCHASE_ORDER_SENT.name() + "/${header.apdPo}")).log(
                        LoggingLevel.INFO, simple("${in.header.CamelHttpUri}").toString()).marshal().json(
                        JsonLibrary.Jackson).to(logOutput);

        from(purchaseOrderResendSource).startupOrder(
                getStartOrderOffset()
                        + EDIOrderUtils.getOffset(EDIOrderUtils.RouteOrderOffsetOutboundEDI850.OUTBOUND_850_RESEND))
                .errorHandler(
                        deadLetterChannel(edi850PartnerEndpointDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //filename
                .setHeader(Exchange.FILE_NAME, simple("po850_apdphoen.dat.${date:now:yyyyMMddhhmmssSSS}"))
                //partner id
                .setHeader("partnerId").jxpath("/in/body/partnerId").setBody().jxpath("in/body/rawMessage").to(
                        edi850PartnerEndpoint);

    }

    public String getPurchaseOrderSource() {
        return purchaseOrderSource;
    }

    public void setPurchaseOrderSource(String purchaseOrderSource) {
        this.purchaseOrderSource = purchaseOrderSource;
    }

    public String getLogOutput() {
        return logOutput;
    }

    public void setLogOutput(String logOutput) {
        this.logOutput = logOutput;
    }

    public String getLogOutputDLQ() {
        return logOutputDLQ;
    }

    public void setLogOutputDLQ(String logOutputDLQ) {
        this.logOutputDLQ = logOutputDLQ;
    }

    public String getEdi850PartnerEndpointDLQ() {
        return edi850PartnerEndpointDLQ;
    }

    public void setEdi850PartnerEndpointDLQ(String edi850PartnerEndpointDLQ) {
        this.edi850PartnerEndpointDLQ = edi850PartnerEndpointDLQ;
    }

    public String getLogOuputURI() {
        return logOutputURI;
    }

    public void setLogOuputURI(String logOuputURI) {
        this.logOutputURI = logOuputURI;
    }

    public String getPurchaseOrderResendSource() {
        return purchaseOrderResendSource;
    }

    public void setPurchaseOrderResendSource(String purchaseOrderResendSource) {
        this.purchaseOrderResendSource = purchaseOrderResendSource;
    }

    public String getEdi850PartnerEndpoint() {
        return edi850PartnerEndpoint;
    }

    public void setEdi850PartnerEndpoint(String edi850PartnerEndpoint) {
        this.edi850PartnerEndpoint = edi850PartnerEndpoint;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /**
     * @return the edi850Endpoint
     */
    public String getEdi850Endpoint() {
        return edi850Endpoint;
    }

    /**
     * @param edi850Endpoint the edi850Endpoint to set
     */
    public void setEdi850Endpoint(String edi850Endpoint) {
        this.edi850Endpoint = edi850Endpoint;
    }

    /**
     * @return the ediLogSource
     */
    public String getEdiLogSource() {
        return ediLogSource;
    }

    /**
     * @param ediLogSource the ediLogSource to set
     */
    public void setEdiLogSource(String ediLogSource) {
        this.ediLogSource = ediLogSource;
    }

    public String getNotifyOnAS2Down() {
        return notifyOnAS2Down;
    }

    public void setNotifyOnAS2Down(String notifyOnAS2Down) {
        this.notifyOnAS2Down = notifyOnAS2Down;
    }

}
