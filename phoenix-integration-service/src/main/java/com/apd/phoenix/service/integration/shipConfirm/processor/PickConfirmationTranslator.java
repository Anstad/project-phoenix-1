package com.apd.phoenix.service.integration.shipConfirm.processor;

import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.soloman.pickConfirm.model.PickConfirmationDto;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.dto.PickDto;
import com.apd.phoenix.service.persistence.jpa.LineItemDao;

@Stateless
public class PickConfirmationTranslator {

    private static final Logger LOG = LoggerFactory.getLogger(PickConfirmationTranslator.class);

    @Inject
    LineItemDao lineItemDao;

    public static final String ORDER_TYPE = "EXC";

    public PickConfirmationDto pickDtoToPickConfirmDto(@Body PickDto pickDto) throws SecurityException,
            IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException,
            SystemException, NotSupportedException {

        PickConfirmationDto pickConfirmationDto = new PickConfirmationDto();
        LineItem lineItem = lineItemDao.getLineItemByLineNumberAndApdPo(pickDto.getLineReference(), pickDto
                .getApdOrderNumber());
        pickConfirmationDto.setCustId(lineItem.getOrder().getAccount().getSolomonCustomerId());
        pickConfirmationDto.setOrderDate(lineItem.getOrder().getOrderDate());
        pickConfirmationDto.setOrderNumber(pickDto.getCustomerPoNumber());
        pickConfirmationDto.setOrderType(ORDER_TYPE);
        pickConfirmationDto.setQuantity(pickDto.getQtyToPick());
        pickConfirmationDto.setShipDate(new Date());
        pickConfirmationDto.setShipperId(pickDto.getShipperId());
        pickConfirmationDto.setSku(pickDto.getInventoryId());

        return pickConfirmationDto;
    }
}
