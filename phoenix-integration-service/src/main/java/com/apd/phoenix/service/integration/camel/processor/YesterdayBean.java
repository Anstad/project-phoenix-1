package com.apd.phoenix.service.integration.camel.processor;

import java.util.Calendar;
import org.apache.camel.Exchange;

public class YesterdayBean {

    public void setYesterday(Exchange exchange) {
        exchange.setOut(exchange.getIn());
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        exchange.getOut().setHeader("yesterday", yesterday.getTime());
    }
}
