package com.apd.phoenix.service.integration.solr;

import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.getLongValue;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapBaseQueryResultsToSolrDocument;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapCategoryXPropertyIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapClassificationIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapFavoritesListResultsToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapImageToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapMatchbookResultsToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapPropertyIconToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapPropertyToItem;
import static com.apd.phoenix.service.integration.solr.CatalogXItemDocumentMapper.mapSkuToItem;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.solr.client.solrj.SolrServer;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.PricingTypeBp.PricingCalculationException;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.listener.CatalogXItemListener;

@LocalBean
@Stateless
public class CatalogXItemUpdateBean {

    private static final Logger LOG = LoggerFactory.getLogger(CatalogXItemUpdateBean.class);

    @Inject
    private ConcurrentUpdateSolrServiceBean solrService;

    @Inject
    private CatalogXItemBp catalogXItemBp;

    @Inject
    private ItemBp itemBp;

    @PersistenceContext(unitName = "Phoenix")
    private EntityManager entityManager;

    private static final String BASE_QUERY = "select i.id as ITEM_ID, c.id as CATALOGXITEM_ID, i.name, i.description, i.HIERARCHYNODE_ID, i.VENDORCATALOG_ID, "
            + "c.CATALOG_ID, c.CUSTOMERSKU_ID, customerSku.value as CUSTOMER_SKU, c.price, i.searchterms, cast(c.coreitemexpirationdate as timestamp) as COREEXPIREDATE, "
            + "cast(c.coreitemstartdate as timestamp)as CORESTARTDATE, uom.name as UOM_NAME, m.name as MNAME, h.description as HIERARCHY_DESCRIPTION, "
            + "h.parent_id as HIERARCHY_PARENT_ID, h.indexedpaths, v.name as vendorName, i.status as ITEM_STATUS, i.multiple as MULTIPLE, i.minimum as MINIMUM, "
            + "rep_sku.value as REP_SKU, rep_vend.name as REP_VEND_NAME, cust_order.value as CUSTOM_ORDER, spec_order.value as SPECIAL_ORDER, c.overrideCatalogs AS OVERRIDE_CATALOGS "
            + "from catalogxitem c "
            + "inner join Item i on i.id = c.item_id "
            + "inner join catalog cat on cat.id = i.vendorcatalog_id "
            + "inner join vendor v on v.id = cat.vendor_id "
            + "inner join catalog cust_cat on cust_cat.id = c.catalog_id "
            + "LEFT JOIN unitofmeasure uom on uom.id = i.unitofmeasure_id "
            + "LEFT JOIN manufacturer m on m.id = i.manufacturer_id "
            + "LEFT JOIN hierarchynode h on h.id = i.hierarchynode_id "
            + "left join item rep on i.replacement_id = rep.id "
            + "left join Sku customerSku on c.CUSTOMERSKU_ID = customerSku.id "
            + "left join sku rep_sku on rep_sku.item_id = rep.id and rep_sku.type_id in (select id from skutype where name like 'APD') "
            + "left join catalog rep_cat on rep_cat.id = rep.vendorcatalog_id "
            + "left join vendor rep_vend on rep_cat.vendor_id = rep_vend.id "
            + "left join itemxitempropertytype cust_order on cust_order.item_id = i.id and cust_order.type_id in (select id from itempropertytype where name like 'customOrder') "
            + "left join itemxitempropertytype spec_order on spec_order.item_id = i.id and spec_order.type_id in (select id from itempropertytype where name like 'specialOrder') "
            + "where c.id=:catxItemId "
            + "and (cat.expirationDate is null OR cat.expirationDate > sysdate) and (cust_cat.expirationDate is null OR cust_cat.expirationDate > sysdate)";

    private static final String IMAGE_QUERY = "select IMAGEURL from ITEMIMAGE where item_id = :itemId";
    private static final String SKU_QUERY = "select s.value, t.name from SKU s, skutype t where t.id=s.type_id and ITEM_ID = :itemId";
    private static final String CLASSIFICATION_ICON_QUERY = "select t.NAME, t.ICONURL, t.TOOLTIPLABEL from ITEMCLASSIFICATION_ITEM i, ITEMCLASSIFICATION c, ITEMCLASSIFICATIONTYPE t where i.ITEMS_ID = :itemId and i.ITEMCLASSIFICATIONS_ID=c.id and c.ITEMCLASSTYPE_ID=t.id";
    private static final String PROPERTY_ICON_QUERY = "select /*+ USE_HASH(t itemx) */ t.NAME, t.ICONURL, t.TOOLTIPLABEL, itemx.VALUE from itempropertytype t, itemxitempropertytype itemx where itemx.item_id=:itemId and itemx.type_id=t.id and t.ICONURL IS NOT NULL";
    private static final String CATEGORY_X_PROPERTY_ICON_QUERY = "select t.NAME, t.ICONURL, t.TOOLTIPLABEL, catx.VALUE from itempropertytype t, catalogxitemxitempropertytype catx where catx.CATALOGXITEM_ID=:catalogxitemID and catx.type_id=t.id";
    private static final String PROPERTY_QUERY = "select s.name,s.value from SPECIFICATIONPROPERTY s,ITEMSPECIFICATION i where i.ITEM_ID = :itemId and i.id = s.itemspecification_id";
    private static final String FAVORITES_LIST_QUERY = "select favoriteslist.ID as FAVORITES_LIST_ID from favoriteslist join comp_favlist_catxitem on comp_favlist_catxitem.FAVORITESLIST_ID = favoriteslist.ID where (favoriteslist.CATALOG_ID is not null) and comp_favlist_catxitem.ITEMS_ID = :catalogXItemId";
    private static final String MATCHBOOK_QUERY = "select itemMatchbook.matchbook_id as MB_ID from item_matchbook itemMatchbook where itemMatchbook.item_id = :itemId";

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void insertItems(Long catalogXItemId) {
        catalogXItemBp.postPersist(catalogXItemId);
        try {
			CatalogXItemListener.addUpdateSolrDocument(catalogXItemId);
		} catch (NamingException | JMSException e) {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Error sending item update", e);
			}
		}
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void repriceItems(Long catalogXItemId) {
        try {
            catalogXItemBp.update(itemBp.calculatePrice(catalogXItemBp.findById(catalogXItemId, CatalogXItem.class)));
        }
        catch (PricingCalculationException priceEx) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("error calculating price", priceEx);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateItems(List<Long> catalogXItemIds) {
        SolrServer solrServer = solrService.getSolrServer();
        for (Long catalogXItemId : catalogXItemIds) {
            try {
                SolrDocumentPojo solrDocument = createUpdateDocument(catalogXItemId);
                if (solrDocument != null) {
                    solrServer.addBean(solrDocument);
                }
            }
            catch (Exception e) {
                LOG.error("Exception search indexing item: " + catalogXItemId, e);
            }
        }
        // commit changes
        LOG.info("Committing Batch Solr Updates");
        try {
            solrServer.commit();
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void deleteItems(List<CatalogXItem.DeleteItemMessage> messages) {
        SolrServer solrServer = solrService.getSolrServer();
        for (CatalogXItem.DeleteItemMessage message : messages) {
            catalogXItemBp.postDelete(message.getCatalogId(), message.getItemId());
            try {
                solrServer.deleteById(message.toString());
            }
            catch (Exception e) {
                LOG.error("Exception deleting item: " + message, e);
            }
        }
        // commit changes
        LOG.info("Committing Batch Solr Deletes");
        try {
            solrServer.commit();
        }
        catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private Map<String, Object> performSingleMapQuery(String query, String parameterName, Long id) {
        List<Map<String, Object>> results = performMultiMapQuery(query, parameterName, id);
        if (results.size() == 1) {
            return results.get(0);
        }
        if (results.size() > 1) {
            throw new IllegalStateException("Expected 1 result, actually: " + results.size() + " for query: " + query
                    + " and id: " + id);
        }
        else {
            return null;
        }
    }

    private List<Map<String, Object>> performMultiMapQuery(String query, String parameterName, Long id) {
        // get the hibernate session from the injected entity manager.
        Session session = (Session) entityManager.getDelegate();

        final Query itemQuery = session.createSQLQuery(query);
        itemQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        itemQuery.setParameter(parameterName, id);

        return (List<Map<String, Object>>) itemQuery.list();
    }

    private SolrDocumentPojo createUpdateDocument(Long catalogXItemId) {
        // create the item document to send to solr.
        SolrDocumentPojo solrDocument = new SolrDocumentPojo();
        // perform the query for the base info.
        Map<String, Object> baseQueryResults = performSingleMapQuery(BASE_QUERY, "catxItemId", catalogXItemId);
        Long itemID;
        try {
            itemID = getLongValue(baseQueryResults.get("ITEM_ID"));
            if (itemID == null) {
                return null;
            }
        }
        catch (NullPointerException ex) {
            return null;
        }
        // init solrdocument with values from base query
        mapBaseQueryResultsToSolrDocument(baseQueryResults, solrDocument);

        // init solrdocument with values from image query
        List<Map<String, Object>> imgQueryResults = performMultiMapQuery(IMAGE_QUERY, "itemId", itemID);
        mapImageToItem(imgQueryResults, solrDocument);

        // perform the query for skus
        List<Map<String, Object>> skus = performMultiMapQuery(SKU_QUERY, "itemId", itemID);
        mapSkuToItem(skus, solrDocument);

        List<Map<String, Object>> classificationIconResults = performMultiMapQuery(CLASSIFICATION_ICON_QUERY, "itemId",
                itemID);
        mapClassificationIconToItem(classificationIconResults, solrDocument);

        List<Map<String, Object>> propertyIcon = performMultiMapQuery(PROPERTY_ICON_QUERY, "itemId", itemID);
        mapPropertyIconToItem(propertyIcon, solrDocument);

        List<Map<String, Object>> categoryXPropertyIconQueryResults = performMultiMapQuery(
                CATEGORY_X_PROPERTY_ICON_QUERY, "catalogxitemID", catalogXItemId);
        mapCategoryXPropertyIconToItem(categoryXPropertyIconQueryResults, solrDocument);

        // get properties
        List<Map<String, Object>> properties = performMultiMapQuery(PROPERTY_QUERY, "itemId", itemID);
        mapPropertyToItem(properties, solrDocument);

        List<Map<String, Object>> favoritesListQueryResults = performMultiMapQuery(FAVORITES_LIST_QUERY,
                "catalogXItemId", catalogXItemId);
        mapFavoritesListResultsToItem(favoritesListQueryResults, solrDocument);

        List<Map<String, Object>> matchbookQueryResults = performMultiMapQuery(MATCHBOOK_QUERY, "itemId", itemID);
        mapMatchbookResultsToItem(matchbookQueryResults, solrDocument);

        // Set the id on itemDoc
        solrDocument.setId(solrDocument.getCatalogxitemId());

        return solrDocument;
    }
}
