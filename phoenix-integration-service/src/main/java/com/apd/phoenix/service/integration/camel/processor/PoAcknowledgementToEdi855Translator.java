/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import javax.edi.model.x12.edi850.segment.ShipmentInformationGroup;
import javax.edi.model.x12.edi855.segment.Detail;
import javax.edi.model.x12.edi855.segment.AckHeader;
import javax.edi.model.x12.edi855.segment.ItemAcknowledgementGroup;
import javax.edi.model.x12.edi855.segment.POAcknowledgementBeginningSegment;
import javax.edi.model.x12.edi855.segment.POAcknowledgementBody;
import javax.edi.model.x12.edi855.segment.Trailer;
import javax.edi.model.x12.segment.AdditionalNameInformation;
import javax.edi.model.x12.segment.AddressInformation;
import javax.edi.model.x12.segment.GeographicLocation;
import javax.edi.model.x12.segment.GroupEnvelopeHeader;
import javax.edi.model.x12.segment.GroupEnvelopeTrailer;
import javax.edi.model.x12.segment.InterchangeEnvelopeHeader;
import javax.edi.model.x12.segment.InterchangeEnvelopeTrailer;
import javax.edi.model.x12.segment.LineItemAcknowledgement;
import javax.edi.model.x12.segment.Name;
import javax.edi.model.x12.segment.POBaselineItemData;
import javax.edi.model.x12.segment.ReferenceIdentification;
import javax.edi.model.x12.segment.ReferenceNumber;
import javax.edi.model.x12.segment.TermsOfSale;
import javax.edi.model.x12.segment.TransactionSetHeader;
import javax.edi.model.x12.segment.TransactionSetTrailer;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;
import com.apd.phoenix.service.model.LineItemStatus.LineItemStatusEnum;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import javax.edi.model.x12.segment.PersonContact;

/**
 *
 * @author nreidelb
 */
public class PoAcknowledgementToEdi855Translator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(PoAcknowledgementToEdi855Translator.class);

    public static final String PURCHASE_ORDER_ACKNOWLEDGEMENT_CODE = "PR";
    public static final String PO_ACKNOWLEDGEMENT_EDI_NUMBER = "855";
    public static final String ORIGINAL_PURPOSE_CODE = "00";
    public static final String STOCK_NUMBER_QUALIFIER = "SW";
    public static final String MUTUALLY_DEFINED = "ZZ";
    public static final String SHIP_TO_ENTITY_IDENTIFIER_CODE = "ST";
    public static final String CONTRACT_NUMBER_QUALIFIER = "CT";
    public static final String ONE_TRANSACTION_PER_EDI_MESSAGE = "1";
    public static final String SUPPORTING_DOCUMENT_NUMBER_QUALIFIER = "43";
    public static final String ELECTRONIC_PURCHASE_ORDER = "E";
    public static final String ACKNOWLEDGED_WITH_CHANGE = "AC";
    public static final String ACKNOWLEDGED_NO_CHANGE = "AD";
    public static final String REJECTED = "RD";
    public static final String ITEM_ACCEPTED = "IA";
    public static final String ITEM_BACKORDERED = "IB";
    public static final String ITEM_DELETED = "ID";
    public static final String ITEM_ACCEPTED_REDUCED_BY_QUANTITY = "IQ";
    public static final String ITEM_REJECTED = "IR";
    public static final String PERSON_CONTACT_FUNCTION_CODE = "OC";
    public static final String PHONE_NUMBER_QUALIFIER = "TE";
    public static final int LENGTH_OF_INTERCHANGE_CONTROL_NUMBER = 4;

    @Override
    public void process(Exchange exchange) throws Exception {
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        String groupId = (String) exchange.getIn().getHeader("groupId");
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        Boolean resend = (Boolean) exchange.getIn().getHeader("resend");
        if(resend == null){
        	resend = Boolean.FALSE;
        }
        POAcknowledgementDto poAcknowledgementDto = (POAcknowledgementDto) exchange.getIn().getBody();
        Properties partnerProperties = PropertiesLoader.getAsProperties(poAcknowledgementDto.getPartnerId().toLowerCase());
        javax.edi.model.x12.edi855.POAcknowledgement pOAcknowledgementEdi = new javax.edi.model.x12.edi855.POAcknowledgement();
        InterchangeEnvelopeHeader interchangeEnvelopeHeader = OutboundEdiUtils.generateDefaultInterchangeEnvelopeHeader(interchangeId);
        interchangeEnvelopeHeader.setTestIndicator(partnerProperties.getProperty(OutboundEdiUtils.TEST_INDICATOR));
        interchangeEnvelopeHeader.setInterchangeReceiverID(OutboundEdiUtils.padWithSpaces(
                                    partnerProperties.getProperty(OutboundEdiUtils.APPLICATION_RECIEVERS_CODE), 
                                    OutboundEdiUtils.LENGTH_OF_INTERCHANGE_RECEIVER_ID));
        pOAcknowledgementEdi.setInterchangeEnvelopeHeader(interchangeEnvelopeHeader);
        
        GroupEnvelopeHeader groupEnvelopeHeader = OutboundEdiUtils.generateDefaultGroupEnvelopeHeader(groupId);
        groupEnvelopeHeader.setFunctionalIDCode(PURCHASE_ORDER_ACKNOWLEDGEMENT_CODE);
        groupEnvelopeHeader.setApplicationReceiversCode(partnerProperties.getProperty(OutboundEdiUtils.APPLICATION_RECIEVERS_CODE));        
        pOAcknowledgementEdi.setGroupEnvelopeHeader(groupEnvelopeHeader);
        
        List<POAcknowledgementBody> bodies = new ArrayList<>();
        POAcknowledgementBody poAcknowledgementBody = new POAcknowledgementBody();
        AckHeader header = new AckHeader();
        TransactionSetHeader transactionSetHeader = new TransactionSetHeader();        
        transactionSetHeader.setTransactionSetIdentifierCode(PO_ACKNOWLEDGEMENT_EDI_NUMBER);
        transactionSetHeader.setTransactionSetControlNumber(transactionId);
        header.setTransactionSetHeader(transactionSetHeader);
        
        POAcknowledgementBeginningSegment poAcknowledgementBeginningSegment = new POAcknowledgementBeginningSegment();
        String status = poAcknowledgementDto.getPurchaseOrderDto().getStatus().toUpperCase();
		if (status.equals(OrderStatusEnum.CANCELED.getValue())
				|| status.equals(OrderStatusEnum.REJECTED.getValue())) {
			poAcknowledgementBeginningSegment.setAcknowledgmentType(REJECTED);
		} else {
			boolean change = false;
			for (LineItemDto item : poAcknowledgementDto.getLineItems()) {
				if (!resend && !item.getStatus().getValue()
						.equals(LineItemStatusEnum.ACCEPTED.getValue())) {
					change = true;
					break;
				}
			}
			if (change) {
				poAcknowledgementBeginningSegment
						.setAcknowledgmentType(ACKNOWLEDGED_WITH_CHANGE);
			} else {
				poAcknowledgementBeginningSegment
						.setAcknowledgmentType(ACKNOWLEDGED_NO_CHANGE);
			}
		}
        poAcknowledgementBeginningSegment.setTransactionSetPurposeCode(ORIGINAL_PURPOSE_CODE);
        PurchaseOrderDto purchaseOrderDto = poAcknowledgementDto.getPurchaseOrderDto();
        if(purchaseOrderDto.retrieveCustomerPoNumber()!=null){
            final String safeCustomerPo = OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.retrieveCustomerPoNumber().getValue(),16);
			poAcknowledgementBeginningSegment.setPurchaseOrderNumber(safeCustomerPo);
        }
        else{
            LOG.warn("No Customer PO number found.");
        }
        poAcknowledgementBeginningSegment.setPurchaseOrderDate(purchaseOrderDto.getOrderDate());
        poAcknowledgementBeginningSegment.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getApdPoNumber(),16));
        header.setBeginningSegment(poAcknowledgementBeginningSegment);
        List<ReferenceIdentification> referenceIdentification = new ArrayList<>();
        ReferenceIdentification referenceIdentification1 = new ReferenceIdentification();
        referenceIdentification1.setReferenceIdentificationQualifier(SUPPORTING_DOCUMENT_NUMBER_QUALIFIER);
        referenceIdentification1.setReferenceIdentification(ELECTRONIC_PURCHASE_ORDER);
        referenceIdentification.add(referenceIdentification1);
        final PersonContact personContact = new PersonContact();
        personContact.setContactFunctionCode(PERSON_CONTACT_FUNCTION_CODE);
        personContact.setName(purchaseOrderDto.getShipToAddressDto().getName());
        if(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto()!=null && 
                purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getRequesterPhone()!=null){
            personContact.setCommunicationNumberQualifier(PHONE_NUMBER_QUALIFIER);
            personContact.setCommunicationNumber(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getRequesterPhone(),20));
        }
        header.setPersonContact(personContact);
        header.setReferenceIdentification(referenceIdentification);
        poAcknowledgementBody.setAckHeader(header);
        
        Detail detail = new Detail();
        List<ItemAcknowledgementGroup> itemAcknowlegements = new ArrayList<>();
        for(LineItemDto item: poAcknowledgementDto.getLineItems()){            
            ItemAcknowledgementGroup itemAcknowledgementGroup = new ItemAcknowledgementGroup();
            POBaselineItemData poBaselineItemData = new POBaselineItemData();
            poBaselineItemData.setAssignedIdentifier(item.getCustomerLineNumber());
            poBaselineItemData.setQuantity(item.getQuantity());
            if(! StringUtils.isEmpty(item.getCustomerExpectedUoM())){
                poBaselineItemData.setUnitOfMeasure(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getCustomerExpectedUoM(),2,2));
            } else {
                poBaselineItemData.setUnitOfMeasure(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getUnitOfMeasure().getName(),2,2));
            }
            poBaselineItemData.setProdServiceIDQualifier1(STOCK_NUMBER_QUALIFIER);
            if(item.getCustomerSku()!=null){
                poBaselineItemData.setProdServID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getCustomerSku().getValue(),30));
            } else {
                poBaselineItemData.setProdServID1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getApdSku(),30));
            }
            itemAcknowledgementGroup.setItemData(poBaselineItemData);
            if(purchaseOrderDto.getShipToAddressDto()!=null && purchaseOrderDto.getShipToAddressDto().getMiscShipToDto()!=null &&  StringUtils.isNotBlank(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getFinanceNumber())){
                TermsOfSale termsOfSale = new TermsOfSale();
                termsOfSale.setTermsTypeCode(MUTUALLY_DEFINED);
                termsOfSale.setDescription(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getFinanceNumber(),16));
                itemAcknowledgementGroup.setTermsOfSale(termsOfSale);
            }
            List<LineItemAcknowledgement> lineItemAcknowledgements = new ArrayList<>();
            LineItemAcknowledgement lineItemAcknowledgement = new LineItemAcknowledgement();
            lineItemAcknowledgement.setQuantity(item.getQuantity());
            lineItemAcknowledgement.setUnitOfMeasure(OutboundEdiUtils.truncateAndRemoveDisallowedChars(item.getUnitOfMeasure().getName(),2,3));
            String itemStatus = item.getStatus().getValue().toUpperCase();
            if (itemStatus.equals(LineItemStatusEnum.BACKORDERED.getValue())) {
                    lineItemAcknowledgement.setLineItemStatus(ITEM_BACKORDERED);
            } else if (itemStatus.equals(LineItemStatusEnum.PARTIAL_BACKORDERED.getValue())) {
                    lineItemAcknowledgement.setLineItemStatus(ITEM_BACKORDERED);
                    lineItemAcknowledgement.setQuantity(item.getBackorderedQuantity());
            } else if (itemStatus.equals(LineItemStatusEnum.CANCELED.getValue())) {
                    lineItemAcknowledgement.setLineItemStatus(ITEM_DELETED);
            } else if (itemStatus.equals( LineItemStatusEnum.ITEM_QUANTITY_CHANGED.getValue())) {
                    lineItemAcknowledgement.setLineItemStatus(ITEM_ACCEPTED_REDUCED_BY_QUANTITY);
                    BigInteger quantityReducedBy = (new BigInteger(item.getOriginalQuantity().toString())).subtract(item.getQuantity());                   
                    if(quantityReducedBy.max(BigInteger.ZERO).equals(BigInteger.ZERO)){ //IF quantity reduced by is negative
                        lineItemAcknowledgement.setQuantity(quantityReducedBy);
                    } else {
                    	lineItemAcknowledgement.setLineItemStatus(ITEM_ACCEPTED);
                    }
            } else if (itemStatus.equals(LineItemStatusEnum.REJECTED.getValue())) {
                    lineItemAcknowledgement.setLineItemStatus(ITEM_REJECTED);
            } else {
            	//Any further along status means the item is accepted
                lineItemAcknowledgement.setLineItemStatus(ITEM_ACCEPTED);
            }
            lineItemAcknowledgement.setDate(purchaseOrderDto.getRequestedDeliveryDate());
            lineItemAcknowledgements.add(lineItemAcknowledgement);
            itemAcknowledgementGroup.setItemAcknowledgements(lineItemAcknowledgements);
            ShipmentInformationGroup shipmentInformationGroup = new ShipmentInformationGroup();
            Name shipToName = new Name();
            shipToName.setEntityIdentifierCode(SHIP_TO_ENTITY_IDENTIFIER_CODE);
            shipToName.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getName(),40));
            shipToName.setIdentificationCodeQualifier(MUTUALLY_DEFINED);
            shipToName.setIdentificationCode(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getFedstripNumber(),2,6));
            shipmentInformationGroup.setShipToName(shipToName);
            List<AdditionalNameInformation> additionalNameInformationList = addNameInformationList(purchaseOrderDto.getShipToAddressDto().getLine1(), new ArrayList<AdditionalNameInformation>());
            if(!StringUtils.isEmpty(purchaseOrderDto.getShipToAddressDto().getLine2())){
                additionalNameInformationList = addNameInformationList(purchaseOrderDto.getShipToAddressDto().getLine2(),additionalNameInformationList);
            }
            shipmentInformationGroup.setAdditionalNameInformation(additionalNameInformationList);
            if(StringUtils.isNotBlank(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getStreet3())){
                AddressInformation addressInformation = new AddressInformation();
                addressInformation.setAddressLine1(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getStreet3().trim(), 30));
                shipmentInformationGroup.setAddressInformation(addressInformation);
            }
            GeographicLocation geographicLocation = OutboundEdiUtils.createN4(purchaseOrderDto.getShipToAddressDto());
            shipmentInformationGroup.setGeographicLocation(geographicLocation);
            if(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getContractNumber() != null){
                ReferenceNumber referenceNumber = new ReferenceNumber();
                referenceNumber.setReferenceIdentificationQualifier(CONTRACT_NUMBER_QUALIFIER);
                referenceNumber.setReferenceIdentification(OutboundEdiUtils.truncateAndRemoveDisallowedChars(purchaseOrderDto.getShipToAddressDto().getMiscShipToDto().getContractNumber(), 30));
                shipmentInformationGroup.setReferenceNumber(referenceNumber);
            }
            itemAcknowledgementGroup.setShipmentInformationGroup(shipmentInformationGroup);
            itemAcknowlegements.add(itemAcknowledgementGroup);
        }
        Collections.sort(itemAcknowlegements,new Comparator<ItemAcknowledgementGroup>(){

			@Override
			public int compare(ItemAcknowledgementGroup arg0, ItemAcknowledgementGroup arg1) {
				return EDIOrderUtils.ediSegmentCompartor(arg0.getItemData().getAssignedIdentifier(), arg1.getItemData().getAssignedIdentifier());
			}
        	
        });
        detail.setItemAcknowledgements(itemAcknowlegements);
        poAcknowledgementBody.setDetail(detail);
        
        Trailer trailer = new Trailer();
        TransactionSetTrailer transactionSetTrailer = new TransactionSetTrailer();
        transactionSetTrailer.setNumberOfIncludedSegments(calculateNumberOfIncldedSegments(poAcknowledgementBody));
        transactionSetTrailer.setTransactionControlNumber(OutboundEdiUtils.zeroPad(transactionSetHeader.getTransactionSetControlNumber(),LENGTH_OF_INTERCHANGE_CONTROL_NUMBER));
        trailer.setTransactionSetTrailer(transactionSetTrailer);
        poAcknowledgementBody.setFooter(trailer);
        bodies.add(poAcknowledgementBody);
        
        pOAcknowledgementEdi.setBody(bodies);
        GroupEnvelopeTrailer groupEnvelopeTrailer = new GroupEnvelopeTrailer();
        groupEnvelopeTrailer.setNumberOfTransactionSets(ONE_TRANSACTION_PER_EDI_MESSAGE);
        groupEnvelopeTrailer.setGroupControlNumber(groupEnvelopeHeader.getGroupControlNumber());
        pOAcknowledgementEdi.setGroupEnvelopeTrailer(groupEnvelopeTrailer);
        InterchangeEnvelopeTrailer interchangeEnvelopeTrailer = new InterchangeEnvelopeTrailer();
        interchangeEnvelopeTrailer.setInterchangeControlNumber(interchangeEnvelopeHeader.getInterchangeControlNumber());
        interchangeEnvelopeTrailer.setNumberOfIncludedGroups(ONE_TRANSACTION_PER_EDI_MESSAGE);
        pOAcknowledgementEdi.setInterchangeEnvelopeTrailer(interchangeEnvelopeTrailer);
        
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setBody(pOAcknowledgementEdi);
    }

    private List<AdditionalNameInformation> addNameInformationList(String name,
            List<AdditionalNameInformation> additionalNameInformationList) {
        AdditionalNameInformation additionalNameInformation1 = new AdditionalNameInformation();
        additionalNameInformation1.setName(OutboundEdiUtils.truncateAndRemoveDisallowedChars(name, 30));
        additionalNameInformationList.add(additionalNameInformation1);
        return additionalNameInformationList;
    }

    private String calculateNumberOfIncldedSegments(POAcknowledgementBody poAcknowledgementBody) {
        int numberOfSegments = 3;
        numberOfSegments = numberOfSegments + poAcknowledgementBody.getAckHeader().getReferenceIdentification().size();
        if (poAcknowledgementBody.getAckHeader().getPersonContact() != null) {
            numberOfSegments++;
        }
        for (ItemAcknowledgementGroup item : poAcknowledgementBody.getDetail().getItemAcknowledgements()) {
            numberOfSegments = numberOfSegments + 4
                    + item.getShipmentInformationGroup().getAdditionalNameInformation().size();
            if (item.getTermsOfSale() != null) {
                numberOfSegments = numberOfSegments + 1;
            }
            if (item.getShipmentInformationGroup().getAddressInformation() != null) {
                numberOfSegments++;
            }
            if (item.getShipmentInformationGroup().getReferenceNumber() != null) {
                numberOfSegments++;
            }
        }

        return Integer.toString(numberOfSegments);
    }
}
