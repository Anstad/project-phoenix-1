/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.drools.core.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.MessageMetadataBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.dto.AdvanceShipmentNoticeDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto;
import com.apd.phoenix.service.model.dto.FunctionalAcknowledgementDto.AcknowledgementCode;
import com.apd.phoenix.service.model.dto.NoteDto;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;
import com.apd.phoenix.service.model.dto.error.EdiDocumentErrorsDto;
import com.apd.phoenix.service.utility.ErrorEmailService;
import com.apd.phoenix.service.workflow.UserTaskService;
import com.apd.phoenix.service.workflow.WorkflowService;

/**
 *
 * @author nreidelb
 */
@Stateless
public class WorkflowServiceCaller {

    private static final Logger logger = LoggerFactory.getLogger(WorkflowServiceCaller.class);

    @Inject
    WorkflowService workflowService;

    @Inject
    private MessageMetadataBp messageMetadataBp;

    @Inject
    private EmailService emailService;

    @Inject
    private MessageService messageService;

    @Inject
    private MessageUtils messageUtils;

    @Inject
    ErrorEmailService errorEmailService;

    @Inject
    private UserTaskService userTaskService;

    public void processOrderPostValidation(@Header(value = "orderId") Long orderId,
            @Body PurchaseOrderDto purchaseOrderDto) {
        workflowService.processOrderAfterValidation(orderId, purchaseOrderDto);
    }

    public void processErrorEmail(@Body EdiDocumentErrorsDto errorDto) {
        errorEmailService.createErrorEmail(errorDto);
    }

    public void sendMarquetteErrorEmail(@Header(value = "exception") String exception, @Body Object body) {
        errorEmailService.sendMarquetteErrorEmail(new CustomerInvoiceDto(), exception);
    }

    public void processEdi864(@Header(value = "partnerId") String partnerId,
            @Header(value = "CamelFileName") String fileName, @Body String message) {
        if (partnerId == null) {
            partnerId = "";
        }
        errorEmailService.create864Email(message, partnerId, fileName);
    }

    public void processOrderAcknoweldgement(@Body POAcknowledgementDto poAcknowledgement) throws SystemException,
            NotSupportedException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
        workflowService.processOrderAcknowledgement(poAcknowledgement);
    }

    public void processAdvanceShipmentNotice(@Body AdvanceShipmentNoticeDto advanceShipmentNoticeDto)
            throws SystemException, NotSupportedException, RollbackException, HeuristicMixedException,
            HeuristicRollbackException {
        workflowService.processAdvancedShipmentNotice(advanceShipmentNoticeDto);
    }

    public void processMarfieldShipment(@Body ShipmentDto shipmentDto) throws SystemException, NotSupportedException,
            RollbackException, HeuristicMixedException, HeuristicRollbackException {
        workflowService.processMarfieldShipment(shipmentDto);
    }

    public void processInvoice(@Body VendorInvoiceDto invoice) throws SystemException, NotSupportedException,
            RollbackException, HeuristicMixedException, HeuristicRollbackException {
        if (invoice instanceof VendorCreditInvoiceDto) {
            workflowService.processVendorCredit((VendorCreditInvoiceDto) invoice);
        }
        else {
            workflowService.processInvoice(invoice);

        }

    }

    public void processCreditInvoice(@Body VendorCreditInvoiceDto vendorInvoiceDto) throws SystemException,
            NotSupportedException, RollbackException, HeuristicMixedException, HeuristicRollbackException {
        workflowService.processVendorCredit(vendorInvoiceDto);
    }

    public void denyOrder(@Body CustomerOrder order) {
        userTaskService.denyOrder(order);
    }

    public void processFunctionalAcknowledgement(@Body FunctionalAcknowledgementDto functionalAcknowledgementDto) throws IOException, MessagingException{
    	Properties commonProperties = PropertiesLoader.getAsProperties("camel.edi-inbound.integration");
		logger.debug("Processing functional Ack");
		String errorMessages = "";
		/*
		 * Functional Acknowledgements will either be identified by their groupId if the response was done per group or by function id 
		 * if the response was done on a more fine grained basis
		 */
		//Is this acknowledgement for the group level instead of the function level?
		boolean groupLevel = StringUtils.isEmpty(functionalAcknowledgementDto.getTransactionId());
		String transactionId = functionalAcknowledgementDto.getTransactionId();
		String groupId = functionalAcknowledgementDto.getGroupId();
		EventTypeDto eventType = functionalAcknowledgementDto.getEventType();
		AcknowledgementCode acknowledgementCode = functionalAcknowledgementDto
				.getAcknowledgementCode();
		String edi997APDSenderId = commonProperties.getProperty("edi997APDSenderId");
		List<CustomerOrder> customerOrders = messageMetadataBp
				.getCustomerOrdersPlacedInLastYearByTransaction(transactionId, edi997APDSenderId);
		boolean noOrder = (customerOrders == null || customerOrders.isEmpty());
		if(groupLevel){
			logger.debug("Processing FA Ack for groupId: {}", groupId);
		}else{
			if(noOrder){
				logger.error("Could not find customer order for transaction id: " + transactionId);
				errorMessages += "No order could be found which is assocated with the transaction " + transactionId + "<br />";			
			} else {
				for(CustomerOrder customerOrder:customerOrders){
					logger.debug("Processing FA Ack for order po: {}", customerOrder.getApdPo());
				}
			}
		}
		//No further processing for accepted messages
		if (acknowledgementCode == AcknowledgementCode.A) {
			return;
		}
		

		//Build the segment and note errors
		if(!functionalAcknowledgementDto.getNoteDtos().isEmpty()) {
			errorMessages += "The following errors were found:<br />";
		}
		for (NoteDto nd : functionalAcknowledgementDto.getNoteDtos()) {
			if (nd.getSegmentNoteDto() != null) {
				errorMessages += "<h4>Segment Information</h4>";
			}
			if (nd.getSegmentNoteDto().getSegmentId() != null) {
				errorMessages += "<strong>Segment ID code: </strong>"
						+ nd.getSegmentNoteDto().getSegmentId() + "<br />";
			}
			if (nd.getSegmentNoteDto().getPosition() != null) {
				errorMessages += "<strong>Segment position in transaction: </strong>"
						+ nd.getSegmentNoteDto().getPosition() + "<br />";
			}
			if (nd.getSegmentNoteDto().getLoopIndentifier() != null) {
				errorMessages += "<strong>Loop identifier code: </strong>"
						+ nd.getSegmentNoteDto().getLoopIndentifier()
						+ "<br />";
			}
			if (nd.getSegmentNoteDto().getSyntaxErrorCode() != null) {
				errorMessages += "<strong>Segment syntax error code: </strong>"
						+ nd.getSegmentNoteDto().getSyntaxErrorCode()
						+ "<br />";
			}
			if (nd.getSegmentNoteDto().getSyntaxErrorDescription() != null) {
				errorMessages += "<strong>Error code description: </strong>"
						+ nd.getSegmentNoteDto().getSyntaxErrorDescription()
						+ "<br />";
			}
			if (nd.getElementNoteDto() != null) {
				errorMessages += "<br /><h4>Element Information</h4>";
			}
			if (nd.getElementNoteDto().getPosition() != null) {
				errorMessages += "<strong>Position in segment: </strong>"
						+ nd.getElementNoteDto().getPosition() + "<br />";
			}
			if (nd.getElementNoteDto().getReferenceNumber() != null) {
				errorMessages += "<strong>Data element reference number: </strong>"
						+ nd.getElementNoteDto().getReferenceNumber()
						+ "<br />";
			}
			if (nd.getElementNoteDto().getSyntaxErrorCode() != null) {
				errorMessages += "<strong>Syntax error code: </strong>"
						+ nd.getElementNoteDto().getSyntaxErrorCode()
						+ "<br />";
			}
			if (nd.getElementNoteDto().getSyntaxErrorDescription() != null) {
				errorMessages += "<strong>Error code description: </strong>"
						+ nd.getElementNoteDto().getSyntaxErrorDescription()
						+ "<br />";
			}
			if (nd.getElementNoteDto().getCopyOfBadDataElement() != null) {
				errorMessages += "<strong>Copy of bad data element: </strong>"
						+ nd.getElementNoteDto().getCopyOfBadDataElement()
						+ "<br />";
			}
			errorMessages += "---------------------------------------";

		}

		//Build email body
		StringTemplate contentTemplate = new StringTemplate(
				"<h1>Functional Acknowledgement Error Notification</h1>"
						+ "<h3>APD PO: $apdPo$</h3>"
						+ (groupLevel ? "<h3>Transaction ID: $transactionId$</h3>": "<h3>Group ID: $groupId$</h3>") 
						+ "<h3>Transaction Type: $transactionType$</h3>"
						+ "<h3>Status: $status$</h3>"
						+ "<p>$errorMessages$</p>");
		if(noOrder){
			contentTemplate.setAttribute("apdPo", "No customer order found");
		} else {
			if(customerOrders.size() == 1){
				contentTemplate.setAttribute("apdPo", customerOrders.get(0).getApdPo().getValue());
			} else {
				String errorPos = "one of the following POs ";
				for(CustomerOrder customerOrder:customerOrders){
					errorPos = errorPos + customerOrder.getApdPo().getValue() + ", ";
				}
				contentTemplate.setAttribute("apdPo", errorPos);
			}
		}
		if(groupLevel){
			contentTemplate.setAttribute("groupId", groupId);
		}else {
			contentTemplate.setAttribute("transactionId", transactionId);
		}
		contentTemplate.setAttribute("transactionType", eventType.getLabel());
		contentTemplate.setAttribute("status", acknowledgementCode.getLabel());
		contentTemplate.setAttribute("errorMessages", errorMessages);

		String emailContent = contentTemplate.toString();

		logger.debug("Email body: {}", emailContent);
		String subject = "Functional Acknowledgement Error Notification";

		Set<String> recipients = new HashSet<>();
		String apdNotificationEmails = commonProperties.getProperty("edi997NotificationToEmails");
		String[] recipientList = apdNotificationEmails.split(",");
		for (String recipient : recipientList) {
			recipients.add(recipient);
		}

		String adminEmail = commonProperties.getProperty("edi997FromEmail");;
		MimeMessage message = emailService.prepareRawMessage(adminEmail,
				null, null, recipients, null, subject, emailContent, null);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		message.writeTo(bos);
		byte[] mimeMessage = bos.toByteArray();
                try(ByteArrayInputStream ips = new ByteArrayInputStream(mimeMessage)){
		Message messageToSend = messageUtils.createMessage(noOrder?null:customerOrders.get(0), ips, mimeMessage.length,
				MessageMetadata.MessageType.EMAIL, apdNotificationEmails, null, null);
		messageService.sendMessage(messageToSend, EventType.TECH_NOTIFICATION);
                } catch (IOException ex) {
                  logger.error("Could not read mimeMessage", ex);
                }
    }
}
