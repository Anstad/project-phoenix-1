package com.apd.phoenix.service.integration.tax.military.model;

import java.io.Serializable;
import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(length = 88, paddingChar = ' ')
public class MilitaryZipBindyDto implements Serializable {

    private static final long serialVersionUID = 7522925547462926089L;

    private Long id = null;

    private int version = 0;

    @DataField(pos = 1, length = 5)
    private String zip;

    @DataField(pos = 6, length = 28, trim = true)
    private String city;

    @DataField(pos = 34, length = 25, trim = true)
    private String county;

    @DataField(pos = 59, length = 2, trim = true)
    private String state;

    @DataField(pos = 61, length = 28, trim = true)
    private String uspsCity;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(final int version) {
        this.version = version;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUspsCity() {
        return uspsCity;
    }

    public void setUspsCity(String uspsCity) {
        this.uspsCity = uspsCity;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        if (id != null) {
            return id.equals(((MilitaryZipBindyDto) that).id);
        }
        return super.equals(that);
    }

    @Override
    public int hashCode() {
        if (id != null) {
            return id.hashCode();
        }
        return super.hashCode();
    }
}
