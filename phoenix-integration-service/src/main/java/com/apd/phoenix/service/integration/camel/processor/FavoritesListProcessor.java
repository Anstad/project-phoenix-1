package com.apd.phoenix.service.integration.camel.processor;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.FavoritesListBp;

@Stateless
public class FavoritesListProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoritesListProcessor.class);

    @Inject
    private FavoritesListBp listBp;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void process(Long catalogId) {
        listBp.removeEmptyCompanyLists(catalogId);
    }
}
