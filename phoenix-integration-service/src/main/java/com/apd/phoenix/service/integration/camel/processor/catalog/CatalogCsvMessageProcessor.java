package com.apd.phoenix.service.integration.camel.processor.catalog;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CatalogCsvBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.SyncItemResultBp;
import com.apd.phoenix.service.catalog.CatalogCsvMessage;
import com.apd.phoenix.service.catalog.CatalogCsvResult;
import com.apd.phoenix.service.catalog.CatalogCsvResultEntryPoint;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.SyncItemResult;

@Stateless
public class CatalogCsvMessageProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogCsvMessageProcessor.class);

    @Inject
    CatalogCsvBp catalogCsvBp;

    @Inject
    ItemBp itemBp;

    @Inject
    CatalogCsvResultEntryPoint catalogCsvEntryPoint;

    @Inject
    CatalogXItemBp catalogXItemBp;

    @Inject
    private SyncItemResultBp syncItemResultBp;

    public void process(CatalogCsvMessage message) {
        ArrayList<String> rows = new ArrayList<>(message.getIds().size());
        CatalogCsvResult result = new CatalogCsvResult();
        //first, pricing is calculated for each item
        ArrayList<Long> ids = message.getIds();
       
    	switch (message.getCatalogCsvType()) {
        	case VENDOR_CATALOG :
        		for (Long id : ids) {
            		Item item = itemBp.findById(id, Item.class);
            		try {
                		itemBp.calculateStreetPrice(item);
            		}
            		catch (Exception e) {
            			LOGGER.warn("Unable to calculate street price for vendor item " + item.getId());
            			if (LOGGER.isDebugEnabled()) {
            				LOGGER.debug("Stack trace", e);
            			}
            		}
        		}
        		break;
        	case SMART_OCI_CATALOG_CSV :
        	case CUSTOMER_CATALOG_CSV :
        		for (Long id : ids) {
            		CatalogXItem catalogXItem = catalogXItemBp.findById(id, CatalogXItem.class);
            		try {
            			catalogXItemBp.calculatePrice(catalogXItem);
            		}
            		catch (Exception e) {
            			LOGGER.warn("Unable to calculate price for customer item " + catalogXItem.getId());
            		}
        		}
        	case REPORT_CSV :
        		break;
    	}
        
        //then, in a separate transaction, the item is pulled for the CSV
        try {
            switch (message.getCatalogCsvType()) {
        	case VENDOR_CATALOG :
        		for (Long id : ids) {
            		Item item = itemBp.findById(id, Item.class);
            		rows.add(itemBp.csvRow(item, message.getPropertyTypeList(), message.getSkuTypeList(), message.getCustomerList()));
        		}
        		break;
        	case SMART_OCI_CATALOG_CSV :
        		LOGGER.info("Creating Batch Rows For Smart OCI");
        		for (Long id : ids) {
            		CatalogXItem catalogXItem = catalogXItemBp.findById(id, CatalogXItem.class);
            		rows.add(catalogXItemBp.smartOciCsvRow(catalogXItem, message.getPropertyTypeList()));
        		}
        		break;
        	case CUSTOMER_CATALOG_CSV :        		 
        		for (Long id : ids) {
            		CatalogXItem catalogXItem = catalogXItemBp.findById(id, CatalogXItem.class);
            		rows.add(catalogXItemBp.csvRow(catalogXItem, message.getPropertyTypeList()));
        		}
        		break;
        	case REPORT_CSV :
        		for (Long id : ids) {
        			SyncItemResult toWrite = syncItemResultBp.findById(id, SyncItemResult.class);
            		rows.add(syncItemResultBp.csvRow(toWrite));
        		}
        		break;
            }
        }
        catch (Exception e) {
        	//occurs when there is a JDBC error with itemBp, catalogXItemBp, or an unexpected error
            LOGGER.error("Exception while trying to process CSV rows", e);
        }
        //pads the CSV with the required additional rows, if processing failed
        while (rows.size() < message.getIds().size()) {
        	rows.add("Could not generate CSV row for an item");
        }
        
        result.setCorrelationId(message.getCorrelationId());
        result.setRows(rows);
        result.setHeader(message.getHeader());
        result.setCatalogId(message.getCatalogId());
        result.setTotalItems(message.getTotalItems());
        result.setCatalogCsvType(message.getCatalogCsvType());
	                    
        catalogCsvEntryPoint.scheduleRequest(result);
    }
}
