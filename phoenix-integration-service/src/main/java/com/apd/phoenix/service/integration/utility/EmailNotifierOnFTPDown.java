package com.apd.phoenix.service.integration.utility;

import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.utility.ErrorEmailService;

public class EmailNotifierOnFTPDown {

    private static final Logger LOG = LoggerFactory.getLogger(EmailNotifierOnFTPDown.class);

    @Inject
    EmailService emailService;

    @Inject
    ErrorEmailService errorEmailService;

    public void as2ServerDownEmailNotify(@Header("messageDestination") String messageDestination, @Header("apdPo") String apdPo) {
		 LOG.info("AS2 server is down");
		 
		 Properties commonProperties = PropertiesLoader.getAsProperties("ussco.integration");
		 String from  = commonProperties.getProperty("emailNofityOnAs2DownFrom");
		 String to  = commonProperties.getProperty("emailNofityOnAs2DownTo");
		 
		 String subject = "Warning:AS2 remote drive not responding";
		 String emailBody = "<h1>AS2 remote drive not responding</h1>"
	        		+ "<p>Failed to connect AS2 server on"+new Date()+ " with message destination=</p>"+ messageDestination
	        		+"  <p>and apdPO="+apdPo+"</p>";
		 
	        		
	    Set<String> recipients = new HashSet<>();
		recipients.add(to);		
		
		MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, null, subject, emailBody, null);
		
		emailService.sendEmail(message);
	 }

    public void uspsFTPServerDownEmailNotify(@Header("destination") String messageDestination, @Header("apdPo") String apdPo) {
		 LOG.info("USPS FTP server failed to connect");
		 
		 Properties commonProperties = PropertiesLoader.getAsProperties("ussco.integration");
		 String from  = commonProperties.getProperty("emailNofityOnAs2DownFrom");
		 String to  = commonProperties.getProperty("emailNofityOnAs2DownTo");
		 
		 String subject = "Warning:USPS FTP not responding";
		 String emailBody = "<h1>USPS FTP not responding</h1>"
	        		+ "<p>Failed to connect USPS FTP server on"+new Date()+ " with message destination=</p>"+ messageDestination
	        		+"  <p>and apdPO="+apdPo+"</p>";		 
	        		
	    Set<String> recipients = new HashSet<>();
		recipients.add(to);		
		
		MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, null, subject, emailBody, null);
		
		emailService.sendEmail(message);
	 }}
