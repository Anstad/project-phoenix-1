package com.apd.phoenix.service.integration.routes.xcbl;

import javax.xml.bind.JAXBContext;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import com.apd.phoenix.service.integration.camel.processor.xcbl.OrderResponseEnricher;
import com.apd.phoenix.service.integration.camel.processor.xcbl.XCBLTransactionLogger;
import com.apd.phoenix.service.integration.camel.processor.xcbl.osn.OSNCustomJaxbMarshaller;
import com.apd.phoenix.service.integration.camel.processor.xcbl.osn.OSNOrderResponseTranslator;
import com.apd.phoenix.service.integration.exception.XCBLValidationException;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

/**
 * Note:
 * - The class resolved from outboundOrderResponseTranslatorClass MUST implement com.apd.phoenix.service.integration.camel.processor.MessageTranslator
 * - The class resolved from outboundOrderResponseDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * - The class resolved from inboundMessageAckDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * - The class resolved from inboundOrderDataFormatClass MUST yield a valid JAXBContext when passed into javax.xml.bind.JAXBContext.newInstance
 * @author RH
 *
 */
public class XCBLOrderResponseRouteBuilder extends RouteBuilder {

    private String outboundOrderResponseSource;
    //private String outboundOrderResponseTranslatorClass;
    private String outboundOrderResponseDataFormatClass;
    private String inboundOrderDataFormatClass;
    private String inboundMessageAckDataFormatClass;
    private String outboundOrderResponseOutput = "http://component";
    private String outboundOrderResponseLogSource;
    private String inboundMessageAckLogSource;

    private String outputDLQ;

    @Override
    public void configure() throws Exception {

        JaxbDataFormat orderDataFormat = new JaxbDataFormat();
        JAXBContext orderJaxbContext = JAXBContext.newInstance(Class.forName(inboundOrderDataFormatClass));
        orderDataFormat.setContext(orderJaxbContext);

        JaxbDataFormat orderResponseDataFormat = new JaxbDataFormat();
        JAXBContext orderResponseJaxbContext = JAXBContext.newInstance(Class
                .forName(outboundOrderResponseDataFormatClass));
        orderResponseDataFormat.setContext(orderResponseJaxbContext);
        //        TODO: Remove after testing this implimentation in DEV
        //        JaxbDataFormat messageAckDataFormat = new JaxbDataFormat();
        //        JAXBContext messageAckJaxbContext = JAXBContext.newInstance(Class.forName(inboundMessageAckDataFormatClass));
        //        messageAckDataFormat.setContext(messageAckJaxbContext);

        onException(Exception.class).maximumRedeliveries(5).redeliveryDelay(2000).useExponentialBackOff()
                .backOffMultiplier(2).handled(true).log(LoggingLevel.INFO,
                        "Error in xcbl processing: ${exception.stacktrace}");

        onException(XCBLValidationException.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "XCBL validation exception");

        //Receive DTO
        from(outboundOrderResponseSource)
        //Setup DLQ
                .errorHandler(
                        deadLetterChannel(outputDLQ).maximumRedeliveries(2).redeliveryDelay(2000)
                                .useExponentialBackOff().backOffMultiplier(2))
                //Prepare for endpoint
                .setHeader(Exchange.HTTP_URI, simple("${header.destination}")).setHeader("apdPo").jxpath(
                        "/in/body/apdPo")
                //Retrieve the original order document
                .bean(OrderResponseEnricher.class, "process")
                //Unmarshal the original xcbl order xml document to an xcbl order pojo
                .unmarshal(orderDataFormat)
                //Convert xcbl order pojo to xcbl order response pojo enriched further by poAckDto
                .bean(OSNOrderResponseTranslator.class, "process")
                //marshal using custom class to include namespace mapping
                .bean(OSNCustomJaxbMarshaller.class, "marshal")
                //Log
                .wireTap(outboundOrderResponseLogSource)
                //Send to external endpoint
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                //Set content type
                .setHeader(Exchange.CONTENT_TYPE, constant("text/xml"))
                //Post to partner
                //Log
                .log(LoggingLevel.DEBUG, XCBLOrderResponseRouteBuilder.class.getName(), "post request: ${body}")
                //Send to external endpoint
                .to(outboundOrderResponseOutput)
                //convert from stream to string
                .convertBodyTo(String.class)
                //Log
                .log(LoggingLevel.DEBUG, XCBLOrderResponseRouteBuilder.class.getName(), "post response: ${body}")
                //Response handler
                .wireTap(inboundMessageAckLogSource);

        from(outboundOrderResponseLogSource)
                .setHeader("messageEventType", constant(EventTypeDto.ORDER_ACKNOWLEDGEMENT)).setHeader(
                        "communicationType", constant(CommunicationType.OUTBOUND)).bean(XCBLTransactionLogger.class,
                        "logTransaction");

        from(inboundMessageAckLogSource).setHeader("messageEventType", constant(EventTypeDto.ORDER_ACKNOWLEDGEMENT))
                .setHeader("communicationType", constant(CommunicationType.INBOUND)).bean(XCBLTransactionLogger.class,
                        "logTransaction");
    }

    public String getOutboundOrderResponseSource() {
        return outboundOrderResponseSource;
    }

    public void setOutboundOrderResponseSource(String outboundOrderResponseSource) {
        this.outboundOrderResponseSource = outboundOrderResponseSource;
    }

    public String getOutboundOrderResponseDataFormatClass() {
        return outboundOrderResponseDataFormatClass;
    }

    public void setOutboundOrderResponseDataFormatClass(String outboundOrderResponseDataFormatClass) {
        this.outboundOrderResponseDataFormatClass = outboundOrderResponseDataFormatClass;
    }

    public String getInboundOrderDataFormatClass() {
        return inboundOrderDataFormatClass;
    }

    public void setInboundOrderDataFormatClass(String inboundOrderDataFormatClass) {
        this.inboundOrderDataFormatClass = inboundOrderDataFormatClass;
    }

    public String getInboundMessageAckDataFormatClass() {
        return inboundMessageAckDataFormatClass;
    }

    public void setInboundMessageAckDataFormatClass(String inboundMessageAckDataFormatClass) {
        this.inboundMessageAckDataFormatClass = inboundMessageAckDataFormatClass;
    }

    public String getOutboundOrderResponseOutput() {
        return outboundOrderResponseOutput;
    }

    public void setOutboundOrderResponseOutput(String outboundOrderResponseOutput) {
        this.outboundOrderResponseOutput = outboundOrderResponseOutput;
    }

    public String getOutboundOrderResponseLogSource() {
        return outboundOrderResponseLogSource;
    }

    public void setOutboundOrderResponseLogSource(String outboundOrderResponseLogSource) {
        this.outboundOrderResponseLogSource = outboundOrderResponseLogSource;
    }

    public String getInboundMessageAckLogSource() {
        return inboundMessageAckLogSource;
    }

    public void setInboundMessageAckLogSource(String inboundMessageAckLogSource) {
        this.inboundMessageAckLogSource = inboundMessageAckLogSource;
    }

    public String getOutputDLQ() {
        return outputDLQ;
    }

    public void setOutputDLQ(String outputDLQ) {
        this.outputDLQ = outputDLQ;
    }
}
