package com.apd.phoenix.service.integration.camel.converter;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.TypeConversionException;
import org.apache.camel.support.TypeConverterSupport;
import com.apd.phoenix.service.integration.tax.zip.model.ZipPlusFourBindyDto;

public class ZipPlusFourToListTypeConverter extends TypeConverterSupport {

    @SuppressWarnings("unchecked")
	@Override
	public <T> T convertTo(Class<T> type, Exchange exchange, Object value)
			throws TypeConversionException {
		List<ZipPlusFourBindyDto> rates = new ArrayList<>();
		rates.add((ZipPlusFourBindyDto) value);
		return (T) rates;
	}
}
