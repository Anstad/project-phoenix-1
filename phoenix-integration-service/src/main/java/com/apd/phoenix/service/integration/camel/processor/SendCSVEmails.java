package com.apd.phoenix.service.integration.camel.processor;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.camel.Exchange;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.email.api.Attachment;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.email.api.Attachment.MimeType;

@Stateless
public class SendCSVEmails {

    private static final String FROM = "no-reply@apdmarketplace.com";

    @Inject
    EmailService emailService;

    public void sendInternalEmails(Exchange exchange){
		exchange.setOut(exchange.getIn());
		Set<String> to = new HashSet<>();
		Properties emailOverride = PropertiesLoader.getAsProperties("email.override");
		if(Boolean.TRUE.toString().equals(emailOverride.get("override"))){
			to.add(emailOverride.getProperty("email"));
		}else{
			to.add("evadinion@americanproduct.com");
			to.add("juanitarichardson@americanproduct.com");
			to.add("acctsreceivable@americanproduct.com");
		};
		List<Attachment> attachments = new ArrayList<>();
		Attachment attachment = new Attachment();
		attachment.setFileName((String) exchange.getIn().getHeader(Exchange.FILE_NAME));
		String csvContent = (String) exchange.getIn().getBody();
		attachment.setContent(new ByteArrayInputStream(csvContent.getBytes(StandardCharsets.UTF_8)));
		attachment.setMimeType(MimeType.csv);
		attachments.add(attachment);
		emailService.sendEmail(FROM, to , "Marquette CSV Document" , "Marquette CSV Document Attatched.", attachments);
		
	}
}
