package com.apd.phoenix.service.integration.manifest.processor;

import org.apache.camel.Body;
import com.apd.phoenix.service.dataformat.xml.request.Request;
import com.apd.phoenix.service.dataformat.xml.request.Request.Report;
import com.apd.phoenix.service.dataformat.xml.request.Request.Report.StopNotification;
import com.apd.phoenix.service.model.dto.StopNotificationRequestDto;

public class JumptrackStopNoticeTranslator {

    public static final String JUMPTRACK_FILE_TYPE = "jumptrack-api-xml";
    public static final Float JUMPTRACK_VERSION = 1.3f;

    public Request translateRequestDtoToXml(@Body StopNotificationRequestDto stopNotificationRequestDto) {
        Request request = new Request();
        request.setType(JUMPTRACK_FILE_TYPE);
        request.setVersion(JUMPTRACK_VERSION);
        Report report = new Report();
        StopNotification stopNotification = new StopNotification();
        stopNotification.setEncodeImage(new Boolean(stopNotificationRequestDto.isEncodeImage()).toString());
        stopNotification.setMax(stopNotificationRequestDto.getMax().byteValue());
        report.setStopNotification(stopNotification);
        request.setReport(report);

        return request;
    }

}
