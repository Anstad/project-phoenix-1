package com.apd.phoenix.service.integration.utility;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.apache.camel.RoutesBuilder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.routes.QueueCatchallRouteBuilder;

public class RouterBeanUtils {

    private static final Logger LOG = LoggerFactory.getLogger(RouterBeanUtils.class);

    private static final String CAMEL_ACTIVEMQ_ENDPOINT_START = "activemq:queue:queue.";

    public static QueueCatchallRouteBuilder createCatchallRouteBuilder(String selector, String queue) {
        QueueCatchallRouteBuilder queueCatchallRouteBuilder = new QueueCatchallRouteBuilder();
        queueCatchallRouteBuilder.setDlq(CAMEL_ACTIVEMQ_ENDPOINT_START + "outbound-bad-partner-id-dlq");
        try {
            selector = URLEncoder.encode(selector, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            LOG.error(e.getLocalizedMessage());
        }
        if (!StringUtils.isEmpty(selector)) {
            queue = queue + "?selector=" + selector;
        }
        queueCatchallRouteBuilder.setQueue(CAMEL_ACTIVEMQ_ENDPOINT_START + queue);
        return queueCatchallRouteBuilder;
    }

    private static String buildNotUsedPartnerIdSelector(String[] partnerList) {
        if (partnerList.length > 0) {
            StringBuffer selector = new StringBuffer();
            for (String partner : partnerList) {
                if (selector.length() > 0) {
                    selector = selector.append(" AND ");
                }
                selector.append("NOT (partnerId='" + partner + "')");
            }
            return selector.toString();
        }
        else {
            return "";
        }
    }

    public static RoutesBuilder createCatchallRouteBuilder(String[] edi850PartnerList, String ediOutbound850) {
        return createCatchallRouteBuilder(buildNotUsedPartnerIdSelector(edi850PartnerList), ediOutbound850);
    }
}
