package com.apd.phoenix.service.integration.camel.processor;

import java.util.Date;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.CommunicationType;
import com.apd.phoenix.service.model.dto.MessageMetadataDto.MessageType;
import com.apd.phoenix.service.model.dto.OrderLogDto.EventTypeDto;

@Stateless
public class CSVTransactionLogger {

    @Inject
    MessageService messageService;

    private static final Logger LOG = LoggerFactory.getLogger(CSVTransactionLogger.class);

    public void logEDITransaction(@Header("apdPo") String apdPo,
            @Header("communicationType") CommunicationType communicationType,
            @Header("destination") String destination, @Header("messageEventType") EventTypeDto messageEventType,
            @Header("senderId") String senderId, @Body String body) throws NotSupportedException, SystemException,
            SecurityException, IllegalStateException, RollbackException, HeuristicMixedException,
            HeuristicRollbackException {
        LOG.debug("apdPo: {}", apdPo);
        LOG.debug("body: {}", body);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(communicationType);
        metadata.setContentLength(body.length());
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-csv-" + System.currentTimeMillis() + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageType.CSV);
        ;
        metadata.setSenderId(senderId);
        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(body);

        messageService.receiveOrderMessage(message, apdPo, messageEventType.name());
    }
}
