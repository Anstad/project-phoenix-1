/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.camel.processor;

import com.apd.phoenix.service.model.dto.MessageDto;
import com.apd.phoenix.service.model.dto.MessageMetadataDto;
import com.apd.phoenix.service.model.dto.OrderLogDto;
import java.util.Date;
import javax.edi.model.x12.edi810.segment.InvoiceBody;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author nreidelb
 */
public class EDI810CreditToOrderLogTranslator implements Processor {

    private static final Logger LOG = LoggerFactory.getLogger(EDI810CreditToOrderLogTranslator.class);

    @Override
    public void process(Exchange exchange) throws Exception {
        String rawMessage = (String) exchange.getIn().getHeader("rawMessage");
        LOG.debug("Raw Message: {}", rawMessage);

        String destination = (String) exchange.getIn().getHeader("CamelFileHost");
        if (destination == null) {
            //TODO: Configure how ftp host is extracted from headers
            destination = "none";
        }
        InvoiceBody body = (InvoiceBody) exchange.getIn().getBody();
        String apdPo = body.getHeader().getBeginningSegmentforInvoice().getPurchaseOrderNumber();
        LOG.debug("po number: {}", apdPo);

        MessageMetadataDto metadata = new MessageMetadataDto();
        metadata.setCommunicationType(MessageMetadataDto.CommunicationType.INBOUND);
        metadata.setContentLength(rawMessage.length());
        metadata.setDestination(destination);
        metadata.setFilePath(apdPo + "/" + apdPo + "-edi810-" + System.currentTimeMillis() + ".txt");
        metadata.setMessageDate(new Date());
        metadata.setMessageType(MessageMetadataDto.MessageType.EDI);
        String groupId = (String) exchange.getIn().getHeader("groupId");
        metadata.setGroupId(groupId);
        String interchangeId = (String) exchange.getIn().getHeader("interchangeId");
        metadata.setInterchangeId(interchangeId);
        String senderId = (String) exchange.getIn().getHeader("senderId");
        metadata.setSenderId(senderId);
        String transactionId = (String) exchange.getIn().getHeader("transactionId");
        metadata.setTransactionId(transactionId);

        MessageDto message = new MessageDto();
        message.setMessageMetadataDto(metadata);
        message.setContent(rawMessage);
        exchange.getOut().setBody(message);
        exchange.getOut().setHeaders(exchange.getIn().getHeaders());
        exchange.getOut().setHeader("apdPo", apdPo);
        exchange.getOut().setHeader("messageEventType", OrderLogDto.EventTypeDto.RETURNED);
    }
}
