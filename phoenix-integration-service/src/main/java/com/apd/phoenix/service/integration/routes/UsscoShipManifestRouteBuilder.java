package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.manifest.processor.UsscoShipManifestProcessor;
import com.apd.phoenix.service.integration.manifest.ussco.model.UsscoManifestCSVDto;

public class UsscoShipManifestRouteBuilder extends RouteBuilder {

    private String source;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.DEBUG,
                "Error in ussco manifest processing: ${exception.stacktrace}")
        //Sedn error notification
                .setBody(simple("${exception.stacktrace}")).bean(UsscoShipManifestProcessor.class,
                        "processUsscoManifestException");

        from(source)
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).setHeader("rawMessage", body())
        //Unmarshal csv
                .unmarshal().bindy(BindyType.Csv, UsscoManifestCSVDto.class)
                //Process manifests
                .bean(UsscoShipManifestProcessor.class, "processUsscoManifest");
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

}
