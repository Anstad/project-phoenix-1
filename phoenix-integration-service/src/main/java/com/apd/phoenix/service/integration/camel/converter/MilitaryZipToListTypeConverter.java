package com.apd.phoenix.service.integration.camel.converter;

import java.util.ArrayList;
import java.util.List;
import org.apache.camel.Exchange;
import org.apache.camel.TypeConversionException;
import org.apache.camel.support.TypeConverterSupport;
import com.apd.phoenix.service.integration.tax.military.model.MilitaryZipBindyDto;

public class MilitaryZipToListTypeConverter extends TypeConverterSupport {

    @SuppressWarnings("unchecked")
	@Override
	public <T> T convertTo(Class<T> type, Exchange exchange, Object value)
			throws TypeConversionException {
		List<MilitaryZipBindyDto> rates = new ArrayList<>();
		rates.add((MilitaryZipBindyDto) value);
		return (T) rates;
	}
}
