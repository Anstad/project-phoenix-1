package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.marfield.processor.MarfieldShipmentFileConverter;
import com.apd.phoenix.service.integration.utility.EDIOrderUtils;

public class FlatFileEntryPointRouteBuilder extends RouteBuilder {

    private String transactionSource;
    private String partnerId;
    private String processorEndpoint;
    private Integer startOrderOffset;

    @Override
    public void configure() throws Exception {
        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in edi entry point processing: ${exception.stacktrace}");

        from(getTransactionSource())
        /*
         * delays picking up more messages to deploy until everything is deployed to avoid 
         * transaction timeouts while the system is under load from deployment
         */
        .autoStartup(false).routePolicy(DelayBean.getDelayPolicy()).startupOrder(getStartOrderOffset()).setHeader(
                "partnerId", simple(getPartnerId())).bean(MarfieldShipmentFileConverter.class, "convertToString").to(
                getProcessorEndpoint());
    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getProcessorEndpoint() {
        return processorEndpoint;
    }

    public void setProcessorEndpoint(String processorEndpoint) {
        this.processorEndpoint = processorEndpoint;
    }

    public Integer getStartOrderOffset() {
        return startOrderOffset;
    }

    public void setStartOrderOffset(Integer startOrderOffset) {
        this.startOrderOffset = startOrderOffset;
    }

}
