/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.integration.utility;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Camel ordering numbers must be unique, the order of the enums in this class will 
 * determine the offset used to set the order in which the camel routes are deployed.
 * @author nreidelb
 */
public class EDIOrderUtils {

    private static final String SEPARATE_FILE_BY_DATE = "&fileName=${date:now:yyyy-MM-dd}/${header.CamelFileName}";
    private static final Logger LOG = LoggerFactory.getLogger(EDIOrderUtils.class);
    public static final String PATH_TO_LOCATION_FOR_BACKUP_FILES = "/backup";
    public static final String END_OF_PATH_DELIMITER = "?";

    public static String createBackupSendEndpoint(String endpoint) {
        String newEndpoint = endpoint.substring(0, endpoint.indexOf(END_OF_PATH_DELIMITER))
                + PATH_TO_LOCATION_FOR_BACKUP_FILES
                + endpoint.substring(endpoint.indexOf(END_OF_PATH_DELIMITER), endpoint.length())
                + SEPARATE_FILE_BY_DATE;
        return newEndpoint;
    }

    public static int ediSegmentCompartor(String arg0, String arg1) {
        Integer lineNumber0 = stringToIntOrNull(arg0);
        Integer lineNumber1 = stringToIntOrNull(arg1);
        if (lineNumber0 == null) {
            return 1;
        }
        if (lineNumber1 == null) {
            return 0;
        }
        return lineNumber0.compareTo(lineNumber1);
    }

    private static Integer stringToIntOrNull(String assignedIdentificationNumber0) {
        if (assignedIdentificationNumber0 != null) {
            try {
                return new Integer(assignedIdentificationNumber0);
            }
            catch (NumberFormatException e) {
                return null;
            }
        }
        return null;
    }

    /**
     * Gets the place in the list of enum constants eg; enum Day{MON,TUES};
     * getOffset(MON) would return 1; getOffset(TUES) would return 2
     **/
    public static <T extends Enum<T>> Integer getOffset(T routeOrderOffset) {
        Integer toReturn = 1;
        for (Enum constant : routeOrderOffset.getClass().getEnumConstants()) {
            if (constant.name().equals(routeOrderOffset.name())) {
                return toReturn;
            }
            else {
                toReturn++;
            }
        }
        //impossible branch, should throw NPE if hit
        LOG.error("Error getting offset for RouteOrderOffsetsInboundEdi, " + routeOrderOffset);
        return null;
    }

    public static Integer getTotalOffset(Class offsetEnum) {
        return offsetEnum.getEnumConstants().length;
    }

    //Order in which each direct rote should be deployed depends on the order of this document
    public enum RouteOrderOffsetInboundEDI810 {
        INBOUND_810_LOG, INBOUND_810_CREDIT_LOG, INBOUND_810_CREDIT_FORMATTED_SOURCE, INBOUND_810_INVOICE_FORMATTED_SOURCE, INBOUND_810_DIFFERNET_CREDIT, INBOUND_810_STANDARD_INVOICE, INBOUND_810_CREDIT_INVOICE, INBOUND_810,
    }

    public enum RouteOrderOffsetInboundEDI816 {
        INBOUND_816_FORMATTED, INBOUND_816
    }

    public enum RouteOrderOffsetInboundEDI824 {
        INBOUND_824_FORMATTED, INBOUND_824_LOG, INBOUND_824
    }

    public enum RouteOrderOffsetInboundEDI850v4010 {
        INBOUND_850v4010_FA_PREPARE, INBOUND_850v4010_FA_SOURCE, INBOUND_850v4010_LOG, INBOUND_850v4010_FORMAT_SOURCE, INBOUND_850v4010,
    }

    public enum RouteOrderOffsetInboundEDI850v5010 {
        INBOUND_850v5010_FA_PREPARE, INBOUND_850v5010_FA_SOURCE, INBOUND_850v5010_LOG, INBOUND_850v5010_FORMAT_SOURCE, INBOUND_850v5010
    }

    public enum RouteOrderOffsetInboundEDI856 {
        INBOUND_856_FORMATTED, INBOUND_856_LOG, INBOUND_856,
    }

    public enum RouteOrderOffsetInboundEDI855 {
        INBOUND_855_FA_PREPARE, INBOUND_855_FA_SOURCE, INBOUND_855_ORDER_ACK_FORMATTED, INBOUND_855_LOG, INBOUND_855,
    }

    public enum RouteOrderOffsetInboundEDI997 {
        INBOUND_997_RESEND, INBOUND_887_OUTBOUND, INBOUND_997_FORMATTED, INBOUND_997_LOG, INBOUND_997,
    }

    public enum RouteOrderOffsetInboundEDI {
        ENTRY_POINT;
    }

    //Order in which each direct rote should be deployed depends on the order of this document
    public enum RouteOrderOffsetOutboundEDI850 {

        OUTBOUND_850_RESEND, OUTBOUND_850_LOG, AS2_DOWN_NOTIFY, OUTBOUND_850_OUT, OUTBOUND_850,
    }

    public enum RouteOrderOffsetOutboundEDI810 {

        FTP_DOWN_810, OUTBOUND_LOG_810, OUTBOUND_810,
    }

    public enum RouteOrderOffsetOutboundEDI810v5010 {

        OUTBOUND_LOG_810v5010, OUTBOUND_810v5010,
    }

    public enum RouteOrderOffsetOutboundEDI855 {

        FTP_DOWN_855, OUTBOUND_LOG_855, OUTBOUND_855,
    }

    public enum RouteOrderOffsetOutboundEDI856 {

        FTP_DOWN_856, OUTBOUND_LOG_856, OUTBOUND_856,
    }

    public enum RouteOrderOffsetOutboundEDI832 {

        OUTBOUND_832_CSV, OUTBOUND_832_CATALOG, OUTBOUND_832;
    }

    public enum RouteOrderOffsetInboundEDI864 {
        EMAIL, LOG, SOURCE
    }

}
