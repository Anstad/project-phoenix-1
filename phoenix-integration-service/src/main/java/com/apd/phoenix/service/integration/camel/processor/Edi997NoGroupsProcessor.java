package com.apd.phoenix.service.integration.camel.processor;

import javax.edi.model.x12.edi997.segment.Detail;
import javax.edi.model.x12.edi997.segment.FunctionalAcknowledgementBody;
import javax.ejb.Stateless;
import org.apache.camel.Exchange;

@Stateless
public class Edi997NoGroupsProcessor {

    public void responseGroupsFound(Exchange exchange) {
        FunctionalAcknowledgementBody functionalAcknowledgementBody = (FunctionalAcknowledgementBody) exchange.getIn()
                .getBody();
        exchange.setOut(exchange.getIn());
        Detail detail = functionalAcknowledgementBody.getDetail();
        exchange.getOut().setHeader(
                "responseGroupNotFound",
                detail == null || detail.getTransactionResponseGroup() == null
                        || detail.getTransactionResponseGroup().isEmpty());
    }
}
