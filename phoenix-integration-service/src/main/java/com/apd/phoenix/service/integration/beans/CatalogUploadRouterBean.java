package com.apd.phoenix.service.integration.beans;

import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.cdi.CdiCamelContext;
import org.apache.camel.component.jms.JmsConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.integration.camel.processor.catalog.CatalogMessageProcessor;
import com.apd.phoenix.service.integration.routes.MessageBeanRouteBuilder;
import com.apd.phoenix.service.integration.utility.PropertiesLoader;

@Startup
@Singleton
public class CatalogUploadRouterBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(CatalogUploadRouterBean.class);

    @Inject
    private CdiCamelContext camelContext;

    @PostConstruct
    public void init() throws Exception {
        LOGGER.info("PostConstruct called");
        setup();
    }

    @PreDestroy
    public void destroy() throws Exception {
        LOGGER.info("PreDestroy called");
        camelContext.stop();
    }

    private void setup() throws Exception {
        Properties commonProperties = PropertiesLoader.getAsProperties("catalog.upload.integration");
        setupAmq(camelContext, commonProperties.getProperty("jmsBrokerUrl"));
        MessageBeanRouteBuilder catalogRoute = new MessageBeanRouteBuilder();
        catalogRoute.setClazz(CatalogMessageProcessor.class);
        catalogRoute.setSource(commonProperties.getProperty("catalogUnprocessedSource"));
        catalogRoute.setMethod("process");
        camelContext.addRoutes(catalogRoute);
        camelContext.start();
        LOGGER.info("CamelContext started");
    }

    public void setupAmq(CamelContext context, String jmsBrokerUrl) {
        Properties amqProperties = PropertiesLoader.getAsProperties("amq");
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL(jmsBrokerUrl);
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(activeMQConnectionFactory);
        pooledConnectionFactory.setMaxConnections(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".maxConnections", "2")));
        JmsConfiguration jmsConfiguration = new JmsConfiguration();
        jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
        jmsConfiguration.setConcurrentConsumers(Integer.parseInt(amqProperties.getProperty(getClass().getSimpleName()
                + ".concurrentConsumers", "2")));
        ActiveMQComponent activeMQComponent = new ActiveMQComponent();
        activeMQComponent.setConfiguration(jmsConfiguration);
        context.addComponent("activemq", activeMQComponent);
    }

}
