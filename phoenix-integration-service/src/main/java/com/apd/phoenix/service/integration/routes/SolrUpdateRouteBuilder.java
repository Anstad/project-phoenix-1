package com.apd.phoenix.service.integration.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.integration.camel.aggregation.strategy.ArrayListAggregationStrategy;
import com.apd.phoenix.service.integration.camel.processor.solr.SolrUpdateProcessor;

public class SolrUpdateRouteBuilder extends RouteBuilder {

    private String updateSource;

    private String updateSourceAggregated;

    private Long throttleMessageCount;

    private Long throttleTimePeriod;

    private int aggregationSize;

    private Long aggregationTimeout;

    @Override
    public void configure() throws Exception {

        onException(Exception.class).maximumRedeliveries(0).handled(true).log(LoggingLevel.INFO,
                "Error in solr processing: ${exception.stacktrace}");

        //Aggregate initial messages
        from(updateSource).autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
        //Logging
                .log(LoggingLevel.DEBUG, "Received message for aggregation")
                //Aggregate all messages
                .aggregate(constant(true), new ArrayListAggregationStrategy())
                //Aggregation size and timeout
                .completionSize(aggregationSize).completionTimeout(aggregationTimeout)
                //Logging
                .log(LoggingLevel.INFO, "Sending to update processor")
                //send to aggregated queue
                .to(updateSourceAggregated);

        //CatXItem Update Source
        from(updateSourceAggregated).autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
        //Throttle, each message contains up to aggregationSize documents
                .throttle(throttleMessageCount).timePeriodMillis(throttleTimePeriod)
                //Logging
                .log(LoggingLevel.INFO, "Sending aggregated message to update processor")
                //Process batch
                .bean(SolrUpdateProcessor.class, "processUpdates");

    }

    public String getUpdateSource() {
        return updateSource;
    }

    public void setUpdateSource(String updateSource) {
        this.updateSource = updateSource;
    }

    public String getUpdateSourceAggregated() {
        return updateSourceAggregated;
    }

    public void setUpdateSourceAggregated(String updateSourceAggregated) {
        this.updateSourceAggregated = updateSourceAggregated;
    }

    public Long getThrottleMessageCount() {
        return throttleMessageCount;
    }

    public void setThrottleMessageCount(Long throttleMessageCount) {
        this.throttleMessageCount = throttleMessageCount;
    }

    public Long getThrottleTimePeriod() {
        return throttleTimePeriod;
    }

    public void setThrottleTimePeriod(Long throttleTimePeriod) {
        this.throttleTimePeriod = throttleTimePeriod;
    }

    public int getAggregationSize() {
        return aggregationSize;
    }

    public void setAggregationSize(int aggregationSize) {
        this.aggregationSize = aggregationSize;
    }

    public Long getAggregationTimeout() {
        return aggregationTimeout;
    }

    public void setAggregationTimeout(Long aggregationTimeout) {
        this.aggregationTimeout = aggregationTimeout;
    }

}
