package com.apd.phoenix.integration.model.xcbl.v3_5;

import rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35.MessageAcknowledgement;

public class XCBLFactory {

    public static String XCBL_DEFAULT_ACKNOWLEDGEMENT_LOCATION = "Inbound XCBL Processor";
    public static String XCBL_DEFAULT_ACKNOWLEDGEMENT_NOTE = "XCBL message was received and is being processed.";
    public static String OSN_DEFAULT_ACKNOWLEDGEMENT_LOCATION = "OSN LiteEnvelope";
    public static String OSN_DEFAULT_ACKNOWLEDGEMENT_NOTE = "LiteEnvelope was received and is being processed.";

    // Standard XCBL Messages
    public static MessageAcknowledgement createMessageAcknowledgement(String acknowledgementReferenceNumber) {
        return createMessageAcknowledgement(XCBL_DEFAULT_ACKNOWLEDGEMENT_LOCATION, XCBL_DEFAULT_ACKNOWLEDGEMENT_NOTE,
                acknowledgementReferenceNumber);
    }

    public static MessageAcknowledgement createMessageAcknowledgement(String acknowledgementLocation,
            String acknowledgementNote, String acknowledgementReferenceNumber) {
        MessageAcknowledgement messageAck = new MessageAcknowledgement();
        messageAck.setAcknowledgementLocation(acknowledgementLocation);
        messageAck.setAcknowledgementNote(acknowledgementNote);
        messageAck.setAcknowledgementReferenceNumber(acknowledgementReferenceNumber);
        return messageAck;
    }

    // OSN LiteEnvelope wrapped XCBL messages
    public static com.perfect.liteenvelope.MessageAcknowledgement createOSNMessageAcknowledgement(
            String acknowledgementReferenceID) {
        return createOSNMessageAcknowledgement(OSN_DEFAULT_ACKNOWLEDGEMENT_LOCATION, OSN_DEFAULT_ACKNOWLEDGEMENT_NOTE,
                acknowledgementReferenceID);
    }

    public static com.perfect.liteenvelope.MessageAcknowledgement createOSNMessageAcknowledgement(
            String acknowledgementLocation, String acknowledgementNote, String acknowledgementReferenceID) {
        com.perfect.liteenvelope.MessageAcknowledgement messageAck = new com.perfect.liteenvelope.MessageAcknowledgement();
        messageAck.setAcknowledgementLocation(acknowledgementLocation);
        messageAck.setAcknowledgementNote(acknowledgementNote);
        messageAck.setAcknowledgementReferenceID(acknowledgementReferenceID);
        return messageAck;
    }
}
