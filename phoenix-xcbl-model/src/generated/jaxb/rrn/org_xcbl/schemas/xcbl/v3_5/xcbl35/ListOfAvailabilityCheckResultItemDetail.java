//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListOfAvailabilityCheckResultItemDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListOfAvailabilityCheckResultItemDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AvailabilityCheckResultItemDetail" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListOfAvailabilityCheckResultItemDetail", propOrder = {
    "availabilityCheckResultItemDetail"
})
public class ListOfAvailabilityCheckResultItemDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "AvailabilityCheckResultItemDetail", required = true)
    protected List<AvailabilityCheckResultItemDetail> availabilityCheckResultItemDetail;

    /**
     * Gets the value of the availabilityCheckResultItemDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availabilityCheckResultItemDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailabilityCheckResultItemDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AvailabilityCheckResultItemDetail }
     * 
     * 
     */
    public List<AvailabilityCheckResultItemDetail> getAvailabilityCheckResultItemDetail() {
        if (availabilityCheckResultItemDetail == null) {
            availabilityCheckResultItemDetail = new ArrayList<AvailabilityCheckResultItemDetail>();
        }
        return this.availabilityCheckResultItemDetail;
    }

}
