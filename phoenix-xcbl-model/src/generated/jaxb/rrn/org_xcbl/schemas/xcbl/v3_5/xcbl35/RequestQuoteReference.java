//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestQuoteReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestQuoteReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ContractReference" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Contract"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AccountNumber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PriceListNumber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PriceListVersionNumber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BuyersCatalogNumber" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OtherRequestQuoteReferences" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestQuoteReference", propOrder = {
    "contractReference",
    "accountNumber",
    "priceListNumber",
    "priceListVersionNumber",
    "buyersCatalogNumber",
    "otherRequestQuoteReferences"
})
public class RequestQuoteReference
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ContractReference")
    protected RequestQuoteReference.ContractReference contractReference;
    @XmlElement(name = "AccountNumber")
    protected RequestQuoteReference.AccountNumber accountNumber;
    @XmlElement(name = "PriceListNumber")
    protected RequestQuoteReference.PriceListNumber priceListNumber;
    @XmlElement(name = "PriceListVersionNumber")
    protected RequestQuoteReference.PriceListVersionNumber priceListVersionNumber;
    @XmlElement(name = "BuyersCatalogNumber")
    protected RequestQuoteReference.BuyersCatalogNumber buyersCatalogNumber;
    @XmlElement(name = "OtherRequestQuoteReferences")
    protected RequestQuoteReference.OtherRequestQuoteReferences otherRequestQuoteReferences;

    /**
     * Gets the value of the contractReference property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.ContractReference }
     *     
     */
    public RequestQuoteReference.ContractReference getContractReference() {
        return contractReference;
    }

    /**
     * Sets the value of the contractReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.ContractReference }
     *     
     */
    public void setContractReference(RequestQuoteReference.ContractReference value) {
        this.contractReference = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.AccountNumber }
     *     
     */
    public RequestQuoteReference.AccountNumber getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.AccountNumber }
     *     
     */
    public void setAccountNumber(RequestQuoteReference.AccountNumber value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the priceListNumber property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.PriceListNumber }
     *     
     */
    public RequestQuoteReference.PriceListNumber getPriceListNumber() {
        return priceListNumber;
    }

    /**
     * Sets the value of the priceListNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.PriceListNumber }
     *     
     */
    public void setPriceListNumber(RequestQuoteReference.PriceListNumber value) {
        this.priceListNumber = value;
    }

    /**
     * Gets the value of the priceListVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.PriceListVersionNumber }
     *     
     */
    public RequestQuoteReference.PriceListVersionNumber getPriceListVersionNumber() {
        return priceListVersionNumber;
    }

    /**
     * Sets the value of the priceListVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.PriceListVersionNumber }
     *     
     */
    public void setPriceListVersionNumber(RequestQuoteReference.PriceListVersionNumber value) {
        this.priceListVersionNumber = value;
    }

    /**
     * Gets the value of the buyersCatalogNumber property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.BuyersCatalogNumber }
     *     
     */
    public RequestQuoteReference.BuyersCatalogNumber getBuyersCatalogNumber() {
        return buyersCatalogNumber;
    }

    /**
     * Sets the value of the buyersCatalogNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.BuyersCatalogNumber }
     *     
     */
    public void setBuyersCatalogNumber(RequestQuoteReference.BuyersCatalogNumber value) {
        this.buyersCatalogNumber = value;
    }

    /**
     * Gets the value of the otherRequestQuoteReferences property.
     * 
     * @return
     *     possible object is
     *     {@link RequestQuoteReference.OtherRequestQuoteReferences }
     *     
     */
    public RequestQuoteReference.OtherRequestQuoteReferences getOtherRequestQuoteReferences() {
        return otherRequestQuoteReferences;
    }

    /**
     * Sets the value of the otherRequestQuoteReferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestQuoteReference.OtherRequestQuoteReferences }
     *     
     */
    public void setOtherRequestQuoteReferences(RequestQuoteReference.OtherRequestQuoteReferences value) {
        this.otherRequestQuoteReferences = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class AccountNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class BuyersCatalogNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Contract"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "contract"
    })
    public static class ContractReference
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Contract", required = true)
        protected Contract contract;

        /**
         * Gets the value of the contract property.
         * 
         * @return
         *     possible object is
         *     {@link Contract }
         *     
         */
        public Contract getContract() {
            return contract;
        }

        /**
         * Sets the value of the contract property.
         * 
         * @param value
         *     allowed object is
         *     {@link Contract }
         *     
         */
        public void setContract(Contract value) {
            this.contract = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "listOfReferenceCoded"
    })
    public static class OtherRequestQuoteReferences
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ListOfReferenceCoded", required = true)
        protected ListOfReferenceCoded listOfReferenceCoded;

        /**
         * Gets the value of the listOfReferenceCoded property.
         * 
         * @return
         *     possible object is
         *     {@link ListOfReferenceCoded }
         *     
         */
        public ListOfReferenceCoded getListOfReferenceCoded() {
            return listOfReferenceCoded;
        }

        /**
         * Sets the value of the listOfReferenceCoded property.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfReferenceCoded }
         *     
         */
        public void setListOfReferenceCoded(ListOfReferenceCoded value) {
            this.listOfReferenceCoded = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class PriceListNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class PriceListVersionNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }

}
