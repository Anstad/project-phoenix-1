//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentRequestAcknowledgment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentRequestAcknowledgment">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PaymentRequestAcknHeader"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfPaymentRequestAcknDetail"/>
 *         &lt;element name="PaymentRequestAcknSummary">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PaymentRequestSummary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentRequestAcknowledgment", propOrder = {
    "paymentRequestAcknHeader",
    "listOfPaymentRequestAcknDetail",
    "paymentRequestAcknSummary"
})
public class PaymentRequestAcknowledgment
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "PaymentRequestAcknHeader", required = true)
    protected PaymentRequestAcknHeader paymentRequestAcknHeader;
    @XmlElement(name = "ListOfPaymentRequestAcknDetail", required = true)
    protected ListOfPaymentRequestAcknDetail listOfPaymentRequestAcknDetail;
    @XmlElement(name = "PaymentRequestAcknSummary", required = true)
    protected PaymentRequestAcknowledgment.PaymentRequestAcknSummary paymentRequestAcknSummary;

    /**
     * Gets the value of the paymentRequestAcknHeader property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentRequestAcknHeader }
     *     
     */
    public PaymentRequestAcknHeader getPaymentRequestAcknHeader() {
        return paymentRequestAcknHeader;
    }

    /**
     * Sets the value of the paymentRequestAcknHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRequestAcknHeader }
     *     
     */
    public void setPaymentRequestAcknHeader(PaymentRequestAcknHeader value) {
        this.paymentRequestAcknHeader = value;
    }

    /**
     * Gets the value of the listOfPaymentRequestAcknDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfPaymentRequestAcknDetail }
     *     
     */
    public ListOfPaymentRequestAcknDetail getListOfPaymentRequestAcknDetail() {
        return listOfPaymentRequestAcknDetail;
    }

    /**
     * Sets the value of the listOfPaymentRequestAcknDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfPaymentRequestAcknDetail }
     *     
     */
    public void setListOfPaymentRequestAcknDetail(ListOfPaymentRequestAcknDetail value) {
        this.listOfPaymentRequestAcknDetail = value;
    }

    /**
     * Gets the value of the paymentRequestAcknSummary property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentRequestAcknowledgment.PaymentRequestAcknSummary }
     *     
     */
    public PaymentRequestAcknowledgment.PaymentRequestAcknSummary getPaymentRequestAcknSummary() {
        return paymentRequestAcknSummary;
    }

    /**
     * Sets the value of the paymentRequestAcknSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentRequestAcknowledgment.PaymentRequestAcknSummary }
     *     
     */
    public void setPaymentRequestAcknSummary(PaymentRequestAcknowledgment.PaymentRequestAcknSummary value) {
        this.paymentRequestAcknSummary = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PaymentRequestSummary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "paymentRequestSummary"
    })
    public static class PaymentRequestAcknSummary
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "PaymentRequestSummary", required = true)
        protected PaymentRequestSummary paymentRequestSummary;

        /**
         * Gets the value of the paymentRequestSummary property.
         * 
         * @return
         *     possible object is
         *     {@link PaymentRequestSummary }
         *     
         */
        public PaymentRequestSummary getPaymentRequestSummary() {
            return paymentRequestSummary;
        }

        /**
         * Sets the value of the paymentRequestSummary property.
         * 
         * @param value
         *     allowed object is
         *     {@link PaymentRequestSummary }
         *     
         */
        public void setPaymentRequestSummary(PaymentRequestSummary value) {
            this.paymentRequestSummary = value;
        }

    }

}
