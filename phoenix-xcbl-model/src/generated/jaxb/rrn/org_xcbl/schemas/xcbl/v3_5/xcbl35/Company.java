//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Company complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Company">
 *   &lt;complexContent>
 *     &lt;extension base="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party">
 *       &lt;sequence>
 *         &lt;element name="IndustrySectorCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}IndustrySectorCode" minOccurs="0"/>
 *         &lt;element name="IndustrySectorCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Company", propOrder = {
    "industrySectorCoded",
    "industrySectorCodedOther"
})
public class Company
    extends Party
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "IndustrySectorCoded")
    protected IndustrySectorCode industrySectorCoded;
    @XmlElement(name = "IndustrySectorCodedOther")
    protected String industrySectorCodedOther;

    /**
     * Gets the value of the industrySectorCoded property.
     * 
     * @return
     *     possible object is
     *     {@link IndustrySectorCode }
     *     
     */
    public IndustrySectorCode getIndustrySectorCoded() {
        return industrySectorCoded;
    }

    /**
     * Sets the value of the industrySectorCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustrySectorCode }
     *     
     */
    public void setIndustrySectorCoded(IndustrySectorCode value) {
        this.industrySectorCoded = value;
    }

    /**
     * Gets the value of the industrySectorCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustrySectorCodedOther() {
        return industrySectorCodedOther;
    }

    /**
     * Sets the value of the industrySectorCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustrySectorCodedOther(String value) {
        this.industrySectorCodedOther = value;
    }

}
