//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderStatusResultSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderStatusResultSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderStatusCheckItemError" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OrderStatusSummaryErrorInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalNumberOfLineItem" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderStatusResultSummary", propOrder = {
    "orderStatusCheckItemError",
    "orderStatusSummaryErrorInfo",
    "totalNumberOfLineItem"
})
public class OrderStatusResultSummary
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OrderStatusCheckItemError")
    protected int orderStatusCheckItemError;
    @XmlElement(name = "OrderStatusSummaryErrorInfo")
    protected OrderStatusResultSummary.OrderStatusSummaryErrorInfo orderStatusSummaryErrorInfo;
    @XmlElement(name = "TotalNumberOfLineItem")
    protected Integer totalNumberOfLineItem;

    /**
     * Gets the value of the orderStatusCheckItemError property.
     * 
     */
    public int getOrderStatusCheckItemError() {
        return orderStatusCheckItemError;
    }

    /**
     * Sets the value of the orderStatusCheckItemError property.
     * 
     */
    public void setOrderStatusCheckItemError(int value) {
        this.orderStatusCheckItemError = value;
    }

    /**
     * Gets the value of the orderStatusSummaryErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatusResultSummary.OrderStatusSummaryErrorInfo }
     *     
     */
    public OrderStatusResultSummary.OrderStatusSummaryErrorInfo getOrderStatusSummaryErrorInfo() {
        return orderStatusSummaryErrorInfo;
    }

    /**
     * Sets the value of the orderStatusSummaryErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatusResultSummary.OrderStatusSummaryErrorInfo }
     *     
     */
    public void setOrderStatusSummaryErrorInfo(OrderStatusResultSummary.OrderStatusSummaryErrorInfo value) {
        this.orderStatusSummaryErrorInfo = value;
    }

    /**
     * Gets the value of the totalNumberOfLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalNumberOfLineItem() {
        return totalNumberOfLineItem;
    }

    /**
     * Sets the value of the totalNumberOfLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalNumberOfLineItem(Integer value) {
        this.totalNumberOfLineItem = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorInfo"
    })
    public static class OrderStatusSummaryErrorInfo
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ErrorInfo", required = true)
        protected ErrorInfo errorInfo;

        /**
         * Gets the value of the errorInfo property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorInfo }
         *     
         */
        public ErrorInfo getErrorInfo() {
            return errorInfo;
        }

        /**
         * Sets the value of the errorInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorInfo }
         *     
         */
        public void setErrorInfo(ErrorInfo value) {
            this.errorInfo = value;
        }

    }

}
