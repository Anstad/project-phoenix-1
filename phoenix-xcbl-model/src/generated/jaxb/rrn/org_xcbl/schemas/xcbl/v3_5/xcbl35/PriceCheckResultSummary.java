//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PriceCheckResultSummary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PriceCheckResultSummary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PriceCheckItemError" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PriceCheckSummaryErrorInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TotalNumberOfLineItem" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PriceCheckResultSummary", propOrder = {
    "priceCheckItemError",
    "priceCheckSummaryErrorInfo",
    "totalNumberOfLineItem"
})
public class PriceCheckResultSummary
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "PriceCheckItemError")
    protected int priceCheckItemError;
    @XmlElement(name = "PriceCheckSummaryErrorInfo")
    protected PriceCheckResultSummary.PriceCheckSummaryErrorInfo priceCheckSummaryErrorInfo;
    @XmlElement(name = "TotalNumberOfLineItem")
    protected Integer totalNumberOfLineItem;

    /**
     * Gets the value of the priceCheckItemError property.
     * 
     */
    public int getPriceCheckItemError() {
        return priceCheckItemError;
    }

    /**
     * Sets the value of the priceCheckItemError property.
     * 
     */
    public void setPriceCheckItemError(int value) {
        this.priceCheckItemError = value;
    }

    /**
     * Gets the value of the priceCheckSummaryErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PriceCheckResultSummary.PriceCheckSummaryErrorInfo }
     *     
     */
    public PriceCheckResultSummary.PriceCheckSummaryErrorInfo getPriceCheckSummaryErrorInfo() {
        return priceCheckSummaryErrorInfo;
    }

    /**
     * Sets the value of the priceCheckSummaryErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceCheckResultSummary.PriceCheckSummaryErrorInfo }
     *     
     */
    public void setPriceCheckSummaryErrorInfo(PriceCheckResultSummary.PriceCheckSummaryErrorInfo value) {
        this.priceCheckSummaryErrorInfo = value;
    }

    /**
     * Gets the value of the totalNumberOfLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTotalNumberOfLineItem() {
        return totalNumberOfLineItem;
    }

    /**
     * Sets the value of the totalNumberOfLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTotalNumberOfLineItem(Integer value) {
        this.totalNumberOfLineItem = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorInfo"
    })
    public static class PriceCheckSummaryErrorInfo
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ErrorInfo", required = true)
        protected ErrorInfo errorInfo;

        /**
         * Gets the value of the errorInfo property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorInfo }
         *     
         */
        public ErrorInfo getErrorInfo() {
            return errorInfo;
        }

        /**
         * Sets the value of the errorInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorInfo }
         *     
         */
        public void setErrorInfo(ErrorInfo value) {
            this.errorInfo = value;
        }

    }

}
