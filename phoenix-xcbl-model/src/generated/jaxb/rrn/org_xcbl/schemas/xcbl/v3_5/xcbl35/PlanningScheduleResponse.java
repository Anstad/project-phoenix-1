//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanningScheduleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlanningScheduleResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PlanningScheduleResponseHeader"/>
 *         &lt;choice>
 *           &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfLocationGroupedPlanningResponse"/>
 *           &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfMaterialGroupedPlanningResponse"/>
 *         &lt;/choice>
 *         &lt;element name="PlanningScheduleResponseSummary" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PlanningScheduleSummary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlanningScheduleResponse", propOrder = {
    "planningScheduleResponseHeader",
    "listOfLocationGroupedPlanningResponse",
    "listOfMaterialGroupedPlanningResponse",
    "planningScheduleResponseSummary"
})
public class PlanningScheduleResponse
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "PlanningScheduleResponseHeader", required = true)
    protected PlanningScheduleResponseHeader planningScheduleResponseHeader;
    @XmlElement(name = "ListOfLocationGroupedPlanningResponse")
    protected ListOfLocationGroupedPlanningResponse listOfLocationGroupedPlanningResponse;
    @XmlElement(name = "ListOfMaterialGroupedPlanningResponse")
    protected ListOfMaterialGroupedPlanningResponse listOfMaterialGroupedPlanningResponse;
    @XmlElement(name = "PlanningScheduleResponseSummary")
    protected PlanningScheduleResponse.PlanningScheduleResponseSummary planningScheduleResponseSummary;

    /**
     * Gets the value of the planningScheduleResponseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link PlanningScheduleResponseHeader }
     *     
     */
    public PlanningScheduleResponseHeader getPlanningScheduleResponseHeader() {
        return planningScheduleResponseHeader;
    }

    /**
     * Sets the value of the planningScheduleResponseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanningScheduleResponseHeader }
     *     
     */
    public void setPlanningScheduleResponseHeader(PlanningScheduleResponseHeader value) {
        this.planningScheduleResponseHeader = value;
    }

    /**
     * Gets the value of the listOfLocationGroupedPlanningResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfLocationGroupedPlanningResponse }
     *     
     */
    public ListOfLocationGroupedPlanningResponse getListOfLocationGroupedPlanningResponse() {
        return listOfLocationGroupedPlanningResponse;
    }

    /**
     * Sets the value of the listOfLocationGroupedPlanningResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfLocationGroupedPlanningResponse }
     *     
     */
    public void setListOfLocationGroupedPlanningResponse(ListOfLocationGroupedPlanningResponse value) {
        this.listOfLocationGroupedPlanningResponse = value;
    }

    /**
     * Gets the value of the listOfMaterialGroupedPlanningResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfMaterialGroupedPlanningResponse }
     *     
     */
    public ListOfMaterialGroupedPlanningResponse getListOfMaterialGroupedPlanningResponse() {
        return listOfMaterialGroupedPlanningResponse;
    }

    /**
     * Sets the value of the listOfMaterialGroupedPlanningResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfMaterialGroupedPlanningResponse }
     *     
     */
    public void setListOfMaterialGroupedPlanningResponse(ListOfMaterialGroupedPlanningResponse value) {
        this.listOfMaterialGroupedPlanningResponse = value;
    }

    /**
     * Gets the value of the planningScheduleResponseSummary property.
     * 
     * @return
     *     possible object is
     *     {@link PlanningScheduleResponse.PlanningScheduleResponseSummary }
     *     
     */
    public PlanningScheduleResponse.PlanningScheduleResponseSummary getPlanningScheduleResponseSummary() {
        return planningScheduleResponseSummary;
    }

    /**
     * Sets the value of the planningScheduleResponseSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanningScheduleResponse.PlanningScheduleResponseSummary }
     *     
     */
    public void setPlanningScheduleResponseSummary(PlanningScheduleResponse.PlanningScheduleResponseSummary value) {
        this.planningScheduleResponseSummary = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}PlanningScheduleSummary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "planningScheduleSummary"
    })
    public static class PlanningScheduleResponseSummary
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "PlanningScheduleSummary", required = true)
        protected PlanningScheduleSummary planningScheduleSummary;

        /**
         * Gets the value of the planningScheduleSummary property.
         * 
         * @return
         *     possible object is
         *     {@link PlanningScheduleSummary }
         *     
         */
        public PlanningScheduleSummary getPlanningScheduleSummary() {
            return planningScheduleSummary;
        }

        /**
         * Sets the value of the planningScheduleSummary property.
         * 
         * @param value
         *     allowed object is
         *     {@link PlanningScheduleSummary }
         *     
         */
        public void setPlanningScheduleSummary(PlanningScheduleSummary value) {
            this.planningScheduleSummary = value;
        }

    }

}
