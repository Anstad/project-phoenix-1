//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvoicePurpose complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvoicePurpose">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoicePurposeCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}InvoicePurposeCode"/>
 *         &lt;element name="InvoicePurposeCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoicePurpose", propOrder = {
    "invoicePurposeCoded",
    "invoicePurposeCodedOther"
})
public class InvoicePurpose
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "InvoicePurposeCoded", required = true)
    protected InvoicePurposeCode invoicePurposeCoded;
    @XmlElement(name = "InvoicePurposeCodedOther")
    protected String invoicePurposeCodedOther;

    /**
     * Gets the value of the invoicePurposeCoded property.
     * 
     * @return
     *     possible object is
     *     {@link InvoicePurposeCode }
     *     
     */
    public InvoicePurposeCode getInvoicePurposeCoded() {
        return invoicePurposeCoded;
    }

    /**
     * Sets the value of the invoicePurposeCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoicePurposeCode }
     *     
     */
    public void setInvoicePurposeCoded(InvoicePurposeCode value) {
        this.invoicePurposeCoded = value;
    }

    /**
     * Gets the value of the invoicePurposeCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicePurposeCodedOther() {
        return invoicePurposeCodedOther;
    }

    /**
     * Sets the value of the invoicePurposeCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicePurposeCodedOther(String value) {
        this.invoicePurposeCodedOther = value;
    }

}
