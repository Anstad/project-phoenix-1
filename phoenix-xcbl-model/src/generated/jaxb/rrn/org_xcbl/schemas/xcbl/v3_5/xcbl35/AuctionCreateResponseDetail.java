//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AuctionCreateResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AuctionCreateResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuctionItemID" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AlphaNum40"/>
 *         &lt;element name="AuctionItemName" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AlphaNum50"/>
 *         &lt;element name="AuctionItemDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuctionItemHierarchyLevel" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AuctionLineItemNum" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="AuctionItemResponseCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AuctionItemResponseCode"/>
 *         &lt;element name="AuctionItemResponseCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChangedAuctionCreateDetail" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AuctionDetail"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfAuctionItemComponentResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AuctionCreateResponseDetail", propOrder = {
    "auctionItemID",
    "auctionItemName",
    "auctionItemDescription",
    "auctionItemHierarchyLevel",
    "auctionLineItemNum",
    "auctionItemResponseCoded",
    "auctionItemResponseCodedOther",
    "changedAuctionCreateDetail",
    "listOfAuctionItemComponentResponse"
})
public class AuctionCreateResponseDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "AuctionItemID", required = true)
    protected String auctionItemID;
    @XmlElement(name = "AuctionItemName", required = true)
    protected String auctionItemName;
    @XmlElement(name = "AuctionItemDescription")
    protected String auctionItemDescription;
    @XmlElement(name = "AuctionItemHierarchyLevel")
    protected int auctionItemHierarchyLevel;
    @XmlElement(name = "AuctionLineItemNum")
    protected Integer auctionLineItemNum;
    @XmlElement(name = "AuctionItemResponseCoded", required = true)
    protected AuctionItemResponseCode auctionItemResponseCoded;
    @XmlElement(name = "AuctionItemResponseCodedOther")
    protected String auctionItemResponseCodedOther;
    @XmlElement(name = "ChangedAuctionCreateDetail")
    protected AuctionCreateResponseDetail.ChangedAuctionCreateDetail changedAuctionCreateDetail;
    @XmlElement(name = "ListOfAuctionItemComponentResponse")
    protected ListOfAuctionItemComponentResponse listOfAuctionItemComponentResponse;

    /**
     * Gets the value of the auctionItemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuctionItemID() {
        return auctionItemID;
    }

    /**
     * Sets the value of the auctionItemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuctionItemID(String value) {
        this.auctionItemID = value;
    }

    /**
     * Gets the value of the auctionItemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuctionItemName() {
        return auctionItemName;
    }

    /**
     * Sets the value of the auctionItemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuctionItemName(String value) {
        this.auctionItemName = value;
    }

    /**
     * Gets the value of the auctionItemDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuctionItemDescription() {
        return auctionItemDescription;
    }

    /**
     * Sets the value of the auctionItemDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuctionItemDescription(String value) {
        this.auctionItemDescription = value;
    }

    /**
     * Gets the value of the auctionItemHierarchyLevel property.
     * 
     */
    public int getAuctionItemHierarchyLevel() {
        return auctionItemHierarchyLevel;
    }

    /**
     * Sets the value of the auctionItemHierarchyLevel property.
     * 
     */
    public void setAuctionItemHierarchyLevel(int value) {
        this.auctionItemHierarchyLevel = value;
    }

    /**
     * Gets the value of the auctionLineItemNum property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAuctionLineItemNum() {
        return auctionLineItemNum;
    }

    /**
     * Sets the value of the auctionLineItemNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAuctionLineItemNum(Integer value) {
        this.auctionLineItemNum = value;
    }

    /**
     * Gets the value of the auctionItemResponseCoded property.
     * 
     * @return
     *     possible object is
     *     {@link AuctionItemResponseCode }
     *     
     */
    public AuctionItemResponseCode getAuctionItemResponseCoded() {
        return auctionItemResponseCoded;
    }

    /**
     * Sets the value of the auctionItemResponseCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuctionItemResponseCode }
     *     
     */
    public void setAuctionItemResponseCoded(AuctionItemResponseCode value) {
        this.auctionItemResponseCoded = value;
    }

    /**
     * Gets the value of the auctionItemResponseCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuctionItemResponseCodedOther() {
        return auctionItemResponseCodedOther;
    }

    /**
     * Sets the value of the auctionItemResponseCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuctionItemResponseCodedOther(String value) {
        this.auctionItemResponseCodedOther = value;
    }

    /**
     * Gets the value of the changedAuctionCreateDetail property.
     * 
     * @return
     *     possible object is
     *     {@link AuctionCreateResponseDetail.ChangedAuctionCreateDetail }
     *     
     */
    public AuctionCreateResponseDetail.ChangedAuctionCreateDetail getChangedAuctionCreateDetail() {
        return changedAuctionCreateDetail;
    }

    /**
     * Sets the value of the changedAuctionCreateDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuctionCreateResponseDetail.ChangedAuctionCreateDetail }
     *     
     */
    public void setChangedAuctionCreateDetail(AuctionCreateResponseDetail.ChangedAuctionCreateDetail value) {
        this.changedAuctionCreateDetail = value;
    }

    /**
     * Gets the value of the listOfAuctionItemComponentResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAuctionItemComponentResponse }
     *     
     */
    public ListOfAuctionItemComponentResponse getListOfAuctionItemComponentResponse() {
        return listOfAuctionItemComponentResponse;
    }

    /**
     * Sets the value of the listOfAuctionItemComponentResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAuctionItemComponentResponse }
     *     
     */
    public void setListOfAuctionItemComponentResponse(ListOfAuctionItemComponentResponse value) {
        this.listOfAuctionItemComponentResponse = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}AuctionDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "auctionDetail"
    })
    public static class ChangedAuctionCreateDetail
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "AuctionDetail", required = true)
        protected AuctionDetail auctionDetail;

        /**
         * Gets the value of the auctionDetail property.
         * 
         * @return
         *     possible object is
         *     {@link AuctionDetail }
         *     
         */
        public AuctionDetail getAuctionDetail() {
            return auctionDetail;
        }

        /**
         * Sets the value of the auctionDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link AuctionDetail }
         *     
         */
        public void setAuctionDetail(AuctionDetail value) {
            this.auctionDetail = value;
        }

    }

}
