//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfOrderResponseItemDetail" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfOrderResponsePackageDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderResponseDetail", propOrder = {
    "listOfOrderResponseItemDetail",
    "listOfOrderResponsePackageDetail"
})
public class OrderResponseDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ListOfOrderResponseItemDetail")
    protected ListOfOrderResponseItemDetail listOfOrderResponseItemDetail;
    @XmlElement(name = "ListOfOrderResponsePackageDetail")
    protected ListOfOrderResponsePackageDetail listOfOrderResponsePackageDetail;

    /**
     * Gets the value of the listOfOrderResponseItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfOrderResponseItemDetail }
     *     
     */
    public ListOfOrderResponseItemDetail getListOfOrderResponseItemDetail() {
        return listOfOrderResponseItemDetail;
    }

    /**
     * Sets the value of the listOfOrderResponseItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOrderResponseItemDetail }
     *     
     */
    public void setListOfOrderResponseItemDetail(ListOfOrderResponseItemDetail value) {
        this.listOfOrderResponseItemDetail = value;
    }

    /**
     * Gets the value of the listOfOrderResponsePackageDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfOrderResponsePackageDetail }
     *     
     */
    public ListOfOrderResponsePackageDetail getListOfOrderResponsePackageDetail() {
        return listOfOrderResponsePackageDetail;
    }

    /**
     * Sets the value of the listOfOrderResponsePackageDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOrderResponsePackageDetail }
     *     
     */
    public void setListOfOrderResponsePackageDetail(ListOfOrderResponsePackageDetail value) {
        this.listOfOrderResponsePackageDetail = value;
    }

}
