//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ForecastFrequencyCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ForecastFrequencyCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *     &lt;enumeration value="Annually"/>
 *     &lt;enumeration value="Daily"/>
 *     &lt;enumeration value="Discrete"/>
 *     &lt;enumeration value="FlexibleInterval"/>
 *     &lt;enumeration value="MonthlyBucket"/>
 *     &lt;enumeration value="Quarterly"/>
 *     &lt;enumeration value="Semi-Annually"/>
 *     &lt;enumeration value="FourWeekBucket"/>
 *     &lt;enumeration value="WeeklyBucket"/>
 *     &lt;enumeration value="Just-In-Time"/>
 *     &lt;enumeration value="Continuous"/>
 *     &lt;enumeration value="Synchronous"/>
 *     &lt;enumeration value="Replenishment"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ForecastFrequencyCode")
@XmlEnum
public enum ForecastFrequencyCode {

    @XmlEnumValue("Annually")
    ANNUALLY("Annually"),
    @XmlEnumValue("Daily")
    DAILY("Daily"),
    @XmlEnumValue("Discrete")
    DISCRETE("Discrete"),
    @XmlEnumValue("FlexibleInterval")
    FLEXIBLE_INTERVAL("FlexibleInterval"),
    @XmlEnumValue("MonthlyBucket")
    MONTHLY_BUCKET("MonthlyBucket"),
    @XmlEnumValue("Quarterly")
    QUARTERLY("Quarterly"),
    @XmlEnumValue("Semi-Annually")
    SEMI_ANNUALLY("Semi-Annually"),
    @XmlEnumValue("FourWeekBucket")
    FOUR_WEEK_BUCKET("FourWeekBucket"),
    @XmlEnumValue("WeeklyBucket")
    WEEKLY_BUCKET("WeeklyBucket"),
    @XmlEnumValue("Just-In-Time")
    JUST_IN_TIME("Just-In-Time"),
    @XmlEnumValue("Continuous")
    CONTINUOUS("Continuous"),
    @XmlEnumValue("Synchronous")
    SYNCHRONOUS("Synchronous"),
    @XmlEnumValue("Replenishment")
    REPLENISHMENT("Replenishment"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    ForecastFrequencyCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ForecastFrequencyCode fromValue(String v) {
        for (ForecastFrequencyCode c: ForecastFrequencyCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
