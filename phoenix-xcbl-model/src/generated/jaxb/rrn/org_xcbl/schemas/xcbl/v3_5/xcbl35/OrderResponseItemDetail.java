//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderResponseItemDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderResponseItemDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ItemDetailResponseCoded" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}DetailResponseCode"/>
 *         &lt;element name="ItemDetailResponseCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ItemStatusEvent" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Status"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ShipmentStatusEvent" minOccurs="0"/>
 *         &lt;element name="PaymentStatusEvent" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Status"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PriceErrorInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AvailabilityErrorInfo" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfErrorInfo" minOccurs="0"/>
 *         &lt;element name="TrackingURL" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="OriginalItemDetail">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ItemDetail"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ChangeOrderItemDetail"/>
 *         &lt;/choice>
 *         &lt;element name="ItemDetailChanges" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ItemDetail"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="LineItemNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfStructuredNote" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderResponseItemDetail", propOrder = {
    "itemDetailResponseCoded",
    "itemDetailResponseCodedOther",
    "itemStatusEvent",
    "shipmentStatusEvent",
    "paymentStatusEvent",
    "priceErrorInfo",
    "availabilityErrorInfo",
    "listOfErrorInfo",
    "trackingURL",
    "listOfReferenceCoded",
    "originalItemDetail",
    "changeOrderItemDetail",
    "itemDetailChanges",
    "lineItemNote",
    "listOfStructuredNote"
})
public class OrderResponseItemDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ItemDetailResponseCoded", required = true)
    protected DetailResponseCode itemDetailResponseCoded;
    @XmlElement(name = "ItemDetailResponseCodedOther")
    protected String itemDetailResponseCodedOther;
    @XmlElement(name = "ItemStatusEvent")
    protected OrderResponseItemDetail.ItemStatusEvent itemStatusEvent;
    @XmlElement(name = "ShipmentStatusEvent")
    protected ShipmentStatusEvent shipmentStatusEvent;
    @XmlElement(name = "PaymentStatusEvent")
    protected OrderResponseItemDetail.PaymentStatusEvent paymentStatusEvent;
    @XmlElement(name = "PriceErrorInfo")
    protected OrderResponseItemDetail.PriceErrorInfo priceErrorInfo;
    @XmlElement(name = "AvailabilityErrorInfo")
    protected OrderResponseItemDetail.AvailabilityErrorInfo availabilityErrorInfo;
    @XmlElement(name = "ListOfErrorInfo")
    protected ListOfErrorInfo listOfErrorInfo;
    @XmlElement(name = "TrackingURL")
    @XmlSchemaType(name = "anyURI")
    protected String trackingURL;
    @XmlElement(name = "ListOfReferenceCoded")
    protected ListOfReferenceCoded listOfReferenceCoded;
    @XmlElement(name = "OriginalItemDetail")
    protected OrderResponseItemDetail.OriginalItemDetail originalItemDetail;
    @XmlElement(name = "ChangeOrderItemDetail")
    protected ChangeOrderItemDetail changeOrderItemDetail;
    @XmlElement(name = "ItemDetailChanges")
    protected OrderResponseItemDetail.ItemDetailChanges itemDetailChanges;
    @XmlElement(name = "LineItemNote")
    protected String lineItemNote;
    @XmlElement(name = "ListOfStructuredNote")
    protected ListOfStructuredNote listOfStructuredNote;

    /**
     * Gets the value of the itemDetailResponseCoded property.
     * 
     * @return
     *     possible object is
     *     {@link DetailResponseCode }
     *     
     */
    public DetailResponseCode getItemDetailResponseCoded() {
        return itemDetailResponseCoded;
    }

    /**
     * Sets the value of the itemDetailResponseCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link DetailResponseCode }
     *     
     */
    public void setItemDetailResponseCoded(DetailResponseCode value) {
        this.itemDetailResponseCoded = value;
    }

    /**
     * Gets the value of the itemDetailResponseCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemDetailResponseCodedOther() {
        return itemDetailResponseCodedOther;
    }

    /**
     * Sets the value of the itemDetailResponseCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemDetailResponseCodedOther(String value) {
        this.itemDetailResponseCodedOther = value;
    }

    /**
     * Gets the value of the itemStatusEvent property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.ItemStatusEvent }
     *     
     */
    public OrderResponseItemDetail.ItemStatusEvent getItemStatusEvent() {
        return itemStatusEvent;
    }

    /**
     * Sets the value of the itemStatusEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.ItemStatusEvent }
     *     
     */
    public void setItemStatusEvent(OrderResponseItemDetail.ItemStatusEvent value) {
        this.itemStatusEvent = value;
    }

    /**
     * Gets the value of the shipmentStatusEvent property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentStatusEvent }
     *     
     */
    public ShipmentStatusEvent getShipmentStatusEvent() {
        return shipmentStatusEvent;
    }

    /**
     * Sets the value of the shipmentStatusEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentStatusEvent }
     *     
     */
    public void setShipmentStatusEvent(ShipmentStatusEvent value) {
        this.shipmentStatusEvent = value;
    }

    /**
     * Gets the value of the paymentStatusEvent property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.PaymentStatusEvent }
     *     
     */
    public OrderResponseItemDetail.PaymentStatusEvent getPaymentStatusEvent() {
        return paymentStatusEvent;
    }

    /**
     * Sets the value of the paymentStatusEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.PaymentStatusEvent }
     *     
     */
    public void setPaymentStatusEvent(OrderResponseItemDetail.PaymentStatusEvent value) {
        this.paymentStatusEvent = value;
    }

    /**
     * Gets the value of the priceErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.PriceErrorInfo }
     *     
     */
    public OrderResponseItemDetail.PriceErrorInfo getPriceErrorInfo() {
        return priceErrorInfo;
    }

    /**
     * Sets the value of the priceErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.PriceErrorInfo }
     *     
     */
    public void setPriceErrorInfo(OrderResponseItemDetail.PriceErrorInfo value) {
        this.priceErrorInfo = value;
    }

    /**
     * Gets the value of the availabilityErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.AvailabilityErrorInfo }
     *     
     */
    public OrderResponseItemDetail.AvailabilityErrorInfo getAvailabilityErrorInfo() {
        return availabilityErrorInfo;
    }

    /**
     * Sets the value of the availabilityErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.AvailabilityErrorInfo }
     *     
     */
    public void setAvailabilityErrorInfo(OrderResponseItemDetail.AvailabilityErrorInfo value) {
        this.availabilityErrorInfo = value;
    }

    /**
     * Gets the value of the listOfErrorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfErrorInfo }
     *     
     */
    public ListOfErrorInfo getListOfErrorInfo() {
        return listOfErrorInfo;
    }

    /**
     * Sets the value of the listOfErrorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfErrorInfo }
     *     
     */
    public void setListOfErrorInfo(ListOfErrorInfo value) {
        this.listOfErrorInfo = value;
    }

    /**
     * Gets the value of the trackingURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackingURL() {
        return trackingURL;
    }

    /**
     * Sets the value of the trackingURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackingURL(String value) {
        this.trackingURL = value;
    }

    /**
     * Gets the value of the listOfReferenceCoded property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfReferenceCoded }
     *     
     */
    public ListOfReferenceCoded getListOfReferenceCoded() {
        return listOfReferenceCoded;
    }

    /**
     * Sets the value of the listOfReferenceCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfReferenceCoded }
     *     
     */
    public void setListOfReferenceCoded(ListOfReferenceCoded value) {
        this.listOfReferenceCoded = value;
    }

    /**
     * Gets the value of the originalItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.OriginalItemDetail }
     *     
     */
    public OrderResponseItemDetail.OriginalItemDetail getOriginalItemDetail() {
        return originalItemDetail;
    }

    /**
     * Sets the value of the originalItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.OriginalItemDetail }
     *     
     */
    public void setOriginalItemDetail(OrderResponseItemDetail.OriginalItemDetail value) {
        this.originalItemDetail = value;
    }

    /**
     * Gets the value of the changeOrderItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ChangeOrderItemDetail }
     *     
     */
    public ChangeOrderItemDetail getChangeOrderItemDetail() {
        return changeOrderItemDetail;
    }

    /**
     * Sets the value of the changeOrderItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChangeOrderItemDetail }
     *     
     */
    public void setChangeOrderItemDetail(ChangeOrderItemDetail value) {
        this.changeOrderItemDetail = value;
    }

    /**
     * Gets the value of the itemDetailChanges property.
     * 
     * @return
     *     possible object is
     *     {@link OrderResponseItemDetail.ItemDetailChanges }
     *     
     */
    public OrderResponseItemDetail.ItemDetailChanges getItemDetailChanges() {
        return itemDetailChanges;
    }

    /**
     * Sets the value of the itemDetailChanges property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderResponseItemDetail.ItemDetailChanges }
     *     
     */
    public void setItemDetailChanges(OrderResponseItemDetail.ItemDetailChanges value) {
        this.itemDetailChanges = value;
    }

    /**
     * Gets the value of the lineItemNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemNote() {
        return lineItemNote;
    }

    /**
     * Sets the value of the lineItemNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemNote(String value) {
        this.lineItemNote = value;
    }

    /**
     * Gets the value of the listOfStructuredNote property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public ListOfStructuredNote getListOfStructuredNote() {
        return listOfStructuredNote;
    }

    /**
     * Sets the value of the listOfStructuredNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public void setListOfStructuredNote(ListOfStructuredNote value) {
        this.listOfStructuredNote = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorInfo"
    })
    public static class AvailabilityErrorInfo
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ErrorInfo", required = true)
        protected ErrorInfo errorInfo;

        /**
         * Gets the value of the errorInfo property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorInfo }
         *     
         */
        public ErrorInfo getErrorInfo() {
            return errorInfo;
        }

        /**
         * Sets the value of the errorInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorInfo }
         *     
         */
        public void setErrorInfo(ErrorInfo value) {
            this.errorInfo = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ItemDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itemDetail"
    })
    public static class ItemDetailChanges
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ItemDetail", required = true)
        protected ItemDetail itemDetail;

        /**
         * Gets the value of the itemDetail property.
         * 
         * @return
         *     possible object is
         *     {@link ItemDetail }
         *     
         */
        public ItemDetail getItemDetail() {
            return itemDetail;
        }

        /**
         * Sets the value of the itemDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemDetail }
         *     
         */
        public void setItemDetail(ItemDetail value) {
            this.itemDetail = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Status"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "status"
    })
    public static class ItemStatusEvent
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Status", required = true)
        protected Status status;

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link Status }
         *     
         */
        public Status getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link Status }
         *     
         */
        public void setStatus(Status value) {
            this.status = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ItemDetail"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "itemDetail"
    })
    public static class OriginalItemDetail
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ItemDetail", required = true)
        protected ItemDetail itemDetail;

        /**
         * Gets the value of the itemDetail property.
         * 
         * @return
         *     possible object is
         *     {@link ItemDetail }
         *     
         */
        public ItemDetail getItemDetail() {
            return itemDetail;
        }

        /**
         * Sets the value of the itemDetail property.
         * 
         * @param value
         *     allowed object is
         *     {@link ItemDetail }
         *     
         */
        public void setItemDetail(ItemDetail value) {
            this.itemDetail = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Status"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "status"
    })
    public static class PaymentStatusEvent
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Status", required = true)
        protected Status status;

        /**
         * Gets the value of the status property.
         * 
         * @return
         *     possible object is
         *     {@link Status }
         *     
         */
        public Status getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         * @param value
         *     allowed object is
         *     {@link Status }
         *     
         */
        public void setStatus(Status value) {
            this.status = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "errorInfo"
    })
    public static class PriceErrorInfo
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ErrorInfo", required = true)
        protected ErrorInfo errorInfo;

        /**
         * Gets the value of the errorInfo property.
         * 
         * @return
         *     possible object is
         *     {@link ErrorInfo }
         *     
         */
        public ErrorInfo getErrorInfo() {
            return errorInfo;
        }

        /**
         * Sets the value of the errorInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorInfo }
         *     
         */
        public void setErrorInfo(ErrorInfo value) {
            this.errorInfo = value;
        }

    }

}
