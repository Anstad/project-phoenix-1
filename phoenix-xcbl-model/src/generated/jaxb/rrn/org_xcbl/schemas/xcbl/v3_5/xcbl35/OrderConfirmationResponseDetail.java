//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderConfirmationResponseDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderConfirmationResponseDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OrderConfirmationItemNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BuyerOrderConfirmationItemNum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OrderConfirmationResponseCode" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}BasicResponseCode" minOccurs="0"/>
 *         &lt;element name="OrderConfirmationResponseCodedOther" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfAccountAssignment" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfStructuredNote" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ErrorInfo" minOccurs="0"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfNameValueSet" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderConfirmationResponseDetail", propOrder = {
    "orderConfirmationItemNum",
    "buyerOrderConfirmationItemNum",
    "orderConfirmationResponseCode",
    "orderConfirmationResponseCodedOther",
    "listOfAccountAssignment",
    "listOfStructuredNote",
    "errorInfo",
    "listOfNameValueSet"
})
public class OrderConfirmationResponseDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "OrderConfirmationItemNum", required = true)
    protected String orderConfirmationItemNum;
    @XmlElement(name = "BuyerOrderConfirmationItemNum", required = true)
    protected String buyerOrderConfirmationItemNum;
    @XmlElement(name = "OrderConfirmationResponseCode")
    protected BasicResponseCode orderConfirmationResponseCode;
    @XmlElement(name = "OrderConfirmationResponseCodedOther")
    protected String orderConfirmationResponseCodedOther;
    @XmlElement(name = "ListOfAccountAssignment")
    protected ListOfAccountAssignment listOfAccountAssignment;
    @XmlElement(name = "ListOfStructuredNote")
    protected ListOfStructuredNote listOfStructuredNote;
    @XmlElement(name = "ErrorInfo")
    protected ErrorInfo errorInfo;
    @XmlElement(name = "ListOfNameValueSet")
    protected ListOfNameValueSet listOfNameValueSet;

    /**
     * Gets the value of the orderConfirmationItemNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderConfirmationItemNum() {
        return orderConfirmationItemNum;
    }

    /**
     * Sets the value of the orderConfirmationItemNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderConfirmationItemNum(String value) {
        this.orderConfirmationItemNum = value;
    }

    /**
     * Gets the value of the buyerOrderConfirmationItemNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyerOrderConfirmationItemNum() {
        return buyerOrderConfirmationItemNum;
    }

    /**
     * Sets the value of the buyerOrderConfirmationItemNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyerOrderConfirmationItemNum(String value) {
        this.buyerOrderConfirmationItemNum = value;
    }

    /**
     * Gets the value of the orderConfirmationResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link BasicResponseCode }
     *     
     */
    public BasicResponseCode getOrderConfirmationResponseCode() {
        return orderConfirmationResponseCode;
    }

    /**
     * Sets the value of the orderConfirmationResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BasicResponseCode }
     *     
     */
    public void setOrderConfirmationResponseCode(BasicResponseCode value) {
        this.orderConfirmationResponseCode = value;
    }

    /**
     * Gets the value of the orderConfirmationResponseCodedOther property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderConfirmationResponseCodedOther() {
        return orderConfirmationResponseCodedOther;
    }

    /**
     * Sets the value of the orderConfirmationResponseCodedOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderConfirmationResponseCodedOther(String value) {
        this.orderConfirmationResponseCodedOther = value;
    }

    /**
     * Gets the value of the listOfAccountAssignment property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAccountAssignment }
     *     
     */
    public ListOfAccountAssignment getListOfAccountAssignment() {
        return listOfAccountAssignment;
    }

    /**
     * Sets the value of the listOfAccountAssignment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAccountAssignment }
     *     
     */
    public void setListOfAccountAssignment(ListOfAccountAssignment value) {
        this.listOfAccountAssignment = value;
    }

    /**
     * Gets the value of the listOfStructuredNote property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public ListOfStructuredNote getListOfStructuredNote() {
        return listOfStructuredNote;
    }

    /**
     * Sets the value of the listOfStructuredNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfStructuredNote }
     *     
     */
    public void setListOfStructuredNote(ListOfStructuredNote value) {
        this.listOfStructuredNote = value;
    }

    /**
     * Gets the value of the errorInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorInfo }
     *     
     */
    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }

    /**
     * Sets the value of the errorInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorInfo }
     *     
     */
    public void setErrorInfo(ErrorInfo value) {
        this.errorInfo = value;
    }

    /**
     * Gets the value of the listOfNameValueSet property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfNameValueSet }
     *     
     */
    public ListOfNameValueSet getListOfNameValueSet() {
        return listOfNameValueSet;
    }

    /**
     * Sets the value of the listOfNameValueSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfNameValueSet }
     *     
     */
    public void setListOfNameValueSet(ListOfNameValueSet value) {
        this.listOfNameValueSet = value;
    }

}
