//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvoiceResponseParty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvoiceResponseParty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoicingParty">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BillToParty">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RemitToParty" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfPartyCoded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceResponseParty", propOrder = {
    "invoicingParty",
    "billToParty",
    "remitToParty",
    "listOfPartyCoded"
})
public class InvoiceResponseParty
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "InvoicingParty", required = true)
    protected InvoiceResponseParty.InvoicingParty invoicingParty;
    @XmlElement(name = "BillToParty", required = true)
    protected InvoiceResponseParty.BillToParty billToParty;
    @XmlElement(name = "RemitToParty")
    protected InvoiceResponseParty.RemitToParty remitToParty;
    @XmlElement(name = "ListOfPartyCoded")
    protected ListOfPartyCoded listOfPartyCoded;

    /**
     * Gets the value of the invoicingParty property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceResponseParty.InvoicingParty }
     *     
     */
    public InvoiceResponseParty.InvoicingParty getInvoicingParty() {
        return invoicingParty;
    }

    /**
     * Sets the value of the invoicingParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceResponseParty.InvoicingParty }
     *     
     */
    public void setInvoicingParty(InvoiceResponseParty.InvoicingParty value) {
        this.invoicingParty = value;
    }

    /**
     * Gets the value of the billToParty property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceResponseParty.BillToParty }
     *     
     */
    public InvoiceResponseParty.BillToParty getBillToParty() {
        return billToParty;
    }

    /**
     * Sets the value of the billToParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceResponseParty.BillToParty }
     *     
     */
    public void setBillToParty(InvoiceResponseParty.BillToParty value) {
        this.billToParty = value;
    }

    /**
     * Gets the value of the remitToParty property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceResponseParty.RemitToParty }
     *     
     */
    public InvoiceResponseParty.RemitToParty getRemitToParty() {
        return remitToParty;
    }

    /**
     * Sets the value of the remitToParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceResponseParty.RemitToParty }
     *     
     */
    public void setRemitToParty(InvoiceResponseParty.RemitToParty value) {
        this.remitToParty = value;
    }

    /**
     * Gets the value of the listOfPartyCoded property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfPartyCoded }
     *     
     */
    public ListOfPartyCoded getListOfPartyCoded() {
        return listOfPartyCoded;
    }

    /**
     * Sets the value of the listOfPartyCoded property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfPartyCoded }
     *     
     */
    public void setListOfPartyCoded(ListOfPartyCoded value) {
        this.listOfPartyCoded = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "party"
    })
    public static class BillToParty
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Party", required = true)
        protected Party party;

        /**
         * Gets the value of the party property.
         * 
         * @return
         *     possible object is
         *     {@link Party }
         *     
         */
        public Party getParty() {
            return party;
        }

        /**
         * Sets the value of the party property.
         * 
         * @param value
         *     allowed object is
         *     {@link Party }
         *     
         */
        public void setParty(Party value) {
            this.party = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "party"
    })
    public static class InvoicingParty
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Party", required = true)
        protected Party party;

        /**
         * Gets the value of the party property.
         * 
         * @return
         *     possible object is
         *     {@link Party }
         *     
         */
        public Party getParty() {
            return party;
        }

        /**
         * Sets the value of the party property.
         * 
         * @param value
         *     allowed object is
         *     {@link Party }
         *     
         */
        public void setParty(Party value) {
            this.party = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Party"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "party"
    })
    public static class RemitToParty
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Party", required = true)
        protected Party party;

        /**
         * Gets the value of the party property.
         * 
         * @return
         *     possible object is
         *     {@link Party }
         *     
         */
        public Party getParty() {
            return party;
        }

        /**
         * Sets the value of the party property.
         * 
         * @param value
         *     allowed object is
         *     {@link Party }
         *     
         */
        public void setParty(Party value) {
            this.party = value;
        }

    }

}
