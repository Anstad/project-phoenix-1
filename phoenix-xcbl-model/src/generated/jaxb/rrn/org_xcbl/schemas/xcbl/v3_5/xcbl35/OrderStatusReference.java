//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrderStatusReference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderStatusReference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountCode" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="BuyerReferenceNumber">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SellerReferenceNumber">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OtherReference" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="OrderDate" type="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}xcblDatetime"/>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfOrderStatusItem" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderStatusReference", propOrder = {
    "accountCode",
    "buyerReferenceNumber",
    "sellerReferenceNumber",
    "otherReference",
    "orderDate",
    "listOfOrderStatusItem"
})
public class OrderStatusReference
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "AccountCode")
    protected OrderStatusReference.AccountCode accountCode;
    @XmlElement(name = "BuyerReferenceNumber", required = true)
    protected OrderStatusReference.BuyerReferenceNumber buyerReferenceNumber;
    @XmlElement(name = "SellerReferenceNumber", required = true)
    protected OrderStatusReference.SellerReferenceNumber sellerReferenceNumber;
    @XmlElement(name = "OtherReference")
    protected OrderStatusReference.OtherReference otherReference;
    @XmlElement(name = "OrderDate", required = true)
    protected String orderDate;
    @XmlElement(name = "ListOfOrderStatusItem")
    protected ListOfOrderStatusItem listOfOrderStatusItem;

    /**
     * Gets the value of the accountCode property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatusReference.AccountCode }
     *     
     */
    public OrderStatusReference.AccountCode getAccountCode() {
        return accountCode;
    }

    /**
     * Sets the value of the accountCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatusReference.AccountCode }
     *     
     */
    public void setAccountCode(OrderStatusReference.AccountCode value) {
        this.accountCode = value;
    }

    /**
     * Gets the value of the buyerReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatusReference.BuyerReferenceNumber }
     *     
     */
    public OrderStatusReference.BuyerReferenceNumber getBuyerReferenceNumber() {
        return buyerReferenceNumber;
    }

    /**
     * Sets the value of the buyerReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatusReference.BuyerReferenceNumber }
     *     
     */
    public void setBuyerReferenceNumber(OrderStatusReference.BuyerReferenceNumber value) {
        this.buyerReferenceNumber = value;
    }

    /**
     * Gets the value of the sellerReferenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatusReference.SellerReferenceNumber }
     *     
     */
    public OrderStatusReference.SellerReferenceNumber getSellerReferenceNumber() {
        return sellerReferenceNumber;
    }

    /**
     * Sets the value of the sellerReferenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatusReference.SellerReferenceNumber }
     *     
     */
    public void setSellerReferenceNumber(OrderStatusReference.SellerReferenceNumber value) {
        this.sellerReferenceNumber = value;
    }

    /**
     * Gets the value of the otherReference property.
     * 
     * @return
     *     possible object is
     *     {@link OrderStatusReference.OtherReference }
     *     
     */
    public OrderStatusReference.OtherReference getOtherReference() {
        return otherReference;
    }

    /**
     * Sets the value of the otherReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderStatusReference.OtherReference }
     *     
     */
    public void setOtherReference(OrderStatusReference.OtherReference value) {
        this.otherReference = value;
    }

    /**
     * Gets the value of the orderDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * Sets the value of the orderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderDate(String value) {
        this.orderDate = value;
    }

    /**
     * Gets the value of the listOfOrderStatusItem property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfOrderStatusItem }
     *     
     */
    public ListOfOrderStatusItem getListOfOrderStatusItem() {
        return listOfOrderStatusItem;
    }

    /**
     * Sets the value of the listOfOrderStatusItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfOrderStatusItem }
     *     
     */
    public void setListOfOrderStatusItem(ListOfOrderStatusItem value) {
        this.listOfOrderStatusItem = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class AccountCode
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class BuyerReferenceNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfReferenceCoded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "listOfReferenceCoded"
    })
    public static class OtherReference
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "ListOfReferenceCoded", required = true)
        protected ListOfReferenceCoded listOfReferenceCoded;

        /**
         * Gets the value of the listOfReferenceCoded property.
         * 
         * @return
         *     possible object is
         *     {@link ListOfReferenceCoded }
         *     
         */
        public ListOfReferenceCoded getListOfReferenceCoded() {
            return listOfReferenceCoded;
        }

        /**
         * Sets the value of the listOfReferenceCoded property.
         * 
         * @param value
         *     allowed object is
         *     {@link ListOfReferenceCoded }
         *     
         */
        public void setListOfReferenceCoded(ListOfReferenceCoded value) {
            this.listOfReferenceCoded = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}Reference"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reference"
    })
    public static class SellerReferenceNumber
        implements Serializable
    {

        private final static long serialVersionUID = 1L;
        @XmlElement(name = "Reference", required = true)
        protected Reference reference;

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link Reference }
         *     
         */
        public Reference getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link Reference }
         *     
         */
        public void setReference(Reference value) {
            this.reference = value;
        }

    }

}
