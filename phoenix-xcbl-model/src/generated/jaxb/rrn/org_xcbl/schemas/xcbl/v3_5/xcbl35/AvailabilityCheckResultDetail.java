//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.5-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.08.13 at 10:13:13 PM EDT 
//


package rrn.org_xcbl.schemas.xcbl.v3_5.xcbl35;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AvailabilityCheckResultDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AvailabilityCheckResultDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{rrn:org.xcbl:schemas/xcbl/v3_5/xcbl35.xsd}ListOfAvailabilityCheckResultItemDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AvailabilityCheckResultDetail", propOrder = {
    "listOfAvailabilityCheckResultItemDetail"
})
public class AvailabilityCheckResultDetail
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(name = "ListOfAvailabilityCheckResultItemDetail", required = true)
    protected ListOfAvailabilityCheckResultItemDetail listOfAvailabilityCheckResultItemDetail;

    /**
     * Gets the value of the listOfAvailabilityCheckResultItemDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ListOfAvailabilityCheckResultItemDetail }
     *     
     */
    public ListOfAvailabilityCheckResultItemDetail getListOfAvailabilityCheckResultItemDetail() {
        return listOfAvailabilityCheckResultItemDetail;
    }

    /**
     * Sets the value of the listOfAvailabilityCheckResultItemDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListOfAvailabilityCheckResultItemDetail }
     *     
     */
    public void setListOfAvailabilityCheckResultItemDetail(ListOfAvailabilityCheckResultItemDetail value) {
        this.listOfAvailabilityCheckResultItemDetail = value;
    }

}
