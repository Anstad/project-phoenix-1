package com.apd.phoenix.service.payment.ws;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for LineItemDetailTempServices complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="LineItemDetailTempServices">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}LineItemDetail">
 *       &lt;sequence>
 *         &lt;element name="CostCenter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmployeeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FlatRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NumberOfOvertimeHours" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="NumberOfRegularHours" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OvertimeHourlyRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PositionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProjectDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegularHourlyRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RequestorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="Supervisor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemDetailTempServices", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "costCenter", "employeeId", "employeeName", "endDate", "flatRate", "numberOfOvertimeHours",
        "numberOfRegularHours", "overtimeHourlyRate", "phoneNumber", "positionCode", "projectCode",
        "projectDescription", "regularHourlyRate", "requestorId", "startDate", "supervisor", "trackingNumber",
        "unionCode", "unitOfMeasure" })
public class LineItemDetailTempServices extends LineItemDetail {

    @XmlElementRef(name = "CostCenter", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> costCenter;
    @XmlElementRef(name = "EmployeeId", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employeeId;
    @XmlElementRef(name = "EmployeeName", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> employeeName;
    @XmlElement(name = "EndDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar endDate;
    @XmlElement(name = "FlatRate")
    protected BigDecimal flatRate;
    @XmlElement(name = "NumberOfOvertimeHours")
    protected Double numberOfOvertimeHours;
    @XmlElement(name = "NumberOfRegularHours")
    protected Double numberOfRegularHours;
    @XmlElement(name = "OvertimeHourlyRate")
    protected BigDecimal overtimeHourlyRate;
    @XmlElementRef(name = "PhoneNumber", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> phoneNumber;
    @XmlElementRef(name = "PositionCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> positionCode;
    @XmlElementRef(name = "ProjectCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> projectCode;
    @XmlElementRef(name = "ProjectDescription", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> projectDescription;
    @XmlElement(name = "RegularHourlyRate")
    protected BigDecimal regularHourlyRate;
    @XmlElementRef(name = "RequestorId", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> requestorId;
    @XmlElement(name = "StartDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar startDate;
    @XmlElementRef(name = "Supervisor", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> supervisor;
    @XmlElementRef(name = "TrackingNumber", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trackingNumber;
    @XmlElementRef(name = "UnionCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unionCode;
    @XmlElementRef(name = "UnitOfMeasure", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> unitOfMeasure;

    /**
     * Gets the value of the costCenter property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getCostCenter() {
        return costCenter;
    }

    /**
     * Sets the value of the costCenter property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setCostCenter(JAXBElement<String> value) {
        this.costCenter = value;
    }

    /**
     * Gets the value of the employeeId property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setEmployeeId(JAXBElement<String> value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the employeeName property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getEmployeeName() {
        return employeeName;
    }

    /**
     * Sets the value of the employeeName property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setEmployeeName(JAXBElement<String> value) {
        this.employeeName = value;
    }

    /**
     * Gets the value of the endDate property.
     *
     * @return possible object is {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     *
     * @param value allowed object is {@link XMLGregorianCalendar }
     *
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the flatRate property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getFlatRate() {
        return flatRate;
    }

    /**
     * Sets the value of the flatRate property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setFlatRate(BigDecimal value) {
        this.flatRate = value;
    }

    /**
     * Gets the value of the numberOfOvertimeHours property.
     *
     * @return possible object is {@link Double }
     *
     */
    public Double getNumberOfOvertimeHours() {
        return numberOfOvertimeHours;
    }

    /**
     * Sets the value of the numberOfOvertimeHours property.
     *
     * @param value allowed object is {@link Double }
     *
     */
    public void setNumberOfOvertimeHours(Double value) {
        this.numberOfOvertimeHours = value;
    }

    /**
     * Gets the value of the numberOfRegularHours property.
     *
     * @return possible object is {@link Double }
     *
     */
    public Double getNumberOfRegularHours() {
        return numberOfRegularHours;
    }

    /**
     * Sets the value of the numberOfRegularHours property.
     *
     * @param value allowed object is {@link Double }
     *
     */
    public void setNumberOfRegularHours(Double value) {
        this.numberOfRegularHours = value;
    }

    /**
     * Gets the value of the overtimeHourlyRate property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getOvertimeHourlyRate() {
        return overtimeHourlyRate;
    }

    /**
     * Sets the value of the overtimeHourlyRate property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setOvertimeHourlyRate(BigDecimal value) {
        this.overtimeHourlyRate = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setPhoneNumber(JAXBElement<String> value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the positionCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getPositionCode() {
        return positionCode;
    }

    /**
     * Sets the value of the positionCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setPositionCode(JAXBElement<String> value) {
        this.positionCode = value;
    }

    /**
     * Gets the value of the projectCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getProjectCode() {
        return projectCode;
    }

    /**
     * Sets the value of the projectCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setProjectCode(JAXBElement<String> value) {
        this.projectCode = value;
    }

    /**
     * Gets the value of the projectDescription property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getProjectDescription() {
        return projectDescription;
    }

    /**
     * Sets the value of the projectDescription property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setProjectDescription(JAXBElement<String> value) {
        this.projectDescription = value;
    }

    /**
     * Gets the value of the regularHourlyRate property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getRegularHourlyRate() {
        return regularHourlyRate;
    }

    /**
     * Sets the value of the regularHourlyRate property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setRegularHourlyRate(BigDecimal value) {
        this.regularHourlyRate = value;
    }

    /**
     * Gets the value of the requestorId property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getRequestorId() {
        return requestorId;
    }

    /**
     * Sets the value of the requestorId property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setRequestorId(JAXBElement<String> value) {
        this.requestorId = value;
    }

    /**
     * Gets the value of the startDate property.
     *
     * @return possible object is {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     *
     * @param value allowed object is {@link XMLGregorianCalendar }
     *
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the supervisor property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getSupervisor() {
        return supervisor;
    }

    /**
     * Sets the value of the supervisor property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setSupervisor(JAXBElement<String> value) {
        this.supervisor = value;
    }

    /**
     * Gets the value of the trackingNumber property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Sets the value of the trackingNumber property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setTrackingNumber(JAXBElement<String> value) {
        this.trackingNumber = value;
    }

    /**
     * Gets the value of the unionCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getUnionCode() {
        return unionCode;
    }

    /**
     * Sets the value of the unionCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setUnionCode(JAXBElement<String> value) {
        this.unionCode = value;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setUnitOfMeasure(JAXBElement<String> value) {
        this.unitOfMeasure = value;
    }
}
