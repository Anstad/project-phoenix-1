package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CardSecurityCodeResponse.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="CardSecurityCodeResponse"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="Matched"/> &lt;enumeration
 * value="NotMatched"/> &lt;enumeration value="ShouldBeOnCard"/> &lt;enumeration
 * value="Unavailable"/> &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CardSecurityCodeResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CardSecurityCodeResponse {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Matched")
    MATCHED("Matched"), @XmlEnumValue("NotMatched")
    NOT_MATCHED("NotMatched"), @XmlEnumValue("ShouldBeOnCard")
    SHOULD_BE_ON_CARD("ShouldBeOnCard"), @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable");

    private final String value;

    CardSecurityCodeResponse(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CardSecurityCodeResponse fromValue(String v) {
        for (CardSecurityCodeResponse c : CardSecurityCodeResponse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
