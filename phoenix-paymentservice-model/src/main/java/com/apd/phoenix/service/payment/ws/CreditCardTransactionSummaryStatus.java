package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionSummaryStatus.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="CreditCardTransactionSummaryStatus"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="Authorized"/> &lt;enumeration
 * value="Submitted"/> &lt;enumeration value="Voided"/> &lt;enumeration
 * value="Verified"/> &lt;enumeration value="Declined"/> &lt;enumeration
 * value="InProgress"/> &lt;enumeration value="Failed"/> &lt;enumeration
 * value="Settling"/> &lt;enumeration value="Processed"/> &lt;enumeration
 * value="NeedsReview"/> &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CreditCardTransactionSummaryStatus", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CreditCardTransactionSummaryStatus {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Authorized")
    AUTHORIZED("Authorized"), @XmlEnumValue("Submitted")
    SUBMITTED("Submitted"), @XmlEnumValue("Voided")
    VOIDED("Voided"), @XmlEnumValue("Verified")
    VERIFIED("Verified"), @XmlEnumValue("Declined")
    DECLINED("Declined"), @XmlEnumValue("InProgress")
    IN_PROGRESS("InProgress"), @XmlEnumValue("Failed")
    FAILED("Failed"), @XmlEnumValue("Settling")
    SETTLING("Settling"), @XmlEnumValue("Processed")
    PROCESSED("Processed"), @XmlEnumValue("NeedsReview")
    NEEDS_REVIEW("NeedsReview");

    private final String value;

    CreditCardTransactionSummaryStatus(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardTransactionSummaryStatus fromValue(String v) {
        for (CreditCardTransactionSummaryStatus c : CreditCardTransactionSummaryStatus.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
