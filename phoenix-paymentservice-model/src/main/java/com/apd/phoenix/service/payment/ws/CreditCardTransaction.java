package com.apd.phoenix.service.payment.ws;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * <p>
 * Java class for CreditCardTransaction complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransaction">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdditionalAmounts" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}AdditionalAmounts" minOccurs="0"/>
 *         &lt;element name="CreditCard" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionCreditCard" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}CurrencyCode" minOccurs="0"/>
 *         &lt;element name="CustomerReferenceValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InvoiceDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineDetail" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}ArrayOfLineItemDetail" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReceiptEmailAddress" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}ArrayOfEmail" minOccurs="0"/>
 *         &lt;element name="ShipFromPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StoredCardIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}StoredCardIdentifier" minOccurs="0"/>
 *         &lt;element name="TotalAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TransactionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UserDefinedFields" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransaction", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "additionalAmounts", "creditCard", "currencyCode", "customerReferenceValue", "invoiceDate", "invoiceNumber",
        "lineDetail", "note", "orderNumber", "receiptEmailAddress", "shipFromPostalCode", "storedCardIdentifier",
        "totalAmount", "transactionKey", "userDefinedFields" })
public class CreditCardTransaction {

    @XmlElementRef(name = "AdditionalAmounts", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<AdditionalAmounts> additionalAmounts;
    @XmlElementRef(name = "CreditCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionCreditCard> creditCard;
    @XmlElement(name = "CurrencyCode")
    protected CurrencyCode currencyCode;
    @XmlElementRef(name = "CustomerReferenceValue", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerReferenceValue;
    @XmlElement(name = "InvoiceDate")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar invoiceDate;
    @XmlElementRef(name = "InvoiceNumber", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> invoiceNumber;
    @XmlElementRef(name = "LineDetail", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfLineItemDetail> lineDetail;
    @XmlElementRef(name = "Note", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> note;
    @XmlElementRef(name = "OrderNumber", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orderNumber;
    @XmlElementRef(name = "ReceiptEmailAddress", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfEmail> receiptEmailAddress;
    @XmlElementRef(name = "ShipFromPostalCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipFromPostalCode;
    @XmlElementRef(name = "StoredCardIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<StoredCardIdentifier> storedCardIdentifier;
    @XmlElement(name = "TotalAmount")
    protected BigDecimal totalAmount;
    @XmlElementRef(name = "TransactionKey", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionKey;
    @XmlElementRef(name = "UserDefinedFields", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> userDefinedFields;

    /**
     * Gets the value of the additionalAmounts property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link AdditionalAmounts }{@code >}
     *
     */
    public JAXBElement<AdditionalAmounts> getAdditionalAmounts() {
        return additionalAmounts;
    }

    /**
     * Sets the value of the additionalAmounts property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link AdditionalAmounts }{@code >}
     *
     */
    public void setAdditionalAmounts(JAXBElement<AdditionalAmounts> value) {
        this.additionalAmounts = value;
    }

    /**
     * Gets the value of the creditCard property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionCreditCard> getCreditCard() {
        return creditCard;
    }

    /**
     * Sets the value of the creditCard property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}
     *
     */
    public void setCreditCard(JAXBElement<CreditCardTransactionCreditCard> value) {
        this.creditCard = value;
    }

    /**
     * Gets the value of the currencyCode property.
     *
     * @return possible object is {@link CurrencyCode }
     *
     */
    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Sets the value of the currencyCode property.
     *
     * @param value allowed object is {@link CurrencyCode }
     *
     */
    public void setCurrencyCode(CurrencyCode value) {
        this.currencyCode = value;
    }

    /**
     * Gets the value of the customerReferenceValue property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getCustomerReferenceValue() {
        return customerReferenceValue;
    }

    /**
     * Sets the value of the customerReferenceValue property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setCustomerReferenceValue(JAXBElement<String> value) {
        this.customerReferenceValue = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     *
     * @return possible object is {@link XMLGregorianCalendar }
     *
     */
    public XMLGregorianCalendar getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     *
     * @param value allowed object is {@link XMLGregorianCalendar }
     *
     */
    public void setInvoiceDate(XMLGregorianCalendar value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setInvoiceNumber(JAXBElement<String> value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the lineDetail property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfLineItemDetail }{@code >}
     *
     */
    public JAXBElement<ArrayOfLineItemDetail> getLineDetail() {
        return lineDetail;
    }

    /**
     * Sets the value of the lineDetail property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link ArrayOfLineItemDetail }{@code >}
     *
     */
    public void setLineDetail(JAXBElement<ArrayOfLineItemDetail> value) {
        this.lineDetail = value;
    }

    /**
     * Gets the value of the note property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setNote(JAXBElement<String> value) {
        this.note = value;
    }

    /**
     * Gets the value of the orderNumber property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setOrderNumber(JAXBElement<String> value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the receiptEmailAddress property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}
     *
     */
    public JAXBElement<ArrayOfEmail> getReceiptEmailAddress() {
        return receiptEmailAddress;
    }

    /**
     * Sets the value of the receiptEmailAddress property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link ArrayOfEmail }{@code >}
     *
     */
    public void setReceiptEmailAddress(JAXBElement<ArrayOfEmail> value) {
        this.receiptEmailAddress = value;
    }

    /**
     * Gets the value of the shipFromPostalCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getShipFromPostalCode() {
        return shipFromPostalCode;
    }

    /**
     * Sets the value of the shipFromPostalCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setShipFromPostalCode(JAXBElement<String> value) {
        this.shipFromPostalCode = value;
    }

    /**
     * Gets the value of the storedCardIdentifier property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}
     *
     */
    public JAXBElement<StoredCardIdentifier> getStoredCardIdentifier() {
        return storedCardIdentifier;
    }

    /**
     * Sets the value of the storedCardIdentifier property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}
     *
     */
    public void setStoredCardIdentifier(JAXBElement<StoredCardIdentifier> value) {
        this.storedCardIdentifier = value;
    }

    /**
     * Gets the value of the totalAmount property.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setTotalAmount(BigDecimal value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the transactionKey property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getTransactionKey() {
        return transactionKey;
    }

    /**
     * Sets the value of the transactionKey property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setTransactionKey(JAXBElement<String> value) {
        this.transactionKey = value;
    }

    /**
     * Gets the value of the userDefinedFields property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *
     */
    public JAXBElement<ArrayOfstring> getUserDefinedFields() {
        return userDefinedFields;
    }

    /**
     * Sets the value of the userDefinedFields property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *
     */
    public void setUserDefinedFields(JAXBElement<ArrayOfstring> value) {
        this.userDefinedFields = value;
    }
}
