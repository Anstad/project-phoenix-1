package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomerResult" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions}CustomerCreditCardManagementResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "getCustomerResult" })
@XmlRootElement(name = "GetCustomerResponse")
public class GetCustomerResponse {

    @XmlElementRef(name = "GetCustomerResult", namespace = "http://3DSI.org/WebServices/CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerCreditCardManagementResult> getCustomerResult;

    /**
     * Gets the value of the getCustomerResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerCreditCardManagementResult }{@code >}
     *     
     */
    public JAXBElement<CustomerCreditCardManagementResult> getGetCustomerResult() {
        return getCustomerResult;
    }

    /**
     * Sets the value of the getCustomerResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerCreditCardManagementResult }{@code >}
     *     
     */
    public void setGetCustomerResult(JAXBElement<CustomerCreditCardManagementResult> value) {
        this.getCustomerResult = value;
    }

}
