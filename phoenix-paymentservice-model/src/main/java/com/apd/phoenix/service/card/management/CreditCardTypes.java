package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CreditCardTypes.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CreditCardTypes">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Visa"/>
 *     &lt;enumeration value="MasterCard"/>
 *     &lt;enumeration value="AmericanExpress"/>
 *     &lt;enumeration value="Discover"/>
 *     &lt;enumeration value="Diners"/>
 *     &lt;enumeration value="JCB"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CreditCardTypes", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums")
@XmlEnum
public enum CreditCardTypes {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Visa")
    VISA("Visa"), @XmlEnumValue("MasterCard")
    MASTER_CARD("MasterCard"), @XmlEnumValue("AmericanExpress")
    AMERICAN_EXPRESS("AmericanExpress"), @XmlEnumValue("Discover")
    DISCOVER("Discover"), @XmlEnumValue("Diners")
    DINERS("Diners"), JCB("JCB");

    private final String value;

    CreditCardTypes(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CreditCardTypes fromValue(String v) {
        for (CreditCardTypes c : CreditCardTypes.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
