package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CardSecurityCodeIndicator.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="CardSecurityCodeIndicator"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="Provided"/> &lt;enumeration
 * value="NotProvided"/> &lt;enumeration value="Illegible"/> &lt;enumeration
 * value="NotOnCard"/> &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CardSecurityCodeIndicator", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CardSecurityCodeIndicator {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Provided")
    PROVIDED("Provided"), @XmlEnumValue("NotProvided")
    NOT_PROVIDED("NotProvided"), @XmlEnumValue("Illegible")
    ILLEGIBLE("Illegible"), @XmlEnumValue("NotOnCard")
    NOT_ON_CARD("NotOnCard");

    private final String value;

    CardSecurityCodeIndicator(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CardSecurityCodeIndicator fromValue(String v) {
        for (CardSecurityCodeIndicator c : CardSecurityCodeIndicator.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
