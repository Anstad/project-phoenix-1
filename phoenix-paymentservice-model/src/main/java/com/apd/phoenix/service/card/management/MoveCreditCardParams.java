package com.apd.phoenix.service.card.management;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for MoveCreditCardParams complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MoveCreditCardParams">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement}CreditCardManagementParams">
 *       &lt;sequence>
 *         &lt;element name="CurrentCustomerIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CustomerIdentifier" minOccurs="0"/>
 *         &lt;element name="NewCustomerIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}CustomerIdentifier" minOccurs="0"/>
 *         &lt;element name="Token" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MoveCreditCardParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", propOrder = {
        "currentCustomerIdentifier", "newCustomerIdentifier", "token" })
public class MoveCreditCardParams extends CreditCardManagementParams {

    @XmlElementRef(name = "CurrentCustomerIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerIdentifier> currentCustomerIdentifier;
    @XmlElementRef(name = "NewCustomerIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomerIdentifier> newCustomerIdentifier;
    @XmlElementRef(name = "Token", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardManagement", type = JAXBElement.class, required = false)
    protected JAXBElement<String> token;

    /**
     * Gets the value of the currentCustomerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public JAXBElement<CustomerIdentifier> getCurrentCustomerIdentifier() {
        return currentCustomerIdentifier;
    }

    /**
     * Sets the value of the currentCustomerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public void setCurrentCustomerIdentifier(JAXBElement<CustomerIdentifier> value) {
        this.currentCustomerIdentifier = value;
    }

    /**
     * Gets the value of the newCustomerIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public JAXBElement<CustomerIdentifier> getNewCustomerIdentifier() {
        return newCustomerIdentifier;
    }

    /**
     * Sets the value of the newCustomerIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomerIdentifier }{@code >}
     *     
     */
    public void setNewCustomerIdentifier(JAXBElement<CustomerIdentifier> value) {
        this.newCustomerIdentifier = value;
    }

    /**
     * Gets the value of the token property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getToken() {
        return token;
    }

    /**
     * Sets the value of the token property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setToken(JAXBElement<String> value) {
        this.token = value;
    }

}
