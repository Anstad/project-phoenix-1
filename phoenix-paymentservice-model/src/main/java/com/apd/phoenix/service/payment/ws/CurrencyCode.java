package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CurrencyCode.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="CurrencyCode"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="CanadianDollars"/> &lt;enumeration
 * value="USDollars"/> &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "CurrencyCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CurrencyCode {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("CanadianDollars")
    CANADIAN_DOLLARS("CanadianDollars"), @XmlEnumValue("USDollars")
    US_DOLLARS("USDollars");

    private final String value;

    CurrencyCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CurrencyCode fromValue(String v) {
        for (CurrencyCode c : CurrencyCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
