package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AvsResponse.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="AvsResponse"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="Matched"/> &lt;enumeration
 * value="NotMatched"/> &lt;enumeration value="Unavailable"/> &lt;enumeration
 * value="Error"/> &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "AvsResponse", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum AvsResponse {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Matched")
    MATCHED("Matched"), @XmlEnumValue("NotMatched")
    NOT_MATCHED("NotMatched"), @XmlEnumValue("Unavailable")
    UNAVAILABLE("Unavailable"), @XmlEnumValue("Error")
    ERROR("Error");

    private final String value;

    AvsResponse(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AvsResponse fromValue(String v) {
        for (AvsResponse c : AvsResponse.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
