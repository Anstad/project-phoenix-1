package com.apd.phoenix.service.card.management;

import com.apd.phoenix.service.payment.utilities.PaymentGatewayPropertiesLoader;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class was generated by Apache CXF 2.7.6 2013-11-18T15:15:16.617-05:00
 * Generated source version: 2.7.6
 *
 */
@WebServiceClient(name = "CreditCardManagementService", wsdlLocation = "https://services.PWSDemo.com/CreditCardManagementService.svc", targetNamespace = "http://3DSI.org/WebServices/CreditCardManagement")
public class CreditCardManagementService extends Service {

    private static final Logger logger = LoggerFactory.getLogger(CreditCardManagementService.class);
    public final static URL WSDL_LOCATION;
    public static final String SERVICE_URL = PaymentGatewayPropertiesLoader.getInstance().getProperties().getString(
            "cardManagementServiceUrl");
    private static final String SHOULD_LOG_MESSAGES = PaymentGatewayPropertiesLoader.getInstance().getProperties()
            .getString("logTransactionContent", "false");
    public final static QName SERVICE = new QName("http://3DSI.org/WebServices/CreditCardManagement",
            "CreditCardManagementService");
    public final static QName CreditCardManagementSoap = new QName("http://3DSI.org/WebServices/CreditCardManagement",
            "CreditCardManagementSoap");

    static {
        URL url = null;
        url = CreditCardManagementService.class.getClassLoader().getResource("wsdl/CardManagementService.wsdl");
        WSDL_LOCATION = url;
    }

    public CreditCardManagementService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public CreditCardManagementService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public CreditCardManagementService() {
        super(WSDL_LOCATION, SERVICE);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardManagementService(WebServiceFeature... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardManagementService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public CreditCardManagementService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return returns ICreditCardManagement
     */
    @WebEndpoint(name = "CreditCardManagementSoap")
    public ICreditCardManagement getCreditCardManagementSoap() {
        return super.getPort(CreditCardManagementSoap, ICreditCardManagement.class);
    }

    /**
     *
     * @param features A list of {@link javax.xml.ws.WebServiceFeature} to
     * configure on the proxy. Supported features not in the
     * <code>features</code> parameter will have their default values.
     * @return returns ICreditCardManagement
     */
    @WebEndpoint(name = "CreditCardManagementSoap")
    public ICreditCardManagement getCreditCardManagementSoap(WebServiceFeature... features) {
        return super.getPort(CreditCardManagementSoap, ICreditCardManagement.class, features);
    }

    public ICreditCardManagement getCreditCardManagementService() {
        JaxWsProxyFactoryBean proxyFactory = new JaxWsProxyFactoryBean();

        if ("true".equals(SHOULD_LOG_MESSAGES)) {
            proxyFactory.getInInterceptors().add(new LoggingInInterceptor());
            proxyFactory.getOutInterceptors().add(new LoggingOutInterceptor());
        }

        proxyFactory.setServiceClass(ICreditCardManagement.class);
        proxyFactory.setAddress(SERVICE_URL);
        ICreditCardManagement port = (ICreditCardManagement) proxyFactory.create();
        return port;
    }
}
