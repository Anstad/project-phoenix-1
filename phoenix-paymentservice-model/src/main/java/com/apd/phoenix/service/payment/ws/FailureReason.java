package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for FailureReason.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="FailureReason"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="ProcessorResponse"/> &lt;enumeration
 * value="ThirdPartyResponse"/> &lt;enumeration
 * value="ProcessorCommunicationError"/> &lt;enumeration
 * value="ThirdPartyCommunicationError"/> &lt;enumeration
 * value="BusinessRuleFailed"/> &lt;enumeration value="ValidationFailed"/>
 * &lt;enumeration value="TransactionNeedsReview"/> &lt;enumeration
 * value="PermissionDenied"/> &lt;enumeration value="ProcessingFailed"/>
 * &lt;enumeration value="TransactionDoesNotExist"/> &lt;enumeration
 * value="InvalidTransactionState"/> &lt;enumeration
 * value="CreditCardDoesNotExist"/> &lt;enumeration
 * value="DuplicateTransactionKey"/> &lt;enumeration
 * value="CardAccountInformationDoesNotMatch"/> &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "FailureReason", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums")
@XmlEnum
public enum FailureReason {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("ProcessorResponse")
    PROCESSOR_RESPONSE("ProcessorResponse"), @XmlEnumValue("ThirdPartyResponse")
    THIRD_PARTY_RESPONSE("ThirdPartyResponse"), @XmlEnumValue("ProcessorCommunicationError")
    PROCESSOR_COMMUNICATION_ERROR("ProcessorCommunicationError"), @XmlEnumValue("ThirdPartyCommunicationError")
    THIRD_PARTY_COMMUNICATION_ERROR("ThirdPartyCommunicationError"), @XmlEnumValue("BusinessRuleFailed")
    BUSINESS_RULE_FAILED("BusinessRuleFailed"), @XmlEnumValue("ValidationFailed")
    VALIDATION_FAILED("ValidationFailed"), @XmlEnumValue("TransactionNeedsReview")
    TRANSACTION_NEEDS_REVIEW("TransactionNeedsReview"), @XmlEnumValue("PermissionDenied")
    PERMISSION_DENIED("PermissionDenied"), @XmlEnumValue("ProcessingFailed")
    PROCESSING_FAILED("ProcessingFailed"), @XmlEnumValue("TransactionDoesNotExist")
    TRANSACTION_DOES_NOT_EXIST("TransactionDoesNotExist"), @XmlEnumValue("InvalidTransactionState")
    INVALID_TRANSACTION_STATE("InvalidTransactionState"), @XmlEnumValue("CreditCardDoesNotExist")
    CREDIT_CARD_DOES_NOT_EXIST("CreditCardDoesNotExist"), @XmlEnumValue("DuplicateTransactionKey")
    DUPLICATE_TRANSACTION_KEY("DuplicateTransactionKey"), @XmlEnumValue("CardAccountInformationDoesNotMatch")
    CARD_ACCOUNT_INFORMATION_DOES_NOT_MATCH("CardAccountInformationDoesNotMatch");

    private final String value;

    FailureReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FailureReason fromValue(String v) {
        for (FailureReason c : FailureReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
