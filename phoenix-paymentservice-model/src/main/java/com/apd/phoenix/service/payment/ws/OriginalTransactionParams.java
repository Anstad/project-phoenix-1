package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for OriginalTransactionParams complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="OriginalTransactionParams">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OriginalTransactionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TerminalIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}TerminalIdentifier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OriginalTransactionParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", propOrder = {
        "originalTransactionKey", "terminalIdentifier" })
@XmlSeeAlso( { GetTransactionStatusParams.class, QuickRefundParams.class, VoidParams.class, QuickForceParams.class,
        QuickCaptureParams.class })
public class OriginalTransactionParams {

    @XmlElementRef(name = "OriginalTransactionKey", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<String> originalTransactionKey;
    @XmlElementRef(name = "TerminalIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<TerminalIdentifier> terminalIdentifier;

    /**
     * Gets the value of the originalTransactionKey property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getOriginalTransactionKey() {
        return originalTransactionKey;
    }

    /**
     * Sets the value of the originalTransactionKey property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setOriginalTransactionKey(JAXBElement<String> value) {
        this.originalTransactionKey = value;
    }

    /**
     * Gets the value of the terminalIdentifier property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public JAXBElement<TerminalIdentifier> getTerminalIdentifier() {
        return terminalIdentifier;
    }

    /**
     * Sets the value of the terminalIdentifier property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public void setTerminalIdentifier(JAXBElement<TerminalIdentifier> value) {
        this.terminalIdentifier = value;
    }
}
