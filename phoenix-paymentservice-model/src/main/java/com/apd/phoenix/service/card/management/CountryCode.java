package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CountryCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CountryCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="UnitedStates"/>
 *     &lt;enumeration value="Canada"/>
 *     &lt;enumeration value="Mexico"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CountryCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum CountryCode {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("UnitedStates")
    UNITED_STATES("UnitedStates"), @XmlEnumValue("Canada")
    CANADA("Canada"), @XmlEnumValue("Mexico")
    MEXICO("Mexico");

    private final String value;

    CountryCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CountryCode fromValue(String v) {
        for (CountryCode c : CountryCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        return CountryCode.NONE;
    }

}
