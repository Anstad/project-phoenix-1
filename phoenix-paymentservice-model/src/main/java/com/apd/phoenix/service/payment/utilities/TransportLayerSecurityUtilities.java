/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.payment.utilities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dnorris
 */
public class TransportLayerSecurityUtilities {

    private static Logger logger = LoggerFactory.getLogger(TransportLayerSecurityUtilities.class);

    private static String keyPassword = PaymentGatewayPropertiesLoader.getInstance().getProperties().getString(
            "keyPassword");
    private static String keyStorePassword = PaymentGatewayPropertiesLoader.getInstance().getProperties().getString(
            "keyStorePassword");
    private static String keyStoreLoc = PaymentGatewayPropertiesLoader.getInstance().getProperties().getString(
            "keyStoreLoc");

    public static void setupTLS(Object port) throws FileNotFoundException, IOException, GeneralSecurityException {

        HTTPConduit httpConduit = (HTTPConduit) ClientProxy.getClient(port).getConduit();

        TLSClientParameters tlsCP = new TLSClientParameters();
        KeyStore keyStore = KeyStore.getInstance("JKS");
        try(InputStream keyStoreInput = TransportLayerSecurityUtilities.class.getClassLoader().getResourceAsStream(
                keyStoreLoc)){
        keyStore.load(keyStoreInput, keyStorePassword.toCharArray());
        KeyManager[] myKeyManagers = getKeyManagers(keyStore, keyPassword);
        tlsCP.setKeyManagers(myKeyManagers);
        } catch (IOException ex) {
            logger.error("Could not open keystore at " + keyStoreLoc);
        }
        KeyStore trustStore = KeyStore.getInstance("JKS");
        try (InputStream trustStoreInput = TransportLayerSecurityUtilities.class.getClassLoader().getResourceAsStream(
                keyStoreLoc)) {
            trustStore.load(trustStoreInput, keyStorePassword.toCharArray());
            TrustManager[] myTrustStoreKeyManagers = getTrustManagers(trustStore);
            tlsCP.setTrustManagers(myTrustStoreKeyManagers);
            httpConduit.setTlsClientParameters(tlsCP);
            HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
            if (httpConduit.getClient() != null) {
            	httpClientPolicy = httpConduit.getClient();
            }
            httpClientPolicy.setConnectionTimeout(36000);
            httpClientPolicy.setAllowChunking(false);
            httpConduit.setClient(httpClientPolicy);
        } catch (IOException ex) {
            logger.error("Could not open keystore at " + keyStoreLoc);
        }
    }

    public static TrustManager[] getTrustManagers(KeyStore trustStore) throws NoSuchAlgorithmException,
            KeyStoreException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory fac = TrustManagerFactory.getInstance(alg);
        fac.init(trustStore);
        return fac.getTrustManagers();
    }

    public static KeyManager[] getKeyManagers(KeyStore keyStore, String keyPassword) throws GeneralSecurityException,
            IOException {
        String alg = KeyManagerFactory.getDefaultAlgorithm();
        char[] keyPass = keyPassword != null ? keyPassword.toCharArray() : null;
        KeyManagerFactory fac = KeyManagerFactory.getInstance(alg);
        fac.init(keyStore, keyPass);
        return fac.getKeyManagers();
    }
}
