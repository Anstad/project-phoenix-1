package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for TransactionType.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 * <p>
 * <
 * pre>
 * &lt;simpleType name="TransactionType"> &lt;restriction
 * base="{http://www.w3.org/2001/XMLSchema}string"> &lt;enumeration
 * value="None"/> &lt;enumeration value="AuthorizeAndCapture"/> &lt;enumeration
 * value="AuthorizeOnly"/> &lt;enumeration value="Refund"/> &lt;enumeration
 * value="Force"/> &lt;enumeration value="QuickForce"/> &lt;enumeration
 * value="Void"/> &lt;enumeration value="Capture"/> &lt;enumeration
 * value="QuickCapture"/> &lt;enumeration value="Credit"/> &lt;enumeration
 * value="QuickRefund"/> &lt;enumeration value="CardVerification"/>
 * &lt;/restriction> &lt;/simpleType>
 * </pre>
 *
 */
@XmlType(name = "TransactionType", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum TransactionType {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("AuthorizeAndCapture")
    AUTHORIZE_AND_CAPTURE("AuthorizeAndCapture"), @XmlEnumValue("AuthorizeOnly")
    AUTHORIZE_ONLY("AuthorizeOnly"), @XmlEnumValue("Refund")
    REFUND("Refund"), @XmlEnumValue("Force")
    FORCE("Force"), @XmlEnumValue("QuickForce")
    QUICK_FORCE("QuickForce"), @XmlEnumValue("Void")
    VOID("Void"), @XmlEnumValue("Capture")
    CAPTURE("Capture"), @XmlEnumValue("QuickCapture")
    QUICK_CAPTURE("QuickCapture"), @XmlEnumValue("Credit")
    CREDIT("Credit"), @XmlEnumValue("QuickRefund")
    QUICK_REFUND("QuickRefund"), @XmlEnumValue("CardVerification")
    CARD_VERIFICATION("CardVerification");

    private final String value;

    TransactionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TransactionType fromValue(String v) {
        for (TransactionType c : TransactionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
