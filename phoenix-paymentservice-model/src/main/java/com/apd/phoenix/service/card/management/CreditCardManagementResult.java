package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for CreditCardManagementResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreditCardManagementResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}WebServiceResult">
 *       &lt;sequence>
 *         &lt;element name="FailureReason" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Enums}CreditCardManagementFailureReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardManagementResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardManagementDefinitions", propOrder = { "failureReason" })
@XmlSeeAlso( { LocationListCreditCardManagementResult.class, TokenCreditCardManagementResult.class,
        StoredCardCreditCardManagementResult.class, CustomerCreditCardManagementResult.class })
public class CreditCardManagementResult extends WebServiceResult {

    @XmlElement(name = "FailureReason")
    protected CreditCardManagementFailureReason failureReason;

    /**
     * Gets the value of the failureReason property.
     * 
     * @return
     *     possible object is
     *     {@link CreditCardManagementFailureReason }
     *     
     */
    public CreditCardManagementFailureReason getFailureReason() {
        return failureReason;
    }

    /**
     * Sets the value of the failureReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreditCardManagementFailureReason }
     *     
     */
    public void setFailureReason(CreditCardManagementFailureReason value) {
        this.failureReason = value;
    }

}
