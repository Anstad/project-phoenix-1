package com.apd.phoenix.service.card.management;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Java class for StateProvinceCode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StateProvinceCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Alabama"/>
 *     &lt;enumeration value="Alaska"/>
 *     &lt;enumeration value="AmericanSamoa"/>
 *     &lt;enumeration value="Arizona"/>
 *     &lt;enumeration value="Arkansas"/>
 *     &lt;enumeration value="California"/>
 *     &lt;enumeration value="Colorado"/>
 *     &lt;enumeration value="Connecticut"/>
 *     &lt;enumeration value="Delaware"/>
 *     &lt;enumeration value="DistrictOfColumbia"/>
 *     &lt;enumeration value="FederatedStateOfMicronesia"/>
 *     &lt;enumeration value="Florida"/>
 *     &lt;enumeration value="Georgia"/>
 *     &lt;enumeration value="Guam"/>
 *     &lt;enumeration value="Hawaii"/>
 *     &lt;enumeration value="Idaho"/>
 *     &lt;enumeration value="Illinois"/>
 *     &lt;enumeration value="Indiana"/>
 *     &lt;enumeration value="Iowa"/>
 *     &lt;enumeration value="Kansas"/>
 *     &lt;enumeration value="Kentucky"/>
 *     &lt;enumeration value="Louisiana"/>
 *     &lt;enumeration value="Maine"/>
 *     &lt;enumeration value="MarshallIslands"/>
 *     &lt;enumeration value="Maryland"/>
 *     &lt;enumeration value="Massachusetts"/>
 *     &lt;enumeration value="Michigan"/>
 *     &lt;enumeration value="Minnesota"/>
 *     &lt;enumeration value="Mississippi"/>
 *     &lt;enumeration value="Missouri"/>
 *     &lt;enumeration value="Montana"/>
 *     &lt;enumeration value="Nebraska"/>
 *     &lt;enumeration value="Nevada"/>
 *     &lt;enumeration value="NewHampshire"/>
 *     &lt;enumeration value="NewJersey"/>
 *     &lt;enumeration value="NewMexico"/>
 *     &lt;enumeration value="NewYork"/>
 *     &lt;enumeration value="NorthCarolina"/>
 *     &lt;enumeration value="NorthDakota"/>
 *     &lt;enumeration value="NorthernMarianaIslands"/>
 *     &lt;enumeration value="Ohio"/>
 *     &lt;enumeration value="Oklahoma"/>
 *     &lt;enumeration value="Oregon"/>
 *     &lt;enumeration value="Palau"/>
 *     &lt;enumeration value="Pennsylvania"/>
 *     &lt;enumeration value="PuertoRico"/>
 *     &lt;enumeration value="RhodeIsland"/>
 *     &lt;enumeration value="SouthCarolina"/>
 *     &lt;enumeration value="SouthDakota"/>
 *     &lt;enumeration value="Tennessee"/>
 *     &lt;enumeration value="Texas"/>
 *     &lt;enumeration value="Utah"/>
 *     &lt;enumeration value="Vermont"/>
 *     &lt;enumeration value="VirginIslands"/>
 *     &lt;enumeration value="Virginia"/>
 *     &lt;enumeration value="Washington"/>
 *     &lt;enumeration value="WestVirginia"/>
 *     &lt;enumeration value="Wisconsin"/>
 *     &lt;enumeration value="Wyoming"/>
 *     &lt;enumeration value="Alberta"/>
 *     &lt;enumeration value="BritishColumbia"/>
 *     &lt;enumeration value="Manitoba"/>
 *     &lt;enumeration value="NewBrunswick"/>
 *     &lt;enumeration value="NewfoundlandAndLabrador"/>
 *     &lt;enumeration value="NorthwestTerritories"/>
 *     &lt;enumeration value="NovaScotia"/>
 *     &lt;enumeration value="Nunavut"/>
 *     &lt;enumeration value="Ontario"/>
 *     &lt;enumeration value="PrinceEdwardIsland"/>
 *     &lt;enumeration value="Quebec"/>
 *     &lt;enumeration value="Saskatchewan"/>
 *     &lt;enumeration value="Yukon"/>
 *     &lt;enumeration value="Aguascalientes"/>
 *     &lt;enumeration value="BajaCalifornia"/>
 *     &lt;enumeration value="BajaCaliforniaSur"/>
 *     &lt;enumeration value="Campeche"/>
 *     &lt;enumeration value="Chiapas"/>
 *     &lt;enumeration value="Chihuahua"/>
 *     &lt;enumeration value="Coahuila"/>
 *     &lt;enumeration value="Colima"/>
 *     &lt;enumeration value="DistritoFederal"/>
 *     &lt;enumeration value="Durango"/>
 *     &lt;enumeration value="Guanajuato"/>
 *     &lt;enumeration value="Guerrero"/>
 *     &lt;enumeration value="Hidalgo"/>
 *     &lt;enumeration value="Jalisco"/>
 *     &lt;enumeration value="Mexico"/>
 *     &lt;enumeration value="Michoacan"/>
 *     &lt;enumeration value="Morelos"/>
 *     &lt;enumeration value="Nayarit"/>
 *     &lt;enumeration value="NuevoLeonv"/>
 *     &lt;enumeration value="Oaxaca"/>
 *     &lt;enumeration value="Puebla"/>
 *     &lt;enumeration value="Queretaro"/>
 *     &lt;enumeration value="QuintanaRoo"/>
 *     &lt;enumeration value="SanLuisPotosi"/>
 *     &lt;enumeration value="Sinaloa"/>
 *     &lt;enumeration value="Sonora"/>
 *     &lt;enumeration value="Tabasco"/>
 *     &lt;enumeration value="Tamaulipas"/>
 *     &lt;enumeration value="Tlaxcala"/>
 *     &lt;enumeration value="Veracruz"/>
 *     &lt;enumeration value="Yucatan"/>
 *     &lt;enumeration value="Zacatecas"/>
 *     &lt;enumeration value="ArmedForcesEurope"/>
 *     &lt;enumeration value="ArmedForcesAsiaPacific"/>
 *     &lt;enumeration value="ArmedForcesAmerica"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StateProvinceCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums")
@XmlEnum
public enum StateProvinceCode {

    @XmlEnumValue("None")
    NONE("None"), @XmlEnumValue("Alabama")
    ALABAMA("Alabama"), @XmlEnumValue("Alaska")
    ALASKA("Alaska"), @XmlEnumValue("AmericanSamoa")
    AMERICAN_SAMOA("AmericanSamoa"), @XmlEnumValue("Arizona")
    ARIZONA("Arizona"), @XmlEnumValue("Arkansas")
    ARKANSAS("Arkansas"), @XmlEnumValue("California")
    CALIFORNIA("California"), @XmlEnumValue("Colorado")
    COLORADO("Colorado"), @XmlEnumValue("Connecticut")
    CONNECTICUT("Connecticut"), @XmlEnumValue("Delaware")
    DELAWARE("Delaware"), @XmlEnumValue("DistrictOfColumbia")
    DISTRICT_OF_COLUMBIA("DistrictOfColumbia"), @XmlEnumValue("FederatedStateOfMicronesia")
    FEDERATED_STATE_OF_MICRONESIA("FederatedStateOfMicronesia"), @XmlEnumValue("Florida")
    FLORIDA("Florida"), @XmlEnumValue("Georgia")
    GEORGIA("Georgia"), @XmlEnumValue("Guam")
    GUAM("Guam"), @XmlEnumValue("Hawaii")
    HAWAII("Hawaii"), @XmlEnumValue("Idaho")
    IDAHO("Idaho"), @XmlEnumValue("Illinois")
    ILLINOIS("Illinois"), @XmlEnumValue("Indiana")
    INDIANA("Indiana"), @XmlEnumValue("Iowa")
    IOWA("Iowa"), @XmlEnumValue("Kansas")
    KANSAS("Kansas"), @XmlEnumValue("Kentucky")
    KENTUCKY("Kentucky"), @XmlEnumValue("Louisiana")
    LOUISIANA("Louisiana"), @XmlEnumValue("Maine")
    MAINE("Maine"), @XmlEnumValue("MarshallIslands")
    MARSHALL_ISLANDS("MarshallIslands"), @XmlEnumValue("Maryland")
    MARYLAND("Maryland"), @XmlEnumValue("Massachusetts")
    MASSACHUSETTS("Massachusetts"), @XmlEnumValue("Michigan")
    MICHIGAN("Michigan"), @XmlEnumValue("Minnesota")
    MINNESOTA("Minnesota"), @XmlEnumValue("Mississippi")
    MISSISSIPPI("Mississippi"), @XmlEnumValue("Missouri")
    MISSOURI("Missouri"), @XmlEnumValue("Montana")
    MONTANA("Montana"), @XmlEnumValue("Nebraska")
    NEBRASKA("Nebraska"), @XmlEnumValue("Nevada")
    NEVADA("Nevada"), @XmlEnumValue("NewHampshire")
    NEW_HAMPSHIRE("NewHampshire"), @XmlEnumValue("NewJersey")
    NEW_JERSEY("NewJersey"), @XmlEnumValue("NewMexico")
    NEW_MEXICO("NewMexico"), @XmlEnumValue("NewYork")
    NEW_YORK("NewYork"), @XmlEnumValue("NorthCarolina")
    NORTH_CAROLINA("NorthCarolina"), @XmlEnumValue("NorthDakota")
    NORTH_DAKOTA("NorthDakota"), @XmlEnumValue("NorthernMarianaIslands")
    NORTHERN_MARIANA_ISLANDS("NorthernMarianaIslands"), @XmlEnumValue("Ohio")
    OHIO("Ohio"), @XmlEnumValue("Oklahoma")
    OKLAHOMA("Oklahoma"), @XmlEnumValue("Oregon")
    OREGON("Oregon"), @XmlEnumValue("Palau")
    PALAU("Palau"), @XmlEnumValue("Pennsylvania")
    PENNSYLVANIA("Pennsylvania"), @XmlEnumValue("PuertoRico")
    PUERTO_RICO("PuertoRico"), @XmlEnumValue("RhodeIsland")
    RHODE_ISLAND("RhodeIsland"), @XmlEnumValue("SouthCarolina")
    SOUTH_CAROLINA("SouthCarolina"), @XmlEnumValue("SouthDakota")
    SOUTH_DAKOTA("SouthDakota"), @XmlEnumValue("Tennessee")
    TENNESSEE("Tennessee"), @XmlEnumValue("Texas")
    TEXAS("Texas"), @XmlEnumValue("Utah")
    UTAH("Utah"), @XmlEnumValue("Vermont")
    VERMONT("Vermont"), @XmlEnumValue("VirginIslands")
    VIRGIN_ISLANDS("VirginIslands"), @XmlEnumValue("Virginia")
    VIRGINIA("Virginia"), @XmlEnumValue("Washington")
    WASHINGTON("Washington"), @XmlEnumValue("WestVirginia")
    WEST_VIRGINIA("WestVirginia"), @XmlEnumValue("Wisconsin")
    WISCONSIN("Wisconsin"), @XmlEnumValue("Wyoming")
    WYOMING("Wyoming"), @XmlEnumValue("Alberta")
    ALBERTA("Alberta"), @XmlEnumValue("BritishColumbia")
    BRITISH_COLUMBIA("BritishColumbia"), @XmlEnumValue("Manitoba")
    MANITOBA("Manitoba"), @XmlEnumValue("NewBrunswick")
    NEW_BRUNSWICK("NewBrunswick"), @XmlEnumValue("NewfoundlandAndLabrador")
    NEWFOUNDLAND_AND_LABRADOR("NewfoundlandAndLabrador"), @XmlEnumValue("NorthwestTerritories")
    NORTHWEST_TERRITORIES("NorthwestTerritories"), @XmlEnumValue("NovaScotia")
    NOVA_SCOTIA("NovaScotia"), @XmlEnumValue("Nunavut")
    NUNAVUT("Nunavut"), @XmlEnumValue("Ontario")
    ONTARIO("Ontario"), @XmlEnumValue("PrinceEdwardIsland")
    PRINCE_EDWARD_ISLAND("PrinceEdwardIsland"), @XmlEnumValue("Quebec")
    QUEBEC("Quebec"), @XmlEnumValue("Saskatchewan")
    SASKATCHEWAN("Saskatchewan"), @XmlEnumValue("Yukon")
    YUKON("Yukon"), @XmlEnumValue("Aguascalientes")
    AGUASCALIENTES("Aguascalientes"), @XmlEnumValue("BajaCalifornia")
    BAJA_CALIFORNIA("BajaCalifornia"), @XmlEnumValue("BajaCaliforniaSur")
    BAJA_CALIFORNIA_SUR("BajaCaliforniaSur"), @XmlEnumValue("Campeche")
    CAMPECHE("Campeche"), @XmlEnumValue("Chiapas")
    CHIAPAS("Chiapas"), @XmlEnumValue("Chihuahua")
    CHIHUAHUA("Chihuahua"), @XmlEnumValue("Coahuila")
    COAHUILA("Coahuila"), @XmlEnumValue("Colima")
    COLIMA("Colima"), @XmlEnumValue("DistritoFederal")
    DISTRITO_FEDERAL("DistritoFederal"), @XmlEnumValue("Durango")
    DURANGO("Durango"), @XmlEnumValue("Guanajuato")
    GUANAJUATO("Guanajuato"), @XmlEnumValue("Guerrero")
    GUERRERO("Guerrero"), @XmlEnumValue("Hidalgo")
    HIDALGO("Hidalgo"), @XmlEnumValue("Jalisco")
    JALISCO("Jalisco"), @XmlEnumValue("Mexico")
    MEXICO("Mexico"), @XmlEnumValue("Michoacan")
    MICHOACAN("Michoacan"), @XmlEnumValue("Morelos")
    MORELOS("Morelos"), @XmlEnumValue("Nayarit")
    NAYARIT("Nayarit"), @XmlEnumValue("NuevoLeonv")
    NUEVO_LEONV("NuevoLeonv"), @XmlEnumValue("Oaxaca")
    OAXACA("Oaxaca"), @XmlEnumValue("Puebla")
    PUEBLA("Puebla"), @XmlEnumValue("Queretaro")
    QUERETARO("Queretaro"), @XmlEnumValue("QuintanaRoo")
    QUINTANA_ROO("QuintanaRoo"), @XmlEnumValue("SanLuisPotosi")
    SAN_LUIS_POTOSI("SanLuisPotosi"), @XmlEnumValue("Sinaloa")
    SINALOA("Sinaloa"), @XmlEnumValue("Sonora")
    SONORA("Sonora"), @XmlEnumValue("Tabasco")
    TABASCO("Tabasco"), @XmlEnumValue("Tamaulipas")
    TAMAULIPAS("Tamaulipas"), @XmlEnumValue("Tlaxcala")
    TLAXCALA("Tlaxcala"), @XmlEnumValue("Veracruz")
    VERACRUZ("Veracruz"), @XmlEnumValue("Yucatan")
    YUCATAN("Yucatan"), @XmlEnumValue("Zacatecas")
    ZACATECAS("Zacatecas"), @XmlEnumValue("ArmedForcesEurope")
    ARMED_FORCES_EUROPE("ArmedForcesEurope"), @XmlEnumValue("ArmedForcesAsiaPacific")
    ARMED_FORCES_ASIA_PACIFIC("ArmedForcesAsiaPacific"), @XmlEnumValue("ArmedForcesAmerica")
    ARMED_FORCES_AMERICA("ArmedForcesAmerica");

    private final String value;

    StateProvinceCode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StateProvinceCode fromValue(String v) {
        for (StateProvinceCode c : StateProvinceCode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        return StateProvinceCode.NONE;
    }

}
