package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CreditCardTransactionAuthorizationResult complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CreditCardTransactionAuthorizationResult">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionVerificationResult">
 *       &lt;sequence>
 *         &lt;element name="AuthCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditCardResponseStatus" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Common.Enums}CreditCardResponseStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditCardTransactionAuthorizationResult", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", propOrder = {
        "authCode", "creditCardResponseStatus" })
@XmlSeeAlso( { CreditCardTransactionStatusResult.class })
public class CreditCardTransactionAuthorizationResult extends CreditCardTransactionVerificationResult {

    @XmlElementRef(name = "AuthCode", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions", type = JAXBElement.class, required = false)
    protected JAXBElement<String> authCode;
    @XmlElement(name = "CreditCardResponseStatus")
    protected CreditCardResponseStatus creditCardResponseStatus;

    /**
     * Gets the value of the authCode property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getAuthCode() {
        return authCode;
    }

    /**
     * Sets the value of the authCode property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setAuthCode(JAXBElement<String> value) {
        this.authCode = value;
    }

    /**
     * Gets the value of the creditCardResponseStatus property.
     *
     * @return possible object is {@link CreditCardResponseStatus }
     *
     */
    public CreditCardResponseStatus getCreditCardResponseStatus() {
        return creditCardResponseStatus;
    }

    /**
     * Sets the value of the creditCardResponseStatus property.
     *
     * @param value allowed object is {@link CreditCardResponseStatus }
     *
     */
    public void setCreditCardResponseStatus(CreditCardResponseStatus value) {
        this.creditCardResponseStatus = value;
    }
}
