package com.apd.phoenix.service.payment.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for CardVerificationParams complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="CardVerificationParams">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AddOrUpdateCard" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CreditCard" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions.CreditCardTransactionDefinitions}CreditCardTransactionCreditCard" minOccurs="0"/>
 *         &lt;element name="StoredCardIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}StoredCardIdentifier" minOccurs="0"/>
 *         &lt;element name="TerminalIdentifier" type="{http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Definitions}TerminalIdentifier" minOccurs="0"/>
 *         &lt;element name="TransactionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardVerificationParams", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", propOrder = {
        "addOrUpdateCard", "creditCard", "storedCardIdentifier", "terminalIdentifier", "transactionKey" })
public class CardVerificationParams {

    @XmlElement(name = "AddOrUpdateCard")
    protected Boolean addOrUpdateCard;
    @XmlElementRef(name = "CreditCard", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<CreditCardTransactionCreditCard> creditCard;
    @XmlElementRef(name = "StoredCardIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<StoredCardIdentifier> storedCardIdentifier;
    @XmlElementRef(name = "TerminalIdentifier", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<TerminalIdentifier> terminalIdentifier;
    @XmlElementRef(name = "TransactionKey", namespace = "http://schemas.datacontract.org/2004/07/ThreeDelta.Web.Services.ECLinx.Parameters.CreditCardTransactionParameters", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transactionKey;

    /**
     * Gets the value of the addOrUpdateCard property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isAddOrUpdateCard() {
        return addOrUpdateCard;
    }

    /**
     * Sets the value of the addOrUpdateCard property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setAddOrUpdateCard(Boolean value) {
        this.addOrUpdateCard = value;
    }

    /**
     * Gets the value of the creditCard property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}
     *
     */
    public JAXBElement<CreditCardTransactionCreditCard> getCreditCard() {
        return creditCard;
    }

    /**
     * Sets the value of the creditCard property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link CreditCardTransactionCreditCard }{@code >}
     *
     */
    public void setCreditCard(JAXBElement<CreditCardTransactionCreditCard> value) {
        this.creditCard = value;
    }

    /**
     * Gets the value of the storedCardIdentifier property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}
     *
     */
    public JAXBElement<StoredCardIdentifier> getStoredCardIdentifier() {
        return storedCardIdentifier;
    }

    /**
     * Sets the value of the storedCardIdentifier property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link StoredCardIdentifier }{@code >}
     *
     */
    public void setStoredCardIdentifier(JAXBElement<StoredCardIdentifier> value) {
        this.storedCardIdentifier = value;
    }

    /**
     * Gets the value of the terminalIdentifier property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public JAXBElement<TerminalIdentifier> getTerminalIdentifier() {
        return terminalIdentifier;
    }

    /**
     * Sets the value of the terminalIdentifier property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link TerminalIdentifier }{@code >}
     *
     */
    public void setTerminalIdentifier(JAXBElement<TerminalIdentifier> value) {
        this.terminalIdentifier = value;
    }

    /**
     * Gets the value of the transactionKey property.
     *
     * @return possible object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public JAXBElement<String> getTransactionKey() {
        return transactionKey;
    }

    /**
     * Sets the value of the transactionKey property.
     *
     * @param value allowed object is
     * {@link JAXBElement }{@code <}{@link String }{@code >}
     *
     */
    public void setTransactionKey(JAXBElement<String> value) {
        this.transactionKey = value;
    }
}
