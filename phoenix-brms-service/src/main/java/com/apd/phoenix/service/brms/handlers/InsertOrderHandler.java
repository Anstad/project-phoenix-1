/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.dto.PurchaseOrderDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;

@Stateless
@LocalBean
public class InsertOrderHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(InsertOrderHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        Long orderId = (Long) wi.getParameter("inputOrderId");
        if (orderId == null) {
            //later versions use "inputOrderId", "orderId" just included for backwards compatibility
            orderId = (Long) wi.getParameter("orderId");
        }
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId(orderId);
        try {
		PurchaseOrderDto purchaseOrderDto = DtoFactory.createPurchaseOrderDto(customerOrder);
		toReturn.put("purchaseOrderDto", purchaseOrderDto);
		if (LOGGER.isTraceEnabled()) {
			LOGGER.trace("Created purchaseorderdto=" + purchaseOrderDto);
		}
	} catch (ParsingException e) {
		LOGGER.error("An error occured:", e);;
	}
        return toReturn;
    }
}
