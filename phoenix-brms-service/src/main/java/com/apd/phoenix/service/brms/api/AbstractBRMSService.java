package com.apd.phoenix.service.brms.api;

import javax.inject.Inject;
import org.drools.runtime.StatefulKnowledgeSession;

public abstract class AbstractBRMSService implements BRMSService {

    @Inject
    StatefulKnowledgeSessionFactory statefulKnowledgeSessionFactory;

    @Override
    public StatefulKnowledgeSession loadStatefulKnowledgeSession(int sessionId, String snapshot) {
        return statefulKnowledgeSessionFactory.loadStatefulKnowledgeSession(sessionId, snapshot);
    }

    @Override
    public StatefulKnowledgeSession newStatefulKnowledgeSession() {
        return statefulKnowledgeSessionFactory.newStatefulKnowledgeSession();
    }

}
