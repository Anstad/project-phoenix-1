package com.apd.phoenix.service.brms.handlers;

import java.util.Date;
import java.util.Map;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;

public class WorkflowLogger extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowLogger.class);

    @Inject
    private CommentBp commentBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        String message = (String) wi.getParameter("message");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        Comment comment = new Comment();
        comment.setCommentDate(new Date());
        comment.setContent(message);
        comment.setMadeBySystem(true);
        customerOrder = commentBp.addComment(customerOrder, comment);
        return null;
    }

}
