/**
 * 
 */
package com.apd.phoenix.service.brms.impl;

import java.io.Serializable;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.transaction.TransactionManager;
import org.drools.KnowledgeBase;
import org.drools.impl.EnvironmentFactory;
import org.drools.persistence.jpa.JPAKnowledgeService;
import org.drools.runtime.Environment;
import org.drools.runtime.EnvironmentName;
import org.drools.runtime.StatefulKnowledgeSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.KnowledgeBaseFactory;
import com.apd.phoenix.service.brms.api.StatefulKnowledgeSessionFactory;

/**
 * @author Red Hat Middleware Consulting. Red Hat, Inc.  Jul 3, 2013
 * 
 *         This class is a wrapper around JPAKnowledgeService. It exposes the
 *         same functions, while hiding the parameters that don't change in our
 *         domain (ksessionconfiguration, environment, and kbase)
 * 
 */
@Stateless
@LocalBean
public class StatefulKnowledgeSessionFactoryImpl implements Serializable, StatefulKnowledgeSessionFactory {

    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(StatefulKnowledgeSessionFactoryImpl.class);

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    @PersistenceContext(unitName = "jbpm")
    private EntityManager em;

    @Inject
    private KnowledgeBaseFactory knowledgeBaseFactory;

    @Override
    public StatefulKnowledgeSession newStatefulKnowledgeSession() {
        LOG.info("Creating new persistent knowledge session");

        KnowledgeBase kbase = knowledgeBaseFactory.getKnowledgeBase();
        Environment env = this.createEnvironment();

        return JPAKnowledgeService.newStatefulKnowledgeSession(kbase, null, env);
    }

    @Override
    public StatefulKnowledgeSession loadStatefulKnowledgeSession(int ksessionId, String snapshot) {
        LOG.info("Loading id=" + ksessionId + " persistent knowledge session");

        KnowledgeBase kbase = knowledgeBaseFactory.getKnowledgeBase(snapshot);
        Environment env = this.createEnvironment();

        StatefulKnowledgeSession session = JPAKnowledgeService.loadStatefulKnowledgeSession(ksessionId, kbase, null,
                env);
        return session;
    }

    private Environment createEnvironment() {
        LOG.info("EM: " + em.getClass());
        Environment env = EnvironmentFactory.newEnvironment();
        env.set(EnvironmentName.ENTITY_MANAGER_FACTORY, getEmf());
        env.set(EnvironmentName.TRANSACTION_MANAGER, tm);
        return env;
    }

    private EntityManagerFactory getEmf() {
        return em.getEntityManagerFactory();
    }
}
