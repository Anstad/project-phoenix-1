package com.apd.phoenix.service.brms.handlers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.PendingWorkflowCommandBp;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.Executor;
import com.apd.phoenix.service.executor.command.api.CommandConstants;
import com.apd.phoenix.service.model.PendingWorkflowCommand;

@Stateless
@LocalBean
public class TimerServiceWorkItemHandler implements WorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimerServiceWorkItemHandler.class);
    public static final String INPUT_SUFFIX = "-in";
    public static final String OUTPUT_SUFFIX = "-out";

    @Inject
    Executor executor;

    @Inject
    PendingWorkflowCommandBp pendingWorkflowCommandBp;

    public void executeWorkItem(final WorkItem workItem, final WorkItemManager manager) {
        String sec = (String) workItem.getParameter("seconds");
        String mins = (String) workItem.getParameter("minutes");
        String hos = (String) workItem.getParameter("hours");
        String ds = (String) workItem.getParameter("days");
        String factsToPersist = (String) workItem.getParameter("factsToPersist");

        final Integer seconds = ((sec == null || sec.isEmpty()) ? 0 : Integer.parseInt(sec));
        final Integer minutes = ((mins == null || mins.isEmpty()) ? 0 : Integer.parseInt(mins));
        final Integer hours = ((hos == null || hos.isEmpty()) ? 0 : Integer.parseInt(hos));
        final Integer days = ((ds == null || ds.isEmpty()) ? 0 : Integer.parseInt(ds));
        String foundType = (String) workItem.getParameter("processType");
        //following lines are a workaround for a bug in an earlier snapshot of our BRMS repo
        if (StringUtils.isBlank(foundType)) {
            foundType = (String) workItem.getParameter("type");
        }
        final String type = foundType;
        final long id = (Long) workItem.getParameter("id");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, seconds);
        calendar.add(Calendar.MINUTE, minutes);
        calendar.add(Calendar.HOUR, hours);
        calendar.add(Calendar.DATE, days);
        Date expires = calendar.getTime();
        final Long workItemId = workItem.getId();

        CommandContext ctxCMD = new CommandContext();
        ctxCMD.setData("commandKey", UUID.randomUUID().toString());
        ctxCMD.setData("idType", type);
        ctxCMD.setData("id", id);
        ctxCMD.setData("workItemID", workItemId);
        ctxCMD.setData("facts", workItem.getParameter("facts"));
        ctxCMD.setData("date", expires);
        if (StringUtils.isNotBlank(factsToPersist)) {
            Map<String, Object> responseData = new HashMap<String, Object>();
            for (String factName : factsToPersist.split(",")) {
                if (workItem.getParameter(factName + INPUT_SUFFIX) != null) {
                    responseData.put(factName + OUTPUT_SUFFIX, workItem.getParameter(factName + INPUT_SUFFIX));
                }
            }
            ctxCMD.setData("responseData", responseData);
        }
        PendingWorkflowCommand pendingWorkflowCommand = new PendingWorkflowCommand();
        pendingWorkflowCommand.setCommandName(CommandConstants.COMPLETE_WI_COMMAND_BEAN);
        pendingWorkflowCommand.setCtx(ctxCMD);
        pendingWorkflowCommand.setExpires(expires);
        pendingWorkflowCommandBp.create(pendingWorkflowCommand);
    }

    @Override
    public void abortWorkItem(WorkItem arg0, WorkItemManager arg1) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Timer process aborted");
        }
    }

}
