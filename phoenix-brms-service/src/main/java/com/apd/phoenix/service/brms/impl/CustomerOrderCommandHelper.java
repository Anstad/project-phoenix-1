package com.apd.phoenix.service.brms.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.WorkItemHandler;
import org.jbpm.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.ReturnOrderBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ReturnOrder;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;

@Stateless
@LocalBean
public class CustomerOrderCommandHelper implements BRMSCommandHelper {

    private static final Logger logger = LoggerFactory.getLogger(CustomerOrderCommandHelper.class);

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp orderProcessBp;

    @Override
    public StatefulKnowledgeSession getStatefulKnowledgeSession(long orderId) throws Exception {
        return apdBRMSService.getStatefulKnowledgeSessionByOrderId(orderId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(StatefulKnowledgeSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByOrderId(id);
    }

    @Override
    public void setRecordProcessId(long id, long processId) {
        apdBRMSService.setOrderRecordProcessId(id, processId);
    }

    @Override
    public boolean isStale(long orderId) {
        return apdBRMSService.isOrderStale(orderId);
    }

    @Override
    public long getProcessId(long recordId) {
        return apdBRMSService.getOrderProcessId(recordId);
    }

    @Override
    public Object getFact(long orderId) {
        Object toReturn = null;
        try {
            CustomerOrder order = orderProcessBp.getOrderFromProcessId(orderId);
            if (order != null) {
                toReturn = DtoFactory.createPurchaseOrderDto(order);
            }
            else {
                throw new ParsingException("Null order when searching with ID " + orderId);
            }
        }
        catch (ParsingException e) {
            logger.error("Cannot create purchase order fact from order=" + orderId);
        }
        return toReturn;
    }

}
