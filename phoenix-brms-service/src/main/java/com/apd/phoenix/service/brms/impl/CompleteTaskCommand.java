package com.apd.phoenix.service.brms.impl;

import org.jbpm.task.service.ContentData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.AbstractBRMSCommand;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;
import org.jbpm.task.Status;
import org.jbpm.task.Task;

@Named(value = "CompleteTaskCommand")
@Stateless
@LocalBean
public class CompleteTaskCommand extends AbstractBRMSCommand {

    private static final Logger logger = LoggerFactory.getLogger(CompleteTaskCommand.class);

    @Override
    public ExecutionResults execute_safe(CommandContext ctx) throws Exception {
        Long taskId = (Long) ctx.getData("taskId");
        String userId = (String) ctx.getData("userId");
        ContentData contentData = (ContentData) ctx.getData("contentData");
        if (userId == null) {
            throw new Exception("USER ID NOT SPECIFIED EXCEPTION");
        }
        else if (taskId == null) {
            throw new Exception("TASK ID NOT SPECIFIED");
        }
        else {
            Task task = client.getTask(taskId);
            logger.info("---------testing id=" + taskId);
            try {
                if (task.getTaskData().getStatus() == Status.Suspended) {
                    logger.info("---------resuming as " + userId);
                    client.resume(taskId, userId);
                }
                logger.info("---------completing with " + contentData);
                client.complete(taskId, userId, contentData);
            }
            catch (org.jbpm.task.service.TaskException e) {
                logger.error("Could not complete the task :" + e.toString());
            }
        }

        return null;
    }
}
