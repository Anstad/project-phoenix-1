package com.apd.phoenix.service.brms.impl;

import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;
import org.drools.runtime.process.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.AbstractBRMSCommand;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

@Named(value = "CompleteWICommand")
@Stateless
@LocalBean
public class CompleteWICommand extends AbstractBRMSCommand {

    private static final Logger LOG = LoggerFactory.getLogger(CompleteWICommand.class);

    @Override
    public ExecutionResults execute_safe(CommandContext ctx) throws Exception {
        Long workItemID = (Long) ctx.getData("workItemID");

        if (workItemID == null) {
            throw new Exception("WI ID NOT SPECIFIED EXCEPTION");
        }

        @SuppressWarnings("unchecked")
        Map<String, Object> responseData = (Map<String, Object>) ctx.getData("responseData");

        LOG.info("Completing work item: {}", workItemID);
        session.getWorkItemManager().completeWorkItem(workItemID, responseData);

        ExecutionResults executionResults = new ExecutionResults();
        return executionResults;
    }

}
