package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.camel.InvoiceService;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class CamelServiceWorkItemHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(CamelServiceWorkItemHandler.class);

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        for (Map.Entry<String, Object> entry : wi.getParameters().entrySet()) {
            logger.info("key: " + entry.getKey() + ", value: " + entry.getValue());
        }
        
        InvoiceService service = new InvoiceService();
        try {
            service.startInvoiceRoute();
        } catch (Exception ex) {
            logger.error("Cannot start invoice route:" + ex.toString());
        }
        
        return toReturn; //To change body of generated methods, choose Tools | Templates.
    }
}
