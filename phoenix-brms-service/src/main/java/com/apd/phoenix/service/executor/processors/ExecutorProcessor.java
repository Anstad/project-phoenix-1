package com.apd.phoenix.service.executor.processors;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.ejb.Stateless;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import org.apache.camel.Body;
import org.apache.camel.Header;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.executor.api.Command;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.ExceptionOrderLog;
import com.apd.phoenix.service.model.OrderLog.EventType;

@Stateless
public class ExecutorProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorProcessor.class);

    @Inject
    private BeanManager beanManager;

    @Inject
    EmailService emailService;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    public void processCommand(@Header("commandName") String commandName, @Header("retries") int retries,
            @Body CommandContext commandContext) throws Exception {
        Command command = findCommand(commandName);
        LOGGER.info("Executing command: " + commandName);

        command.execute(commandContext);
    }

    public void sendErrorNotification(@Header("commandName") String commandName, @Body CommandContext commandContext, Exception exception) {
    	CustomerOrder order = null;
    	try {
			Long id = (Long) commandContext.getData("id");
			if (id != null) {
				order = processLookupBp.getOrderFromProcessId(id);
				if (order != null) {
					ExceptionOrderLog log = new ExceptionOrderLog();
					log.setDescription(exception.getMessage());
					log.setCustomerOrder(order);
					log.setEventType(EventType.INTERNAL_ERROR);
					customerOrderBp.addLog(order, log);
					//TODO: create a task for a failed order, instead of sending email, per PHOEN-4146
				}
			}
		}
		catch (Exception e) {
    		LOGGER.error("Exception when creating exception log for order, email will be sent anyway");
    		if (LOGGER.isDebugEnabled()) {
    			LOGGER.debug("Stack trace", e);
    		}
		}
        LOGGER.info("Sending error email");
        Properties brmsProperties = PropertiesLoader.getAsProperties("brms.integration");
        String from = brmsProperties.getProperty("workflowErrorFrom","admin@apdmarketplace.com");
        Set<String> recipients = new HashSet<>();
        recipients.add(brmsProperties.getProperty("workflowErrorTo"));
        Set<String> ccRecipients = new HashSet<>();
        String cc = brmsProperties.getProperty("workflowErrorCC");
        if(StringUtils.isNotEmpty(cc)) {
        	ccRecipients.add(cc);
        }
        String subject = "[Workflow] Execution Exception";
        
        String emailBody = "<p><strong>The following exceptions was thrown when processing a workflow command:</strong></p>";
        
        Map<String, Object> commandData = commandContext.getData();
        if(order != null && order.getApdPo() != null){
        	emailBody = emailBody + "<p> apdPo is " + order.getApdPo().getValue() + "</p>";
        }
        emailBody += "<h3>Command Data:</h3><p>";
        for(String key : commandData.keySet()) {
        	emailBody += "["+ key +"] " + commandData.get(key) + "<br />";
        }
        emailBody += "</p>";
        
        emailBody += "<p>=============================<br />";
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        emailBody += sw.toString() + "</p>";
        pw.close();
        
        
       
        
        MimeMessage message = emailService.prepareRawMessage(from, null, null, recipients, ccRecipients, subject, emailBody, null);
		
		emailService.sendEmail(message);
    }

    private Command findCommand(String name) {
        Set<Bean<?>> beans = beanManager.getBeans(name);
        if (!beans.iterator().hasNext()) {
            throw new IllegalArgumentException("Unknown Command implemenation with name '" + name + "'");
        }
        Bean<?> bean = beans.iterator().next();
        return (Command) beanManager.getReference(bean, Command.class, beanManager.createCreationalContext(bean));

    }

}
