package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.WorkItemHandler;
import org.jbpm.task.TaskService;
import org.jbpm.task.service.TaskClient;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;

public class UserRequestCommandHelper implements BRMSCommandHelper {

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Override
    public StatefulKnowledgeSession getStatefulKnowledgeSession(long id) throws Exception {
        return apdBRMSService.getStatefulKnowledgeSessionByUserRequestId(id);
    }

    @Override
    public void setRecordProcessId(long userRequestId, long processId) {
        apdBRMSService.setUserRequestRecordProccessId(userRequestId, processId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(StatefulKnowledgeSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByUserRequestId(id);
    }

    @Override
    public boolean isStale(long id) {
        return apdBRMSService.isUserRequestStale(id);
    }

    @Override
    public long getProcessId(long id) {
        return apdBRMSService.getUserRequestProccessId(id);
    }

    public long getIdByToken(String token) {
        return apdBRMSService.getUserRequestIdByToken(token);
    }

    @Override
    public Object getFact(long userRequestId) {
        return userRequestId;
    }

}
