package com.apd.phoenix.service.brms.api;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.transaction.Synchronization;
import javax.transaction.TransactionManager;
import org.drools.event.process.DefaultProcessEventListener;
import org.drools.event.process.ProcessNodeTriggeredEvent;
import org.drools.event.rule.DefaultAgendaEventListener;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.rule.FactHandle;
import org.jbpm.process.workitem.wsht.SyncWSHumanTaskHandler;
import org.jbpm.task.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.impl.AccountCommandHelper;
import com.apd.phoenix.service.brms.impl.CustomerOrderCommandHelper;
import com.apd.phoenix.service.brms.impl.UserRequestCommandHelper;
import com.apd.phoenix.service.executor.api.Command;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

public abstract class AbstractBRMSCommand implements Command {

    @Inject
    private CustomerOrderCommandHelper orderCommandHelper;

    @Inject
    private AccountCommandHelper accountCommandHelper;

    @Inject
    private UserRequestCommandHelper userRequestCommandHelper;

    protected BRMSCommandHelper commandHelper;

    @Resource(lookup = "java:/TransactionManager")
    private TransactionManager tm;

    protected StatefulKnowledgeSession session;

    protected TaskService client;

    protected SyncWSHumanTaskHandler humanTaskHandler;

    protected CountDownLatch doneSignal;

    protected Long id;

    List<FactHandle> factHandles;

    FactHandle domainFact;

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBRMSCommand.class);

    public abstract ExecutionResults execute_safe(CommandContext ctx) throws Exception;

    public ExecutionResults execute(CommandContext ctx) throws Exception {
        try {
            this.setUp(ctx);
            return execute_safe(ctx);
        }
        catch (Exception e) {
            LOGGER.error("Exception thrown in executing command", e);
            throw e;
        }
        finally {
            this.tearDown();
        }
    }

    public void setUp(CommandContext ctx) throws Exception {
    	doneSignal = new CountDownLatch(1);
    	LOGGER.info("Latch Count: " + doneSignal.getCount());
        Object idType = ctx.getData("idType");
        id = (Long) ctx.getData("id");
        
        
       
        

        if (idType == null) {
            throw new Exception("NO ID TYPE SPECIFIED EXCEPTION");
        }
        else if (idType.equals("CustomerOrder")) {
            commandHelper = orderCommandHelper;
        }
        else if (idType.equals("Account")) {
            commandHelper = accountCommandHelper;
        }
        else if(idType.equals("UserRequest")) {
        	commandHelper = userRequestCommandHelper;
        }
        else {
            throw new Exception("ID TYPE NOT RECOGNIZED EXCEPTION");
        }
        
        if (id == null) {
            throw new Exception("NO ID SPECIFIED");
        }
        
        session = commandHelper.getStatefulKnowledgeSession(id);
        client = commandHelper.getTaskService();
        humanTaskHandler = new SyncWSHumanTaskHandler(client, session);
        commandHelper.registerHandlers(session, humanTaskHandler);
        
        
//        humanTaskHandler.configureClient(client);
        // Note: Needed for JIRA issue:
        // https://issues.jboss.org/browse/JBPM-3673
        humanTaskHandler.setLocal(true);
        humanTaskHandler.connect();

        try {
            tm.getTransaction().registerSynchronization(new Synchronization() {

                @Override
                public void beforeCompletion() {
                    // not used here          
                }

                @Override
                public void afterCompletion(int arg0) {
                    LOGGER.info("Disposing Session");
                    try {
                    	session.dispose();
                    }
                    catch (Exception e) {
                    	LOGGER.error("Unable to dispose session with transaction status " + arg0, e);
                    }
                }
            });
        }
        catch (Exception e) {
            LOGGER.error("Could not register synchronization to dispose session when transaction completes :", e);
        }
        
        session.addEventListener(new DefaultAgendaEventListener() {

            @Override
            public void afterRuleFlowGroupActivated(org.drools.event.rule.RuleFlowGroupActivatedEvent event) { 	   
                session.fireAllRules();
            }

        });
        
        session.addEventListener(new DefaultProcessEventListener() {
        	
        	private boolean inserted = false;

        	@Override
        	public void beforeNodeTriggered(ProcessNodeTriggeredEvent event) {
        		if (!inserted){
            		session.insert(event.getProcessInstance());
            		inserted = true;
        		}
        	}

        });

        domainFact = session.insert(commandHelper.getFact(id));
        //add facts to session
        @SuppressWarnings("unchecked")
		List<Object> facts = (List<Object>) ctx.getData("facts");
        factHandles = new ArrayList<>();
        if(facts != null) {
        	for(Object o:facts) {
        		FactHandle fh = session.insert(o);
        		factHandles.add(fh);
        	}
        }

    }

    public void tearDown() throws Exception {
        commandHelper.finishedWith(id);
        client.disconnect();
        //Remove facts
        for (FactHandle fh : factHandles) {
            session.retract(fh);
        }
        session.retract(domainFact);
        humanTaskHandler.dispose();
    }

}
