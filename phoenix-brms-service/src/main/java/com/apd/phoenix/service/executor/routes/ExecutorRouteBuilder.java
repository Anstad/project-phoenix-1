package com.apd.phoenix.service.executor.routes;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import com.apd.phoenix.service.camel.DelayBean;
import com.apd.phoenix.service.executor.processors.ExecutorProcessor;

public class ExecutorRouteBuilder extends RouteBuilder {

    private String source;

    private String sourceDlq;

    @Override
    public void configure() throws Exception {

        //Generic error handler
        errorHandler(deadLetterChannel(sourceDlq).maximumRedeliveries(0));

        //Exception handling
        onException(Exception.class).maximumRedeliveries(0).handled(true)
        //Log
                .log(LoggingLevel.INFO, "Error in workflow processing: ${exception.stacktrace}")
                //Send notification
                .bean(ExecutorProcessor.class, "sendErrorNotification").to(sourceDlq);

        //Command source
        from(source)
        //delays the messages when deploying
                .autoStartup(false).routePolicy(DelayBean.getDelayPolicy())
                //Process command
                .bean(ExecutorProcessor.class, "processCommand");

    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceDlq() {
        return sourceDlq;
    }

    public void setSourceDlq(String sourceDlq) {
        this.sourceDlq = sourceDlq;
    }

}
