package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.business.CatalogBp;
import com.apd.phoenix.service.business.CatalogXItemBp;
import com.apd.phoenix.service.business.CredentialBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.ItemBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.model.Catalog;
import com.apd.phoenix.service.model.CatalogXItem;
import com.apd.phoenix.service.model.Credential;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.Item;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.Vendor;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class ReplaceLineItemWorkItemHandler extends SimpleAbortWorkItemHandler {

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    CredentialBp credentialBp;

    @Inject
    CatalogBp catalogBp;

    @Inject
    CatalogXItemBp catalogXItemBp;

    @Inject
    ItemBp itemBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    private static final Logger logger = LoggerFactory.getLogger(ReplaceLineItemWorkItemHandler.class);

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        
            long rejectedItemId = (Long) wi.getParameter("lineItemId");
            String skuValue = (String) wi.getParameter("replacementItemSku");
            
            //Initialize objects using work item parameters
            //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
            CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
            LineItem rejectedItem = lineItemBp.findById(rejectedItemId, LineItem.class);
            
            //CustomerOrder to VendorName mapping
            Credential credential = customerOrder.getCredential();
            Catalog catalog = credentialBp.eagerLoad(credential).getCatalog();
            Vendor vendor = catalogBp.eagerLoad(catalog).getVendor();
            
            //Retrieve the replacement LineItem
            Item resultItem = itemBp.searchItem(skuValue, vendor.getName());
            CatalogXItem catalogXItem = new CatalogXItem();
            catalogXItem.setItem(new Item());
            catalogXItem.getItem().setId(resultItem.getId());
            catalogXItem.setCatalog(new Catalog());
            catalogXItem.getCatalog().setId(catalog.getId());
            catalogXItem = catalogXItemBp.searchByExactExample(catalogXItem, 0, 0).get(0);            
            LineItem replacementLineItem = catalogXItemBp.getFromCatalogXItem(catalogXItem);
            
            //Perform replacement
            customerOrderBp.replaceItem(rejectedItem, replacementLineItem, customerOrder);
            toReturn.put("replacementItemId", replacementLineItem.getId());
        
        return toReturn; //To change body of generated methods, choose Tools | Templates.
    }
}
