package com.apd.phoenix.service.brms.api;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.process.WorkItemHandler;
import org.jbpm.task.TaskService;

public interface BRMSCommandHelper {

    public StatefulKnowledgeSession getStatefulKnowledgeSession(long id) throws Exception;

    public void setRecordProcessId(long recordId, long processId);

    public boolean isStale(long recordId);

    public TaskService getTaskService();

    public void registerHandlers(StatefulKnowledgeSession session, WorkItemHandler humanTaskHandler);

    public void finishedWith(long id);

    public long getProcessId(long recordId);

    public Object getFact(long id);
}
