package com.apd.phoenix.service.brms.api;

import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.task.TaskService;

public interface BRMSService {

    public StatefulKnowledgeSession loadStatefulKnowledgeSession(int sessionId, String snapshot);

    public StatefulKnowledgeSession newStatefulKnowledgeSession();

    public TaskService getTaskServiceClient();
}
