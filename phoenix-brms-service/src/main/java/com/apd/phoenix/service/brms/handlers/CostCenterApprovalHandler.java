/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.business.AssignedCostCenterBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.model.AssignedCostCenter;
import com.apd.phoenix.service.model.CustomerOrder;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class CostCenterApprovalHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(CostCenterApprovalHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    private AssignedCostCenterBp assignedCostCenterBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        AssignedCostCenter assignedCostCenter = customerOrder.getAssignedCostCenter();
        
        Boolean approved = assignedCostCenterBp.isAmountAllowed(assignedCostCenter, customerOrder.getOrderTotal());
        
        if (assignedCostCenter != null) {
        	if (approved) {
        		if (assignedCostCenter.getPendingCharge() == null) {
        			assignedCostCenter.setPendingCharge(BigDecimal.ZERO);
        		}
        		assignedCostCenter.setPendingCharge(assignedCostCenter.getPendingCharge().add(customerOrder.getOrderTotal()));
        	}
        
            logger.info("Total limit: " + assignedCostCenter.getTotalLimit());
            logger.info("Current Charge: " + assignedCostCenter.getCurrentCharge());
            logger.info("Pending Charge: " + assignedCostCenter.getPendingCharge());
            logger.info("Order Total: " + customerOrder.getOrderTotal());
            logger.info("Approved?: " + approved);
        }
        
        toReturn.put("approved", approved);
        return toReturn;
    }
}
