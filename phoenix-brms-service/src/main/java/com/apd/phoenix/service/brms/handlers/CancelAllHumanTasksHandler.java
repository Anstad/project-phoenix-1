/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import com.apd.phoenix.service.brms.api.TaskServiceClientFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.SystemEventListenerFactory;
import org.drools.runtime.process.WorkItem;
import org.jbpm.task.Status;
import org.jbpm.task.TaskService;
import org.jbpm.task.query.TaskSummary;
import org.jbpm.task.service.TaskClient;
import org.jbpm.task.service.mina.MinaTaskClientConnector;
import org.jbpm.task.service.mina.MinaTaskClientHandler;
import org.jbpm.task.service.responsehandlers.BlockingTaskOperationResponseHandler;
import org.jbpm.task.service.responsehandlers.BlockingTaskSummaryResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
@LocalBean
public class CancelAllHumanTasksHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(CancelAllHumanTasksHandler.class);

    @Inject
    TaskServiceClientFactory taskServiceClientFactory;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        long processId = wi.getProcessInstanceId();
        TaskService client = taskServiceClientFactory.getTaskService();
        client.connect();
        List<Status> statusList = new ArrayList<>();
        statusList.add(Status.Created);
        statusList.add(Status.InProgress);
        statusList.add(Status.Ready);
        statusList.add(Status.Reserved);
        statusList.add(Status.Suspended);
        List<TaskSummary> tasks = client.getTasksByStatusByProcessId(processId, statusList, "en-UK");
        
        
        for (TaskSummary summary : tasks){
            logger.info("Exiting taskid=" + summary.getId() + " as Administrator!");
            client.exit(summary.getId(), "Administrator");
        }
        
        try {
            client.disconnect();
        } catch (Exception ex) {
            logger.error("Could not disconnect the client: " + ex.toString());
        }
        return toReturn;
    }
}
