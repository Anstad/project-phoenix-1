/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.VendorCreditInvoiceBp;
import com.apd.phoenix.service.business.VendorInvoiceBp;
import com.apd.phoenix.service.model.VendorCreditInvoice;
import com.apd.phoenix.service.model.VendorInvoice;
import com.apd.phoenix.service.model.dto.VendorCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.VendorInvoiceDto;

@Stateless
@LocalBean
public class CreateVendorInvoiceHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(CreateVendorInvoiceHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    VendorInvoiceBp vendorInvoiceBp;

    @Inject
    VendorCreditInvoiceBp vendorCreditInvoiceBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        VendorInvoiceDto vendorInvoiceDto = (VendorInvoiceDto) wi.getParameter("vendorInvoiceDto");
        VendorCreditInvoiceDto vendorCreditInvoiceDto = (VendorCreditInvoiceDto) wi.getParameter("vendorCreditInvoiceDto");
        Long invoiceId = null;
        if (vendorInvoiceDto != null) {
        	VendorInvoice vendorInvoice = vendorInvoiceBp.persistVendorInvoice(vendorInvoiceDto);
        	logger.info("Created invoice with id=" + vendorInvoice.getId());
        	invoiceId = vendorInvoice.getId();
        }
        if (vendorCreditInvoiceDto != null) {
        	VendorCreditInvoice vendorCreditInvoice = vendorCreditInvoiceBp.persistVendorCreditInvoice(vendorCreditInvoiceDto);
        	logger.info("Created credit invoice with id=" + vendorCreditInvoice.getId());
        	invoiceId = vendorCreditInvoice.getId();
        }
        toReturn.put("outputInvoiceId", invoiceId);
        return toReturn;
    }
}
