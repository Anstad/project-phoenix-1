package com.apd.phoenix.service.brms.handlers;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.antlr.stringtemplate.StringTemplate;
import org.apache.commons.lang.StringUtils;
import org.drools.process.instance.WorkItemHandler;
import org.drools.runtime.process.WorkItem;
import org.drools.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.core.utility.PropertiesLoader;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.AddressBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.CustomerOrderRequestBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.email.api.EmailService;
import com.apd.phoenix.service.message.api.Message;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.message.api.MessageUtils;
import com.apd.phoenix.service.model.Account;
import com.apd.phoenix.service.model.Address;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.MessageMetadata;
import com.apd.phoenix.service.model.MiscShipTo;
import com.apd.phoenix.service.model.OrderLog.EventType;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.persistence.jpa.CredentialXVendorDao;
import com.sun.istack.NotNull;

@Stateless
@LocalBean
public class RequestShipToWorkItemHandler implements WorkItemHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RequestShipToWorkItemHandler.class);

    @Inject
    private AddressBp addressBp;

    @Inject
    private AccountBp accountBp;

    @Inject
    private CustomerOrderBp customerOrderBp;

    @Inject
    private MessageService messageService;

    @Inject
    private EmailService emailService;

    @Inject
    private CustomerOrderRequestBp customerOrderRequestBp;

    @Inject
    private CredentialXVendorDao credentialXVendorDao;

    @Inject
    private MessageUtils messageUtils;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public void abortWorkItem(WorkItem wi, WorkItemManager wim) {
        LOG.info("Aborting workitem");
        wim.abortWorkItem(wi.getId());
    }

    @Override
	public void executeWorkItem(WorkItem wi, WorkItemManager wim) {
    	try {
 			Properties emailsLoader = PropertiesLoader.getAsProperties("internal.emails");
 			Properties integrationLoader = PropertiesLoader.getAsProperties("integration");
 			String emailRecipients = emailsLoader.getProperty((String) wi.getParameter("recipient"));
 			String usscoAccountNumber = (String) wi.getParameter("usscoAccountNumber");
 			String authority = integrationLoader.getProperty("shipToApprovalAuthority", "http://localhost:8080");
 	        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
 	        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
			Account account = customerOrder.getAccount();
			Address pendingAccountShipTo = addressBp.retrieve(DtoFactory.createAddressDto(customerOrder.getAddress()), customerOrder
                    .getAccount());
			Address shipTo = customerOrder.getAddress();
			String shipToId = null;
			if (pendingAccountShipTo != null){
				//If the work item was changed to one that already exists, complete the work item and proceed
				updateShipTo(pendingAccountShipTo, shipTo);
				wim.completeWorkItem(wi.getId(), new HashMap<String,Object>());				
			} else {
				//otherwise send an email and await approval
				pendingAccountShipTo = addressBp.cloneEntity(shipTo);
				if(pendingAccountShipTo.getMiscShipTo() == null ){
					pendingAccountShipTo.setMiscShipTo(new MiscShipTo());
				}
				if(!StringUtils.isEmpty(usscoAccountNumber)){
					pendingAccountShipTo.getMiscShipTo().setUsAccount(usscoAccountNumber);
				}
				pendingAccountShipTo.setAccount(account);
				if (shipTo.getCity() != null) {
					pendingAccountShipTo.setCity(shipTo.getCity().toUpperCase());
				}
				if (shipTo.getState() != null) {
					pendingAccountShipTo.setState(shipTo.getState().toUpperCase());
				}
				if (shipTo.getZip() != null) {
					pendingAccountShipTo.setZip(shipTo.getZip());
				}
				if (shipTo.getCountry() != null) {
					pendingAccountShipTo.setCountry(shipTo.getCountry().toUpperCase());
				}
				if(shipTo.getMiscShipTo() != null && !StringUtils.isEmpty(shipTo.getMiscShipTo().getDivision())){
					pendingAccountShipTo.getMiscShipTo().setDivision(shipTo.getMiscShipTo().getDivision());
				}
				if (shipTo.getCompany() != null) {
					pendingAccountShipTo.setCompany(shipTo.getCompany().toUpperCase());
				}
				if (shipTo.getLine1() != null) {
					pendingAccountShipTo.setLine1(shipTo.getLine1().toUpperCase());
				}
				shipToId = addressBp.newShipToId(
					customerOrder.getAddress(), account);
				pendingAccountShipTo.setShipToId(shipToId);
				pendingAccountShipTo.setPendingShipToAuthorization(Boolean.TRUE);
				accountBp.addAddress(account, pendingAccountShipTo, false);
				
				//account = accountBp.addAddress(account, address);
				LOG.info("Message service call for shipToId=" + shipToId);
				String masterAccountNumber = credentialXVendorDao.getAccountIdByCredentialAndVendorName(
						customerOrder.getCredential().getVendorName(), customerOrder.getCredential());
				
				if (StringUtils.isBlank(masterAccountNumber)) {
					masterAccountNumber = customerOrderBp.getUsAccountNumber(customerOrder);
				}
				
				LOG.info("Getting token");
				String token = customerOrderRequestBp.createRequestToken(customerOrder);
				LOG.info("Token {}",token);
				
				String link = authority + "/rest/shiptos/" + token + "/workflow?action=approve&id=" + wi.getId();
				
				String sector = StringUtils.isEmpty(account.getApdAssignedAccountId()) ? "No Sector" : account.getApdAssignedAccountId();
				
				String accountName = StringUtils.isEmpty(account.getName()) ? "Unknown" : account.getName();
				
				String emailBody = prepareEmailMessage(masterAccountNumber, shipToId, usscoAccountNumber, link, shipTo, sector, accountName);
				
				String from = "no-reply@apdmarketplace.com";
				Set<String> replyTos = new HashSet<>();
				String[] replyToList = customerOrder.getCredential().getCsrEmail() != null ? customerOrder.getCredential().getCsrEmail().split(",")
						: new String[]{"customerservice@americanproduct.com"};
				for(String replyTo : replyToList) {
					replyTos.add(replyTo);
				}
				
				Set<String> recipients = new HashSet<>();
				String[] recipientList = emailRecipients.split(",");
				for(String recipient:recipientList) {
					recipients.add(recipient);	
				}
				
				String subject = "NAT " + masterAccountNumber + " - New ShipTo IDRequest: " + shipToId;
				
				MimeMessage message = emailService.prepareRawMessage(from, replyTos, null, recipients, null, subject, emailBody, null);
				
				//TODO: Fix so it doesn't load all into memory
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				try {
					message.writeTo(bos);
				} catch (IOException | MessagingException e) {
					LOG.error("Could not write the message to a byte array output stream", e);
				}
				byte[] mimeMessage = bos.toByteArray();
				try (
                        ByteArrayInputStream ips = new ByteArrayInputStream(mimeMessage)) {

                    Message messageToSend = messageUtils.createMessage(customerOrder, ips,
                            mimeMessage.length,
                            MessageMetadata.MessageType.EMAIL, emailRecipients, null, null);
                    messageService.sendMessage(messageToSend, EventType.VENDOR_NOTIFICATION);
                } catch (IOException ex) {
                    LOG.error("Could not read mimeMessage to ByteArrayInputStream", ex);
                }
				updateShipTo(pendingAccountShipTo, shipTo);
			}
			
			
		} catch (Exception e) {
			LOG.error("Caught Exception:" + e.getMessage());
			this.abortWorkItem(wi, wim);
		}
	}

    private void updateShipTo(Address pendingAccountShipTo, Address shipTo) {
        String shipToId;
        shipToId = pendingAccountShipTo.getShipToId();

        shipTo.setShipToId(pendingAccountShipTo.getShipToId());
        if (shipTo.getMiscShipTo() == null) {
            shipTo.setMiscShipTo(new MiscShipTo());
        }
        shipTo.getMiscShipTo().setAddressId(shipToId);
        shipTo.getMiscShipTo().setUsAccount(pendingAccountShipTo.getMiscShipTo().getUsAccount());
        shipTo.setPendingShipToAuthorization(pendingAccountShipTo.getPendingShipToAuthorization());
        shipTo.setLine1(pendingAccountShipTo.getLine1());
        shipTo.setCity(pendingAccountShipTo.getCity());
        shipTo.setState(pendingAccountShipTo.getState());
        shipTo.setZip(pendingAccountShipTo.getZip());
        //Allow different companies based on inbound documents
        if (StringUtils.isEmpty(shipTo.getCompany())) {
            shipTo.setCompany(pendingAccountShipTo.getCompany());
        }
        if (StringUtils.isEmpty(shipTo.getMiscShipTo().getDivision())) {
            shipTo.getMiscShipTo().setDivision(shipTo.getMiscShipTo().getDivision());
        }
        shipTo.setCountry(pendingAccountShipTo.getCountry());
        shipTo = addressBp.update(shipTo);
    }

    public String prepareEmailMessage(String masterAccountNumber, String shipToId, String usscoNumber, String link,
            @NotNull Address shipTo, String sector, String accountName) {
        StringTemplate contentTemplate = new StringTemplate("<p>Within Master Acct # $masterAccountNumber$,<br />"
                + "please associate the new ShipTo ID $shipToId$ with the account number: $usscoNumber$</p>"
                + "<p>Account Name: $accountName$</p>" + "<p>Sector: $sector$</p>" + "<p>Address: </p>"
                + "<p>$line1$</p>" + "<p>$city$ $state$, $zip$</p>"
                + "<p>Please inform the APD Team once the new ShipTo ID has been created<br />"
                + "by replying to this email or clicking the following link:</p>" + "<p>" + link + "</p>");

        contentTemplate.setAttribute("masterAccountNumber", masterAccountNumber);
        contentTemplate.setAttribute("usscoNumber", usscoNumber);
        contentTemplate.setAttribute("shipToId", shipToId);
        contentTemplate.setAttribute("sector", sector);
        contentTemplate.setAttribute("accountName", accountName);
        contentTemplate.setAttribute("line1", StringUtils.isEmpty(shipTo.getLine1()) ? "" : shipTo.getLine1());
        contentTemplate.setAttribute("city", StringUtils.isEmpty(shipTo.getCity()) ? "" : shipTo.getCity());
        contentTemplate.setAttribute("state", StringUtils.isEmpty(shipTo.getState()) ? "" : shipTo.getState());
        contentTemplate.setAttribute("zip", StringUtils.isEmpty(shipTo.getZip()) ? "" : shipTo.getZip());

        return contentTemplate.toString();
    }
}
