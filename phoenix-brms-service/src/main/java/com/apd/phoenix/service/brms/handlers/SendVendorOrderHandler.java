/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.EmailFactoryBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.LineItemStatusBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.VendorBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.Vendor;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.POAcknowledgementDto;
import com.apd.phoenix.service.order.PlaceOrderService;
import com.apd.phoenix.service.persistence.jpa.CredentialXVendorDao;
import com.apd.phoenix.service.persistence.jpa.SequenceDao;

@Stateless
@LocalBean
public class SendVendorOrderHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(SendVendorOrderHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    MessageService messageService;

    @Inject
    VendorBp vendorBp;

    @Inject
    SequenceDao sequenceDao;

    @Inject
    CredentialXVendorDao credentialXVendorDao;

    @Inject
    EmailFactoryBp emailFactoryBp;

    @Inject
    PlaceOrderService placeOrderService;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Inject
    private LineItemStatusBp itemStatusBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        
        
        String vendorName = (String) wi.getParameter("vendorName");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));

        Object passString = wi.getParameter("pass");
        Integer pass = (passString == null || StringUtils.isBlank(passString.toString())) ? null : Integer.parseInt(passString.toString());
        POAcknowledgementDto poAcknowledgementDto = (POAcknowledgementDto) wi.getParameter("vendorPoAcknowledementDto");
        
        if (vendorName == null || vendorName.isEmpty()){
        	//All vendors for the order will be contacted
        	if (poAcknowledgementDto == null) {
        		//Not a retry
    			placeOrderService.sendOrder(customerOrder);
    		} else {
    			//Retry the order to all vendors?
    		}
        } else {
        	//A vendor has been specified for communication
        	Vendor search = new Vendor();
        	search.setName(vendorName);
        	List<Vendor> results = vendorBp.searchByExample(search, 0, 1);
        	if (results.isEmpty()){
        		logger.error("Tried to send an order to a vendor that doesn't exist!!!");
        	} else {
        		Vendor vendor = results.get(0);
        		if (poAcknowledgementDto == null) {
        			//Not a retry
        			placeOrderService.sendOrder(vendor, customerOrder);
        		} else {
        			//Retry the order to a specific vendor
        			Set<LineItem> rejectedItems = new HashSet<>();
        			for (LineItemDto lineItemDto : poAcknowledgementDto.getLineItems()) {
        				//Only retry items that were rejected on the po acknowledgement
        				//canBeRetried is null for legacy orders, and so always retries if null
        				if (lineItemDto.getValid() && lineItemDto.getStatus().getValue().equalsIgnoreCase("REJECTED") 
        						&& (lineItemDto.getCanBeRetried() == null || lineItemDto.getCanBeRetried())) {
        					LineItem retrievedLineItem = lineItemBp.retrieveLineItem(lineItemDto, customerOrder);
        					//RESET the item back to accepted so that it can be sent to the vendor again
        					lineItemBp.setStatus(retrievedLineItem, itemStatusBp.getStatus(LineItemStatus.LineItemStatusEnum.RESENT));
							rejectedItems.add(retrievedLineItem);
        				}
        			}
        			placeOrderService.sendRetryOrder(vendor, customerOrder, rejectedItems, pass);
        		}
        	}
        }
        return toReturn;
    }
}
