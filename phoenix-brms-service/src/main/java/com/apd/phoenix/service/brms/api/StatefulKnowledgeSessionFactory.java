package com.apd.phoenix.service.brms.api;

import javax.ejb.Stateless;
import org.drools.runtime.StatefulKnowledgeSession;

@Stateless
public interface StatefulKnowledgeSessionFactory {

    public StatefulKnowledgeSession loadStatefulKnowledgeSession(int sessionId, String snapshot);

    public StatefulKnowledgeSession newStatefulKnowledgeSession();
}
