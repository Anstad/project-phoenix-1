package com.apd.phoenix.service.brms.impl;

import javax.inject.Inject;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jbpm.task.TaskService;
import com.apd.phoenix.service.brms.api.BRMSCommandHelper;
import org.drools.runtime.process.WorkItemHandler;
import org.jbpm.task.service.TaskClient;

public class AccountCommandHelper implements BRMSCommandHelper {

    @Inject
    BRMSServiceImpl apdBRMSService;

    @Override
    public StatefulKnowledgeSession getStatefulKnowledgeSession(long accountId) {
        return apdBRMSService.getStatefulKnowledgeSessionByAccountId(accountId);
    }

    @Override
    public TaskService getTaskService() {
        return apdBRMSService.getTaskServiceClient();
    }

    @Override
    public void registerHandlers(StatefulKnowledgeSession session, WorkItemHandler humanTaskHandler) {
        apdBRMSService.registerHandlers(session, humanTaskHandler);
    }

    @Override
    public void finishedWith(long id) {
        apdBRMSService.setSessionInactiveByAccountId(id);
    }

    @Override
    public void setRecordProcessId(long id, long processId) {
        apdBRMSService.setAccountRecordProcessId(id, processId);
    }

    @Override
    public boolean isStale(long recordId) {
        return false;
    }

    @Override
    public long getProcessId(long recordId) {
        return apdBRMSService.getAccountProcessId(recordId);
    }

    @Override
    public Object getFact(long recordId) {
        return recordId;
    }

}
