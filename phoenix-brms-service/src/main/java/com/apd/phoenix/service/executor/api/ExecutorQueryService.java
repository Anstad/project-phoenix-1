/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.executor.api;

import java.util.List;
import com.apd.phoenix.service.executor.entities.ErrorInfo;
import com.apd.phoenix.service.executor.entities.RequestInfo;

public interface ExecutorQueryService {

    List<RequestInfo> getQueuedRequests();

    List<RequestInfo> getExecutedRequests();

    List<RequestInfo> getInErrorRequests();

    List<RequestInfo> getCancelledRequests();

    List<ErrorInfo> getAllErrors();

    List<RequestInfo> getAllRequests();
}
