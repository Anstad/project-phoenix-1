/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerCreditInvoiceBp;
import com.apd.phoenix.service.business.CustomerInvoiceBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.business.VendorInvoiceBp;
import com.apd.phoenix.service.message.api.MessageService;
import com.apd.phoenix.service.model.CustomerCreditInvoice;
import com.apd.phoenix.service.model.CustomerInvoice;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItemStatus;
import com.apd.phoenix.service.model.LineItemXShipment;
import com.apd.phoenix.service.model.MessageMetadata.MessageType;
import com.apd.phoenix.service.model.Shipment;
import com.apd.phoenix.service.model.dto.CustomerCreditInvoiceDto;
import com.apd.phoenix.service.model.dto.CustomerInvoiceDto;
import com.apd.phoenix.service.model.dto.LineItemDto;
import com.apd.phoenix.service.model.dto.LineItemStatusDto;
import com.apd.phoenix.service.model.dto.LineItemXReturnDto;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ReturnOrderDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.dto.UnitOfMeasureDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import java.math.BigDecimal;

@Stateless
@LocalBean
public class InvoiceCustomerHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(InvoiceCustomerHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    VendorInvoiceBp vendorInvoiceBp;

    @Inject
    ShipmentBp shipmentBp;

    @Inject
    CustomerCreditInvoiceBp customerCreditInvoiceBp;

    @Inject
    CustomerInvoiceBp customerInvoiceBp;

    @Inject
    MessageService messageService;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        ReturnOrderDto returnOrderDto = (ReturnOrderDto) wi.getParameter("returnOrderDto");
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        if (shipmentDto != null){
        	toReturn = billCustomer(wi);
        } else if (returnOrderDto != null){
        	toReturn = creditCustomer(wi);
        }
        return toReturn;
    }

    private Map<String, Object> creditCustomer(WorkItem wi){
    	Map<String, Object> toReturn = new HashMap<>();
        ReturnOrderDto returnOrderDto = (ReturnOrderDto) wi.getParameter("returnOrderDto");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
    	LOGGER.info("Crediting the customer for the following return------------");
    	LOGGER.info("RA number = " + returnOrderDto.getRaNumber());
    	LOGGER.info("------------------------------------------------");
    	CustomerCreditInvoice customerCreditInvoice = customerCreditInvoiceBp.credit(returnOrderDto);
    	try {
    		CustomerCreditInvoiceDto customerCreditInvoiceDto = DtoFactory.createCustomerCreditInvoiceDto(customerCreditInvoice);
    		if(customerOrder.getCredential().getCreditInvoiceMessageType() != null && customerOrder.getCredential().getCreditInvoiceMessageType().equals(MessageType.CXML)) {
				LineItemXReturnDto lineItemXReturnDto = new LineItemXReturnDto();
				lineItemXReturnDto.setQuantity(BigInteger.ONE);
				LineItemDto lineItemDto = new LineItemDto();
				UnitOfMeasureDto unitOfMeasureDto = new UnitOfMeasureDto();
				unitOfMeasureDto.setName("EA");
				lineItemDto.setUnitOfMeasure(unitOfMeasureDto);
				lineItemDto.setUnitPrice(customerCreditInvoiceDto.getReturnOrderDto().getRestockingFee());
				lineItemDto.setFee(true);
				lineItemDto.setShortName("Restocking fee");
				lineItemDto.setEstimatedShippingAmount(BigDecimal.ZERO);
				lineItemXReturnDto.setLineItemDto(lineItemDto);
				customerCreditInvoiceDto.getReturnOrderDto().getItems().add(lineItemXReturnDto);
    		}
    		messageService.sendCreditInvoice(customerCreditInvoiceDto, customerOrder.getCredential().getCreditInvoiceMessageType());

    		if (customerOrder.getCredential().getCreditInvoiceMessageType() == null || !customerOrder.getCredential().getCreditInvoiceMessageType().equals(MessageType.EMAIL)) {
    			messageService.sendCreditInvoice(customerCreditInvoiceDto, MessageType.EMAIL);
    		}
    	} catch (ParsingException e) {
    		LOGGER.error("Could not create credit invoice for email",e);
    	}
    	return toReturn;
    }

    private Map<String, Object> billCustomer(WorkItem wi){
    	Map<String, Object> toReturn = new HashMap<>();
        ShipmentDto shipmentDto = (ShipmentDto) wi.getParameter("shipmentDto");
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
    	LOGGER.info("Creating shipment");
    	Shipment shipment = shipmentBp.retrieveShipment(shipmentDto);
    	if (shipment != null) {
    		LOGGER.info("Invoicing for the following shipment------------");
    		LOGGER.info("Tracking number = " + shipment.getTrackingNumber());
    		for (LineItemXShipment lixs : shipment.getItemXShipments()){
    			LOGGER.info("-" + lixs.getLineItem().getApdSku());
    			LOGGER.info("--Quantity=" + lixs.getQuantity());
    		}
    		LOGGER.info("------------------------------------------------");
    		CustomerInvoice customerInvoice = customerInvoiceBp.invoice(shipment);
    		for (LineItemXShipmentDto lixsd : shipmentDto.getLineItemXShipments()){
    			lixsd.setPaid(true);
    			if(lixsd.getLineItemDto().getStatus()!=null){
                            if(lixsd.getLineItemDto().getStatus().getValue().equals(LineItemStatus.LineItemStatusEnum.PARTIAL_SHIP.getValue())) {
                                    lixsd.getLineItemDto().setStatus(new LineItemStatusDto());
                                    lixsd.getLineItemDto().getStatus().setValue(LineItemStatus.LineItemStatusEnum.PARTIAL_BILL.getValue());
                            } else if (lixsd.getLineItemDto().getStatus().getValue().equals(LineItemStatus.LineItemStatusEnum.FULLY_SHIPPED.getValue())) {
                                    lixsd.getLineItemDto().setStatus(new LineItemStatusDto());
                                    lixsd.getLineItemDto().getStatus().setValue(LineItemStatus.LineItemStatusEnum.FULLY_BILLED.getValue());
                            }
                        }
    		}
    		CustomerInvoiceDto customerInvoiceDto = null;
			try {
				customerInvoiceDto = DtoFactory.createCustomerInvoiceDto(customerInvoice);
			} catch (ParsingException e) {
				LOGGER.error("Could not parse customer invoice dto:", e);
			}
			customerInvoiceDto.setReceiverCode(customerOrder.getCredential().getEdiReceiverCode());
			customerInvoiceDto.setEdiVersion(customerOrder.getCredential().getEdiVersion());
			customerInvoiceDto.setPartnerId(customerOrder.getCredential().getPartnerId());
			messageService.sendInvoice(customerInvoiceDto, customerOrder.getCredential().getInvoiceMessageType());
			if (customerOrder.getCredential().getInvoiceMessageType() == null || !customerOrder.getCredential().getInvoiceMessageType().equals(MessageType.EMAIL)) {
				messageService.sendInvoice(customerInvoiceDto, MessageType.EMAIL);
			}
	        //mark the line item x shipments as paid
	        if(shipment.getItemXShipments()!=null){
	            for(LineItemXShipment lixs: shipment.getItemXShipments()){
	                        lixs.setPaid(true);
	            }
	            shipmentBp.update(shipment);
	        }
    	}
    	else {
    		LOGGER.warn("No shipment found for shipment DTO, probably due to all items failing validation");
    	}
    	toReturn.put("outputDto", shipmentDto);
    	return toReturn;
    }
}
