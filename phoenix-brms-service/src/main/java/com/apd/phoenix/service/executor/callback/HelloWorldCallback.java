package com.apd.phoenix.service.executor.callback;

import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.api.CommandCallback;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

@Named(value = "HelloWorldCallback")
public class HelloWorldCallback implements CommandCallback {

    private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldCallback.class);

    @Override
    public void onCommandDone(CommandContext ctx, ExecutionResults results) {
        LOGGER.info("**Starting hello world callback**");

    }

}
