package com.apd.phoenix.service.executor.api;

public interface CommandCallback {

    void onCommandDone(CommandContext ctx, ExecutionResults results);
}
