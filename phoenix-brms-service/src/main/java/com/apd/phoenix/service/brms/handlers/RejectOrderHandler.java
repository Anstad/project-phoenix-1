/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CommentBp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.OrderStatusBp;
import com.apd.phoenix.service.model.Comment;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.OrderStatus.OrderStatusEnum;

@Stateless
@LocalBean
public class RejectOrderHandler extends SimpleAbortWorkItemHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RejectOrderHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    CommentBp commentBp;

    @Inject
    private OrderStatusBp orderStatusBp;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        String message = (String) wi.getParameter("message");
        
        customerOrderBp.setOrderStatus(customerOrder, orderStatusBp.getOrderStatus(OrderStatusEnum.REJECTED));
        customerOrder = customerOrderBp.internalRejectOrder(customerOrder);
        
        Comment comment = new Comment();
        comment.setCommentDate(new Date());
        comment.setContent(message);
        comment.setMadeBySystem(true);
        commentBp.addComment(customerOrder, comment);
        
        return toReturn;
    }
}
