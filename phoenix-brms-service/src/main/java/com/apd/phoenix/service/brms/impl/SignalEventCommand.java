package com.apd.phoenix.service.brms.impl;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.api.AbstractBRMSCommand;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.api.ExecutionResults;

@Named(value = "SignalEventCommand")
@Stateless
@LocalBean
public class SignalEventCommand extends AbstractBRMSCommand {

    @Inject
    BRMSServiceImpl apdBrmsService;

    private static final Logger LOG = LoggerFactory.getLogger(SignalEventCommand.class);

    public ExecutionResults execute_safe(CommandContext ctx) throws Exception {

        String type = (String) ctx.getData("type");
        Object event = ctx.getData("event");
        if (type == null) {
            throw new Exception("TYPE NOT SPECIFIED EXCEPTION");
        }
        else if (event == null) {
            throw new Exception("EVENT NOT SPECIFIED EXCEPTION");
        }
        else if (commandHelper.isStale(id)) {
            throw new Exception("RECORD IS STALE");
        }
        else {
            long processId = commandHelper.getProcessId(id);
            LOG.info("Signaling event: {}, processId: {}", type, processId);
            session.signalEvent(type, event, processId);
        }

        ExecutionResults executionResults = new ExecutionResults();
        return executionResults;
    }

}
