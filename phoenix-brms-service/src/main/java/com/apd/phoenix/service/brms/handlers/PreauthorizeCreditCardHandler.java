/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apd.phoenix.service.brms.handlers;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.lang.StringUtils;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderProcessLookupBp;
import com.apd.phoenix.service.business.CustomerOrderBp.AuthorizationStrategy;
import com.apd.phoenix.service.model.CredentialXCredentialPropertyType;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.payment.ws.CreditCardTransactionResult;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayPropertiesLoader;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayService;
import com.apd.phoenix.service.payment.ws.client.PaymentGatewayTransaction;
import com.apd.phoenix.service.payment.ws.client.VerificationTransaction;

@Stateless
@LocalBean
public class PreauthorizeCreditCardHandler extends SimpleAbortWorkItemHandler {

    private static final String PREAUTH_PROPERTY_TYPE = "cc preauth strategy (full, one, none)";

    private static final Logger LOGGER = LoggerFactory.getLogger(PreauthorizeCreditCardHandler.class);

    @Inject
    CustomerOrderBp customerOrderBp;

    @Inject
    PaymentGatewayService paymentGatewayService;

    @Inject
    private OrderProcessLookupBp processLookupBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {
        Map<String, Object> toReturn = new HashMap<>();
        //the variable name "orderId" is a misnomer, a legacy from when the process ID was set to the order ID
        CustomerOrder customerOrder = processLookupBp.getOrderFromProcessId((Long) wi.getParameter("orderId"));
        Integer transactionAttempts = (Integer) wi.getParameter("transactionAttempts");
        BigDecimal orderTotal = customerOrder.getOrderTotal();
        CreditCardTransactionResult cardTransactionResult;
        boolean verificationNeeded = customerOrder.getPaymentInformation() == null 
        		|| customerOrder.getPaymentInformation().getCard() == null
        		|| StringUtils.isBlank(customerOrder.getPaymentInformation().getCard().getCardVaultToken());
        if (verificationNeeded) {
        	LOGGER.info("Verfiying CC information for order id={}" + customerOrder.getId());
        	VerificationTransaction verificationTransaction = paymentGatewayService.createVerificationTransaction(customerOrder);
        	cardTransactionResult = paymentGatewayService.verification(verificationTransaction);
        	if (cardTransactionResult != null && !cardTransactionResult.isSucceeded()) {
            	toReturn.put("isAuthorized", false);
				toReturn.put("outputShouldRetryCard", false);
            	toReturn.put("cardTransactionResult", cardTransactionResult);
            	return toReturn;
            }
        	else if (cardTransactionResult == null) {
            	toReturn.put("isAuthorized", false);
    			if (transactionAttempts == null || transactionAttempts < PaymentGatewayPropertiesLoader.getInstance().getProperties().getInt("technicalErrorRetryAttempts", 5)) {
	    			toReturn.put("outputShouldRetryCard", true);
	    			toReturn.put("outputTransactionAttempts", transactionAttempts != null ? transactionAttempts + 1 : 1);
    			}
    			else {
    				toReturn.put("outputShouldRetryCard", false);
    			}
    			return toReturn;
    		}
        }
        
	    String credentialPreAuthProperty = null;
        for (CredentialXCredentialPropertyType property : customerOrder.getCredential().getProperties()) {
        	if (property.getType().getName().equalsIgnoreCase(PREAUTH_PROPERTY_TYPE)) {
        		credentialPreAuthProperty = property.getValue();
        	}
        }
        AuthorizationStrategy strategy;
        if (!StringUtils.isEmpty(credentialPreAuthProperty)
                && credentialPreAuthProperty.equalsIgnoreCase(AuthorizationStrategy.ONE.toString())) {
            strategy = AuthorizationStrategy.ONE;
        }
        else if (!StringUtils.isEmpty(credentialPreAuthProperty)
                && credentialPreAuthProperty.equalsIgnoreCase(AuthorizationStrategy.NONE.toString())) {
            strategy = AuthorizationStrategy.NONE;
        }
        else {
            strategy = AuthorizationStrategy.FULL;
        }
        if (strategy == AuthorizationStrategy.NONE) {
        	LOGGER.info("CC authorization not required for the integrated order id=${}",customerOrder.getId());
        	toReturn.put("isAuthorized", true);
        } else {
        	LOGGER.info("Preauthorizing CC for the amount: ${}",orderTotal);
            PaymentGatewayTransaction transaction = paymentGatewayService
                    .createPaymentGatewayTransaction(customerOrder);
            cardTransactionResult = paymentGatewayService.authorize(transaction);
            if (cardTransactionResult != null && !cardTransactionResult.isSucceeded()) {
            	LOGGER.error("CC authorization failed for order id={}.", customerOrder.getId());
                if(cardTransactionResult.getFailureReason()!=null){
                    LOGGER.error(cardTransactionResult.getFailureReason().value());
                } else {
                    LOGGER.error("No reason given for failure.");
                }
            	toReturn.put("isAuthorized", false);
    			toReturn.put("outputShouldRetryCard", false);
            	toReturn.put("cardTransactionResult", cardTransactionResult);
            }
    		else if (cardTransactionResult == null) {
            	toReturn.put("isAuthorized", false);
    			if (transactionAttempts == null || transactionAttempts < PaymentGatewayPropertiesLoader.getInstance().getProperties().getInt("technicalErrorRetryAttempts", 5)) {
	    			toReturn.put("outputShouldRetryCard", true);
	    			toReturn.put("outputTransactionAttempts", transactionAttempts != null ? transactionAttempts + 1 : 1);
    			}
    			else {
    				toReturn.put("outputShouldRetryCard", false);
    			}
    		} else {
            	toReturn.put("isAuthorized", true);
            }
        }
        return toReturn;
    }
}
