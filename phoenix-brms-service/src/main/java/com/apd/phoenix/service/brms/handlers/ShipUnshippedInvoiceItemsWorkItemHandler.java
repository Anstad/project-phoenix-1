package com.apd.phoenix.service.brms.handlers;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.drools.runtime.process.WorkItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.business.LineItemBp;
import com.apd.phoenix.service.business.OrderLogBp;
import com.apd.phoenix.service.business.ShipmentBp;
import com.apd.phoenix.service.business.VendorInvoiceBp;
import com.apd.phoenix.service.model.CustomerOrder;
import com.apd.phoenix.service.model.LineItem;
import com.apd.phoenix.service.model.VendorInvoice;
import com.apd.phoenix.service.model.dto.LineItemXShipmentDto;
import com.apd.phoenix.service.model.dto.ShipmentDto;
import com.apd.phoenix.service.model.factory.DtoFactory;
import com.apd.phoenix.service.model.factory.DtoFactory.ParsingException;
import com.apd.phoenix.service.workflow.WorkflowService;

@Stateless
@LocalBean
public class ShipUnshippedInvoiceItemsWorkItemHandler extends SimpleAbortWorkItemHandler {

    private static final Logger logger = LoggerFactory.getLogger(ShipUnshippedInvoiceItemsWorkItemHandler.class);

    @Inject
    VendorInvoiceBp vendorInvoiceBp;

    @Inject
    LineItemBp lineItemBp;

    @Inject
    WorkflowService workflowService;

    @Inject
    OrderLogBp orderLogBp;

    @Override
    public Map<String, Object> obtainResults(WorkItem wi) {

        Long invoiceId = (Long) wi.getParameter("inputInvoiceId");
        if (invoiceId == null) {
            logger.error("Unexpected null invoice Id");
            return null;
        }
        VendorInvoice invoice = vendorInvoiceBp.findById(invoiceId, VendorInvoice.class);
        if (invoice == null) {
            logger.error("Unexpectedly null invoice with invoice Id" + invoiceId);
        }

        String shipNumber = invoice.getInvoiceNumber() + "-810";
        ShipmentDto shipmentDto = DtoFactory.createBasicShipmentDto(invoice.getCustomerOrder(), shipNumber);
        BigDecimal shipping = BigDecimal.ZERO;
        BigDecimal totalShipPrice = shipping;

        for (LineItem invoiceItem : invoice.getActualItems()) {
            if (invoiceItem.getExpected() != null) {
                LineItem expected = lineItemBp.findById(invoiceItem.getExpected().getId(), LineItem.class);
                BigInteger quantityShipped = new BigInteger(expected.getQuantityShipped().toString());
                if (-1 == quantityShipped.compareTo(expected.getQuantityInvoicedByVendor())) {
                    BigInteger quantity = expected.getQuantityInvoicedByVendor().subtract(quantityShipped);
                    LineItemXShipmentDto lixs = lineItemBp.createLineItemXShipmentDto(expected, quantity.intValue(),
                            shipping);
                    shipmentDto.getLineItemXShipments().add(lixs);
                    totalShipPrice = totalShipPrice.add(shipping);
                    totalShipPrice = totalShipPrice.add(expected.getUnitPrice().multiply(new BigDecimal(quantity)));
                }
            }
        }

        shipmentDto.setShipmentPrice(totalShipPrice);
        if (!shipmentDto.getLineItemXShipments().isEmpty()) {
            logger.info("Manually shipping items after receiving invoice from vendor.");
            workflowService.processOrderShipment(shipmentDto);
            orderLogBp.logChangeOrder(invoice.getCustomerOrder(), "created the shipment, " + shipNumber
                    + ", because the vendor bill processed ahead of the vendor shipment notice", null);
        }

        return null;
    }

}
