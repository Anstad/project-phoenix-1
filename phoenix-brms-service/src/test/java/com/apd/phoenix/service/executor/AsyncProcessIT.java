package com.apd.phoenix.service.executor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import javax.inject.Inject;
import junit.framework.Assert;
import org.drools.runtime.StatefulKnowledgeSession;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.executor.entities.ErrorInfo;
import com.apd.phoenix.service.executor.entities.RequestInfo;
import com.apd.phoenix.service.persistence.jpa.ManagedResources;

@RunWith(Arquillian.class)
public class AsyncProcessIT {

    @Inject
    protected ExecutorServiceEntryPoint executor;
    public static final Map<String, Object> cachedEntities = new HashMap<String, Object>();

    public static StatefulKnowledgeSession session;
    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncProcessIT.class);

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addClasses(ManagedResources.class,
                SimpleIncrementCallback.class, ThrowExceptionCommand.class).addAsResource(
                "executor/executor-persistence.xml", "META-INF/persistence.xml").addAsResource(
                "executor/Executor-orm.xml", "META-INF/Executor-orm.xml").addPackages(true,
                "com.apd.phoenix.service.executor").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("executor/change-set.xml", "executor/change-set.xml").addAsResource(
                        "executor/async-work-item-wait.bpmn", "executor/async-work-item-wait.bpmn").addAsLibraries(
                        resolver.artifacts("org.hornetq:hornetq-core:2.2.11.Final", "org.jbpm:jbpm-bam:5.3.1.BRMS",
                                "org.jbpm:jbpm-human-task:5.3.1.BRMS", "org.jbpm:jbpm-bpmn2:5.3.1.BRMS",
                                "org.jbpm:jbpm-flow-builder:5.3.1.BRMS", "org.jbpm:jbpm-flow:5.3.1.BRMS",
                                "org.drools:knowledge-api:5.3.1.BRMS", "org.drools:drools-core:5.3.1.BRMS",
                                "org.drools:drools-compiler:5.3.1.BRMS",
                                "org.drools:drools-persistence-jpa:5.3.1.BRMS", "org.drools:drools-spring:5.3.1.BRMS",
                                "org.apache.camel:camel-core:2.11.0", "org.apache.camel:camel-stream:2.11.0",
                                "commons-lang:commons-lang:2.6").resolveAsFiles());
        //LOGGER.info(test.toString(true));
        return test;

    }

    /**
     * Tests a simple command request.
     * @throws InterruptedException 
     */
    @Test
    public void simpleExcecutionTest() throws InterruptedException {
        CommandContext ctxCMD = new CommandContext();
        ctxCMD.setData("businessKey", UUID.randomUUID().toString());

        //A job is scheduled by using its CDI @Name
        executor.scheduleRequest("PrintOutCmd", ctxCMD);

        Thread.sleep(10000);

        //after 10 seconds we should have no errors, no queued requests and
        //one executed request.
        List<RequestInfo> inErrorRequests = executor.getInErrorRequests();
        Assert.assertEquals(0, inErrorRequests.size());
        List<RequestInfo> queuedRequests = executor.getQueuedRequests();
        Assert.assertEquals(0, queuedRequests.size());
        List<RequestInfo> executedRequests = executor.getExecutedRequests();
        Assert.assertEquals(1, executedRequests.size());

    }

    /**
     * Tests callback execution after a command was successfully executed.
     * @throws InterruptedException 
     */
    @Test
    public void callbackTest() throws InterruptedException {

        CommandContext commandContext = new CommandContext();

        //We register a business key in the command context so we can add
        //extra information on it.
        commandContext.setData("businessKey", UUID.randomUUID().toString());

        //We are going to put a new AtomicLong in the context. The idea is 
        //that the callback we will register will get this 'entity' and increments
        //its value.
        cachedEntities.put((String) commandContext.getData("businessKey"), new AtomicLong(1));

        //A job is scheduled. Using commandContext we can register a callback
        //using its CDI name.
        commandContext.setData("callbacks", "SimpleIncrementCallback");
        executor.scheduleRequest("PrintOutCmd", commandContext);

        Thread.sleep(10000);

        //after 10 seconds we should have no errors, no queued requests and
        //one executed request.
        List<RequestInfo> inErrorRequests = executor.getInErrorRequests();
        Assert.assertEquals(0, inErrorRequests.size());
        List<RequestInfo> queuedRequests = executor.getQueuedRequests();
        Assert.assertEquals(0, queuedRequests.size());
        List<RequestInfo> executedRequests = executor.getExecutedRequests();
        Assert.assertEquals(1, executedRequests.size());

        //Since the callback was invoked, the value of the entity should have
        //been incremented.
        Assert.assertEquals(2, ((AtomicLong) cachedEntities.get((String) commandContext.getData("businessKey")))
                .longValue());

    }

    /**
     * Test showing the exception handling mechanism of the Executor Service.
     * @throws InterruptedException 
     */
    @Test
    public void executorExceptionTest() throws InterruptedException {

        CommandContext commandContext = new CommandContext();
        commandContext.setData("businessKey", UUID.randomUUID().toString());
        cachedEntities.put((String) commandContext.getData("businessKey"), new AtomicLong(1));

        //Same callback as the precious test
        commandContext.setData("callbacks", "SimpleIncrementCallback");

        //no retries please.
        commandContext.setData("retries", 0);

        //The command we are registering will cause an exception.
        executor.scheduleRequest("ThrowExceptionCmd", commandContext);

        Thread.sleep(10000);

        //After 10 seconds, we should have a failing request.
        List<RequestInfo> inErrorRequests = executor.getInErrorRequests();
        Assert.assertEquals(1, inErrorRequests.size());
        LOGGER.info("Error: " + inErrorRequests.get(0));

        List<ErrorInfo> errors = executor.getAllErrors();
        LOGGER.info(" >>> Errors: " + errors);
        Assert.assertEquals(1, errors.size());

    }

    /**
     * Test showing the retry mechanism for failing commands.
     * @throws InterruptedException 
     */
    @Test
    public void defaultRequestRetryTest() throws InterruptedException {
        CommandContext ctxCMD = new CommandContext();
        ctxCMD.setData("businessKey", UUID.randomUUID().toString());

        //The command we are registering will cause an exception.
        //Remeber that the default number of reties is 3.
        executor.scheduleRequest("ThrowExceptionCmd", ctxCMD);

        Thread.sleep(12000);

        //After 12 seconds we should have 4 errors: 1 corresponding to the
        //first time the command failed. The other 3 correspond to the 3
        //retries.
        List<RequestInfo> inErrorRequests = executor.getInErrorRequests();
        Assert.assertEquals(1, inErrorRequests.size());

        List<ErrorInfo> errors = executor.getAllErrors();
        LOGGER.info(" >>> Errors: " + errors);
        // Three retries means 4 executions in total 1(regular) + 3(retries)
        Assert.assertEquals(4, errors.size());

    }

    /**
     * Test showing how a request can be canceled.
     * @throws InterruptedException 
     */
    @Test
    public void cancelRequestTest() throws InterruptedException {
        CommandContext ctxCMD = new CommandContext();
        ctxCMD.setData("businessKey", UUID.randomUUID().toString());

        //Schedule a task.
        Long requestId = executor.scheduleRequest("PrintOutCmd", ctxCMD);

        // cancel the task immediately
        executor.cancelRequest(requestId);

        //We should see the canceled task now
        List<RequestInfo> cancelledRequests = executor.getCancelledRequests();
        Assert.assertEquals(1, cancelledRequests.size());

    }

    @Before
    public void setUp() {
        executor.setThreadPoolSize(1);
        executor.setInterval(3);
        executor.init();
    }

    @After
    public void tearDown() {
        executor.clearAllRequests();
        executor.clearAllErrors();
        executor.destroy();
    }

}
