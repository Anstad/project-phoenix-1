package com.apd.phoenix.service.executor;

import java.util.UUID;
import javax.inject.Inject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.apd.phoenix.service.brms.impl.BRMSServiceImpl;
import com.apd.phoenix.service.brms.impl.SignalEventCommand;
import com.apd.phoenix.service.brms.impl.StartProcessCommand;
import com.apd.phoenix.service.business.AbstractBp;
import com.apd.phoenix.service.business.AccountBRMSRecordBp;
import com.apd.phoenix.service.business.AccountBp;
import com.apd.phoenix.service.business.Bp;
import com.apd.phoenix.service.business.CustomerOrderBp;
import com.apd.phoenix.service.business.OrderBRMSRecordBp;
import com.apd.phoenix.service.business.ProcessBp;
import com.apd.phoenix.service.executor.api.CommandContext;
import com.apd.phoenix.service.persistence.jpa.AbstractDao;
import com.apd.phoenix.service.persistence.jpa.AccountBRMSRecordDao;
import com.apd.phoenix.service.persistence.jpa.AccountDao;
import com.apd.phoenix.service.persistence.jpa.CustomerOrderDao;
import com.apd.phoenix.service.persistence.jpa.Dao;
import com.apd.phoenix.service.persistence.jpa.ManagedResources;
import com.apd.phoenix.service.persistence.jpa.OrderBRMSRecordDao;
import com.apd.phoenix.service.persistence.jpa.PoNumberDao;
import com.apd.phoenix.service.persistence.jpa.ProcessDao;

@RunWith(Arquillian.class)
public class WorkflowExecutorIT {

    @Inject
    BRMSServiceImpl apdBrmsService;

    @Inject
    protected ExecutorServiceEntryPoint executor;

    @Inject
    SignalEventCommand cmd1;

    @Inject
    StartProcessCommand cmd2;

    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowExecutorIT.class);

    @Deployment
    public static WebArchive createDeployment() {
        // Create a resolver that will get dependencies from the pom for use
        // it the tests.
        MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom(
                "pom.xml");

        // Build and return a dummy deployment of the app for testing.
        WebArchive test = ShrinkWrap.create(WebArchive.class, "executor-test.war").addPackages(true,
                "com.apd.phoenix.service.brms").addPackages(true, "com.apd.phoenix.service.model").addClasses(
                ManagedResources.class, Dao.class, ProcessDao.class, CustomerOrderDao.class, PoNumberDao.class,
                OrderBRMSRecordBp.class, AbstractBp.class, Bp.class, OrderBRMSRecordDao.class, AccountDao.class,
                AccountBRMSRecordBp.class, AccountBRMSRecordDao.class, AbstractDao.class, CustomerOrderBp.class,
                AccountBp.class, ProcessBp.class).addAsResource("executor/executor-persistence.xml",
                "META-INF/persistence.xml").addAsResource("executor/Executor-orm.xml", "META-INF/Executor-orm.xml")
                .addPackages(true, "com.apd.phoenix.service.executor").addAsWebInfResource(EmptyAsset.INSTANCE,
                        "beans.xml").addAsResource("executor/change-set.xml", "executor/change-set.xml").addAsResource(
                        "executor/async-work-item-wait.bpmn", "executor/async-work-item-wait.bpmn").addAsLibraries(
                        resolver.artifacts("org.jbpm:jbpm-bam:5.3.1.BRMS", "org.jbpm:jbpm-human-task:5.3.1.BRMS",
                                "org.jbpm:jbpm-bpmn2:5.3.1.BRMS", "org.jbpm:jbpm-flow-builder:5.3.1.BRMS",
                                "org.jbpm:jbpm-flow:5.3.1.BRMS", "org.jbpm:jbpm-persistence-jpa:5.3.1.BRMS",
                                "org.drools:knowledge-api:5.3.1.BRMS", "org.drools:drools-core:5.3.1.BRMS",
                                "org.drools:drools-compiler:5.3.1.BRMS",
                                "org.drools:drools-persistence-jpa:5.3.1.BRMS", "org.drools:drools-spring:5.3.1.BRMS",
                                "org.apache.camel:camel-core:2.11.0", "org.apache.camel:camel-stream:2.11.0",
                                "org.codehaus.btm:btm:2.1.2").resolveAsFiles());

        // LOGGER.info(test.toString(true));
        return test;

    }

    @Before
    public void setUp() throws Exception {
        executor.init();

    }

    @After
    public void tearDown() throws Exception {
        executor.destroy();

    }

    @Test
    public void signalTest() throws Exception {

        CommandContext ctxCMD1 = new CommandContext();
        ctxCMD1.setData("processName", "phoenix.signalTest");
        ctxCMD1.setData("commandKey", UUID.randomUUID().toString());
        ctxCMD1.setData("callbacks", "HelloWorldCallback");
        ctxCMD1.setData("idType", "CustomerOrder");
        ctxCMD1.setData("id", new Long(1747));
        ctxCMD1.setData("type", "testSignal");
        ctxCMD1.setData("event", "Hello world");
        cmd1.execute(ctxCMD1);
        //executor.scheduleRequest("StartProcessCommand", ctxCMD1);
        Thread.sleep(30000);

        CommandContext ctxCMD2 = new CommandContext();
        ctxCMD2.setData("commandKey", UUID.randomUUID().toString());
        ctxCMD2.setData("callbacks", "HelloWorldCallback");
        ctxCMD2.setData("idType", "CustomerOrder");
        ctxCMD2.setData("id", new Long(1747));
        ctxCMD2.setData("type", "testSignal");
        ctxCMD2.setData("event", "Hello world");
        cmd2.execute(ctxCMD2);
        //executor.scheduleRequest("SignalEventCommand", ctxCMD2);
        Thread.sleep(30000);
    }

}
