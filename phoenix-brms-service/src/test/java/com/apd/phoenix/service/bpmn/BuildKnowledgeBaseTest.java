package com.apd.phoenix.service.bpmn;

import java.util.HashMap;
import java.util.Map;
import org.drools.builder.ResourceType;
import org.drools.io.Resource;
import org.drools.io.ResourceFactory;
import org.drools.io.impl.UrlResource;
import org.junit.Assert;
import org.junit.Test;

public class BuildKnowledgeBaseTest extends BrmsBaseTest {

    private static final String URL = "http://10.0.1.254:8080/jboss-brms/org.drools.guvnor.Guvnor/package/phoenix/LATEST";
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "BRMSadm!n";

    @Override
	protected Map<Resource, ResourceType> getResources() {
		Map<Resource, ResourceType> resources = new HashMap<>();
		
		Resource repo = setupUrlResource(URL);
		resources.put(repo, ResourceType.PKG);
		
		return resources;
	}

    private Resource setupUrlResource(String url) {
        UrlResource resource = (UrlResource) ResourceFactory.newUrlResource(url);
        resource.setBasicAuthentication("enabled");
        // TODO: this needs to be pulled out
        resource.setUsername(USERNAME);
        resource.setPassword(PASSWORD);
        return resource;
    }

    @Test
    public void testBuild() {
        Assert.assertTrue(true);
    }
}
